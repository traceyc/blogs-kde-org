---
title:   "Qt Cryptographic Architecture moves into KDE"
date:    2004-09-28
authors:
  - brad hards
slug:    qt-cryptographic-architecture-moves-kde
---
The Qt Crytographic Architecture (QCA) was originally written by Justin Karneges as part of the PSI instant messanger. After a little bit of discussion, and an agreement on how it will work out, he's agreed to move it into the kdesupport module (KDE already has two copies - one in kdenetwork, and another in kdenonbeta).

The plan is to develop it in kdesupport, and still maintain cross platform capability. Releases will probably happen outside of the normal KDE cycle. The model is taglib (which is the other library sleeping on the couch in kdesupport), except with a Qt dependency.

I've done a bit of work on it, adding some unit tests, and some API documentation. I also have a PBKDF2 ( aka PKCS#5, aka RFC2898 ) routine to add (heavily based on the code in the Secure Programming Cookbook, because I am a crypto nobody), but its still pretty ugly. It might also be nice to have some more unit tests for it - I only have one (from OpenOffice.org), and there are code paths that aren't being touched in that test.

I also have to do a new web-page (featuring whatever API doco we have), and put it on developer.kde.org.

If Qt and crypto is your thing, come join us!