---
title:   "Plasma tutorial video - online"
date:    2008-02-03
authors:
  - brad hards
slug:    plasma-tutorial-video-online
---
For those that missed Aaron's <a href="http://linux.conf.au/programme/detail?TalkID=296">Plasma tutorial</a> at linux.conf.au 2008, the good news is you can now view it online. 

<br>
You can have slides, ogg video, or ogg speex:
<ul>
<li><a href="http://linux.conf.au/attachment/60">Slides&nbsp;1</a></li>
<li><a href="http://mirror.linux.org.au/pub/linux.conf.au/2008/Thu/mel8-296a.ogg">OGG part 1</a></li>
<li><a href="http://mirror.linux.org.au/pub/linux.conf.au/2008/Thu/mel8-296b.ogg">OGG part 2</a></li>
<li><a href="http://mirror.linux.org.au/pub/linux.conf.au/2008/Thu/mel8-296a.spx">SPX part 1</a></li>
<li><a href="http://mirror.linux.org.au/pub/linux.conf.au/2008/Thu/mel8-296b.spx">SPX part 2</a></li>
</ul>

[Sorry if someone already noted this]