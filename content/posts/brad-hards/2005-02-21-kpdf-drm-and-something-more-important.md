---
title:   "On KPDF - DRM and something more important"
date:    2005-02-21
authors:
  - brad hards
slug:    kpdf-drm-and-something-more-important
---
I'm not going to argue the morality of DRM - others have done that, and another opinion won't really change anything.

However those arguing against the specific changes made to KPDF by the authors might like to consider that Adobe owns the PDF specification, and claims ownership of the datastructures and operators defined in that spec. The copyright permission is conditional on making "reasonable efforts" to implement the restrictions (see the PDF specification, Version 1.5, section 1.4 for the condition and Table 3.20 for the specific restrictions).

On that basis, I think that the authors of a package that is impacted by those conditions should have the right to respect them - others might not like the consequences, but we aren't forcing people to use KPDF, and the authors face the repercussions of their actions, or inactions. 

I promised (in the title) that I'd also write about something more important. Well, more important to me, perhaps. What would you like to see in the KPDF manual? Any really cool hints or tips? Fun stuff? How much of "KDPF implements Adobe PDF Specification content, that is Copyright Adobe, the people who got Dmitri imprisoned" would we like to see :-)