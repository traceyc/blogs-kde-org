---
title:   "Qt bindings for libusb"
date:    2004-03-01
authors:
  - brad hards
slug:    qt-bindings-libusb
---
I've committed Qt bindings for libusb - see http://sourceforge.net/projects/libusb/

This includes API docs and unit tests.

This isn't released yet, so you'll need to suck down the CVS version. I'm a bit worried that the bindings aren't really Qt-ish enough, because I'm not very experienced with class design. So I'm taking advice (and patches!). You'll need to provide fairly specific advice though - at least the interface needs to be specific.

Even if you just compile test it, I'd still like to know the results.