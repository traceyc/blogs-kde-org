---
title:   "Qt Crypto Architecture for KDE4?"
date:    2005-09-11
authors:
  - brad hards
slug:    qt-crypto-architecture-kde4
---
One of the things I'd hoped to do at aKademy was to get some discussion going about using QCA in Qt4. Unfortunately I had a cold, and spent much too much time sleeping and feeling unmotivated instead. So I thought I'd try to kick off some discussion about it now. I'm also going to post this to kde-core-devel, and that is where the discussion should take place.

Firstly, a bit about QCA:
It provides a range of encryption/decryption, hashing, random number, secure transport and authentication support - the normal crypto functions that you'd be looking for. The design is essentially a Bridge - a standard API, and pluggable back-ends (called "providers") that actually implement the crypto operations. A provider is normally an Adapter that calls a standard library - Right now we have providers that use OpenSSL, gcrypt, Botan, gnupg.  You can change out providers at run-time. QCA also provides some functionality using an internal provider - things like basic random number support, MD5 and SHA1. The API is almost completely documented in Doxygen, and there are some examples (linked into the Doxygen output, and also compilable - its a nice trick). There are unit tests for most of the lower level API, and I'm going to convert them to QtTestLib.

Working with KDE:
There are three ways in which KDE could use QCA (and they aren't completely exclusive, but making a decision as to the general direction would probably save work and be cleaner).
1. Re-implement the kdelibs crypto related classes (stuff like KMD5/KMD4, perhaps part of KCodecs, KSSL) using QCA. So the API would remain pretty much the same, but would be implemented using QCA.
2. Remove most/all of the existing crypto code from kdelibs (and possibly other places) and use QCA instead. If there are things that QCA can't do that are needed, we just implement the additional parts in QCA. Applications then just use QCA for all their crypto needs.
3. Use QCA for the simple stuff (removing KMD4/KMD5) and implement a different KDE specific API over the top of QCA for the more complex stuff (eg for SASL, SSL, PGP).
As I see it, the second options would be the cleanest, but probably the most work for the application authors. The third is probably the most flexible, and might be needed to ensure that QCA doesn't become dependent on any part of KDE.


Issues:
* Is there a desire to change out the current dependency on OpenSSL? It is a bit of work, and the advantages are really only in not having to deal with the ugly OpenSSL API (which might make it easier for more applications to support security/privacy functions), and for isolating the crypto parts (which might be a bigger deal for distributions).
* The providers aren't all done. Surely they will be more complete by KDE4, but I don't want to hold up any development because of lack of functionality.
* The API isn't set yet. This cuts both ways - we can still fix it, but it isn't stable. It is pretty close though, and we will provide API and ABI compatibility through the life of Qt4 (unless a change is required to fix a security issue, although that seems unlikely).
* Requiring QCA does introduce another compile time dependency. We wouldn't need openssl, but then most people will already have it.

As I said above, look for the post on kde-core-devel, and please provide feedback.