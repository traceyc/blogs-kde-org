---
title:   "C++ bindings to hardware"
date:    2003-11-24
authors:
  - brad hards
slug:    c-bindings-hardware
---
I've been working on C++ bindings for <a href="http://libusb.sf.net">libusb</a>, which is a cross platform library for accessing USB devices. Currently I have a really hierachial class structure, where each Endpoint is in a Qt:QPtrList - the Interface. Each Interface is in a Qt:QPtrList - the Alternative Setting. Each AltSetting is in a Qt:QPtrList - the Configuration. Each Configuration is in a Qt:QPtrList - the Device. Each Device is in a Qt:QPtrList - the Bus. Each Bus is in a Qt:QPtrList - the Busses.

Right now I'm not seeing a simpler way (although perhaps dropping out the Configuration and AltSetting steps might be possible). If anyone has particular needs for C++ bindings for PCI or USB, I'd appreciate hearing them. Or if you know how to fix the class design - I definately want to talk. Also, some of the libusb people would prefer STL. I'm still undecided on that - I need Qt compatible, if not Qt.

The goal out of this is to support Logitech mice in a nice way for the mouse KCM - I have a command line tool, but it is way sucky. We should be able to do resolution switching, battery status, channel, etc.
