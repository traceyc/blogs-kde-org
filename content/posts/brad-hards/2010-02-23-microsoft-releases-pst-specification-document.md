---
title:   "Microsoft releases PST specification document"
date:    2010-02-23
authors:
  - brad hards
slug:    microsoft-releases-pst-specification-document
---
Looks like Microsoft has released the PST format specification.

I don't normally like to link to MSDN, but I'll do it this once:
<a href="http://msdn.microsoft.com/en-us/library/ff385210.aspx">http://msdn.microsoft.com/en-us/library/ff385210.aspx</a>

As usual with these documents, I recommend reading the PDF version rather than the HTML. Also, Firefox seems to handle MSDN a bit better than my (KDE 4.3.5) Konqueror.

If you were a mad-keen PIM hacker, and looking for a GSoC project, might be worth a look.

[Thanks to Tom Devey for the heads-up on this]
