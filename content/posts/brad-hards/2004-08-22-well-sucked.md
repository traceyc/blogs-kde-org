---
title:   "Well that sucked"
date:    2004-08-22
authors:
  - brad hards
slug:    well-sucked
---
It seemed like a lot cooler idea when I submitted it. In the end, my talk was an ugly mix of the wrong material for the audience and some bad presentation skills. An accent people weren't really getting it either.

Maybe next year.