---
title:   "Another approach to dumping C++ symbols"
date:    2004-11-23
authors:
  - brad hards
slug:    another-approach-dumping-c-symbols
---
Michael Pyne proposed <a href="http://grammarian.homelinux.net/~mpyne/weblog/kde/tips/c++-syms-in-lib.html">a way to dump C++ symbols from a shared library</a>

I'd suggest something like <tt>nm -C</tt> <i>library name</i>. <tt>man nm</tt> offers lots more.
