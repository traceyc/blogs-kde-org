---
title:   "A fun LUG meeting"
date:    2004-11-26
authors:
  - brad hards
slug:    fun-lug-meeting
---
I went to another great <a href="http://www.clug.org.au">Canberra Linux Users Group</a> meeting last night. Alex Satrapa did a demonstration of setting up Samba with OpenLDAP, OpenSSL, etc.

Of course, doing a Samba demo in front of Andrew Tridgell (who is a fairly regular attendee, and a KDE user) resulted in a few comments, and a short cameo when Tridge explained what is happening with Samba 4. Basically there is going to be a single deaemon, listening on about 10-15 ports. In addition to supporting some of the multiple streams required for the fileserver side, it will have the ability to act as an Active Directory server, and SWAT will be built in (perhaps on the grounds that every server design eventually evolves to include HTTP :-) ). After watching a 2.5 hours setup by someone who'd done it twice before <b>that day</b>, clearly it needs to be easier, and the integrated daemon should help that.

One thing that may end up helping KDE is Tridge's work on LDB, which is essentially the directory part of Samba 4 (replacing OpenLDAP). The API can talk to a local data store (it is TDB), or to a LDAP server (over ldapi or TCP). The local data store is very fast. It might make sense to use LDB for saving our application config data. I'd appreciate thoughts on this (including a pointer to an existing LDAP backend for config data, if any).