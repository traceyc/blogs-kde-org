---
title:   "Openchange resource - SoC update"
date:    2008-07-29
authors:
  - brad hards
slug:    openchange-resource-soc-update
---
Its been a long time (again) between blog entries. The good news is that this time I have something interesting to show: <a href="http://www.mailody.net/">Mailody</a> using <a href="http://pim.kde.org/akonadi/">Akonadi</a> and <a href="http://www.openchange.org">OpenChange</a> to get mail from a Microsoft Exchange server, using native protocols.

Of course, I didn't actually do this myself - all the hard work was done by Alan Alvarez (SoC student for the OpenChange project). I'm just basking in the reflected glory.
<!--break-->

Enough of the talk - bring on the screenies!

First, plain text (click for bigger):
[image:3580]

and for those who would prefer HTML (click for bigger):
[image:3579]
(I'm not sure why the images aren't showing - I think my Qt is built with GIF support - perhaps might be something that Mailody doesn't handle?).

The way this works is that Mailody talks the akonadiserver, which talks to the openchange resource. The talking happens over some DBus stuff, but Akonadi libraries shield you from all that. The openchange resource (which is essentially a full re-write of my old resource) uses a C library (libmapi) via some C++ bindings that Alan wrote as part of his project. libmapi in turn makes use of some samba4 client libraries for some of the hard DCE/RPC protocol stuff.

Unfortunately all that makes this a bit hard to get going on your own right now. You need a really recent version of samba4 (as in, more recent that alpha5, and that means using git; or patching alpha5), a recent version of openchange libraries (svn r676 or later), and pretty recent versions of kdesupport/akonadi, kdepimlibs and kdepim (plus extragear/pim/mailody if you want to see it).

The good news is that packages for Debian are being worked on by Jelmer Vernooij, and Andrew Bartlett and I are trying to get packages for <a href="http://fedoraproject.org/wiki/Features/OpenChange">Fedora</a> sorted out.
I'm more than willing to help anyone else trying to get packaging sorted.

In the mean time, I'm also trying to get the next version of the openchange code finished off so we can work off something released.
