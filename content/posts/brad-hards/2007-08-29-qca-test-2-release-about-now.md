---
title:   "QCA Test 2 release - about now."
date:    2007-08-29
authors:
  - brad hards
slug:    qca-test-2-release-about-now
---
As promised, QCA is getting close to final release.

Justin has <a href="http://lists.affinix.com/pipermail/delta-affinix.com/2007-August/001008.html">just released</a> "test2", which is the "all but final" version of 2.0.0.

We did make some changes from "test1", mostly adding more documentation and a test case or two, but one change identified by Rich Moore made it - a list of all available hashes. The basis for that is so you can possibly show them to the user who makes a selection. That logic also applies to the list of cipher algorithms and MAC algorithms, so we also provide a static method to get the lists of those too.

For those not really familiar with QCA, I've also made up a diagram that tries to explain it:
[image:2963] (click to expand)

Basically, QCA is really just a big <a href="http://en.wikipedia.org/wiki/Bridge_pattern">Bridge</a>. All the heavy lifting in terms of crypto operations is done by the plugins, which are normally implemented as an <a href="http://en.wikipedia.org/wiki/Adapter_pattern">Adapter</a>, but don't need to be (for example, the OpenSSL-based plugin is basically an adapter, but the GnuPG plugin does its work using the GnuPG executable, in machine mode where applicable). 

Except for a couple of built-in capabilities (random number generation, SHA1 and MD5), everything else is plugged in.

Let me know if you have any more questions. If you are particularly keen, you can catch up with me at <a href="http://www.linuxconf.eu">linuxconf.eu</a>.

This is the last chance for any BIC. Speak up!