---
title:   "I deny all knowledge of glib!"
date:    2006-02-24
authors:
  - brad hards
slug:    i-deny-all-knowledge-glib
---
I need to point out that I am the victim of identity theft (or another clee mistake, take your pick). I didn't work on Qt and glib. I think that may have been a Trolltech employee - perhaps Brad Hughes.

I personally don't mind using apps written using glib/gtk. However after some time with Ethereal, I'd rather not have to wash that API off me again. Qt is so much cleaner, and much more enjoyable to work on.

Hmmm, I wonder why the QDevBlog (http://blogs.qtdeveloper.net) aggregator doesn't put the authors' name in the post...