---
title:   "QCA2 beta2 release"
date:    2006-04-07
authors:
  - brad hards
slug:    qca2-beta2-release
---
Justin Karneges has released "beta2" of the Qt Cryptographic Architecture.

You can get the tarballs from http://delta.affinix.com/download/qca/2.0/beta2/ - grab all three tarballs. You can also grab it from KDE's subversion archive.

I'm pretty proud of where we've got to on this - a good design; nice Qt-like API; good API documentation, linked into examples that show how to use the API; and a good start on unit tests. Still needs some more polishing of course, but already useable. There is also a smartcard (PKCS#11) provider about to be released, courtesy of some great work by Alon Bar-Lev.

If you have interest in using cryptographic operations - TLS secure networking, SASL, symmetric ciphers, PGP, CMS or the normal primitives - in Qt code, please take a look at the beta2 release and provide feedback.

The API documentation is built in the tarball, or you can see it online at http://www.frogmouth.net/qca/apidocs/html/index.html (built with a different version of Doxygen, which is why it looks different).

Note that QCA will be binary (and source) compatible after formal release, so now is the right time to bring up issues and concerns! Probably the easiest way is to send me an email, but http://bugs.kde.org (against the qca product) or even a blog comment will probably work.