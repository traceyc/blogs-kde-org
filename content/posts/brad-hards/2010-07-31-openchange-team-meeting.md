---
title:   "OpenChange team meeting"
date:    2010-07-31
authors:
  - brad hards
slug:    openchange-team-meeting
---
The OpenChange team had a short online (IRC) meeting on Friday. The meeting record is at http://tracker.openchange.org/projects/openchange/wiki/Meeting_of_2010-07-30

We're considering holding an "open session" meeting (again on IRC), possibly in a couple of weeks. If you'd be interested in attending, please leave a comment on the best days and times (relative to UTC) so we can accommodate as many people as possible.