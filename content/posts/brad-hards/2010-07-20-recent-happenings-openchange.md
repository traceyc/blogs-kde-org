---
title:   "Recent happenings in OpenChange"
date:    2010-07-20
authors:
  - brad hards
slug:    recent-happenings-openchange
---
I haven't been doing a lot of KDE stuff recently (happy user, although I'd be happier if I could find some extra time for development...). Instead, I've been doing some "real" work, and also going some <a href="http://www.openchange.org">OpenChange</a> work. [For those that tuned in late, OpenChange is an implementation of the Exchange RPC protocols on both the client (i.e. "Outlook") and server (i.e. "Microsoft Exchange") sides of the network protocol]
<!--break-->
Some recent changes:
 - we've arranged some new infrastructure servers, with the help of the Free Software Conservancy and FSF France (especially thanks to Loic Dachary for his work on this)
 - we've migrated from Trac to Redmine, which is working out pretty well so far
 - got the buildbot back up (on http://buildbot.openchange.org:8010 for those who'd like to check it out)
 - significant progress on the server side (including getting outlook to show a message), although there is still a lot to do here
 - better handling of some data types (especially unicode strings)
 - a lot of little code cleanups

Thanks to the Novell (Evolution) team for their work in identifying issues and suggesting fixes, and to the Microsoft Open Specification team for following up on our obscure questions.

Future (near-term) plans mainly focus on server-side functionality, and fixing up some of the current bugs that are biting client users.