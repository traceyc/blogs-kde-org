---
title:   "OpenChange 0.9 - coming soon"
date:    2009-11-26
authors:
  - brad hards
slug:    openchange-09-coming-soon
---
Not really a KDE related post, but instead one about the <a href="http://www.openchange.org">OpenChange</a> project.

OpenChange is a project to implement the Microsoft Exchange / Outlook protocols, and we're creeping up on the 0.9 release. For those not familiar with it, the aim is to be wire-level compatible, so that you can use a FOSS client (such as Evolution or an Akonadi client) with an unmodified Exchange server.  

OpenChange depends on some underlying Samba4 libraries, so we normally work with the Samba project to get releases that basically match up - we don't want to rely on building bits of Samba4 from the git repository, and we don't want to depend on really on versions of Samba4. Not too far ahead, and not too far behind. Just right...

The next release of Openchange (0.9 "COCHRANE") will rely on Samba 4 alpha9. So the main thing to do is to make sure that we can work with the current state of Samba 4, to provide patches to Samba for stuff that needs to be fixed on the Samba side, and to have changes for OpenChange ready to go. 

We expect to release within two weeks of Samba 4 alpha9, but OpenChange 0.9 will be released when it is considered done.

We anticipate a 0.10 release in early 2010, and probably a subsequent 0.11 release prior to 1.0.

I've gone through the trac tickets, and moved anything that can't reasonably be achieved for 0.9 to a 0.10 milestone. Large changes have to wait for 0.10 - it is getting very late to risk destablising libmapi.
So what we still have left:
  - integrate exchange2ical work from Ryan Lepinski into trunk
  - ensure that OpenChange works with FreeBSD/OpenBSD
  - ensure that OpenChange works with OpenSolaris
  - libmapi/Samba4 API updates
  - mapiproxy/Samba4 API updates and association group API evolution
  - a couple of tickets that may not be too hard to fix, related to pkg-config support
  - a ticket related to Free / Busy time support that has a crash.
  - a patch that checks some return values that we current don't handle.
  - some documentation stuff.

Details are on <a href="http://trac.openchange.org/query?status=assigned&status=new&status=accepted&status=reopened&group=status&milestone=libmapi-0.9+COCHRANE">http://trac.openchange.org</a>.

Known problems:
  - openchange development server will not be working "as it is". Resolving this requires revision of the OpenChange schema updates (to match Samba4 changes).  This may not be resolved for 0.9.

If I've dropped a patch, or there is something you really need for 0.9, now is your chance to make yourself known. Probability of a patch being applied is inversely proportional to complexity, and also inversely proportional to how close I think the release is. That is, simple and soon is good.

