---
title:   "Conference fu"
date:    2004-08-08
authors:
  - brad hards
slug:    conference-fu
---
I've been working on my presentation for <a href="http://conference2004.kde.org">aKademy</a>, and it looked a lot easier before someone posted a link to <a href="http://www.perl.com/pub/a/2004/07/30/lightningtalk.html">a guide about lightning talks</a> to the <a href="http://lca2005.linux.org.au">linux.conf.au 2005</a> organisers list. That lead me to another guide, called <a href="http://perl.plover.com/yak/presentation/">Conference Presentation Judo</a>. At that stage I realised I was spending too much time talking about what meta-data is, and why you should have some. That is particularly bad news considering that Scott Wheeler is probably going to spend most of his <a href="http://conference2004.kde.org/cfp-devconf/scott.wheeler-search.metadata.interface.elements.php"> talk about metadata</a> explaining that.

The other thing I've been working on is an outline program for <a href="http://lca2005.linux.org.au">linux.conf.au 2005</a>. Talks will be 45 minutes, and will start about every hour. The tough part is about how late to run, when to hold the lightning talks, and how to make sure that people get enough to eat and drink. Would you like a conference that had talks from 0900 to 2000? I'm hoping to get some more insight at aKademy and Linux Kongress.