---
title:   "ack, a better grep"
date:    2009-01-25
authors:
  - brad hards
slug:    ack-better-grep
---
One of the many things I learned at linux.conf.au was about <a href="http://petdance.com/ack/">ack</a>. Thanks to Paul Fenwick for bringing it to our attention. Spend a few minutes with me on this...

If you are a fan of grep (especially with -drecurse), but hate seeing the .svn directories and contents of the object files, you need ack.

Colourised outputs, highlighting, nice organisation, awesome configurability are all bonuses. Even has documentation.

Packages exist for at least Debian and Fedora. Portable (perl), installable using CPAN.

Less to type. What more could you ask for?


Example (without colour, sorry):
<code>
bradh-dev@saxicola:~/kde/src/kdesupport/akonadi$ ack AkonadiServer
server/src/akonadi.cpp
53:static AkonadiServer *s_instance = 0;
55:AkonadiServer::AkonadiServer( QObject* parent )
130:AkonadiServer::~AkonadiServer()
134:void AkonadiServer::quit()
187:void AkonadiServer::doQuit()
192:void AkonadiServer::incomingConnection( quintptr socketDescriptor )
201:AkonadiServer * AkonadiServer::instance()
204:        s_instance = new AkonadiServer();
208:void AkonadiServer::startDatabaseProcess()
338:void AkonadiServer::stopDatabaseProcess()
346:void AkonadiServer::serviceOwnerChanged(const QString & name, const QString & oldOwner, const QString & newOwner)

server/src/main.cpp
40:  qDebug( "Shutting down AkonadiServer..." );
42:  Akonadi::AkonadiServer::instance()->quit();
67:    Akonadi::AkonadiServer::instance(); // trigger singleton creation
75:    Akonadi::AkonadiServer::instance()->quit();

server/src/akonadi.h
39:class AKONADIPRIVATE_EXPORT AkonadiServer: public QLocalServer
44:    static AkonadiServer* instance();
46:    AkonadiServer( QObject *parent = 0 );
47:    ~AkonadiServer();
</code>