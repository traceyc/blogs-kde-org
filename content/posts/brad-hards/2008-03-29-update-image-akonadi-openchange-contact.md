---
title:   "Update - Image in Akonadi OpenChange contact"
date:    2008-03-29
authors:
  - brad hards
slug:    update-image-akonadi-openchange-contact
---
In my last blog, the screenshot showed a broken image.

Thanks to some very fast work by Tobias König (tokoe), it now works. That picture of Konqi was uploaded to the server using Outlook, and downloaded using OpenChange and Akonadi, before being rendered. 

[Image:3361]
(again, click to expand).

Thanks again to tokoe.