---
title:   "Update, and some SoC thoughts"
date:    2008-03-23
authors:
  - brad hards
slug:    update-and-some-soc-thoughts
---
Haven't blogged for a while, mainly because I didn't have much worth saying. I still don't have a lot worth saying, but I'll blog anyway.

I recently became part of the <a href="http://www.openchange.org">OpenChange</a> team. For those not familiar with it, the OpenChange project is developing a client and server implementation of the "MAPI" protocol (which is really Exchange RPC - MAPI is the API you use to access the transport) used by Microsoft Outlook and Microsoft Exchange. OpenChange is the key to implementing an Akonadi resource that can work with Microsoft Exchange.

I've put myself down as a mentor for both KDE (<a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas">Ideas</a>) and OpenChange (<a href="http://wiki.openchange.org/index.php/SoC/Ideas">Ideas</a>) Summer of Code projects. If you are considering applying as a student or mentor for SoC, I encourage you to apply soon (student applications open on Monday at 1900UTC - which is tomorrow for at least some people; and you only have a week to apply).

There are a lot of interesting ideas on other SoC Project lists, some of which could also be interesting for KDE given an appropriate application focus or integration (samples:
<a href="http://wiki.panotools.org/SoC_2008_ideas">Hugin/Panotools</a>, <a href="http://openinkpot.org/wiki/GSoC2008Ideas">OpenInkPot</a>, <a href="http://wiki.openstreetmap.org/index.php/Google_Summer_of_Code#Pool_of_Potential_Projects">OpenStreetMap</a>, <a href="http://wiki.osgeo.org/wiki/Google_Summer_of_Code_2008_Ideas">OpenGeo</a>, <a href="http://chess.eecs.berkeley.edu/ptides/wiki/">UCB's Ptolemy project</a>, <a href="http://wiki.samba.org/index.php/SoC/Ideas">Samba</a>, <a href="http://wiki.jabber.org/index.php/Summer_of_Code_2008">Jabber</a>) or are enablers for many projects (for example: <a href="http://llvm.org/OpenProjects.html">LLVM</a> and <a href="http://gcc.gnu.org/wiki/SummerOfCode">gcc</a> compilers, <a href="http://www.freedesktop.org/wiki/OpenIccForGoogleSoC2008">OpenICC</a>, <a href="http://www.mediawiki.org/wiki/Summer_of_Code_2008#Projects.21">MediaWiki</a>, <a href="http://subversion.tigris.org/project_tasks.html#summer-of-code">Subversion</a>, <a href="http://trac.webkit.org/projects/webkit/wiki/Google Summer of Code 2008">WebKit</a>, and <a href="http://wiki.xiph.org/index.php/Summer_of_Code">Xiph</a>). Your search of the <a href="http://code.google.com/soc/2008/">Main SoC project list</a> will undoubtedly turn up more.

There are a range of other projects that I haven't seen on the KDE ideas page that could be worth a submission if you had the right interest / ability. In particular, I'd like to see some proposals on accessibility tools  and virtualisation.