---
title:   "KDE developer training needs?"
date:    2005-02-24
authors:
  - brad hards
slug:    kde-developer-training-needs
---
Adriaan has <a href="http://people.fruitsalad.org/adridg/frond/archives/000784.html">posted some ideas for a "UofKDE"</a>. I had been thinking along the same lines for developers, only without the first year stuff (which I guess I assumed that people already had by the time they started thinking about becoming a KDE developer). My list is below - I'm currently working on the Doxygen stuff, and some of the other material already exists, although some of the stuff we do have needs to be re-arranged or updated.

I've also looked at course management tools, such as <a href="http://www.moodle.org">Moodle</a>, and I think that moodle.kde.org would probably work quite well. As pointed out by others, the key issues are who is going to write all this stuff, and whether we have enough interest from current developers that they are willing to act as tutors/guides/helpers for an on-line course.

In the longer term, we could have self-paced user training as well, but I don't see enough motivated people to support that yet. Similar infrastructure would probably be appropriate in any case.

Anyway, things a KDE developer must, should or might want to know:

Basics of windowing - modal dialogs, SDI, MDI, window elements

Introduction to programming with Qt Classes

The KDE widget set

KDE convenience classes

KDE build system (compare with Qt build system?), writing autoconf
tests and using automake

Introduction to XML

Using CVS and Subversion

programming for Internationalisation and Localisation, KLocale

KGlobal

Writing user documentation - docbook, author guide - maybe just an outreference to a real documenter training course?

Writing programmer documentation - Doxygen, rough style guides

i18n Translation guide(s)?

shared libraries and binary compatibility

debugging concepts and tools

using and updating kde-config

.desktop files

kdeinit, kded, KTrader, kbuildsycoca?

KWin, Kicker

writing command line applications (KCmdLineArgs and the like)?

starting other applications (KProcess, KProcIO, startServiceByDesktopPath, KRun)?

programming for security

understanding / programming for usability

understanding / programming for accessability

programming for kiosk

Configuration file support? (multipart?)

kdesu / libkdesu

Writing a KIO slave

Using KIO (multipart?)

writing a KPart application - maybe this should be "writing a KDE GUI application", with just a bit on the side about KParts

XMLGUI (multipart)

using KABC

using KResources

using kdefx

implementing a KResources plugin

Using KFind / KReplace

Using KNewStuff

using KTextEditor

using DNSSD

using KHTML

using KPrinter

using KNetwork support

Writing a KfileMetaInfo plugin

Writing a thumnail plugin

Adding a DCOP interface to an application

Developing a plugin architecture

Writing decoration plugins for KWin

Writing a Panel applet

writing KImgIO plugins

KConfig Modules

using kdialog (the command line app, not the class) in shell scripts

using kfmclient in scripts

DCOP CLI and KDCOP

other language bindings

FAQ?

Developers checklist, commit guidelines


If you got this far, anything else? Anything wrong? And the key question - are you willing to help make this happen?