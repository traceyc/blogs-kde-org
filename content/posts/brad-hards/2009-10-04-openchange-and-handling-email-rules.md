---
title:   "OpenChange, and handling email rules"
date:    2009-10-04
authors:
  - brad hards
slug:    openchange-and-handling-email-rules
---
Once again, its been a long time since I blogged. I have been doing a bit of OpenChange development though.

Mostly its been minor bug fixes, cleanups and so on. This weekend I decided to take on something a bit more substantial. Email rules handling between Microsoft Outlook and Microsoft Exchange are pretty sophisticated, and OpenChange didn't do too much of it. I ended up having to get all the way down to the unmarshalling data structures from the RPC calls to understand why things didn't work. Its starting to come together now, with the condition part of the rule mostly under control (although not complete) and the actions part of the rule hopefully not too hard once I get conditions sorted.
<!--break-->
I'm a long way from done, but the current diffstat:
<pre>
 exchange.idl             |   97 ++++++-----
 libmapi/conf/mparse.pl   |    4
 libmapi/emsmdb.c         |   10 +
 libmapi/mapidump.c       |  384 +++++++++++++++++++++++++++++++++++++++++++++++
 libmapi/property.c       |  152 ++++++++++++++++++
 utils/openchangeclient.c |   84 ++++++++++
 6 files changed, 686 insertions(+), 45 deletions(-)
</pre>

Most of the IDL changes are just reorganising the order of things. The hard bits were in libmapi/property.c.

OpenChange is a library, so there aren't nice GUI screenshots. However I can show the results of a new openchangeclient operation, to list the rules on a mailbox folder:
<pre>
bradh-dev@saxicola:~/openchange/oxorule$ ./bin/openchangeclient --ruleslist
[10] "which is an Out of Office message" (0xE1FCF59014000001)              
        rule provider: RuleOrganizer                                       
        state: 0x00000011 ( ST_ENABLED ST_EXIT_LEVEL )                     
        All of the following are true:                                     
                Content of PR_MESSAGE_CLASS_UNICODE matches (case insensitive) IPM.Note.Rules.OofTemplate.Microsoft                                                                                                                 
                All of the following are true:                                                                    
                        PR_MESSAGE_SIZE is greater than 12288                                                     
                        PR_MESSAGE_SIZE is less than 20480                                                        

[11] "where my name is not in the To box" (0xE2FCF59014000001)
        rule provider: RuleOrganizer                          
        state: 0x00000001 ( ST_ENABLED )                      
        All of the following are true:                        
                PR_MESSAGE_TO_ME is equal to false            
                Comments contain 4 tagged values:Unknown tag: 0x60000003 (1), Unknown tag: 0x00010102 (<<binary>>), Unknown tag: 0x0001001f (priority@example.com), PR_DISPLAY_TYPE (0),  Restriction present:                      
                        PR_SENDER_SEARCH_KEY is equal to <<binary>>                                               
                All of the following are true:                                                                    
                        PR_FLAG_STATUS is equal to 2                                                              
                        Unknown tag: 0x802a001f is equal to Call                                                  

[12] "test user1 or foo@example.com" (0xE3FCF59014000001)
        rule provider: RuleOrganizer2                    
        state: 0x00000001 ( ST_ENABLED )                 
        Any of the following are true:                   
                Comments contain 4 tagged values:Unknown tag: 0x60000003 (1), Unknown tag: 0x00010102 (<<binary>>), Unknown tag: 0x0001001f (test user1), PR_DISPLAY_TYPE (0),  Restriction present:                                
                        PR_SENDER_SEARCH_KEY is equal to <<binary>>                                               
                Comments contain 4 tagged values:Unknown tag: 0x60000003 (1), Unknown tag: 0x00010102 (<<binary>>), Unknown tag: 0x0001001f (foo@example.com), PR_DISPLAY_TYPE (0),  Restriction present:                           
                        PR_SENDER_SEARCH_KEY is equal to <<binary>>                                               

[13] "'Old project' or 'Really old project'" (0xE0FCF59014000001)
        rule provider: RuleOrganizer
        state: 0x00000011 ( ST_ENABLED ST_EXIT_LEVEL )
        Any of the following are true:
                Content of PR_SUBJECT_UNICODE contains (case insensitive) Old project
                Content of PR_SUBJECT_UNICODE contains (case insensitive) Really old project

[14] "Project X" (0x15B3492E12000001)
        rule provider: RuleOrganizer
        state: 0x00000001 ( ST_ENABLED )
        Content of PR_SUBJECT_UNICODE contains (case insensitive) Project X

[15] "'Viagra' or 'herbal'" (0x14B3492E12000001)
        rule provider: RuleOrganizer
        state: 0x00000011 ( ST_ENABLED ST_EXIT_LEVEL )
        Any of the following are true:
                Content of PR_SUBJECT_UNICODE contains (case insensitive) Viagra
                Content of PR_SUBJECT_UNICODE contains (case insensitive) herbal

[16] "Administrator" (0x16B3492E12000001)
        rule provider: RuleOrganizer
        state: 0x00000001 ( ST_ENABLED )
        All of the following are true:
                Comments contain 4 tagged values:Unknown tag: 0x60000003 (1), Unknown tag: 0x00010102 (<<binary>>), Unknown tag: 0x0001001f (Administrator), PR_DISPLAY_TYPE (0),  Restriction present:
                        PR_SENDER_SEARCH_KEY is equal to <<binary>>
                PR_IMPORTANCE is equal to 2
</pre>

That mostly converts the data structure into something intelligible (although there are a few things that I'm not able to sensibly represent in natural language yet, as you can see). There are other variations within the outlook rules, and my code cannot yet do them all. It isn't too hard to complete, but I'm trying to make sure I have test cases for each case before implementing. If you have a really sophisticated set of rules within outlook and would like to share them, feel free to export them and mail it to me.