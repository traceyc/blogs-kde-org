---
title:   "developer.kde.org work"
date:    2006-06-10
authors:
  - brad hards
slug:    developerkdeorg-work
---
I've been giving some of developer.kde.org a bit of love. In particular, the "Joining Us" page (http://developer.kde.org/joining/) is now hopefully a bit more useful on how to get involved in various aspects of KDE development - not just coding, but all aspects. There is a lot more to do - hoperfully I'll get some more done over this weekend.

Anyway, if you have any additional content, or notice anything wrong, please let me know. 