---
title:   "We should carefully examine specifications and code on freedesktop.org"
date:    2004-01-29
authors:
  - brad hards
slug:    we-should-carefully-examine-specifications-and-code-freedesktoporg
---
freedesktop.org is a website. Really! It contains code and documentation. We could use some of that for KDE, if it suits us. But KDE is a volunteer effort, and we don't have to read any particular website, nor use any particular code, nor implement any particular specifications.<br><br>

The specs are at <a href="http://www.freedesktop.org/Standards/Home">http://www.freedesktop.org/Standards/Home</a> and include:
<li>Xdnd
<li>X clipboard
<li>XEmbed
<li>System Tray
<li>Mimetypes
<br><br>

The code is at <a href="http://www.freedesktop.org/Software/Home">http://www.freedesktop.org/Software/Home</a>.
It includes stuff like:
<li>DRI for X
<li>cairo 2D renderer
<li>D-BUS message passing system
<br><br>

Essentially its a hoster, more focussed that Sourceforge, but not that different in some ways.
<br><br>

I think that Ian mistook the some of the implementations (eg. D-BUS, which I think avoided a glib dependency by importing bits of glib :) <b>Update: I was wrong  :-(. It doesn't do any such thing.</b>) with the protocols or behaviours that they implement. We generally want a good, stable, interoperable implementation. So do the Gnome guys - at least they claim to, despite their selection of tools. The difference is that we want it in C++ and Qt.
<br><br>

There is nothing that I could see that requires use of Glib in any of the specs that are likely to be important. Sure, we could choose to not re-implement some of the reference implementations (and live with any stability or performance issues), but that isn't a function of the spec, it is about the reference implementations. Would it really take 25K lines of Qt/KDE/C++ code to implement a D-BUS client? I'd hope not, but that is how much C is claimed to be in libdbus.
<br><br>

Now the choice is about what we want to work on. I wish you well in your choices, and remind you that just cause you saw it on the web, it may not be a good idea.
<br><br>

Oh yeah, I'm not sure why Rants is so close to the bottom of the kdedeveloper blog category list. Seems to be what I (and more than a couple of other people around here:)) use.

