---
title:   "linux.conf.au - you know you should...."
date:    2007-11-13
authors:
  - brad hards
slug:    linuxconfau-you-know-you-should
---
<a href="http://linux.conf.au">linux.conf.au</a> early-bird registrations close soon. Now would be an excellent time to register if you want to come along. I'm booked in...

There are a lot of cool looking talks - I'm personally planning to heckle at <a href="http://linux.conf.au/programme/detail?TalkID=296">this talk</a> :-)

This is really a great conference, and offers a great experience and opportunity to collaborate. I'm also looking to organise a KDE get-together (probably a lunch on the Saturday - we'll see how it looks closer to the event).