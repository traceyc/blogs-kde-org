---
title:   "Qt bindings for libusb"
date:    2003-12-31
authors:
  - brad hards
slug:    qt-bindings-libusb
---
I've finally completed the C++ bindings for libusb - what a fun way to spend New Years Eve.

This implementation is layered on top of the C routines - no changes to any of the existing code, so it should be portable to any platform supported by libusb. It relies on Qt - I looked hard at the STL stuff, but Qt was much better, especially for string handling (yep - USB devices return strings in Unicode). I decided to stay with QPtrList for now, despite being told it wasn't likely to last long - it was a better match for my conceptual design.

I'm trying to get it incorporated into libusb, but distributing it as part of kdelibs is a backup plan.

If Qt isn't available at build time, only the C stuff is done - it automatically detects and adjusts. It only errors out if you specifically ask for Qt support (with --with-qt=yes) and it isn't available. I think that the two hardest parts were getting the autoconf/automake parts right, and remembering how to do strings when you've only got a miserable char*.

The API is documented in Doxygen - its up at http://www.frogmouth.net/libusb-apidox/annotated.html

I'm not sure why it looks so horrible when rendered.

The patch includes test code that exercises most of the C++ code. This was the first time I'd tried some of the Test Driven Development ideas. Seems to work well - it is very powerful knowing that "make check" is covering my backside when I make changes late at night. The test harness handles Qt bindings being present/absent, and also for tests that only work on Linux. I hope so, anyway.

This isn't supposed to be the final design / implementation - I'm putting this one out for suggestions and testing. I can now get onto the whole point of the exercise though - support for the vendor specific features in Logitech mice.