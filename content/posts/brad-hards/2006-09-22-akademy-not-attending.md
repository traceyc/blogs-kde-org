---
title:   "akademy - not attending"
date:    2006-09-22
authors:
  - brad hards
slug:    akademy-not-attending
---
I really would have liked to have been in Dublin this weekend, but unfortunately I am much too heavily committed at work. I definitely plan to be at akademy 2007, and also at http://linux.conf.au in January 2007. So I hope to catch up with some of you then.

Work, and the commensurate tiredness has also slowed down progress on QCA, progress on http://www.kdelabs.net, and the XML Paper Specification work. I have done a little bit of XPS implementation in okular (so it can render text using the wrong font - there appears to be a Qt bug that prevents loading application fonts).

Anyway, have a great time in Dublin, get way inspired, and feel free to hack on QCA or XPS anytime!