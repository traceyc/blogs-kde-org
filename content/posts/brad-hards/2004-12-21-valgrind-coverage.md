---
title:   "Valgrind coverage?"
date:    2004-12-21
authors:
  - brad hards
slug:    valgrind-coverage
---
Michael Ellerman has made a recent blog post about a coverage tool for valgrind (http://michael.ellerman.id.au/index.cgi/2004/12/19#valgrind). I've been looking for that for a while, hope he gets it released, since that would help with tricky coverage issues like our shared library plugins, such as that used in the Qt Cryptographic Architecture (QCA) that I've been working on, albeit a bit on-n-off recently. Hopefully I should be able to get a bit more done in the Christmas break.

Also, it seems like Qt4 Beta 1 is getting closer.

