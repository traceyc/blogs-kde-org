---
title:   "Openchange goes to Redmond, and a shout out to Inverse / SoGo"
date:    2010-09-28
authors:
  - brad hards
slug:    openchange-goes-redmond-and-shout-out-inverse-sogo
---
Julien and I met up in Redmond last week, just before the Exchange Open Specifications event at Microsoft. It was a productive time, where we did some serious planning and a little coding, and learned quite a lot more about the protocols from some of the main developers of Exchange and Outlook. Thanks to Microsoft for hosting it.

I also wanted to highlight some impressive work from the people at <a href="http://www.inverse.ca/english.html">Inverse</a> (in particular, Wolfgang Sourdeau) in building a backend for the <a href="http://www.sogo.nu/english.html">SoGo groupware suite</a> that uses OpenChange to provide native Outlook connectivity (using Exchange RPC) to the SoGo server. There is a <a href="http://inverse.ca/downloads/tmp/openchange.mov">screencast video</a> that shows access to the SoGo server via Outlook and Firefox (web UI). The video goes by quite quickly, but you can see new folder creation, messages moved between folders and creation of a new contact.
<!--break-->
There is still a way to go, but this is a very promising start. It also gives us confidence in the OpenChange server architecture, and shows where we need to focus our attention.