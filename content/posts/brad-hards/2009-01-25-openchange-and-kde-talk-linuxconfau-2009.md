---
title:   "OpenChange and KDE talk (linux.conf.au 2009)"
date:    2009-01-25
authors:
  - brad hards
slug:    openchange-and-kde-talk-linuxconfau-2009
---
On Friday, I gave my talk at <a href="http://linux.conf.au">linux.conf.au</a> 2009. 

I'm sure the slides (and the recordings will be up on the conference web site at some point), but you can get them from my site in <a href="http://www.frogmouth.net/lca2009-hards.odp">ODP</a> and <a href="http://www.frogmouth.net/lca2009-hards.pdf">PDF</a> versions.

Thanks to all those who came along - much appreciated.

