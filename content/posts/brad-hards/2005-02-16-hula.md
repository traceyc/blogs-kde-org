---
title:   "Hula"
date:    2005-02-16
authors:
  - brad hards
slug:    hula
---
Well, a few people have commented on Hula. I thought I might give it a try before commenting. 

So I downloaded it from the SVN repo. It uses autoconf and automake (yes there are some dependencies, but autoconf didn't complain so I didn't look that closely). Starts to build OK, but there are a few warnings about implicit declarations and using kernel headers. Eventually it gets to the linking part, and dies. 

I don't think my patch is going into the codebase though - I have no intention of signing over my Copyright to Novell unless they are paying me, and the Hula wiki says no input without Copyright assignment. Whatever.

OK, so it wants to use Linux kernel headers to compile some otherwise apparently portable code. I ported the atomic operators to glib (and then went to take a shower to wash off the grime from grovelling around in glib-2.x), fixed another place where it isn't even using its own abstraction layer consistently, and eventually got it to compile, link and install.

The setup proggie (hulasetup) works OK as root - segfaults if you try to run it as a normal user though.

The main startup code (hulamanager) doesn't work for me though. Looks like something is missing from my build, or maybe my checkout.

Summary: for KDE, it is probably just another POP/IMAP server, maybe some calendaring stuff that might go somewhere, but the port is way incomplete, and there is a long, long way to go.