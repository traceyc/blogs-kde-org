---
title:   "Another opinion on build system support"
date:    2005-06-15
authors:
  - brad hards
slug:    another-opinion-build-system-support
---
I've got another suggestion. The best language to write the "./configure" tests in is one that developers are familiar with. I'm thinking C++.

We can write the tests in C++, as long as they don't need any special libs, which seems likely. If you can compile with qmake, then that is enough. If qmake doesn't work, then the build system is terminally broken in any case.

The use of C++ with Qt can provide good string handling - probably up there with scripting languages.

C++ can provide nice functionality with inheritance - making tests easy to write, and consistent.

Here is an example of how it might work, based on a real check for libgcrypt.

<code>
#include <qprocess.h>
#include <qstringlist.h>
/*
-----BEGIN QCMOD-----
name: libgcrypt
arg: gcrypt-config=[path],Path to the libgcrypt configure script
-----END QCMOD-----
*/

class qc_libgcrypt : public ConfObj
{
public:
        qc_libgcrypt(Conf *c) : ConfObj(c) {}
        QString name() const { return "libgcrypt"; }
        QString shortname() const { return "libgcrypt"; }

        bool exec()
        {
                QString path = conf->getenv("QC_GCRYPT_CONFIG");
                if(path.isEmpty())
                        path = "libgcrypt-config";

                QStringList incs;
                QString ver, libs, other;
                if(!conf->findFooConfig(path, &ver, &incs, &libs, &other))
                        return false;

                for(int n = 0; n < incs.count(); ++n)
                        conf->addIncludePath(incs[n]);
                if(!libs.isEmpty())
                        conf->addLib(libs);
                if(!other.isEmpty())
                        conf->addExtra(QString("QMAKE_CFLAGS += %1\n").arg(other));
                return true;
        }
};
</>

That is from qconf BTW. Now I'm not suggesting qconf is the right answer, but it might serve as a model, and perhaps even as a starting point for development.