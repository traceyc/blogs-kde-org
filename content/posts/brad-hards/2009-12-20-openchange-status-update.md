---
title:   "OpenChange status update"
date:    2009-12-20
authors:
  - brad hards
slug:    openchange-status-update
---
I've been working on the next OpenChange release (0.9), and it is getting quite close.

So where are we up to:
<ul>
<li>Merge of Ryan Lepinski's Summer of Code project (on converting Exchange calendars to the ICal format) is done.</li>
<li>Did some more testing with Exchange 2010.</li>
<li>Julien Kerihuel added support for encrypted connections, which are required in a default install of Exchange 2010.</li>
<li>The server provisioning works again. (Note: Server is pre-alpha. This is a developer preview only, not intended for any kind of other use.)</li>
<li>More documentation tweaks.</li>
</ul>

I'm pretty happy with where we're going on this release. It has been a bit slower than we'd expected, but there are also some useful fixes that are about to go in, especially one related to how recipients are handled, which is a nasty problem for many users.

Unfortunately I've introduced some regressions with the merge of the Exchange / Ical stuff, including failure to build on FreeBSD 7.2 (although FreeBSD 8.0 is fine) and a case where some kinds of appointments cause crashes. The problem on FreeBSD 7.2 relates to use of the "timezone" variable (in time.h). We also still have quite a few problems with Exchange 2010, although some of those are places where Microsoft changed the server behaviour, so the underlying Exchange RPC method simply can't work. However the problems shouldn't take too much more work to knock into shape, and the buildbot setup is good for catching some of those problems.

I've also grabbed a slot for a BoF (Birds of a Feather - informal get together) session at <a href="http://www.lca2010.org.nz/">linux.conf.au 2010</a>. You can find the <a href="http://www.lca2010.org.nz/wiki/OpenChangeBoF">details of the BoF on the wiki</a> (Thanks to Andrew for pointing out that I originally had grabbed the slot during the closing ceremony, and sorry for anyone that can't make that time - unfortunately with so many great presentations, there isn't a "good" time).

The OpenChange project is also actively looking for potential sponsors for hosting (servers, connectivity, or both) and for more people to get involved in the project. Here is the <a href="http://mailman.openchange.org/pipermail/devel/2009-December/001408.html">mailing list post</a> that Julien made:
<pre>
Background:

      * OpenChange has:
        
              * main website: http://www.openchange.org
              * wiki: http://wiki.openchange.org
              * ticket management system: http://trac.openchange.org
              * websvn: http://websvn.openchange.org
        
        
      * The main website has not been updated since April 2009. This
        unfortunately demonstrates that the core OpenChange team was not
        able to find the time requested to manage the website properly
        or add entries on a regular basis.



      * Our main server starts to show some bandwidth and resources
        limitations due to the large number of visitors.
        
        

Recruitment:

The OpenChange project is looking for new team members and contributors
who would endorse:


              * Community Manager role:
                
                      * manage the overall website
                      * coordinate the editing effort
                      * review the blog entries (technically and
                        grammatically)
                      * be actively involved in OpenChange project life
                        (follow development, related projects etc.)
                      * be actively participating and contributing to
                        the list
                        
                        
                        
              * Regular Contributors roles:
                
                      * contribute to the OpenChange website
                      * submit blog entries such as:
                        
                              * experience with OpenChange code
                              * tricks and tips
                              * code samples / snipset
                              * desktop recording / screencast
                              * technical documentation
                              * final-user documentation
        
        

Services Providers:

Infrastructure is important for the project to grow from a "sourceforge"
size project to a major project. We have some issues that are probably
common with many projects, and some unique things that we'd
like.                        
The key things are that we have a supportable, sustainable
infrastructure that allows us to get the project done. 

        
        
              * Candidates for services should provide:
                      * high bandwidth (100Mbits)            
                      * high availability                    
                      * access for openchange team members to
                        configure / update as required
                
                
              * Rough indications of bandwidth usage (websvn, svn
                checkouts, website usage) to potential providers:
                
                      * minimum is 52Gb and max is 87Gb per month
                      * "roughly 60/70Gb month"
                      * Further details can be provided on request
                
                
              * List of services and servers for the upcoming
                infrastructure:
                
                      * Server 1:
                        
                              * Trac
                              * SVN read-only mirror (primary repository
                                for non-team members)
                              * WebSVN
                                
                      * Server 2:
                        
                              * Main Website
                              * Wiki
                                
                      * Server 3:
                        
                              * Main Website mirror
                              * Wiki mirror
</pre>

The lack of reliable services has held up development on a few occasions, and we really don't want that to continue.

Also, like most projects, we're always looking for more contributors. So if those descriptions don't sit well with you, but you have another interest, please get in contact with us. IRC can be a useful place to get started - we're usually in #openchange on FreeNode.
