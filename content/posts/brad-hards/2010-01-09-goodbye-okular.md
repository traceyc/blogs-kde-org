---
title:   "Goodbye Okular"
date:    2010-01-09
authors:
  - brad hards
slug:    goodbye-okular
---
The <a href="http://okular.kde.org/team.php">Okular</a> team has never been all that big. Recently we <a href="http://www.mail-archive.com/okular-devel@kde.org/msg04862.html">lost Pino as the maintainer</a>. His reasons are his reasons, but I can't say I blame him. I can personally no longer tolerate the level of abuse that we're seeing on bug reports. The latest example is <a href="https://bugs.kde.org/show_bug.cgi?id=157284">Wishlist item 157284</a>

I'm unsubscribed from the okular-devel mailing list. I'm not going to be in #okular. I'll still look at XPS bugs if I notice them.

It is difficult to leave. I really do care about Okular - I gave quite a lot of my time to improve it. It is a really nice application. However every time I looked at some bug comment insisting on a change (in something like the GUI that is subjective in the absence of actual usability study), I get disheartened. Even when I'm supposed to be working on something else, it gets to me. I've got better things to work on, where I'm not seeing that level of criticism of volunteer efforts.

I'd like to thank everyone involved in Okular, especially Albert and Pino for their maintainership, Piotr for the initial work, and Tobias for some inspirational work on the generator side.

Best wishes to Okular and its happy users.