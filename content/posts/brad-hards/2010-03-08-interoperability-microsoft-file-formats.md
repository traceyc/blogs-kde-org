---
title:   "Interoperability with Microsoft File Formats."
date:    2010-03-08
authors:
  - brad hards
slug:    interoperability-microsoft-file-formats
---
I recently realised that much of the code I find interesting is about interoperability. That is, I'm interested in making sure we can get at data in a range of formats. Work on libtiff, poppler, okular generators and openchange are all examples of that. I also like Qt as a very nice cross-platform API. The convergence of those interests is having Qt-style libraries and tools that can get access to data, especially data in widely used proprietary formats (e.g. those produced by Microsoft products).

I've set up a gitorious repository (<a href="http://gitorious.org/microsoft-qt-interop/microsoft-qt-interop">http://gitorious.org/microsoft-qt-interop/microsoft-qt-interop</a>) for some of that stuff.
<!--break-->
At the moment, it mainly has a Compound File Binary Format (aka "OLE") parser, written from the MS-CFB specification.

I plan to add an EMF ("Enhanced Metafile") format parser / renderer (already written and currently used in KOffice) at some point too - just need to find some more time.

There are a lot more things that could go in there (e.g. converting the various things in MS-DTYP into Qt equivalents), but I've only implemented those things I actually need.

Contributions are welcome - I'm pretty flexible on format. If you have some suggestions, please add them to the project wiki on gitorious.