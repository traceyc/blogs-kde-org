---
title:   "Were-rabbits and unit tests"
date:    2005-11-13
authors:
  - brad hards
slug:    were-rabbits-and-unit-tests
---
Apparently there was a kangaroo in our back yard this morning. Given that just after that we saw a (probably domestic) rabbit, I wonder it it might have been a Were-Rabbit, rather than a 'roo.

In other news, I've been working on some documentation on writing unit tests. If you are interested, there is an initial checkin at http://developer.kde.org/documentation/tutorials/writingunittests/writingunittests.html for your reviewing pleasure. Please don't edit the HTML though - only the SGML/Docbook source.

Other stuff you might be interested in:
  - At our local LUG, Andrew Tridgell talked about a new version of the SMB protocol that Microsoft appears to be putting into Vista. Some interesting features (eg transactions) and some interesting bugs (eg you can crash any Vista beta box over the network :-) )
  - The buildbot is getting a bit more stable. I hope to have the IRC bot and RSS feeds done in the next week or two. Some more targets are also planned.

