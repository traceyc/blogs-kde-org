---
title:   "Encryption in KOffice"
date:    2004-08-02
authors:
  - brad hards
slug:    encryption-koffice
---
I'm putting off writing my presentation for <a href="/conference2004.kde.org">aKademy</a>, and working on a really old KOffice wishlist item - bug:24399, which hopes for encryption. The move to the new OASIS format (aka the OpenOffice format) provides the requirements.

All I have to do is take a SHA1 hash of the password (producing 20 bytes), feed that with a salt value into an RFC2898 (PKCS#5, password based key distribution function version 2) method, and then feed the output key and the initialisation vector into a blowfish cipher in CFB mode.

Unfortunately I know next to nothing about crypto, and the only Qt/KDE friendly crypto library (qca, with qca-tls) that I've found doesn't have a RFC2898 implementation. So I have to write one (or actually adapt the one in O'Reilly's <a href="http://www.secureprogramming.com/">Secure Programming Cookbook</a>. I don't have test vectors - I'm not even sure that the OASIS format uses HMAC-SHA-1 in the RFC2898 function. I'm clearly going to have to grovel through more OpenOffice code.

I am really looking forward to the KDE conference though, along with a long-sought after holiday in Europe. I guess 20 hours of flying isn't really all that much to get there...