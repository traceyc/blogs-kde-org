---
title:   "Amazing \"Distributed\" Summer of Code in KDevelop"
date:    2008-08-30
authors:
  - powerfox
slug:    amazing-distributed-summer-code-kdevelop
---
Hi! My name is Evgeniy Ivanov and I was a GSoC student mentored by Alexander Dymo. 
I've developed DVCS support in KDevelop.
It's a small preview of working things I've implemented this summer (some things need some love and I will give it).
<br>
This summer was amazing: our team includes very interesting, intelligent and kind people. I wasn't able to know all of them during one summer, but Alexander, Andreas, Amilcar, David, Vladimir, Manuel, all other guys impressed me very much. When I knew most of them have PhD I decided I want it too (but I have to study 3-3.5 years before getting Master degree :D).  

Before I start, the sweetest thing (currently only for Git and with few minor glitches):
<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://1.bp.blogspot.com/_wb8_4jUpYW4/SKtAvgKZOiI/AAAAAAAAAEQ/bdYoMrG061k/s1600-h/revHistory_Beta1.jpg"><img style="display:block; margin:0px auto 10px; text-align:center;cursor:pointer; cursor:hand;" src="http://1.bp.blogspot.com/_wb8_4jUpYW4/SKtAvgKZOiI/AAAAAAAAAEQ/bdYoMrG061k/s400/revHistory_Beta1.jpg" border="0" alt=""id="BLOGGER_PHOTO_ID_5236350176579893794" /></a>

KDevelop has basic support for three most popular DVCSes now:
<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://bp3.blogger.com/_wb8_4jUpYW4/SHpQ0n8Z4WI/AAAAAAAAADM/iYvIJokjiEI/s1600-h/dvcs.jpg"><img style="display:block; margin:0px auto 10px; text-align:center;cursor:pointer; cursor:hand;" src="http://bp3.blogger.com/_wb8_4jUpYW4/SHpQ0n8Z4WI/AAAAAAAAADM/iYvIJokjiEI/s400/dvcs.jpg" border="0" alt=""id="BLOGGER_PHOTO_ID_5222575582895268194" /></a>

Mercurial and Bazaar are less supported, but I need just few lines to make them more functional, so things below are based on generic code + special Git executors(proxy) which a few lines.
<br>Branching and VCScommitDialog used powered by Git:
<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://3.bp.blogspot.com/_wb8_4jUpYW4/SJn9j6uUmVI/AAAAAAAAADc/zgiwk5rZJg0/s1600-h/branchManager.jpg"><img style="display:block; margin:0px auto 10px; text-align:center;cursor:pointer; cursor:hand;" src="http://3.bp.blogspot.com/_wb8_4jUpYW4/SJn9j6uUmVI/AAAAAAAAADc/zgiwk5rZJg0/s400/branchManager.jpg" border="0" alt=""id="BLOGGER_PHOTO_ID_5231491235668334930" /></a>

<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://4.bp.blogspot.com/_wb8_4jUpYW4/SJn9vTfblAI/AAAAAAAAADk/NiKRdaoEF5A/s1600-h/commitManager.jpg"><img style="display:block; margin:0px auto 10px; text-align:center;cursor:pointer; cursor:hand;" src="http://4.bp.blogspot.com/_wb8_4jUpYW4/SJn9vTfblAI/AAAAAAAAADk/NiKRdaoEF5A/s400/commitManager.jpg" border="0" alt=""id="BLOGGER_PHOTO_ID_5231491431295325186" /></a>

Finally, whetting your appetite. Here is a screen of QGit integrated into KDevelop4. I hope in KDevelop4 we will have something like this (we have almost many things done) for all our DVCSes (Git, Mercurial, Bazaar) and Subversion.
<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://bp3.blogger.com/_wb8_4jUpYW4/SIYYzrO9Y6I/AAAAAAAAADU/k9mOSlqDXH0/s1600-h/qgitIntegration.jpg"><img style="display:block; margin:0px auto 10px; text-align:center;cursor:pointer; cursor:hand;" src="http://bp3.blogger.com/_wb8_4jUpYW4/SIYYzrO9Y6I/AAAAAAAAADU/k9mOSlqDXH0/s400/qgitIntegration.jpg" border="0" alt=""id="BLOGGER_PHOTO_ID_5225891693667902370" /></a>

I want to thank all people helped me during this summer:
<b>Alexander Dymo(adymo, KDevelop)</b> — My mentor who is strong both in GUI and Git.
<b>Andreas Pakulat(apaku, KDevelop)</b> — The man who can help with any part of KDevelop(or maybe even ith whole KDE && Qt).
<b>Shawn O. Pearce (spearce, Git)</b> — A man who is not in KDE, but who contacted my mentor and me to suggest his help.
<b>Marco Costalba — QGit author</b>, explained a lot of code from QGit.
<b>Paul Mackerras — Gitk author</b>, explained some basic algorithm (building rev history).
<b>All guys</b> from different IRC channels, mailinglist.
And of course Google for the amazing Open Source Program: Google summer of Code! 
