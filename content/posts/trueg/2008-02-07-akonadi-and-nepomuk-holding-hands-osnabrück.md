---
title:   "Akonadi and Nepomuk - Holding Hands in Osnabrück"
date:    2008-02-07
authors:
  - trueg
slug:    akonadi-and-nepomuk-holding-hands-osnabrück
---
Last weekend I was invited to the KDE-PIM meeting in Osnabrück to represent Nepomuk. First of all I have to say: thanks a lot for inviting me, guys. The meeting was a lot of fun (although staying awake got harder during the course of the three days you crazy work-maniacs!) and it was great to see known faces again and meet new nice people. As they have during the last years <a href="http://www.intevation.de/">Intevation</a> hosted the event and I want to give a quick thanks to them, too.

So much for the introduction. Let's dive into the good stuff now. The main focus of the meeting were the plans for KDE 4.1 and the integration of Aknoadi. However, the part that is most interesting to me is the Nepomuk integration. And this is were I was very pleasantly surprised. I did not have to do any convincing or argumenting at all. It was obvious that Nepomuk would be the solution for search in Akonadi. And not only that. The understanding of the concepts was flawless.

So what are the plans for Akonadi-Nepomuk integration?
<ul>
<li><b>Taging in KDE-PIM:</b> The most obvious integration at the momment is without a doubt the replacement of categories in KDE-PIM with Nepomuk tags. This would relate PIM resources with tagged files (and of course any other resource type in the future).
<li><b>Akonadi Agents to push data into Nepomuk:</b> Akonadi has the concept of agents. Agents are plugins (although running in their own process) that act on changed data in the Akonadi store. In this case the agents will gather changed data and push it into the Nepomuk storage so it gets searchable and indexed properly. Tobias König already started a first agent which handles contact data, meaning that it converts the Akonadi items into <a href="http://www.semanticdesktop.org/ontologies/nco/">NCO</a> resources which are then stored into Nepomuk.
<li><b>Virtual folders in KMail:</b> KMail will combine the current static folder layout with virtual folders based on live searches. A virtual folder selects a set of emails based on a Nepomuk query. This can turn out to be very powerful since one can define queries that do simple things like <i>"select all emails that contain picture attachments"</i> or more complex stuff like <i>"select all emails that were sent by someone who participated in events tagged with 'KDE-PIM'"</i> or even very fuzzy ones like <i>"select all emails relating to a certain topic"</i>. For this to work Tobias and I started to create a higher level query interface. Although it is currently possible to do these queries, one has to do so by using the Soprano SPARQL query interface which may be too much for many applications.
</ul>

While this is by no means a complete list it shows the direction Nepomuk integration will take in KDE-PIM. A fact I am very happy about.

So much for the high level report about the KDE-PIM meeting. More technical details about the implementation and the problems that still have to be solved later...