---
title:   "The Last Bug..."
date:    2008-03-27
authors:
  - trueg
slug:    last-bug
---
We probably all know the situation: I finally fixes the last bug in Soprano. Yes, I know, there probably is no such thing as the last bug. But it feels good to lie to myself in this case. The redesigned Nepomuk server is done and it works smoothly.
First of all, the Nepomuk Server is no KDED module anymore. So no more 90% CPU load for KDED. It was to heavyweight for KDED anyway. As a recently learned KDED was never intended to be a general purpose service daemon but a manager for small and stable modules. So now the Nepomuk server has its own service management including dependencies handling. Each Nepomuk service runs in its own child process and can be controlled through D-Bus, either through the Nepomuk server's service manager or the processes interface directly (The idea of course is not new: I took the ProcessControl class from Akonadi, thanks guys).
This has several advantages:
<ul>
<li> Implementing a Nepomuk service is still as simple as writing a KDED module except that you derive from Nepomuk::Service instead and use the NEPOMUK_EXPORT_SERVICE macro.
<li> The Nepomuk repository (storage) is a service itself like any other which makes for a much cleaner design.
<li> Strigi is also handled through a Nepomuk service. Again: clean.
<li> A buggy service will never bring down the whole system.
<li> Services can perform as many blocking operations as they want. When implementing a service you don't have to care about threading or asyncing (except if the service should be responsive itself)
<li> The dependency handling automatically delays starting of services until the Nepomuk storage is ready (in case it needs to perform some longer initialization like converting to a new backend)
</ul>
All in all I am very pleased with the new design. I will commit all of it next Monday since we have the new Nepomuk::Service class.
And in case you are wondering why we need a Nepomuk service manager: at the moment we have a total of six Nepomuk services:
<ul>
<li> The data storage
<li> Strigi
<li> The ontology loader which makes sure ontologies are up to date (and will soon support importing new ontologies from the web)
<li> The file watch service which monitors file move and delete operations and updates the metadata accordingly (still in playground)
<li> The alignment service which tries to optimize the data stored in Nepomuk (still in playground and very simple, at some point will do stuff like merging duplicates contact information and so on)
<li> The Nepomuk Social Query Daemon which I blogged about before (in playground and more a proof-of-concept but still....)
</ul>

One future services will be the search service which provides a nice search interface to all KDE applications (that do not want to use low level SPARQL stuff). Work on something in this direction has begun as part of Akonadi.