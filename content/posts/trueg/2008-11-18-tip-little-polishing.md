---
title:   "Tip: a little polishing"
date:    2008-11-18
authors:
  - trueg
slug:    tip-little-polishing
---
I always thought that some KDE apps looked a bit cluttered. Yesterday I finally tried to do something about it. I started with Gwenview. Two things bothered me: 1. the status bar buttons were too small for their text. Easy to solve by simply not forcing the height of the statusbar. 2. the sidebar had a different color than the status bar. Now this is due to Oxygen using gradients which is cool. It turned out to be rather simple. And this is also the actual reason for this blog.

A tip: whenever using sidebars with scroll areas which are supposed to have the Window color as Base do NOT use something like <i>setBackgroundRole( QPalette::Base )</i>. Better let the scroll area not print any background at all. Simply do that by changing the viewport properties:

<code cppqt>
sidebar->viewport()->setAutoFillBackground( false );
</code>

And if you are using QScrollArea be aware that it changes this property also for the widget set via <i>QScrollArea::setWidget</i>. Thus:

<code cppqt>
sidebar->setWidget( myWidget );
myWidget->setAutoFillBackground( false );
</code>

Enough words. This is what it looks like. Notice the difference in the lower right.

<img src="https://blogs.kde.org/files/images/gwenview-before-and-after-unclutter.png">

And BTW: Now that apparently the blogging system changed, how do I properly include images? [image:ID] was a really nice system...