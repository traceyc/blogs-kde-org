---
title:   "\"Aaron, we owe you\" or \"Why I am happy that Nepomuk is not as popular as Plasma\""
date:    2008-06-26
authors:
  - trueg
slug:    aaron-we-owe-you-or-why-i-am-happy-nepomuk-not-popular-plasma
---
After more than two weeks of vacation I read up on my email and of course am also sickened by some of the stuff I have to read there. Let me open with a quote:

<i>Here's a real suggestion: give us back our Desktops!</i>

That is just plain sad! But it pretty much covers the topic of the whole Aaron/Plasma unpleasantness. Aaron and the Plasma devs are trying something really innovative here, trying to bring a new level of usability and beauty to our desktop. This is not an easy task but nonetheless they do it and IMHO they succeed. And what do they get for it: bashing and complaint over complaint. 

Being a developer myself, I know how frustrating even one mail of the "your-solution-is-crap-do-it-this-way-it-will-be-easy" kind can be. But getting such a load of them over months. Man, and still Aaron continues to work on Plasma.

Even if it was not possible to have the old plain and boring desktop back, we should get over ourselves and give new ideas a chance! The whole KDE4 thing is such a brave endeavor. Introducing so many new things, knowing that KDE will lack features for quite some time, and doing it anyway because the desktop has to evolve somehow... why do so many people always only see the problems and never the opportunities? Or are they like me and shut up about it? Is that our problem: do we only speak up to complain but never to praise?

If that is the case, then here we go: I personally think that KDE 4 is a great step forward. I know that it will take some time to port all KDE 3 features. But all the new ideas, all the chances that were taken, make me proud to be part of it. Thanks and congratulations to all of you who took the chance!

I think I am one of those benefiting directly from the willingness to try new things. After all, the KDE developer community gave me the chance to bring Nepomuk into the KDE 4 mix, even though there are still usability problems around and the real benefits are not yet visible. The only reason I don't get beaten over the head all the time like Aaron, is that Nepomuk is not as visible, not as sexy as Plasma. And for once (after envying Aaron for nearly two years) I am happy that many users fail to understand what Nepomuk is or that I fail to explain and promote it properly.

Aaron, I am sorry, that I did not step up before. I, too, should know better. So today I do.

<i>Addendum: I stopped reading the main thread on kde-devel after half the messages. Even I could not take it anymore. How could the Plasma devs and Aaron?</i>