---
title:   "Moving my Blog"
date:    2009-02-19
authors:
  - trueg
slug:    moving-my-blog
---
Since I am missing important features from the kdedevelopers.org blog system I am moving my blog to <a href="http://trueg.wordpress.com/">wordpress</a>. From now on all Nepomuk related blogs will be posted there.