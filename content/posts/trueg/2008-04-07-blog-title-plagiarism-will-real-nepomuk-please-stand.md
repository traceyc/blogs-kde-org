---
title:   "Blog title plagiarism: \"Will the real Nepomuk please stand up!\""
date:    2008-04-07
authors:
  - trueg
slug:    blog-title-plagiarism-will-real-nepomuk-please-stand
---
Now what is that supposed to mean? The "real" Nepomuk? Well, you did not actually think that I would introduce an RDF store into KDE just to save some tags and ratings? No, the "real" motivation goes way beyond that and it is time to hint at it.
Today I committed the PIMOShell to the Nepomuk playground (To the right you see the PIMOShell main window showing all xesam:Music resources).

[image:3374 style="float:right; margin:10px;"]

The PIMOShell is a metadata maintenance and debugging tool which I will now use to give a glimpse of the "big picture".

But first a few words on PIMO: PIMO, the <b>P</b>ersonal <b>I</b>nformation <b>M</b>odel <b>O</b>ntology, forms the basis for all custom, user-created classes (types) and properties. It defines basic stuff like an Agent or a Location and is intended to be extended by the user in any way he or she likes (<a href="http://dev.nepomuk.semanticdesktop.org/wiki/PimoOntology">more information on PIMO</a>).

Let us dive into an example:

By using the context menu in the upper left class list PIMOShell allows us to create new classes with a nice little dialog. Basic information like a label and an optional icon and description can be added directly. We create a new class "Friend" which is a special kind of Person: a close friend.

[image:3375 size=preview]

Once the class is created it shows up as a new subclass of pimo:Person which in turn is a subclass of pimo:Agent. It can now be used like any other class in the system. That essentially means that we can create instances:

[image:3376 size=preview]

Again we can set basic properties like the label and the image. We create an instance of Tudor, my friend from DERI, Galway. Once we did so, PIMOShell lists Tudor as a new instance of Friend:

[image:3377 size=preview]

Now Tudor has been created as a new RDF resource in the Nepomuk storage. And that means we can query him using the simple Nepomuk query client:

[image:3378 size=preview]

When searching for all resources of type "Friend" we find Tudor. Nice. :)

But simply creating new classes is not fun enough. To categorize friends we might be interested in what relates us to them. Like a mutual interest. Thus, we create a new property:

[image:3379 size=preview]

As you can see the new property will be for our new class "Friend". And once it is created we can change its value in the lower pane:

[image:3380 size=preview]

The mutual interest that I share with Tudor is complaining about bad food (just for the sake of the example of course).

Again the new information allows us to find Tudor:

[image:3381 size=preview]

I personally think this is quite cool. Of course the PIMOShell is not intended for the end user. But it gives an idea pf the possibilities. A PIM application might categorize according to user created classes with additional fields, saved text documents might be typed, we can organize arbitrary data in a powerful way. All we need is a deeper application integration. And this is where you come in. I hope. ;)