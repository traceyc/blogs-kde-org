---
title:   "Me Nepomuk, You Nepomuk, too?"
date:    2008-03-18
authors:
  - trueg
slug:    me-nepomuk-you-nepomuk-too
---
Now that the Nepomuk project review is done I can get back to promoting Nepomuk features and possibilities. Today I will show how existing Nepomuk and Soprano technologies can be combined to provide very simple "Social" capabilities.
In a <a href="http://blogs.kde.org/node/3274">previous blog entry</a> I presented the Nepomuk search client which allows to search the Nepomuk data store based on installed types and properties. Now how about taking that, wrapping it in the simple <a href="http://api.kde.org/kdesupport-api/kdesupport-apidocs/soprano/html/classSoprano_1_1Server_1_1ServerCore.html">Soprano tcp server/client system</a> and announcing it via Avahi? That would allow us to query our buddies' Nepomuk data. I did exactly that and the result are two little tools with very fancy names: The <i>Nepomuk Social Query Daemon and Client</i>.

[image:3333 align=center size=preview]

The nsqd, the <i>Nepomuk Social Query Daemon</i>, is a kded module which provides an Avahi service that is then found by the nsqclient, the <i>Nepomuk Social Query Client</i>, which allows to perform queries on remote data in the exact same way as the normal search client does. Easy.
Now as this is just a showcase tool there is close to no security except for read-only access. Thus, once the nsqd is running everyone able to open a connection to the server port is able to read your data. So there is room for improvement. ;)

The nsqd and nsqclient can be found (like all experimental Nepomuk stuff) in the <a href="http://websvn.kde.org/trunk/playground/base/nepomuk-kde/">KDE svn playground</a> module. Due to the security issues the nsqd is not started by default but has to be started manually:

<pre>qdbus org.kde.kded /kded org.kde.kded.loadModule nsqd</pre>

<b>Implementation</b>
<i>If you are not interested in hacking details just stop reading now.</i>
Let's look at some details. The most simple thing first: the read-only access. I did this by implementing a read only Soprano Model (which I actually moved to <a href="http://api.kde.org/kdesupport-api/kdesupport-apidocs/soprano/html/classSoprano_1_1Util_1_1ReadOnlyModel.html">Soprano::Utils</a> since it has a clean API and seems usable beyond this example). This is actually pretty simple: I just derived it from <a href="http://api.kde.org/kdesupport-api/kdesupport-apidocs/soprano/html/classSoprano_1_1Model.html">Soprano::Model</a> and made all writing methods throw a "permission denied" error. (I did not derive from Soprano::FilterModel since then one could easily access the parent model and write to it anyway.)
Exposing the local Nepomuk store through TCP was simple, too. Soprano already comes with a simple binary TCP server/client implementation (which I used before the cleaner D-Bus one). So all I had to do was create a <a href="http://api.kde.org/kdesupport-api/kdesupport-apidocs/soprano/html/classSoprano_1_1Server_1_1ServerCore.html">Soprano::Server::ServerCore</a> implementation which forwards all calls to the local Nepomuk server.
This is actually pretty simple. In our new ServerCore subclass we create a connection to the Nepomuk server:

<pre>
SopranoForwardingCore::SopranoForwardingCore( QObject* parent )
    : ServerCore( parent )
{
    m_client = new Soprano::Client::DBusClient( "org.kde.NepomukServer", this );
[...]
</pre>

Then we have a cache for the models which store the wrapping ReadOnlyModel instances and simply forward:

<pre>
Soprano::Model* SopranoForwardingCore::model( const QString& name )
{
    if ( m_models.contains( name ) ) {
        return m_models[name];
    }
    else {
        if ( Soprano::Model* model = m_client->createModel( name ) ) {
            Soprano::Util::ReadOnlyModel* roModel = new Soprano::Util::ReadOnlyModel( model );
            model->setParent( roModel ); // memory management
            m_models.insert( name, roModel );
            return roModel;
        }
    }
    return 0;
}
</pre>

Last but not least we start the server core, i.e. make it listen on some port and promote the service through Avahi:

<pre>
m_serverCore = new SopranoForwardingCore( this );
m_serverCore->listen( 0 );
m_dnssdService = new DNSSD::PublicService( "Nepomuk Social Query Service","_nepomuk._tcp", m_serverCore->serverPort() );
m_dnssdService->publish();
</pre>

Now the nsqclient can discover our service through Avahi and connect to it using Soprano::Client::TcpClient:

<pre>
Soprano::Model* createModel( DNSSD::RemoteService::Ptr service, const QString& name )
{
    Soprano::Client::TcpClient* sopranoTcpClient = new Soprano::Client::TcpClient();
    QHostInfo hostInfo = QHostInfo::fromName( service->hostName() );
    sopranoTcpClient->connect( hostInfo.addresses().first(), service->port() );
    return sopranoTcpClient->createModel( name );
}
</pre>

Again: easy.
Ok, that is all for today. I feel I am getting too technical again anyway.