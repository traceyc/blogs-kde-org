---
title:   "Nepomuk Performance and GUI goodies"
date:    2008-03-22
authors:
  - trueg
slug:    nepomuk-performance-and-gui-goodies
---
<b>Some words on performance</b>

Nepomuk performance has always been a bit of a problem. Last but not least this was due to the D-Bus communication with the Nepomuk server that took place all the time. Don't get me wrong, D-Bus is pretty fast, but you always get the overhead of the marshalling of messages and routing them through the D-Bus daemon.
So with the new QLocalServer and QLocalSocket in QT 4.4 which introduce Windows compatibility, I re-enabled the Soprano local socket communication which is a lot faster.
Now the Nepomuk server provides two interfaces: the good old and very easy to use D-Bus interface and the fast binary local socket interface. (The latter is barely documented since it is only intended for Soprano itself through Soprano::Client::LocalSocketClient).
To use the new interface one could of course create and instance of LocalSocketClient but that is not recommended for two reasons:
<ol>
<li>The path to the socket would be hardcoded in the application</li>
<li>The local socket communication does not support signals</li>
</ol>
That is why libnepomuk provides a fancy Soprano::Model implementation that handles all this transparently. It executes all commands through the socket interface while listening for signals via D-Bus. At the moment this Model can be accessed through
<pre>
Soprano::Model* mainModel = Nepomuk::ResourceManager::instance()->mainModel();
</pre>
Internally a new class called Nepomuk::MainModel is used. That one is not public API yet but might become at some point to allow developers to use the interface without creating a ResourceManager instance.
Apart from that I also did some small optimizations in Soprano which I tracked down using valgrind. Very cool, I think that is actually the first time I was able to improve performance based on valgrind results.

<b>GUI stuff</b>

Ok, enough of the internal implementation details. Lets have a look at some nice GUI improvements that I would like feedback on. The target is Dolphin which has been providing Nepomuk annotations for quite a while now. The problem always was that I created ugly prototype widgets which I never improved (except for the rating one). Yesterday evening I changed that and commited a tagcloud and a new commenting widget to Dolphin. Now it still looks a bit cluttered but IMHO much better than before.

[image:3342 style="float:right; margin:10px;"]

The basic idea for both is to display the comment and the tags read-only and allow the user to edit them in a fancy popup which appears after clicking on the appropriate button. In case of the tagcloud we get another tagcloud which shows all available tags, the ones assigned selected.
Please go ahead and test it and let me know how it can be improved. This does include the look and feel as well as the layout of the sidebar (spacing and margins and alignment and stuff).