---
title:   "PIM Reference."
date:    2004-09-18
authors:
  - allen winter
slug:    pim-reference
---
From the One-Thing-Leads-To-Another Department:<br>
You may recall from a previous blog that I am changing libkcal to provide a better Incidence sorting interface.  While I am working this I decided to check for consistency across the Event, Todo, and Journal methods in libkcal.<br>
So, I searched for an on-line KDEPIM API Reference at the <a href="http://api.kde.org/3.3-api">KDE API Reference pages</a>.  Not there.

Cornelius said there was a very old version at korganizer.kde.org

So I ran 'make apidox' in the kdepim module to generate the updated doc but there were tons of doxygen errors.

With encouragement from the kdepim developers I managed to fix all the doxygen errors.  But work still needs to be done to write accurate class documentation.  I hope the Quality Team can help with this job.

I contacted sysadmin@kde.org about auto-generating the KDEPIM Reference and hanging it up on <a href="http://pim.kde.org">KDEPIM Home Pages</a>.  They haven't responded to me yet... NAG!
