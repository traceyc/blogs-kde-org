---
title:   "Not! My Halloween Document"
date:    2004-10-31
authors:
  - allen winter
slug:    not-my-halloween-document
---
Some thoughts about "the competition":  Nah...<br>
I like KDE and I enjoy developing for it.  Thank goodness we have choices.
<br><br>
Some recent PIM accomplishments:
<ul>
 <li>I wrote (not really, I took the To-do plugin and hacked on it for a couple of hours) a Journal plugin for Kontact.  You'll find it in the development code.</li>
 <li>Finally committed all my Incidence sorting methods into libkcal.  Yeah! That code has been sitting in my local sandbox for months.</li>
</ul>
<br><br>
I really want to get to back to work on my "Special Dates" summary plugin for Kontact.  This thing will display holidays, birthdays, anniversaries, special occassions,
in the Kontact summary.  I hope it will make the Addressbook summary obsolete.
<br><br>
...and KonsoleKalendar is in a bad way in CVS right now. drat.
<br><br>
Until next time....