---
title:   "KasBar 3NG"
date:    2004-07-24
authors:
  - rich
slug:    kasbar-3ng
---
I've been doing some work in the last few weeks to get Kasbar ready for the KDE 3.3 release. The code is now looking good, and doing this has given me some ideas for some major Kasbar enhancements for the next KDE release.
<br>
<br>
I've fixed the progress indicator code - you now get a progress bar just below the icon label for any progress dialog. This works even if the window is minimised. The overall effect of this is really quite cool and I've found it works really well.
<br>
<br>
I've also fixed the version indicator in the about dialog, and some minor display problems with the display of the license text. I've also fixed a problem that was reported with the positioning of the popup in some circumstances.
<br>
<br>
To make up for the dull work above, I've created a branch in which I'm working on a massively souped version of Kasbar. I've already done a lot of work reworking the drawing code and added a number of new features:
<ul>
<li>Kasbar can now be detached to become a floating window. This lets it be dragged to wherever the user wants.
<li>Kasbar can send any window to the system tray using ksystraycmd. This should probably be moved into the taskmanager library so that it become a feature in all KDE taskbars.
<li>I have a prototype of embedding the window thumbnails in the icon boxes instead of showing icons.
<li>Support for custom sizes, instead of the fixed set of 3 you have in the current code.
</ul>
There are a bunch of other things I plan to add including custom grouping, support for showing all the documents in MDI windows and more, but I think I've made a good start. Once I get the code into a decent state I'll make a standalone release that can be used with KDE 3.3 so people won't have to wait for the next KDE release to get these benefits.
