---
title:   "SSL at the Qt Contributor Summit"
date:    2011-07-03
authors:
  - rich
slug:    ssl-qt-contributor-summit
---
I haven't written any posts for a few weeks, so I figure it's time for an update. I, along with a bunch of other people who hack on Qt, attended the Qt Contributor Summit in Berlin. This was a very useful event, since it brought together a good mixture of both Nokia developers and external developers - as I would have hoped, KDE was one of the most common afiliations. One of the main things I wanted to do during the event was get together with the people responsible for the Qt SSL code and ensure that the plans for Qt5 will meet the needs of KDE (and others who want non-trivial SSL usage).

The discussions with Peter and Markus (the guys responsible for this code) were very productive, to the point that the only reason we didn't get a couple of the smaller items merged then and there were that they didn't have access to the Nokia internal network to do it. Peter, Markus and I have been tracking the stuff we'd like to see sorted in Qt's SSL for quite some time in a wiki page at <a href="http://developer.qt.nokia.com/wiki/Improving_Qts_SSL_Support">http://developer.qt.nokia.com/wiki/Improving_Qts_SSL_Support</a> and after the discussions, I forsee few problems in getting any of the improvements on the list incorporated into Qt.

One complex area we touched on was the possibility of changing the Qt APIs so that the various authentication required, and ssl certificate errors signals can be handled without a nested event loop. We even managed to figure out a way of doing this that would remain backwards compatible with code using the current API, so I have some hope that this can be achieved.

Following the conference I've already had a couple of merge requests approved - printing certs as text <a href="https://qt.gitorious.org/qt/qtbase/merge_requests/2">https://qt.gitorious.org/qt/qtbase/merge_requests/2</a> and support for duplicated elements in certificate names <a href="https://qt.gitorious.org/qt/qtbase/merge_requests/5">https://qt.gitorious.org/qt/qtbase/merge_requests/5</a> (source incompatible with Qt 4). Currently I'm finishing off the code for validation of certificate chains (when not connecting to a server) which should be done in the next week or two, the code is at <a href="https://qt.gitorious.org/~rich/qt/richs-qtbase/commits/verify-certificate">https://qt.gitorious.org/~rich/qt/richs-qtbase/commits/verify-certificate</a> if you want to look.

Once I've completed the changes above, I currently trying to decide between access to certificate extensions, EV certificate support and finishing integrating my OCSP code.
<!--break-->
