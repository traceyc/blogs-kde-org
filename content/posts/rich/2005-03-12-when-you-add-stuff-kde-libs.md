---
title:   "When you add stuff to KDE Libs..."
date:    2005-03-12
authors:
  - rich
slug:    when-you-add-stuff-kde-libs
---
Reviewing some of the new code that has been added to kdelibs for 3.4, I've noticed a few old problems recurring. Some of the new classes are missing d pointers, this will be a pain in the future and should not have happenned. I strongly urge anyone writing stuff for kdelibs (or interfaces that plugins etc. will talk to for that matter) to review the doc on binary compatibility I link below.

http://developer.kde.org/documentation/other/binarycompatibility.html

While you're at it, you should also probably review:

http://developer.kde.org/documentation/other/mistakes.html

In future, we probably need to ensure that APIs etc. are reviewed before additions are made to kdelibs, but until we break bc we're stuck for now.
