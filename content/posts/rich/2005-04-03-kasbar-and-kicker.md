---
title:   "Kasbar and Kicker"
date:    2005-04-03
authors:
  - rich
slug:    kasbar-and-kicker
---
I'm wondering about kicker and kasbar in KDE 4. If we can make kicker's abilities powerful enough to support the features kasbar needs, then maybe it should be less of a standalone app. At the moment, maintaining kasbar is nightmare because it makes kicker do a bunch of things it wasn't designed to support (assuming kicker was ever designed at all!).

These problems manifest in things like kicker's autohiding of kasbar (kicker doesn't both to tell anyone when it does this), the way the docking to the edge of the screen works (the API has sucked since support for the WM spec was hacked on) and many other areas.

The changes needed to make kicker sufficient would include things like supporting the idea of applets painting their own backgrounds (which used to be the case, but then got somewhat broken when transparency was added). A size handling system that actually works. And of course, a more sensible definition of what is an applet and what is an extension.

Really, what's needed is a complete rethink of how kicker works, and the APIs it provides. Just as well that's the plan. :-)
