---
title:   "KJSEmbed support for event handling"
date:    2003-09-06
authors:
  - rich
slug:    kjsembed-support-event-handling
---
KJSEmbed can now handle events. At the moment only mouse events, but the rest will come soon.  The example below shows how this can be used in practice. Note that I haven't committed this code yet, so it'll be a couple of days before you can use it.

<pre>
var scribble = new QWidget();
scribble.line=false;
scribble.x=0;
scribble.y=0;

scribble.mousePressEvent = function(ev)
{
  if ( ev.button == 2 ) {
    this.drawText(ev.x,ev.y,'KJSEmbed!');
  }
  else {
    x = ev.x;
    y = ev.y;
    line=true;
  }
}

scribble.mouseReleaseEvent = function(ev)
{
  if ( line )
    this.drawLine(x,y,ev.x,ev.y);
  line = false;
}

scribble.show();
</pre>