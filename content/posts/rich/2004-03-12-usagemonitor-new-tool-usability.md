---
title:   "UsageMonitor - A New Tool for Usability"
date:    2004-03-12
authors:
  - rich
slug:    usagemonitor-new-tool-usability
---
One thing that recent dicussions about usability have highlighted is how much our decisions are currently based on personal opinions rather than hard facts. Aaron has recently been trying to address this by asking people how much they use the various buttons in the konqueror toolbar, but this is both labour intensive and inaccurate. So, I've written the 'Usage Monitor' plugin.
<br><br>
The Usage Monitor is a KParts plugin that records a log message whenever a KAction is fired. The log simply notes the action that was activated and how it was done - eg. was it via the toolbar, a menu or a keyboard shortcut. By analysing this log it is possible to measure which parts of an applications GUI are being used, and which are not. If a reasonable users can provide sample data using this tool, then we can build up an acurate picture of things we could previously on guess at.
<br><br>
The plugin itself is tiny at around 150 lines of code, and works with any application that supports KPart plugins (which covers most KDE  apps these days). No modifications are needed to the application itself, and you don't need to tweak the monitor for your app either. The code is a good example of the power of the XMLGUI framework as it shows how easy it makes creating tools that can apply throughout KDE. In this case, it even lets you track the use of actions provided by plugins and embeded kparts as well as the main application.
<br><br>
Here is a sample log recorded from Konqueror:

<pre>
konqueror: Monitor:     reload  toolbutton_reload       KToolBarButton
konqueror: Monitor:     home    toolbutton_home KToolBarButton
konqueror: Monitor:     back    unnamed KToolBarButton
konqueror: Monitor:     newtab  unnamed QSignal
konqueror: Monitor:     quit    unnamed KAccelPrivate
</pre>

As you can see, I was driving the app mainly through the toolbar. The newtab action was fired from the menubar, and I quit using the keyboard shortcut. In order to get decent use of these logs, they would obviously need to be recorded over a longer period of time but at least now we have a tool with which this can be done!
<br><br>
The code can be found in <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/usagemonitor/">kdenonbeta/usagemonitor</a> - users and people wanting to hack on it are welcome.