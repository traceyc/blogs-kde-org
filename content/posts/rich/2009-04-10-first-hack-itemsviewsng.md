---
title:   "First Hack with ItemsViewsNG"
date:    2009-04-10
authors:
  - rich
slug:    first-hack-itemsviewsng
---
People reading planet will have seen Thomas Zander's post about the new ItemViews framework the Trolls^H^H^HQt Software guys have been working on. It's very experimental right now, but I thought I'd have a quick look. I decided that a fun hack to write would be to take a standard listview that displays a list of URLs (boring!) and write a custom view that instead displays the rendered web page. I only spent a couple of hours on it, so the code is a hack (eg. you need to resize it to get the pages to display once they've loaded) but the results look ok. The whole <a href="http://xmelegance.org/devel/weblist.tar.gz">code for this example</a> comes in under 120 lines.
<br>
<img src="http://xmelegance.org/devel/itemviewsng2.png">

So, that's my hack in response to Thomas's challenge, who's next?

Here's an <a href="http://xmelegance.org/devel/weblist2.tar.gz">updated version</a> that correctly updates the items when the page is loaded without requiring a resize.

And now <a href="http://xmelegance.org/devel/weblist3.tar.gz">version 3</a> that disables the scrollbars in the previews making things look <a href="http://xmelegance.org/devel/itemviewsng3.png">a lot nicer</a>.
<!--break-->
