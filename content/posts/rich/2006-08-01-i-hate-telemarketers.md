---
title:   "I hate telemarketers"
date:    2006-08-01
authors:
  - rich
slug:    i-hate-telemarketers
---
I just got a spam call from some telemarketers called Eurointerview who are based in Germany. They seem to think that because they're not in the UK the telephone preference service doesn't apply to them, and presumably that because they're calling the UK the German laws don't apply either. If anyone has any spare junk mail they don't want they could give it to them at: EuroInterview GmbH, Hansestraße 69, 48165 Muenster, Germany. Phone: +49 (0) 2501-968-0, Fax: +49 (0) 2501-968-190, 
E-Mail: info@eurointerview.com.

