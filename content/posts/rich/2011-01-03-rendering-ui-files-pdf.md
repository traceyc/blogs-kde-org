---
title:   "Rendering UI files to PDF"
date:    2011-01-03
authors:
  - rich
slug:    rendering-ui-files-pdf
---
As a followup to my previous blog post about rendering widgets to SVG, lets take a look at rendering them to PDF. I won't go into as much detail as the  previous blog post since the code is mostly the same. This time, instead of a renderToSvg() method, we have a renderToPdf() method. All the rest of the code is basically the same as the previous example.
<p>
The renderToPdf() method makes use of the ability of QPrinter to generate PDF files (incidentally, it can also generate postscript). The important part of the code is as follows:
<pre>
    QPrinter pdf;
    pdf.setOutputFormat( QPrinter::PdfFormat );
    pdf.setOutputFileName( pdfFile );
    QPainter p;
    p.begin(&amp;pdf);
    target->render(&amp;p);
    p.end();
</pre>
We create a printer (using the default printing mode etc.), then tell it to use PDF output. Next we set the output filename. Finally, we render the widget as before. Like QSvgGenerator, QPrinter is a QPaintDevice so the API is just the same. The output file can be seen at <a
    href="http://xmelegance.org/devel/networkrequests.pdf">http://xmelegance.org/devel/networkrequests.pdf</a>, and the code is in gitorious <a href="http://gitorious.org/qt-examples/qt-examples/trees/master/uifile-fun2/">qt-examples repository</a> as before.

<p>
I mentioned in my previous post that QImage is also a QPaintDevice, so an exercise I'm leaving to you is to make 3rd version of the code that will render widgets to an image.
<!--break-->
