---
title:   "Duplication in open source"
date:    2004-08-27
authors:
  - rich
slug:    duplication-open-source
---
One thing that people constantly seem to carp on about in Open Source circles is
the frequency with which there are several apps that do the same thing. They then
generally go on to say that the developers should work on whatever their favourite
application is. The implicit assumption here is that having multiple apps is wasteful,
but I don't agree that this is the case.

One of the well known tenets of computer science (first set down by Fred Brooks in 'the
mythical man month' ) is that adding more developers to a project will not always make
things go faster. There are a number of reasons for this including the length of time
it takes people to get up to speed with what they're doing, but also the difficulty of
coordinating the effort.

In open source development, work is generally distributed between developers who are
scattered around the world. Communication is generally by email or IRC and only rarely
are there opportunities like aKademy where people can get together and talk face to face.
The limited communication bandwidth between developers is also reduced by the fact that
the most productive developers tend to code rather than talk! The net effect of these
factors is that the difficulties outlined by Fred Brooks are even more difficult to overcome
in open source development than in traditional 'cathederal' style development.

A further factor exists in open source, and that is the ability of one project to reuse
code from another freely. This is very common though it often takes the form of reusing
ideas rather than directly copying code. Since the ideas and concepts behind all apps are
visible to everyone because we have the source, developments in one application can help
the progress of another. If application 'A' comes up with a cool way of solving a problem
then there's a good chance you'll see application 'B' using a variant of it soon after.
Taking all these things together, you can see that having two apps for a problem is not
necessarily a waste.

A final thought to consider is that most open source developers are volunteers. They
are writing code because they like to write code, open source projects are often simply
a way for like-minded people to get together and write cool stuff. If one day someone
thinks I wonder how you'd write something that does XYZ then an application that does
XYZ is what will result. But the motivation wasn't to produce the application, so much
as to see how you would do it.

