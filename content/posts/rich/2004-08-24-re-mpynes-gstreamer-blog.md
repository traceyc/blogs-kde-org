---
title:   "Re mpyne's gstreamer blog"
date:    2004-08-24
authors:
  - rich
slug:    re-mpynes-gstreamer-blog
---
Since mpyne's blog on planetkde doesn't allow comments I'll post this here: You are incorrect that KDE requires glib thanks to arts - use the ARTS_1_1_BRANCH like most of the core team. That way you can avoid glib entirely. glib <b>is</b> a major barrier to acceptance of gstreamer (and to other technologies).

