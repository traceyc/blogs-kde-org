---
title:   "Things are coming along nicely"
date:    2003-08-02
authors:
  - rich
slug:    things-are-coming-along-nicely
---
I managed to get a first pure-KJSEmbed application working last night. It's only a basic text editor, but it creates its  own KMainWindow and has acess to the action collection. Combined with the signal/JS function code I added recently I think we're almost at the point where KJSEmbed can be used to create real applications rather than just simple wizards etc.
