---
title:   "Tokamak4 - A New Hope"
date:    2010-02-24
authors:
  - rich
slug:    tokamak4-new-hope
---
Today I'm  leaving Tokamak 4, so I thought I'd write a post about what I've been up to while I've been here.
<p>
The first day or so I spent getting my machine sorted out as trunk was causing some issues with my graphics driver leading to a hang in the DRI layer of the kernel. I also had a chance to triage some ksnapshot bug reports and look over some patches.
<p>
The next day was largely taken up with talks about the current status of the various parts of plasma, kwin and related technologies. We got a little hacking done but not much as it was more important to make sure we're all coordinating our development.
<p>
The remaining days became a bit of a blur, so in no particular order:
<ul>
<li>I refactored the plasma javascript scriptengine to make it easier to use it with QScriptEngines that are created externally. This is required before we can integrate things like QML.
<li>Worked out what is required for QML integration to be implemented in a way that is compatible with our existing javascript bindings etc. This will require some small changes to the API offered by Qt for QML, which may be a problem. If we don't get those changes then QML is likely to be trapped outside the main engine which will reduce what it can do unless we write
another set of bindings.
<li>Made some fixes to the webslice applet to improve error handling.
<li>Started work on an idea I've had in the back of my head for a while, I won't say too much about it here, but I'll include a pretty picture to whet your appetite:
<br>
<img src="http://xmelegance.org/devel/webwatcher3.png">
<p>
In addition to this I've taken part in lots of discussions on topics ranging from activities/contexts, scripting of animations, silk and more. I'd like to thank Will and the rest of the openSuSE team for making this such a pleasant and productive sprint.
<!--break-->
