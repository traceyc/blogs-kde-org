---
title:   "Trinary operator in python"
date:    2005-04-15
authors:
  - rich
slug:    trinary-operator-python
---
Here's an example of an easy and compact way to simuate the trinary operator in python. It is not short circuit like the one in C, but it is pretty simple:
<pre>
  newowner    = &#91;self.newowner, None&#93;&#91;self.contactid == self.newowner&#93;
</pre>
This will set newowner to self.newowner if self.contactid is not the same as self.newowner. Otherwise it will set it to None.
[EDIT] How the hell do I stop drupal destroying square brackets?
[EDIT] Aha, you need to use escape codes.

