---
title:   "EEE PC"
date:    2007-12-09
authors:
  - rich
slug:    eee-pc
---
I finally got my hands on an EEE PC on Friday after having waited for nearly a month for the one I ordered by mail order to arrive. I found out that 'Toys R Us' had some stock and checked that the one in manchester had some. Now I've just got the difficult decision of whether to cancel my original order or not!

The device itself feels a lot more solid than I was expecting, and the pearly white look is actually rather nice. It comes with KDE 3.4 installed, but with a cut down launcher UI. Switching from  the 'easy mode' to a real KDE desktop only took a couple of minutes, so I now have a tiny KDE laptop! 

I had some trouble getting it onto my home wifi network, but giving it a static IP address has meant things now work fine. Given that the forums indicate no one else has had a problem, I suspect the problem was something specific to the way my network here is set up.

The set of installed software is impressive - in fact most of the things I was planning to add in turned out to be there already! Some of the highlights I've noticed so far are:

<ul>
<li>Python 2.4.4
<li>ssh
<li>krdc (VNC client)
<li>mplayer (all the video files I've tried worked first time)
<li>konqueror, konsole, ksnapshot, etc. etc. Basically all of the common KDE applications.
</ul>

One thing that really impressed me is that it comes with the python Qt bindings included making it very easy to write tools to overcome any missing pieces I find. One that I've started some UI mockups for is a backup application which will be my first chance to play with Qt programming in python.

Another neat feature is that the supplied media includes a tool to put a bootable rescue image onto a pen drive - this means that I can feel quite free to replace the Xandros install it comes with if I want to. Putting the image from my mandriva pen drive (or the image for the intel classmate) onto the EEE for example should be easy.

Now it's time to get started trying to put some code around the UI mockups for my backup tool, and learning the python side of Qt programming.
<!--break-->