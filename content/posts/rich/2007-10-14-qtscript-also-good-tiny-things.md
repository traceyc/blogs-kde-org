---
title:   "QtScript is also good for tiny things"
date:    2007-10-14
authors:
  - rich
slug:    qtscript-also-good-tiny-things
---
Here's a quick example of why it's nice to have a script interpreter embedded in Qt: Plasma's KRunner has a calculator which used code borrowed from the KDE 3.x minicli. The old code started up the bc command line calculator then displayed the result - not exactly an efficient way to do things. I've just committed a change that makes it use QtScript and the code is trivial:
<pre>
QString CalculatorRunner::calculate( const QString& term )
{
    QScriptEngine eng;
    QScriptValue result = eng.evaluate( term );
    return result.toString();
}
</pre>
This replaces 23 lines of code and is quite a bit more powerful. Nice!
<!--break-->