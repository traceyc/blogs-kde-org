---
title:   "Being Earnest is Nice, but Being Useful is More Important"
date:    2004-05-21
authors:
  - rich
slug:    being-earnest-nice-being-useful-more-important
---
Last night I was able to commit to CVS the first cut of a kjsembed app I'm working on that will be useful to anyone - even if they don't care about scripting. The app I'm talking about is called Imunge and is intended to be a styleguide compliant tool for manipulating images.

Imunge needs quite a bit of work before it will be attracting users in droves, but it already has enough functionality to show that kjsembed apps can provide both the functionality and performance needed to be useful.

Since I'm writing this note, I'll also mention that I've restructured the binding code a little. The new structure makes it possible to provide a much richer environment when kjsembed is built in qt-only mode. We will soon be providing nightly builds for win32 that use the qt-only mode, so people who want to write cross-platform scripts can join the fun. The win32 builds will use the Qt non-commercial license.

