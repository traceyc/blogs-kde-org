---
title:   "Creation of KParts, opaque QVariant support, void * support..."
date:    2003-08-17
authors:
  - rich
slug:    creation-kparts-opaque-qvariant-support-void-support
---
Lots of new features in KJSEmbed over the last few days. I got a new factory method implemented that lets you create read-only parts! This has let me knock together an example that uses KHTMLPart to provide a web browser - total code 22 lines (including blanks and comments). There is also now support for any QVariant type - even those that aren't explicitly suported.
