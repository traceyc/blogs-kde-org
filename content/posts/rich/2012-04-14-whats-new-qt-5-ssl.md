---
title:   "What's New in Qt 5 for SSL?"
date:    2012-04-14
authors:
  - rich
slug:    whats-new-qt-5-ssl
---
With the availability of the Qt 5 alpha, I thought I'd try to summarise what's been done in the SSL stack. Most of the changes in Qt 5 for SSL are incremental improvements, or things that will form the basis of future changes. In this post I'll try to highlight the main changes:

<li>QSslCertificate::subjectInfo() and QSslCertificate::issuerInfo() now return a QStringList instead of a QString

It's pretty common for a certificate to contain more than entry of a specific type, but in Qt 4 the API only let you access the first one.

<li>QSslCertificate::isValid() has been deprecated.

Originally this method was just used to check the validity of the dates for which a certificate was valid, when the certificate blacklist was introduced to deal with issues such as the Commodo compromise, checking for blacklisted certificates was added too. Unfortunately the name of the method gave the misleading impression that simply calling it was enough check the validity of a certificate - it isn't. There is now a new isBlacklisted() method which can be used to check if a certificate is blacklisted (and checking the dates is trivial anyway).

<li>QSslCertificate::alternateSubjectNames() is deprecated and replaced by QSslCertificate::subjectAlternativeNames().

The new name reflects the actual name of this field in the RFCs etc.

<li>The QSsl::TlsV1 enum value was renamed to QSsl::TlsV1_0

Since there are now multiple versions of TLS this is a pretty obvious change.

<li>QSslCertificate::serialNumber() now always returns the serial number in hexadecimal format.

The old code returned the serial number as a hex string if it was long, but as an integer if it was short. For consistency we now always use the same format.

<li>The openssl network backend now reads the ssl configuration file allowing the use of openssl engines.

This change means that people wanting to use openssl engines such as hardware accelerators can do so using their openssl config file and Qt will respect the setting. This was possible before, but required setting a flag at compile time.

<li>QSSlCertificate::toText()

There's now a method to convert a certificate to human readable text (for certain values of human).

<li>QsslCertificate::verify()

There is now a method to verify a certificate chains validity. This means that you can check a certificate chain against the root store directly.

<li>QSslCertificate::extensions()

X.509 certificates can contain extensions (and almost all do). Qt previously only supported the subject alternative name extension. In Qt 5 this new method returns a list of all extensions, and does its best to convert them into a sensible structure. Some extensions such as basicConstraints and subjectKeyIdentifier (for example) are supported to the extent that the structure is defined. Other extensions can also be accessed, though the structure of the information returned may change between versions.
<p>
This change isn't massively useful in itself, but it provides a foundation for future improvements.

<li>QSslSocket can now pause on ssl errors

Currently, a nested event loop is required in order to process the sslErrors signal from QSslSocket. In Qt 5, you can use the setPauseMode() method of QAbstractSocket to tell the socket to pause when the error signal is emitted. This means that no data will be transmitted until you tell the socket to continue, allowing the nested event loop to be avoided.
<p>
It is intended to extend this facility to cover authentication requests too. In future, you will probably also be able to request that the socket be paused at the end of the handshake even if there were no errors so you can perform future checks. Unfortunately time constraints meant that only the first step described above was completed (and that QNetworkAccessManager does not make use of this facility).

<li>QSslConfiguration::setSslOption()

In Qt5 you can enable and disable various bug workarounds etc. using this method. This change was backported to 4.8.

<li>New QT_NO_SSL define

In older versions of Qt you could make code conditional on SSL support being available using #ifdef QT_NO_OPENSSL, but this is tied to the openssl backend. In order to allow for additional backends in the future such as one using GnuTLS there is now a QT_NO_SSL define too. This means that unless you actually depend on the openssl backend (eg. because you are using the native handles to perform additional openssl calls yourself) you should use QT_NO_SSL.

<li>Support for opaque QSslKeys

Qt 5 adds support for a new type of QSslKey that is 'opaque' this new type can be used to build things like PKCS#11 support into code using Qt. An example of this is at http://git.iksaif.net/?p=qsslkey-p11.git;a=tree
<p>
In addition to these new features, there have also been lots of bug fixes etc. too, but you can see those in the bug tracker.
<!--break-->
