---
title:   "Yet another CMake Blog"
date:    2006-08-06
authors:
  - rich
slug:    yet-another-cmake-blog
---
Since everyone else is blogging on build systems today, I thought I'd join in. Overall CMake seems to be working out ok, but like anything else, it is not without its problems. One problem I had with it was fixed in the new release this weekend though - you no longer need to write:
<pre>
IF something
  do stuff
ELSE something
  do other stuff
ENDIF something
</pre>
I don't understand why it was written like that in the first place, but hey it's fixed so lets move on.

The other issue I have is more serious - the command line usage is frankly awful. Accordingly I'd like to know what people would like to see in a 'configure' wrapper script that lets you invoke cmake without -DSOME_VERY_OBSCURE_COMMAND in order to something as simple as tell it where to install. The things that I think need to be in there so far are:
<ul>
<li>--prefix
<li>--debug
<li>--qt-dir
<li>--kde-dir
<li>An option for passing raw cmake options for stuff not covered by the wrapper.
</ul>
Anything else that should be provided?
<!--break-->