---
title:   "Akademy 2008"
date:    2008-08-14
authors:
  - rich
slug:    akademy-2008
---
As you've seen from all the stores on the <a
href="http://dot.kde.org/">dot</a> and on <a
href="http://planetkde.org/">planetkde</a> this week has seen all the KDE
developers gathering in Mechelen in Belgium. The organisation this year has
been excellent - even the network (usually the achilles heel of KDE
conferences) has worked from day one.

As well as the usual socialising, talks and BoF sessions there has also been a
lot of coding. I've focussed on ksnapshot and plasma, KSnapshot gained a few
minor changes:

<ul>
<li>KSnapshot will now record the window title in the image if you've chosen
to capture the window decorations. Hopefully this should be useful to indexers
like strigi.
<li>I've fixed the 'not saving settings' bug - it looks like kdelibs changed
behaviour.
<li>I've increased the JPEG quality setting a little so screen shots look nicer.
<li>Tackat has persuaded me to look into writing a simple post processing app
so that you can easily apply effects like screenie before you upload your pics
to the dot.
</ul>

The work on plasma was again to do with scripting, I first of all got the code
that was broken by the API changes fixed - this meant that the spinning
squares demo now works again. I then got the scripting support for the new
widget api working. You can see this in action in  the screenshot below which
shows a calculator written in javascript using the plama widgets next to the
current C++ calculator.

<img src="http://xmelegance.org/devel/plasma-calculators.png">

This isn't quite ready to commit because the actual calculator logic is a bit
broken, but it shows that the widget support is nearly ready. In order to make
it easy to test this, I finished the implementation of package uninstalling in
plasma libs so that you can now use the plasmapkg tool to easily install  and
remove scripted plasmoids.

I've done a little work on a few other things, but they're not quite ready
yet, so I'll save them for another post.
<!--break-->
