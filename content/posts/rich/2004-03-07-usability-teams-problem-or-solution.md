---
title:   "Usability Teams - A Problem or a Solution"
date:    2004-03-07
authors:
  - rich
slug:    usability-teams-problem-or-solution
---
Aaron has posted a blog entry about developes 'fearing' usability teams, I've replied to his post itself by a comment but I'd like to provide a fuller view of things here.

I think that having a separate 'usability team' is an unworkable concept in an open source project, what we need is a way to make the usability guys part of the development team. Sure, there will be discussions on a usuability list about the issues the usability guys are working on, but the primary communication about any usability driven changes should be with the people who are writing or maintaining the application.

Imagine first:

1. Developer announces on the devel lists that he's working on an app and puts it in kdenonbeta.
2. Usability guy offers to help by designing the dialogs for the app.
3. Between developer and usability guy the app takes shape.
4. Usability guy reviews and tidies up the main application for usability issues. He's also been involved in the apps development so he's hopefully been able to ensure there are no major problems already.

Or alternatively:

1. Developer has a long standing app.
2. The usability team discussess the app and decides on a way to improve it.
3. Usability guy codes up the results of the discussion and changes the application.

In the latter siutation the maintainer has now got to deal with the fact that any changes he has pending are probably invalidated eg. the because the new dialogs don't support the right options, he's also had someone he's never heard of modifying his application.

My point: Usability teams and development teams need to work side-by-side - if they don't things will get nasty.

