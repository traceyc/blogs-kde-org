---
title:   "Ready for UK Expo"
date:    2004-04-19
authors:
  - rich
slug:    ready-uk-expo
---
Well, I'm all set for the UK expo now. Hopefully I'll see some of you there. I'm only going to be around on the second day, but the booth will be manned on both. See you there!
