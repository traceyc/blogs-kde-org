---
title:   "Moved to kdedevelopers.org"
date:    2009-07-02
authors:
  - lvillani
slug:    moved-kdedevelopersorg
---
I have moved my blog to kdedevelopers.org. The previous blog was self-hosted (at home) but I simply don't have enough bandwidth to host it (that box will be used to serve uber-lightweight static pages and a koji instance only).

And since I've been mostly blogging about Qt and KDE in the past, I think that this is the best place to host my blog.

See ya!