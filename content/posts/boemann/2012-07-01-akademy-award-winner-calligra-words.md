---
title:   "Akademy Award winner Calligra Words"
date:    2012-07-01
authors:
  - boemann
slug:    akademy-award-winner-calligra-words
---
It was with great joy I received the Akademy award for Calligra Words today. The acknowledgement is much appreciated, and is for sure a great motivator.

I may have been the one accepting this award, but it is just as much an award for the entire Calligra Suite and an recognition of the entire team of developers, artists, bugreporters, testers, translators, sponsors. Calligra would not be what it is today without the effort of every single one of you. Out of fear of missing someone I'm not going to list specific names. If you have done something for Calligra then this award is for you too.

When I held a presentation about Calligra in Brussels a couple of months ago, I said that one of the things we pride ourselves of is that we are inclusive of everyone. We want users to feel as contributers as well. Be it reporting bugs, or writing a sentence or two for the manual. Every contribution counts and is valued.

But we are also aware that we are not done yet. I hope this will encourage everyone to do even more, or for new contributers to join. Because in Brussels I also said, without shame or doubt, to people from the likes of IBM, Microsoft and governments, that in Calligra we have a passion, a passion to be the best application suite in the world.

Now let us all go make that happen, and show them what KDE is capable of.

Thank you!
<!--break-->
