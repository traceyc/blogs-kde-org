---
title:   "Using KWord as a .doc  viewer"
date:    2009-09-18
authors:
  - boemann
slug:    using-kword-doc-viewer
---
I've been improving the .doc import of KWord over the last couple of months (besides working on <a href="http://blogs.kde.org/node/4048">tables</a>)

I must say it has been quite a dramatic improvement. From having each paragraph being loaded onto each own page with the formatting all wrong, no tables, page size wrong, and many other bugs all over, I'm pleased to say that I'll this weekend give the .doc import a real world test, as I'll be conducting a 2,5 hour lecture using KWord to go through a 40 page document with tables, images inside tables, pagebreaks and lots of formatting.

I'm quite excited as this will be the first indication to me that KWord is getting ready for real use. We are not there yet for everyone, but I certainly intend to be using KWord as more than a .doc viewer in the future. That future still requires some hard work which I'm glad to be doing as my full time day job employed by <a href="http://www.kogmbh.com">KO GmbH</a>

<!--break-->