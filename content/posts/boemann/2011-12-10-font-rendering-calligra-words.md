---
title:   "Font rendering in Calligra Words"
date:    2011-12-10
authors:
  - boemann
slug:    font-rendering-calligra-words
---
I've just committed a fix to the Calligra Engine, so that font rendering will be without hinting when compiled against Qt 4.8.

And here are the obligatory screenshots to prove it (thanks buscher). Both are with system wide hinting turned on.

First on Qt 4.7:
<img src="http://www.abload.de/img/fonttest-2-3-84-more5g0cf.png">

And here on Qt 4.8:
<img src="http://www.abload.de/img/fonttest-2-3-85-moreaz5zn.png">

Big thanks to Eskil Abrahamsen Blomfeldt at Nokia for implementing it in Qt.
<!--break-->
