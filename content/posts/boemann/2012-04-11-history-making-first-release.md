---
title:   "history making first release of"
date:    2012-04-11
authors:
  - boemann
slug:    history-making-first-release
---
<img src="http://www.calligra.org/wp-content/uploads/2012/04/calligra-logo-transparent-for-light-600.png" width="600" height="425" </img>

After about 2 years of development and 1½ year since we officially formed the <a href="http://calligra.org">Calligra</a> project I'm happy that Calligra 2.4 is being released later today.

In my vision Calligra becomes a ground breaking fresh alternative to the established free and proprietary players. Calligra should create history and revolutionize it. We are not there yet, but we have great plans and are very much still evolving.

We don't believe that applications have to chose between being powerful and complex, or being dumbded down to make them easy to use. In the Calligra Words team we have this crede "powerfeatures made easy" and we are committed to making this come true by working hard on the usability as well as adding powerfeatures. And we don't want to just add configuration options. We want to create our interfaces in a way that puts all the power at your fingertips in the most intuitive way.

We know we are not there yet. As I said it's early days yet, but we have already made some new first-of-a-kind usability improvements for the next version (2.5). Let me share an example with you. In other wordprocessors when you drag the borders of a table you see a line that shows you where the border will end up. In Caligra Words 2.5 not only do you instantly see the text being relayouted, but you also get helpers that show you the width of the columns or height of rows as you manipulate them. Additionally, if you press shift while dragging a border it changes the way it moves the following column(s). Obviously using the shift key for extended functionality is not an original idea, but coupled with the innovative helper (note how it even suggests to use the shift key) this makes this powerfeature easy reachable and understandable for the avarage user.

<a href="http://player.vimeo.com/video/40126803"> <img align="center" src="http://wstaw.org/m/2012/04/11/vlcsnap-2012-04-11-03h00m21s171.png" width="320" height="200"</img>Click for video</a>

This underlines what I said of us wanting to be more than just another player on the market. We want to create history. We want to innovate and think out of the box. But before we could do that we had to create a solid baseline, and that is what 2.4 represents.

So for 2.4 we concentrated on becoming a solid document viewer so you would have no need to look elsewhere. And also to fulfil your daily editing needs, with quite a number of advanced features also already working.

It thus brings us great joy that early adopters of our betas and release candidates are saying that we rock and find us really usable. That is so gratifying to hear. Thank you!

And to top it off we wil begin to release new versions every 4 months or so.
<!--break-->
