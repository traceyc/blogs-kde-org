---
title:   "A selection of krita stuff"
date:    2007-05-25
authors:
  - boemann
slug:    selection-krita-stuff
---
For Google Summer of Code Sven Langkamp was assigned the project of adding different kind of selection visualisations for Krita 2.0. Being his mentor is very easy as already now before gSoc has even started officially he has completed 2 out of 3 items in the project description. Krita 2.0 is now capable of showing selections as:

* Marching ants
* Overlay (as known from Krita 1.6)

Marching ants involved analysing the bitmap of the selection and extracting a form of path from it. Pretty wellknown algorithm, but Sven nevertheless had to adapt it to the render pipeline of krita. And he had to make some optimisations as well. For the overlay visualisation he could reuse some code from 1.6 but he still had to change it to fit with the new Krita 2.0 display pipeline.

Additionally he has worked on the selection tools as well.

For the remaining 3 months of Google Summer of Code he will be focusing on the last item which  is to be able to modify the selection as if it were a true path. This might turn out to be a somewhat larger task, but we have the opportunity to reuse Emanuele Tamponi's gSoC work from last year.

Thanks, Google for hiring Sven :)
<!--break-->