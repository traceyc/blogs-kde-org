---
title:   "Calligra Words"
date:    2011-12-07
authors:
  - boemann
slug:    calligra-words
---
Allow me to introduce the amazing new word processor that will be released with Calligra Suite 2.4 in 2012. It is fast and nible to use, and using the powerful Calligra Engine we have made sure it can load .doc and .docx and ODF documents. And with so astonishing quality, that it will be your favourite for viewing such documents.

For writing and editing we are developing an efficient and innovative user interface. It has a lightweight menu bar with only the basic File, Edit, View, Settings and Help menus. Everything else is manipulated through the sidebar.

And by choosing ODF as our native loading and saving format, we have ensured that your documents can be exchanged easily with other users. ODF is chosen by many governments and users around the world.

Our aim is to become the word processor of choice for everyone. In the following releases we will gradually expand and improve our user interface so you can insert and manipulate more of the fantastic features hidden in our engine. But even in the first release you can add and manipulate many of the basic document elements like tables and lists plus more special elements like footnotes, endnotes, autoupdating table of contents, graphics (the text flows around it) and even a bibliography.

We are still in beta phase of development which means we fix bugs and improve the stability, but we are no longer adding new features for the first release. We are regularly releasing beta snapshots and would like to invite you to thoroughly test and report any issues you find. Especially in the areas of editing and saving. This way you can be part of making Calligra Words the best it can be when we finally release.

<img width="500" src="http://wstaw.org/m/2011/12/07/spring.png">
<!--break-->
