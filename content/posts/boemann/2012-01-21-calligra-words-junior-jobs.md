---
title:   "Calligra Words junior jobs"
date:    2012-01-21
authors:
  - boemann
slug:    calligra-words-junior-jobs
---
I often get asked about junior jobs in Calligra Words. I ususally say no, but now I actually have some.

The reason I say no is that there are no easy jobs left, and so jobs would take much more of my time than I would like. And I also don't want to purposely leave unfinished stuff behind.

Anyway here is a brief synopsis of the junior job: We have code that applies bold, italic etc, and it works, except it doesn't respect table cell protection. The job consists of repeating an example I've already made to the application of bold, italic and several other similar things.

It's pretty easy copy,paste,edit and will give you practical experience with the <a href="http://en.wikipedia.org/wiki/Visitor_pattern">visitor pattern</a>
<!--break-->
