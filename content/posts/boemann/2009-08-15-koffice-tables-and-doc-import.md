---
title:   "KOffice tables and .doc import"
date:    2009-08-15
authors:
  - boemann
slug:    koffice-tables-and-doc-import
---
So Elvis Stansvik and I have been working on table support for KWord. And simultaneously I've been working on improving the MS .doc import filter for KWord. I've improved the generally font and paragraph import enormously, but in this blog I'll focus on the table import.

First I'll show you the original MSWord table:

<img src="https://blogs.kde.org/files/images/tablescomparison1.png">

And then how it looks in OOWriter 3.0:

<img src="https://blogs.kde.org/files/images/tablescomparison2.png">

And in AbiWord:
<img src="https://blogs.kde.org/files/images/tablescomparison3.png">

And then finally in KWord:

<img src="https://blogs.kde.org/files/images/tablescomparison4.png">

I think you can see how good our support already is, and that we already beat OpenOffice. Obviously there are some glitches in the corners but I'm fairly sure those problems can at the very least be minimized.

I also still miss to load the tablecell padding. If you look closely you can see the cells are not completely the correct size yet.

So stay tuned as this will all come in KOffice 2.1 which has just entered freeze and will be released in a couple of months. Due to the freeze the table support in 2.1 will however only be as a viewer basically. Ok you will be able to enter text into cells and even create a table, but you will not be able to add or remove columns or cells or change column width etc. These user interface things are scheduled for 2.2.

<!--break-->