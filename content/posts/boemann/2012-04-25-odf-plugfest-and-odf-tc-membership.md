---
title:   "ODF plugfest and ODF TC membership"
date:    2012-04-25
authors:
  - boemann
slug:    odf-plugfest-and-odf-tc-membership
---
So Thursday and Friday last week I was in Brussels for an <a href="http://en.wikipedia.org/wiki/OpenDocument">ODF</a> plugfest representing <a href="http://www.kogmbh.com">KO GmbH</a> and the <a href="http://www.calligra.org">Calligra</a> project. The <a href="http://www.odfplugfest.org/">plugfest</a> is some kind of a mix between interacting with other ODF implementors (like Microsoft, IBM, LibreOffice etc), and a publicity event to engage the government and industry in the country where the plugfest is held.

So besides trying various interoperability scenarios, I also made a presentation about Calligra which was well received. Jos van den Oever also from KO GmbH made a presentation on <a href="http://www.webodf.org">WebOdf</a> (a browser/javascript based editor). And Thorsten Zachmann from Nokia also represents Calligra, so all in all we have quite some impact.

Now back home and fast forward to Monday I got asked to join the <a href="http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office">ODF technical committee at OASIS</a>. Jos van den Oever is already a member on behalf of KDE, and so he got me in (also for KDE). The initial purpose is to make recomendations and alterations for the next generation of change tracking in the ODF file format. I am working together with two other members of the TC, but, even though I'm not alone, I really feel the personal responsibility to make something good, as billions of people can potentially be affected.

But I've joined the real Technical Committee so I will be joining the weekly phone conferences and have my say on all matters ODF. Now, I will go slowly at first, getting acquainted with the processes and how best to get your point of view across. Luckily Thorsten and Jos will be able to help me with any insider knowledge I need.

So now with 3 TC members KDE is influencing the world of ODF even more than ever.
<!--break-->
