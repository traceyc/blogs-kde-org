---
title:   "A curve widget anyone?"
date:    2007-03-20
authors:
  - boemann
slug:    curve-widget-anyone
---
[image:2731 height=200]

I'm trying to create some interest in this curve widget for a wider audience. For almost two years now Krita have had it, though it originates from digiKam where it adjusts colors. I took the original code and pretty much rewrote it to be more generic. And that was a good call, because a year later Bart Coppens used it for something completely different namely the pressure sensitivity of  tablets.

The widget is just the white area, the curve and the control points. Any outside stuff like scales on the axes you would need to add yourself. It does however allow you to add a background image, which we in Krita use to display a histogram when used to adjust colors.

With KDE4 getting nearer a freeze I would like to hear how many others would find this widget useful and if we should try to get it somewhere central for more people to use.

If you have ideas to possible uses please leave a comment.