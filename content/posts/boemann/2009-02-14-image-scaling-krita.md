---
title:   "Image scaling in Krita"
date:    2009-02-14
authors:
  - boemann
slug:    image-scaling-krita
---
This weekend have so far resulted in 5 commits on Krita. In particular on the scaling stuff. Krita has for way too long had some outstanding quality issues with scaling. The edge pixels were transparent and the images themselves were too blurry.

Just see this page from over 2 years ago: http://gis-blog.leonde.de/index.php/2006/11/10/scaling-algorithms-in-krita/

Well this weekend was the day to end the misery. I started with implementing bicubic filtering which removed the blur, and went on to fix the transparent edges. The main part of the bug was actually a simplerounding error, but looking into the issue i also found that the filter weights were being applied wrongly, so I fixed that too.

But that was not all. The code for mixing of colors couldn't handle negative weights which gave some weird colors popping up weird places in the images. After some debugging all it took to fix it was clamping the resulting rgb values to not be negative. 

After that I fixed some regressions regarding progress feedback while scaling, and right now I've bugged Sven Langkamp to help me with the selection stuff in the transform tool. Funny thing is I was his mentor on a GSoC about selections two years ago and now he is the one who helps me :)

Anyway here are the results so judge for yourself (I've used the same original as in the blog I referred to above and bicubic filters in all cases).

Photoshop 7:
<img  src=https://blogs.kde.org/files/images/sealion-ps7.png>

Gimp 2.6.1:
<img  src=https://blogs.kde.org/files/images/sealion-gimp-cubic.png>

Krita (trunk):
<img  src=https://blogs.kde.org/files/images/sealion-krita-bicubic.png>

I'm quite pleased as it looks very similar to photoshop.
<!--break-->