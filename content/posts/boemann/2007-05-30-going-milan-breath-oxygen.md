---
title:   "Going to Milan for a breath of Oxygen"
date:    2007-05-30
authors:
  - boemann
slug:    going-milan-breath-oxygen
---
On Friday I'll fly to Milan for the upcoming Oxygen coding sprint. The objective is to get the Oxygen style into shape so it can be imported into kdebase. There is a lot of work for us to do, but Nuno and David are there for the artistic side and Riccardo (ruphy) and I will be coding on the spot. In addition Thomas will try to be online as much as possible from his home.

Ruphy have been kind enough to offer his house for a place to work and there is a nice cheap hotel down the corner where we'll spend the 3 nights. Apart from the excitement of getting Oxygen style ready, I'm also looking forward to some nice Italian food and icecream

We'll be sure to blog during the event
<!--break-->