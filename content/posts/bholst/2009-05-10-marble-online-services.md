---
title:   "Marble online services"
date:    2009-05-10
authors:
  - bholst
slug:    marble-online-services
---
During the last weeks I introduces Marbles AbstractDataPlugin classes. These make it possible to generate a full featured Marble plugin showing for example photos on the globe with several hundered lines of code.
<a target="_BLANK" href="http://oculusaquilae.de/gsoc/blog/paris_with_flickr.png"><img align="right" src="http://oculusaquilae.de/gsoc/blog/paris_with_flickr_small.png"></a>In KDE 4.3 beta 1 is a photo and a wikipedia plugin. On clicking on the little items on the globe, Marble will open a browser showing the corresponding flickr site. The wikipedia plugin may still have some issues, so it probably won't work, but this is already fixed in trunk.

If you have more ideas for plugins don't hesitate to post them.
A weather plugin will be part of my this years summer of code, but weather is another problem as the data is not so easily available with reverse geocoding.