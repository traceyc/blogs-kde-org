---
title:   "Releasing software is almost fun"
date:    2008-11-15
authors:
  - cornelius schumacher
slug:    releasing-software-almost-fun
---
I'm developing and maintaining a small application called <a href="http://www.lst.de/~cs/plutimikation">Plutimikation</a> for my daughters. It's a math learning game for children. Today I did a release of the current state as Plutimikation 0.2. It's the second release. The first release was four years ago. It's interesting how tools and infrastructure have evolved since then. Today releasing software is almost fun.

Four years ago I hosted the code in a private Subversion repository and manually built a package for SuSE Linux 9.1, which was the current SUSE version back then.

Today I host the code on <a href="http://github.com/cornelius/plutimikation/">Github</a>, and the <a href="http://build.opensuse.org">openSUSE Build Service</a> builds packages for openSUSE 10.2, 10.3, 11.0 and Factory and for SLED. It's amazing how much more easy it has become to develop free software openly and distribute it to the world.

There are some small pieces still missing, though. Getting the source code from github to the build service still involves some manual work. I would prefer to just type in the URL of the git repository in the build service and let the rest be handled automatically. It's just a little bit of code, which is missing here and it could be added easily as the build service is free software. Would be a nice project for a cold winter evening.

The other missing bit is the distribution of the software. SUSE has the fabulous one-click-install mechanism, but apparently it requires some work to create a one-click button I could put on the homepage for Plutimikation. I see no reason why the build service couldn't provide the HTML for such a button automatically. This would probably mean dynamic creation of the ymp file which is used by the one-click-install mechanism and a suitable graphics. Another nice project for cold evenings.

So even, if for perfection there are still some bits and pieces missing, the tool chain for maintaining and releasing software has become incredibly great.

One thing hasn't changed in the four years. I'm still using the KDE 3 platform for Plutimikation. It provides everything what's needed to develop this kind of small application in no time, including stuff like translations. There is not much to be wished for here. That really tells about the quality of the KDE platform.

But the next version of Plutimikation will use <a href="http://techbase.kde.org/Welcome_to_KDE_TechBase">KDE 4</a>, so it also gets all the beauty, portability and functionality goodness which other apps already enjoy for a while. I'm looking forward to Plutimikation 0.3.
<!--break-->