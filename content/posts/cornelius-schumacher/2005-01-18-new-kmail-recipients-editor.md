---
title:   "New KMail Recipients Editor"
date:    2005-01-18
authors:
  - cornelius schumacher
slug:    new-kmail-recipients-editor
---
Over Christmas I wrote a new recipients editor for the KMail composer:

[image:808]
<!--break-->
The old one was really annoying because of two big problems:

First, when having multiple recipients the line edit used for the To/CC/BCC fields wasn't really suitable because it only showed a part of them and when trying to get an overview about who gets the mail, you had to scroll around a lot with the cursor keys.

Second, the dialog for selecting recipients from the addressbook was just too clumsy. The semantics of moving around contacts between two hierarchical lists were too complex and not consistently implemented and by having so many control elements in the dialog the space for the actual data was too limited, so that names and email addresses usually were truncated and only fully viewable by lots of scrolling.

The new recipients editor addresses these problems. It uses one line per recipient and provides a simple one-column contact picker dialog. This makes it much more easy to get an overview of who gets a mail and select recipients from the addressbook.

I have now used the new editor for a while and am quite satisfied. It works very well and eliminates some of the last problems of KMail which really annoyed me.

If you want to try it yourself, you need KMail from HEAD and have to put the line <tt>RecipientsEditorType=1</tt> into the <tt>Composer</tt> section of <tt>kmailrc</tt>.
