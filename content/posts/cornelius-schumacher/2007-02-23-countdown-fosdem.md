---
title:   "Countdown to FOSDEM"
date:    2007-02-23
authors:
  - cornelius schumacher
slug:    countdown-fosdem
---
Only a few hours left until FOSDEM starts. I'm about to leave for the airport to fly to Brussels together with the rest of the SUSE crew. There will be an <a href="http://www.fosdem.org/2007/schedule/devroom/opensuse">openSUSE developer room</a> and I will be speaking on Saturday afternoon about <a href="http://www.fosdem.org/2007/schedule/events/opensuse_packaging_made_easy">"Packging made easy"</a>. The intention is to present some of the tools we have created with the <a href="http://build.opensuse.org">openSUSE Build Service</a> which make packaging of software much more fun than it was before.

I'm looking forward to meet some of you at Brussels. Let's have some fun.
<!--break-->