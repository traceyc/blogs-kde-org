---
title:   "Hack Week"
date:    2007-06-25
authors:
  - cornelius schumacher
slug:    hack-week
---
This week is <a href="http://lists.opensuse.org/opensuse-announce/2007-06/msg00009.html">Hack Week</a> at Novell. All the Linux engineers are hacking along on their favorite projects. There is an incredibly long list of great ideas on <a href="http://idea.opensuse.org">idea.opensuse.org</a>.

I'm working on some kind of <a href="http://idea.opensuse.org/content/ideas/dynamic-web-service-portal">fancy aggregating portal</a>. I'm not completely sure yet, where this will lead to, but I have already created a sophisticated design document and made a mockup.

[image:2848 size=original hspace=100]

As aggregrating of blog feeds is part of the concept, I created a dedicated <a href="http://hackweek.blogspot.com">Hack Week Blog</a>. I will document progress there and use it as test data source at the same time.

This will be a fun week :-)
<!--break-->
