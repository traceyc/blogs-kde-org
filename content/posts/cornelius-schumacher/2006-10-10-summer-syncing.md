---
title:   "Summer Syncing"
date:    2006-10-10
authors:
  - cornelius schumacher
slug:    summer-syncing
---
This summer was a good summer for syncing. A couple of days ago the <a href="http://www.opensync.org">OpenSync</a> project released their <a href="http://www.opensync.org/wiki/opensync-0.19-notes">version 0.19</a> which now finally is able to sync the KDE desktop data also when using the KDE frontend KitchenSync. Previously this wasn't possible because the event loops of the KDE plugin and the frontend got in conflict, when running in the same process. With a new architecture which allows to run plugins in separate processes communicating with the sync engine via an IPC protocol, OpenSync solves this problem. This work was started at the <a href="http://blogs.kde.org/node/1744">OpenSync meeting in Amsterdam</a> earlier this year and now finally completed and released. Congratulations!

[image:2431 size=original hspace=80]

Part of the work of merging the code from the IPC branch back to the main code line was done by <a href="http://raisama.net">Eduardo Habkost</a> as part of his <a href="http://code.google.com/soc/kde/appinfo.html?csaid=932767063AAF7DF4">Summer of Code project</a>. He did a great job with the project. Read more in his <a href="http://raisama.net/diary/archive/2006/09/02/soc-summary">report</a>. Packages for various distributions are available through the <a href="http://build.opensuse.org/project/show?project=OpenSync">openSUSE Build Service</a> and can be downloaded from the <a href="http://software.opensuse.org/download/OpenSync/">openSUSE Software Server</a>. This also includes the current version of KitchenSync which fits to the OpenSync 0.19 release. Go and have a look. It's great stuff.
<!--break-->