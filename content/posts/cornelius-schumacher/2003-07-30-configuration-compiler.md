---
title:   "The configuration compiler"
date:    2003-07-30
authors:
  - cornelius schumacher
slug:    configuration-compiler
---
<a href="http://www.derkarl.org/cvscommits.phtml?id=47395">kdepim/libkdepim</a> - <i></i> <a href="http://www.derkarl.org/">(KDE CVS Commits)</a>
<br><br>
This is going to be something really cool. I now have an example configuration dialog which is automatically generated from an XML description of the configuration options. This includes an automatically generated API to the configurations options for convenient access by the application, it will support all the fancy config stuff like immutable entries etc., and it provides a configuration dialog to edit the options without needing to write any additional code.
<br><br>
The best thing: It will be incredibly simple for a developer to use this. Drop the XML description of the configuration options into the source code directory, add custom code, if necessary, and you are done. You will get the configuration dialog for your application for free.
<br><br>
It also fits very nicely into the Qt/KDE framework along the lines of XML-GUI and uic. cfgc (the configuration compiler) will be for configuration what uic is for user interface. Maybe it might even make sense to add a configuration designer to provide a GUI for editing the configuration descriptions. Let's see...