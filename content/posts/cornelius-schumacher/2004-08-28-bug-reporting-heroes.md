---
title:   "Bug Reporting Heroes"
date:    2004-08-28
authors:
  - cornelius schumacher
slug:    bug-reporting-heroes
---
One morning at the youth hostel during aKademy I had an interesting conversation with Eric Laffoon. He told me about an idea to give more credit to bug reporters. Right now there is a statistic for the most active bug closers, but for bug reporters there isn't such a thing. I think it would be very interesting to give people doing bug reports of high quality bug more credit for their contribution by introducing something like a list of the top ten bug reporters. Maybe this could give bug reporters an incentive to make better bug reports.

How would a top bug reporters list be calculated? The number of bug reports is a bad criteria because reports of low quality don't help the project. So maybe a combination of how many reports actually led to bug fixes weighted against invalid or duplicate reports would do the trick. We could also add the time between bug report and bug fix as a criteria recognizing the fact that focused report concentrating on problems which can be fixed with a limited amount of effort are more helpful than reports describing a very general problem which takes lots of work to fix. Developers mostly are aware of these kind of problems anyway. Another way to identify the top bug reporters would be to give developers the option to rank bug reports in a similar way like the Bugzilla voting mechanism.

It would be interesting to try if acknowledging the work of bug reporters in such a way would have an effect on the quality of the reports. Maybe it could even lead to some healthy competition between reporters to do the best reporting work. It probably also wouldn't very hard to implement. If I find some time I might try to do this, but I would be more than happy if somebody else would beat me to this.