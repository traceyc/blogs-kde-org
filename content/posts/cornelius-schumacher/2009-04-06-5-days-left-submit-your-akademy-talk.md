---
title:   "5 days left to submit your Akademy talk"
date:    2009-04-06
authors:
  - cornelius schumacher
slug:    5-days-left-submit-your-akademy-talk
---
There are still five days left to submit a proposal for a presentation at Akademy 2009. The deadline is on Friday, April 10th. Akademy happens as part of the <a href="http://www.grancanariadesktopsummit.org/node/74">Gran Canaria Desktop Summit</a> this year. See more details about what we are looking for in the <a href="http://www.grancanariadesktopsummit.org/node/74">call for presentations</a>. Akademy is the prime occasion for meeting the community, and present and discuss your ideas. Lots of great initiatives were kick-started at Akademy. Don't miss out on this opportunity and <a href="mailto:akademy-talks@kde.org">submit your proposal</a> now!

Personally I'm particularly interested three kinds of presentations:
<ul>
<li>First I would like to see presentations about the beauty of KDE. KDE 4 shines, and I would love to meet the people who make it shine at Akademy, and hear how they did it, and what we can expect in the future. That's not only limited to beautiful graphics. Elegant code, organic user interfaces, beautiful APIs, engines which enable a beautiful desktop, fluffy-bunny themed Chuck Norris, all this falls under this category.</li>
<li>Second I would like to hear about projects which go beyond KDE or which serve as a foundation for KDE. The state of X, cross-desktop infrastructure like Akonadi or other freedesktop.org projects, general interesting desktop computing concepts, all this would make great presentations for Akademy and the Gran Canaria Desktop Summit in general.</li>
<li>Third, and that actually might be the most important category, I would really love to have lots of application developers at Akademy, especially the people who stand behind the thousands of "third-party" applications which create the richness of the KDE experience. You might think that your application is not important enough for a big conference like Akademy, but that's not true. All the little tools, which were created to scratch a special itch, they make up for an essential part of the KDE software universe, and they should be represented at Akademy. This also includes all the nice Qt applications, which only use a small or no part of the KDE libraries. They share the same base with every other KDE application, and are a natural fit for the KDE community. So, if you are the author of one of the great applications on <a href="http://kde-apps.org">kde-apps.org</a>, don't think twice, <a href="mailto:akademy-talks@kde.org">submit your abstract</a>.</li>
</ul>

I'm looking forward to a special Akademy in a beautiful surrounding with lots of exciting content. Be part of it, <a href="mailto:akademy-team@kde.org">submit a talk</a>.
<!--break-->