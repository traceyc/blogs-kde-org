---
title:   "Passing the Torch"
date:    2004-08-27
authors:
  - cornelius schumacher
slug:    passing-torch
---
This week at aKademy I asked Reinhold if he would be willing to take over the maintainership of KOrganizer and he said yes. So now after five years of being the KOrganizer maintainer I pass the torch to Reinhold.

That doesn't mean that I will stop working on KOrganizer. I will continue to work on it as something like a co-maintainer, but my focus will shift a bit to other things like kdepim in general or the new kxml_compiler stuff I presented at the developer conference on Saturday. In fact it has already shifted since quite some time and now giving Reinhold the official title of KOrganizer maintainer only reflects the current reality.

I'm happy that we are in the comfortable situation of doing this maintainer change for a healthy project which has active contributors and where it turned out to be no problem to find a new maintainer. That's how Free Software development is supposed to be.

In addition to that I'm also happy about the progress kdepim made as a project and as a team as well. We had a kdepim meeting yesterday and it was amazing how easy it has become to find common ground and constructively work together into one direction. I really enjoy being part of this team.

As a sidenote, Tobias did an exceptional job in implementing threading in the Groupwise resource this evening so that it now doesn't block Kontact anymore
when loading data from the server. So it's now in a state where it really becomes usable. That's incredible given the fact that we started with writing the resource only last week. But with the right people and the right framework everything is possible. That's KDE.
