---
title:   "History"
date:    2007-01-14
authors:
  - cornelius schumacher
slug:    history
---
I was doing a bit of historical work today. While moving the content of the <a href="http://korganizer.kde.org">old KOrganizer home page</a> to the <a href="http://kontact.kde.org/korganizer">Kontact web site</a> I came across all kinds of interesting stuff of the past which I had completely forgotten. When I told Adriaan about the news entry on the KOrganizer web page from almost seven years ago that reported about me becoming KOrganizer maintainer, he quickly put on his SVN statistics guru hat and digged up some <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/361-Old-farts.html">interesting numbers</a>. Boy are we a bunch of oldtimers.

[image:2621 size=original hspace=100]

It's also interesting to have a look at the history of the Osnabrueck meeting:

<b>2003:</b> This somehow was the birth of the KDE PIM community. Kontact still was named Kaplan and we decided at the meeting to move KMail to kdepim. We discussed merging the "Kroupware" branch into the main branch which was the first version of the Kolab client.

<b>2004:</b> We came up with the brilliant idea to have a separate release of kdepim shortly after KDE 3.2. That was the plan. The result was that we just were ready with kdepim in time for KDE 3.3. The 2004 meeting also saw the first appearance of icecream, an exciting new tool for distributed compiling.

<b>2005:</b> This meeting was the first which saw roadmap planning for KDE 4. We also came up with some <a href="http://pim.kde.org/development/meetings/osnabrueck3/kitchensync.php">fantastic design documents</a> for a new KitchenSync interface.

<b>2006:</b> The birth of <a href="http://pim.kde.org/akonadi">Akonadi</a>. Lots of time was spend on discussing its design and the implications for KDE PIM. We also created the famous <a href="http://pim.kde.org/akonadi">Akonadi architecture diagram</a>.

<b>2007:</b> Akonadi becomes reality. There is serious hacking going on and it's becoming clearer and clearer where it will lead to. Newest ideas include integration of Strigi and Nepomuk-KDE. We are also talking about a KDE PIM Enterprise branch, which is supposed to be the base for those who need a really stable branch for enterprise use, for example the Kolab people. Half of us are almost Osnabrueck locals now.

One of the most exciting things about these past five years is that there has been a really strong community all the time. We feel and act as a team and have come an amazingly long way. Another nice aspect of the meetings is that we always had a number of fresh people there. So in spite of some of us being <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/361-Old-farts.html">old farts</a> KDE PIM meetings are continuing to be a stimulating experience. The air is still burning of ideas and productivity.

<!--break-->
