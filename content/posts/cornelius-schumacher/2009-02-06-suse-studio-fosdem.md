---
title:   "SUSE Studio at FOSDEM"
date:    2009-02-06
authors:
  - cornelius schumacher
slug:    suse-studio-fosdem
---
I'm doing my last preparations for <a href="http://www.fosdem.org">FOSDEM</a> right now. Together with the other SUSE guys I will go to Brussels later today.

On Saturday at 17:00 there will be a presentation about <a href="http://susestudio.com">SUSE Studio</a> in the <a href="http://en.opensuse.org/FOSDEM2009">openSUSE Track</a>. <a href="http://bornkessel.com/kesselblog/">Daniel Bornkessel</a> and me will show you how to create openSUSE based software appliances with just a few clicks. As special guests we will have <a href="http://www.wafaa.eu/">Andrew Wafaa</a> and <a href="http://jordimassaguerpla.blogspot.com/">Jordi Massaguer Pla</a> at the talk. They will tell a bit about what they have done with Studio so far.

We also will show Studio at the openSUSE booth and have some DVDs and USB sticks with us, so you can get your own customized openSUSE live media created there.

I'm looking forward to the event. See you in Brussels.
<!--break-->