---
title:   "KDE Wiki Meeting Report"
date:    2009-06-30
authors:
  - cornelius schumacher
slug:    kde-wiki-meeting-report
---
Two days of KDE Wiki Meeting are over. Danimo, Frank, Lydia, Dominik, Milian, Thorsten and me met in Berlin with the goal to get some more structure into the KDE Wikis and provide a plan for the future, where to put content. I'm happy to say that we accomplished this mission.

While we have <a href="http://techbase.kde.org">TechBase</a> for high quality technical documentation for a while, and the corresponding <a href="http://userbase.kde.org">UserBase</a> for end-user information since last year's Akademy, we were still missing a proper place for community content, especially for content which is mostly community internal, of more transient
nature, or just not finished yet. The idea to create a dedicated Wiki for this community content was floating around since a while, and now we created it at <a href="http://community.kde.org">community.kde.org</a>.

To make it clear which content belongs where, we created a mission statement, which gives clear guidance about which Wiki serves which purpose. You'll find it at wiki.kde.org in a few days. The basic idea is that <a href="http://userbase.kde.org">userbase.kde.org</a> provides end-user information, <a href="http://techbase.kde.org">techbase.kde.org</a> contains high-quality technical content for third party developers, distributors, and system administrators, while <a href="http://community.kde.org">community.kde.org</a> acts as a collaboration space for the community.

Actually community.kde.org already existed. It contained the charter of the community working group. But to keep things short and to the point we decided against creating another base, but go with the logical and short community.kde.org domain. The charter of the CWG will find a new home on the KDE e.V. web site.

With the creation of community.kde.org we can also shut down at least two places where community content ended up due to lack of a proper home. We'll shut down the old Wiki, which was available under wiki.kde.org, but whose content wasn't that well maintained, and which didn't fit too well in KDE's infrastructure because of technical reasons. We'll also move all the pages which piled up under the Projects directory on techbase, but in almost all cases didn't really belong there and also didn't match the quality requirements of being polished content targeted at technical people who aren't necessarily familiar with the community. Most of these pages find a proper home on community.kde.org, though.

In addition to the general cleanup and structuring we also worked on some improvements of the existing Mediawiki installation. Danimo replaced the OpenID login UI by a much more usable version, Milian finally managed to get rid of the annoying horizontal scrollbars inside the page on code samples, and we also discussed some more improvements, like the intensified use of templates and the introduction of a way to rate and classify documents on the Wiki to indicate their quality and make it more obvious what needs more work.

As a side track, we had an interesting discussion with some <a href="http://info.tikiwiki.org/tiki-index.php">Tiki</a> developers. They have an amazingly powerful and feature rich system, which would
be able to solve some of the problems, we still have with our Wikis, such as translation infrastructure. For now we decided for the sake of consistency and simplicity to stay with the current Mediawiki installation, but maybe Tiki is an option in the future.

Working on the Wikis was fun and satisfying because we got some concrete results, which will simplify maintaining KDE web content in the future. But besides all the work we also didn't forget to relax with a great dinner at a Chinese restaurant at Berlin-Adlershof, enjoying a great buffet, including cheese cake for dessert.

Thanks go to the KDE e.V. and Qt Software for supporting the meeting.
<!--break-->