---
title:   "An anniversary"
date:    2007-01-12
authors:
  - cornelius schumacher
slug:    anniversary
---
It's the fifth time that I'm at <a href="http://en.wikipedia.org/wiki/Osnabrück">Osnabrueck</a>, always for the same reason, the KDE PIM meeting we hold each January <a href="http://dot.kde.org/1043346607/">since 2003</a>. So now it's the fifth meeting and that's a small anniversary. As always the meeting is hosted by <a href="http://www.intevation.de/index.en.html">Intevation</a>. Many thanks to them for doing this for five years in a row. It always has been great.

I arrived at noon together with Will. We met Till and Frank in the train and Martin and Volker already were there when we got at the Intevation office. After a nice lunch at a bagel place we are now preparing for some serious hacking. Volker already has <a href="http://lists.kde.org/?l=kde-commits&m=116861235422107&w=2">committed</a> a complete Akonadi browser. In the mean time now also David is here, which is especially great, as he is one of the few long-term KDE PIM hackers, who the rest of us hasn't met in person before. We are still waiting for Tobias. When he is there we will officially kick-off the meeting. Some more people will arrive later today and tomorrow, so that we will have a nice group together tomorrow in the afternoon.

[image:2616 size=original hspace=80]

Main topics of the meeting will be Akonadi and KDE PIM 4. We will document what we are doing on the <a href="http://pim.kde.org/development/meetings/osnabrueck5/">KDE PIM web site</a> and you will get some of the results on the <a href="https://mail.kde.org/mailman/listinfo/kde-pim">kde-pim mailing list</a> and of course in <a href="http://websvn.kde.org/trunk/KDE/kdepim/">SVN</a>.
<!--break-->