---
title:   "More color for Icecream"
date:    2004-08-25
authors:
  - cornelius schumacher
slug:    more-color-icecream
---
Here at <a href="http://conference2004.kde.org">aKademy</a> <a href="http://wiki.kde.org/tiki-index.php?page=icecream">Icecream</a> is an essential tool. Compiling is just so much more fun when it takes no time. Today I finally managed to find the time do to some coding and implemented the long-outstanding feature of properly colored job halos in the star view. <a href="http://blogs.kde.org/node/view/583">Take a look</a>, update to the <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/icecream/icecream">latest version</a> and enjoy the hypnotizing effect of watching colored bubbles.
