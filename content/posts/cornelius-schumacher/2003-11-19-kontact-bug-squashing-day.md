---
title:   "Kontact Bug Squashing Day"
date:    2003-11-19
authors:
  - cornelius schumacher
slug:    kontact-bug-squashing-day
---
On Sunday we held the first Kontact Bug Squashing Day. A couple of core developers met on IRC and tried to fix some Kontact bugs. We started with 431 open bug reports (Summing up the reports of kontact, kaddressbook, kmail, knode, knotes and korganizer) and ended with 419 open bug reports. This doesn't sound too impressive, but we were able to address some of the remaining major problems, so all in all it was a success and not to forget it also was fun.

Ingo created a patch fixing the syncing problems of the KMail summary view, Tobias and me brought back the configuration dialogs of the newsticker and weather plugins and KOrganizer now doesn't have any major or more severe bugs open. In addition to that we fixed a couple of smaller things and did some cleaning up of the bug reports.

The general feeling was that we should do it again, so look out for the second Kontact Bug Squashing Day. It might be even more fun if some more people would join us on IRC.