---
title:   "Planet KDE"
date:    2004-06-13
authors:
  - cornelius schumacher
slug:    planet-kde
---
I just discovered <a href="http://planetkde.org">Planet KDE</a> and immediately got addicted. <a href="http://planet.gnome.org">Planet GNOME</a> was one of my favorite readings on the web the last months, but I think I have to switch now. ;-)

It's great to see that there is a Planet KDE, but why didn't this information come through earlier? Well, the dot is down. The other question is why the blogs from kdedevelopers.org aren't on the planet. Clee blames Ian. So Ian, stop fooling around with Windows programs and fix the feeds, please.

Recompiling kdepim again. It's not that much fun without proper icecream support, but it leaves me some time to write a blog entry, so that's not entirely bad. The annyoing thing about writing blogs is the clumsy web interface of kdedevelopers.org, so earlier today I tried to get an overview about blog tools. The only result was that I got completely lost. I hoped for the one and only tool which perfectly fits all my needs. What I got was a collection of strange scripts I don't understand and some unfinished apps. So what do people actually use to write their blogs?
