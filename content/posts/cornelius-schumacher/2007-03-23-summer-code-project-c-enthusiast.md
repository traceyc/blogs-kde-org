---
title:   "A Summer of Code project for a C++ enthusiast"
date:    2007-03-23
authors:
  - cornelius schumacher
slug:    summer-code-project-c-enthusiast
---
I just added a proposal for an interesting project to <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Ideas">KDE's Summer of Code Ideas page</a>. It's about <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Ideas#kxml_compiler">kxml_compiler</a>, a tool to automatically generate C++ code for parsing XML data from XML schemas. There is some existing code, but it only barely works. Improving this code to become useful for a wide range of schemas and applications would be a great project. The proposed XML based KOrganier holiday description format provides a nice benchmark for this. The code is used in KDE, but it's pretty much self-contained, so working on it will mainly require solid C++ knowledge. kxml_compiler is part of the <a href="http://rechner.lst.de/~cs/kode/index.html">Kode suite</a>.

For a student who likes a challenge and enjoys the mind-boggling experience of writing code that writes code, kxml_compiler would be a great project. Don't hesitate, <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Participation">apply now!</a>
<!--break-->
