---
title:   "Akonadi Clock"
date:    2008-08-28
authors:
  - cornelius schumacher
slug:    akonadi-clock
---
While browsing through <a href="http://www.kde-look.org">kde-look.org</a> I found a <a href="http://www.kde-look.org/content/show.php?content=82071">cool idea for visualizing a daily agenda</a>.

<img src="https://blogs.kde.org/files/images/akonadi-clock.png"/>

This reminds me of the Akonadi architecture diagram and I even have <a href="http://rechner.lst.de/~cs/archibald/">code</a> (probably not up to date) for drawing this kind of diagrams. So I guess it would be doable without too much effort to implement a daily agenda viewer like this. Would be a fun project.
<!--break-->