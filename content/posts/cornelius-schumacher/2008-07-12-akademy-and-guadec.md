---
title:   "Akademy and GUADEC"
date:    2008-07-12
authors:
  - cornelius schumacher
slug:    akademy-and-guadec
---
It has been a tough decision, because we had three awesome proposals, but after a period of getting feedback from the community and some conversations between the boards of the <a href="http://ev.kde.org">KDE e.V.</a> and the <a href="http://foundation.gnome.org/">GNOME Foudation</a> we have settled on the <a href="http://grancanariadesktopmeeting.eslic.es/index.php/GUADEC_Akademy_english">Gran Canaria bid</a> for holding Akademy and GUADEC as co-hosted event in 2009.

This event will be something new and it will be big in many ways. If you read about <a href="http://2008.guadec.org">this year's GUADEC</a> which is happening in Istanbul right now and see the anticipation of <a href="http://akademy2008.kde.org">Akademy in Belgium</a> on Planet KDE you get a glimpse of what energy will come together at the co-hosted event next year. We have a chance to put the Free Desktop on a whole new level by exploring areas of collaboration between the two big free software desktops in yet unknown depth and showing the world that the free software ideals which unite us are more important than the competition which sometimes is between our projects.

I'm happy that we have a strong local organizing team behind the event, and I'm looking forward to work with them and the GNOME people to make Akademy 2009 an outstanding event for both our communities and everybody else who wants to take part in the future of the free desktop.
<!--break-->