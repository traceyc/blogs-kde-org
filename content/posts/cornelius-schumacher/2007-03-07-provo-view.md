---
title:   "Provo View"
date:    2007-03-07
authors:
  - cornelius schumacher
slug:    provo-view
---
<b>Provo</b>

I'm at the <a href="http://en.wikipedia.org/wiki/Novell">Novell</a> office in <a href="http://en.wikipedia.org/wiki/Provo,_Utah">Provo</a> this week. We are having some pretty productive meetings here. The conference room we use is impressive. It's on the fifth floor and has an amazing view on the mountains. I like working this way.

[image:2709 size=original hspace=100]


<b>Summer of Code</b>

<a href="http://code.google.com/soc/">Google's Summer of Code</a> is starting early this year. There already are students proposing projects to mailing lists or in private mail although the program will only open for student applications in some weeks. They seem to be very motivated. The mentor organization application phase has started today. Like last year I will be a mentor for <a href="http://www.kde.org">KDE</a> and <a href="http://www.opensuse.org">openSUSE</a>. Let's see how it works out this year. There already is a nice list of <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Ideas">ideas for KDE projects</a>. Stay tuned.
<!--break-->