---
title:   "Ludwigsburg"
date:    2004-08-23
authors:
  - cornelius schumacher
slug:    ludwigsburg
---
The conference is over. We had a lot of good talks. It was interesting and it was fun. Now we are heading for the coding marathon and the evening promises that this will be even more fun.

I sat down together with Reinhold and went through the latest usability report on KOrganizer from <a href="/www.openusability.org">OpenUsability.org</a>. We came up with list of things to change. These are in particular:

<ul>
<li>Remove the expand/collapse button on the agenda view and put a label "All Day" at the place where now the button is to indicate better what the all-day agenda view is god for.</li>
<li>Replace the orange buttons indicating events in non-visible areas of the agenda view by the old flat black ones. They are less intrusive and don't look line buttons to press.</li>
<li>We discussed in length if the suggestion to move the date navigator pane to the right side of the main window would be a good idea. The conclusion was that it actually is better to have it on the left, because it contains navigation elements similar to the ones in Kontact, KMail and KAddressbook and in these applications they are also on the left. Also the menus start from the left, so in fact the majority of control elements seems to be oriented at the left side of the windows. That's seems to be reason enough to keep it as it is right now.

As a goodie for those of our users who are passionate about the placement of the date navigator pane we would be willing to make it configurable if somebody gives us a case of beer in exchange.
</li>
<li>Create a default location for new resources, especially for the local one, e.g. ~/.kde/share/apps/korganizer/std[n+1].ics.</li>
<li>Add a warning if a new resource is created with invalid settings (e.g. an empty URL).
</li>
<li>TODO for the kresources framework: Add function to prevent closing of resource config dialog with
invalid data.
</li>
<li>Add a switch for changing between coloring of events by category and coloring them by resource
</li>
<li>If a resource has multiple categories use the category which doesn't have a default color for coloring.
</li>
</ul>

Let's see what we are able to implement of this list in the coming week.
