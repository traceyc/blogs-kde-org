---
title:   "openSUSE on Rails"
date:    2006-05-03
authors:
  - cornelius schumacher
slug:    opensuse-rails
---
The last couple of months I have worked on the <a href="http://en.opensuse.org/Build_Service">openSUSE Build Service</a>. The goal of the Build Service is to make it dead easy for developers to provide installable packages of their software on a broad variety of distributions. We presented a first preview at <a href="http://www.fosdem.org">FOSDEM</a>. At the <a href="http://www.linuxtag.org">Linuxtag 2006</a>, which takes place later this week in Wiesbaden, we will show the current state. On Thursday, May 4th, there is a complete <a href="http://www.linuxtag.org/2006/de/besucher/programm/freies-vortragsprogramm/donnerstag.html">openSUSE track</a>. I will give a talk about the Build Service architecture. You are invited. Don't miss the opportunity to learn about this exciting project.

From a technical point of view the Build Service has two shining aspects. The first is the nice architecture we came up with. It's based on a straight-forward REST based web service, which basically simply does XML over standard HTTP. This leverages existing tools and makes it easy to write clients on top of the web service. There are several of these clients. The most prominent currently is the <a href="http://build.opensuse.org">web client</a>. The following picture illustrates the architecture of the Build Service.

[image:1974 size=original hspace=80]

The other shining aspect is the implementation which is done with <a href="http://www.rubyonrails.org">Ruby on Rails</a>. Since we started using Rails I was getting on the nerves of my colleagues with my enthusiasm for Ruby and especially the Rails framework. Amazingly this hasn't worn off. I'm still enthusiastic about the framework and really enjoy using it. It's so simple, productive and well-designed that it easily beats every other web application development framework I have ever used. This explains why I endorse Ruby from <a href="http://blogs.kde.org/node/1970">time</a> to <a href="http://blogs.kde.org/node/1765">time</a>.

So, if you want to learn something more about the openSUSE Build Service, its architecture and its implementation, join us on Linuxtag and listen to the presentations. I'm looking forward to see you in Wiesbaden.
<!--break-->