---
title:   "KDE Everywhere"
date:    2005-03-27
authors:
  - cornelius schumacher
slug:    kde-everywhere
---
<p>KDE is used all over the world. We have a truly <a href="http://worldwide.kde.org">international community</a>. Our desktop is <a href="http://i18n.kde.org/stats/gui/KDE_3_4_BRANCH/index.php">translated to 79 languages</a>. KDE is everywhere.</p>

<p>To express this better I thought it would be nice to put our beautiful new <a href="http://wiki.kde.org/tiki-index.php?page=KDE+Logo">logo</a> on a voyage throughout the world. The first stop is Berlin. Here is a sneak-peak. The full set can be found on <a href="http://www.kde-look.org/index.php?xcontentmode=44">kde-look</a>. By the way, all photos are real.</p>

<!--break-->
<a href="http://www.kde-look.org/content/show.php?content=22323"><img src="http://developer.kde.org/~danimo/images/reichstag.jpg"></a>

<p>Now it's your turn. We need your help to continue the voyage of our logo. Are you in New York, Paris, Rio de Janeiro? Can you get to the Chinese Wall, Ayers Rock or Mont Blanc? Do you live at another interesting place? Take the <a href="http://www.kde.org/stuff/clipart/klogo-official-lineart_detailed-3000x3000.png">KDE logo</a> with you and take a picture. It's simple, you just have to print it (click <a href="http://www.kde.org/stuff/clipart/klogo-official-lineart_detailed-3000x3000.png">here</a> and press "Print" in your browser), get your digital camera, find an interesting place and submit the picture to <a href="http://www.kde-look.org/index.php?xcontentmode=44">kde-look</a> under the "KDE Everywhere" category. If you have a particular interesting place in mind and can't print the logo yourself, <a href="mailto:schumacher@kde.org">contact me</a> and I will send you the KDE Everywhere kit, consisting of a laminated printout of the icon and a powerstrip.</p>

<p>I'm looking forward to your submissions. Let's get out and show the world that KDE is everywhere!</p>
