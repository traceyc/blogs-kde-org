---
title:   "Playing with words"
date:    2008-07-03
authors:
  - cornelius schumacher
slug:    playing-words
---
Two great pictures brought to you by <a href="http://wordle.net">Wordle</a> in combination with <a href="https://blogs.kde.org/blog/54">my blog</a> and <a href="http://planetsuse.org/">Planet SUSE</a>.

        <a href="http://wordle.net/gallery/wrdl/49581/My_Blog" 
          title="Wordle: My Blog"><img
          src="http://wordle.net/thumb/wrdl/49581/My_Blog"
          style="padding:4px;border:1px solid #ddd"
          ></a>  <a href="http://wordle.net/gallery/wrdl/49578/Planet_SUSE" 
          title="Wordle: Planet SUSE"><img
          src="http://wordle.net/thumb/wrdl/49578/Planet_SUSE"
          style="padding:4px;border:1px solid #ddd"
          ></a>

Beautiful.
<!--break-->
