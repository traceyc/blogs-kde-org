---
title:   "Upcoming Events"
date:    2008-02-22
authors:
  - cornelius schumacher
slug:    upcoming-events
---
As <a href="http://franz.keferboeck.info/blog/?p=14">Franz already wrote</a> there are a couple of exciting free software events coming up. From March 4th to 9th there is <a href="http://www.cebit.de">CeBIT</a>, the world's largest computer trade show. KDE will have a booth there. If you want to help to show KDE to a broad variety of visitors there, don't hesitate to contact <a href="mailto:kde-events@kde.org">kde-events@kde.org</a>. It's interesting, it's fun, and it's a great help for KDE. The KDE e.V. is able to help with travel costs if needed.

For me next stop will be <a href="http://www.fosdem.org">FOSDEM</a> this weekend. I'm part of the SUSE crew which will have a strong presence there. Check out the <a href="http://www.fosdem.org/2008/schedule/devroom/opensuse">openSUSE developer room</a> to get the latest info about the upcoming openSUSE 11.0 and a lot of other interesting topics around openSUSE. FOSDEM is great because there probably is no other place where you can meet that many free software people on one weekend. See you in Brussels.
<!--break-->
