---
title:   "Kolab 2 hits the masses"
date:    2005-06-20
authors:
  - cornelius schumacher
slug:    kolab-2-hits-masses
---
Today the second generation of the <a href="http://www.kolab.org">Kolab</a> groupware solution <a href="http://www.kolab.org/news/pr-20050620.html">was released</a>. It's incredible how far this project has come. I still remember when <a href="http://www.linux-user.de/ausgabe/2000/12/059-ksplitter/linus-kde.jpg">Martin Konold</a> first told us about his ideas about <a href="http://www.erfrakon.de/seiten/aktuelles/?article=2">"How to escape from the Outlook trap"</a> at <a href="http://www.linuxtag.de">Linuxtag</a> three years ago. We were sceptical, because it was all vaporware back then and the plans were, well, ambitious. But now with Kolab 2 we actually have a solid all Free Software groupware solution with full KDE client support through <a href="http://kontact.kde.org">Kontact</a> and even <a href="http://www.kde.org">KDE</a> as a project has begun to <a href="http://dot.kde.org/1117544382/">use it</a>.

There are two aspects about Kolab which I think are particular noteworthy. First, it is an example how commercial development of Free Software can work. Most of the Kolab development was payed work. But it was done in KDE CVS and well integrated with the project. It took some time until this kind of collaboration really worked, but now it can safely be considered a success and a prototype for future commercial KDE development.

The other aspect is the design decision to access all data on the server via IMAP. This makes Kolab unique and allows for extreme scalability. It also means that a rich client is required, but this in fact is a great benefit which automatically brings nice features as offline capabilities or a rich user interface for viewing and editing calendar data. An interesting tweak is that the Kolab client not only works with the dedicated Kolab server, but with any IMAP server. So you can use your normal mail account to do server-based calendaring with Kontact.

So what will bring the future? Kontact has support for an <a href="http://kontact.kde.org/groupwareservers.php">impressive list of groupware servers</a>, the Kolab support probably being the most stable right now. The crazy thing is that this spans such a huge variety of different technologies, from iCalendar over HTTP and WebDAV, through SOAP and XML-RPC to LDAP and IMAP. Which one is the best and will become most widely accepted only the future can tell. KDE Kontact is well equipped to be an important part of this future.

One major problem still exists, though: There are no Kolab T-Shirts yet ;-).
