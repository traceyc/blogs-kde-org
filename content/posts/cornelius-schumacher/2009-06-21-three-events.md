---
title:   "Three Events"
date:    2009-06-21
authors:
  - cornelius schumacher
slug:    three-events
---
The next three weeks will be pretty busy. I'm looking forward to three exciting events which will take place during this time, and I'm happy to be able to attend them.

<b>Linuxtag</b>

First event is <a href="http://www.linuxtag.org/2009/">Linuxtag</a>, from June 24th to June 27th in Berlin. I will be there and give a <a href="http://www.linuxtag.org/2009/de/program/freies-vortragsprogramm/mittwoch/vortragsdetails.html?talkid=731">presentation about SUSE Studio</a> as part of the <a href="http://en.opensuse.org/LinuxTag">openSUSE Day</a>. We'll also present <a href="http://susestudio.com/">SUSE Studio</a> at the openSUSE booth as well as other openSUSE goodness.

Linuxtag of course always has been a great place to meet the KDE community as well (I'm thinking for example of <a href="http://www.flickr.com/photos/29913312%40N04/2801367476/">this</a>, and <a href="http://www.flickr.com/photos/foobarbaz/141521711/">that</a>, and <a href="http://www.linux-user.de/ausgabe/2005/08/026-ksplitter/wikipedia.png">that</a>). So I'm looking forward to the KDE related presentations as well, and will certainly also spend some time at the KDE booth.

As a special opportunity, if you want to sign the <a href="http://ev.kde.org/rules/fla.php">FLA</a> with KDE e.V., find me and we'll get this done.

<b>KDE Wiki Meeting</b>

Directly after Linuxtag there will be a small <a href="http://techbase.kde.org/index.php?title=Projects/kde.org/Meetings/June09">KDE Wiki Meeting</a>. We'll sit down and get some content, structure, and technology sorting done, to improve the Wiki presence on <a href="http://techbase.kde.org/">techbase.kde.org</a> and <a href="http://userbase.kde.org/">userbase.kde.org</a>. I hope we'll also find some time to discuss the idea to create a third Wiki communitybase.kde.org, for holding community content, which currently floats around without a proper home on various different KDE sites. This should be a place for collaboration and for content which is important to the community, but not really relevant for the general public.

<b>Gran Canaria Desktop Summit</b>

From July 3rd to July 11th there is the <a href="http://www.grancanariadesktopsummit.org/">Gran Canaria Desktop Summit</a>. This includes <a href="http://akademy2009.kde.org/">Akademy</a> and <a href="http://www.facebook.com/group.php?gid=23102876751">GUADEC</a>, and will be a fantastic meeting place for everybody working on or caring about free software on the desktop.

We have a great <a href="http://dot.kde.org/2009/06/11/stallman-bender-lefkowitz-and-pavelek-hold-keynotes-gran-canaria-desktop-summit">lineup of keynote speakers</a>, a fantastic <a href="http://akademy2009.kde.org/conference/program.php">Akademy conference program</a>, and much more. And of course there is plenty of opportunity to informally meet, discuss, and get some work done. It will be fun.
<!--break-->