---
title:   "Ruby, Ruby, Ruby"
date:    2006-01-26
authors:
  - cornelius schumacher
slug:    ruby-ruby-ruby
---
<a href="http://www.ruby-lang.org">Ruby</a> is rolling. It's amazing how much enthusiasm it accumulates. There seems to be a broad movement of people exploring Ruby, using it and getting addicted. Especially because there is <a href="http://www.rubyonrails.org">Rails</a>. If there ever was a killer application for a programming language, here it is. Three examples for amazing Ruby adoption:

Example One: <a href="http://amarok.kde.org"><b>amaroK</b></a>. <a href="http://amarok.kde.org/blog/categories/1-markey">Mark</a> just wrote in his <a href="http://amarok.kde.org/blog/archives/73-Scriptable-Lyrics-Support.html">blog</a>: <i>(...) - And it means amaroK now depends on Ruby. (...) I like it.</i>. Enough said.

Example Two: <a href="http://clickspotter.ath.cx/"><b>ClickSpotter</b></a>. ClickSpotter is a nice app for analyzing web server logs and showing where your visitors come from on a world map in real time. It's done by Chris Schlaeger (you know him) with Ruby and the Qt bindings. Cool stuff.

Example Three: <b>Converts</b>. There are lots of blogs about comparing Ruby to other languages. It's interesting that prominent people are converting to Ruby. See for example the blog of <a href="http://jroller.com/page/dgeary">David Geary</a>, one of the key people behind <a href="http://java.sun.com/j2ee/javaserverfaces/">Java Server Faces</a>. He writes in <a href="http://jroller.com/page/dgeary?entry=tipping_rails">"Tipping Rails"</a>: <i>I couldn't stop programming with Rails.</i> Or there is Steve Yegge, whose blogs were brought to my attention by <a href="http://blogs.kde.org/node/1741">Richard Dale</a>. He writes in <a href="http://www.oreillynet.com/ruby/blog/2006/01/a_little_antiantihype.html">"A little anti-anti-hype"</a> (with its great story about "quit" in the Python and Ruby shells): <i>In fact it may have already tipped, with Ruby headed to become the winner, a programming-language force as prominent on resumes and bookshelves as Java is today.</i> Is there more to say?

I'm having fun with Ruby, too. I use it on Rails for a couple of weeks now and my colleagues are already annoyed by my enthusiasm. But this is something for a different blog entry...
<!--break-->