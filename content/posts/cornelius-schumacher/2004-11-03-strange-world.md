---
title:   "Strange world"
date:    2004-11-03
authors:
  - cornelius schumacher
slug:    strange-world
---
It's a <a href="http://edition.cnn.com/2004/ALLPOLITICS/11/02/election.worldview.ap/index.html">strange day</a> in a <a href="http://www.hansemun.de/images/ronald.jpg">strange world</a>. But it's nice to see that the Linux desktop seems to be able to adapt to this strangeness by <a href="http://tech9.net/rml/log/2004110201">some means</a> or <a href="http://www.hakubi.us/ridge/">other</a>.
