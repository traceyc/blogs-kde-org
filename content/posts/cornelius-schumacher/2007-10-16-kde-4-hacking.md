---
title:   "KDE 4 Hacking"
date:    2007-10-16
authors:
  - cornelius schumacher
slug:    kde-4-hacking
---
This week is KDE 4 hack week for me and some other colleagues at SUSE. We have thrown in some of our ITO time (that's a certain fraction of our work time we can flexibly spend on innovative projects which aren't necessarily related to our day-to-day jobs) to help making KDE 4 ready for release before the total release freeze comes into effect on Friday. Some of the KDAB folks were attracted by this idea as well and will also chime in and do some serious KDE 4 hacking. So hopefully we will have a well-working KDE PIM in KDE 4. It certainly will be a fun week!

[image:3034 width="640" height="480" hspace=100]

I have a KDE 4 desktop up and running now and am looking at KOrganizer, the project which initially brought me to KDE. It's nice to see how far it has come. The KDE 4 version already runs pretty well, but there a still lots of small glitches and I will spend some time on getting rid of these. Enough blogging, back to hacking now.

If somebody wants to be part of the fun, get your SVN checkout ready and join us on IRC.
<!--break-->
