---
title:   "3.5 or 4.0: that is the question"
date:    2005-03-12
authors:
  - cornelius schumacher
slug:    35-or-40-question
---
It's interesting to observe <a href="http://lists.kde.org/?l=kde-cvs">KDE development</a> these days. Since the <a href="http://dot.kde.org/1109582845/">3.4</a> code was branched from the main line, the <a href="http://webcvs.kde.org">repository</a> is open for new development again.

For past releases this meant committing frenzy with new features flying in, development branches being merged and all the creativity bound by the feature freeze breaking loose. This time it's different. It's quiet. It's the silence before the storm. I know that there is new and interesting code around, but it doesn't seem to find its way into the main development line yet. So what's going on?

We are facing some big changes. KDE 4 appears on the horizon. <a href="http://www.trolltech.com/products/qt/qt4info.html">Qt 4</a> is getting finished and KDE prepares to make use of it. This will get us a whole bunch of new opportunities and some nice improvements like better performance, smaller memory footprint and full accessibilty support. KDE 4, the first release based on Qt 4, will also be the first release since more than three years breaking binary compatibility of the KDE libraries. This will give us the opportunity to clean up the API and elevate the KDE libraries to the next higher level.

Other than that there also is the <a href="http://lists.kde.org/?l=kde-core-devel&m=110873178005143">imminent move</a> of our version control system from <a href="http://www.gnu.org/software/cvs/">CVS</a> to <a href="http://subversion.tigris.org">Subversion</a>, which will be a challenge, because it will be the biggest CVS repository moving to Subversion until now. We also already had some heated debates about the KDE build system and the Windows port of KDE. You see that we are looking forward to some pretty exciting times.

In addition to these quite fundamental changes of the framework and the environment, there of course are lots of new features, usability improvements, code cleanups and bug fixes waiting to be implemented. These areas are the ones where the most people are involved, where the most effort is spent, and what will have the most impact on the end user. How will this be done?

A surprisingly short part of the <a href="http://lists.kde.org/?l=kde-core-devel&m=110839367905112">Future of KDE Development</a> thread was about actual relase planning after 3.4. Common opinion seems to be that there will be a <a href="http://developer.kde.org/development-versions/kde-3.5-features.html">3.5</a> release at some time and later the full-blown <a href="http://developer.kde.org/development-versions/kde-4.0-features.html">4.0</a> release with all the new bells and whistles.

But how much of this will happen in parallel? Personally I'm pretty sure that I can't do both, working on applications for 3.5 and on the framework for 4.0, including porting the applications to the new framework. I have a long list of things I would like to implement, todos, new features and crazy ideas (I will post <a href="http://blogs.kde.org/node/view/974">the list</a> later). Most of them could well be done for 3.5, some would have to wait for 4.0. So, 3.5 or 4.0: that is the question.

My <a href="http://lists.kde.org/?l=kde-core-devel&m=110844793627064">feeling</a> is that it would be best to first do a 3.5 release and wait with Qt 4 porting and KDE 4 library development until the final version of Qt 4 actually is released. This would allow us to take profit of the stable and well-understood 3.x framework to implement some of the outstanding features with maximum efficiency and focus on the KDE 4 framework when we have a stable Qt 4 as base.
