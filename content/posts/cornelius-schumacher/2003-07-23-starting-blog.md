---
title:   "Starting a blog"
date:    2003-07-23
authors:
  - cornelius schumacher
slug:    starting-blog
---
After reading the announcement on the dot, compiling kblog and creating an account, I thought it would be a good idea to start a blog myself. Let's see if anybody reads it...<br><br>
What I like about www.kdedevelopers.org is that there is a native client (at least there is the start of a native client). Web interfaces suck, but with KBlog this could become fun...