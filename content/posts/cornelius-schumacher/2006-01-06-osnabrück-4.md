---
title:   "Osnabrück 4"
date:    2006-01-06
authors:
  - cornelius schumacher
slug:    osnabrück-4
---
Today starts the KDE PIM meeting at Osnabr&uuml;ck. It has become a <a href="http://pim.kde.org/development/meetings.php">real tradition</a>. We are now meeting for the fourth year in a row on the first January weekend.

Many people are already there. I'm sitting together with Marc, Reinhold, Volker and Ingo in the <a href="http://www.intevation.de">Intevation</a> office and we are preparing for the afternoon when we will officially start the meeting. The main and maybe only topic will be to create the architecture for the PIM storage layer for KDE 4. I have started to collect material at a <a href="http://pim.kde.org/playground/osnabrueck4/">Osnabr&uuml;ck 4 section on the playground of pim.kde.org</a>.

We have big plans and for KDE 4 we are finally able to address all the ugly problems we had with our current storage, but we weren't able to fix because of the compatibility requirements of kdelibs in the 3.x series. I'm really looking forward to the meeting now. Developing for KDE PIM will be much more fun once we have a modern well-architectured backend.
<!--break-->
