---
title:   "New look for KOrganizer"
date:    2003-07-24
authors:
  - cornelius schumacher
slug:    new-look-korganizer
---
Tim Jansen has committed the new look for KOrganizer's agenda view (<a href="http://www.tjansen.de/blog/pics/after2.png">Screenshot</a>). This is a big improvement. People were complaining from time to time about the "boring" look of KOrganizer. Now they don't have a reason for that anymore (Well, I'm sure somebody will come up with another reason to complain ;-).
<br><br>
Now I just have to unbreak libkcal. Seems like my 200k patch from yesterday broke recurring events...