---
title:   "Experimenting with KBlog"
date:    2004-06-13
authors:
  - cornelius schumacher
slug:    experimenting-kblog
---
I'm experimenting with KBlog. I have no idea if this will work, post embarrassing texts or delete all my data. But it's worth a try. It at least looks like it could once become a useful tool.

It doesn't seem to be able to correctly set a category and the as-you-type spell-checking is missing, but it is able to post a blog, so that's not bad after all.

<i>I posted this entry with KBlog, but had to tune it a bit with the web interface after posting.</i>
