---
title:   "The perfect editor"
date:    2003-07-29
authors:
  - cornelius schumacher
slug:    perfect-editor
---
Hooray, there is a new NEdit release! Today the first release candidate of NEdit 5.4 was announced. There were three features I was missing from NEdit up to now:
<ul>
<li>Start scrolling before the cursor has reached the end of the screen to make sure you always have the overview of a few lines in advance</li>
<li>An option to show whitespace at the end of lines</li>
<li>Hiding the mouse pointer when you start typing</li>
</ul>
All these features are implemented now (and a couple more). I tried the new version and the new features and immediately liked it. That makes NEdit the perfect editor for me. Congratulations to the NEdit team for this marvelous job.
<br><br>
See the <a href="http://www.nedit.org">NEdit homepage</a> for details.