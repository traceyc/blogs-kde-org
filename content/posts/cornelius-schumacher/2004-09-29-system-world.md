---
title:   "The System of the World"
date:    2004-09-29
authors:
  - cornelius schumacher
slug:    system-world
---
Today I received <a href="http://www.metaweb.com/wiki/wiki.phtml?title=Stephenson:Neal:The_System_of_the_World">The System of the World</a> which makes my <a href="http://www.nealstephenson.com">Neal Stephenson</a> collection complete again and me a happy guy. I just have finished <a href="http://www.metaweb.com/wiki/wiki.phtml?title=Stephenson:Neal:Quicksilver">Quicksilver</a> and am about to start to read <a href="http://www.metaweb.com/wiki/wiki.phtml?title=Stephenson:Neal:The_Confusion">The Confusion</a>. It feels good to know that two and a half kilograms of words are still ahead.
