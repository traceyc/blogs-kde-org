---
title:   "Trying to make things simpler..."
date:    2006-03-28
authors:
  - aurélien gâteau
slug:    trying-make-things-simpler
---
I've been working lately on Gwenview UI, experimenting various changes, trying to make the application simpler to use.

One thing that did not satisfy me was that nobody seemed to grasp the way to switch between browsing and viewing modes. Have a look at <a href="http://gwenview.sourceforge.net/data/screenshots/1.3/full/big_thumbs_no_caption-127.jpg">this screenshot</a> and try to guess how it's supposed to work. 

In case you didn't find it: you are expected to "uncheck" the first toggle button in the toolbar.

My first idea was to replace the toggle button with two radio buttons, one for each mode. Still, I didn't find it obvious. So I decided to go even further and configure the toolbar to show text alongside icons by default. 

These two screenshots present the result of my experiments. 

[image:1887 width=350 align=left] &nbsp; [image:1888 width=350]

I like it, but I'm a bit worried that it does not look very KDE-like. What do you think about this?
<!--break-->