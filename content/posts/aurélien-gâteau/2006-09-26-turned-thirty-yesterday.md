---
title:   "Turned thirty yesterday"
date:    2006-09-26
authors:
  - aurélien gâteau
slug:    turned-thirty-yesterday
---
I turned thirty yesterday. Still can't believe it...

My wife came up with a really awesome present: a booklet containing an invitation for 100 different high sensation activities. These activities are as versatile as scuba diving, helicopter flying, bungee jumping, driving a Ferrari, an F3 or even a 2nd war assault tank (!), paragliding... Only problem: I must choose only one of them. And I'm afraid receiving the same present for the next years so that I can try the other activities is not an option...
<!--break-->