---
title:   "Never too busy to post pictures :-)"
date:    2006-06-25
authors:
  - aurélien gâteau
slug:    never-too-busy-post-pictures
---
If you don't understand the title of this blog, have a look at <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/237-The-Personals.html">this entry in Adrian blog</a>...

So here is Clara in action:

[image:2132]
"Do not disturb me, I'm busy hacking with my father..."

[image:2133]
"Look at my baggy trousers, don't you like my streetwear look?"

[image:2134]
"My parents love this smile!"

<!--break-->