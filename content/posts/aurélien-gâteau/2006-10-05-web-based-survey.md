---
title:   "Web based survey"
date:    2006-10-05
authors:
  - aurélien gâteau
slug:    web-based-survey
---
I want to run a survey about Gwenview. I have been googling for some web based surveys for a while, but it would be even better if I could get some real experiences. 

Here is my feature list:
<ul>
<li>Support for multiple questions</li>
<li>A simple admin page to analyse the result (I don't need fancy graphes, but won't complain if there are)</li>
<li>Free or at least not too expensive</li>
<li>I would prefer it to be self hosted, but I could host it on Gwenview website if requirements are not too odd</li>
</ul>

So dear lazy web, which survey system would you suggest?
<!--break-->