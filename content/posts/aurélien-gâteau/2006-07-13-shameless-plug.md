---
title:   "Shameless plug"
date:    2006-07-13
authors:
  - aurélien gâteau
slug:    shameless-plug
---
A coworker and I recently started a website where we host the various opensource tweaks we write for our company, Dental On Line. There is not a lot available right now, but you can already find a few KDE patches:

<ul>
<li>The <a href="http://opensource.dental-on-line.com/?page_id=20">(in)famous Flash installer for Konqueror</a></li>
<li>Some <a href="http://opensource.dental-on-line.com/?page_id=17">KDEPIM patches</a></li>
<li>A patch to <a href="http://opensource.dental-on-line.com/?page_id=18">embed PyQt widgets in KDM</a>
<a href="http://opensource.dental-on-line.com/wp-content/uploads/2006/07/kdmpywidget.png">
<img src="http://opensource.dental-on-line.com/wp-content/uploads/2006/07/kdmpywidget.thumbnail.png">
</a>
</li>
</ul>

Anyway, the address is http://opensource.dental-on-line.com, we may even add more content later :-)
<!--break-->