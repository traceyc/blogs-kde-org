---
title:   "Gwenview II, the return"
date:    2007-01-27
authors:
  - aurélien gâteau
slug:    gwenview-ii-return
---
I forgot to blog about this: I'm not giving up maintainership of Gwenview anymore, in fact it has been decided that Gwenview will move to kdegraphics for KDE4! This is what I call great news!

The nice people from kde-usability and I have been working on the design of this new Gwenview. It's going to be a bit different from what you have been used to, but I believe it's more focused on the way most people use Gwenview: either to quickly view an image, or to browse all images in a folder.

To wet your appetite, here are two mock-ups of what Gwenview 2.0 will probably look like:

[image:2658 size=thumbnail] [image:2659 size=thumbnail]
<!--break-->