---
title:   "First Post"
date:    2006-03-28
authors:
  - aurélien gâteau
slug:    first-post
---
Hello,

I am new to this blog thing :-). I am the main developer of <a href="http://gwenview.sf.net">Gwenview</a> and also hack on <a href="http://extragear.kde.org/apps/kipi/">KIPI</a>.

We'll see if I can produce some interesting content for a while...
<!--break-->