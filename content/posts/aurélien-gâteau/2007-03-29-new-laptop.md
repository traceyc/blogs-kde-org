---
title:   "New Laptop"
date:    2007-03-29
authors:
  - aurélien gâteau
slug:    new-laptop
---
Hum, long time no blog...

I finally bought a laptop: it's a Lenovo 3000 N100. I am happy to report almost everything worked out of the box when I installed Ubuntu Edgy, including Wifi. Only tricky thing was figuring out how to get the correct video resolution. It turned out the only necessary action was to apt-get install 915resolution.

Webcam and fingerprint reader are not supported right now, but I don't need them.  Only minor quirk is the missing support for headphone jack sense (turning off internal speakers when a headphone is plugged in), but I believe next version of Alsa will support it.

Its core duo processor is much more powerful than my old Duron 1200, and being a laptop make it possible for me to hack on my daily commute time. It's also fully baby compliant, <a href="http://www.dailymotion.com/video/x1jx9y_clara-apprend-a-programmer-a-son-pere">as you can see...</a>
<!--break-->