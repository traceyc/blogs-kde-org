---
title:   "Gwenview survey"
date:    2006-10-21
authors:
  - aurélien gâteau
slug:    gwenview-survey
---
Here it is. I finally found the time to set up a quick survey regarding Gwenview usage. Your participation is greatly appreciated!

<a href="http://www.questionpro.com/akira/TakeSurvey?id=556852">Gwenview survey</a>

<!--break-->