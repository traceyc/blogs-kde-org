---
title:   "Filtering"
date:    2006-05-08
authors:
  - aurélien gâteau
slug:    filtering
---
A few weeks ago a Gwenview user asked if it would be possible to implement file name filtering. I initially answered that while the idea was nice, I didn't have time to work on it.

Of course, one should never trust me when I answer mails too quickly. A few days later, it turned out I needed such a feature. So I decided to implement it.

It started as a simple lineedit to filter filenames:
[image:1992]

But this was not handy for me: I needed to filter images by file date. So I ended up implementing it as a toolbar button. When clicked it reveals a few fields to filter by name and/or by date, like this:
[image:1993]

All this is available from your nearest KDE SVN mirror.
<!--break-->