---
title:   "Now I can browse faster in KickPim"
date:    2004-12-02
authors:
  - duncan mac-vicar
slug:    now-i-can-browse-faster-kickpim
---
After upgrading my kdepim to HEAD (nicely, I did a PKGBUILD for archlinux that grabs and builds it). I started to fill my kabc pictures with the nice kde:KPixmapRegionSelectorDialog from Antonio and other developers (This dialog should be used in Kopete to select the MSN display picture for instance) (yes I coded a similar one too and then realized it was already commited). Then I modified the nice applet kickpim (I can't live without it) to show the kabc pictures. Kopete contact list should be next victim. Even KMail's quick address selector is a good candidate. Any place where you look for a person quickly is a good candidate if you can match faces instead of reading the name.

[image:746,middle,4,4,1]