---
title:   "Faces everywhere, Part III : Kopete Contact List Avatars"
date:    2004-12-05
authors:
  - duncan mac-vicar
slug:    faces-everywhere-part-iii-kopete-contact-list-avatars
---
Chapter III of the crusade is done. Kopete now supports displaying KABC pictures both in the metacontact tooltip and in the contact list itself.
Further integration with specific protocols avatars and this property is coming. <a href="http://img106.exs.cx/my.php?loc=img106&image=l6nkopeteyay2.png">How it looks by now?</a>
