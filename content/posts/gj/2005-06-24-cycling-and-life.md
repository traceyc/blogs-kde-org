---
title:   "Off cycling and life."
date:    2005-06-24
authors:
  - gj
slug:    cycling-and-life
---
So, today wasn't very good day for me. First, I didn't manage to finish my project at work as expected today. I was asked to add few things to bind-dlz. Basically T-SIG database support.
 Anyway, I decided to cycle around Stanley Park. Stunning place BTW., just in the middle of road my rear tire was completely flat, as it turned out, small piece of glass made hole in my rear tube.
 Not only I wasn't able to chase back that nice girl I was talking to before, to ask her out, or just ask her for phone number, email, whatever. But also I had to walk my bicycle all the way downtown. 1.5h. One and the half hour, not wasted. I had a chance to look at the city from another perspective. At my life. This place is a paradise for me. If there is anything I would like now very much, is to be Canadian, and be able to stay here forever. Vancouver, BC is really really my dream place. Through out all these years, when I managed to get from someone not having anything, someone who never was anywhere, never tried anything in his life up to today, being in this gorgeous place, having very good job, being able to do so much. I can't imagine my life can be better. Now if only I will manage to stay here, settle down up here, have family, continue bussines up here life would be perfect. Yeah, one and the half hour all alone. One on One with me self.

(no stories about Kopete this time, sorry).
But you might wanna check new initiative, Gaim mocked up for KDE. <A href="http://kaim.kde.org.pl/">http://kaim.kde.org.pl/</A> ;-)
