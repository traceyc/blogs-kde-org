---
title:   "as the years pass by....."
date:    2005-08-17
authors:
  - gj
slug:    years-pass
---
I was born, long ago.... as the song says. 0x1C, still teenager in Hex :P
So we are planing to drink a lot of .... alcohol tonight. Good, I can still do that :p

On KDE, I am slowly getting new libgadu done. Rewriting it in qt4. I know I know, probably only we are going to use it, possibly kadu project. But they are not very open, I can compare them to few long time KDE contributing blokes, still young, done a bit coding, and think they've conquer the world. Maybe I will have time and patience to create plain C interface to it. But I don't quite care about it. I surely will create command line "client" as example. Finally I can create lib in QT that is light weight, as they say. But feature complete. 

On personal side, hard work, bind-dlz, postgres and lately bacula. Only bind-dlz is scarry, everything else is a pleasure to hack. Seriously. I think someone should rewrite bind in C++. Maybe even in qt4. 

I am going to Malaga this year. I hope someone on local site will have time and patience to pick us up from the airport. My arival is scheduled for 2355 on 25th. That's quite late, so I really count on you guys.

Nothing more to say, I hope next year will bring me even more happiness, and that I will be able to create some more for everyone to enjoy. (not that I have done much in past 0x1C years).

(and the song continues) .... I am the chosen I'm the one I have come to save the day And
I won't leave until I'm done...........

Thank you all. You know who you are.