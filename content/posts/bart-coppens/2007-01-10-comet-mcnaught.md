---
title:   "Comet McNaught"
date:    2007-01-10
authors:
  - bart coppens
slug:    comet-mcnaught
---
Ok, so no next update in the Krita plugin tutorial yet (it'll surely be done before the end of the month *ahem*). But much more spectacular was the comet I just saw in the dusky sky. I had a very lucky few moments of no clouds in just the right area, which also by pure luck had a relatively clear line of sight to the horizon. <a href="http://en.wikipedia.org/wiki/Comet_McNaught">Comet McNaught</a> was very noticeable with the naked eye, and even nicer to look at with a pair of binoculars. Too bad the clouds overtook the comit rapidly, so I had no chance to get my camera and take a picture of it. I fear that by tomorrow it'll be too low to spot, but still, I'm very happy to have seen it, even if it was only for a few moments :) Maybe this blog will encourage the people that have a less cloudy weather, have a clearer horizon, and are located more northernly, to take a look outside their windows tomorrow. (<a href="http://skytonight.com/observing/home/5133461.html">More info on how to observe.</a>) For the people on the southern hemisphere: you'll apparently get your chance to observe it as well in a little while :)<!--break-->