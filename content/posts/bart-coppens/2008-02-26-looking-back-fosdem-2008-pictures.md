---
title:   "Looking back on FOSDEM 2008 (with pictures)"
date:    2008-02-26
authors:
  - bart coppens
slug:    looking-back-fosdem-2008-pictures
---
So, after Saturday's <a href="http://blogs.kde.org/node/3292">FOSDEM KDE Group Picture</a>, perhaps it's now time to give a slightly fuller account of FOSDEM 2008 (with pictures). Since most of my pictures from the main tracks were too blurry anyway, I'll just focus on some of my <a href="http://www.bartcoppens.be/photos/Fosdem2008/">pictures related to the KDE FOSDEM 2008 presence</a> (leaving out some pics of individual persons and overview shots). Photos have only been resized and cropped where somewhat appropriate. Perhaps to do for next year: buy a lens which captures more light, or a bigger flashlight :P (Shooting at ISO 1600 is rather noisy :()

Let's start with a picture of the KDE stand, featuring <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/526-Nous-sommes-partis-de-Bruxelles.html">Adriaan and his fancy box</a> (btw Adriaan, another piece of consumer electronics that people willingly pose with would be the OLPC).
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1649_small.JPG" />

The first KDE talk was by Nikolaj Hald Nielsen about Amarok 2, and introduced a live mascote for Amarok. I noted that Amarok 2 really should try to get the ability to download and manage live performances and other music available from the <a href="http://www.archive.org/details/audio">Internet Archive</a>. Let's hope they try that again (given that he actually looked into it already).
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1684_small.JPG" />
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1680_small.JPG" />

The following talk was by me about KOffice 2, but since I did that talk myself, I don't have pictures of that ;) I was followed by Sebastian Trüg talking about Nepomuk. I could really have used his proposed feature of using FOSDEM speaker information to find mails from them ;)
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1686_small.JPG" />

Knut Yrvin then talked about Free Software in telecom. Not only did he pass around a Greenphone (unfortunately no decent pics of that), but he also gave away a free book to the first person in the audience that could answer a tough question about what code TT GPLed at some point last year. It was so hard that he had to ask an easier question to be able to give away the book :)
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1695_small.JPG" />

Unfortunately I had to miss most of Holger Schröder's KDE on Windows in order to meet someone, but I was still able to snap some pictures of him in action (actually I made more decent pictures of him than of most other speakers, intriguingly enough).
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1710_small.JPG" />

The last talk on Saturday was done by Josef Spillner. He talked about multiplayer gaming for KDE4 games, which apparently exists already even though the functionality is hidden somewhat. I can't really check that because I don't actually compile KDE4 games, but it's still interesting to know about.
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1712_small.JPG" />

The first talk on Sunday morning (way too early for my taste :P) in the Crossdesktop room was done by the dynamic duo Simon Peter and Kurt Pfeifle (Simon doing the talking, and Kurt demonstrating Klik on his laptop/the projector while Simon discussed it, very nice interaction). They talked about Klik (2). I guess their target audience was me, because they showed a Klik error dialog about Krita not working ;)
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1717_small.JPG" />
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1724_small.JPG" />

Unfortunately I had to leave early during their Q&A session, because I wanted to see part of the Xen main track talk. I arrived late, and the room was completely packed and rather hot. So instead of staying in that room for the next talk about VirtualBox, I left in the break to go to the other main track about build tools, just in time for the SCons talk. (I was planning to meet a friend of mine during the CMake talk anyway, so now I just met him slightly earlier.) It was a lot less crowded there, and even a bit too cold (but that might've been because there was a draught). While I moved rooms, I was still able to quickly make a picture of the questions section of Jos Poortvliet and Sebastian Kuegler's talk on KDE4.
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1727_small.JPG" />

After the CMake talk I had to leave early again (and miss the next devroom talk), because we had a small meeting on the grass field next to the devroom about the upcoming aKademy 2008 in Belgium. That meeting finished just in time to catch another talk by Knut Yrvin, this time on Free Software in Education (he gave a total of 3 devroom talks, but his other talk about Free Software and phones was scheduled at the same time as the Klik one). No free books this time, but he did tell us about how teaching young kids how to use an office suite is a bad idea. I guess Inge won't like the sound of that ;)
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1736_small.JPG" />

After the main track CMake talk by Bill Hoffman, we also had our own version of a CMake talk, this time by Alexander Neundorf. I guess I should really try if that excluding of unit tests also works with the Krita unit tests, since that does sound like a nice feature to use. Bill attended the talk as well, and gave a free CMake book to a happily surprised Peter Rockai (mornfall) during the questions section, after Peter asked a question about integrating Java in CMake projects.
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1745_small.JPG" />
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1751_small.JPG" />

After that, we first had Øyvind Kolås (pippin) talking a bit about GEGL (we had to switch his talk and the Deb Packaging Jam, unfortunately). It seems that the current development version of Gimp has a checkbox in the main GUI to 'Enable GEGL', which is rather interesting (amongst other things, it provides them with live previewing of filters, a feature the current Krita development version has as well, and which I also demonstrated the day before). As seems to be typical with talks by pippin, he used a custom presentation interface to do his presentation (this time it appeared to be a graphically 'enhanced' file system browser :))
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1755_small.JPG" />

Following that I had another scheduling conflict: there was both the Deb talk, and another Klik talk (this time as a main track). I resolved it by quickly visiting Simon and Kurt to take a picture, and then go back to the Crossdesktop room where Jonathan Riddell explained us all about making Debian Packages. He did this by using the rather amusing example of packaging the <a href="http://www.gnu.org/software/hello/">GNU Hello World</a> program.
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1763_small.JPG" />
<img src="http://www.bartcoppens.be/photos/Fosdem2008/IMG_1776_small.JPG" />

Unfortunately, due to circumstances, we had to cancel the last devroom talk, so no pictures from that. After that, I hung around a bit with mornfall and Pino Toscano (who doesn't like me taking pictures of him, yet is on the group picture :P). After Holger talked a bit with us, FOSDEM 2008 was unfortunately over already. Let's hope next year's FOSDEM will be at least as good as this one :)

PS: I bought a Debian T-Shirt at FOSDEM, I guess that makes up for <a href="http://blogs.kde.org/node/3245">my rant</a> about them :) I actually wanted to buy one last year already, but they were sold out then. I made sure to buy one on Saturday this year, and indeed they still had some :) (FYI: like said in the comments there, that issue has been temporarily fixed in their kernels now. I tested it, and it indeed works, yay :)) (And generally speaking, I really like Debian, and that's mainly why I get angry at it sometimes: I care about it)

Edit/PS 2: Seems like Jonathan Riddell put up all of my KDE related FOSDEM pictures on <a href="http://flickr.com/photos/jriddell/">his Flickr account</a>, so now you can have a more extensive view of them :)
<!--break-->