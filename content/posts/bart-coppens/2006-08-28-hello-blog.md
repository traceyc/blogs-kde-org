---
title:   "Hello Blog"
date:    2006-08-28
authors:
  - bart coppens
slug:    hello-blog
---
So, after a long period of nagging by Cyrille and others, I finally got myself a blog. The idea is that I'll occasionally post something about a new Cool Krita Feature, or other stuff I might want to talk about. I think one of the first things I'll write about, once this gets syndicated on Planet KDE, will be the result of our SoC (ooh, suspense!).<!--break-->