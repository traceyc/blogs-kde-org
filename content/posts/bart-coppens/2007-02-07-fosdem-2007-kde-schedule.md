---
title:   "FOSDEM 2007 KDE schedule"
date:    2007-02-07
authors:
  - bart coppens
slug:    fosdem-2007-kde-schedule
---
Next <a href="http://www.fosdem.org/">FOSDEM</a> will be February 24-25 in Brussels (that's a little over two weeks away). There will be some interesting talks in the general tracks, but I think there's also some very interesting stuff going on in the KDE Devroom. This is a room where KDE people will give some talks, and can meet and so. The schedule is available <a href="http://www.fosdem.org/2007/schedule/devroom/kde">here</a>. The focus will be on KDE4 related topics, with, amongst others, a <a href="http://www.fosdem.org/2007/schedule/events/kde_semantic">talk about the semantic desktop in KDE</a> by Sebastian Trüg.

Even if you don't care for the KDE talks (which I doubt ;)), there will be lots of other talks on the <a href="http://www.fosdem.org/2007/schedule/days">complete schedule</a> (I'm not sure how finished it is, but it surely gives a good impression). Do note that on Sunday morning, the GNOME room will host a session with common topics and KDE/GNOME integration. This includes a talk by Jos van den Oever about Strigi. At the same time, the KDE room will be hosting a BoF meeting for people interested in free education software.

Hope to see you there! :)<!--break-->