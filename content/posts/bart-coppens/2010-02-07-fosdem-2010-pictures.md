---
title:   "FOSDEM 2010 Pictures"
date:    2010-02-07
authors:
  - bart coppens
slug:    fosdem-2010-pictures
---
As always, I took some <a href="http://www.bartcoppens.be/photos/Fosdem2010/">pictures at FOSDEM 2010</a>. Disappointingly I took less than last year: only 55 instead of 132 (and even that is cheating, because that includes a picture *of* me taken by xvello). Still, better than nothing, I guess... In any case, as always at FOSDEM, I had lots of fun, and that's what counts! :)<!--break-->