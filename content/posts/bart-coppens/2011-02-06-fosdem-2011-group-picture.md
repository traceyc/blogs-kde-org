---
title:   "FOSDEM 2011 Group Picture"
date:    2011-02-06
authors:
  - bart coppens
slug:    fosdem-2011-group-picture
---
Here's this year's crossdesktop group picture of FOSDEM, featuring all your favourite developers that attended FOSDEM (and me)! \o/
<a href="http://www.bartcoppens.be/blogimages/fosdem2011_group.jpg"><img src="http://www.bartcoppens.be/blogimages/fosdem2011_group_small.jpg" /></a>
(and yes I now also see that I focussed on the building instead of the people, but it's hardly noticeble ;))

<!--break-->