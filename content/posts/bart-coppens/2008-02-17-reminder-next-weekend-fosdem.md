---
title:   "Reminder: Next Weekend == FOSDEM"
date:    2008-02-17
authors:
  - bart coppens
slug:    reminder-next-weekend-fosdem
---
Just so that you won't all forget: next weekend there's FOSDEM in Brussels. You can find the complete schedule in a handy grid layout <a href="http://fosdem.org/2008/schedule/days">here</a>. For KDE people, I guess the most interesting non-main track talks could be those in the <a href="http://fosdem.org/2008/schedule/tracks/kde">KDE devroom</a> and the <a href="http://fosdem.org/2008/schedule/devroom/crossdesktop">Crossdesktop devroom</a>.

Due to reasons beyond my control, I find myself yet again doing a talk in the KDE devroom about <a href="http://fosdem.org/2008/schedule/events/kde_koffice2_odf">>KOffice 2</a> (the original speaker is unfortunately unable to come). I hope you'll enjoy it (<a href="http://www.kuarepoti-dju.net/index.php?p=147">for</a> <a href="http://blogs.kde.org/node/3280">those</a> keeping track: I'm a big fan of LaTeX-beamer for making presentations, though the brainstorming plans I saw for KPresenter2 might make that appealing for me as well in the future).

For the KDE and GNOME people attending: we'll be doing <a href="http://fosdem.org/2008/schedule/events/kde_group_photo">KDE Group Photo</a> on Saturday noon (presumably followed up with a picture together with the GNOME people, if they come: it's not advertised on their track's schedule), it might be fun if you'd drop by! :)

Edit: this one is for pino: <a href="http://www.fosdem.org"><img src="http://www.fosdem.org/promo/going-to" alt="I’m going to FOSDEM, the Free and Open Source Software Developers’ European Meeting" /></a> *hug*<!--break-->