---
title:   "Battery status, the kernel, and Debian stupidity"
date:    2008-02-02
authors:
  - bart coppens
slug:    battery-status-kernel-and-debian-stupidity
---
So, given that I'd be on a holiday next week, I thought it would've been a good idea to do an upgrade of my Debian install on my laptop, in the hope that it'd use less power. The good news was that it seemed like it did, with slightly lower temperature (I guess due to tickless timer on AMD64 in the 2.6.24 kernel). The bad news was that I couldn't even try to quantify it at all: all my battery-measuring tools wouldn't work at all anymore. In particular, no KDE Battery Systray icon anymore, leaving me without any indication at all about how much battery I have left. Hoping it'd be just a KDE update fluke, I checked my other favourite power-related tool, powertop. Unfortunately it also failed to show any relevant information about power usage... :(

I must say, it's real fun to lose the ability to see your battery status a few days before you leave on a holiday. Particularly funny, given that at the same day that I discover this, I read a post on the planet about some people getting double battery information.

Which of course pointed me directly to the most likely cause: a change in the kernel. Some googling later, it turns out that, once again, I am pretty angry at the Debian people. It seems like they switched to the new way of doing this kind of stuff, sysfs style. Unfortunately, they completely disabled the legacy support for the procfs way of measuring things, leaving all applications that use the old way in the cold.

Now, you'd think that once you'd point this out, the Debian people would try to fix this post-hase. <a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=463253">But nooooh!</a>, apparently having people's applications and systems become useless and being userfriendly is less important to the Debian kernel maintainers than keeping their kernels clean of 'deprecated' /proc entries...

So, how do we fix this? I just wasted my time <a href="http://www.debian.org/doc/FAQ/ch-kernel.en.html">making my own debian kernel package</a>. In particular, in the make menuconfig, go to
<tt>Power management options  ---&gt;
ACPI (Advanced Configuration and Power Interface) Support  ---&gt;</tt>
And then just enable
<tt>[*]   Deprecated power /proc/acpi folders</tt>
(If you're particularly pissed, you could perhaps take it out by disabling <tt>[ ]   Future power /sys interface</tt>...)

Luckily the walkthrough was clear enough, except that the package's name prefix had slightly changed. I just had to issue a  <tt>dpkg --install ../linux-image-2.6.24_Custom.DebianFuckwits.0_amd64.deb</tt> to install the kernel, and then edit my grub file manually (bah!).

After a reboot, I had restored full usability of my power-measuring tools. Turns out my laptop seems to have an <b>increased</b> power use. No idea why, but at least now I actually <b>know</b> it is the case.

Anyway, this isn't doing pretty well in trying to prove to other people that Linux is decent to use. In particular, one of my colleagues pointed out to me that this is exactly the kind of crap (noting something doesn't work, wasting time googling for the problem, hoping someone else has described a fix, trying to apply the fix) he is so happy to have left behind him by switching to OS X...<!--break-->