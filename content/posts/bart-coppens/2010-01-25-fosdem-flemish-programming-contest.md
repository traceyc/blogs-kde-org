---
title:   "FOSDEM / Flemish Programming Contest"
date:    2010-01-25
authors:
  - bart coppens
slug:    fosdem-flemish-programming-contest
---
It's been a while, but I'd just like to bring the following 2 upcoming events in to your attention:
<ul>
<li><a href="http://www.fosdem.org/2010/">FOSDEM</a>. As always, the yearly free/open source software is being organized in Brussels again. I think there's something interesting for most of you, so be sure to have a look at the schedule (even though it is not finalized yet) :) Of course, for all the KDE people among us: don't forget the <a href="http://www.fosdem.org/2010/schedule/events/kde_group_photo"><b>KDE Group Picture</b></a>! As for people wanting to know which talks I'll probably attend, I put a tentative list on <a href="http://www.bartcoppens.be/blog/FOSDEM_2010_list_of_talks.html">my website</a>.</li>
<li><a href="http://www.vlaamseprogrammeerwedstrijd.be/">Vlaamse Programmeerwedstrijd</a> (Flemish Programming Contest). This is a programming contest, open to almost everybody: senior high school students, 'hogeschool'/university students (bachelor, master and PhD), and other people that have graduated from one of those options. This year, it's being organized at Ghent University, which happens to be where I'm working. That means I'm obviously attending it (using Haskell and/or C++). So if you ever wanted to make fun of me or humiliate me (and my teammate, of course) in a kinda-official setting: this is your chance ;)</li>
</ul>
<!--break-->
