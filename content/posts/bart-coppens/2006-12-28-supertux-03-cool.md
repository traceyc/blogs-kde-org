---
title:   "SuperTux 0.3 is cool"
date:    2006-12-28
authors:
  - bart coppens
slug:    supertux-03-cool
---
I feel it is my happy duty to make all of you code less, by mentioning that the <a href="http://supertux.berlios.de/">SuperTux people</a> created <a href="http://prdownload.berlios.de/supertux/supertux-0.3.0.tar.bz2">a new release: 0.3.0</a>. (Note that Berlios seems to have some trouble at the moment, so hopefully it'll still be working when you click the links ;)). They apparently changed most of their rendering engine and physics code, and lots of other stuff changed with it. It looks much better than the already incredible 0.1.3 version that I played a lot.

Now you don't only get the old ice world (but with a different, non-linear path through it), but also a whole second forest world! Cool new features are, for example, the fact that levels now also can go higher than a single screen, and all kinds of new tricks exploiting this extention into the second dimension. I found the wind-tubes that let Tux float in the sky extremely fun there. Also cool is the fact that Tux can now walk on slopes, as opposed to the squares you always had to jump over (he even starts to slip if the slope is steep).

They seem to have made cool use of the 'Up' key as well, to open doors, and of the 'Action' key to carry around tiles (just like you could carry the Mr Ice Guy in SuperTux 0.1), providing an extra level of fun. And talking about action, they also added scripting support, and things like trampolines, switches, upside down levels, moving objects, a backflip Tux can make (instead of jumping to the right when you're ducked), etc. I'm pretty sure I'll find out more cool things as I progress in finishing the second world :)

One small note: I'm pretty sensitive to fast changing motion on the screen, and their new Camera was moving a bit too unpredictable for my brains (it is, of course, completely predictable, but my body seemed to disagree). The fix was extremely simple for me, though, I just changed 2 lines in the Camera::update_scroll_normal function so that it tries to always horizontally center Tux in the middle of the screen. (It's written in very clean looking C++ code).

<a href="http://www.bartcoppens.be/supertux03-1.png"><img src="http://www.bartcoppens.be/supertux03-1-small.png" /></a> <a href="http://www.bartcoppens.be/supertux03-2.png"><img src="http://www.bartcoppens.be/supertux03-2-small.png" /></a><!--break-->