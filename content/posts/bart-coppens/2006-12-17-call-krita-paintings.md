---
title:   "Call for Krita Paintings"
date:    2006-12-17
authors:
  - bart coppens
slug:    call-krita-paintings
---
Now that Krita 1.6.1 has been out for a little while, I think it's time to bring the <a href="http://dot.kde.org/1162398211/">Search for Artwork</a> under the attention of the public again. In order to have some nice content for the Krita website, we are looking for people that can create some nice artwork with Krita. So if you made some nice drawing with Krita, there's always the possibility that it can get featured on the Krita site.

Of course, this benefits both sides: the artist can show his paintings somewhere other than his own page, and we can get feedback on things that Krita lacks. Feedback on bugs or lacking features is pretty important for us. It's obvious that we like bug reports, but we also like feature requests a lot! For example, if we get to know about a feature, it's often difficult to estimate how important it is to the users, or how easy it is to implement. If a user then can tell us what he needs it for (and maybe if he happens to know a bit on how it could be implemented), that makes the our job a lot easier. For example: the smudge tool. If people wouldn't have asked about it that much, we probably would not have thought about making it work with Krita (it will be in the upcoming Krita 1.6.2).

So, with the holiday season approaching rapidly, maybe all the artists among the Krita users might have some spare time to make something.<!--break-->