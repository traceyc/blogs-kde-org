---
title:   "KDE at FOSDEM 2009"
date:    2009-02-07
authors:
  - bart coppens
slug:    kde-fosdem-2009
---
So, of course there's a lot of interesting things going on at FOSDEM here. Just like last year, we had a nice group picture on the grass field next to the KDE devroom. Thanks to all people who could make it. If you look verrry closely, you can even spot the GNOME person in it ;)
<img src="https://blogs.kde.org/files/images/IMG_5464_resized.jpg" /><!--break-->