---
title:   "Konqueror and the router"
date:    2006-11-19
authors:
  - bart coppens
slug:    konqueror-and-router
---
After our previous router died after a lightning strike, we got a new one. Since that new one was active, though, I had the strangest experience: when my KDE session reloaded my Konquerors, it would often claim the hosts did not exist. That's quite a big WTF, I'd say. For some reason, all DNS requests stopped working over UDP for a small while. (Manually dig'ing with +tcp worked.)  Which is pretty annoying if your session also starts Akregator, which also wants to look up lots of hostnames.

At first, I blamed my DNS server. I then quickly moved to blaming my router or KDE, depending on my mood. Turns out, it's probably the last one. Because yesterday, a blog title caught my eye: <a href="http://endymion.ugent.be/~kromagg/blog/2006/11/16/konqueror-en-dns-requests/">konqueror en dns requests</a> (Dutch. And what are the odds of randomly finding a solution to a weird Konq problem on a blog hosted at my university! For those not understanding Dutch, he also commented on <a href="http://bugs.kde.org/show_bug.cgi?id=135230">the bug report</a>). It described my problem, and offered an easy solution: use a small and local DNS server. So I installed dnsmasq, and this morning I had no more DNS problems anymore! Yay! Let's hope it stays that way.

Now I seem to have regressed back into my pre-DNS-problem issue of '<a href="http://bugs.kde.org/show_bug.cgi?id=132587">the process for the ...url... protocol died unexpectedly</a>'. Which while annoying, doesn't even start to reach the DNS annoyingness.<!--break-->