---
title:   "The McGurk effect"
date:    2008-01-26
authors:
  - bart coppens
slug:    mcgurk-effect
---
While idly browsing through Wikipedia, I came across something very cool: <a href="http://en.wikipedia.org/wiki/McGurk_Effect">the McGurk effect</a>. Just read the Wikipedia page, and try it out with the linked YouTube video. I found the effect rather weird to experience :) (You can watch YouTube without creepy Flash plugins using ffplay/mplayer and the <a href="http://www.kde-apps.org/content/show.php/Get+YouTube+Video+(improved)?content=41456">Konqueror YouTube servicemenu</a>. The HTML for the YouTube site apparently got changed recently, so you'll have to manually patch the servicemenu script according to the last comment in that page).<!--break-->