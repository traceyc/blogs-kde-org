---
title:   "Some DCOP fun"
date:    2007-08-20
authors:
  - bart coppens
slug:    some-dcop-fun
---
Sometimes KDE makes me want to cry out of frustration (like tonight, because of Qt using select(2) instead of poll(2), breaking my akregator), but sometimes it just fills me with so much joy that I could also cry =) Like just now, when someone came in #koffice to ask if he could somehow print his presentation notes that he put in KPresenter. Unfortunately, I did not know how to do that (quite likely it's just not possible), but it took me just a few minutes tinkering around with DCOP (yes, people still use KDE3.5 technology for work ;)) to make a tiny script that just prints out all your notes!

Of course, the person already copypasted all his notes manually, but for posterity I hereby present my small shellscript: <a href="http://www.bartcoppens.be/kpresenternotes">kpresenternotes</a>. Have fun with it (or not :P).

Example output:
<tt>$ ./kspreadnotes.sh
The notes of page 0 are:
Ladielala
Notes for slide 1
The notes of page 1 are:
Having some notes fun on slide 2
Done</tt><!--break-->