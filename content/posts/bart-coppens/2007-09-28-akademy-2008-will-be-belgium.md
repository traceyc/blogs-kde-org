---
title:   "Akademy 2008 will be in Belgium!"
date:    2007-09-28
authors:
  - bart coppens
slug:    akademy-2008-will-be-belgium
---
So, <a href="http://dot.kde.org/1191001763/">Akademy 2008 will be in Belgium</a>! Yay \o/ ! Some of you might have known this already if you'd have read the planet closely (troy leaked it already...), but still I'm terribly excited to be able to say it officially :) After all, I've been asking about Akademy (the yearly KDE Developer's Conference+Hacking/BoF sessions+eV General Assembly) coming to Belgium for like 4 years already... The location will be the De Nayer Institute in <a href="http://en.wikipedia.org/wiki/Sint-Katelijne-Waver">Sint-Katelijne-Waver</a>, and the pictures I saw of the building we'll be in look pretty awesome.<!--break-->