---
title:   "FOSDEM 2009"
date:    2009-02-08
authors:
  - bart coppens
slug:    fosdem-2009
---
Wow, so yet another FOSDEM lies behind us. As always, it was quite fantastic; those who decided not to come were clearly mistaken ;)

So, other than the obligatory <a href="http://blogs.kde.org/node/3885">KDE Group Picture</a>, here are some impressions from this year. Let's start with the biggest surprise of all: Boudewijn Rempt was able to make it to FOSDEM!
<img src="http://www.bartcoppens.be/photos/Fosdem2009/tag_exportable/img_5430_jpg.jpeg" />

Here we have Thomas Zander, who was answering a Qt-related question during Jos' talk on KDE4.2:
<img src="http://www.bartcoppens.be/photos/Fosdem2009/tag_exportable/img_5448_jpg.jpeg" />

Apparently, Linus was at FOSDEM too ;)
<img src="http://www.bartcoppens.be/photos/Fosdem2009/tag_exportable/img_5589_jpg.jpeg" />


Some people had printed Wade's KDE 'promotional' pictures on postcards. Fine by me. But seeing the scary dolls 'strongly suggesting to use KDE' (complete with 'KDE is watching' above it) when you need to go to the men's room is quite unsettling :o.
<img src="http://www.bartcoppens.be/photos/Fosdem2009/tag_exportable/img_5488_jpg.jpeg" />


But, all in all, I think everyone had a lot of fun at FOSDEM:
<img src="http://www.bartcoppens.be/photos/Fosdem2009/tag_exportable/img_5543_jpg.jpeg" />
(Yes, I'm quite mean ;))

The rest of my <a href="http://www.bartcoppens.be/photos/Fosdem2009/">FOSDEM 2009 pictures</a> can be found in their usual place. As usual, if you don't like a picture of yourself (or very much <i>do</i> like a picture of yourself), feel free to contact me about it.

Things to remember when going to a next FOSDEM/conference:
<ul>
<li>Remember to change whitebalance settings when inside</li>
<li>Learn how to use a flash. I bought a new one, but using it felt like using black magic, in that I was unable to reliably predict how the end result of a particular set of settings would affect the outcome (usually, this was not good :P).</li>
</ul>

<!--break-->