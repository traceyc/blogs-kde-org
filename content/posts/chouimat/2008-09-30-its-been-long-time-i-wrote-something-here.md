---
title:   "it's been a long time since I wrote something here."
date:    2008-09-30
authors:
  - chouimat
slug:    its-been-long-time-i-wrote-something-here
---
A few weeks ago, I decided to write a small utility to
help me with my work and also to relearn Qt. This small
utility enable me to control the boot process, update
the firmware of various devices I have at home and at work.

The current version only work with a serial link, and it's
it's also hard coded for the specific devices I have.
I'm currently thinking about refactoring the current code base
so I can remove the proprietary stuff and make the thning more flexible
with different plugins, I will probably had jtag support and
maybe a nice kde4 gui ...

On another notes, it's fun to have his name on a product
that is on the shelves :D
