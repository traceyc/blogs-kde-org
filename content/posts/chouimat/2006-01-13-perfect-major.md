---
title:   "perfect major"
date:    2006-01-13
authors:
  - chouimat
slug:    perfect-major
---
You scored as Engineering. 
    
  You should be an Engineering major!
  
  Mathematics                               100%
  Engineering                               100%
  Philosophy                                100%
  English                                    75%
  Sociology                                  75%
  Chemistry                                  67%
  Journalism                                 67%
  Theater                                    58%
  Biology                                    58%
  Psychology                                 58%
  Anthropology                               50%
  Art                                        50%
  Linguistics                                50%
  Dance                                      42%


Not bad at all and pretty close to the truth ... except for maths :)
anyway back to sleep
<!--break-->