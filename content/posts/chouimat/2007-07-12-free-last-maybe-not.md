---
title:   "Free at last ... maybe not"
date:    2007-07-12
authors:
  - chouimat
slug:    free-last-maybe-not
---
Afer a few months of really bad working atmosphere (see blogs.kde.org/node/2523)
and some of legal stuff, I thought all of this was over until yesterday
when the fruitcase called me to ask me to join his new venture, offer 
which I naturally refused. I just have to say he didn't liked this answer,
I have a few recorded voicemails as proof of this :(

On brighter side, since I didn't got fully paid for all the work I did
for him, the court decided that I was the owner of the code I wrote.
Now I'm currently reviewing all of it to see what could be reused if I decide
to start my own company (still a work in progress) and what can (this is  a huge if)
be opensourced. One thing that come to mind is a CANOpen to jabber gateway and 
a nice CANOpen ioslave. There a few other things that could be interesting but 
they are incomplete and need specilized hardware ...

Slowly I think I will be able to be myself back on the track and start to have fun
again programming :D

And I must also add that the new theme of kdedevelopers is nice.
<!--break-->
