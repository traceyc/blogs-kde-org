---
title:   "when developers think they know what the users want"
date:    2006-11-17
authors:
  - chouimat
slug:    when-developers-think-they-know-what-users-want
---
Lately on the kde-core-devel there is a [http://lists.kde.org/?t=116336735900002&r=1&w=2|thread] which started innocently by asking the permission to move an application back to kdegraphics, degenerated into a small my app is better than yours type of crap and now it's getting to the a nice discussion on what users want or expect from an application. I will post my comment here since I don't have write access to this mailing list and I also don't want to keep adding to the traffic of the mailing list (and mostly I want to save precious time to the moderators :) )

From what I'm seeing in the thread we have 2 clans: the minimalistics and those "who want more powerful applications". At one point someone said if you want to edit a pdf file (probably produced by TeX or something else) you simply use kword or scribus. This doesn't work for a lot of users because I'm case (by example) I know and use LaTeX for all my documents, why because I know I can have a more professional looking document without having to learn typesetting (TeX does it for me).
I have nothing against kword, it's a nice application, or scribus and I believe they are adequate for their target audience but why I
should learn or use another applications to do simple edition or annotate a pdf. I don't mean rewriting the document by simply fixing typos and things like this? I believe there is different "level" of edition. 

From what I understand from this [http://lists.kde.org/?t=116336735900002&r=1&w=2|thread] some people want to go the gnome way, by simplifying KDE to the extreme, if so simply tell it right now so the KDE power user base can find something else.
Or maybe it's time to have 2 editions of KDE: the KDE Standard Edition for "normal users", notice I didn't say the light version, were most of the most advanced (or power user) features are disabled by default, shouldn't be hard to do with the kiosk mode and the
KDE Power User Edition, where the features that doesn't fit the needs for "normal user" are enabled. If you are a power user and you find yourself that you will never need a feature, let say the pdf edition or something like this, you will be able to disable it, and then get your pdf viewer load faster.

My points here is never assume what the user might want to do either he is a power user or an idiot. It also the reason why I don't use GNOME, they are arrogant enough to say I will never need this ... and this pissing me off.

And before I end this blog I want to say this: we all know there is a race between software developers and the universe, our job is to produce better idiotproof software and the Universe job is to produce better idiots... and in my humble opinion the Universe is currently winning it :D and those are my opinions on this.

 Thank you for reading 

