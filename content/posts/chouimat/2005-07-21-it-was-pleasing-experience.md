---
title:   "it was a pleasing experience"
date:    2005-07-21
authors:
  - chouimat
slug:    it-was-pleasing-experience
---
Lately I publicly stated my opinions about what I believe is the "ideal opensource DE and world", but some people who doesn't share the same faith or the same political agenda kind of make me rethink about my conception of the world, and I don't talk about  the trolls who commented on this website but those who sent me life threathening letter on my email accounts.  Those who did this I must thank you because you might me realise that life is more precious thant the things you belive in ... and now OSS is going the way of religious fanatics and political activist, if you think about it's the same thing.

I want to say to those who I shared some of the online life and something meet in real space, you were great and probably more friendly that a lot of people I knew for years, you didn't even complaint about my really bad english the first time and I won't say a word the first time you heard me talking. I'm really sad that I must take some distance but I can't stand those kind of attack, even if they are not serious, they might become, I know myself, when I get passionate about something I'm passionate. I also know that I'm now the most diplomatic guy, I'm too sarcastic, cynical and sometime ironic (good word? I don't care now) for my own good, but we all know that those core personality characters are impossible to change.


I know I took some professional engagement for the end of this summer, all I can say is I will keep them. since I really try to be professional, even if my comments often don't seem like I am :D

anyway I might see you again after this period of personal reflexion, i might decide to stay in the linux/kde world or maybe I will simply move on to OS X or go back to winblows ... I don't know, so see you all in the future and guys keep the good work.

And for concluse to those who flood my mailboxes with those emails, it seem you succeeded in what you want to do ... is it for now or for good even me I don't know
<!--break-->