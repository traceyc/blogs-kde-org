---
title:   "M2 once again"
date:    2005-07-16
authors:
  - chouimat
slug:    m2-once-again
---
Yesterday I was happy to get a nice news, my talk about M2 for akademy was accepted.
So I imagine it might be a wise for me to finaly say what M2 is really is. 
So here I go:
M2 is a generic framework for kde to help for systems management, application, update and security fixes deployment, user management etc ...


I started to work on it when I discovered that the management of the network
was taking too much of our time and since I also need a tool for heterogenous setup I decided to
write our own because all the competing products are expensive and tied to a certain os, Microsoft SMserver,
or to a certain linux distribution like Xandros xDMS.  What I needed is something that can
work in a linux/freebsd/netbsd/openbsd setup and be able to manage everything without having to say "which OS
I'm updating now?" The first version of the prototype (shell scripts) only managed the update of the system for all the network,
but I discovered that it can be extended to do much more.

the framework can be also used for other kind of deployment problem like the one of deploying certificate for a vpn.


Now you know. 

since this description the design changed a lot, and it's still evolving. I have currently a remote management ioslave prototype in the work, and I hope being able to finish it before the conference.

and for those who follow my blogs, i'm still working on the beamer theme ... and I will have all my summer free to work on m2 ... 
<!--break-->