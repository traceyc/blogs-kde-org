---
title:   "And the winner is ..."
date:    2005-07-27
authors:
  - chouimat
slug:    and-winner
---
I was trying to find another name for m2 for a while and I come up with some nice name here the list:
-Bakkus
-Rembrant
-Kobold
-M2

and the winner is M2, yes I will stick with it. And I even find a nice way to pronounce it "Me Too" :D

Now back to work ...

<b>UPDATE</b> Seems that the opera mail client is also called M2 :( so I decided to change the name of the framework M.2, same pronounciation (and a cookie to anyone who find the pun)
<!--break-->
