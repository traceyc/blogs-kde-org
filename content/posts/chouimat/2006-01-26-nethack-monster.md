---
title:   "nethack monster"
date:    2006-01-26
authors:
  - chouimat
slug:    nethack-monster
---
|)?!)%+
+.@%/]?
|!?[?[%
If I were a NetHack monster, I would be a mimic. I can be whatever I think you need me to be - it might look like I'm here to help you, but really you're here to help me.