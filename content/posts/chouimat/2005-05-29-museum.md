---
title:   "museum"
date:    2005-05-29
authors:
  - chouimat
slug:    museum
---
This weekend my father and my little brother came to visit me, so we decided to visit the Canadian National War Museum. It was a very interesting visit. It took us 3.5 hours to see everythings in the permanent exposition. Like every museum I visited, I noticed some "errors", not historical errors but more like technical errors, like the text didn't fit the size of the panel so it was cut by the next display case and sometimes the French text wasn't as accurate as the English one. But the most annoying thing was in nearly every display case the text had label 1) 2) 3) etc ... but the objects aren't labeled ... and since I'm not an expert on the german or allies gun used in WW2, I had to try to guess which gun (or object) goes with which description. 
Beside those little "details" it's a very very interesting museum

<!--break-->