---
title:   "GSoC: Transformation Tool for Krita (screencast)"
date:    2010-07-08
authors:
  - pegon
slug:    gsoc-transformation-tool-krita-screencast
---
Hello everyone !

Last week I finally decided to spend some time on the UI of the tool, and you will see on this <a href=http://dl.free.fr/p7Qmo5Ydt>screencast</a> that there has been some changes !

After correcting a small bug I introduced in the transform worker when adding shearing, I designed the Tool Options widget. I decided not to keep the mockup I made for my GSoC application, because I realized something vertical was more appropriate for Krita. 
In short, I added little buttons to set the rotation center to the 4 corners, the middle of the sides, and the center, and also spinboxes to display and change the current arguments of the transformation.

After that, I thought it was time to change the behaviour like it was discussed with the team : basically, we wanted the tool to show a low quality preview of the transformed pixels in real time, and let the user click on an "apply" button when he's satisfied with the current transformation, in order to transform eventually the selected pixels.
I used QImage and QTransform classes to do that, as Boudewijn suggested. I was a bit worried that even Qt transform methods should be too slow to have something smooth, but actually, it worked just fine (as you can see on the video). That was really a relief.
In the process, I broke the undo/redo actions, but I restored them just after I was finished, so everything is fine.

Finally, I added some useful constraints to the tool :
<ul><li>Hold shift while rotating to rotate by steps of 15°.</li>
<li>Hold shift while scaling to keep aspect ratio.</li>
<li>Hold shift while moving selection to move along X or Y "global" axis only. Hold shift+ctrl to move along X or Y "object" axis only.</li>
<li>Hold shift while moving rotation center to move it along X or Y "local" axis only. Hold shift+ctrl to move it along X or Y "global" axis only.</li></ul>

I made the handles a little bit bigger because they weren't easy to catch.
I decided to make a <a href=http://dl.free.fr/p7Qmo5Ydt>screencast</a> because people on the team had some trouble finding where to click to do this or that with the tool.

<img src=http://lh5.ggpht.com/_KZOijM4jJGU/TFbC1BhWaeI/AAAAAAAAACk/Lgi3ivBn2GQ/s640/krita_screenshot.png align=middle></img>