---
title:   "GSoC: Transformation Tool for Krita"
date:    2010-06-21
authors:
  - pegon
slug:    gsoc-transformation-tool-krita
---
This is my first blog entry about my <a href=http://socghop.appspot.com/gsoc/student_project/show/google/gsoc2010/kde/t127230762010>GSoC project</a> for Krita. <br>

Because i was busy doing a school project until June 11, i actually began working on the transformation tool last week. <br>
The aim was to rewrite most parts of the old transformation tools, in order to continue the project on a solid base, and with my own code, to make the job easier. I also wanted to make the tool perform transformations in real time, not just when the user releases the mouse button. <br>

What has been done :
<ul>
	<li>The computation of the scale factors/rotation angle/translation vector from the mouse movement has been rewritten.
	<li>The tool now updates the canvas properly after each transformations, so there are no more artefacts visible.
	<li>I added a temporary workaround to move the selection, because the move method for KisPaintDevice is apparently deprecated.
	<li>The undo/redo commands have been fixed, using the new system written by dmitryK last week (though it isn't working completely, because of a reported bug in KisDoc2 + QUndoStack)
</ul>

I spent a few days trying to perform the transformations in real time. I wanted to see if indirect painting could help me do that, but i couldn't even manage to move a selection without blitting a rectangles, so I decided i'd better go on with the project for now and give it another try later.

That's it for now ! I hope to be more productive this week, with real new features implemented.