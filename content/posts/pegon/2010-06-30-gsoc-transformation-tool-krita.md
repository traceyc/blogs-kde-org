---
title:   "GSoC: Transformation Tool for Krita"
date:    2010-06-30
authors:
  - pegon
slug:    gsoc-transformation-tool-krita
---
Hi guys,

Maybe it's a bit late to talk about what I did last week, but anyway : 

After fixing some bugs in the transformation tool, I decided to add the possibility for the user to change the center of rotation.
It was a bit tricky, because I wanted to express this rotation (around the current center) as a composition of a rotation around the original center of the selection and a translation (which is possible). Consequently, a succession of rotations around different center can be reduced to only 1 rotation around the original center and 1 translation.
I used complex numbers for the calculation (quite convenient for 2D transformations), and I was pleased to see that complex numbers were standard C++ objects.

After that, I decided to implement horizontal and vertical shearing. When doing some researches, I found out that such transformations weren't commutative, so I had to make a choice : horizontal shearing is applied before vertical shearing. Thus, even though it may appear a little bit strange, shearing vertically when there is already a horizontal shearing changes the angle of the left/right sides of the parallelogram.
I compared it to gimp, and I noticed that it refuses to compose horizontal and vertical shearing : if you want to do both, you have to shear horizontally using the shear Tool, then close it, reopen it, and shear vertically (which produces the same result as our transform tool). That means what I do is normal.
I also found out that in the Transform Worker (the class which applies the transformation to the selected pixels), shearing wasn't implemented ! It wasn't expected, because it takes shear factors in argument, and there is no mention of it in the code.
Fortunately, the worker was essentially capable of shearing with just a few modifications, because it used shearing to perform rotations (rotations can be done efficiently with 3 particular shearings).

Then I did some code cleaning / restructurations.
I also prepared everything for perspective transformation (added a 3rd coordinate, 3D rotations, perspective projection calculation), which will be added later. In fact, while there is no ambiguity on how to perform 3D rotations, there are different ways to do perspective projections, and I want to see how Qt does it first (with QMatrix4x4), because the idea is eventually to use QImage and Qt transformations to display a preview of the transformed selection.

That's it for last week !
