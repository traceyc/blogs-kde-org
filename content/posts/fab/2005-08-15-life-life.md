---
title:   "Life ... is life"
date:    2005-08-15
authors:
  - fab
slug:    life-life
---
Congratulations to <a href="https://blogs.kde.org/blog/457">Stephan</a> on his <a href="http://blogs.kde.org/node/1321">KDE-related job</a>. My <a href="http://blogs.kde.org/node/1330">condolences to Jess and Ian</a>. Take care!<!--break-->