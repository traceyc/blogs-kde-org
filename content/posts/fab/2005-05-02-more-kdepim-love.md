---
title:   "More KDEPIM love!"
date:    2005-05-02
authors:
  - fab
slug:    more-kdepim-love
---
I asked Basse to make a nice logo and banner for the <a href="http://pim.kde.org/development/meetings/nlpim1/index.php">upcoming KDEPIM meeting</a>. The cool little dragon seems to be crazy about KDEPIM (so are the PIM developers about Konqi I heard). 
<IMG src="http://www.kde.nl/mediakit/banners/konqi/konqi-kdepim/150x150_4.png" border="0">

An article about the upcoming KDEPIM meeting can be read in the Dutch e-zine <a href="http://www.livre.nl/">Livre</a>. That article still mentions Tilburg as the location and that's not correct. Now we moved the whole meeting <a href="http://www.kde.nl/agenda/2005-kdepim-meeting/achtmaal.png">Achtmaal</a>, a small village situated in the south of the Netherlands. We will be using the facilities offered by the convention center <a href="http://www.wallsteijn.nl/">Wallsteijn</a>. They even have a small bar where the hackers can have inspiring drinks :)

<IMG src="http://www.wallsteijn.nl/images/fotos/praathuisje.jpg" border="0">
<!--break-->