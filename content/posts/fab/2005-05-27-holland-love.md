---
title:   "From Holland With Love"
date:    2005-05-27
authors:
  - fab
slug:    holland-love
---
I am currently sitting down in the garden of <a href="http://www.wallsteijn.nl/html/index.html">Annahoeve</a> (yes we have wireless). As  promised I show you some first pictures of the <a href="http://pim.kde.org/development/meetings/nlpim1/index.php">Dutch KDEPIM meeting</a> which is currently taking place here. 

<a href="http://www.kde.nl/agenda/2005-kdepim-meeting/pics/2705/IMG_1317.JPG">Ben and Lutz</a> were discussing the whole afternoon in the garden. The <a href="http://www.kde.nl/agenda/2005-kdepim-meeting/pics/2705/IMG_1318.JPG">hacker room</a> were <a href="http://www.kde.nl/agenda/2005-kdepim-meeting/pics/2705/thumbs/IMG_1322.jpg">some discussions</a> took place! And of course <a href="http://www.kde.nl/agenda/2005-kdepim-meeting/pics/2705/thumbs/IMG_1321.jpg">enjoying the nice weather</a>. Gallery <a href="http://www.kde.nl/agenda/2005-kdepim-meeting/pics/2705/images.html">here</a>!

<!--break-->