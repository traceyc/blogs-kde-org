---
title:   "Amarok getting attention from Podcasting community"
date:    2005-09-25
authors:
  - fab
slug:    amarok-getting-attention-podcasting-community
---
The website <a href="http://www.podcastinfo.nl/">Podcastinfo.nl</a>, an initiative by Bernard Flach, <a href="http://www.podcastinfo.nl/2005/09/11/podcast-support-voor-amarok/">mentions</a> <a href="http://amarok.kde.org/">Amarok</a> as an excellent player which comes default with podcasting support.<!--break-->

