---
title:   "Missing Applications!?"
date:    2005-07-17
authors:
  - fab
slug:    missing-applications
---
<a href="https://blogs.kde.org/blog/457">Beineri</a> talks in his <a href="http://blogs.kde.org/node/1243">latest blog entry</a> about some applications we are missing in KDE. Please check <a href="http://www.kde-apps.org/">KDE-apps-org</a> where you can <a href="http://www.kde-apps.org/content/show.php?content=16736">find</a> a blog client called <a href="http://pyqlogger.berlios.de/wiki/index.php/Main_Page">PyQLogger</a>. This blog client is written in PyQt by <a href="http://yukelzon.blogspot.com/">Reflog</a> and Xander (<a href="http://linux01.gwdg.de/~pbleser/rpm-navigation.php?cat=Network/pyqlogger/">suse rpm</a>). Also I like Bram's suggestion of having an application like <a href="http://www.workrave.org/">workrave</a>. Perhaps it is time for <a href="http://bram85.blogspot.com/2005/05/stop-for-15-seconds.html">Aktivity</a> now!<!--break-->