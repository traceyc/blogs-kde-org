---
title:   "Apologies"
date:    2005-07-26
authors:
  - fab
slug:    apologies
---
I apologize to Robert Stoffers for not sending his book prize which he won in the <a href="http://dot.kde.org/1097912170/">KDE Docs Competition</a> last year. I forgot about the whole thing because I was consumed with other non-KDE stuff. But this is no excuse. Robert <a href="http://www.oreilly.com/openbook/freedom/">your book</a> is on its way to Australia and I owe you a drink. <!--break-->