---
title:   "Spotted!"
date:    2005-05-04
authors:
  - fab
slug:    spotted
---
Spotted by <a href="http://dev-loki.blogspot.com/2005/04/fab-has-blog.html">Loki</a> aka Pascal. 

Pascal is one of the <a href="http://www.fosdem.org/">FOSDEM</a> dudes. Honestly all these FOSDEM organising people are really doing an amazing job and should get more credit. Pascal especially kicks ass. Not only does he provide a lot of <a href="http://linux01.gwdg.de/~pbleser/">rpms</a> for the SUSE community... But also at this years <a href="http://dot.kde.org/1109807746/">FOSDEM</a> he guided a group of 20 KDE-hackers through Brussels to a <a href="http://jriddell.org/photos/2005-02-26-fosdem/hacking-michael-maarten-bram.jpg">secret hacking location</a> :) (FOSDEM closes its doors after 18:00). He was dead tired I remember but determined to bring us there. Thank you Pascal! 

Btw .. have you become a daddy recently?