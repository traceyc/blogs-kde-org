---
title:   "Local groups on the move!"
date:    2005-05-14
authors:
  - fab
slug:    local-groups-move
---
<p>I applaud the Turkish KDE community for <a href="http://dot.kde.org/1116048757/">founding</a> <a href="http://www.kde.org.tr/">KDE Turkey</a>(cool website btw). All these local groups really do an enormous service to KDE by creating awareness in their own country.</p>

<p>I truly hope this inspires more people to form a local KDE group. <b>You</b> really <i>can</i> make a difference!</p>

<p>Also I invite KDE Turkey to work together with other local KDE groups. For example by translating the series <a href="http://dot.kde.org/1115581145/">Application of the Month</a> to your native language.</p>

<p>Please contact <a href="mailto:fabrice@kde.nl">me</a> if you are interested.</p>

<!--break-->