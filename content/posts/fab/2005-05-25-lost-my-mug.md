---
title:   "Lost my mug"
date:    2005-05-25
authors:
  - fab
slug:    lost-my-mug
---
At work somebody offered to bring me some coffee and used my <a href="http://www.kde.org/stuff/images/kde_tasse_s.jpg">cool KDE mug</a> for it. I forgot who offered me to bring the coffee and worse .. he did not show up with mug or coffee. I suspect it is somewhere in this building serving its purpose. Damn, now I really need to go to <a href="http://conference2005.kde.org/">Akademy</a> to get me a new one. <!--break-->