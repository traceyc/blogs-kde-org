---
title:   "Trolltech sponsors upcoming KDEPIM meeting as well"
date:    2005-05-23
authors:
  - fab
slug:    trolltech-sponsors-upcoming-kdepim-meeting-well
---
Stephan, you forgot <a href="http://blogs.kde.org/node/view/1096">to mention</a> that Trolltech also supports the upcoming <a href="http://pim.kde.org/development/meetings/nlpim1/index.php">KDEPIM meeting in the Netherlands</a>.<!--break-->