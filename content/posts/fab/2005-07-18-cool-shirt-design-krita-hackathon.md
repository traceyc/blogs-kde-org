---
title:   "Cool Shirt Design for Krita Hackathon"
date:    2005-07-18
authors:
  - fab
slug:    cool-shirt-design-krita-hackathon
---
<a href="http://www.valdyas.org/fading/index.cgi">Boudewijn</a> mailed me about a <a href="http://www.koffice.org/krita/">Krita</a> hackfest which is taking place soon in the Netherlands (we keep you posted). It would be a cool and fun idea to have some shirts the hackers could wear during this hackfest. So I contacted the <a href="http://www.kde-artists.org/main/">KDE-Artists</a> on IRC (#kde-artists). <a href="http://pinheiro-kde.blogspot.com/">Nuno</a> promptly took the job of creating a <a href="http://pinheiro-kde.blogspot.com/2005/07/do-you-guys-want-plasma-mug.html">cool design</a> for a shirt. Thanks Nuno!

<p align="center"><img class="showonplanet" src="http://img278.imageshack.us/img278/3374/krita25cl.png" border="0" width="500" alt="Image Hosted by ImageShack.us" /></p>


Boudewijn, you like the design of that shirt?  If so ... <a href="http://www.kde.nl/">KDE-NL</a> should go ahead and order them.<!--break-->