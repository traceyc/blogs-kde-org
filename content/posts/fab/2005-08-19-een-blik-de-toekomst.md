---
title:   "Een blik in de toekomst ..."
date:    2005-08-19
authors:
  - fab
slug:    een-blik-de-toekomst
---
<a href"http://www.kde.nl/future/">Een blik in de toekomst</a> is a corner on our Dutch KDE website were we offer Dutch translations of the series 'This Month in SVN ...' done by our lovely <a href="http://www.canllaith.org/blog/">Jess</a> . Currently the <a href="http://www.canllaith.org/svn-features/17-08-05.html">August edition of This Month in SVN</a> now also has a <a href="http://www.kde.nl/future/20050818/">Dutch translation</a> thanks to <a href="http://bram85.blogspot.com/">Bram</a> and Jos*. 


* Joske, where is your blog?

UPDATE: <a href="http://nowwhatthe.blogspot.com/">Jos</a> indeed has a blog. Use it Jos!

<!--break-->