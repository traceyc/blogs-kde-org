---
title:   "[GSoC] [KDE-Edu][KHangMan] Evolution of KHangMan - GSoC 2010"
date:    2010-08-19
authors:
  - adamrakowski
slug:    gsoc-kde-edukhangman-evolution-khangman-gsoc-2010
---
GSoC 2010 is finished. I would like to share my opinions with you.

It was great summer. I want to thank you all, especially Anne Marie Mahfouf, for trusting me I am good candidate. I learned a lot. Working with kdelibs and qt was an exciting experience to me. Trust me, I learned a lot. I believe that my little contribution will improve the quality of KHangMan. Hope so :)

Thank you annma, dfaure, djustice, hubnerd, Nightrose, pinotree, reavertm, SadEagle, sandsmark, sreich, thiago and others not mentioned for you help, support and answering to my questions. Anne Marie, you were very, very good mentor :)

Meanwhile I had HDD failure with complete data lost, so I had delay. However, I want to stay with KHangMan after GSoC, so this delay doesn't seem to be important. Below is the summary of successes and new TODOs.

<p>Successess
<ol>
<li>Recent files support</li>
<li>XML-based KHangMan themes. Now artists can easly create new themes and add them to application without code modification. Anne Marie wrote a document about creating 'new' themes.</li>
<li>Spiral mode - order of words to be guessed depends on user's individual results. Words hardest to user are displayed much often, than easier. Each user has his own list of words hardness. Spiral mode stores info in XML file. The files below show the way of activating Spiral mode and sample file with user's results. Words whose haven't been ever guessed aren't listed in XML file.<img src="http://img341.imageshack.us/img341/1504/spiralb.jpg" />
<img src="http://img809.imageshack.us/img809/4381/spiralxml.jpg" /></li>
<li>Some bugs were found</li>
<li>Some bugs were patched</li></ol></p>

<p>Things waiting to be done<ol>
<li>Fixing saving recent files list, when applications is closed by pressing X</li>
<li>Cleaning hangovers of hard-coded themes. Probably prefs.h has to be cleaned</li>
<li>Patching switchable hangman images. Many bugs were found.</li>
</ol></p>

Thank you all for this GSoC! :)
Now I'm taking some days off. Expect next part of my questions (and commits ;) ) for about 1.5 week.

Thanks!