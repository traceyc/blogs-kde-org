---
title:   "KHTML: a position statement"
date:    2007-10-24
authors:
  - sad eagle
slug:    khtml-position-statement
---
I guess sometimes one has to be direct. So, here is what I do and do not believe in:

I am not opposed to:

(1) Dropping our tree in favor of Apple's in general. I am quite aware of the tons of good things they've done (along with a few things I thought were poor decisions); however, I would only want this to happen after some concerns are addressed, and I, perhaps naively, expect that all those people going around talking about how it would be great, and how all those people and businesses are involved would lend a hand. I am a busy guy and my priority is to get things in shape for 4.0.

The truth is, we have done something like this with JavaScript. And why did it happen? In large part because George Staikos did a lot of the heavy work, and convinced me to help. He believed it was right, and he made it happen by working within the community, and doing some of the skull-numbing portions. Of course, it was easier there, due to smaller, less active, codebase, and since regression testing is more robust for programs than for mages. However, that didn't result in a single repository, but rather a close working relationship.

(2) Working with Apple. I do it in JS all the time. See above. We have a pretty good relation with them, where the ideas bounce back and forth, get developed in one repository, and then get incorporated into an another, usually getting improved in the process. It's an unorthodox setup, perhaps, but it lets us work at our own paces and styles and have some reasonable differences  
based on those in our goals and value systems.


So what are my concerns, then?

I am opposed to:

(1) Dropping our source tree w/o a good undestanding of what's being regressed.

(2) Dropping our source tree w/o being sure that the interests of KDE community are going to be represented, and that the development setup will not be isolated from KDE contributors. 
I do not think our interests can survive long term if we don't provide a way for people to get interested in web stuff when they originally didn't plan of it. This should actually not be too hard these days, because a lot of Apple people have earned our utmost respect, unlike some of the folks alluded to by the very last bullet

(3) Dropping our source tree in a way that drops API compatibility. It doesn't matter what tree the renderer is based on, one can still provide a libkhtml that's backwards compatible for all applications. The only thing that would cause problems for doing that would be using whatever TT ships, since it would have a different API and hide the internals.

(4) Having to rely on someone else to get bugfixes out to our users (or more importantly security fixes). Call me selfish, but if I spend a weekend fixing something, I want to know that it'll get to KDE users by a certain KDE release, and not when TrollTech feels like shipping a release. I want to be able to provide improvements with each KDE release, I don't care about versions of Safari or Qt including it or not. 

I am more than disappointed by:
(1) Attempts by people who have never touched KHTML to bypass concerns of the active maintainers and contributors by misleading PR campaigns, such as articles making definitive statements on future of something, written without asking people working on it. Or, by defining maintainer as "someone who didn't touch something for 3 or more years".
<!--break-->