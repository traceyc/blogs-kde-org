---
title:   "Canvasing the vectors"
date:    2007-10-16
authors:
  - sad eagle
slug:    canvasing-vectors
---
As some of you may have known, KHTML in trunk has support for the &lt;canvas&gt; element. Unfortunately, it was based on some very old and borderline insane Apple code, which meant that except for the nice graphics bits written by Zack, it was all wrong, and when it worked, did so mostly by accident.

Well, not any more. Over Friday evening and parts of Saturday I rewrote most of it, doing my best to bring it in line with HTML5. Allan helped out by making it parse right. Well, it works pretty well now, good enough for the obligatory screenshot:
[image:3032]

Yes, there are still bugs. There are some missing features (some due to Qt limitations, some due to me thinking they are horrific ideas, and lots due to me not being sufficiently familiar with modern <strike>inefficient</strike> vector graphics), but it should all be incremental from this.


<!--break-->