---
title:   "Kivio features that is on my todo list"
date:    2003-07-05
authors:
  - psn
slug:    kivio-features-my-todo-list
---
Ok I thought I would start with making my intentions for the future of Kivio clear. Well at least the things that are most visible to the user... :)
<br/><br/>
<dl>
<dt><b>Autorouting connector</b></dt>
<dd>
The connector should automaticaly try to calculate the best route on the canvas. This might take awhile and will probably start with something earier like the connectors in dia for example.
</dd>
<dt><b>More advanced text support</b></dt>
<dd>
Support for richtext and inlineediting (ie edit the text directly on the canvas). This is a project that will take some time I think...
</dd>
<dt><b>Line styles</b></dt>
<dd>
Ah, a simple thing at last! :) This should be implemented as fast as the feature freeze is lifted.
</dd>
<dt><b>Better arrowhead support</b></dt>
<dd>
Change the code so that the arrowheads does not need to be hardcoded into kivio...
</dd>
</dl>
<br/>
The above list is not in any special order. And there is absolutly no promises attached to it. :)