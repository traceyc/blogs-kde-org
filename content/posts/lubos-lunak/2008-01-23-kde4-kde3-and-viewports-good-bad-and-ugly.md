---
title:   "KDE4, KDE3 and viewports (the good, the bad and the ugly)"
date:    2008-01-23
authors:
  - lubos lunak
slug:    kde4-kde3-and-viewports-good-bad-and-ugly
---
As of now [*], I hereby declare that KDE4 kind of supports viewports[^] (that is, the implementation of virtual desktops that Compiz and probably practically nobody else uses). Which means it possibly still sucks a bit here and there [x], but it's at least as good as in GNOME. In fact, after little checking[!], it appears that it is a bit better (try e.g. dragging a window in GNOME pager with Compiz running). Strange, I remember loads of people complaining about KDE, but not a single case about GNOME - maybe I just haven't noticed, or GNOME has less features that can break in the first place, or maybe that's what we here call 'one-eyed the king in the land of the blind'[-]. Anyway. The good news for KDE developers is that they don't have to care - viewports are mapped to virtual desktops by libraries[#] and only the pager seems to need few small tweaks. That in fact seems a much better solution than the KDE3 way of actually really trying to support viewports (I think I feel shame remembering I supported this and thought it was a good idea - one never stops learning). Which means a backport to KDE3 would be problematic, maybe I'll try for openSUSE11.0, but I'm not very confident that's a good idea. We'll see.

Time for the small print:
[^] Viewports suck.
[*] That actually means next Monday, when I enable one small check in pager that requires new kdelibs functionality added today.
[#] Well, at least the theory is that viewports can be transparently mapped to virtual desktops, reality may possibly decide to disagree on some aspects.
[x] Viewports will never be perfect [.].
[.] And, frankly, I'm not even that interested in making the support as good as possible[_]. Despite what some people might probably think, I'm still human and reasonably sane[y]. Years of working with Xlib and other things probably have increased my pain threshold, but I can still only take so much. Taking care of some minor details would be a major pain.
[y] Although I admit protecting others from these insanities may have some unpleasant effects. Caution suggested when approaching.
[_] That said, the support should be good enough, and, as said, probably better than GNOME's at the time being.
[!] I checked with GNOME shipped with openSUSE10.3, so maybe that's improved since then[?].
[?] I noticed that the SUSE packages for the related functionality in GNOME actually have patches with some viewports code added[/]. I wonder how it works elsewhere.
[/] It seems GNOME basically has code path for virtual desktops and then the same again for viewports. I guess Havoc wouldn't like that[@].
[@] There are many things about Metacity design I don't agree with, but I think I strongly agree that having both virtual desktops and viewports is crackrock[$].
[$] Well, here I wanted to note that this is a quote from Metacity's README, but while apparently many things are crackrock according to it, this subject seems only doesn't make sense, so that's me saying it, ok[+]?
[+] Well, I guess it saved some work in Compiz, but I have to wonder if the idea of going with viewports was really worth it. 
[-] No warranty included at all.

<!--break-->
