---
title:   "Speaking of fontconfig bugs ... and using unstable versions"
date:    2005-11-18
authors:
  - lubos lunak
slug:    speaking-fontconfig-bugs-and-using-unstable-versions
---
In case you have upgraded your fontconfig packages to those I provided in <a href="http://blogs.kde.org/node/1495">my blog entry about new fontconfig version</a>, you may have run into <a href="http://bugs.kde.org/show_bug.cgi?id=116176">KDE bug #116176</a> with the KDE3.5rc1 packages. Well, I warned you. That sometimes happens with unstable versions. I suggest you downgrade again or do your own build of newer fontconfig version or do whatever you want to do, but I'm not going to provide newer packages. It's still an unstable version after all.

Speaking of fontconfig bugs, with my current optimized KDE build I use for examining KDE startup performance, 1/3 (yes, that reads one third) of the startup time (with warm caches) is spent in fontconfig. And that's already this new unstable version of fontconfig with the better caches *sigh*. Fontconfig apparently still needs a lot of work.

Just in case you don't believe me, <a href="http://ktown.kde.org/~seli/performance/kde_opt_start3_opt_vis_prel.txt.gz">here</a> is a gzipped file for <a href="http://www.daimi.au.dk/~sandmann/sysprof/">sysprof</a>. Download, gunzip, load into sysprof and see for yourself. Note that those 72.45 is actually 100%. And note that there's QFontDatabase::findFont() with 28.06 (==38.7%), which, if you expand the tree for it, is almost completely spent in fontconfig. Don't get me started on how KDE is so slow and bloated because we KDE developers don't care about performance.
<!--break-->
