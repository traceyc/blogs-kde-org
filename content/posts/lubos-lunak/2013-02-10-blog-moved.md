---
title:   "Blog Moved"
date:    2013-02-10
authors:
  - lubos lunak
slug:    blog-moved
---
I've moved my blog to <a href="http://llunak.blogspot.com">http://llunak.blogspot.com</a>. As I no longer focus on KDE (and even previously my blogs were often more about openSUSE than KDE), I've moved to a more generic blog site. KDE-related posts will still be aggregated on Planet KDE.
