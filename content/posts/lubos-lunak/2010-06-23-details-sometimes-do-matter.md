---
title:   "Details that sometimes do matter"
date:    2010-06-23
authors:
  - lubos lunak
slug:    details-sometimes-do-matter
---
<p>
Some things are really really tiny details, yet they can be annoying in way. Something that's been occassionally bugging me is that fact that KDE uses the same wallpaper as KDM background, the splashscreen background and desktop background, yet depending on the screen resolution it may not be exactly the same background - during login the picture may stretch or shrink at certain points. The times when decent monitor screens had a 4:3 ratio are a thing of the past, starting with LCD makers making 5:4 "narrow-screens", then changing their minds and making 16:10 or 16:9 wide-screens. The choice of screen resolutions is not that limited either and that means that the wallpaper has to be scaled ... and that was the problem. Plasma has code to select how to do the scaling, KSplashX has code for that and KDM has code for that, and yes, you guessed it, it's always a different code. So unlucky resolutions get different wallpapers from different code. Since I actually spent some time in the past trying to make the login as seamless as possible, this indeed made me twitch whenever I saw it.</p>
<p>
Seeing this again while testing openSUSE 11.3 made me finally spend the time to patch the openSUSE package to use the same selection code in all the three components. We really lack polish in so many places :(. But now it looks like the change is almost not there - there's just a progressbar and logo shown during startup and that changes to the desktop. With compositing enabled there would be also the fade-in animation.</p>
<a href="http://www.youtube.com/watch?v=1ue4r2vYFGk"><img src="https://blogs.kde.org/files/images/p.preview.jpeg"></a>
<p>
Seeing that 4.4's KDM had no support for differently sized wallpapers, I was about to submit a copy of Plasma's code there when I noticed that trunk has some code for it. Of course, different from the rest again. Also, the login sequence is basically just lucky to be so smooth. The splashscreen is supposed to stay visible until Plasma is ready with its wallpapers and panel layout. And there is code in KSMServer to ensure this. And Plasma uses it. Yet it's apparently not used properly - during the first login, when there is more setup to be done during login, it's perfectly possible to see how the panels are set up. Well ... maybe in time for openSUSE 11.3 + 1.</p>
<a href="http://en.opensuse.org/openSUSE_11.3"><img src="http://counter.opensuse.org/11.3/small" border="0"/></a>
<!--break-->
