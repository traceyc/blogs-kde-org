---
title:   "KWin vs Firefox (or why there is the word manager in \"Window manager\")"
date:    2009-06-12
authors:
  - lubos lunak
slug:    kwin-vs-firefox-or-why-there-word-manager-window-manager
---
I found out today that two of my colleagues in the office have the same problem with Firefox - when clicking on a link in a mail client, their open Firefox is brought to the current desktop from wherever it was before. Rather annoying I guess (especially for the normal mode of operation with Firefox, keep-it-running-all-the-time-on-its-virtual-desktop), and I bet they've lived with that for quite some time already.

And handling this can be so simple. One of them uses KDE, so it was just switching to Firefox, Alt+F3/Advanced/Special Window Settings, enabling Desktop there, setting it to Force and choosing the virtual desktop it's supposed to be pinned to. Problem solved (or rather worked around, of course). The other one uses GNOME and I have no idea how to help him - Metacity itself does not have any such "advanced" functionality and Devil's Pie probably can't force settings. Bad luck. Maybe one really really long <a href="http://bugzilla.gnome.org/show_bug.cgi?id=482354">bugreport</a> in GNOME bugzilla has a trick somewhere (the "fixed" resolution is unlikely to be it though, since it breaks other things).

The lessons learned could be: 1) It can be useful to have ways to handle exceptions. 2) KWin can do quite a lot, just check out the window-specific settings, and it can do it because as a window manager it should have the ultimate say about what happens. 3) If you refuse to aknowledge that the window manager is a manager and try to outsmart it or roll your own in your application, it may hurt when you shoot the foot.

<!--break-->
