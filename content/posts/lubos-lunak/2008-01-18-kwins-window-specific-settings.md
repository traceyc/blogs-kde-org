---
title:   "KWin's window-specific settings"
date:    2008-01-18
authors:
  - lubos lunak
slug:    kwins-window-specific-settings
---
Today, in one user forum, I noticed somebody saying that they use different virtual desktops for different things and therefore they'd want different panel for each desktop. Our <a href="http://bugs.kde.org/show_bug.cgi?id=79531">bug #79531</a>, more or less. One interesting note is that the user uses GNOME, but would be willing to switch to whatever provides this feature.

Kicker does not support this, nor does GNOME panel it seems, and I doubt any panel actually can do this. However, since one can create multiple panels with Kicker, it should be sufficient to put each of them on a different virtual desktop. Kicker can't do this, but should be this allmighty window manager thingy. <i>Alt+F3/Configure Window Behavior->Window-specific settings</i>, click <i>New</i>, click <i>Detect</i>, select a Kicker panel, confirm and then apply any desired changes - in this case it's going to the <i>Geometry</i> tab, enabling <i>Desktop</i>, setting it to <i>Force</i> and choosing the right virtual desktop.

[image:3211 size=original]

That should do it. Except that, in this case, it doesn't, as KWin cannot uniquely identify the panels. They all claim the application is Kicker, but the role (<i>"xprop | grep ROLE"</i>) is always ExtensionContainer :( - object name (<i>QWidget::setWindowRole()</i> for Qt4) for all of them is the same and I had to <a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kicker/kicker/core/container_extension.cpp?r1=594139&r2=763066">fix it in SVN</a>. Now it works. Eventually, when KDE4 development becomes a bit less exciting again :), I plan to put code somewhere in kdelibs that would check for this and annoy people.

And there are other tricks one could do with KWin's window-specific settings, so it's a pity that the help for it is very short and unsufficient and I haven't found time yet to write good docs for it (well, and I'm lame at that, too). How about, if there was somebody who can't code, but would like to contribute to KDE anyway, created the docs for it ;)? Interesting topic for many people to read, and <a href="http://i18n.kde.org/docs/">docs howto</a> should have info on how to make that part of KDE. I'll answer any questions necessary. Any takers ;)?

PS: The widget style in the screenshot is the B3 style (variation of the so-called Highcolor). Since I'm already naively asking for things, could somebody port that to KDE4? I'm bad at styles too.

<!--break-->
