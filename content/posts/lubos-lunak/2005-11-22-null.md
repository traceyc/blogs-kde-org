---
title:   "NULL"
date:    2005-11-22
authors:
  - lubos lunak
slug:    null
---
From the <a href="http://pim.kde.org/development/coding-korganizer.php">KOrganizer coding style guidelines</a>: <i>"A null pointer is 0, not 0l, 0L or NULL. Once again, this is C++, not C."</i> Hmmm. If you get bored sometimes, try to compile with gcc something like <i>"void foo( int ); ... foo( NULL );"</i> and see what happens. And then what happens with <i>"foo( 0 );"</i> (and yes, I remember fixing a bug in KDE caused by this). Ok, NULL is not what it really should be, but C/C++ have many small weird things (ever wondered why binary operators | and & have so stupid priority? It's called backwards compatibility lasting for more than 30 years). Moreover, I wonder, why are symbolic constants C but not C++?
