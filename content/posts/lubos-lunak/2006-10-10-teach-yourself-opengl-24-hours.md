---
title:   "Teach yourself OpenGL in 24 hours"
date:    2006-10-10
authors:
  - lubos lunak
slug:    teach-yourself-opengl-24-hours
---
Near the end of Akademy, when it was finally short enough on talks, BoFs and whatnots to leave some time for serious hacking, after staring at the source of <a href="http://www.mail-archive.com/compiz%40lists.freedesktop.org/msg00023.html">glcompmgr</a>, I finally decided that this OpenGL compositing thingie cannot be <i>that</i> hard. And, 24 hours (with some sleep in the middle :) ) later, after staring at the glcompmgr code more, using code from it and searching in the OpenGL Redbook, I had this:

[image:2439 size=original]

Great, isn't it? I don't get it why Coolo didn't seem to be excited about it when I showed him ;). I could understand when he wasn't excited when I had showed him the same with XRender in Trysil, but this one is with OpenGL, so it must be cool, right :) ?

Anyway, glcompmgr was really of tremendous help. I don't think I would have ever gotten it while staring at Compiz sources, but glcompmgr was simple, right to the point and I didn't have to mess with Xgl. There were already enough challenges, no need to add more (have I already mentioned that I'm really lame when it gets to graphics?).

Right now, the hard part of having some basic system to build on seems to be done. Now comes the hard part of making it work on more systems than just my machine with current beta nVidia drivers, the hard part of designing the effects framework (modelling it after Compiz seems to make a lot of sense currently) and very likely other hard parts (no idea if there will be any easy parts). If somebody feels like trying whether it works on their machine, the source is in the <a href="http://websvn.kde.org/branches/work/kwin_composite/">kwin_composite</a> branch and the place for the patches is <a href="https://mail.kde.org/mailman/listinfo/kwin">kwin@kde.org</a>.

Oh, and the mandatory wow-screenshot:

[image:2440 size=original]

It's actually just a cheap trick with manually adding in few OpenGL calls just to make it visible that it's done with OpenGL, nothing impressive about it. The good thing is that I'm already able to get basic transformations right after only at most five attempts and that the OpenGL Redbook was only about 400 pages without appendices.
<!--break-->
