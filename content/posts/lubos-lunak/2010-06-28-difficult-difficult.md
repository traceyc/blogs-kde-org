---
title:   "Difficult, difficult..."
date:    2010-06-28
authors:
  - lubos lunak
slug:    difficult-difficult
---
<p>
It is interesting to notice what is sometimes seen as difficult. "It's too hard for me, I can't do that." "I'll never be able to do that, that's nothing for me." Like if most things could be done instantly just by snapping one's fingers. They instead require all these tedious things like effort, trying, learning, practicing and so on. The funny thing is, figuring out things in the IT area is not really that demanding. Wanna write a Plasma applet? There's a step-by-step <a href="http://techbase.kde.org/Development/Tutorials/Plasma/GettingStarted">tutorial at Techbase</a>, just follow it blindly and with a decent skill in reading and typing, tadda, there's a Plasma applet. Wanna a package in the build service? You can use another one as a template, find a tutorial on the wiki or just google for it, and if you'll be just a little lucky, a <a href="http://blogs.kde.org/node/4177">tool</a> can even do the work for you.</p>
<p>
For getting a good comparison of what can difficult actually mean, let me show you something I consider to be pretty hard to learn. To have a better contrast, let's go in some completely different area that has absolutely nothing to do with computers. So if you think something is difficult, instead of doing this whatever something, try doing for example the Salchow jump. And since I expect many people here have no idea what that is, it looks like this, performed by yours truly:</p>

<a href="http://www.youtube.com/watch?v=bcIYs4cHiL0"><img src="https://blogs.kde.org/files/images/salchow_0.jpg"></a>

<p>
That's roughly it. I assume it looks quite unimpressive to anyone who's never tried it or anything close (and, possibly, in this specific case it probably looks quite unimpressive even to whose who have). Yet this thing was bloody hard to learn for me. I probably learnt coding with Qt quite decently with much less effort (although, that's one of Qt's selling points, isn't it). Writing .spec files and creating packages? Nah, eeeasy. Even getting into Xlib programming was probably less effort, and I read a good part of the Xlib manual as a part of that. I admit getting into compositing effects and adding them to KWin might have been harder than the Salchow though :). Still, for somebody whose reaction to the idea of writting <a href="http://kde-apps.org/content/show.php/Kor+Testudo+Shell?content=126302">an alternative KDE workspace shell</a> was 'how hard can that be?', the Salchow proved to be an unexpectedly difficult matter.</p>
<p>
I was first shown and explained the Salchow about 9 months ago. I think I needed about 2 or 3 months just to perform it in the most lame way that'd technically qualify, about as much as <i>x = 1</i> qualifies for a math equation. The video is from April, i.e. more than 3 months on top. Today I can perform it somewhat higher and at slightly faster speed, but it still hardly qualifies for anything better than 'decent'. And while I hope it'll one day get to something I'd consider good, I'll probably never ever get to those crazy things like multiple rotations or anything even remotely close to what you can see on the TV, no matter how much and how hard I'd try. Do you still think that e.g. creating and maintaining a package is hard, compared to this? And don't even get me started on the next jumps ... the Salchow is actually <i>easy</i>. Try to think of this next time when you'd want to do something but would consider it too difficult (besides, take this from me, trying difficult things is actually much more interesting than the easy ones).</p>

<p>
PS: Come to think of this, I've never thanked Danimo and Scott Wheeler, who happen to be ultimately reponsible for me starting with skating and having a lot of fun, as I'd probably never come across any such idea myself. So, well, thank you. </p>
<!--break-->
