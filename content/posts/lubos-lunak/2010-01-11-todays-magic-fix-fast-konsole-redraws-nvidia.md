---
title:   "Today's magic fix: Fast Konsole redraws with nvidia"
date:    2010-01-11
authors:
  - lubos lunak
slug:    todays-magic-fix-fast-konsole-redraws-nvidia
---
There is something magical about hacking on things without having much clue about them. It almost feels like a treasure hunt, with mysterious traps all along the way and an elusive treasure maybe at the end. Today's treasure is KDE4's <a href="https://bugs.kde.org/show_bug.cgi?id=155174">Konsole (preferably not) being awfully slow with some fonts</a>. Specifically, as irony would dictate, the rule could be almost said that the better suited font for Konsole the slower the text rendering is, whereas if the font breaks the text layout completely or hurts to look at, the speed is just fine.

Profiling showed only that the CPU time was all spent in the X server in the nvidia driver, which is really not that helpful with something that is closed-source. It only meant that Qt was feeding to X something that the driver didn't like much. Eventually, I tracked it down to one XRenderCompositeText32() call in Qt (which, in a retrospect, wasn't really a very obscure hint - if there's something slow with drawing, suspecting XRender is always a worthwhile guess).

That showed where the treasure was, but now there was the part of getting it. Not that easy if the knowledge about font rendering doesn't go much further than knowing it's drawing text. Especially when it turned out that disabling the XRender path in the code resulted in some fonts being shifted one pixel down (and only some of them, so that it wouldn't be too simple).

But anyway, to make the story short, for those who want a share in the treasure, the patch is <a href="http://bugreports.qt.nokia.com/browse/QTBUG-7255">in the Qt bugtracker</a>, waiting for somebody with more knowledge to answer the questions for which I don't know the answers. And <a href="http://download.opensuse.org/repositories/home:/llunak:/branches:/KDE:/Qt/">home:lllunak:branches:KDE:Qt</a> and <a href="http://download.opensuse.org/repositories/home:/llunak:/branches:/KDE:/Qt:/STABLE/">home:llunak:branches:KDE:Qt:STABLE</a> repositories have the 4.6/4.5 Qt packages with the patch added.

<!--break-->
