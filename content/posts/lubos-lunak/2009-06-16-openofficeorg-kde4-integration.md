---
title:   "OpenOffice.Org KDE4 Integration"
date:    2009-06-16
authors:
  - lubos lunak
slug:    openofficeorg-kde4-integration
---
Since Kendy's blog has somehow disappeared from Planet KDE, let me copy&paste one entry (<a href="http://artax.karlin.mff.cuni.cz/~kendy/blog/archives/monthly/2009-06.html#2009-06-15T14_37_23.htm">http://artax.karlin.mff.cuni.cz/~kendy/blog/archives/monthly/2009-06.html#2009-06-15T14_37_23.htm</a>):
<p>
Thanks to the heroic efforts of Éric Bischoff, Bernhard Rosenkränzer, and Roman Shtylman, the OpenOffice.org KDE Integration has been <a href="http://lists.freedesktop.org/archives/ooo-build/2009-June/000036.html">ported to KDE4</a>. It still has some rough edges (currently the detection does not work out of the box—you have to export OOO_FORCE_DESKTOP=kde4 to get it), but it is safe because it co-exists nicely with the existing KDE3 integration; so if you are not satisfied, you are able just swich back to KDE3 (export OOO_FORCE_DESKTOP=kde).
<p>
 For KDE4, Roman is also changing the out-of-process implementation of the KDE file picker to an in-process implementation, so you'll probably notice some performance improvement of the KDE file dialog launch too :-) 
<p>
End of copy&paste. For people who'd be interested in helping with this, I've been told those should join #go-oo on Freenode.
<!--break-->
