---
title:   "Why C sucks. Part 3473."
date:    2006-03-05
authors:
  - lubos lunak
slug:    why-c-sucks-part-3473
---
<a href="http://spooky-possum.org/cgi-bin/pyblosxom.cgi/forever.html">Every now and again you hear someone complaining about C++.</a> You will probably also have heard that C sucks. The two statements are of course linked; the following code is valid C, but will never compile.
<pre>a.c:
#include "a.c"
</pre>
gcc bails out at the 200th recursion with the error message:
<pre>
In file included from a.c:1,
a.c:1:15: error: #include nested too deeply
</pre>
I've omitted the two-hundred line long trace back through the other includes.

<b>Update (apparently needed):</b>
Disclaimer: This blog entry on its own doesn't make any sense. If you feel an urge to take it seriously, you probably need to apply your morning coffee.
<!--break-->
