---
title:   "News from the Wobblyland, part II."
date:    2006-11-21
authors:
  - lubos lunak
slug:    news-wobblyland-part-ii
---
Ok, time for another screenshot. It actually shows most of the recent improvements in <a href="http://websvn.kde.org/branches/work/kwin_composite/">kwin_composite</a>:

[image:2548 size=original]

At the bottom-left there's KWrite with the file dialog open. The dialog is transparent because I have MakeTransparentEffect enabled - it makes moved/resized windows transparent and it also makes dialogs transparent (hey, I need to test the stuff somehow). The KWrite main window is de-saturated and has reduced brightness - that's DialogParentEffect and support for saturation/brightness, patches from Rivo Laks.

Above it is glxgears, the ultimate benchmark ;) , with adjusted transparency. The xterm behind it shows its output, the first three lines are when no compositing is running - this stuff takes quite some resources, just in case somebody believes that calling it "accelerated desktop" makes things faster (hmm, I guess I'll quickly run out of names for this stuff this way).

The performance is not that bad though, as shown by ShowFpsEffect, that small rectangle in the top right corner. The blue bar is frames per second, this GeForce4 laptop has no problem keeping up with the currently hardcoded 50FPS, the green graph running to the right shows time needed to draw recent frames, again the laptop in this picture manages to keep it green with about 7ms per frame, going temporarily to yellow/red/black values only during big changes. This effect is the first one that doesn't just alter drawing attributes but paints directly using OpenGL or XRender depending on the mode (no, the rectangle is actually not really there and no, I still haven't given up on the crazy idea of at least partially supporting XRender too).

And, as it seems I happen to be a lucky exception (even my home GeForce2 doesn't perform that bad), I decided to work on some performance-related parts sooner than I had expected and I've implemented things like partial repaints, support for double-buffered painting with the composite overlay and also SHM mode from Beryl (which in turn has it from Looking Glass - Beryl actually simply calls it non-TFP but KWin already has another non-TFP mode from glcompmgr). As much as I don't get it SHM mode actually slightly outperforms TFP on the average - I guess the texture_from_pixmap extension support in the drivers/X/wherever still needs some work.

Hopefully that improves matters for other people. Oh, that's the one thing that cannot be seen in the picture. Philip Falkner has successfully battled the ATI OpenGL support and KWin is no longer NVidia-only (I have no idea about Intel but it probably should work too, given that it should share some of the problems with ATI like GLX only version 1.2 and not having TFP support in drivers).

Next I'll probably try to improve the window geometry support so that it will be actually possible for all the things to wobble (gee). I'll see, with the openSUSE 10.2 release getting very close. Incidentally, since we've decided there to start Compiz or Kompmgr by default if X is configured to have the necessary support, I finally had a thorough look at Kompmgr while hunting some bugs (yeah, at the time I try to make it permanently obsolete :) ). I'm really glad Thomas Luebking was the hero to take XCompmgr and create Kompmgr - plain C is scary.

<!--break-->
