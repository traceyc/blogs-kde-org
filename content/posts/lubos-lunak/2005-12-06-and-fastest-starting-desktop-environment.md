---
title:   "And the fastest starting desktop environment is ..."
date:    2005-12-06
authors:
  - lubos lunak
slug:    and-fastest-starting-desktop-environment
---
No, not really. But it's quite close (and it actually also depends on how you twist the benchmark ;) ). When my desktop machine could <a href="http://blogs.kde.org/node/1661">start KDE in less than 4 seconds</a> I was curious what would the situation be with this slow laptop, the one which <a href="http://blogs.kde.org/node/1400">started KDE in 5-6 seconds at aKademy</a>. But that was with almost no fonts installed, no XIM, no wallpaper, the simplest splash and other tricks from <a href="http://wiki.kde.org/tiki-index.php?page=Performance%20Tips">the KDE performance tips page</a>. Well, now it can do it in the same time even without that. It leaves GNOME few seconds behind and gets very close to Xfce startup time.

And since it's always more fun with screenshots, here are some pictures from bootchart. Note that since this is a relatively slow machine, the pictures show non-negligible CPU usage even when the desktop is ready and this also results in slightly longer startup times.

Xfce is the latest 4.2.3.2 built from sources, GNOME is the latest 2.12.2 built using GARNOME, KDE is my development build of 3.5.0 with some patches, all using current fontconfig CVS version, all built using the same gcc flags. The config was more or less the defaults for all, with some adjustments (e.g. the same wallpaper, no mixer applet in GNOME since I didn't get as far as kdemultimedia with my KDE development build). All tests are done with warm disk caches, with cold caches the KDE case actually quadruples(!), but at least some of current distributions use various preloading techniques anyway, and this laptop has bloody slow disk (and loading things from the disk could use some improvements - MS Windows's been reordering data on the disk for faster reading during next startup since when, W2k? ).

So, the basic startup, KDE:
<img src="http://ktown.kde.org/~seli/performance/kde-plain.png">
Xfce:
<img src="http://ktown.kde.org/~seli/performance/xfce-plain.png">
GNOME:
<img src="http://ktown.kde.org/~seli/performance/gnome-plain.png">

(Never mind the funny characters there, it just shows that just sticking the "Unicode" stamp on something, Java in this case, doesn't necessarily mean it really works. Also, as already said, there's some overhead of bootchart visible, so measure time until the place where the CPU usage drops from 100%, that's 7 seconds in KDE's case, and then subtract like 15-20%, which will get the KDE time to those 5-6 seconds.)

There's one more picture worth showing:
<img src="http://ktown.kde.org/~seli/performance/kde-very-plain.png">
Since Xfce is far from providing the same like KDE, in this bootchart I tried to strip at least some of KDE functionality that Xfce doesn't have, and suddenly it's even closer to the Xfce bootchart. (Also, if Xfce had any GNOME or KDE application saved and restored in the session, that'd pull in some of the GNOME/KDE stuff anyway, presumably making Xfce startup even longer. I kind of wonder how many apps running would be enough for making this "lightweight" desktop environment as "bloated" as KDE or GNOME. After all desktop alone is not really that useful.) 

Ok, and now something for you: <a href="http://ktown.kde.org/~seli/performance/fontconfig-2.3.92.20051204-1s1.i586.rpm">fontconfig</a>,
<a href="http://ktown.kde.org/~seli/performance/fontconfig-devel-2.3.92.20051204-1s1.i586.rpm">fontconfig-devel</a> and <a href="http://ktown.kde.org/~seli/performance/qt3-3.3.5-22s1.i586.rpm">qt3</a> SUSE 10.0 packages that noticeably reduce the slowdown caused by fonts. The fontconfig packages are current CVS, the Qt3 package is the one shipped with SUSE packages for KDE3.5.0 from ftp.kde.org, with some patches. NOTE: They are unsupported and I mean it. If you get any strange crashes, first downgrade and reproduce the problem again before reporting it.

The relevant KDE patches will eventually end up in KDE SVN, after I find out where to put them - KDE3.5 is frozen and no KDE3.6 is planned, only KDE4. Don't worry, they'll find you sooner or later. And since I've heard GNOME developers have recently started working on optimizing GNOME, the pictures may look differently in a couple of months.
<!--break-->
