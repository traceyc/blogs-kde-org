---
title:   "5 seconds ... ok, 6"
date:    2005-08-31
authors:
  - lubos lunak
slug:    5-seconds-ok-6
---
Yes, I could really make KDE start up in just 5 seconds on a 900MHz laptop ... 6 seconds during the presentation, the laptop apparently decided to spend the extra second somewhere. As it was measured using a wristwatch it doesn't matter much anyway. And you can download the video, see it for yourself and measure it yourself.

And it wasn't even crippled KDE, not really. I turned off pretty much everything I didn't need, but it was still a fully usable KDE. The only things that really were kind of cheating was using just 5 fonts (because fontconfig sucks perfomance-wise), creating empty ~/.XCompose fonts (same comment for XIM) and doing login->logout->login which avoided disk reads because of disk caches (we could so much use kernel support for this *sigh*). For the rest of the tricks, just see "ggl:KDE performance tips" (that's Google's I'm feeling lucky just in case you for some weird reason don't happen to know this is all you need to type in the Alt+F2 dialog).

I had a look at the icon loading yesterday, as that was the slide with the most question marks and there were probably most questions about it. The test was running "konqueror /", no sidebar, and it resulted in 23 icon loads (out of about 50 iconsets). Warm start: 3,5s until Konqy is idle, out of that 0,15s spent loading icons and applying effects. Bothering with 4% CPU time spent there is probably not really worth the effort. Cold start: about 11s and 1,8s or 0,8s spent loading icons (no idea why the results were almost always one of these two values ... let's use the 0,8s one). That's still only slightly more than 7%. And interestingly enough when I did "find . | xargs cat" in the icons directory before a cold start the time spent in icon loading was 0,4s. So maybe paging in the code or something like that skews the numbers for cold start. Makes me wonder again about it actually being worth bothering.

Makes me also wonder about the gtk icon cache, storing the directory/files structure of the icon images in the file, and actually also storing the images themselves there. Given that gtk somehow <a href="http://mail.gnome.org/archives/gtk-devel-list/2004-April/msg00065.html">manages</a> to waste 0,5M memory just on icon themes seems to be good enough a reason for gtk. The question is whether that would make an improvement for KDE. The cache is about 21M for our crystalsvg/. Loading the file into memory at once means using all that memory, but when paging it in on demand I'd expect there'd be disk seeks anyway. Storing just the directory structure in the file would make it small, but then the icon images would need to be loaded, and that'd mean reading all least some of the directories anyway.

Maybe all this icons stuff just needs a new look from a different perspective.
<!--break-->