---
title:   "Packager-O-Matic"
date:    2010-06-30
authors:
  - lubos lunak
slug:    packager-o-matic
---
<p>
As already <a href="http://blogs.kde.org/node/4177">mentioned</a>, I have this certain <a href="http://kde-apps.org/content/show.php/kde-obs-generator?content=121094">tool</a> in works that can do various magic when it comes to creating packages, especially for people who have no idea how to do them themselves. And since</p>
<img src="http://community.kde.org/images.community/2/22/Igta2010.png">
<p>
too, and on Wednesday I have scheduled a slot for demoing the tool and helping people who'd like to create packages of their software, I've worked on implementing and improving various features that make it more interesting:</p>
<ul>
<li>Besides the obvious CMake support, there is now also support for autotools-based software. Autotools stuff is harder to process then CMake (funny, I remember not being very impressed by the move to CMake, but going back now made me wince here and there), but it's already pretty usable.</li>
<li>Which means that even though the tools is called kde-obs-generator it can handle non-KDE stuff too. For example I have GEdit, Gnumeric or GOffice (why aim low, right?) in my testing project, and while none of that finish the build completely yet, it just needs handling more of the autofoo trickery. I guess the tool is about to need a better name.</li>
<li>There's QMake support too, kind of. However, if you use QMake, you probably want to switch to CMake anyway.</li>
<li>The recommended way of use is setting the build service project in a way that presents a more unified build environment, with macros and package name substitutions automatically taking care of distribution differences. This results in the .spec files being quite clean and easy to read. But there is also a mode that doesn't require this setup and builds directly in normal openSUSE, Mandriva and Fedora repositories (and Ubuntu, but that's already different enough from the rest). It means of course that the .spec may have a bunch of %ifdef's and manually written macros at the top, but it works.</li>
<li>A consequence of this is that it is also usable for creating normal openSUSE packages. If the project build only for openSUSE, the resulting .spec is more or less a normal openSUSE .spec file. I've already watched few times Pavol and Michal explaining packaging to people who would like to learn it, and I couldn't help wondering how scary it has to be for some of them, getting a full .spec file explained line by line on how they need to write it manually. Well, for the next time the introduction part can be greatly reduced to just "run this tool, if it works and you're happy with the result, that's all then".</li>
<li>There is also the bonus even for somebody who knows how to create packages. It's still nice to have the .spec generated automatically, including all the build requirements (if they are already in the database), the file list and so on. Everything I have posted at kde-apps.org for the last several months has up-to-date binary packages too, and it's almost boring to create them.</li>
</ul>
<p>
So, whoever would be interested, just talk to me at Akademy or come to the Wednesday session and I'll try to help you getting your stuff built and answer any questions (except for the two questions about blue hair - really, people, I've heard both of them already enough times, can't you get at least a bit innovative :) ?).</p>
<!--break-->
