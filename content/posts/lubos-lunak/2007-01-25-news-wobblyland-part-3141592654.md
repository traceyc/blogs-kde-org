---
title:   "News from the Wobblyland, part 3.141592654"
date:    2007-01-25
authors:
  - lubos lunak
slug:    news-wobblyland-part-3141592654
---
I finally find out <a href="http://recordmydesktop.sourceforge.net/">recordMyDesktop</a> and played a bit with it. Animations are a bit boring as PNG images after all. Too bad the recording seems to take up quite some resources and the videos look a bit jerky because of that (GeForce2 is not <i>that</i> slow :) ). I had to intentionally make some of the things slower and take longer and it still looks worse than in reality. Oh well. Ok, some new things in <a href="http://websvn.kde.org/branches/work/kwin_composite/">kwin_composite</a> from Rivo Laks, Philip Falkner and yours truly:

- <a href="http://websvn.kde.org/*checkout*/branches/work/kwin_composite/COMPOSITE_HOWTO">COMPOSITE_HOWTO</a>, the starting point doc for kwin_composite.

- <a href="http://www.youtube.com/watch?v=DxpWmy03TUY">PresentWindowsEffect</a> - a simple demo effect presenting all windows on the screen. Also shows the support for keeping contents of windows that are currently hidden, can be later used e.g. for thumbnails for taskbar entries.

- <a href="http://www.youtube.com/watch?v=sKXKGc-bf0g">ZoomEffect</a> - shows the screen with double size, in this case. Can be e.g. useful with all those webpages created by "designers" thinking 7pt fonts are a pleasure to read. This video is actually taken in XRender mode, not OpenGL (because I wanted to show it and because, er, it's currently broken with OpenGL, reason yet unknown). The screen follows the mouse cursor in order to make the mouse to work - X currently has no support for input transformations. The black color in the FPS graph after turning the transparency on shows that XRender performance is not exactly stunning (or is, depending on the interpretation).

- <a href="http://www.youtube.com/watch?v=yDjbxa9IMYs">ScaleInEffect, FadeInEffect and FadeOutEffect</a> - the fade out effect shows support for keeping contents of windows that have been already closed. The other two are there just for the fun of it.

That's enough for now, I'm getting really sleepy ;). And it really looks better for real. No XTerm's with debug output, smoother because no screen recording slows it down, the FPS graph is not so red all the time, oh and the (completely non-existent, of course) crashes accompanying the work in progress also feel more real.

Rumour has it that KWin was so happy because of these changes that it celebrated a bit too much and <a href="http://www.youtube.com/watch?v=UP7BvZWyKQ8">got seriously drunk</a> (investigations are already underway and the video taken shows a suspect, currently only known as the 'International Dragon of Mystery').
<!--break-->
