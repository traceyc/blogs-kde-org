---
title:   "Why C++ sucks, part 5632"
date:    2006-08-30
authors:
  - lubos lunak
slug:    why-c-sucks-part-5632
---
One of bugzilla features is displaying various headlines, probably in order to cheer up the poor bugreporter or bughunter. KDE bugzilla actually doesn't seem to have this enabled, but I liked this in the old SUSE bugzilla, it had a nice collection of funny quips for the headlines. Those are gone now though after the switch to Novell bugzilla, which seems to prefer various encouraging quotes from famous people (I can't help it but sometimes they kinda remind me of the old communist times with their slogans). However, looking at the complete list I can see that some funny comments are finding their way even here, so all is not lost :).

Anyway. Among the various lines I found some that show that even wise and well-known people have their limits. Consider, for example, this one:

<i>If you think C++ is not overly complicated, just what is a protected abstract virtual base pure virtual private destructor, and when was the last time you needed one? -- Tom Cargil, C++ Journal.</i>

The name rings a bell, I think I read a C++ book by this person once (I don't remember if it was a good or bad one though, I've read both kinds). But if he doesn't known, well, good luck I'm here to the rescue. The answers are "some nonsense just made up" (because, if nothing else, private destructor effectively disables inheriting, so it can't be a base class) and therefore the second answer is "never", for everybody. I admit though that the question looked really tricky. Let's try if I can make up something like that as well. How about this one?

<i>If you think plain C is not overly complicated, just what is a static const non-volatile pointer to a register non-const volatile integer, and when was the last time you needed one?</i>

(Just in case you wonder, the answers happen to be remarkably similar. Bonus points for people who can explain why a correct answer may be "&" or "" and "all the time" if it's changed to "register const non-volatile pointer to a static const non-volatile X" depending on the type of X. Extra bonus points if I didn't get it right and you can point it out ;) )

And there's one more interesting quote that shows that a person often doesn't see all the posibilities of their invention:

<i>I invented the term Object-Oriented, and I can tell you I did not have C++ in mind. -- Alan Kay</i>

I don't see why one should feel so depressed about not seeing all the possible uses. It happens to everybody, all the time. I wonder if something like this would cheer this guy up:

<i>I invented the term Programming, and I can tell you I did not have C in mind. -- Charless Babbage</i>

(Although, I suppose, Alan Kay would probably like it more to find out Charles Babbage had no idea Smalltalk would exist one day. My bad.)

I think I should even try to submit these to the Novell bugzilla, I know I can't compare myself with the others, but perhaps they'll make it through. Everybody needs to cheer up sometimes when digging through bugreports.

Anyway. I have one more that probably says it all:

<i>Bugrit. -- Foul Ole Ron</i>

<!--break-->
