---
title:   "Working on performance is so easy"
date:    2006-01-02
authors:
  - lubos lunak
slug:    working-performance-so-easy
---
Or at least many people apparently think so. One just has to love all these people believing that KDE could definitely perform at least as good as Windows 98 (but preferably better of course) if we developers weren't just so damn lazy and finally fixed it during one of our coffee breaks. Coffee (or tea in my case) in one hand, magically snapping fingers on the other hand, probably.

Anyway, to add another episode to the fontconfig adventure, if you've tried the experimental Qt and fontconfig packages improving performance, you've probably noticed crashes in some cases (usually rare, like reading Chinese spam). So, after spending a good portion of this afternoon snapping my fingers while debugging fontconfig (I wouldn't believe just finding a right font could be so complicated), I have new fontconfig packages: <a href="http://ktown.kde.org/~seli/performance/fontconfig-2.3.93.20060102-1s.i586.rpm">fontconfig</a> and <a href="http://ktown.kde.org/~seli/performance/fontconfig-devel-2.3.93.20060102-1s.i586.rpm">fontconfig-devel</a>, fontconfig patch <a href="http://ktown.kde.org/~seli/performance/fccfg.c.patch">here</a>. Qt remains the same, no fixes needed there. Just in case there's still some brave soul left, feel free to try them (don't forget to run SuSEconfig). Again note that the usual unstable-not-supported blah blah applies here, I even haven't got a reply to my patch yet, but I'm quite confident it's ok. Well, or you've been warned at least ... again. I actually use the packages myself, but you know, one rarely runs into their own bugs.
<!--break-->
