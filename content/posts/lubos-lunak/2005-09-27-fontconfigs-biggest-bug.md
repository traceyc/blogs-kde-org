---
title:   "Fontconfig's biggest bug"
date:    2005-09-27
authors:
  - lubos lunak
slug:    fontconfigs-biggest-bug
---
<b>Update:</b> The fontconfig packages linked from this post seem to cause <a href="http://bugs.kde.org/show_bug.cgi?id=116176">KDE bug #116176</a> with KDE3.5 packages. Well, I warned you.

Biggest at least in terms of slowness and memory usage. Yes, I'm talking about fontconfig checking all fonts and building an in-memory representation at application startup, as already mentioned in the <a href="http://wiki.kde.org/tiki-index.php?page=Performance%20Tips">KDE performance tips page</a>. This fontconfig problem has had a noticeable impact on memory usage and startup time of KDE applications. But that has come to an end now it seems.

Thanks to Patrick Lam the latest development version of fontconfig now uses mmap-ed caches, in a similar way like KDE has been using ksycoca since ages. I tested it yesterday, and the bad news is, to my big disappointment it performed really poorly, even worse than the last stable version. But worry not, the good news is that it was just a minor problem and I have already a <a href="http://ktown.kde.org/~seli/download/fccache.c.patch">patch</a> that fixes the issue and I'll send it to the fontconfig developers. In other words it now seems to work great.

For the brave ones I've made SUSE packages that you can try (<a href="http://ktown.kde.org/~seli/download/fontconfig-2.3.90-4s.i586.rpm">fontconfig</a> and <a href="http://ktown.kde.org/~seli/download/fontconfig-devel-2.3.90-4s.i586.rpm">fontconfig-devel</a>). Remember to run <b>'fc-cache -f -v'</b> after installing in order to create the new cache files. Of course, unsupported, use at your own risk, blah blah blah. And if you find a problem, you know who to tell (yes, right, the fontconfig developers).

Update: I've just found another bug. Don't run fc-cache while you have any application running. It seems fontconfig rewrites the old file with new contents instead of unlinking the old file first and writing a new file. That results in fontconfig crashes in the already running applications.
Update #2: That bug has been already fixed and packages have been updated.
<!--break-->
