---
title:   "WMIface 2.0 - CLI scripting of any (decently wm-spec-compliant) window manager"
date:    2009-07-08
authors:
  - lubos lunak
slug:    wmiface-20-cli-scripting-any-decently-wm-spec-compliant-window-manager
---
I noticed yesterday that at the <a href="http://www.kde-apps.org/content/show.php/WMIface?content=40425">kde-apps.org page for WMIface</a>, a tool that allowed scripting the window manager used by KDE3 from command line, a comment appeared asking about a version for KDE4. It felt like a good idea to do something as simple as this in the evening as a relaxation, so, after quite some time playing with C++ templates and so on (since I'm a lazy developer and therefore I went with doing some extra work that would save me from doing work), I have to disappoint you. There is no version specifically for KDE4. In order to reduce the dependencies I made it depend on just QtCore and X11 libs, with those few important classes from kdeui copied into the project. No KDE dependency whatsoever. This provides the CLI tool with startup time decent enough for direct usage, and makes it usable on every system, KDE, GNOME, Xfce, whatever. 

PS: The possibility of the much more powerful <a href="http://blogs.kde.org/node/3336">scripting in KWin itself</a> is still there for anybody interested enough to do it.
PPS: Anybody who ignores the kdeui classes for window management and uses this <i>user</i> scripting tool from an application will be tarred, feathered and publicly laughed at. You've been warned.
PPPS: Every decent system, that is - anything that has QtCore and a reasonably wm-spec-compatible window manager. 

<!--break-->
