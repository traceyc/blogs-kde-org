---
title: New blogs.kde.org
authors:
  - carlschwan
date: 2024-03-03
---

I'm happy to announce the launch of our revamped Blogs.KDE.org website, now
powered by Hugo instead of Drupal 7! 

This switch was motivated by the fact that Drupal 7 is now reaching end of life
and since Blogs.kde.org is the home of many individual blog for KDE hackers so
it was important to keep it up to date and secure.

Upgrading to Drupal 9 was ruled out because Drupal 8 significantly changed and
the migration would have been very complex. By using Hugo, we also leverage the
existing infrastructure which we use for most of other websites. The migration
was fortunately simple and has been fully automated thanks to a small PHP
script reading the database entries.

The repository for this new website is hosted on [Invent](https://invent.kde.org/websites/blogs-kde-org)
and people who posted on blogs.kde.org in the last three years should have
write access. If you don't have access and want to publish a blog post, please
create a sysadmin request to get write access or just create a merge request
and someone from the web team will be able to merge it.

With the new system, creating a blog entry, is as simple as writing a markdown
file in `content/posts/<username>/<year>-<month>-<day>-<identitfier>/index.md`
with some small extra metadata. For example:

```md
---
# content/posts/carlschwan/2024-04-02-example-blog/index.md
title: My example blog post
authors:
  - carlschwan
date: 2024-04-02
---

Here goes the content
```

You can easily preview the content locally by running the following commands:

```sh
wget https://github.com/gohugoio/hugo/releases/download/v0.123.7/hugo_extended_0.123.7_Linux-64bit.tar.gz
mkdir hugo
tar xvzf hugo_extended_0.123.7_Linux-64bit.tar.gz -C hugo
./hugo/hugo serve --buildFuture --buildDrafts
```

And open `http://localhost:1313` in your browser.

Happy blogging!