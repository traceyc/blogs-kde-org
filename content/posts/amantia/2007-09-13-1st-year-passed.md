---
title:   "1st year passed by"
date:    2007-09-13
authors:
  - amantia
slug:    1st-year-passed
---
Even if <a href="http://blogs.kde.org/node/2948">Coolo says</a> they have the cutest baby on earth, I have to disagree and post a proof of it. :) Our little daughter is with us since a year, we had her <a href="http://andras.kdewebdev.org/Lilla_1_eves/">birthday</a> yesterday.
<img src="http://andras.kdewebdev.org/Lilla_1_eves/fullsize/100_6969.jpg" />
<!--break-->
<img src="http://andras.kdewebdev.org/Lilla_1_eves/fullsize/100_6994.jpg" />

Meantime I also uploaded some photos about my <a href="http://andras.kdewebdev.org/Alpok2007/">vacation in the Alps</a>, as well as from a trip I did near Glasgow, before aKademy 2007: <a href="http://andras.kdewebdev.org/TheCobbler/">The Cobbler/Ben Arthur peak</a>.