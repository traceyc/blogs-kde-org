---
title:   "Call for developers: Quanta Plus and KDEWebDev"
date:    2008-05-02
authors:
  - amantia
slug:    call-developers-quanta-plus-and-kdewebdev
---
 Time is passing by. Sometimes I'm also amazed that it was more than 5 years ago when I wrote my first KDE application and soon after I joined the Quanta Plus project. And a few months later Quanta Plus become part of the KDE releases, I think with version 3.1.
 Probably many of you know that I worked full time on Quanta in the past years, thanks to Eric Laffoon and many other supporters, who made this possible. But things have changed, and I cannot spend all my time anymore on this beloved project. I don't abandon it, just realized that alone it would take just too much time to get a release for KDE 4.x series out in time. Therefore I call for help, I'd like to ask the community, existing developers or users with some C++ knowledge, developers who would like to find a challenging project in the open source world to come, join us. Help to make Quanta4 a reality and make many users happy throughout the world. You don't have to be afraid of the size of the project, one of the goals of Quanta4 is to have a modular code, build up as KDevPlatform (KDevelop) plugins.

 There are other projects inside the KDEWebDev module that need help, some even maintainers:

- Kommander: just take a look at <a href="http://www.kde-apps.org/index.php?xsortmode=new&page=0&xcontentmode=288">www.kde-apps.org </a> and you will be amazed by the number of Kommander scripts uploaded by the users. Help to have a good Kommander for KDE4 as well! 
The executor is already ported, but we have lots of new ideas waiting to be implemented.

- KFileReplace: useful search and replace tool, unfortunately without a current maintainer. It works, but needs some love.

- KImageMapEditor: don't let web developers without a KDE image map editor!

Of course our priority would be Quanta Plus and Kommander, but if you are interested in either of the above, just contact us on our <a href="https://mail.kde.org/mailman/listinfo/quanta-devel">developer list</a>.
<!--break-->