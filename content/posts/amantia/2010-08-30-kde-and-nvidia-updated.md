---
title:   "KDE and NVidia (updated)"
date:    2010-08-30
authors:
  - amantia
slug:    kde-and-nvidia-updated
---
The above combination was never a painless experience, still at some point in past it seemed to be better to have a NVidia card on Linux then anything else, so I continued to buy them whenever my system was upgraded. Lately although it started to make me rather bad. I have two computers, one that is a 4 core Intel CPU with 8GB of memory, the other is a Core2Duo with 3GB. The latter is a Lenovo laptop. Both have NVidia, nothing high end (Qudaro NVS something and 9300GE, both used with dual monitor setup), but they should be more than enough for desktop usage. Are they?
 Well, something goes wrong there. Is that KDE, is that XOrg, is that the driver? I suspect the latter. From time to time (read: often), I ended up with 100% CPU usage for XOrg. Even though I had 3 cores doing nothing the desktop was unusable. Slow scroll, scroll mouse movements, things typed appearing with a delay, things like that. Like I'd have an XT. I tried several driver version, as I didn't always have this issues, but with newer kernel you cannot go back to (too) old drivers. I googled, and found others having similar experience, with no real solution. A suspicion is font rendering for some (non-aliased) fonts, eg. Monospace. Switching fonts sometimes seemed to make a difference, but in the end, the bug returned. Others said GTK apps under Qt cause the problem, and indeed closing Firefox sometimes helped. But it wasn't a solution. Or there was a suggestion to turn the "UseEvents" option on. This really seemed to help, but broke suspend to disk. :( Turning off the second display and turning on again seemed to help...for a while. Turning off the composite manager did not change the situation.
 Finally I tried the latest driver that appeared not so long ago, 256.44. And although the CPU usage of XOrg is still visible, with pikes going up to 20-40%, I gain back the control over the desktop. Am I happy with it? Well, not....
 As this was only my desktop computer. I quickly updated the driver on the laptop as well, and went on the road. Just to see 100% CPU usage there. :( Did all the tricks again, but nothing helped. Until I had the crazy idea to change my widget theme from the default Oxygen to Plastique. And hurray, the problem went away! It is not perfect, with dual monitor enabled sometimes maximizing a konsole window takes seconds, but still in general the desktop is now usable. And of course this should also make me have more uptime on battery. 
 Do I blame Oxygen? No, not directly. Although might make sense to investigate what causes there the NVidia driver going crazy and report to NVidia.

So in case you have similar problems, try to switch to 256.44 and if it doesn't help chose a different widget style.

Now, don't say me to use nouveau or nv. Nouveau gave me graphic artifacts and it (or KDE?) didn't remember the dual card setup. Nv failed the suspend to disk test with my machine and doesn't provide 3D acceleration needed eg. for Google Earth.

UPDATE: I upgraded my laptop to 4.5.1 (from openSUSE packages).Well, this broke composition completely, I got only black windows. I saw a new driver is available (256.53), let's try it. So far, so good, even with Oxygen. Let's see on the long run how it behaves, I didn't test it in deep.
