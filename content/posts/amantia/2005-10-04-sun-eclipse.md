---
title:   "Sun eclipse"
date:    2005-10-04
authors:
  - amantia
slug:    sun-eclipse
---
The sun eclipse was also visible from Romania, altough it was not that spectacular. In 1999 was a full eclipse that I was supposed to see from the mountains, but minutes before the eclipse started, the sky was filled by clouds and some minutes after it ended, it cleared up. Go figure. ;-) Now it was cloudy, but cleared up before the eclipse started. I made some digital and non-digital photos. They are not so spectacular as the sone shined quite bright (you couldn't notice the eclipse from the amount of light if you did not know about), and I don't want to become blind, so I used a filtering material to make the photos. This way only the sun and the moon is visible. A picture can be seen <a href="http://www.kdewebdev.org/~amantia/eclipse.jpg">here</a>.
