---
title:   "The plus one post"
date:    2009-08-25
authors:
  - amantia
slug:    plus-one-post
---
Recently, let's say in the past year, I saw a growing number of email messages on the KDE lists with the following content only:

+1

This is getting annoying for me. I get a nice notification from KMail about the messages arrived, and if I saw that it came to some folder that I'm interested in, I look at the message. And many times it contains nothing, but a +1. I can understand the urge to express support for an idea, a person, but please, think about it twice before doing so. These days it is not about abusing my / the worlds bandwidth, as we use more to share "stuff", it is about abusing the time of the readers.

Thanks for reading.

PS: Although I never lived in the US and missed the early times of internet chats, I still remember the <a href="http://catb.org/~esr/jargon/html/A/AOL-.html">AOL!</a> jargon.