---
title:   "ASUS merchandise ;-)"
date:    2006-04-05
authors:
  - amantia
slug:    asus-merchandise
---
Some days ago I bought an ASUS jeans. We've been searching for a jean that is good for my size (not that easy), and found this one with reduced price (~$10 instead of $22). When I saw the name, I thought it will be fun to buy it. :-) 
See the pictures:
<img src="http://kdewebdev.org/~amantia/images/asusjeans.JPG">
<img src="http://kdewebdev.org/~amantia/images/asus.JPG">
<!--break-->