---
title:   "Using NX"
date:    2006-02-07
authors:
  - amantia
slug:    using-nx
---
Today I took some time to configure an nx server on a system for which I am the administrator. It is a server in a library behind a firewall server. 
Actually I have to access both a linux server behind that firewall and a windows server as the application they are using is windows based (and even if it runs just fine through wine, it is not enough in every case). So first I had to solve the direct access from myself to the linux server. With some iptables rules I did it (first time in my life!). It is not perfect as I am using a dial-up connection, so my IP address is not fixed and I had to enable ssh access from a wider range, but it works. ssh to the server behind the firewall worked on a non-standard port. Time to try nx. I had some problems running nxclient. For me, on SuSE 10, it showed an empty xmessage window with an OK button. :-0 I checked, I have everything installed, still nothing happens. As a last resort I downloaded the client RPM package from NoMachine. And the wonder happened: it works. Now I could enable the encryption for all traffic, meaning that theoretically I must be able to access the nxserver with only the custom ssh port open in the firewall. And for my surprise, it worked: I could log in to the KDE desktop from the library. The speed is quite amazing, I would say. I may tweak this even more for the future, but even now with a 800x600 screen it is fast enough.
I run krdc to connect to the Windows server: works and fast again. So basicly I have 
now my KDE desktop, inside that another KDE desktop and inside that the Windows desktop. Amazing, I would say. The latency is a little bit high, but this is because of my modem connection (it is a 3G wireless modem, ~128kbps upload, ~300kbps download).

Good job NoMachine!

See this <a href="http://kdewebdev.org/~amantia/nx-session.png">screenshot</a>.