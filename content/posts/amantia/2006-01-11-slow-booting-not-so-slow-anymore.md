---
title:   "Slow booting? Not so slow anymore..."
date:    2006-01-11
authors:
  - amantia
slug:    slow-booting-not-so-slow-anymore
---
I did some performace tuning today on my laptop, so i can make Kubuntu start up faster. It was mostly about disabling startup services I don't need and optimizing KDE startup performace according to the performace tips from wiki.kde.org. I did not tried to use the patched fontconfig or do something like that. I just have the standard kubuntu debs installed. The system was prelinked, but it was before I even started to optimize it, so it is not really relevant.<br>
I will not go into the details, but here are some figures. The laptop is a PIII-500Mhz (actually this is written on the box, but every application, including the POST screen says it's a 550Mhz one), 192MB SDRAM 100Mhz (261MB/s), 6GB disk, Trident video adapter, 15" monitor, working in 16bit colors. <b>hdparm -t /dev/hda</b> shows 13.22 MB/s. Not too much...
<ul>
<li>Before optimizing: around 3 minutes (2:58 or something like that, I did not write down)</li>
<li>After optimizing: 2:01, from which 1:21 until KDM was usable. The rest is me typing the password until KDE become functional (kicker and its applets appeared, K Menu was usable).
</ul>

As you can see I meassured together the startup of the Linux system and the login inside KDE, as after all I'm interested about the time I have to wait until I can use the desktop.<br>
The speedup is impressive, I would say: ~33%. I don't know if the 40s KDE login can be reduced further or not, but it might be that I did not applied everything.<br>
For what it matters I meassured the startup of my desktop system, which is way faster: AMD64 3200+ (slightly overcloked to around 3500+), 1GB dual-channel DDR 400Mhz (1957MB/s), 40GB disk (39.23MB/s), Nvidia Geforce FX5500 and X working in 24bits. Here I meassured a Windows XP and a SuSE 10.0 startup with SuSE's KDM, but my self-compiled KDE with debug info enabled:

<ul>
<li>Windows XP: 39s (I had to type the password here as well)</li>
<li>SuSE: 47s until KDM, 1:31 in total.</li>
</ul>

The surprise for me is that from KDM to my KDE session it took 44 seconds. What I can think of is that
<ul>
<li>I have a non-optimized debug build: bigger files, slower reading from disk, non-optimized code</li>
<li>I have more fonts</li>
<li>The session restoration is activated and it might have started to do the restoration before kicker and the applets were loaded.</li>
<li>I did not do anything described in the performace tips ;-)</li>
</ul>

From this the 47s is hard to reduce as I already have disabled all the startup services I don't need. Still SuSE does some things I cannot disable without rebuilding a kernel, like detecting RAID arrays.<br>
<b>UPDATE:</b> I tried with another user, where the standard SuSE KDE 3.5 is started and there is no session restoration: KDM appeared in 50s (I think the 3s difference was due to a CD spin-up), total time was 1:10s. So logging in took 20s, which is less than half compared to what I got first time...<br>
Conclusion? My conclusion is that if you are bothered with long startup times, there IS room to improve the situation. I will try at my father's PC as well. Your conclusion can be something else... <br>
<b>Links</b>:
<ul>
  <li><a href="http://wiki.kde.org/tiki-index.php?page=Performance%20Tips">KDE performace tips</a></li>
  <li><a href="http://www-128.ibm.com/developerworks/linux/library/l-boot.html?ca=dgr-lnxw16BootFaster#listing1">IBM article about boot process</a></li>
  <li><a href="http://ubuntuforums.org/archive/index.php/t-89491.html">Article about faster boot for Ubuntu</a></li>
  <li><a href="http://doc.gwos.org/index.php/Speed_up_boot">Another article about Ubuntu</a></li>
  
</ul>

