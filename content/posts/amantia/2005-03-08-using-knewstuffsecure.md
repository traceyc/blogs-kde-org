---
title:   "Using KNewStuffSecure"
date:    2005-03-08
authors:
  - amantia
slug:    using-knewstuffsecure
---
As a follow-up to my <a href="http://blogs.kde.org/node/view/908">last blog</a>, here comes some detail about how to use KNewStuffSecure in your application. Shortly again, the idea behind it is to provide a way to upload and download digitaly signed resources, thus making it possible for the user to check the real
source as well as the integrity of the downloaded resources on one side, and
on the other side it allows automatization for processing the uploaded resources
on the server side.

Using KNewStuff itself is quite simple, and I tried to make KNewStuffSecure not
more complicated. The only extra requirement is to have gpg installed in your
path, and for uploading of course it is preferred if you already have a GPG key
that you can use for resource signing.

The first thing you must to is to subclass the KNewStuffSecure class and implement the installResource method, which is a pure virtual method in KNewStuffSecure. Here is an example of what you should put in a header file:
<code>
#include <knewstuff/knewstuffsecure.h>
class MyNewStuff: public KNewStuffSecure
{
  Q_OBJECT
  
public:
  MyNewStuff(const QString &type,  QWidget *parentWidget=0)
  :KNewStuffSecure(type, parentWidget){};
  ~MyNewStuff() {};
  
private:
  virtual void installResource();
};
</code>

In order to know what you should do in installResource() it is important to understand the structure of the resource you get via KNewStuffSecure. The resource is a gzipped tarball, let's call it resource.tar.gz. The resource.tar.gz contains three files:
data.tar.gz : another tarball containing the actual data to be installed. The   
              name is not fixed, it can be anything like cards.tgz,
              greetings.tar.gz or whatever.
signature : holds the signature for data.tar.gz
md5sum: holds the MD5 sum for the data.tar.gz
              
In the implementation file you have to process the data.tar.gz only, the rest is
handled by KNewStuffSecure. If installing the resource means that you put the 
downloaded file(s) from data.tar.gz into a directory, the implementation of
MyNewStuff looks like:
<code>
void MyNewStuff::installResource()
{
  bool ok = true;
  KTar tar(m_tarName, "application/x-gzip" );
  if (tar.open(IO_ReadOnly))
  {
    const KArchiveDirectory *directory = tar.directory();
    QString resDir =KGlobal::dirs()->saveLocation("data" ) + "appname/stuff/";
    directory->copyTo(resDir, true);
    tar.close();
  } else
  ok = false;
  
  if (!ok)
    KMessageBox::error(parentWidget(), i18n("There was an error with the downloaded resource tarball file. Possible causes are damaged archive or invalid directory structure in the archive." ), i18n("Resource Installation Error" ));
}
</code>

As you can see the name of the resource tarball you have to install is in
"m_tarName". The above code installs the files from m_tarName to 
$KDEHOME/share/appname/stuff. Of course, you must provice the real appname there.

You are free to do other installation methods, depending on your needs. In some
cases it may be just enough to copy the resource tarball somewhere. This part
of the code depends completely on the type of the resource and your application.

Now how to initiate a download? You have to do three things:
- create a MyNewStuff object
- connect the signal installFinished() to a slot to do things what you want after the install is done
- call downloadResource() for the MyNewStuff object.

Example:
<code>
void MyApp::slotDownloadResource()
{
  if (!m_newStuff)
  {
    m_newStuff = new MyNewStuff("appname/resourcetype", this);
    connect(m_newStuff, SIGNAL(installFinished()), this, SLOT(slotResourceInstalled()));
  }
  m_newStuff->downloadResource();
}
</code>
Just a note: "appname/resourcetype" is in free form, it identifies the type of the resource. See the standard KNewStuff documentation for details.

That's all for downloading. Uploading is simple as well. You just have to create
the data.tar.gz (which is specific for your application) and call uploadResource(fileName), where fileName points to the created data tarball. 
Example:
<code>
void MyApp::slotUploadResource()
{
  QString fileName = createUploadResource();
  if (!m_newStuff)
    m_newStuff = new MyNewStuff("application/resourcetype", this);
    m_newStuff->uploadResource(fileName);
  }
}
</code>
Here createUploadResource() creates the data tarball and returns the name with path to the created tarball.

That's all. I won't cover here the details about KNewStuff itself or what you should and can do on server side, for that read the KNewStuff API documentation 
and see 