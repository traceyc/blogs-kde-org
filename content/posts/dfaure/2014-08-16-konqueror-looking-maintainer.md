---
title:   "Konqueror is looking for a maintainer"
date:    2014-08-16
authors:
  - dfaure
slug:    konqueror-looking-maintainer
---
For quite some time now (OK, many years...) I haven't had time for Konqueror.
KDE Frameworks 5 and Qt 5 have kept me quite busy.

It's time to face the facts: Konqueror needs a new maintainer.

This is a good time to publish the thoughts we had many years ago about a possible new GUI for Konqueror.
<!--break-->
Kévin Ottens, Nuno Pinheiro and myself had a meeting (at some Akademy, many years ago) where we thought about how Konqueror's GUI could be improved to be cleaner and more intuitive, while keeping the idea of Konqueror being the universal navigator, i.e. the swiss-army knife that includes file management, web browsing, document viewing, and more. This is what makes it different from rekonq and dolphin, for instance.

So, if you're interested in Konqueror and you want to keep it alive, please consider taking over its development; whether the ideas below sound good to you or not. This is no obligation, just a brain-storming that could prove useful for a future redesign of Konqueror's UI.

The first task is much simpler anyway: it needs to be ported to KF 5 and Qt 5.

<hr/>

<b>1) Definition of the target user group:</b>
<ul>
<li>Users who prefer to stay within the same application as much as possible
    (which includes both those who don't like major changes, and those
     who prefer a single window over new windows and apps being started
     for every type of document)</li>
<li>All web users (from one webpage to many at the same time).</li>
<li>Heavy content consumers (local and remote)</li>
</ul>

<b>2) Workflow diagrams<b>

<b>2.1) Navigation to a document in a known location</b>
  Pre-requisite: a "starting point" selector is available
<ul>
<li>Step 1: choosing a starting point</li>
<li>Step 2: navigating to that starting point in the main view</li>
<li>Step 3: when reaching the document, its contents are displayed, still in the main view.</li>
<li>Optional step 4: for edit the document, an easily accessible button is available.</li>
</ul>

<b>2.2) File management during navigation</b>
  During step 2 above, the user might notice files that need renaming, deleting,
  moving, etc. Moving and copying is especially interesting, we made a list of
  the possible ways to do that, and kept:
    Copy/Paste, Splitted views, Dropping on a tab, Dropping on a breadcrumb
  while discarding the sidebar folder tree.

This also led to a new concept in addition, for the following workflow:
<ul>
<li>Step 1: Select files</li>
<li>Step 2: Trigger cut or copy action</li>
<li>Step 3: A "clipboard" window appears on the side of the current window,
     showing an icon for the copied files.</li>
<li>Optional step 4: drag more files to the clipboard, even from other 
locations, which creates more "groups of files" icons in the clipboard.</li>
<li>Step 5: Navigate to the target location</li>
<li>Step 6: Trigger the paste action to paste all the files from the clipboard,
    or drag individual groups of files from the clipboard to the intended 
location.</li>
</ul>

  This has the advantage that no splitting or sidebar is necessary within konqueror.
  The clipboard window (inspired by the concept of a shelf in NeXT) offers a temporary
  area for holding the files while we are moving from the source location to the
  target location within the main view.
  Note that the clipboard would have a visible label "Copy" or "Move", depending
  on the action in step 2 which made it appear. This makes it clear that the files
  dropped in step 4 will also be copied or moved, and saves asking the user again
  at every drop in step 4, as well as asking in the final drop in step 6.

  This concept could even be made KDE-wide; whenever the user uses "copy" or "cut" in
  an application, the clipboard window would appear (at a fixed location, in this idea,
  not attached to the application window) and an icon for the copied or cut data would 
  appear in the clipboard window.
  On the other hand it might be annoying when using a quick Ctrl+C Ctrl+V in a word
  processor, so it should probably not appear automatically in most applications.
  But for files it would be nice if it appeared automatically, because this makes the
  feature very easy to discover, compared to a "Tools / Show Clipboard Window" that
  nobody would use.
  (Another issue is that the workspace-global window might appear on top of the
   window where you wanted to copy or paste... this problem doesn't happen in the
   initial idea of attaching to the konqueror window (a bit like a Mac slide panel
   or whatever it's called). It could appear on whichever side there is room for it,
   or the window could move to make room for it).

  The nice thing is that this is actually very easy to implement, it fits exactly the
  QMimeData/QClipboard concepts. But let's put this aside for now, it's a bit separate
  from the main idea of this document. It was mostly a way to provide a solution for
  the loss of sidebar folder tree, currently used by people who move files to other
  directories. Meanwhile another solution for these users is to split and switch to
  "tree view" mode, which konqueror would offer again (like the file dialog does,
  and unlike dolphin). (However that's a tree of files and dirs, not a tree of dirs).

<b>2.3) Previewing many files from the same directory</b>

  For fast multiple previews, konqueror currently has a very hidden but very nice
  feature: split+link+lock. Since it's impossible to discover, these features would
  be removed from the GUI, and instead a single action would be offered for these
  three steps. A good name still has to be found, something like "Splitted Preview".
  The workflow would be:
<ul>
<li>Step 1: Navigate to directory</li>
<li>Step 2: Trigger "Splitted preview" action.
   The window is split vertically, and the right view shows a single label 
like "click on files to preview them here".</li>
<li>Step 3: Click on a file in the left view
   The file is embedded into the right view.</li>
</ul>
  Repeat step 3 as many times as needed.
  This is a single-click preview, faster than waiting for tooltips or having to
  navigate back after each file being opened.
  
  For images especially we would like to also provide the following workflow:
  <ul>
<li>Step 1: Navigate to directory</li>
<li>Step 2: Click on first image
    It appears embedded into the main view</li>
<li>Step 3: Click on "previous" or "next" buttons which appear on top of the 
image.</li>
</ul>
  [Technically, since gwenview has this feature already, this is just a matter
   of making it available in gwenview part]

  Note that the first workflow is more generic since the user can use it to look
  into text documents, html pages, pdfs, or anything else, not only images.

<b>2.4) Internet search (not only google, but also wikipedia etc)</b>

  Pre-requisite: a "starting point" selector is available
<ul>
<li>Step 1: Choose starting point "web"
     Search form appears in main view</li>
<li>Step 2: Choose type of search (if different from the default one)</li>
<li>Step 3: Type search</li>
<li>Step 4: Resulting webpage appears in main view</li>
</ul>

2.5) Local document search using non-spatial criterias (nepomuk)

  Pre-requisite: a "starting point" selector is available
<ul>
<li>Step 1: Choose starting point "local search"
     Search form appears in main view</li>
<li>Step 2: Fill in search form</li>
<li>Step 3: Results appear in main view</li>
</ul>

<b>2.6) Search using time criteria (history)</b>

  Use case: I want to open again a location that I know I visited recently,
     but I forgot the exact URL for it. E.g. after clicking on a link on irc.

  Pre-requisite: a "starting point" selector is available
<ul>
<li>Step 1: Choose starting point "history".<br/>
     A list of recently visited locations appears in main view,
     sorted by time of last visit (most recent on top)</li>
<li>Step 2: Choose location (direct click, or using as-you-type filtering)</li>
<li>Step 3: Location is loaded in main view</li>
</ul>

<b><u>General idea</u></b>

It appears that all the navigation can be done in the main view, if "starting points"
 are easily made available. The sidebar would therefore be removed. Most of its current
 modules are covered above. The others do not appear to be useful.
We still have to check whether web sidebars still make sense or whether that 
concept died.

The following four starting points would be used for navigation:
<ul>
<li>Places  (basically: directories, local or remote)</li>
<li>Document search   [how to say that in one word?]</li>
<li>Web</li>
<li>History</li>
</ul>

The current "Home" button would be replaced with four buttons, for these four starting points.
When clicking on one of them, the main view would show the corresponding part, so there is
room to make things look nice:

- "Places" would show large icons, for directories (somewhat similar to the 
places model, but in two dimensions and with "groups" separated by a line, like the 
grouped view in dolphin)

- "Document search" would be the opportunity for baloo to provide a 
full form for searching.

- "Web" would show the web-search-lineedit (from the current konqueror), as 
well as the list of bookmarks, with a lineedit filter.

- "History": as described before, list of recent locations ordered by time.

<b><u>Mockup</u></b>

In <a href="http://www.davidfaure.fr/kde/konqueror.ui">konqueror.ui</a>, you will find a konqueror window, shown in "wireframe" style so that nobody comments on colors and pixels, as recommended by Nuno.
Note how easily this can be done: just paste this line into the styleSheet property of the toplevel widget:
  QLabel { border: none }\n * { border: 1px solid black; background: white }
This could probably be refined to remove some double borders, but it works good enough for now.

In this mockup window, you will find multiple tabs, to show the various things
one can do. First: one tab per "starting point": Places, Search, Web, History.
Then one tab per location where the user could end up, after some navigation:
<ul>
<li>a dolphinpart directory view ("dfaure")</li>
<li>a web page ("KDE - Experience Freedom!")</li>
<li>a PDF document ("foo.pdf"), using okularpart</li>
<li>an image ("image.jpg"), using gwenviewpart</li>
</ul>

The interesting part of this mockup is that the menu items in the main 
konqueror menu and the toolbar buttons in the main toolbar always stay the same, whatever the
current part is. All the part-specific actions are shown in a toolbar
inside the part, with credits for the idea to a mockup which used to be
http://oversip.net/public/konqueror-mockup/ but no longer exists.
Where the mockup currently uses an ugly combobox, it would 
obviously be a toolbar button with a dropdown menu, like the right-most button in the 
file dialog.
It's just that this can't be added in Qt Designer with the default set of widgets.

The button saying [View Properties] would only have the tool icon, like the 
equivalent button in the file dialog.
For the other buttons, it is yet to be decided if they should have icon-only 
or icon-and text, which is definitely more discoverable but takes more space.

The available actions for each part have been taken from the current parts mentioned above.
The case of okularpart is still a problem though, we didn't find out how to finish the
grouping of actions so that the toolbar isn't too crowded. However the concept seems to
work OK for the other parts, okularpart is really the worst one in terms of the number
of actions :)

<hr/>

Anyhow - if you want to help port Konqueror to KF5, this is a good opportunity to start getting to know its code.
If you're not sure you're ready for the big step of becoming maintainer, you can at least start with porting it and decide later.
Join kde-frameworks-devel (for KF5 porting questions or patches) and kfm-devel (for general konqueror / dolphin discussions) (but CC me, I stopped monitoring kfm-devel since the only activity was about dolphin...). If this blog leads to new developers, maybe we could create a konqueror-devel mailing-list finally ;)