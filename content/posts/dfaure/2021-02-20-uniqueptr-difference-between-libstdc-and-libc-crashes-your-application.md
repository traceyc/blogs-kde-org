---
title:   "unique_ptr difference between libstdc++ and libc++ crashes your application"
date:    2021-02-20
authors:
  - dfaure
slug:    uniqueptr-difference-between-libstdc-and-libc-crashes-your-application
categories:
 - C++
---
Thanks to the KDE FreeBSD CI, which runs our code on top of libc++, we discovered an interesting difference between libstdc++ and libc++'s implementation of unique_ptr. This is quite unexpected, and the actual result for users is even more unexpected: it can lead to crashes in specific situations. This happens when a widget -- using unique_ptr for its d pointer, as is customary these days -- installs an event filter. That event filter will be triggered during destruction of child widgets (at least for the QEvent::Destroy event, I've also seen it with QEvent::Leave events for instance). And, depending on how the event filter is written, it might use the d pointer of the widget, possibly before checking the event type. That's where it gets interesting: the libc++ implementation of unique_ptr sets it to null *before* calling the destructor (because it's implemented in terms of <tt>reset(nullptr);</tt>. In libstdc++ however, unique_ptr's destructor just calls the destructor, its value remains valid during destruction.

Here's <a href="http://www.davidfaure.fr/kde/unique_ptr_for_d_pointer.cpp">a testcase of the crash</a>, extracted from the KIO code for which you can see the fix <a href="https://invent.kde.org/frameworks/kio/-/commit/a8a2c08014484145a4bd2a541a1cbeb8be856bf1">here</a>. It works with gcc/libstdc++, and crashes with clang/libc++, because MyWindow::eventFilter() is using a d pointer that is null already.
In this particular case the fix is easy, testing the event type before anything else. That's good practice anyway, for performance reasons. But there are <a href="https://invent.kde.org/frameworks/kio/commit/3134b410b98e40b34d3b80d123ee1304f">other cases</a>, where the situation was a bit different and the solution was to remove the event filter upon destruction.

I like unique_ptr, but beware of the interaction with event filters...