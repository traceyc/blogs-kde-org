---
title:   "2018 Toulouse PIM sprint report"
date:    2018-07-24
authors:
  - dfaure
slug:    2018-toulouse-pim-sprint-report
categories:
  - PIM
---
Like every year, the PIM developers met in Toulouse for a bit of bugfixing ;)

Except for <a href="https://www.dvratil.cz/2018/04/my-kde-pim-update/">Dan Vratil</a>, we forgot to blog about it, here's a belated blog post with what we did there.

<b>Privacy goal</b>
<a href="https://phabricator.kde.org/T742">Memory hole support</a>
The big issue here is that we need to update the headers outside the mimetreeparser, like subject or from/to etc. That's why mimetreeaparser should get an interface, so plugins can change headers. Another header that would benefit of that interface is a DKIM signature checks on the mail, that would update the DKIM status based on actual data.

<b>KMail</b>
<ul><li>Improved security when encountering encrypted emails with external references.
  <a href="https://phabricator.kde.org/D12391">D12391</a>, <a href="https://phabricator.kde.org/D12393">D12393</a>, <a href="https://phabricator.kde.org/D12394">D12394</a>.</li>
<li>Discussed header validation support (memory hole, DKIM): how to integrate this into mimetreeparser, and how to feed that into the mail renderer. See <a href="https://phabricator.kde.org/T8568">this task</a>.</li>
</ul>

<b>Akonadi Search</b>
Support for encrypted emails

<b>kdepim dependencies</b>
In order to move ksieve down the dependency tree:
   <ul><li> pimcommon - get rid of PimCommon::SieveSyntaxHighlighterUtil (<a href="https://phabricator.kde.org/D12401">here</a> and <a href="https://phabricator.kde.org/D12409">here</a>)</li>
  <li>PimCommon::Util::createImapSettingsInterface  is also in AkonadiMime, so properly moved the function to PimCommonAkonadi at least, as it needs Akonadiserver to run anyways</li>
   <li>for libSieve change dependency kdepim-runtime (imapresource <- libksieve) should make it easier to move libsieve to a lower tier and make imapresource push informations to ksiveui and we can remove akonadi dependency</li>
</ul>

<b>KItinerary</b>
<ul><li>try harder to extract boarding times from pkpass files</li>
<li>live test of the itinerary app passing manual and automatic boarding pass inspection on two airports</li>
<li>acquire more test data from sprint attendees (and as a result of this made EasyJet IATA BCBP data parsing work, and fixed Iberia boarding pass extraction)</li>
<li>handle merging of codeshare flight data</li>
<li>rework data extraction in KMail when multiple data elements are found (this is now all merged together to further improve the completeness of the data)</li>
<li>made boarding passes/tickets for trains and busses accessbile in the app too (this should in theory enable the app to let you travel on SNCF when booked via trainline.eu)</li>
<li>attach the entire reservation data to ical events created from it, so it's fully synced to other devices that way</li>
</ul>

<b> Akonadi</b>
<ul><li>finished and merged Notification Payloads (T639). The hope of Notification Payloads is to give us substantial performance boost and IO reduction by easing on the amount of SQL queries we do, especially during sync, by sending the changed entities as part of change notifications, so that clients don't have to query the entity from Akonadi after they receive the notification</li>
<li>improved Akonadi Console to simplify debugging notifications and monitors</li>
<li>new Logging tab in Akonadi Console with logs from all Akonadi apps</li>
<li>improve Debbugger view</li>
</ul>

<b>Debugging akonadi</b>
Update debugger window to handle more messages without slowing down the machine too much

<b>LDAP completion</b>
Now works again with non-ascii characters in the search string

<b>WebEngine regressions</b>
Investigated Qt 5.11 bug which broke clicking on links in KMail, reported to <a href=" https://bugreports.qt.io/browse/QTBUG-67870">the Qt bugtracker</a>, fixed since.

<b>Qt performance</b>
<a href="https://codereview.qt-project.org/227128">Sped up QTimeZone::isTimeZoneIdAvailable by a factor 43</a>. This is relevant when deserializing many events from akonadi (testcase: opening an invitation email in standalone kmail, loads the full calendar)

<b>IMAP Resource</b>
<ul><li>Fix IMAP resource stuck if connection lost between OK and Login, as well as during Login</li>
<li>Fix possible data loss on connection lost, depending on the current task. This is likely what leads to some emails in a Send folder having no remoteId, making every future sync of that folder slower</li>
</ul>

<b>kdepim addons</b>
Fixed long-standing crash on make install, due to a memory leak when closing a separate viewer window.

<b>KSMTP</b>
<ul><li>Fix sending emails where a line starts with a dot</li>
<li>Remove dependency on KMime</li>
</ul>

<b>Kontact</b>
Prettier icons in the Kontact side pane

<b>Turning PIM libraries into KDE Frameworks (ongoing effort)</b>
<ul><li>Plan for syndication: remove FileRetriever (only used by akregator via Loader::loadFrom), that removes the KIO dependency, after 18.08 freeze ABI and move to KF5</li>
<li>kcontacts kdelibs4support data dependency: move to CLDR for country name mapping, move the address formatting data from kdelibs4support to kcontacts</li>
</ul>

<b>OAuth login</b>
<ul>
<li>moved the XOAUTH2 SASL plugin to LibKGAPI</li>
<li>IMAP: it's possible to use Gmail IMAP without the OAuth authentication again</li>
<li>added support for OAuth authentication to SMTP module</li>
<li>added XOAUTH2 support to KMailTransport, so it's possible to authenticate with Gmail SMTP via XOAUTH2 (which means 2FA and Gmail for Business domains support out of the box)</li>
</ul>

<b>outreach</b>
Accept and handle outstanding review requests from new contributors.

<b>Ruqola (RocketChat client)</b>
<ul>
<li>Fixing AboutPage tab bar</li>
<li>Fixing about page author layouting</li>
<li>Preparing fix for password security if prefilled</li>
<li>Discussion design over support for multiple accounts</li>
</ul>

<b>Android backend for KCalCore</b>
<ul>
<li>assumption: we use the platform calendaring API, rather than Akonadi, and use KCalCore::Calendar as the unifying API for that, backed by AkonadiCalendar and a yet to be created AndroidCalendar</li>
<li>challenge: that API doesn't cover all of ical, but for some sync connectors (namely DavDroid) some extra fields seem available in the ExtraProperties table (confirmed for GEO, X-xxx and attachments)</li>
<li>problem: OpenTasks + DavDroid do not store custom properties or parent <-> child relations, that's even lost on Android-side changes. This means we also can't access all information needed for Zanshin on Android.</li>
<li>what to do about the google calendar? any chance to access additional ical fields there?</li>
</ul>

<b>Syndication</b>
Removed KIO and KCoreAddons dependency to make it ready to be moved to Frameworks

<b>Modern C++ usage</b>
KF5 is still on C++11, due to GCC 4.8 and MSVC 2013
Application code could use C++14 (e.g. generic lambdas), which would require GCC 4.9 and MSVC 2015, which is fine
 
<b>Akregator</b>
<ul>
<li>cleanups on the storage plugin API</li>
<li>drop deprecated unused code</li>
<li>work on a SQL backend (private branch so far, but beginning to work)</li>
</ul>

<b>Community Data Analysis</b>
See <a href="https://ervin.ipsquad.net/2018/04/22/exploring-contributors-centrality-over-time/">Kévin's blog post</a>.

Many thanks to all our sponsors and donators for making such sprints possible.