---
title:   "News from KDE PIM in July/August 2019"
date:    2019-09-11
authors:
  - dfaure
slug:    news-kde-pim-julyaugust-2019
categories:
 - PIM
---
Following Volker's last blog on this topic, here are some highlights of the recent work that has been done around Kontact / PIM during this summer. First of all, stats: there were around 1200 commits in the past two months, leading to the new 19.08 release.

<b>KDE Itinerary </b>

You can have a good overview of Volker's work by following <a href="https://volkerkrause.eu/2019/08/01/kde-itinerary-june-july-2019.html">this blog</a>.

<b>Kontact</b>

It seems our team was mostly focused on cleaning, fixing and introducing little UI features during summer: 

Laurent added folder specific exportation from PIM within its PIM data exporter, contact selection from LDAP servers, refactoring some PIM libraries to new ECM macros and cleaned up deprecated method preparing for a Qt 6 future. 
In addition to that some bug fixes got in, most notably adding reminders to new events in KOrganizer, text-to-speech fixes and general KMail behavior corrections. 

Reminders to new events in Korganizer:
<img src="http://www.davidfaure.fr/kde/reminder1.png"/>
<img src="http://www.davidfaure.fr/kde/reminder2.png"/>

Choosing a contact from LDAP server : <img src="http://www.davidfaure.fr/kde/ldap1.png"/>

All of the libraries have been adapted to compile with Qt 5.13 and soonish we should reach 5.14 compilation state. 

Volker spent time unifying instant message address storage in vCards to KContacts, KContacts soon to be its own framework. The visual style for inline messages in KMail's message viewer was updated too, following the Breeze style as you can see in the picture.

Visuals for inline message boxes in the mail view : <img src="http://www.davidfaure.fr/kde/inline1.png"/>

Sandro Knauß worked on the begining of an implementation for MemoryHole support : the idea is to encrypt the mail header (containing metadata) to enforce even further user privacy. In order to achieve that, they wrote a new interface between MimeTreeParser and MessageViewer so we now can update mail headers for later extraction, stay tune for more deails about that next blog posts. In addition and with the help of Glen Ditchfield they put their effort on making messagelib tests green again!

David Faure ported the all of KDE PIM to D-Bus activation as a way to start applications, which enabled the deprecation of KDBusServiceTrader and maybe in a long term future a refactor to simplify KLauncher.

<b>Akonadi</b>

Dan worked on a better support of PostgreSQL (better handling of table name case sensitivity and the sorting of versioned directories). Akonadi server received some love with a few cleanup and modernization treats, you will have more detail about what Dan cooked in his kitchen in <a href="https://www.dvratil.cz/">his blog</a> so stay tuned!

<b>Infrastructure</b>

Volker and Laurent made sure all KDE PIM modules would appear correctly under the api.kde.org, to make the KDE PIM codebase easier to follow and discover.

<b>Help us make Kontact even better!</b>

All these efforts to provide good free software to the world would not be possible without our committers, if you want to take part on that feel free to contact us #kontact and #akonadi IRC channels on Freenode, see the below section to get started:
<a href="https://community.kde.org/KDE_PIM/Development">https://community.kde.org/KDE_PIM/Development</a>.

And again, if you are attending Akademy, feel free to come see us !

This is a guest post from Franck Arrecot due to technical issues with his blog.
Thanks Franck for writing up the above!