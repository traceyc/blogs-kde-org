---
title:   "Installed Kubuntu on AMD Athlon 64 Dual Core"
date:    2006-05-03
authors:
  - dfaure
slug:    installed-kubuntu-amd-athlon-64-dual-core
---
Since my previous machine heated too much and crashed all the time, my employer (KDAB) got me a new HP Pavillion t3350 machine, with a dual-core Athlon 64 X2 (M) 3800+ 2.0 GHz CPU - very nice :)

The support for the ATI Radeon Xpress 200 chipset (and the SATA harddisk, apparently) is a bit new in Linux - the last version of Mandriva didn't really have a recent-enough kernel for it (although it had some backported patches it seems)... I didn't get it to work. So I decided it was time to try another distro - for the first time in my life, being loyal to my former employer up to now :) - and this is how I installed KUbuntu 6.06 beta2.

The installation went fine and the system booted nicely (for a change Smiling, thanks to the 2.6.15 kernel handling SATA just fine. X didn't start up, probably because the driver for the video card (ATI Radeon X1300 Pro) was set to "ati" in /etc/x11/xorg.conf (but this is a too recent ATI card it seems...). X -configure selected "vga", worked but with a horrible resolution obviously. Manually setting it to "vesa" got me a bit further, but after that I downloaded the binary drivers from ATI. Of course they are for xorg-6.9 and kunbuntu has 7.0 now, so export X_VERSION=x690_64a was necessary, but after that the driver could install itself - and after some setup (/usr/X11R6/lib in /etc/ld.so.conf, apt-get install libstdc++5, moving the ati files around), the driver worked fine. Oh I also had to tweak xorg.conf as indicated on this blog to avoid a crash. Somehow this is all much more fiddling than most people expect, I hope the next version of things will support this rather new hardware better out of the box.

Impressive choice of resolutions now, especially after copying the HorizSync, VertRefresh and DisplaySize values from my former configuration of the Monitor in Mandriva - where I think I was able to select the exact monitor type in a list.
So after all this - and after discovering a bad RAM module in memtest, which explained the occasional lockups - the system finally works!

I'm learning the debian commands for administration (apt-get, dpkg-query etc.) and I keep stumbling on things that are not installed by default (devel libs of course, but also essential things like gcc or openntpd). In both Mandriva and Kubuntu, I'm really missing a "install me all the devel libs for all the libs I have installed" command... Instead I have to keep hitting missing devel libs, and installing them one by one, but of course I keep a list of all the devel libs I needed for kde, for any version of Mandriva or now Kubuntu that I installed. Next time (new machine or reinstall of linux) it's just a matter of urpmi `cat thelist` or apt-get install `cat thelist`...

Well - back to KDE hacking, on that fast machine :)

I just wish the "Install Bitmap [font]" thing in konsole worked, somehow it fails with a strange "Could not install console8x16.pcf.gz into fonts:/Personal". Ah well, I guess I'll need to compile kde3 on that machine, not just kde4 :)

PS: first blog!