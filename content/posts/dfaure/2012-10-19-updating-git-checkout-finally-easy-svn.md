---
title:   "Updating a git checkout - finally as easy as with svn"
date:    2012-10-19
authors:
  - dfaure
slug:    updating-git-checkout-finally-easy-svn
---
git: very powerful, not easy to use for simple tasks.
Especially when coming from svn, where updating a local checkout was simply "svn up", and with git it's "git pull --rebase, and if that fails, stash, pull --rebase, stash pop".

Finally, all this can be automated, so that one can simply keep the good old habits from svn and type "git up"!

Here's the recipe. In your ~/.gitconfig, write
[alias]
up = !gitup

Then download the script http://www.davidfaure.fr/scripts/gitup and put it in your $PATH.
Done!
(Should have done that years ago...)

Before the git purists start screaming, yes, I know, one should not do that (rebasing when pulling) if sharing your local branch with other people. But let's face it, you usually don't do that :-)