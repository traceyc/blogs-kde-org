---
title:   "Unittests, now with automatic upload of results"
date:    2010-11-19
authors:
  - dfaure
slug:    unittests-now-automatic-upload-results
---
As a followup to my previous post: I just added a feature to kdesrc-build which makes it possible to run the tests and upload the result to http://my.cdash.org. Just add the line "run-tests upload" in your module definition for kdelibs, and others if wanted.
<!--break-->
Internally this simply enables the cmake options -DKDE4_BUILD_TESTS=TRUE and -DBUILD_experimental=TRUE, and uses "make Experimental" instead of "make test". However you don't need to know all this anymore, you can just use "run-tests true" to run tests locally or "run-tests upload" to run them and upload the result automatically. This way you can point others to what they broke :-) And bored people at work can monitor the dashboard...

To see the kdelibs dashboard: http://my.cdash.org/index.php?project=kdelibs
 (my build machine is "obelix")

We had a discussion about build machines running unittests, but the setup is tricky (you need a $DISPLAY, and a running kde session or at least kdeinit4 and friends), and the administration of such build machines takes additional efforts, while as developers we need to make sure we have all dependencies, so we might as well upload our tests results directly from the development machines.
(The dashboard naming for manually-pushed results is "Experimental" which is somewhat misleading, it was meant to be "for manual testing before setting up a nightly build").