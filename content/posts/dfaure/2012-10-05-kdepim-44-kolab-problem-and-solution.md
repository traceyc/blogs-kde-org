---
title:   "kdepim-4.4 with kolab (problem and solution)"
date:    2012-10-05
authors:
  - dfaure
slug:    kdepim-44-kolab-problem-and-solution
---
My wife's assistant uses KMail (obviously) and KOrganizer (for a Kolab shared calendar).
To spare her from akonadi trouble, I installed OpenSuse-11.4 for her, which comes with KDEPIM 4.4.

No akonadi needed for kmail in that version, but it existed already, for the address book.
And due to that, a nasty bug happens at almost every start of kontact: the "starting akonadi" progressbar stays on top of kontact forever, even though everything else indicates that akonadi started properly. I have no idea what causes it, it's obviously been fixed since, but since it actually prevents new emails from coming in (!? makes no sense, since emails are unrelated to akonadi in that version), just ignoring the progressbar isn't an option.

The solution I found: using kmail and korganizer separately.
However the Kolab support (for the shared calendars) requires that korganizer finds the kolab folders in the running KMail rather than trying to look them in the (not running) kontact.
To make that possible I had to revive my very old script which allows to select the IMAP backend between kmail and kontact --- and by revive I mean, port it from KDE3 to KDE4.

So, in case it's useful to anyone, here it is: http://www.davidfaure.fr/kde/select_imap_backend.sh
Basically, it moves one line from Kontact.desktop to KMail2.desktop, but it's easier to run a script than to try and remember which line to move in which file.

Of course if you use kmail2 (akonadi-based, kdepim >= 4.6) then you can ignore this entire post. Too late, I know :-)