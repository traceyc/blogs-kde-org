---
title:   "10 times faster deleting a directory (different use case)"
date:    2009-02-18
authors:
  - dfaure
slug:    10-times-faster-deleting-directory-different-use-case
---
Yes, very much like my previous blog post some time ago, but for a slightly different use case. Last time it was about deleting 10000 files viewed in dolphin/konqueror, using "select all", while this time it's about selecting ONE directory and deleting it - the recursive listing involving multiple ioslaves took far too much time. This became the highest-voted bug assigned to me - bug <a href="https://bugs.kde.org/show_bug.cgi?id=174144">174144</a> - so it's fixed now. kio_file implements recursive deletion itself using QDirIterator, and announces this capability (a mechanism that was already in place for kio_trash).
Result: from 20 seconds to 2 seconds for deleting 5000 subdirs.
Well, that's it. Tell me if that was boring, I'll stop blogging :-)

In other news, mysql doesn't lock up on bugs.kde.org anymore since the upgrade to bugzilla-3.2. Finally! Keep voting on the bugs & wishes that matter to you, I hate having to decide myself what is important and what is not.