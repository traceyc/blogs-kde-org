---
title:   "2019 Toulouse PIM sprint report"
date:    2019-04-17
authors:
  - dfaure
slug:    2019-toulouse-pim-sprint-report
categories:
  - PIM
---
Like every year, a number of KDE PIM developers met in Toulouse for a bit of bugfixing.

<b>Discussions and decisions</b>
There were a number of those, the most important ones being about food, of course.
Among the topics of lesser importance: turning some PIM libraries into KF5 frameworks (that's just their way to dump more work on me, clearly... but it also means a lot of cleanup work for Volker, first), outreach to the Plasma Mobile PIM team, how to increase the number of attendees for this kind of sprint, how to make it easier to start contributing to KDE PIM, how to blog more often about progress.

In terms of the actual work done, the list is quite long, here is my view on things.

<b>Features</b>
Moving an event from one calendar to another is now possible in KOrganizer (the combobox in the editor dialog was disabled due to an old bug, it all seems to work now)

<b>Library cleanups (so they can become frameworks)</b>
Most of this happened in KCalCore and KContacts. The plan is to submit them for inclusion into KF5 after the 19.08 release (Sep/Oct 2019). Other libs were discussed, a longer and more specific sprint would be needed, to untangle some of the dependencies and do a major cleanup of the libraries.

<b>Deadlock handling</b>
Thanks to Dan's insights, I was able to <a href="https://phabricator.kde.org/D20326">improve greatly the handling of the unavoidable database deadlocks</a>. It's a feature from all database implementations: when multiple transactions are happening at the same time and they end up locking the same rows, the database rolls back forcefully one of the transactions and asks the application to retry. But retrying doesn't mean running the exact same SQL statements (as the code was doing)... If we retry inserting into a table we might get a different auto-increment ID than the first time, so subsequent queries (which might use this ID) have to be adjusted. Therefore we now rerun the whole handling of the current command at the C++ level, instead.
The new "retry" logic has its limits though, some transactions are currently created from another process (in ItemSync). Dan started to redesign sync'ing that it happens more server-side and we don't have those TransactionJobs anymore. To be continued...

<b>Multiple merge candidates</b>
Despite a lot of head scratching and digging through possible scenarios, we still don't know how exactly this error can happen.
But at least when it does happen, <a href="https://phabricator.kde.org/D20243">the sync doesn't get stuck forever anymore</a>.

Many thanks to all our sponsors and donators for making such sprints possible, and I hope we do this again, with more people, and less critical bugs :-)