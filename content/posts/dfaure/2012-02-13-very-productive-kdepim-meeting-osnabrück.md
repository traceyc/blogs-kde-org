---
title:   "A very productive KDEPIM meeting in Osnabrück"
date:    2012-02-13
authors:
  - dfaure
slug:    very-productive-kdepim-meeting-osnabrück
---
After 6 years of absence, I participated again in a KDEPIM meeting, to fix some issues in Akonadi in order to improve my daily life :-)

Here's a report of what I did:

Fixed: "incomplete collection" error in mixedmailresource (bug 285973)
Fixed: mixedmaildir resource can lose email when changing message flags (bug 289428)
Fixed: Assert in imap resource when resuming work after going offline (bug 291810)
Fixed: cancelling mail check doesn't really work (two different bugs)

The first two with the help of Kevin Krammer, the other two with the help of Kévin Ottens. I would not have been able to fix these bugs without their input, which shows the usefulness of in-person meetings.

Also implemented some new debugging helpers (clearing the database cache for a folder, in akonadiconsole, and new methods for dumping the scheduler task list in a resource).

I also took the opportunity to discuss other issues with people present there (with Kévin Krammer during a much-longer-than-expected travel time, and with Volker, thanks to the long waiting for our food at the restaurant!). Overall I had time to discuss most of the other things I want to fix in akonadi, even though the actual fixing will have to be done later:
- Speeding up operations leading to many change notifications, like deleting many emails, by removing the need to save all pending changes after every notification handling.
- Fixing the jumpy scrollbar when switching to a folder sorted "oldest first"
- Making it possible for the imap resource to handle multiple (readonly) operations in parallel, for performance. For instance while a large folder is being synced, reading an unrelated email in another folder currently has to wait for the syncing to finish.
- And some ideas for investigating another imap issue that seems to only happen to me: newly subscribed folder are not showing up, an error about "incomplete ancestor chain" happens at some point during the syncing, which aborts at that point.

Overall, what's very useful for a non-kdepim developer like me, forced to fix bugs in kdepim to be able to read his emails :-), is to have been able to discuss with the akonadi master-minds in order to grasp the overall concepts.
