---
title:   "Gradient support for the X server"
date:    2005-05-30
authors:
  - lars
slug:    gradient-support-x-server
---
After the last round of hacking with Zack beginning of April where we speeded up the software fallback for image compositing in the X11 Render extension, I decided to take another round, this time at missing features in X11.

Qt4 offers nice support for gradients in the QPainter API. Unfortunately, we have to completely emulate them on the client side. That means we have to create an image, draw the whole gradient into the image, transfer the whole stuff over to the server, and then blit it to the screen. Not exactly fast, especially when running your application remotely.

So I sat down last weekend and gave it a go. How hard could it be? Well, as it turned out it wasn't. By the end of today I had it all running. 

You can find a small screenshot of Xephyr drawing a radial gradient natively <a href="http://trolls.troll.no/lars/xgradients/xserver_gradient.png">here</a>. 

