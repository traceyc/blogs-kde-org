---
title:   "Konsole new features 2.13"
date:    2014-03-16
authors:
  - khindenburg
slug:    konsole-new-features-213
---
With the KDE SC 4.13 release soon, a few new features made it into Konsole.
<ul>
<li>There have been quite a few requests dealing with the tabs over the years.  Now, Konsole can load a stylesheet .css file that will be applied the the tabbar.  This can control the size, colors, and text among a few items.
There's a <a href="http://docs.kde.org/development/en/applications/konsole/tabbarstylsheet.html">section</a> in the <a href="http://docs.kde.org/development/en/applications/konsole/index.html">Handbook</a> which gives a few examples.
Configure Konsole->TabBar
<img src="http://konsole.kde.org/blogs/Features4.13/konsole_tab_css.png" alt="Konsole Tab CSS">
</li>
<li>Profiles can now store the desired column and row sizes.  This is for new windows only.  
Configure Current Profile->General
<img src="http://konsole.kde.org/blogs/Features4.13/konsole_terminal_size.png" alt="Konsole Terminal Size">
Configure Konsole->General
<img src="http://konsole.kde.org/blogs/Features4.13/konsole_use_size.png" alt="Konsole Terminal Size">
</li>
</ul>

Please provide any feedback on these or any other issues with recent Konsole versions.
