---
title:   "KDE4"
date:    2005-07-13
authors:
  - khindenburg
slug:    kde4
---
I finally gave up on getting qt-copy from /work/kde4 to compile.  I ended up editing Gentoo's 4.0.0 ebuild so that it installs into /usr/qt/4.  Unlike the 3.x ebuilds, they decided to be FHS 'correct' and not use /usr/qt/4.  This makes having both Qt3 and Qt4 installed at the same time problematic.  Anyway, the edited ebuild is on my site.

Then I get to kdelibs.  I thought of switching from /trunk but I get the dreaded and <b>extremely</b> unhelpful error message: <code>svn: Cannot replace a directory from within</code>

And on dialup I don't care to do a straight checkout.

And so it goes... :-()

<!--break-->