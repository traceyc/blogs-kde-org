---
title:   "Gentoo/cmake ebuilds"
date:    2006-05-17
authors:
  - khindenburg
slug:    gentoocmake-ebuilds
---
For those using Gentoo and building KDE4, you'll find the latest ebuild for cmake here:
http://bugs.gentoo.org/show_bug.cgi?id=131080

I personally have not been using their rpath patch... KDE4 works w/o it...