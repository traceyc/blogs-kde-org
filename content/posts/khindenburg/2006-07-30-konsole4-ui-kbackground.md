---
title:   "Konsole4-ui / KBackground"
date:    2006-07-30
authors:
  - khindenburg
slug:    konsole4-ui-kbackground
---
So I created work/konsole4-ui to work on porting Konsole to XMLGUI (and LiveUI?).  I'm also using as a test area for some ideas and code cleanup.  So far so good, I got the session menus to work.  The shortcuts are an issue I have to look into again.  Things will be much better when I can actually run KDE4 so I can test session managment, config files, etc...

Many thanks to Robert Knight who has been doing a lot of work on Konsole' backend for KDE4.

I like APOD images and in general cool outer space images.  I wanted to place these small images (400x400) on my KDE desktop in a certain location that didn't have anything else there.  How to do this?  No idea... :-(  I couldn't find a single program KDE or not to handle this.  KDE3 KControl/Background has a few issues... urgh!  I hope KDE4 is "better".

Anyway, since I've coded KDE kdesktop/background DCOP before, I threw together a DCOP call to position an image on KDE's desktop.  It is not perfect and it won't go into KDE3.  But it works... :-)

void setWallpaper(int desk,QString wallpaper,int X,int Y)

dcop kdesktop KBackgroundIface setWallpaper 1 %s 400 0

The %s will get changed to the image... pretty cool!