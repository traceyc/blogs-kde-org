---
title:   "Houses and Konsole tab colors"
date:    2005-05-15
authors:
  - khindenburg
slug:    houses-and-konsole-tab-colors
---
Well, I put an offer on a house today... rather nervous.  I expect a counter-offer tomorrow; hopefully by Monday I should know..

In KDE3.5, you will be able to set Konsole's tab text color via the tab menu.  I'm thinking about adding the ability to change the color via an ESC code.
I'm not sure if I should mimic the ESC[0;#m and/or create a new code say ESC[0;RRGGBBy.  It is rather easy to code.

Tried to create a new branch of konsole today.  I couldn't figure out how to do it in svn.  Hopefully someone will repond to my post on kde-devel.
<!--break-->