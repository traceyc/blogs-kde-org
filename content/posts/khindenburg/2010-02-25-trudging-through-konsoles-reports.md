---
title:   "trudging through Konsole's reports"
date:    2010-02-25
authors:
  - khindenburg
slug:    trudging-through-konsoles-reports
---
With only a few hours a week to work on Konsole, I often view the logs and other reports for simple issues to investigate.  The compile warnings were fixed sometime ago.  Most of the simple Krazy and Apidocs issues were also fixed.  The remaining Krazy issues would require some heavy duty changes.
<!--break-->
Recently I've been trudging through the -Weff++ logs.  I coded a simple Ruby script to parse them as Qt generates a ton of warnings.  Currently there are around 300 issues.  The vast majority of these fall into 2 categories:
<ol>
<li> ... should be initialized in the member initialization list</li>
<li> ‘class ...’ has pointer data members
    but does not override ‘Class(const Class&)’
    or ‘operator=(const Class&)’</li>
</ol>

<ol>These 2 can be fix easily enough.  
<li>Add them to the constructor watching the order of the initialization list</li>
<li>Add these to the private section of the .h file:
    // Disallow copy and assign
    Class(const Class &);
    Class & operator=(const Class &);
  If anyone tries to use the Copy or Assign, it will fail at build time.</li>
</ol>

<br />
Thoughts on these 2 issues?
<br />

I did manage to get the Apidocs to generate the class hierarchy in graphic form which is very helpful.  Although I'm still struggling with using valgrind for memory issues (and unfortunately, valgrind doesn't work on Mac Snow Leopard).