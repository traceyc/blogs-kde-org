---
title:   "cmake ebuild for KDE4"
date:    2006-03-19
authors:
  - khindenburg
slug:    cmake-ebuild-kde4
---
For those running Gentoo, I've created an ebuild for the cmake required to compile KDE4.  

Although, the current kdelibs4_snapshot doesn't seem to compile with cmake.  :-(

http://kurt.hindenburg.name/
http://kurt.hindenburg.name/downloads/cmake-2.3.4-20060317.tar.gz

<!--break-->
