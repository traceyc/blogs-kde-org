---
title:   "KDE test scripts report #2"
date:    2005-09-29
authors:
  - khindenburg
slug:    kde-test-scripts-report-2
---
For those that use KDE Automated Test Report located at http://kde.icefox.net/tests/, I have created a new script that generates a different report.

It is a Ruby script, playground/base/kdetestscripts/create_module_reports.rb, that displays every file and the errors/warnings for that file on a per module basis (kdelibs, kdebase, etc).

The new report2 is not automatically generated online.  You'll have to first use the ./generatereport script locally to create a report/ directory.  Then use the create_module_report.rb script on that report/ directory.

<code>./create_module_report.rb report/</code>

The report2 files can be big:
<code>
 37254 Sep 28 12:21 report/report2-arts.html
  1365 Sep 28 12:21 report/report2.html
411392 Sep 28 12:21 report/report2-kdebase.html
508761 Sep 28 12:21 report/report2-kdelibs.html
162100 Sep 28 12:21 report/report2-kdepim.html
</code>

I find this new report to be easier to determine what errors/warnings are for a specific package (ie Konsole).