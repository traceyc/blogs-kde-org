---
title:   "World Cup is coming"
date:    2006-06-08
authors:
  - barefooted
slug:    world-cup-coming
---
FIFA World Cup is coming to town.
I just walked through the city and saw the first fans arrive.
Although I don't like football at all, it's nice to see what's happening here in <a href="http://www.gelsenkirchen.de">Gelsenkirchen</a>.
We got a new station, many streets have been refurbished, the city is full of people again.
It's a very seldom opportunity to meet people from a lot of other countries in your own city :-)

There are few things to moan about:
- everywhere you look: football :-/
- everywhere you look: german flags on cars, windows, houses, people

It's not that I wouldn't mind people having/wearing German flags.
I just think it's plainly stupid to do this for the following four weeks and then hide those flags away again.
If I'm proud about my country, I'd show it anytime - not only for those four to six weeks before and while the World Cup takes place...

Anyway, I'm happy with the World Cup taking place in Germany and especially in Gelsenkirchen as this is a great opportunity for the city to shine. In general, everybody's ever complaining how bad things are going here - not for the weeks to come.

If anybody comes to visit, have fun :-)
If anybody who comes to visit wants to meet KDE/Linux-people, feel free to contact me ;-)
