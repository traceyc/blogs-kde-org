---
title:   "Nice effects"
date:    2005-02-12
authors:
  - barefooted
slug:    nice-effects
---
At first, as this is my first posting to kdedevelopers.org, I'd like to introduce myself.
To avoid writing the whole stuff again, please take a look at <a href="http://www.kexi-project.org/pbarefeeted.html">this page</a>.

Now, after you know who I am, I can start...
As noted on the page above, I'm working on effects in Kexi.
This really is for the eye-candy department.
On the <a href="http://kinitiator.sourceforge.net/kexi/fd2.png">shot</a> you can see a nice gradient that fades with a background pixmap and a label that has a drop-shadow below the text.
Looks nice, eh?

The code for this is in CVS for some days now.
Now, what have I done in the time between?
I worked on a generic feedback wizard and released a first version on <a href="http://kde-apps.org/content/show.php?content=20360">kde-apps.org</a> today.
You can use it for every application you like very easily.
It will perhaps be included in Kexi.

After introducing myself, I'll stop blogging now and return to KDevelop...
KEFeedbackWizard still needs some improvements ;-)