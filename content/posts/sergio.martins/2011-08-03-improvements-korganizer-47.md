---
title:   "Improvements in KOrganizer 4.7"
date:    2011-08-03
authors:
  - sergio.martins
slug:    improvements-korganizer-47
---
You can now set a custom calendar icon and have it shown in each agenda/month view events.

Before:
<img src="https://lh6.googleusercontent.com/-WidLfBpljUQ/TjiJ9PGhgmI/AAAAAAAAAMA/HkZbqyVtKD4/4.6_1.png" />

Now:
<img src="https://lh6.googleusercontent.com/-e-RAQhxGpes/TjiJ9GetTaI/AAAAAAAAAME/aTjRCMqXdh0/4.7_1.png" />

Makes the view much more pleasant to look at when you have dozens of events, and you don't have to memorize which colours belong to which calendars any more.

To set an icon, go to the Calendar Manager, in the sidebar ( bottom left ), right click, and choose "Folder Properties":

<img src="https://lh4.googleusercontent.com/-04pw5jbIjLk/TjiHrhm9kzI/AAAAAAAAALs/WheSCw_4q6o/foldprop.png" />

You can disable this feature in Settings->Views->Agenda/Month->Icons to show.


<br>
<br>

See you at the Desktop Summit ;-)