---
title:   "KDEPIM Git Resource"
date:    2012-02-15
authors:
  - sergio.martins
slug:    kdepim-git-resource
---
KDEPIM Git Resource

You can now monitor any git repository with KMail. Commits will appear as e-mails in the message list.

KMail configuration:
<a href="https://lh3.googleusercontent.com/-4WHJHl-n1Sk/TzwOG3uTEKI/AAAAAAAAASE/agx9jzCEfuQ/s720/configure.png" target="_blank"> <img src="https://lh3.googleusercontent.com/-4WHJHl-n1Sk/TzwOG3uTEKI/AAAAAAAAASE/agx9jzCEfuQ/s128/configure.png"> </a>

Resource configuration:
<a href="https://lh3.googleusercontent.com/-J_feruGMCUc/TzwOGdxh4JI/AAAAAAAAASA/isdYqIuONt4/s294/configure2.png" target="_blank"> <img src="https://lh3.googleusercontent.com/-J_feruGMCUc/TzwOGdxh4JI/AAAAAAAAASA/isdYqIuONt4/s128/configure2.png"> </a>

Message list:
<a href="https://lh6.googleusercontent.com/-_dWnn5GA4lQ/TzwOHMgdWRI/AAAAAAAAASM/EgKrgJbEOoA/s912/print.png" target="_blank"> <img src="https://lh6.googleusercontent.com/-_dWnn5GA4lQ/TzwOHMgdWRI/AAAAAAAAASM/EgKrgJbEOoA/s128/print.png"> </a>

It will do a git fetch every 5 minutes or so.

This is still a playground project, so it has some limitations:
- Only master is supported yet
- If authentication is needed for the git-fetch, you must run ssh-add
in the same terminal where you're going to start akonadi. ( ssh-add && akonadictl restart )


While not needing to depend on commitfilter.kde.org or other external notification services is great, this wasn't my primary reason for creating this resource.

I did it because we can, or rather "since KDEPIM>4.4 we can".

Thanks to the new KDEPIM architecture, the application is now really decoupled from the data and that opens us a world of possibilities.

The git resource took me only 8 hours of coding, without needing to touch a single line of KMail code, therefore not introducing any regressions.

The barrier to contribute new features to kontact has never been so low.

--
git clone git://anongit.kde.org/akonadi-git-resource