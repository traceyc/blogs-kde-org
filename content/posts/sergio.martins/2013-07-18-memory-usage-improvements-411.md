---
title:   "Memory usage improvements for 4.11"
date:    2013-07-18
authors:
  - sergio.martins
slug:    memory-usage-improvements-411
---
The other day I went to buy a DDR stick but the shop was closed so the only solution was to sit down and fix some memory hungry applications ;).

These will be in 4.11:

* Fixed a bug where maildir resource would use 1 or 2GB when importing large folders.

* Fixed a bug where mixedmaildir resource would also use 1 or 2GB.
  (Thanks to Martin Steigerwald for providing a a 77k e-mail folder).

* akonadiserver wasn't clearing uneeded query caches. Saved 65MB here.

* Kontact had 5 copies of all calendaring data in memory if you used summary view which
  accounted for 200MB with my big test .ical file.

* KOrganizer grew infinitely over night under some circumstances.

* KAlarm wasn't freeing editor dialogs, resulting in infinite memory usage over time.

* Mysql got up to 25 MB of savings (4.12)

Also, Dan Vratil and Vishesh Handa have been working hard on performance/memory of nepomuk and akonadi.


To be continued...
