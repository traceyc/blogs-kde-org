---
title:   "clazy: Suppressing warnings"
date:    2016-04-25
authors:
  - sergio.martins
slug:    clazy-suppressing-warnings
---
Clazy <sub>[1][2]</sub> just got support for suppressing warnings by reading a special comment in your code.
The syntax is similar to what krazy has.

Ignore the whole file:
<code>// clazy:skip</code>

Ignore the whole file for these checks:
<code>// clazy:excludeall=foreach,qstring-allocations</code>

Ignore these checks at the line number where the comment appears:
<code>(...) // clazy:exclude=qfileinfo-exists,copyable-polymorphic</code>

Enjoy!

[1] <a>https://www.kdab.com/use-static-analysis-improve-performance/</a>
[2] <a>https://blogs.kde.org/2015/11/15/new-cqt-code-checks-clazy-static-analyzer </a>
