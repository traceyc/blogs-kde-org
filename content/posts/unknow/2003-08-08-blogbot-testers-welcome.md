---
title:   "Blogbot testers welcome"
date:    2003-08-08
authors:
  - unknow
slug:    blogbot-testers-welcome
---
The bot seems to behave, so I welcome any testers. If you want to use it, subscribe to it at kdedevelopers@jabber.org. It will reply with a short welcome message and ask for authorization in turn.<!--break-->Use $help to get a small help text and one command per message. Your passwords are being encrypted, so you don't have to fear that I will collect them. :)