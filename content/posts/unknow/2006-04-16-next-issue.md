---
title:   "Next Issue"
date:    2006-04-16
authors:
  - unknow
slug:    next-issue
---
So, it is that time of the week again... commit-digest time!
<a href="http://commit-digest.org/issues/2006-04-16/">Read the latest issue here</a>

This week, I have enhanced the <a href="http://commit-digest.org/">commit-digest.org</a> site quite a bit (though I am certainly not finished) - now there are actually links in the sidebar, so it is now a functional random strip of blue, and I have implemented the <a href="http://commit-digest.org/options/">options</a> page that I always wanted. This means that you can now modify how the digest looks in your browser, for example, if you don't like the italic quotes, change to regular. And if you want the sections to be a bit more defined, turn on "highlight headers". These are all features that have been requested, so hopefully at least Pino will be happy :)

I've also extended the <a href="http://commit-digest.org/archive/">archive</a> by adding another 32 digests, extending back to 3rd January 2003. I still have a lot of work to do on the archive, as it doesn't even have entries for all the digests that existed yet. But it it moving ahead. I need to think about how I am going to extract and then represent the information from the CVS days, as webcvs is now offline... i'll be adding all the introduction segments from the digests to the archives first, as they are the most interesting to the casual reader.

Enjoy the <a href="http://commit-digest.org/issues/2006-04-16/">latest digest</a>, and don't forget to leave any comments at <a href="http://dot.kde.org/1145181720/">this dot story</a>!

p.s. To be notified at the exact moment each new digest goes online, subscribe to <a href="http://commit-digest.org/updates.rdf">this RSS feed</a>.

<!--break-->