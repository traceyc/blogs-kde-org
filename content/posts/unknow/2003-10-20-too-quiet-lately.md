---
title:   "Too quiet lately."
date:    2003-10-20
authors:
  - unknow
slug:    too-quiet-lately
---
So, I've been rather quiet on the KDE front lately. I've been busy, though. As much as it might shock and dismay a lot of you, I've been playing around with GNOME. I've even immersed myself in it, seeing exactly how their desktop works and taking notes of things that I like and don't like. The main reason for this is that I was reading through <a href="http://mail.gnome.org/archives/nautilus-list/2003-September/msg00071.html">a thread on the Nautilus mailing list entitled "We're going all spatial."</a>

Naturally, I was interested. As I suspected, the thread referenced <a href="http://arstechnica.com/paedia/f/finder/finder-3.html">John Siracusa's article on the Spatial Finder</a> over at Ars Technica. (Great site, btw. I love those guys.) And I thought when I read the article the first time that spatial file management just might have some benefits, but I wasn't really convinced. I remember MacOS 8 and MacOS 9, and I hated those operating systems with a passion. But I didn't really hate the Finder, and I had never bothered to really analyze it, so the article was informative and eye-opening for me.

After reading that the Nautilus team had decided to go with a spatial model (only they started calling it 'object oriented' for some unknown reason), I decided that it was time to give it a try. So the code is in their CVS, and they do a few things right. I also think they're doing a few things wrong, but that could just be my perspective.

My initial conclusion: Spatial file management really is neat. I want a spatial filemanager for KDE. Konq is awesome - it's the most impressive single application I've ever seen, but it's really something of a kitchen sink application; it's a web browser! No, it's a file manager! No, it's a universal document viewer! No, it's an orgy of KParts! The list goes on. But what Konqueror doesn't have is lightning fast response times, a small code footprint, and a focus on file management.

This is not a dig against Konq. I love Konq. Konqueror was THE reason I started using KDE from CVS back in the pre-KDE2 days. But just because I love it doesn't mean that I think it's the best application for every single application that it can do.

I want a spatial file manager in KDE, so I've decided to code it. Extra ideas I've had have come from John's article and from the things that I've decided the Nautilus guys are doing wrong. For example - in Nautilus in CVS - the default icon layout is 'Arrange by Name' but - if you move an icon, it doesn't automatically switch the layout from 'Arrange' mode to 'Manually arrange.' There are a dozen or so small issues that I've found like that with Nautilus, and there are a few really big ones as well. (For one, I don't care how long it's been since I clicked on the desktop. If I click on my home folder, the folder's contents should come up instantly. Without question. Nautilus takes quite a while if you don't use it - on the order of six or seven seconds on my machine, which is just shameful.)

Watch this space.

Also, in Kiwi-related news - Cliff has taken over and done another rewrite of the backend. It's a little buggy again, but it's also smaller, faster, and easier on the memory. Plus it has live searching now. Check it out at http://kiwimusic.sf.net and give some feedback to us :)