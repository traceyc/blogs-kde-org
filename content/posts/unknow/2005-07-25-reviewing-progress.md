---
title:   "Reviewing progress"
date:    2005-07-25
authors:
  - unknow
slug:    reviewing-progress
---
Some of you may now have guessed the project that I hinted at in my last entry. Well, I am not going to talk about that this time (I will in my next entry, I promise!), other than to say that it is running fine after switching servers - sorry for using so much of your traffic allocation, cmk! :)

Working for the past few months with in kde-edu with Albert (of KPDF fame), we have been creating a new educational game for KDE, called KSimon. 

<img src="http://www.tuxipuxi.org/files/ksimon.png" class="showonplanet">

KSimon is a clone of the electronic "Simon" game that many of you may remember from the late 70's. The gameplay consists of ever-increasing sequences of lights appearing, which you must re-enter in the correct order. We think it is a lot of fun :)

As we believe that KSimon is now a fully-functional, ready-to-be-released app, we invite you to test it (yes, software testing can sometimes be fun!). KSimon is currently located at kdereview/ksimon.

Enjoy, and don't forget to tell us what you think in #kde-edu!
<!--break-->