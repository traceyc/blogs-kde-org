---
title:   "libxmpp for Jabber"
date:    2003-07-23
authors:
  - unknow
slug:    libxmpp-jabber
---
I've been checking out libxmpp lately, the successor of libpsi. Actually I am supposed to be studying for exams, but well, you need to have a few interesting things to do once in a while. :)

Justin allowed us to use development snapshots from the library, and since it is still close to libpsi in terms of the API, porting will not be too hard. I can't wait for Kopete 0.7 to be released and CVS to be open again.

The features it will bring:

- filetransfer (woohoo)
- presence signing
- proper encryption support
- new multi-user chat code

...and many other advantages of course. Also, since this library will at least be shared between Psi and Kopete, it will most likely be released independently and thus a whole bunch of static code can be pulled out of the current plugin.

Actually, I should hope that Kopete won't be released during the next month or I'll most likely get lost porting the plugin.

The broader effects to Kopete of complying to more JEPs will most likely include necessary changes in some of the plugins. For example, the cryptography plugin changes the messages on the fly without a protocol being able to control the encryption. Probably a "crypt-on-request" method should be added which simply returns the encrypted version of a string.

Multi-user chat is still my biggest problem since internally, Kopete handles a chat room as one single contact. The difference to IRC is, that all group chat messages are passed from the server via this single contact. This makes displaying the messages correctly (that is, with the correct nickname prefixed) very hard since Kopete solely relies on the contacts it knows for displaying the messages. Right now, all messages in a chat room appear to be originating from the room itself and one is not able to distinguish between the room members.

Think, think, think....