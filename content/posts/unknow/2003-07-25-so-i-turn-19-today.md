---
title:   "So I turn 19 today"
date:    2003-07-25
authors:
  - unknow
slug:    so-i-turn-19-today
---
What fun! What glory! I have a birthday today.

So, work is interesting. I get to set up a PII 400 machine, for use as a server. FreeBSD 4.8 is the OS of choice (I just like FreeBSD on my servers, and I don't know why.)

But, there's also a dual PIII-550 box that is just begging me to put it into service as a database machine. Choices, choices....

KDE-related - hmmm. One of the reasons that I post this interesting information is so that people will know - the new server will probably become svn.c133.org, which is where Kiwi development will be happening. (Hooray!)

I'm probably going to import the dotNET sources, too, just for fun. Maybe not, though. kdeartwork is a good location for dotNET. And I wonder what Cullman is planning on doing with that fork in kdenonbeta/clean...

BTW: Since I don't believe in editing my posts to correct them, but posting corrections in later updates (editing your past content, even for corrections, seems sort of revisionist to me)... I would like to apologize to wheels over the comments I made about JuK. I will now freely admit that Kiwi is behind JuK when it comes to features. I'm going to fix this, of course, but it helps that wheels seems to be slowing down and only doing bugfixes on JuK lately. MWAHAHAHAHA!

Not that Kiwi couldn't use bugfixing. It can. But I've fixed all of the obvious bugs, or at least the ones I can find. I'm going to be writing up our first non-Library based source (hopefully this weekend) which means that Kiwi may actually get either a) Internet Radio or b) Audio CD or c) Smart playlists this weekend. All depends on what I have going on. I need more time to code. Birthdays, work... a Jedi craves not these things.

Also, I really hope we get approved for our apartment application. That'd be a nice birthday present.

EDIT: Ok, so this post makes me a liar about my policy of non-editing to fix stuff. Had to change entries like (a) to a) because if I don't use c), I get (c), which is not what I want. ::sigh::