---
title:   "Progress, in any language"
date:    2006-06-07
authors:
  - unknow
slug:    progress-any-language
---
For the last few days, I have been busy implementing translation support for the <a href="http://commit-digest.org">KDE Commit-Digest</a>. Both the website and the actual digest introduction are now able to be translated and displayed in other languages. Translating the digest introduction every week is a much bigger undertaking than a one-off translation of the website, and so only for <a href="http://commit-digest.org/issues/2006-06-04/?showlanguage=it">the hardcore</a>.

Right now, thanks to the following people, the site is available in 4 languages (excluding English):
<ul><li><a href="http://commit-digest.org/?showlanguage=it">Italian</a>: Leonardo Cassarani (emc2[]) and Pino Toscano (pino)
<li><a href="http://commit-digest.org/?showlanguage=sl">Slovenian</a>: Jure Repinc (JLP)
<li><a href="http://commit-digest.org/?showlanguage=nl">Dutch</a>: Rinse de Vries (rinse)
<li><a href="http://commit-digest.org/?showlanguage=de">German</a>: Carsten Niehaus (carsten) and Martin Pfeiffer (hubipete)</ul>

(To make a permanent selection from the (soon to be increasing? ;)) languages, use the <a href="http://commit-digest.org/options/">Options</a> panel)

Which I think is pretty nifty. But, of course, that is not too impressive considering the <a href="http://i18n.kde.org/stats/gui/stable/toplist.php">variety of languages that KDE is available in</a> :).

So, if you are able to translate the Commit-Digest website to be available in your language, get in touch (<a href="mailto:danny@commit-digest.org">email</a>, #digest on IRC, or midnight arrival at my house? :)), and i'll walk you through the proceedure. This fun activity should only take about 45 minutes, and you'll be credited on the translation page that i'm about to make.

And I'll have more announcements shortly :)

<!--break-->