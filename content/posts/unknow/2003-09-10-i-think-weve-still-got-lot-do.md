---
title:   "I think we've still got a lot to do."
date:    2003-09-10
authors:
  - unknow
slug:    i-think-weve-still-got-lot-do
---
So much to code, so little time...

I found an older .plan, and was reading through it. It's from roughly two months ago, and it has notes such as "Find all configuration pages that have a sucky UI, and make them suck less." Heh. Was I really that naive so recently?

Speaking of which, aseigo, how is that going? I mean KMail, of course. ;)

Hmmm. So the GNOME panel blogging applet thing that Seth did is pretty neat. We should steal it and use something similar in KDE. (Although preferably something with slightly less ass. Seth, your applet shows the window and then moves it! Bad form. I was sorely disappointed by that.)

I think that I can do at least a slightly better interface for a Kicker blogger applet, but we'll see. I know it won't make it into KDE 3.2, but if we ever do make a KDE 3.3, there's a chance it might be in there :) Also, I need to implement the tasklist button for Kicker as well. ::sigh::

Anyway. Asteroid is still improving, although much more slowly now. Still haven't done sliders yet. Found some annoying bugs using it with KDE 3.1. Ugh.

That's enough for now. I'll update later and let everyone know how things are going.