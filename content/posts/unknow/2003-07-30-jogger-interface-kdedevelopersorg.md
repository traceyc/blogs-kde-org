---
title:   "Jogger interface for kdedevelopers.org?"
date:    2003-07-30
authors:
  - unknow
slug:    jogger-interface-kdedevelopersorg
---
What would you think about a Jogger interface for kdedevelopers.org?

For those who don't know Jogger, it is a Jabber component that allows you to post diary entries via any Jabber client, such as Kopete for example. To see it in action, you can see one installation at <a href="http://jogger.jabber.org">http://jogger.jabber.org</a>.

Since most IM clients feature RichText editing capabilities, this would be an ideal companion for the web-interface and KBlog. Especially since IM clients are the type of "always on" applications, blog entries could be submitted with a single mouse click.

As I couldn't reach him on IRC right now, I didn't talk with Ian about this yet. If he doesn't want it, this idea might not be worth the blog entry. :)