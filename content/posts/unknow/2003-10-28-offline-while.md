---
title:   "offline for a while"
date:    2003-10-28
authors:
  - unknow
slug:    offline-while
---
i'm having a coding frenzy at home at the moment while waiting
for my adsl to be installed. amazing the amount of work you can 
get done when irc, mail and even sane tv listings are all unavailable.

its a very long time since i've recieved such a small amount of email
each day. i'm down to only 200 at the moment, from 1000-1200 / day.

for some reason i thought i'd write this note just in case people are wondering why the heck bookmarks are unchanging and beta's are 
flying past... (but, then, "bookmarks, whats that then?" is my most
commonly quoted response to any news about them :) )

see y'all soonish,
Alex