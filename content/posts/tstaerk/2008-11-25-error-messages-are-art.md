---
title:   "Error messages are art"
date:    2008-11-25
authors:
  - tstaerk
slug:    error-messages-are-art
---
Writing good error messages for your programs is art. Your user gets an error message - he cannot ask his computer "how do you mean this?". Error messages are important because they can help you fix a problem. Some error messages are critical because the error prevents you from achieving anything. One example are the error message of startkde. When you have a problem with startkde, you have a real problem. If you cannot solve it, you cannot work (with KDE) at all. Sad enough, some error messages resemble to the infamous OpenOffice help that (while leaving out important context) explains things like
<br />
<br />
File|Open : Opens a file
<br />
<br />
Today I <a href=http://websvn.kde.org/?view=rev&revision=888689>improved an error message in startkde.</a> The error message was like the following:
<br />
<br />
Cannot start kstartupconfig4. Check your installation.
<br />
<br />
This error message had made me copy kstartupconfig4 everywhere: /bin, /usr/bin, /usr/local/bin and I was just about to copy it to /sbin when I started mistrusting this error message. Analyzing the code I found out kstartupconfig4 was found, called and executed, but delivered a return code of 3 (non-zero meaning error). So the error message missed point one for good error messages:<br />
<b>be correct</b><br />
Second, its advice how to proceed was too generalized: "Check your installation" is not only what you do when kstartupconfig4 is delivering an error. You also do it if KDE consumes too much system load, when it eats your data or when your computer shows a black screen or starts mooing. So the error message misses point two for good error messages:<br />
<b>be concise</b><br />

OK, so what would be the best error message here? Let's start with this:
<br />
<br />
Calling kstartupconfig returned an error.
<br />
<br />
Because, no matter if it exists or delivers a return code of 3, it <i>returns an error</i> anyway. If it does not exist, it delivers the return code 127.<br />
But wait.<br />
This error message is not understandable for a normal user. I would ask myself "well, was it found or not?" if I got an error message like this. That brings us to point 3 for good error messages:<br />
<b>be understandable</b>
<br />
OK, trying to be understandable brought me to my final error message:<br />
<br />
kstartupconfig4 does not exist or fails. 
<br />
<br />
Good stuff, isn't it? Not yet. It can still be improved.
<ul>
<li>Tell the user to check permissions</li>
<li>Tell the user to check the existance of files</li>
<li>Tell the user to check your files are in the right directories</li>
<li>decide upon the return code what action to recommend to the user</li>
<li>hand over an error message from kstartupconfig4 to startkde and present it to the user</li>
</ul>

OK, now as you are eager to start re-designing broken error messages, I just want to add a big THANKS to virtualization. Without it, I would never have started kompiling my desktop environment from scratch. VMWare Server, VirtualBox, should I make a mistake and screw my system, I know you will forgive me by allowing a decent rollback to my latest snapshot! Thanks, thanks!!!