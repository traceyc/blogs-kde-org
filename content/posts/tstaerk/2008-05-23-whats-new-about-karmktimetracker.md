---
title:   "What's new about karm/ktimetracker"
date:    2008-05-23
authors:
  - tstaerk
slug:    whats-new-about-karmktimetracker
---
Since some years I have the pleasure to maintain KArm, the friendly KDE timetracker. KArm allows users to find out how much time they spend on which task since longer than 10 years. For KDE 4, it was time for a big renovation.<br>
<ul>
<li>name changed from KArm to ktimetracker</li>
<li>manage history</li>
<li>track active applications</li>
<li>icon order</li>
<li>drag&drop</li>
</ul>


<b>Name change</b><br>
The most obvious change for KArm is that it is no longer called so. Distributors always face a problem:<br>
<pre>
Applications
    ­ |
      ----> KArm
      ----> oKular
      ----> amaroK
</pre>
<b>Problem:</b> What does e.g. the oKular program do ?<br>
or do they want the user to understand the menu by telling the purpose of the programs:<br>
<pre>
Applications
     |
      ----> Time Tracker
      ----> PDF Viewer
      ----> Video Player
</pre>
<b>Problem:</b> How do I call the "PDF Viewer" if I do not have access to the menu ?<br>
or do they simply <i>bloat the UI</i>:<br>
<pre>
Applications
     |
      ----> Time Tracker (KArm)
      ----> PDF Viewer (oKular)
      ----> Video Player (amaroK)
</pre>
<b>Problem (a):</b> The experienced user does not want to read so much<br>
<b>Problem (b):</b> The newbie asks himself "is it so hard to name applications appropriately?"<br>
To be short, problems arise if <br>
(a) new users start with KDE and<br>
(b) application names differ from their purpose, i.e. you cannot tell from the application name what the program does.<br>
BTW, this is why I think renaming kpdf to oKular was a bad idea.<br>
Having the kde time tracker named ktimetracker is as sane as the naming of kword, kedit, kmail, konsole and kompare. Sometimes it takes 10 years for a KDE program to get a decent name, but then it is about to last. BTW, KArm never had anything to do with weapons ;)<br>
OK, so all the future users calling karm will stand in front of a konsole saying <br>
<pre>
bash: karm: command not found
</pre>
? Not by any means. karm is now a stub program that reminds you about the new name of our timetracker:
<img src=https://blogs.kde.org/files/images//karmoutput.png />
<hr>


<b>manage history</b><br>
You can now manage what you have done (call it journal or history). You can edit the start time, end time and you can add a comment about it:
<img src=https://blogs.kde.org/files/images//managehistory.png />
<hr>


<b>track active applications</b>
It is now possible to automate your time tracking by active applications. This means, as soon as konsole gets the window focus, a task named "konsole" is added (if it wasn't there before), all other tasks are stopped, and the konsole task is started.

<b>Icon order</b><br>
I listened to my users (no, really: I listened to my users) and changed the icon order in ktimetracker - the first thing you want to do is to add a task - so I put this icon on the left. Needless to say, all other icons are now right as well :)
<hr>


<b>Drag and Drop</b><br>
Yes, drag and drop finally works with KDE 4.