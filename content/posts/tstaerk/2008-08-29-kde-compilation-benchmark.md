---
title:   "KDE compilation benchmark"
date:    2008-08-29
authors:
  - tstaerk
slug:    kde-compilation-benchmark
---
Many of us have the cool <a href=http://en.wikipedia.org/wiki/Nokia_N810>Nokia N810</a> that is an <a href=http://en.wikipedia.org/wiki/ARM_architecture>ARM</a> system based on <a href=http://en.wikipedia.org/wiki/Maemo_Platform>maemo</a>. To compile software for it, you will normally use <a href=http://en.wikipedia.org/wiki/Scratchbox>scratchbox</a>. What a pitty scratchbox only runs on 32bit hardware. As a proud user of a 64bit desktop, I have to use a virtual machine for running scratchbox. Now the question is what is the better virtualization solution: VirtualBox or VMWare?

Time for a KDE Compilation benchmark. I compiled KDEPIM Revision 854206 once in a VirtualBox-virtual machine, once in a VMWare-virtual machine.

With VirtualBox, you can only use of of your CPU cores, with VMWare Server, you can use two.

Here is the result:
Compilation with make in VirtualBox: 1 hour 13 minutes
Compilation with make -j2 in VMWare Server: 38 minutes 32 seconds


So the winner is... VMWARE !