---
title:   "Call Graphs, Eclipse and Techbase"
date:    2009-06-21
authors:
  - tstaerk
slug:    call-graphs-eclipse-and-techbase
---
Some weeks ago, someone posted a question on the KDE PIM mailing list "Which IDE do you use" or so. This reminded me of the ideals of my youth when I believed that the better your IDE - the more efficient your programming work - the more you get done in a given time for your software development.
<!--break-->
Well, since some years I have not been using any IDE any more. First because I was too lazy to integrate KDE 4 into kdevelop. Then Thomas wrote <a href=http://techbase.kde.org/Getting_Started/Set_up_KDE_4_for_development#KDevelop> this cool howto</a> and I got my ktimetracker to kompile from within kdevelop quickly. Then I was frustrated by several glitches of kdevelop. In my eyes, there is no tremendous difference to kwrite. However, I gave up again.

OK, on this mail to the KDE PIM mailing list, I saw eclipse mentioned. I thought it would be a good idea to pass a weekend evaluating it and writing a techbase article on it. And I can only say - I am overwhelmed. Apart from some minor glitches (to import a project, do not choose "File -> Import", but "File -> Open") I just get what I wanted:
<ul>
<li>Code completion works</li>
<li>Running applications with one click works</li>
<li>You can carry on svn'ing</li>
<li>Classes are detected dependably</li>
<li>Quickly switch between implementation and declaration (meaning you can get to the apidox-explanation of a function quickly)</li>
</ul>
And, the killer feature for me: Call graphs work. Yes, those little graphs telling you what function calls which! That do not even work well in the stand-alone program doxygen!

If you now want to know how you can use eclipse for your KDE development, <a href=http://techbase.kde.org/Getting_Started/Set_up_KDE_4_for_development#Eclipse> here is my tutorial</a>.