---
title:   "Using a virtual machine in an icecream cluster"
date:    2008-09-28
authors:
  - tstaerk
slug:    using-virtual-machine-icecream-cluster
---
As I pointed out <a href=http://blogs.kde.org/node/3675>recently</a>, I only develop KDE in a virtual machine. It does not only enable me to rollback changes that screwed up something, it also allows me to go back to a verbatim snapshot where I can e.g. be sure that there are no mysterious plugins installed to directories that I have not thought of. I also pointed out that <a href=http://blogs.kde.org/node/3649>compiling in a virtual machine is slower</a>, because you cannot use more than 2 processor cores per virtual machine. No problem! Use coolo's icecream and build up a compile cluster as described on <a href=http://en.opensuse.org/Icecream>http://en.opensuse.org/Icecream</a>. I have done it and at the moment my fans are roaring and compiling the KDE for my virtual machine.