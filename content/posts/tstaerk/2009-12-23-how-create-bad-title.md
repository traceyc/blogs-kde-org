---
title:   "How to create a bad title"
date:    2009-12-23
authors:
  - tstaerk
slug:    how-create-bad-title
---
At work, I stumbled across a problem that I want to declare as universal. I got a mail with a title like

<b>christmas party invitation</b>

I deleted this mail without reading because I knew I would not go there. Fine so far. But later I found out this mail contained one sentence that actually was of interest to me:

"due to the preparations of the christmas party, the canteen will be closed starting 12:30"

So, what happened? Someone had created a bad mail title (also known as subject) and as a consequence I could not eat for lunch. This is what I call bad information logistics. Logistics deal with goods being 
* at the right time
* in the right quality
* at the right place
Information logistics deals with the same, just "goods" being "pieces of information" (also known as "knowledge"). Structure information wisely. Make good titles and mail subjects. Tell the reader of the subject if he or she should read the mail. Most commercial websites are a mess of "information" but nobody knows what to read. Imagine a company website having only the following items:
* click here if you want to buy online
* click here if you want to apply for a job
* click here for hot-air-powerpoint slides around our products
* click here if you want to complain

Hey, a site-map would not be needed any more. Customers would not be scared any longer by too much information. Just put yourself into the situation of a user/reader.

Now the same stuff happens when you read about regex's. Take the tutorials. It is unbelievable how quickly people get to talk about branches, pieces and atoms and completely fail to understand why you use regex's at all:

Find all lines that (do/do not) (contain/start with/end with) (the string/one of the letters/a character range) XXX, followed or not by [same again]

This is exactly what I aim at with my software <a href=http://www.staerk.de/thorsten/index.php/Software/Krep>krep</a>. It gives you the right things to choose from, just like a folder full of mails with good subjects.