---
title:   "Going Passive"
date:    2006-09-15
authors:
  - scott wheeler
slug:    going-passive
---
Effective as of the upcoming e.V. meeting, after four years of active membership, I've decided to make my membership passive (for those not familiar with the terminology, that's where you're still technically a member, but aren't on the list and don't have voting rights).

I believe that there is something of an identity crisis for the KDE e.V. at the moment; there are some conflicting notions on what the organization is.

At the heart of this is on the one hand an organization with a set of specific goals, some defined responsibilities and a construction set up to streamline those.  On the other hand is a catch-all private mailing list for long-time contributors.

KDE e.V. has certainly had some notable successes of late; it's managed to procure the KDE trademark, has recieved non-profit status, facilitated the organization of aKademy and several smaller events, budget and organizational communication have improved.

It's wonderful that those thigns are happening; they seem to be what the KDE e.V. is rightfully for, but honestlty, I'm just not that interested in them, though I have nothing but appreciation for the people that have put work into getting them done.

For most mailing lists, there's a simple solution -- unsubscribe, as I've done with kde-promo and kde-policies as my interest has waned, and if something piques my interest at some later date I can just peek into the archives.

But here's where the conflict happens:  KDE e.V., by some estimations, is supposed to be something all long-time KDE contributors should want to be a part of.  Its mandate for expanding its influence into marketing and technical spheres is derrived from this notion.  I've vocally opposed those expansions, but the fact is that they're there now and having a voice in them at a structural level requires sticking around KDE e.V.

The real problem is that the set of long-time contributors isn't equivalent to the set of people who are effective in managing the financial, legal and political responsibilities of the project.  In fact, I believe that they work significantly against each other.  By having such a large, and generally unqualified group constantly debating organization details, those processes are slowed down, and in turn, also takes those people away from the tasks that they are most qualified for within the project.

On the other hand, when technical or promotional things come up on the list, because the list is not focused around a specific group it really invites winding threads about what people think should be done, but don't have any intention of doing.  I'm not pointing fingers as I get sucked into those just like everyone else, but the bikeshedding-to-action ratio is fabulously high.  And because it's a group of people that's passionate about KDE a lot of time is spent in those threads.  I've had a long history in the e.V. of opposing those things falling under the e.V. umbrella, but after several years in the clear minority, I'm not sure it's fruitful to keep up that resistance.

I don't buy chocolate.  I don't buy chocolate because I have no restraint at home, but more at the grocery store.  If I don't want to eat chocolate I don't buy it.  In wanting to get less wrapped up in the present discussions (both emotionally and time-wise), the only practical way for me to do that is to step away from the list.

Two additional notes -- this in not me stepping away from KDE.  I've been more active in the last month than I have been in a while and I hope that the trend continues.  The other note is that I'm posting this in my blog rather than on the list (I'll post a link there.) since it would kind of go against my whole rant to start another big thread and if the debate happens I'd rather see it out in the open anyway.

Finishing up, I naturally wish the best for the e.V.  I have a lot of respect for the couple of folks that have expressed interest in being on the board and have no doubt that the e.V. will continue to do good things.  Depending on how things shape up in the future I may someday reactivate my membership.
<!--break-->