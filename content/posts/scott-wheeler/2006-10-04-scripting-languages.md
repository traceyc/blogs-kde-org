---
title:   "Scripting Languages"
date:    2006-10-04
authors:
  - scott wheeler
slug:    scripting-languages
---
There's a long <a href="http://lists.kde.org/?l=kde-core-devel&m=115945249226185&w=2">thread</a> currently going on on core-devel about scripting within KDE.

Here's the executive summary:

<ul>
<li>Having a "blessed" KDE scripting language for writing complete KDE applications is a good thing and allowing applications written in that language in the main modules would be a step in the right direction</li>
<li>A tangent to the main thread is adding scriptability to KDE applications</li>
<li>For the first sort of scripting, there's something of a concensus that Python or Ruby are the primary candidate languages</li>
<li>There hasn't been much language flaming between Ruby and Python; it seems most folks agree that they're both acceptable OO scripting languages, though there have been plugs a bit for one language or the other</li>
<li>There's some debate over what appropriate languages are for the latter; KJS (JavaScript) is currently advocated, but there's some debate over the merits of JavaScript</li>
</ul>

To qualify the first comment, even if your language of choice isn't the one taken, there's nothing lost.  Currently all scripting languages are second class citizens in the KDE world.  Promoting one to first-class status doesn't demote the others significantly.  An "everybody wins, use what you want" solution really is just a way of rephrasing the current situation.

I've started a <a href="http://blogs.kde.org/node/2425">poll</a> to see if it might turn up a clearly prefered language.  I'm not sure if anything will come of it, but I'd encourage folks to drop their opinion in just to see where it leads.
<!--break-->