---
title:   "JuK facelift"
date:    2011-05-15
authors:
  - scott wheeler
slug:    juk-facelift
---
So, somewhat miraculously, I've been doing a little KDE hacking again this week for the first time since, oh, 2006 or so (aside from TagLib, which recently <a href="https://github.com/taglib">moved to GitHub</a>).

Being as I'm all a <a href="http://www.directededge.com/">startupy</a> web weenie these days, some of the design sins of my youth have been haunting me.  I wanted to give JuK a little bit of a fixer-upper.  Almost all of the 20 commits that <a href="http://cia.vc/stats/author/wheeler">cia.vc</a> says that I've done this week stem from either polishing the interface some or improving the initial experience on startup.

The major bits are:

<ul>
<li>Made the selected playlist thing nice and antialiased and gradiented and shiney.  That had been bugging me for ages.</li>
<li>Ganked the slider and volume control code from Amarok and and popped them into JuK.</li>
<li>Cleaned up the tag editor (still not shown by default, but in the screenies below for the before-and-after).</li>
</ul>

We'll probably be sacrificing a few of the more broken features on the alter of polishedness in the next bit trying to get a nice spiffied up version into KDE 4.7.

Here's the before and after:

<a href="https://blogs.kde.org/files/images/juk-before_1.png"><img src="https://blogs.kde.org/files/images/temp/juk-before_0.preview.png"></a>


<a href="https://blogs.kde.org/files/images/juk-after_0.png"><img src="https://blogs.kde.org/files/images/temp/juk-after.preview.png"></a>

I'd love to someday have time to go all OCD and just spend like a couple weeks fixing things like text alignment and margins all over KDE.  I tend to look at apps like I would web pages at this point, and such things pop right out at me at this point.  Alas, I reckon I'll probably be back to the grind of work here pretty soon, but it's been a fun nostalgic interlude.
<!--break-->
