---
title:   "Novell, Microsoft, Linux Business"
date:    2006-11-06
authors:
  - scott wheeler
slug:    novell-microsoft-linux-business
---
There's been quite a flurry in the blogosphere in the last couple of days over this and it's clear that a lot of people aren't really looking at this from the right angle.

First misconception:

<ul><li><b>Novell / SUSE sold out.</b></li></ul>

SUSE sold out on November 4, 2003.  They agreed to be acquired by Novell, a publically traded company with all that is entailed by such.  Novell does not exist for the benefit of the Linux community.  Like any publically traded company, (hopefully) within moral bounds, they aim to make a profit, to increase their value and provide dividends to their share holders.

So, almost exactly three years into Novell / SUSE, how has Linux business been?  Well, not great.  Not terrible either, but there was much hope that Novell would be able to turn SUSE and or Ximian into a major profit center and thus far that hasn't materialized.  Performance relative to Redhat has been lackluster.

So, back to the point.  Somebody at Novell comes up with the idea to partner with Microsoft on some stuff.  There's probably a bit of cash flowing around this deal.  Novell is in the press.  Linux enthusiasts may not trust Microsoft, but investors certainly do.  This bumps their stock.  Novell also gets some exclusivity in collaboration with Microsoft on virtualization.

Second misconception:

<ul><li><b>Microsoft can strike back with patents in 2012.</b></li></ul>

So, why didn't they last week?  The whole bit about patents was fodder thrown to the crowd and is essentially meaningless.  If anything it was thrown in to hit Redhat, their common competitor, while investor confidence is already down.

The patent clauses were the equivalent of a cold-war non-aggression treaty between Sweden and the USSR.  Sweden naturally had the US looking over its shoulder in the cold-war.  Novell has IBM.  The patent agreement was just formalizing the obvious.  Unless the IT landscape around Linux drastically changes in the next few years, MS would never dare open war with the combined forces of IBM, Novell and maybe friends like Google and Amazon and some of the major hardware vendors.  They've still reserved the right to go after small-time players that don't have powerfull friends.  The only thing Microsoft got out of the deal was basically insurance that in the (unlikely) case of Novell spiraling down in the next few years that it won't take jabs at Microsoft on the way down.

<ul><li><b>So, at the end, what's left?</b></li></ul>

Well, virtualization and Office XML documents.  There, each company gets one win, and I think that was the crux of the agreement.  Novell, for all practical purposes has ensured that it has a semi-exclusive contract for supported virtualization in Microsoft environments.  That's will likely give a little push to Novell's market share.  Microsoft also gets some of the folks from OpenOffice to support their XML format.  That reopens the market for interchangeable formats to Microsoft's own flavor which may keep them from getting shut out of certain markets.  At the very least it gives them some lobbying fire-power when MS Office's formats are set up as one-vendor formats.  Open formats are good for the F/OSS culture in general, but they don't particularly push Novell sales (though they probably hurt OpenOffice adoption on Windows) and depending on whether or not their patches make it into OpenOffice mainline, they may give a brief competitive advantage to Novell's Linux OpenOffice distribution.  On the other hand, they've not lost anything by being supported in Microsoft virtualization environments.

At the end of the day, the deal does make a good deal of financial sense.  That doesn't mean that I like it, but then I don't own any Novell stock.  The big loser in this deal in my opinion is Redhat and the reason that I tend to not like it is because we've got Novell and Microsoft teaming up to kick them while their stock is sinking from the recent Oracle posturing.  But as Linux culture becomes more and more corporate we have to expect, whether or not we like it, that we're things get dirty from time to time and we'll see more, not less of this type of behavior in the future.
<!--break-->