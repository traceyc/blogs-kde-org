---
title:   "QActiveResource"
date:    2010-05-06
authors:
  - scott wheeler
slug:    qactiveresource
---
I thought this might be interesting for some folks in the KDE world -- for <a href="http://www.directededge.com/">work stuff</a> we needed a fast implementation of Ruby's ActiveResource, so I wrote a Qt / C++ ActiveResource consumer. The performance relative to the default Rails backend is somewhat telling:

<img src="http://blog.directededge.com/wp-content/uploads/2010/05/active-resource-benchmark.png" />

It gives a nice QVariant-filled API for working with Rails web services APIs. Full story is <a href="http://blog.directededge.com/2010/05/06/making-activeresource-34x-faster-qactiveresource"/>here</a> and the code up on <a href="http://github.com/directededge/QActiveResource">GitHub</a>.
<!--break-->
