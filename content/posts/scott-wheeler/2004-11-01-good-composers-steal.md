---
title:   "Good composers steal."
date:    2004-11-01
authors:
  - scott wheeler
slug:    good-composers-steal
---
Since I've been doing fairly well at churning out flame-o-rific blog entries lately, I see no reason to deviate from this fine pattern, so here we go again.  I should start with a quote from one of my favorite composers that I think is right on:

<b>&nbsp;&nbsp;<i>"A good composer does not imitate, he steals."</i>
&nbsp;&nbsp;&nbsp;&nbsp;-Igor Stravinsky</b>

Ok, so given that.  Here's another one of those little things that people repeat all the time that makes me want to hog-tie the offenders.

<b>&nbsp;&nbsp;<i>"Eww, that's the GNOME / Windows way!  Don't do that!"</i></b>

Ok, again, let's look at this statement.  So, we're saying that if GNOME or Windows or anyone else does something right that sheer hubris will keep us from copying it  -- the old, familiar "not invented here" syndrome.  Now, there are a lot of things that I think are done wrong in both; and there are a lot of things that are done right.  KDE can learn -- and should learn -- from of these desktops and really anywhere else that we can gain insight.  In fact, the key to putting together a strong desktop is taking the best ideas that we can find elsewhere, combining these with innovation and working towards cohesion of these elements.

I'm not going to get into specifics just now, but there are places where KDE really shines -- and I think we've got one of the best and most integrated desktops around these days.  But we also have to keep our eyes open and no be so reactionary as to assume that just because someone else does something one way that we have to be different just for the sake of being different.

Putting this in context of the quote above:  Stravinsky was a great composer and extremely influential.  It's clear that when he took the best from what he saw in music historically and at the present time he still did this in a way that was distinctively his own style.

And my friends, we can do the same.  So how about this proposal -- when there's something that is mentioned in relation to KDE let's evaluate it objectively, not based on where the idea came from.  Science works this way; art works this way; software should too.

Thank you, that will be all.
<!--break-->