---
title:   "Hotel Wheeler Newsletter"
date:    2005-05-18
authors:
  - scott wheeler
slug:    hotel-wheeler-newsletter
---
Living rooms are overrated.  My next apartment is so going to have a studio / computer / theater room instead.  My living room gets little use other than having hosted about half a dozen KDE folks for various lengths of time.  But my bedroom is getting overcrowded.
<br><br>
<center><a href="http://developer.kde.org/~wheeler/images/room.jpg"><img class="showonplanet" src="http://developer.kde.org/~wheeler/images/room-400.jpg"></a></center>
<br><br>
Ok, so I just built the last bit of the shelving a couple weeks ago and I'm proud of myself.  Sure they're nothing impressive, but building stuff is fun sometimes.  And they fit my desk perfectly.  Unfortunately they still don't hold all of my CDs, so there may be a third level forthcoming.
<br><br>
There are still a guitar, a bass, a djembe (drum) and a closet full of audio equipment missing there.  Right now the routing is a little goofy.  Bass goes into the bass preamp, out through the rack, to the mixer, back to the rack, back to the mixer and then can go out to the computer with the flip of a subgroup switch on the mixer.  The computer comes back to the mixer on another channel.  I've been messing some with electronic music lately and will probably be adding a keyboard and possibly a second rack of equipment (samplers and whatnot).
<br><br>
At some point in the next few months Hotel Wheeler will be relocating -- after three years in Germany it's time for me to move out of what was always meant to be a "temporary" apartment.  At that point I think I want one of those nifty corner desks that goes like 2 meters in both directions with plenty of room for computer and music stuff that's <i>not</i> in my bedroom.  The sound of my computer, which stays running all the time, is starting to bother me some.  Other requirements include a big white wall, ideally opposite the desk, for movies.  I hate TV -- a.k.a. "the stupid box" -- and haven't had one since around 1997, but love film, so I compromised about a year ago and got a cheap projector.
<br><br>
<hr width="80%">
<br>
Ok, so I'll try to salvage this mostly useless post by mentioning that some of my motivation is finally returning and I got several TagLib commits done last night.  I'm going to try to get a 1.4 version out this weekend and got half of its 12 bugs closed last night.  As usual I plan on having all reproducible bugs closed for the release, so if you've got grievances head over to <a href="http://bugs.kde.org/">bugs.kde.org</a> and report them.  (And no, responding to this entry doesn't count.)
<!--break-->