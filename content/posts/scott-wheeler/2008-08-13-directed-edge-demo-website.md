---
title:   "Directed Edge Demo / Website Up."
date:    2008-08-13
authors:
  - scott wheeler
slug:    directed-edge-demo-website
---
I didn't get to be one of the cool-kids at Akademy this year, but it's still a pretty exciting week for me.  I won't drone on about it too much, but since I mentioned here a while back that I'd just founded a new company I thought I'd drop in a link now that we're actually talking about what we're doing.

<a href="http://www.directededge.com/">Directed Edge</a> is doing a graph-based recommender system for web sites that we partner with.  Basically, we take a page or user as a starting point and find related or interesting stuff.  We built a <a href="http://pedia.directededge.com/">prototype</a> based on Wikipedia's content to show what the system can do.  There's still a lot of room to make the system better, but we're pretty excited to get something out there for folks to start messing with.

The announcement, with more details is <a href="http://blog.directededge.com/2008/08/13/directed-edge-launches-recommender-engine-public-beta/">here</a>.

<img src="http://developer.kde.org/~wheeler/images/directededge-kde.jpg" alt="Directed Edge Demo on KDE Page" />
<!--break-->
