---
title:   "Culture Shock"
date:    2005-04-08
authors:
  - scott wheeler
slug:    culture-shock
---
<b>Today</b>

KDE's culture is one of our most important assets.  It's also one of our worst enemies.

KDE's culture has been ideal to bring us to where we are today.  More or less, our goal for years has been to produce <i>a</i> desktop -- something that can actually be called a modern desktop.

And now we've got it.

<b>Identity Crisis</b>

KDE has a strong engineering culture -- I'll go out on a limb and say that we have the strongest engineering culture of any desktop environment.  That's what's made it possible for us to get to where we are technologically.  But that's not enough to sustain the next phase of KDE development.

<b>The Future of KDE Hacking</b>

KDE has the tightest coupling of application frameworks and applications of any desktop in existence.  We can't throw as much money or as many PhD's at a problem as proprietary desktops, but we can push new ideas out in a fraction of the time that they can.

We have a strong technological base.  We will continue to have a strong technological base.  That's what we're good at.  But let's face it -- we're not going to win the world over on the strength of our APIs.

What we need are people being brave enough to ask big questions and to solve them in creative ways.  That's what we as developers can do.  We need to transcend being hackers to being innovative technologists pushing the envelope for what's possible on the desktop.

<b>The Future of KDE Usability</b>

We have a few good people in our usability projects.  We need to draw them into the core of our development groups and treat them as first class contributors to KDE development -- core-developers, if you will.  We need to be able to utilize them for more than pixel-pushing and draw them into the creative process.  We need to end the split between "we're developers, they're usability people" and say, "the desktop is our project and we're working on it together -- let's make the best use of each other's expertise".

By recognizing their importance to our project and interacting with them as co-conspirators, and building on the things that we already do very well, good things will happen.

But this is also a charge to our usability folks -- engage our development groups; be a part of the process.  We need you.

<b>The Future of KDE Artwork</b>

In the same way that we need hackers that are asking big questions and usability people that are doing more than pixel-pushing, we need KDE artists and graphic designers that are more than "icon people".

We need to be asking these people, "How can we do better visual encoding of this interface."  "How can we not just make this work, not just make this usable, but also make it cool."  And not just cool -- we have some great artists in our project; these are the people that if prodded enough may be able to lead the way to better ways of visualizing desktop components -- of innovating.

They too need to be added to our "core".

<b>And Then We Take Over the World</b>

If we make it over this hump -- if we can integrate innovative hackers, innovative usability people and innovative visual designers  (and the groups that I've missed here) -- all groups that we already have in KDE -- into a single team; a single mesh, <i>that</i> is what will drive the future of the KDE desktop.

There will always be a need for nuts and bolts -- we still need to do the boring stuff and keep that in line, but that's what we know how to do; our momentum will take us there.  But where we need a <i>culture shock</i> is pulling the groups together to do the nuts and bolts and the innovation.  That's what will push us beyond having <i>a</i> desktop (a fine one, mind you) to having <i>the</i> desktop -- the one that everyone else looks to because that's where the cool shit is happening.

We've got some great people and through technical leadership we'll draw in even more.  We've set goals and reached them and now it's time to ask "What next?"
<!--break-->