---
title:   "Choices & Configurability, In Easy to Understand Parables"
date:    2004-10-26
authors:
  - scott wheeler
slug:    choices-configurability-easy-understand-parables
---
Since I seem to have sparked a bit of a debate on <a href="http://www.planetkde.org">Planet KDE</a>, let's see if I can bring a little clarity to things.

Let's play a little game.  Let's call it "the abstraction game".  In this game we'll have words and thoughts and these words and thoughts need not be representative of what actually exists in the real world.

<b>The Oh-So-Tough-To-Master-With-Zen-Like-Transcendence <i>Abstraction Game</i></b>

Now in our little world let's say that we've got a computer, say, called <a href="http://en.wikipedia.org/wiki/Minor_characters_from_The_Hitchhiker's_Guide_to_the_Galaxy#Deep_Thought">Deep Thought</a>.  Deep Thought understands life, the universe and everything, but today it's decided that it's going to serve up some KDE application.  Now, since Deep Thought knows, well, everything we can assume that it knows exactly the right interface for us at every point in time and that's what it presents to you.

So, here's the critical question in our little thought experiment, should Deep Thought serve up this application with any choices? Keeping in mind of course that all that these options will do is make it possible for you to choose things that aren't what you want, because Deep Thought already knows what you want and has shown that to you.  And since Deep Thought is a pretty hefty computer, there are a lot of possible choices.

Simply put -- no.  It shouldn't.  Because the only possible choices it could have would only be able to confuse you and give you something you don't want.

So, for those of you that just fell off the wrong side of your ethereal bongs, here's the moral of the story:  <i>choice isn't good by itself.  If choices don't lead to something meaningful, they're just noise.</i>  Now there's a whole lot of things that can bring meaning to choice, and many of them make it worthwhile.  But it's silly to say that choice by itself -- independent of what you're able to chose is worth anything.

<b>Bob's Everything Store</b>

Now, kids, let's try another parable.

Let's say we have a bike.  And let's say that it has a broken rear wheel.  We go to Bob's Everything Store and Bob comes up and says, "Oh, yes -- wonderful -- we have lots of choices for you."  He proceeds to show you a wheelbarrow wheels, car wheels, lawnmower wheels, broken bike wheels and a wonderful assortment of basket weaving kits and self help videos.  And you say, "Well, really I'm here to fix my bike.  Don't you have any bike wheels?"  He sighs and then shows you three -- one will last a really long time, but's pretty expensive, one that's better for city riding and one that's better for off road.  His lovely assistant, Jane, who also happens to be a biking guru struts onto the scene and blurts out, "Uhm, dude -- the only one of those that will work on your bike is the last one."

Now, in the first case we certainly had a lot more choice, in the second we had a little bit and it was more focused and in the end we had none at all.  But which was the most useful?

So, what does this have to do with software design?  Well, the first thing is choice doesn't get us anywhere by itself.  In fact, <i>if we don't use some discretion they're just going to confuse us and waste our time</i>.

<b>Things That Suck and Things That Suck Less</b>

Now, we as software developers are often in all three of the above situations.  And often all at once.  There's a lot of software where we're clearly trying to sell people proverbial self help videos where what they're looking for is a bike tire.  <i>Discretion here is important and sometimes things are pretty clearly irrelevant.</i>

There are also times like the second category.  We don't know what's best so we have to give people some options.  There are different bikes and they need different wheels.  <i>There are different users and sometimes they need different levels of interfaces.</i>  But here we do have to have to be careful that we don't fall into the first category because, hey, I mean, choice is good, while we're at it let's see if we can unload a few copies of Sweatin' to the Oldies.

In the third category there are a couple of things for us to learn.  The primary message is that <i>sometimes the experts know what they're talking about</i>, but there are a couple kinds of experts that are relevant here.  The first is usability people.  They're not all cranks (though those are to be found).  But there are times where even they, enlightened as they may be, simply can't say "this works for all users", but in the times when they can tell us with unanimous certainty that something just plain sucks, we should listen.  But we've also encountered another type of expert in our wanderings here -- Deep Thought.

You see, we throw away a lot of information about how users interact with their computers.  We've got a lot of horsepower in that noisy little cube we call a PC and sometimes we can learn from the users themselves and try to feed that back into the interfaces that we present to them.  I've been doing some scheming on this theme and I think I can say that as computing moves forward we're going to see a lot more of our expert-in-a-box when it comes to UIs.

Now, there has been one party that I've left out of this discussion.  They're the ones that do want lawnmower wheels and helicopter blades and hats of the finest tin foil $14 can get you, because hey, they're building a flying bike.  And well, that's fine.  Say "hi" to Raster for me.  Doofuses.  ;-)
<!--break-->

