---
title:   "TagLib 1.6.2 Released"
date:    2010-04-09
authors:
  - scott wheeler
slug:    taglib-162-released
---
Lukáš, who's taken over TagLib maintainership these days, has just released the latest bug fix release for TagLib, also posted in <a href="http://oxygene.sk/lukas/2010/04/taglib-1-6-2/">his blog</a>:

Changes from 1.6.1 are:

  <ul>
    <li>Read Vorbis Comments from the first FLAC metadata block, if there are
     multipe ones.</li>
    <li>Fixed a memory leak in FileRef's OGA format detection.</li>
    <li>Fixed compilation with the Sun Studio compiler.</li>
    <li>Handle WM/TrackNumber attributes with DWORD content in WMA files.</li>
    <li>More strict check if something is a valid MP4 file.</li>
    <li>Correctly save MP4 int-pair atoms with flags set to 0.</li>
    <li>Fixed compilation of the test runner on Windows.</li>
    <li>Store ASF attributes larger than 64k in the metadata library object.</li>
    <li>Ignore trailing non-data atoms when parsing MP4 covr atoms.</li>
    <li>Don't upgrade ID3v2.2 frame TDA to TDRC.</li>
  </ul>

<hr />

As a side-note, some of my buddies from <a href="http://rethinkdb.com/">RethinkDB</a> have been looking hard for good C++ folks out in Silicon Valley doing a MySQL backend for solid state devices.  If you're a systems-y C++ wonk in search of a <a href="http://rethinkdb.com/jobs/">job</a> at a hacker-friendly company, they're good folks.  If you're interested and we know each other drop <a href="mailto:wheeler@kde.org">me</a> a line and I'll do a little intro show-and-dance.
<!--break-->
