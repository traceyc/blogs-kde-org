---
title:   "Insomnia."
date:    2006-04-24
authors:
  - scott wheeler
slug:    insomnia
---
Insomnia has to be one of the worst things in the universe.  It seems that I've been due for my annual fit of such.  Life has been rather stressful lately and while I usually deal well with stress, my body seems to rebel after a certain threshold.  I've slept about 3 hours total in the last two nights.

The worst thing about insomnia is that you can't actually get anything done during the time while you're awake.  I mean, normally I'd love to only need to sleep a few hours a week.  But with insomnia you're just kind of zombified and for me that usually means just laying around in bed wishing I was asleep.  Case in point -- this weekend -- no code, reading, music...not even movies, despite having stuff that I wanted to work on.  Bleh.
<!--break-->