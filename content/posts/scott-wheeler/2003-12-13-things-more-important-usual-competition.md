---
title:   "Things more important the usual competition..."
date:    2003-12-13
authors:
  - scott wheeler
slug:    things-more-important-usual-competition
---
Haven't said much in a while -- mostly because things have been really busy for me lately.  But today something really got my attention and merited some thought.  In the last couple of days <a href="http://www.gnomedesktop.org/article.php?sid=1523&mode=thread&order=0">Ettore Perazzoli</a> a GNOME / Evolution / Ximian hacker type of guy died; it was one of those moments for me where reading about it all of the desktop flame wars seemed to pass into irrelevance.

It sickened me to pull up Slashdot and see it really as a footnote to Miguel's late board submission or something like that and then even worse to see the cracks about it in the comments...  I guess if there's some point to what I'm rambling about it's that despite all the competition and all that (which I'm active in and think is healthy) at a certain point there's kind of some basic Open Source geekiness that's common and it's quite sad to see an important contributer go.

Anyway -- I'll shut up, but this had been on my mind today.  Best wishes to the guys around him both personally and in the GNOME project.  Stuff like this always sucks.