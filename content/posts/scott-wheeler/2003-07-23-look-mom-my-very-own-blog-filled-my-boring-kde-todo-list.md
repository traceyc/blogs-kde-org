---
title:   "Look mom, my very own blog -- filled with my boring KDE TODO list!"
date:    2003-07-23
authors:
  - scott wheeler
slug:    look-mom-my-very-own-blog-filled-my-boring-kde-todo-list
---
Well, I'm looking for the "stuff that nobody cares about" category and not finding it, so "development" shall be the lucky winner in this contest.

So, stuff that I'm up to lately:

Well, first, I'm playing tour guide again with my 4th visitor of the summer.  This cuts into coding time pretty significantly, but I'm planning on hiding in my bedroom for the next few weeks and gnawing away at my TODO list.

There's a huge amount of stuff that needs to be done for JuK, but for the moment I'm trying to clear away the outstanding bug reports and easy / reasonable wishlist requests.  I keep swearing that I'm going to redo the GUI before 3.2, but such hasn't happened yet.  I've been mostly playing a more traditional "maintainer" role since JuK went into KDE CVS reviewing (or too often forgetting) patches, cleaning up aging code, keeping an eye on the architecture and fixing bugs.  Most of the coolest stuff has come from other developers.

I'm slowly hacking away on TagLib, my "One Audio-Meta-Data Lib to Rule Them All".  I had fun showing it off to Carsten (one of the KFileMetaInfo authors) this last weekend while visiting Berlin at a Berliner KDE-geek-fest.  I've talked to some of the Vorbis guys so that I can clean up my implementation a bit and then I'll be off to fill in the other gaps in the API.  Die id3lib; die!

Need to fix some of the spelling highlighting bugs in kdelibs that I seem to have inherited; need to fix my QListView patch to make KListView painting not suck so much; need to finish a signature pluging thingie for KMail; All three of the FlashKard users have feature requests.  :-)

So basically this is my TODO list of things that I'm not brave enough to put in the feature plan...  I'm sure I'm missing things, but hey, I've never been one for organization.