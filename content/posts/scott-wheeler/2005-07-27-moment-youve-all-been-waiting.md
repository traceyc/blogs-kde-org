---
title:   "The Moment You've All Been Waiting For"
date:    2005-07-27
authors:
  - scott wheeler
slug:    moment-youve-all-been-waiting
---
Well, at least the moment Mark has been waiting for.  And Ian.  TagLib 1.4 is <a href="http://developer.kde.org/~wheeler/taglib/">out</a>.  Try to refrain from foaming at the mouth.  I know there's little more exciting than a meta-data library to really get your blood pumping on a slow Wednesday afternoon.

(Actually I did the tarball last night, but just got around to writing the announcement regalia today.)
<!--break-->