---
title:   "Everything's Bigger in Texas"
date:    2004-04-13
authors:
  - scott wheeler
slug:    everythings-bigger-texas
---
Apparently including the number of bugfixes.  :-)  Matt and I -- being the two resident (and fairly active) Texans in the KDE project had a bit of fun going back and forth on the top spot for bug fixes this last weekend.  He's currently ahead by two, but more of mine were from actual commits, so I think those should count double.
<br><br>
<p>Since this blog will last longer than, well, this week, just pasting in the present top 5:</p>

<ul>
 <li>30 matt.rogers@kdemail.net</li>
 <li>28 wheeler@kde.org</li>
 <li>12 binner@kde.org</li>
 <li>11 l.lunak@kde.org</li>
 <li>10 amantia@kde.org</li>
</ul>

So, yeah, for all of the chiding that I get for being a Texan ex-pat semi-permanently relocated to Germany, it's fun to have a laugh at stereotypes every once in a while.  :-)
<br><br>
And well, <b>I reckon' this's a big-ole-texas-ass-whoopin' in the bug fixin' showdown.</b>  (Ok, I'm going to hate myself for saying that in about 5 minutes, but it seems rather funny at the moment.)
<br><br>
Anyway -- it was a really nice long, lazy weekend.  4 days of almost solid coding, reading and playing bass (and sleep!).  It's been quite a while since I've been able to just relax that much.  I got a lot of code written, refactored and whatnot in JuK and TagLib and started work (in CVS) on a new build monitor <a href="http://ktown.kde.org/~wheeler/images/icemon.png">thinger</a> for icecream.
<br><br>
Also got a new bass last week.  I'm sure pictures of it will crop up at some point.  It had been a while since there was a new member added to the family.  :-)  This one (4th bass) is a fretless 4-string from <a href="http://www.sandberg-guitars.com/">Sandberg</a>, a relatively small German luthier.  Sounds and plays great.  I'm surprised I got as much coding done this last weekend because it kept distracting me.