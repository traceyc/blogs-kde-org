---
title:   "Brace yourself; I've gotten a little too close to doing graphics work for public safety."
date:    2004-05-03
authors:
  - scott wheeler
slug:    brace-yourself-ive-gotten-little-too-close-doing-graphics-work-public-safety
---
So the JuK GUI hasn't exactly been stunning historically.  There have been a few times that there have been plans to make it nicer, but they've never amounted to much.  Well, I finally decided to try my own hand at it and had a little fun with <a href="http://www.sodipodi.com/">Sodipodi</a>.  I think it's a pretty big improvement and if all goes well I'll have it checked in soon.  [image:454]

None of the buttons actually worked yet, but I'll get going on that soon.  :-)  I also played around with a <a href="http://ktown.kde.org/~wheeler/images/juk-icon.png">new icon</a>, but I think I still need a few iterations of trying things before I arrive at one that I like there.

I also spent this last weekend mostly at the Linux Audio Conference in Karlsruhe.  There were some interesting talks and there's some interesting stuff in the works.  However I at one point felt that it should renamed "The GUI Hall of Shame and Linux Audio Conference".  Wow.  Yikes.  Some of the stuff was just really bad.  And coming home excited to try out some of the mentioned stuff a lot of it isn't really stable enough to be usable yet -- maybe someday.

Let's see -- also working on ripping out and replacing bits of the KConfig internals.  That was looking promising until my short affair with SVG stuff this weekend.

Also I really need to get around to reviewing / integrating some patches.  I always feel bad when I let them build up, but they're time consuming to review and normally not much fun.

