---
title:   "As an American ex-pat..."
date:    2004-11-03
authors:
  - scott wheeler
slug:    american-ex-pat
---
...today was a bit saddening; failing all of the hooplah of the last election, which doesn't seem likely at this point, it seems that we're looking at 4 more years of Bush and likely 4 more years of apologizing for my country.  Time to get an hour or so of rest and then drag myself into work.

On the plus side I got most of the GStreamer 0.8 output thinger that I wrote a while back integrated into JuK; I still have to get seeking / time reporting done and I'll hopefully commit it this week.
<!--break-->