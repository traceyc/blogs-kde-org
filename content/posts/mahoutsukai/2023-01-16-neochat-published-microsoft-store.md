---
title:   "NeoChat Published in the Microsoft Store"
date:    2023-01-16
authors:
  - mahoutsukai
slug:    neochat-published-microsoft-store
categories:
  - CI/CD
  - Windows
---
Carl Schwan already announced it on <a href="https://discuss.kde.org/t/neochat-is-now-available-on-the-windows-store/107">discuss.kde.org</a> and at <a href="https://fosstodon.org/@neochat/109624702727489819">@neochat@fosstodon.org</a>.

In this blog we’ll see how it was done and how you can publish your KDE app in the <a href="https://apps.microsoft.com/store/search?publisher=KDE%20e.V.">Microsoft Store</a>.

<b>Reserving a Name and Age Rating Your App</b>

The first step requires some manual work. In <a href="https://partner.microsoft.com/en-us/dashboard/windows/overview">Microsoft Partner Center</a> you need to create a new app by reserving a name and complete a first submission. How to do this has been described by Christoph Cullmann in the <a href="https://kate-editor.org/post/2019/2019-11-03-windows-store-submission-guide/">Windows Store Submission Guide</a>. Don’t hesitate to reserve the name of your app even if you are not yet ready for the first submission to the Microsoft Store. Once a name is reserved nobody else can publish an app with this name.

The first submission needs to be done manually because you will have to answer the age ratings questionnaire. NeoChat was rated 18+ because it allows you to publish all kinds of offensive content on public Matrix rooms. Filling out the questionnaire was quite amusing because I did it together with the NeoChat crowd at <a href="https://webchat.kde.org/#/room/#neochat:kde.org">#neochat:kde.org</a>.

On the first submission of NeoChat I chose to restrict the visibility to <i>Private audience</i> until it was ready for public consumption. I created a new customer group <i>NeoChat Beta Testers</i> with the email address of my regular Microsoft Store account in <a href="https://partner.microsoft.com/en-us/dashboard/analytics/customers">Microsoft Partner Center</a> and then selected this group under <i>Private audience</i>. This way I could test installing NeoChat with the Microsoft Store app before anybody else could see it.

Don’t spend too much time filling out things like Description, Screenshots, etc. under Store Listings because some of this information will be added automatically from the AppStream data of your app for all available translations.

<b>Semi-automatic App Submissions</b>

The next submissions of NeoChat were done semi-automatically via the Microsoft Submission API with the <a href="https://invent.kde.org/sysadmin/ci-utilities/-/tree/master/microsoft-store">submit-to-microsoft-store.py</a> script while writing this Python script and the underlying general Microsoft Store API Python module <a href="https://invent.kde.org/kloecker/microstore">microstore</a>. The script is based on a Ruby prototype (<a href="https://invent.kde.org/sdk/releaseme/-/blob/work/windowsstore/windows.rb">windows.rb</a>) written by Harald Sitter.

The idea is that the script is run by a (manual) CI job that the app’s release manager can trigger if they want to publish a new version on the Microsoft Store.

To run the script locally you need the credentials for an Azure AD application associated with KDE’s Partner Center account. Anything else you need to know is documented in the script’s <a href="https://invent.kde.org/sysadmin/ci-utilities/-/blob/work/kloecker/submit-to-ms-store/microsoft-store/README.md">README.md</a>.

<b>Making NeoChat Publically Available</b>

The last step of the process to get NeoChat published in the Microsoft Store was another manual submission which just changed the visibility to <i>Private audience</i>. This could also have been done via the Microsoft Submission API (but not with the current version of the script), but I think it’s good to have a last look at the information about the app before it is published. In particular, you may have to fill out the <i>Notes for certification</i>, e.g. if your app cannot be tested without a service or social media account. For NeoChat we had to provide a test account for Matrix.

Moreover, you may want to fill out some details that are currently not available in the AppStream data, e.g. a list of <i>Product features</i>, the <i>Copyright and trademark info</i>, or special screenshots of the Windows version of your app.

<b>What’s Next</b>

On <a href="https://invent.kde.org">our GitLab instance</a>, we want to provide a full CI/CD pipeline for building and publishing our KDE apps on the Microsoft Store (and many other app stores). A few important things that require special credentials or signing certificates are still missing to complete this pipeline.

And we want to get more KDE apps into the Microsoft Store.

If you need help with getting your KDE app into the Microsoft Store, then come find me in the <a href="https://webchat.kde.org/#/room/#kde-windows:kde.org">#kde-windows room</a>.

<hr>

Updates after publication:
<ul>
<li>2023-01-31: Updated link to script after merge of the MR</li>
</ul>