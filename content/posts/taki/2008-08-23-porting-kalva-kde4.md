---
title:   "Porting Kalva to Kde4"
date:    2008-08-23
authors:
  - taki
slug:    porting-kalva-kde4
---
I could no longer resist, in spite my notorious lack of spare time... It began with installing SUSE 11 via KDE4 Live CD on my eeePC. On my desktop PC I had been trying KDE4 since the RC's where released with a special user and was quite happy I could come back to 3.5 for daily use. I was really astonished how very well even 4.0.4 was looking and behaving on the eee 701.
Since the release of kde4.1 RC1 I switched my default also for the desktop mashine from 3.5.9 to 4.1 and am very fond of it. All that did commit to KDE4 did and still do a great job.

On linuxtag in Berlin I had some interesting talks and now the seeds begin to ripe. A two weeks holyday gave me the chance to start porting kalva.

Kalva2 will not be a simple port. It gets totally refactored. I am really astonished about the possibilities that KDE4 and QT4 are granting and somehow feel the duty to make kalva2 much cleaner.

Kalva2 has recieved a plugin system to make it more modular. The main view uses perspections that are constructed by  plugins. Perspective plugins also expand the config dialog of Kalva2 and can be enabled and disabled. With perspective plugins it is quite easy to expand kalva2.

The bad news for anyone hoping for a kde4 version of kalva replacing Kalva 0.98: As of now Kalva2 is not a recording app anymore and that state will last some time. By the help of phonon it mutated to a video player. The first and at the moment only usefull perspective is the collection perspective. It consists of an integrated filemanager (like in kalva <1) and of a video player. Later on I plan to integrate an indexing widget for nepumuk integration. Imagine You tag Your video files and use virtual Folders with nepomuk search ioslaves to order Your collection.

When recording comes back to Kalva2 I want to build that with solid and phonon. Phonon already provides recording functionality. Solid knows about dvb. But I am not sure that this already is enough to build a tv player and a recorder around... Sorry that recording is rather late in my schedule.

Kalva2 at the moment is my favorite video player for the eeePC. One reason of course is that everyone should love his / her own pet most. But also it is because I do not allow the gui to be bigger then the screen. I am not really proud about the hack to achieve this: The maximum size hint for the main view is hardcoded to 800x404 which fits perfect on the 701's display. If I find out how to get the size of the display in a system independent way that has to be changed to set maximum dynamically.

The config dialog is not storing any preferences and does not load them. I will do a first pre pre alpha release when I fixed the config dialog.

The sources are in trunk/playground/multimedia/kalva2.

P.S.: I forgot to say: Phonon rocks, Qt 4.4 rocks, CMake rocks and KDE 4.1 rocks anyway :-D

P.P.S: So my very firt blog on the planet was much longer than I had intended...
