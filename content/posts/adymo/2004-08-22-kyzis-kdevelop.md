---
title:   "Kyzis + KDevelop"
date:    2004-08-22
authors:
  - adymo
slug:    kyzis-kdevelop
---
Today I've finished KTextEditor::CodeCompletion and PopupMenu interfaces for Kyzis text editor (those who don't know about the editor should visit <a href="http://www.yzis.org">http://www.yzis.org</a>). Now the editor works like a charm with KDevelop. Vi(m) fans should check for a first IDE with vi-like text editor ;) Editor is still in development but it's in a good shape now, everyone interested is welcome to try it out and help the development of an editor itself and a kdevelop integration process ;)<!--break-->