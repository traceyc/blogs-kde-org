---
title:   "Spoiler pt_BR Code Warning :-)  !!! E teremos um contest Rot13 no FISL"
date:    2007-04-05
authors:
  - heliocastro
slug:    spoiler-ptbr-code-warning-e-teremos-um-contest-rot13-no-fisl
---
Nada como um "desafio" de uma arena de programação. Esconderam um código na página e com dicas do futuro lá também :-) 

O "código" escondido é um pequeno javascript do infame Rot13 "criptografado" em Rot13.
Espero que o grau de dificuldade posterior vá bem além disso, porque um bom programador ia acabar se sentindo ofendido de entrar num trial com esse tipo de questionamento. Basta dar uma pequena olhada nos trials do Google que se entende o que é uma dificuldade real.

Para colaborar com o que vem por aí, aqui estão as próximas dicas que ele colocaram na própria página:

Dica 3 (6/4/2007): "As salas do fisl importam."
Dica 4 (7/4/2007): "O CMS usado no site do fisl importa."
Dica 5 (8/4/2007): "Se você já sabe o que faz o código embutido e porque as salas do fisl importam então você já sabe o caminho."

Bom, eu disse o que o código faz aí em cima e postei ele embaixo :-)
As dicas do "futuro" estão aí ( acho que vão mudar quando lerem que já está postado )
Divirtam-se :-)

<code>

// This work is hereby released into the Public Domain. To view a copy of the public domain dedication, visit http://creativecommons.org/licenses/publicdomain/ or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.
// origin: 2000-01-08 nospam@geht.net http://tools.geht.net/rot13.html
// Use at own risk.
var last=\"\";
var rot13map;
// The problem is that JavaScript 1.0
// does not provide a Char to Numeric value conversion
// Thus we define a map.
// Because there are 64K UniCode characters, this map does not cover all characters.
function rot13init()
{
  var map = new Array();
  var s   = \"abcdefghijklmnopqrstuvwxyz\";
  for (i=0; i=\'A\' b<=\'Z\' || b>=\'a\' b<=\'z\' ? rot13map[b] : b);
    }
  return s;
}
function upd()
{
  if (document.forms[0].text.value==last)
    return;
  last = document.forms[0].text.value;
  document.forms[0].rot13.value = rot13(last);
}

</code>