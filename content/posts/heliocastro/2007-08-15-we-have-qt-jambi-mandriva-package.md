---
title:   "An we have Qt Jambi Mandriva package !!"
date:    2007-08-15
authors:
  - heliocastro
slug:    we-have-qt-jambi-mandriva-package
---
   I started Jambi package to Mandriva at Akademy this year, but process was complex since we're dealing with buildsystem changes, Java new packages, Qt4 layout changing, so lots of barriers to get the task done.
My personal effort on this is have next Mandriva releases be ready for developers in all cases, to use best available Qt and KDE technologies as possible, improving our situation from the past.
   Then, yesterday i had all things in really good shape, and finally a good initial package could be produced.
I tested many environments. gcj/ecj was having some compile issues, but java 1.6 sun and the best, java 1.7 icedtea worked as a breeze.
Linking against system Qt 4.3.1 was not a problem at all. Great that both i586 and x86_64 arches are compiled fine as well ( thanks for David Walluck tips ).
   So the major trouble in my special case is the install process, as Jambi not provide a proper install script, and even a proper layout to install.
I decided make launcher and designer-jambi scripts to lauch on doc and devel package respectively, and separate Jambi jar and libraries in different packages, and most of files fall down on same qt4 dir on Mandriva.
We fall down with this result:

<li> <b>qtjambi-4.3.1-1mdv2008.0</b> - Main jar files, requires libraries
<li> <b>lib{64}qtjambi1-4.3.1-1mdv2008.0</b> - Main libraries, autoreq qt4 libs
<li> <b>qtjambi-doc</b> - examples, launcher files ( will be moved ), templates
<li> <b>qtjambi-devel-4.3.1-1mdv2008.0</b> - Specific devel bins likr juic e generator, designer-jambi wrapper for main qt4 designer with proper plugins
<li> <b>qtjambi-launcher-4.3.1-1mdv2008.0</b> - The launcher wrapper

Soon Mandriva 2008.0 users will have this great Qt Java in their systems ready to develop. Cooker users just need mirror be synced in a few hours.

Now, aiming PyQt4..
<!--break-->