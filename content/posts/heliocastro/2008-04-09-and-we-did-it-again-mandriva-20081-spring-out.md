---
title:   "And we did it again - Mandriva 2008.1 Spring is out..."
date:    2008-04-09
authors:
  - heliocastro
slug:    and-we-did-it-again-mandriva-20081-spring-out
---
Well, was a huge journey for usr this time. 2008.1 was not only an updated 2008.0, but since early design, was clearly that we would try do a big step on our development. Just to mention what arrived since 2008.0, we're integrated Manbo labs work, which means now we're have a real common core system  between us and Turbo Linux, we're integrated PulseAudio on whole distro, which was a bold move and even not been so perfect yet, for me proves at all that was the right decision, and of course, we did most of possible fixes and insanities on KDE 3 !!
I can tell, we sacrificed polish a kde 4 environment, today in contrib, due our heavy tasks to make KDE 3 as best as possible. I can't measure the stress of hunt and do changes like we want, and some couldn't be solved to last moments, but i can say was one of the best KDE deployments the Mandriva KDE team did ever. From the screensaver to the kicker changes to menu changes, all small patches and fixes almost impossible to be noted, but which took hours from us to find, or even touch in monster codes like kicker one, and integrate new features.
As i promised myself, the day we gone gold, would be last day i would see KDE 3 in my personal work machine, so now i'm using in production KDE 4.0.68 over 2008.1 base and i guess you know why ...
So congrats to everyone that did a wonderful job on Mandriva Spring and we finally break the curse :-D
<!--break-->