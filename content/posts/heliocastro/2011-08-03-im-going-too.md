---
title:   "I'm going too !!"
date:    2011-08-03
authors:
  - heliocastro
slug:    im-going-too
---
Another year, my first Desktop Summit, plenty of things to do, gstreamer training, help organization, even be a referee in a geek football match.
Crazy days ahead.
Leaving sunny Florianópolis right now !!
<img src="https://www.desktopsummit.org/sites/www.desktopsummit.org/files/DS2011banner.png"></img>