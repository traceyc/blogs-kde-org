---
title:   "Lost luggage :-("
date:    2007-07-13
authors:
  - heliocastro
slug:    lost-luggage
---
BIG RANT MODE
Seens like air companies are really lost on this days.
My luggage still not arrived, and neither companies, TAM and British Airways seens able to find what's happened.

Would not be so important if you consider that is just money, and i am back to home already safe, but inside the bag was the wonderfull computers shown in Akademy for everyone, like the Classmate, and mainly the MID with all Mandriva work and KDE 4 work. This work is unvaluable, can't put a price on the efforts of some guys :-(

As TAM company said, seens like British Airways decide for some reason tag the luggage to Paris, instead of final destination. Of course this after i asked three or four times on the checkin that my luggage WILL arrive at Brazil directly.
Then of course, after filling all documents as usual, i'm still waiting three days for the luggage and all that both companies can tell me is that my luggage was dispatched in a flight to CDG to Rio de Janeiro, but nobody can know where it is.

I was silly in trust put this mini computers inside, but the crazy moment on checkin in Glasgow, and been my backpack heavy to bring inside the plane made me opt to bring my notebook on backpack, instead of other devices.
And i even bought a new suitcase, to make easy have one only bag to bring to airport. Sigh!!

Now what i can do is just wait from both TAM and British Airways airlines find some solution, because probably the meeting to show MID with Mandriva and KDE work are pratically ruined. And i'm feeling personally responsible for that...
/BIG RANT MODE
<!--break-->