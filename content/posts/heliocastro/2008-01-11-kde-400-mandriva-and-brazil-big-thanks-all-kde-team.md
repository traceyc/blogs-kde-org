---
title:   "KDE 4.0.0 from Mandriva and Brazil - A big thanks to all KDE team"
date:    2008-01-11
authors:
  - heliocastro
slug:    kde-400-mandriva-and-brazil-big-thanks-all-kde-team
---
<center><img src="https://blogs.kde.org/files/images//bannertuga.png" class="showonplanet" /></center>

First of all, on behalf of all Mandriva KDE team ( me, Gustavo Boiko, Danilo César and our main contributor Nicolas Lecureuil ), we want to to say a big thanks for the enourmous efforts and the present result named KDE 4.0.0 !
Been for some time in the last years actively involved in KDE development in many ways considering distributions and even the amount of rants and talks on conferences, i'm proud to be the small part of this huge project that prove in many ways that we can deploy desktop on opensource with quality.
If users are looking for Mandriva packages, we have it at http://download.kde.org/download.php?url=stable/4.0.0/Mandriva/

Now, on behalf of Brazilian KDE team, i can say we did it !! Let's get out tonight and celebrate because is all deserved, mainly the translation team who did a fantastic last hour work !
Thanks to Nuno Pinheiro from Oxygen that provided the nice portuguese image.

So happy KDE 4.0.0 day for everyone  !!

<!--break-->

