---
title:   "Mandriva 2008 Spring KDE 4.1 RC1 packages available"
date:    2008-07-16
authors:
  - heliocastro
slug:    mandriva-2008-spring-kde-41-rc1-packages-available
---
With some delay due the heavy workload at Mandriva offices, i manage to provide some experimental packages for Mandriva 2008 Spring.
A standard urpmi repository is available.
Please refer to <a href="ftp://ftp.kde.org/pub/kde/unstable/4.0.98/mandriva/README">README</a> for detailed information. 
<br>And of course:

<p style="text-align:center"><img src="http://commit-digest.org/issues/2008-06-08/files/akademy2008.png" alt="I'm going to Akademy 2008" style="width:320px;height:178px"/></p>