---
title:   "Cleaning the past..."
date:    2004-08-15
authors:
  - heliocastro
slug:    cleaning-past
---
Vacations lead you to do strange things...
I waked up this morning and strangely aim my eyes to the huge closet barely opened in last three years at my apartment..
So, i decided that it's time to throw out almost everything in there, since i really didn't need this stuff.
And in this times you see things that did your past and probably meant a lot for you. From letter of ex-girlfriends to friends cards. From a full collection of Wired magazines ( in their good times ) to a Dr. Dobbs and C++ Journal editions ( you need been a brazilan to know how expensive and hard was to buy such magazines, life used to be a lot hard here in those times ). From a X-Men collection to a university notes and old computer books.
It's misterious how mind's work, bringing missing feelings, and histories with this inanimated objects, and causing some affection to then, trying to keep you away from the "throw away" idea.
But i decided that's time to move on on this things.
Among things, i decided give my fully working old Super Nintendo to the son of one of securty persons of my building, give another Board game to another son of other security guy and the full x-men collection to the son of the third security guy ( they are four at all ). 
I made this to assure that this things will give same fun i had on the past for new children.
From magazines, i decided keep three Wired editions that marked me a lot, specially the one showing the dawn of Atari and how this technicias become half of entire Silicon Valley companies.
From ex-girlfriends things ? Garbage. Good and bad memories, but must go. I keep special pictures.
From Comics, just the Graphic Novels that i re-read from time to time, mainly the Sandman ones.
And the university notes, i kept just the conclusion work. I really wanted save old work, but it's interesting how's Internet provides everything that i use to have in paper. Not that is a good thng, is dangerous. We are becoming too dependant. But even with this, i decided throw away everything.
As everything have the good and the bad side, the bad side was throw my notes where i can saw that truly grow up and did a lot of mistakes, but the good is that i'm opening space for growing more ( and make new mistakes )... 
Anyway, life's going on