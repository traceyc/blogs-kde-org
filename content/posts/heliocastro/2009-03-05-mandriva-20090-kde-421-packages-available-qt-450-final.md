---
title:   "Mandriva 2009.0 KDE 4.2.1 packages available - With Qt 4.5.0 final !!!"
date:    2009-03-05
authors:
  - heliocastro
slug:    mandriva-20090-kde-421-packages-available-qt-450-final
---
Arrived the time of year of doing another test upgrade for Mandriva users.
This time is a little bit special, since you will be using Qt 4.5.0 final, with all our efforts to make it stable with  backports, annoying some KDE devels ( thanks dfaure and Thiago ), etc.. 
If you want to know who are the guilt ones for this release, go and see nice about tab in "KDE about" after installed :-)
Standard urpmi repositories are available.
<a href="ftp://ftp.kde.org/pub/kde/stable/4.2.1/Mandriva/2009.0/README">README</a> for some detailed information.

