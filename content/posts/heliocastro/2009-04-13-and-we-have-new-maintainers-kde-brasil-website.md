---
title:   "And we have new maintainers for KDE Brasil website"
date:    2009-04-13
authors:
  - heliocastro
slug:    and-we-have-new-maintainers-kde-brasil-website
---
Hello everyone, from some time br.kde.org, the Brazilian KDE page was maintained in a slow pace, due the heavy duty that me, Maurício, Felipe ( the previous mantainer ) and other translations guys in role are into ( like real life ). Was basically pt_BR carbon copies of main website, nothing more.
As Felipe asked for a temp departure, we are in need of new fresh blood insane people to join the hard task and bring new life to the home.
So i'm proud to announce that Tomaz and Sandro, from the Live Blue project accepted take the task.
So, thanks to Felipe for all the years in the translation team and taking care of the site, and welcome to the new overlords of Brazilian KDE Site.
Hope they will aim to a new high quality project as our friends of KDE India are doing !!