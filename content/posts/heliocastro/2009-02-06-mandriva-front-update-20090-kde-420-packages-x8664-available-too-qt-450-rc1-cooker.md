---
title:   "Mandriva Front Update - 2009.0 KDE 4.2.0 packages for x86_64 available too. Qt 4.5.0 RC1 for cooker available"
date:    2009-02-06
authors:
  - heliocastro
slug:    mandriva-front-update-20090-kde-420-packages-x8664-available-too-qt-450-rc1-cooker
---
* Mandriva 2009.0 
KDE 4.2.0 packages now updated for both i586 and x86_64.
A standard urpmi repository is available.
<a href="ftp://ftp.kde.org/pub/kde/stable/4.2.0/Mandriva/2009.0/README">README</a> for some detailed information.
Some reference to a corrupted l10n packages will be verified and fixed this weekend, but not affects regular usage.

* Cooker
Now to the early adopters/testers, the brand new Qt 4.5.0 RC1 is available for Cooker ( future 2009 spring ) users.
The urpmi test repository is available for i586 only for now, using:
urpmi.addmedia qt45 ftp://users.mandriva.com.br/~heliocastro/qt45/i586
SRPMS can be found on same tree.
KDE users in cooker will be able to upgrade and test it. Some minor issues can be found, but should work without bumps.

Have a good weekend !!
<!--break-->
