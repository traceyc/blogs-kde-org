---
title:   "We're looking for passionate Qt and KDE developers !!"
date:    2010-08-17
authors:
  - heliocastro
slug:    were-looking-passionate-qt-and-kde-developers
---
<a href="http://www.collabora.co.uk/"><img src="http://www.collabora.co.uk/logos/collabora-logo-small.png"></a>



So, as looking that everyone is in the hiring mode...

Yes, <a href="http://www.collabora.co.uk/">Collabora</a> is hiring. So if you are passionate for open source, want join a company that share the same passion as well, want to work from any place in the world and have at least some of these skills below, then we want to know you.

<ul>
<li>KDE
<li>Qt
<li>Qml ( Qt Quick )
<li>C++
<li>WM and Graphics(3D, GL/GLES, X, Mesa) as a plus
</ul>

Consider yourself a serious KDE hacker? Don't be shy, we want to know you too ! KDE is an important part of our lives now.

Then, what are you waiting for ?
Share your hacking ninja code skills contacting us at our <a href="mailto:jobs@collabora.co.uk">hiring info channel</a>, also known as email :-).
<!--break-->
