---
title:   "A Tale of Chile "
date:    2010-10-27
authors:
  - heliocastro
slug:    tale-chile
---
I've been last week in Concépcion, south of Chile to attend another nice open source conference, been the second brazilian around ( Sulamita Garcia beat me first :-). But for me was special since last year we have finally some latin american KDE communities giving life signs, and i would expect that take more time to have they start to appear strongly, even more most of latin america been a strong Gnome supporter for years.

Well, here's Camilo Astete to prove me wrong:
<table style="width:auto;"><td><tr><a href="http://picasaweb.google.com/lh/photo/bjkDFK7RryeRIbTpAwcMnQ?feat=embedwebsite"><img src="http://lh6.ggpht.com/_eTOpRUrSM-c/TMheW9iWJMI/AAAAAAAAB74/5Dha1lTGNDU/s144/IMG_0695.JPG" height="80" width="144" /></a></tr><tr style="font-family:arial,sans-serif; font-size:11px; text-align:right">From <a href="http://picasaweb.google.com/heliocastro/EncuentroLinux2010?feat=embedwebsite">EncuentroLinux2010</a></tr></td></table>

Camilo managed to start a group that is large in quality and quantity than we had in Brazil for years, and the result was quite brilliant:

<table style="width:auto;"><tr><td><a href="http://picasaweb.google.com/lh/photo/PBealOqL3YxYNn531sAe_g?feat=embedwebsite"><img src="http://lh4.ggpht.com/_eTOpRUrSM-c/TMhelL83SMI/AAAAAAAAB9M/j32K11FYigE/s144/IMG_0720.JPG" height="80" width="144" /></a></td></tr><tr><td style="font-family:arial,sans-serif; font-size:11px; text-align:right">From <a href="http://picasaweb.google.com/heliocastro/EncuentroLinux2010?feat=embedwebsite">EncuentroLinux2010</a></td></tr></table>

I just jumped in to give my usual motivational talks ( i wish ) and talk from KDE to even Meego in a 4th extra talk i gave on Saturday, the first KDE Day on EncuentroLinux. Not mention small tremor during my talk that i barely felt, but all the guys mentioned to me after. Concépcion was near the epicenter of tragic earthquake on January.

About the conference, EncuentroLinux, i have huge thanks to Carlos Rios Vera for all the help. The "expositores" never was so well been treated ! 
The event itself was almost like Latinoware for me. The only different part is that for some reason people speaks more spanish and i was the only speaking portuguese ( or portuñol sometimes :-). But place, weather, people. All latinoware. All friendly and happy faces. See for yourself.

<table style="width:194px;"><tr><td align="center" style="height:194px;background:url(http://picasaweb.google.com/s/c/transparent_album_background.gif) no-repeat left"><a href="http://picasaweb.google.com/heliocastro/EncuentroLinux2010?feat=embedwebsite"><img src="http://lh6.ggpht.com/_eTOpRUrSM-c/TMheKfT9QrE/AAAAAAAAB9Y/OeMF9DzZBe4/s160-c/EncuentroLinux2010.jpg" width="160" height="160" style="margin:1px 0 0 4px;"></a></td></tr><tr><td style="text-align:center;font-family:arial,sans-serif;font-size:11px"><a href="http://picasaweb.google.com/heliocastro/EncuentroLinux2010?feat=embedwebsite" style="color:#4D4D4D;font-weight:bold;text-decoration:none;">EncuentroLinux2010</a></td></tr></table>

I had a great time there and aiming for next year in Puerto Montt or Santiago, where for sure we will have a bigger KDE Day how this community deserve.