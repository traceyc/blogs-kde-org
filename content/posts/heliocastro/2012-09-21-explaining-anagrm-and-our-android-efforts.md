---
title:   "Explaining Anagrm and our Android efforts"
date:    2012-09-21
authors:
  - heliocastro
slug:    explaining-anagrm-and-our-android-efforts
---
Hello blog, long time no see :-)

In last few months, <a href="http://www.collabora.co.uk">Collabora</a>, my employer, moved me and a team of good hackers to a mission of enable in some way automatic open source tools, code and libraries on top of AOSP ( Android Open Source Project ), and with our central focus are Wayland, Gstreamer and Pulseaudio, and a bit with Qt5 before, but this for later and will be some personal project soon.

Along with Pekka Paalanen buildsystem approach, we're able to create a buildsystem process where using standard AOSP and OSS libraries and we can compile with a simple addition of a Android.mk and non intrusive changes on Makefile.am. This are the results of Derek Foreman's initial work with <a href="http://derekforeman.blogspot.com.br/2012/04/androgenizer-porting-libtoolized.html">Androgenizer</a> and afterwards our work on our own AOSP tree,

For the most of parts, we needed a simple tool which could aggregate our manifests for the projects, available here in <a href="http://cgit.collabora.com/git/android/aggregates.git">Collabora CGit Aggregates</a> and join for one or more projects. This is what anagrm tool is about. Name comes from ANdroid AGReggates Merge and is aimed to work with XML files available there. It creates a local_manifest with merged projects ( or single project ) and uses the required entries to adapt on the specifications.
Usage is quite simple and straight forward, you can list available projects, and request the local_manifest for one or more. Done.
Was done using our cgit infrastructure, so if you think there's some usage for you, maybe some customization will be needed. 
You can find it here: <a href="http://cgit.collabora.com/git/android/anagrman.git/">Anagrm</a>

If you interested in how things gone at all, Pekka will be blogging on <a href="http://planet.collabora.com/">Planet Collabora</a> about all this details on our Android open source enablement project soon.
<!--break-->