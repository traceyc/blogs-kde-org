---
title:   "Radio"
date:    2009-01-18
authors:
  - frederik gladhorn
slug:    radio
---
Björn and I sat down (or rather mailed around) to get the FSFE Fellowship Meeting and KDE 4.2 Stuttgart Release Party going.
Of course that involved talking to people around here.
Ingo of RadioTux was on my list of locals, so I contacted him.
What I didn't expect was to be interviewed on RadioTux just a few days later.
I had a short interview on <a href="http://radiotux.de">RadioTux</a>.
So if you are really brave, you can listen to me (in German), stuttering <a href="http://www.linuxpodcast.de/index.php?id=273">here</a>.
After some technical problems they got me on the phone at around 65 minutes.
And d'oh, could I think of many things to say when skipping through it afterwards... I guess I still need to work on my promo skills.
But it turned out much less embarassing than I had originally feared :D

Regarding the release party, I'm quite happy that we'll be a strong team to represent FSFE and KDE.
Now I'd be happy to know who else is comming... (yes, please drop me a mail at my lastname@kde.org if you plan to come).
I'm looking forward to meeting Björn and the FSFE Fellows, Ingo (RadioTux), Daniel, Frank, Vallerie, Jörg and Lydia (KDE all) :)

In totally unrelated other news...
<img src="http://hemswell.lincoln.ac.uk/~padams/images/igtf.png" alt="I'm going to fosdem"/>
<!--break-->