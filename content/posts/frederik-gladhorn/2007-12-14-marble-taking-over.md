---
title:   "Marble taking over?"
date:    2007-12-14
authors:
  - frederik gladhorn
slug:    marble-taking-over
---
Torsten, the evil marble-mind tries to take over the world, but that's old news (pun where?).
He talked me into getting hot new stuff support into Marble. That was real easy... except of course that marble has all this custom stuff to be a pure qt app. Well it works now. You can get a very nice map of crustal ages of the earth there now. Don't ask me what it actually shows, I guess the legend has to be adjusted a bit ;)

After hacking away on KNS2 for a while, I just could not see it any more, so I continued my experiments on Parley, and tried putting the Marble widget in there.
The vocabulary file has been created by a nice guy, including pictures and just makes me smile when I look at it. There are about 1600 words in four languages in there and about 125 pictures. Yay!
And don't be scared by the interface clutter - all docks can be hidden and the first time start will be with only the lesson dock visible probably.
[image:3147]
This rocks. Now if we add in Johannes new practice gui... I'm already looking forward to 4.1 :)
Maybe someone can tell me what I have to do to get the HTTP header when doing a KIO::FileCopyJob. With KIO::get, I get the header as metaData(). For FileCopy the meta data is just empty. Is that intentionally? Or is it reasonably easy to use get and something to download the file?
Also big files seem to work badly on the server we now have managed by Josef - kstuff.org. While small files work, a 3.6 mb marble map chokes from there, while it works when downloading from edu.kde.org.
Argh.
<!--break-->