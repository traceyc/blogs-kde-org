---
title:   "Zombies"
date:    2007-11-06
authors:
  - frederik gladhorn
slug:    zombies
---
Yesterday I wandered around in my library a little and stumbled upon a book about XSLT. I never intended to get into that stuff because I hate doing html and this looked far too similiar to that. But I picked up the book, read some of it yesterday for half an hour and today I took two hours to see, if our kvtml format was usable with it, since it's just xml.
I also picked up a book about css which I never looked at before.

The idea is to make printing from Parley (and on the way KHangMan, Kanagram and KWordQuiz) just a little less painful.
The current implementation is horrible to use and so far I have not spent a minute looking at it. Maybe I can avoid it this way ;)
So the idea is to profit of the xml we have and shift the problem. If we generate nice HTML, printing that will be easy and hopefully everyone is happy with that too.
There's still a lot to do, but for now the basic is there. This is the time where people with some better HTML/CSS knowledge have to get involved, as I am incapable of coming up with a nice layout.

I'll abuse websvn.kde.org to shown an example. That's really easy for any of our vocabulary documents, further improvements are on their way.
Sadly this does not work with konqueror since it needs client side XSL, but with firefox it works.
<a href="http://websvn.kde.org/*checkout*/trunk/KDE/kdeedu/libkdeedu/keduvocdocument/docs/basic_vocabulary_en2de.kvtml?content-type=text%2Fxml">a kvtml file transformed to html (needs a xsl capable browser)</a>

On IRC we had a nice visitor "Derbeth" from the Polish wiktionary, which was a good thing, because he was working to create some vocabulary files - even including images and sounds -  which are already collected at the <a href="http://pl.wiktionary.org/wiki/Indeks:Angielski_-_Owoce">Polish wiktionary site</a>. Yay! One bad thing with wiktionary (it's a great source of words, pronunciation files and such) is that it seems to be heavily localized, so that the English page has less strict templates and will be much hard to parse while the German and Polish ones are much easier. So much for automatically generating vocabulary files from there... ideas are welcome of course, I'd love to start some cooperation with wiktionary.

And I'm quite happy that Parley starts receiving a bunch of feature requests and little improvement suggestions, but no crash or other big problem has been reported in quite a while :)

[image:3082]
Well, Halloween is over and I really don't care for it much, since here it seems the kids don't get around to saying something. Opening the door and looking at them either makes them run or babble something. And the German version of "Trick or treat" sounds rather bad to my ears. So I decided to just spend a nice evening relaxing. But then I was invited over to some friends and figured it was just the occasion for some "Zombies!!!" - one of the worst board games I own. It always boils down to pure luck and a (rather boring) die throwing... having the most 5 and 6 will let you survive longest :(
<!--break-->