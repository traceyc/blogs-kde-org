---
title:   "Open Source Expo 08"
date:    2008-05-26
authors:
  - frederik gladhorn
slug:    open-source-expo-08
---
Yesterday I arrived (too early for my taste) in Karlsruhe, to present KDE at the <a href="www.openexpo.de">Open Source Expo</a>. The OpenExpo is a rather new fair, so it was not very full but there was an audience for the talks.
I gave a general KDE 4 introduction, with the help of Martin Gräßlin.
We started early since the talk before ours did not take place for some reason. That took away a little from my pre talk meditation time :o
I started with some slides about KDE 4 based on what Lydia already used a while ago, but very reduced. I soon switched over to live demoing of what will become KDE 4.1 and it was great fun. I could even show how nice the crash handler looks, though that was somewhat unplanned.
Having a nice benevolent audience with good humor and me being in a good mood too, I got pretty positive feedback after-wards anyway :)
Martin then gave a ten minute KWin tour showing compositing and explaining some of the backgrounds, very nice!
He'll be working on the desktop cube KWin effect this summer.
Marble with open street map is quite a looker too.
Sven later gave a talk about Amarok 2 which also got quite a bit of attention.
What surprised me (yeah, call me naive) was people requesting documentation (books on KDE 4) to get started :)
And maybe I should start using a localized version of KDE for this kind of presentation, something that I usually don't do.
All in all the fair could have benefited from more visitors but was nice. Great catering :)
I enjoyed meeting new people again, Stephan and Martin for KDE as well as some nice people from Python, Ubuntu and Open Street Map.
Seeing the local crowd (Sput, Sven and Lydia) in Karlsruhe again was great too, I found out Nini is as good a table soccer player as I am.
Sven (the one with the good taste in music:) ) provided me with a place to sleep, thanks!

[image:3487]
Lydia and Stephan
[image:3486]
Stephan and Sven