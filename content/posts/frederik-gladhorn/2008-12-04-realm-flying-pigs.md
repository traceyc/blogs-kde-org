---
title:   "The Realm of the Flying Pigs"
date:    2008-12-04
authors:
  - frederik gladhorn
slug:    realm-flying-pigs
---
<p>
So M got a flying pig for his birthday. Imagine that!
Awesomeness - aren't you jealous? I surely am. It's even pink!
There are days (or does it only happen at night, when you can't sleep?) where flying pigs seem to be the secret rulers of KDE.
<img src="http://ktown.kde.org/~gladhorn/blog/pig/blog_pig.jpg" alt="Flying Pig" />
</p>
<p>
M told me that he used to have a moving earth as desktop wallpaper. 
At night when I should have been sleeping the pig came flying by to remind me of the marble spinning in space.
The pig kept talking to me. It reminded me that I had read about patterns as desktop background and the Mandelbrot fractal.
And there was a plasma applet based on marble already. All ingredients there right?
So yesterday I sat down and started writing a moving earth desktop wallpaper.
It's currently in playground/base/plasma/wallpapers and just an early proof of concept.
</p>
<embed src="http://blip.tv/play/Ad6WOwA" type="application/x-shockwave-flash" width="824" height="680" allowscriptaccess="always" allowfullscreen="true"></embed>
Since the planet doesn't show the video, here is a link: <a href="http://blip.tv/file/1534665">Marble wallpaper video</a>
<p>
The great thing about this is that it took only approximately three hours to get it to work for the first time. 
To me that shows how easy to use our APIs have become and how much power KDE 4 offers.
There are stil lots of things to improve in the wallpaper globe.
Some are very simple like adding a sun and stars (maybe optionally) by simply switching them on as Marble plugins. 
Another great idea would be to enable mouse interaction with the background. 
It's all there, probably just a few lines of code missing to get the first truly interactive background in KDE.
And how about a different perspective - some more tilt to get a more 3d impression when flying over the country.
Thank you pink flying pig.
</p>
<!--break-->
