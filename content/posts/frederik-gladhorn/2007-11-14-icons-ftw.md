---
title:   "Icons ftw!"
date:    2007-11-14
authors:
  - frederik gladhorn
slug:    icons-ftw
---
After much back and forth, Parley has an icon!
<img src="https://blogs.kde.org/files/images//ox256-app-parley.png">
And not only one, Lee also gave in to my wish to create maybe one or two other icons for the menus and such... so now I have 13 new icons, some even in svn already and some more on the way :) (note: the screenshots are not the final versions... some of the icons have already been updated yet again)
It's great to see how many people care about this little edu app which really is not that important... but it made four people (that I have contact with) get KDE4 from svn ;)
Getting regular user feed back is the best that can happen I guess. Which reminds me: Thanks to you all for making this possible! Keep up the great work!
<img src="https://blogs.kde.org/files/images//parley_config_practice.png">
Thanks Lee Olson!
<!--break-->