---
title:   "Note to self - vim :g"
date:    2008-11-11
authors:
  - frederik gladhorn
slug:    note-self-vim-g
---
While eagerly awaiting KDevelop4 to become stable (it crashes on me too often right now)...
I'm back to forgetting vim commands ;)

To get a list of files on the left (still need to get comfy with it, but it looks useful) use the <a href="http://www.vim.org/scripts/script.php?script_id=69">Project plugin</a>

Substitute (replace stuff - I've used this so often that I remember, but maybe you don't yet?)
<code>:%s/what/with/g</code>
(with % on the entire file, without only current line)
(g all occurrences in the line, without only the first hit)

Do something on lines that mach a search pattern (this is what I always forget because I don't use it that often):
<code>:g/search pattern/command</code>

Delete empty lines using the above:
<code>:g/^$/d</code>
<!--break-->
