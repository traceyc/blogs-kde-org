---
title:   "FrOSCon"
date:    2008-08-24
authors:
  - frederik gladhorn
slug:    froscon
---
Back from <a href="http://www.froscon.de">FrOSCon</a>.
I followed the <strike>frog</strike> grasshopper.
It was great to meet so many nice people again :)
The KDE table (booth just doesn't fit) was shared with Kubuntu and Amarok. I think we all fitted together nicely, not really appearing as three projects that much. Thanks to the Kubuntu people for helping a lot in keeping the table/booth running!
The blue table cloth we had (kudos Kubuntu) gave us extra appeal. And if it hadn't had a little too much beer in the evening that would have included Sunday...
Apart from talking to visitors and other projects and listening to talks, Saturday evening was all about being social. Or it was all about beer, I don't remember clearly.
I think we did fairly well on the dance floor also, I enjoyed bouncing around with Sebas, Danimo, Markey, Sven and all the others, though I do hope, no photos of us dancing around professionally are around ;)
Thanks a lot to Valerie and Jörg for giving me a ride, that was awesome. And the Grasshopper.

<img src="https://blogs.kde.org/files/images/booth.jpg">

Things learned:
<ul>
 <li>The true reason to "get that KDE thing":
   <ul>
      <li> Wobbly windows (not for me though) </li>
      <li> The amazing Pink Bunny theme for KBlocks (I agree on this one so much, couldn't live without it any more) </li>
  </ul>
</li>
 <li> Andrew Tanenbaum gives great talks, lots of humor included.</li>
 <li> If you are going to give talks in a secluded dev room far far away from everyone, at least make sure to have some schedule for that hanging around publicly, especially if it's not in the official program </li>
 <li> Planning stuff with Alexandra is fun. Though we could have ended up with the resulting price list for the merchandising faster if we hadn't taken some detours :)
 <li> Lydia and Alexandra have this mindsharing thing going on. Giving a talk together, one of them starts the sentence, half the time the other finishes it. Some sentences were even split into three parts, going back to the original speaker again. Impressive. </li>
 <li> The new booth box rocks! It has comfortably padded compartments, rolls to move it around and good stuff inside. Including the shiny new mini booth PC. Yay! </li>
 <li> How to create a gallery thing for images. Uploaded <a href="http://ktown.kde.org/~gladhorn/akademy_pictures">Akademy pictures</a> using the Matrix theme from Digikam. Got to love this green. </li>
 <li> This time I changed my KDE language to German, only to end up not really showing it to anyone :) I still think it might be a good idea to run l10ned versions of KDE for local events, though right now it irritates me a lot seeing it in German. </li>
 <li> Grasshoppers can be adrenaline junkies too. We had one going south with us. It chose to travel on the windshield and had no problems staying there at 130 km/h. We let it hop off on the next parking lot though. Hero of the day I'd say. </li>
<img src="https://blogs.kde.org/files/images/grasshopper_0.jpg">
</ul>
<!--break-->