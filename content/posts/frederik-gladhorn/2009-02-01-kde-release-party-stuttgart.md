---
title:   "KDE Release Party Stuttgart"
date:    2009-02-01
authors:
  - frederik gladhorn
slug:    kde-release-party-stuttgart
---
Björn of the FSFE initially triggered the organization of a joint KDE 4.2 Release Party and FSFE Fellow Meeting in Stuttgart. I'm very happy that our local team grew quickly so everything went very smooth. So Friday it was time to meet and celebrate. At this point we had no idea how many people would show up.

<img src="http://ktown.kde.org/~gladhorn/blog/release42/img_0025_small.jpg" align="left" border="3"  vspace="10" hspace="20"  />
At the time we were supposed to meet and start preparing the few things we had to it was not exactly very warm outside. Me being extra double plus busy all week, I arrived slightly late at 5:45 at the Unithekle where our release party was to take place. To my surprise I found some brave souls standing in the cold outside. They quickly enlightened me, that being outside was not a voluntary decision, but because the place hadn't opened yet. Blame goes to me, since I booked the room - oops. Seems like FSFE and KDE people are not only very nice but also forgiving, I didn't get thrown into the next dumpster or any of that kind. A few minutes later the nice guy on duty this evening opened the doors for us (relive). The only downside was that this evening they couldn't serve food since the kitchen wasn't open :(

After these initial hurdles we happily moved inside (getting warm again, yay). I have to thanks everyone for helping so much, getting the projector set up, posters pinned to the wall and being great! While everyone got stuff done I finally managed to my slides in place so I could do the official welcome.
<br clear="all">

<img src="http://ktown.kde.org/~gladhorn/blog/release42/img_0032_small.jpeg" align="right" border="3"  vspace="10" hspace="20"  />
We were more than 40 people crowding the room we had reserved! The evening stared with a welcome and quick introduction to KDE 4.2 by me. Björn gave an overview of the FSFE activities. We had a short break to get something to drink and chat. Next up was a fun round of mini introductions so we'd know who we are. This is something I'd really recommend for meetings of this size, it went extremely smooth without anyone talking too long since we set up some rules. Everyone had to tag ("real life 2.0")  her/himself with three words (I started taking four words which lead to instant booing ;) ). This way introductions were really quick and we could place each other a little bit - great conversation starter for people that have never met before.

The official part then continued with Frank talking about getting involved with KDE and our community. Sven finished with a live presentation of Amarok 2.

Discussions lasted late into the night. Still, with so many people I sadly didn't manage to talk to everyone personally, so there's still some catching up left to do.

One idea that Kurt Pfeifle came up with is to have regular local KDE meetings. I think it's a great idea. We are enough people to have a good basis and I'd love to get more people involved. I think it would be great to have some workshops - for example how to start translating or hacking on parts of KDE. If you're interested in meeting KDE people in and around Stuttgart, please let me know (my mail is still my lastname @kde.org).
<br clear="all">
<img src="http://ktown.kde.org/~gladhorn/blog/release42/img_0035_small.jpeg" align="left" border="3"  vspace="10" hspace="20"  />After many interesting discussions we enjoyed listening to Martin Konold who told us some fun storys about the first years of KDE. I enjoyed meeting everyone and I'm looking forward to other opportunities to gather. One thing that made me particularly happy was that I received lots of positive feedback, even by mail on the next day. It seems everyone enjoyed the evening and we had a broad enough spectrum of talks to offer something for all.

Hopefully Ingo will organize another Linux Day at the Hochschule der Medien soon, so we'll get together again. Kudos to Jörg for getting us a projector on short notice! And especially <a href="http://www.schiessle.org/blog/2009/02/01/an-awesome-event-is-over-kde-42-release-party-and-fellowship-meeting-in-stuttgart/">Björn</a> who did lots of the organizational work and starting it in the first place! Thanks Frank and Sven for giving interesting talks!

Now I'm looking forward to meeting many of you at FOSDEM next week :)
<!--break-->
