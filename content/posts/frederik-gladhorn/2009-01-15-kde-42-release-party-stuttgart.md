---
title:   "KDE 4.2 Release Party in Stuttgart!"
date:    2009-01-15
authors:
  - frederik gladhorn
slug:    kde-42-release-party-stuttgart
---
<a href="http://fsfeurope.org/news/2009/nyr.en.html"><img src="http://fsfeurope.org/news/2009/nyr/nyr2009_button.png" align="right"/></a>
I'm happy to announce a KDE 4.2 Release Party in Stuttgart. At the same time this will be the first FSFE Fellowship meeting here.
Together with the FSFE we will celebrate the release, meet, talk and have fun.
Of course everyone interested in Free Software is invited.

<a href="http://www.schiessle.org/blog/">Björn Schiessle</a> will talk about the FSFE and we will present the goodness that KDE 4.2 brings live on stage ;)

<p><b>Date:</b> January 30th, 2009 (Friday)<br />
<b>Time:</b> 18:00<br />
<b>Place:</b> Unithekle (<a href="http://openstreetmap.org/?lat=48.74341&lon=9.09723&zoom=17&layers=B000FTF">Open Street Map</a>, <a href="http://www.lug-s.org/unitop_/">Lageplan der LUG-S</a>)</p>

For a list of all release parties have a look at <a href="http://wiki.kde.org/tiki-index.php?page=KDE+4.2+Release+Party"> wiki.kde.org</a>.
<!--break-->
