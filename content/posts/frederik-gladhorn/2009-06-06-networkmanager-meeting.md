---
title:   "Networkmanager Meeting"
date:    2009-06-06
authors:
  - frederik gladhorn
slug:    networkmanager-meeting
---
Yesterday I arrived in Oslo just in time for pizza and talking to Will, Darío, Thiago, Olivier, Knut and the students working on mobile broadband connections.
We had a great time so far, discussing apis and cleanup of our network manager bits.
Working with Will and Darío is fun! We kept planning even on our little bar prowl with Olivier.
Darío and I kept giggling about the never ending night. Three in the morning felt like late afternoon... Good thing we came during the summer to visit Oslo.
Now it's time to start improving the applet and libs after a great breakfast.
Time to get some work done ;)
<!--break-->
