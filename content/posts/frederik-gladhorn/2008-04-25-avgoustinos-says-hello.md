---
title:   "Avgoustinos says hello"
date:    2008-04-25
authors:
  - frederik gladhorn
slug:    avgoustinos-says-hello
---
Avgoustino Kadis works on a summer of code project for Parley and has ambitious plans:

Hi! I'm a fresher in Computing at Imperial College London and I've the
luck to participate in GSoC this year with doing something for Parley.
I'm going to make it easier & faster for someone to create a
vocabulary file for Parley by providing automatic translation and
retrieval of unknown words from a text. At the same time I'll get
involved in open source development for the first time and if I like
it I'll keep on helping on making Parley even more useful, easy and
fun! And then!! I'll get deeper and deeper into KDE development,
become the leader of all the development team and force all KDE
desktops have me as background and no way to change it!! Just kidding!
:D Have a nice day ;)
<!--break-->