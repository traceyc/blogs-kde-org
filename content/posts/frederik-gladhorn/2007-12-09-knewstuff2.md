---
title:   "KNewStuff2"
date:    2007-12-09
authors:
  - frederik gladhorn
slug:    knewstuff2
---
After Jeremy adopted KNS2 as his new project, being pushed by Annma also, the first results start to show.
We invaded the so far very low traffic <a href="http://lists.freedesktop.org/mailman/listinfo/ghns">freedesktop.org get hot new stuff mailing list</a> and got some status updates so.
Jeremy already implemented returning a list of changed entries to the app using KNS2 two weeks ago, tonight I added getting a list of installed files from these entries.
This is good because it enables apps to work with the downloaded data without having to use the install command in their appname.knsrc which was rather limited in possibilities.
The other thing is being able to uninstall downloaded stuff. After getting a file, one can now click uninstall again to get rid of it. And install it again. And remove it again... you get the hang of it.
Since we are now trying to get it to work properly (without api modifications of course) this is probably the time to ask questions and tell us what's not working with it for you. At least regarding download. I hope upload will work soon as well, I don't know if anyone really tested it yet.
<!--break-->