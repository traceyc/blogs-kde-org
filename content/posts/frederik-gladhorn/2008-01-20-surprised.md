---
title:   "Surprised"
date:    2008-01-20
authors:
  - frederik gladhorn
slug:    surprised
---
Seems like the official launch of KDE4 generates quite some momentum all around. I got two surprise presents today ;)
Michael Hofer started a Java app to practice Parley vocabulary files "on the road" - using mobile phones that is. Open xml formats are a good thing.
See the project homepage at sourceforge: <a href="http://sourceforge.net/projects/mobvoc/">MobVoc</a>.
Lee sent me an update of Parley icons and I'm very very happy this time! The toolbar looks much clearer now (the icons are a bit simpler to make them easier to recognize in small sizes).