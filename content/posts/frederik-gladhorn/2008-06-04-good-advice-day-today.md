---
title:   "good advice day today"
date:    2008-06-04
authors:
  - frederik gladhorn
slug:    good-advice-day-today
---
Here's another tip for those not yet on subversion 1.5 (which supposedly comes with the goodness below integrated).
<a href="http://www.orcaware.com/svn/wiki/Svnmerge.py">svnmerge.py</a> makes merging branches back and forth a little less painful. Most distributions have a package for it, it can be in svn-utils or something like that and sometimes it's simply called svnmerge without the py. Whatever.
Say we have this great new project in KDE/kdeedu/foo and now we decided to branch it because the soc-foo project will be worked on during feature freeze...

First set up a branch in subversion - nothing to be feared :)
You only need to get the path right, add your user name or https.
We'll copy the cmake directory also, but leave out other projects from kdeedu since we'll only develop foo. (Note the -N to not copy sub-directories in the first svn cp)
<code>
$ svn cp svn+ssh://username@svn.kde.org/home/kde/trunk/KDE/kdeedu svn+ssh://username@svn.kde.org/home/kde/branches/work/soc-foo -N
$ svn cp svn+ssh://username@svn.kde.org/home/kde/trunk/KDE/kdeedu/foo svn+ssh://username@svn.kde.org/home/kde/branches/work/soc-foo/foo
$ svn cp svn+ssh://username@svn.kde.org/home/kde/trunk/KDE/kdeedu/cmake svn+ssh://username@svn.kde.org/home/kde/branches/work/soc-foo/cmake
$ svn co svn+ssh://username@svn.kde.org/home/kde/branches/work/soc-foo
</code>
Now we have our copy of foo and also the cmake dir in a branch :)

Making changes to the branch is required, but we also want the bug fixes from trunk.
svnmerge.py makes that easy (once it is set up).

Now you take the revision where the branch was created (eg 814453) and init that with svnmerge.py:
<code>
$ cd soc-foo #be in the BRANCH directory
$ svnmerge init svn+ssh://username@svn.kde.org/home/kde/trunk/KDE/kdeedu -r1-814453
</code>
Results in the status message <code>property 'svnmerge-integrated' set on '.'</code>.
Now we need to check in this property change.
<code>$ svn ci -m "init svnmerge.py"</code>
Done.
You might wonder what we have won at this point though.
The real benefit is when you want to get changes from trunk into your branch now:
<code>
$ cd foo-soc
$ svnmerge.py merge
$ svn ci -F svnmerge-commit-message.txt
</code>
Use either svnmerge-commit-message.txt which tends to be lengthy and is just a textfile created by svnmerge.py and contains a revision log, or use any commit message to your liking. (<code>$ svn ci -m "merge fix for fooXYZ from trunk"</code>)
svnmerge.py takes care of tracking what you have merged, you can also tell it to skip individual revisions.
To get more info read the nice <a href="http://www.orcaware.com/svn/wiki/Svnmerge.py">svnmerge.py</a> wiki page.
And if you're really bored, find out what was branched in revision 814453 in reality.
Oh yes, and of course the guy to originally point me to this was Pino again, thanks dude ;)
<!--break-->