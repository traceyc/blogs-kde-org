---
title:   "It's a long way to Rome."
date:    2005-05-18
authors:
  - wildfox
slug:    its-long-way-rome
---
The last weeks have been amazing, this is the only word I can
use to describe what happened in kdom/ksvg2/kcanvas development.

KCanvas has been rewritten some weeks ago to go back from the
old chunk-based rerendering algorithms to a new strategy, using
z-ordered trees (well read the source :-) - it turned out to
perform much faster, especially when panning/zoominf, it also
simpified internal KCanvas code a lot, and yes it's much more OOP.

ksvg2 is being stabilized & markers have been added recently.
A lot of work can still be spent in the Animations area, though
the framework if pretty much done now, in a clean & stable way.

I'm, perosnally, more into callgrinding (aka. profiling) ksvg2,
in order to load a 33 MB (yes 33) svg data file, in short time.
Short time currently means minutes & extensive memory usage (factor 10).

Now what to do to optimize that bugger? Easy answer: Safari WebCore
contains a lot of optimizations (ie. in the css area) which speed up
the whole parsing/restyling etc. really in a great way. Also memory
usage is much reduced. So first thing I did last weekend is merging
in some WebCore changes, which I stopped for now because of a simple
reason: I'm also porting khtml to kdom in the meanwhile, and well
the internal concepts in for instance the css engine changed heavily.

So I spent the last days syncing khtml's with kdom's css stuff, which
I just commited to SVN. Then I'll continue khtml2 (new fancy codename ;).

Once we have ksvg2 & khtml2 both on top of kdom, I will definately work
on the Safari optimizations, before I'll concentrate on CDF (Compound
Document Format; ie. svg in xhtml...) again. As you see a LOT of work
for me recently, but hey I'll hope it'll improve our all future :-)

Thanks for listening until here, I'll keep you updated over the next
days - ah and for the curious: yes we'll port everything to Qt4, once
the khtml2 -> kdom port is somewhat functional.... Long live KDE!

WildFox, aka. Niko :-)
