---
title:   "Akonadi logo contest heats up"
date:    2008-04-25
authors:
  - till
slug:    akonadi-logo-contest-heats
---
As Tom <a href=http://www.omat.nl/drupal/needed-akonadi-logo>announced</a> a few days ago, the Akonadi team is looking for a logo and an icon for the little system tray application. So far we have three submissions, but us kdepim hackers at <a href=http://www.kdab.net>KDAB</a> thought we'd give folks an extra incentive by donating a Canon Powershot digital camera, new and unused, to be given to the creator of the work we will chose in the end. This fine and only slightly out-of-date (as digital cameras are doomed to be the moment one takes them off the shelf) piece of gear can be yours, yes, yours, if you add your submission <a href=http://techbase.kde.org/index.php?title=Projects/PIM/Akonadi/Logo>to the techbase page</a> and we end up selecting it in a process that will very likely be utterly subjective, unfair, unprofessional and morally objectionable. So there. Keep those submissions coming!<!--break-->