---
title:   "We're going to Akademy :)"
date:    2008-07-11
authors:
  - till
slug:    were-going-akademy
---
Like every year, so far, and as befits a <a href="http://ev.kde.org/supporting-members.php">patron of KDE</a>, KDAB is covering the travel and lodging expenses of all KDABians who want to attend Akademy. Yeah, for Kalle :). A few of the KDE folks in KDAB have chosen to stay behind, this year, and keep the customers happy, but most of us will be there. The current list includes myself, David Faure, Kevin Ottens, Laurent Montel, Andras Mantia, Volker Krause, Pradeepto Bhattacharya, Thomas Moenicke, Thomas McGuire, Andreas Hartmetz and Marc Mutz. We thought of renting a bus, collecting everyone in Berlin and then driving over, but the logistical aspects of that seemed dauting, for a bunch of geeks. So planes and trains it'll be. The talk schedule looks particularly interesting this year, but the best thing, as always, will be to catch up with old friends, make new ones and generally hang around the nicest, smartest, most enthusiastic and engaging group of people ever. Magically diverse, but filled with a common spirit and love, this community continues to fascinate and inspire me, as it does, I'm sure, all of my colleagues. See you soon, at Sint-Katelijne-Waver.<br><br><img src=http://hemswell.lincoln.ac.uk/~padams/images/igta.png><!--break-->