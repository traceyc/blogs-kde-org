---
title:   "mos def"
date:    2009-11-14
authors:
  - till
slug:    mos-def
---
Given the pile of awesome that was Camp KDE 2009 in Negril, Jamaica, how could I not attend this year as well? I'll be presenting and doing some Qt training sessions again, like last year, on whatever topic the audience wants. There'll be sun, there'll be hackery, there'll be merriment. You must not miss this, so make sure to sign up now and meet us in Sand Diego in January.<br >
<img src=http://amarok.kde.org/blog/uploads/campkde2010_logo.png>
<!--break-->
