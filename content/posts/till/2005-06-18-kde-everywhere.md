---
title:   "KDE Everywhere"
date:    2005-06-18
authors:
  - till
slug:    kde-everywhere
---
Since today <a href="http://www.kieler-woche.de">Kieler Woche</a> started, here in Kiel, the largest sailing event in the world, and it's an extremely beautiful day, 20 degrees Celsius, a warm breeze, and clear blue skies, I decided to go outside (yes, outside, meatspace, I am not kidding you) and finally take long promised pictures of the KDE logo with our local tourist attractions as part of Cornelius' brilliant "KDE Everywhere" series. So here goes:
[image:1176][image:1177]
I've also submitted the UBoot one to kde-look. I wonder whom to send the logo next? Any volunteers? :)