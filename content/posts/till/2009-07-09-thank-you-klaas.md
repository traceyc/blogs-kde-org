---
title:   "Thank you, Klaas "
date:    2009-07-09
authors:
  - till
slug:    thank-you-klaas
---
I'm back from the awesome Gran Canaria Desktop Summit. It feels good to be back in Berlin and with my family, but I'm very scared of the backlog that now awaits me. Before I tackle it (and potentially blog more about the event) I need to get something off my chest, lest it is forgotten. I'd like to publicly thank Klaas Freitag, who's term as a member of the KDE e.V. board of directors just ended with our general assembly a few days ago, for his contribution to our project. He stepped up to help out with the more mundane and every-day tasks that are required of the board, thus freeing up people like our beloved bouncing ball and poster boy Aaron (who's term also ended, but who'll get plenty credit anyway ;) to do what they do best. I really admire the effective, quiet and ego-less way in which Klaas has carried himself and represented us. He's done a lot of work behind the scenes that benefits KDE greatly and helped get e.V. and its operational side up to a sustainable level. So thank you very much, Klaas, and enjoy the extra time you can hopefully now spend with your family again. :)
<!--break-->
