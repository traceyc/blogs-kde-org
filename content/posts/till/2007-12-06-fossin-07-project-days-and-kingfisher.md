---
title:   "FOSS.in 07 - Project Days and Kingfisher"
date:    2007-12-06
authors:
  - till
slug:    fossin-07-project-days-and-kingfisher
---
Today is the third day of FOSS.in, the first day of the conference proper. The past two days have seen the "project days" for Gnome, KDE, Fedora, Debian/Ubuntu, Mozilla, OpenOffice.org and IndLinux. These are full day tracks, organized by the community, each packed with technical and non-technical talks. The KDE PD was a success, I think, our talks were well attended and people seemed interested in what we had to say. The whole event so far is a blast, just like last time I was here. The organizers somehow manage to keep a 2500 people event very personal, the whole team is extremely dedicated and nice.

<p>Last night we were kindly invited to a party sponsored by Mozilla, at Opus, a cool Goan place in downtown Bangalore, with a stage for (mostly) amateur performances. Excellent prawn curry, hilarious Karaoke and judging by the hangover I am currently shaking off 1 or 2 bottles of Kingfisher too many. Great evening with a great crowd.</p>

<p> It should be noted that Shreyas and Shilpa, in particular, sacrificed a lot of their own fun in order to stay completely on top of the situation, utterly professional, not to mention sober, and make sure all 17 (or was it 23? Hm, let's count again!) of us get back safely to our hotel, and get to bed early enough so we don't miss Atul's keynote. Which is about to start, so I better get myself over there, lest I bring his wrath upon me.</p>