---
title:   "GammaRay"
date:    2011-10-30
authors:
  - till
slug:    gammaray
---
At <a href=http://www.kdab.com>KDAB</a> we spend all of our time working with Qt. As with any software development, much of that time ends up being focused on understanding, debugging, profiling and bug fixing rather than implementing new functionality. To help us with that we constantly improve the tools available to us and other Qt and KDE developers. That is what has brought us awesomeness such as <a href=http://gitorious.org/massif-visualizer>massif-visualizer</a>, <a href=http://gitorious.org/kdevcpptools/verktyg>verktyg</a> or various bits and pieces in Qt Creator and KDevelop. The latest result of our tinkering with tooling was released at Qt Developer Days in Munich last week under the name GammaRay. It's a comprehensive collection of high level introspection and debugging utilities specifically tailored for the various frameworks in Qt. You can find out more about it and see some screenshots <a href=http://www.kdab.com/gammaray>here</a>. The whole thing, including all plugins, is released under the GPL so it can be useful to as many Qt developers as possible and in the hope that many of you will pitch in and help us make it even more useful for us all. <a href=http://lists.kde.org/?l=kde-core-devel&m=131997790827755&w=2>Here</a> is the announcement email to the KDE lists sent by Volker Krause, who has been leading this effort since its beginnings in the Kontact Touch project. Enjoy!
<!--break-->
