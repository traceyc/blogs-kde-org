---
title:   "h00t!"
date:    2004-07-22
authors:
  - till
slug:    h00t
---
Since I mentionend here that the KMail team could use some help with bug work a few days ago, Ismail "cartman" Donmez has started going through the wishlist closing duplicates and stuff that has since been implemented. I thought I'd give him a public thank you for that, it's really, really great to see that list get some love. So thank you, cartman, and keep up the good work. Maybe if a few more people find some time to help him, we might get the darn thing down to where we can actually use it again.<br>On a related note, we'll probably do a bug squashing day on sunday for Kontact and its components, Cornelius suggested it and everyone thinks its a great idea. It'll be interesting to see how many people show up to help. The Gnomes do them regularly and I guess if they were useless, they wouldn't do them, what with the the Gnomes not being stupid and all. :)