---
title:   "Akonadi, bossa remix"
date:    2010-03-11
authors:
  - till
slug:    akonadi-bossa-remix
---
It is raining massively, outside, again. It does that every day here, in Manaus, what with it being the rainy season and this being the Amazon jungle. The negativity ends there, though, since it takes about 15 minutes, is very refreshing, and everything else here is Awesome (TM). I have really enjoyed the past few days, Bossa Conference has been a great experience. The presentations were generally of high quality, I had many very good conversations over many excellent meals, and by a luxurious pool, met several impressively talented individuals and the equally impressive INdT teams. There is a lot of very nice work being done here in Brazil in Free Software in general and around Qt and KDE in particular. I'm proud to have been invited to come.
<!--break-->
The main reason for my being here is to present the current state of our efforts to bring Akonadi to mobile devices, solicit the feedback of the mobile experts here and hopefully convince some of them to help us out with their experience. The presentation was quite well received, I think, judging from the amount of interest afterwards and the very curious and in-depth questions. Let's hope our groove will soon be spiced up with some bossa and samba. My next stop on this trip is Recife, in the south of Brazil, to spread the Akonadi gospel there.