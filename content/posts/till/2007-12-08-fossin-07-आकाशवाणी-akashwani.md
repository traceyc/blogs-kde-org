---
title:   "FOSS.in 07 - आकाशवाणी (Akashwani)"
date:    2007-12-08
authors:
  - till
slug:    fossin-07-आकाशवाणी-akashwani
---
FOSS.in 07 continues to be a great conference experience for me. I had no talks on the first day of the main conference, which meant I got to listen to other people's presentations, chat with many interesting people and generally hang out and hack. In the evening we went to a downtown restaurant with a bunch of folks for a traditional south Indian style meal, which was excellent. After a good night's sleep we headed out to the conference venue again, through the utter insanity that is the Bangalore traffic, and Volker and I spent most of the morning finishing up and polishing our slides for our two Akonadi talks.<br>

The first one, a general introduction to Akonadi, was scheduled in the huge 750 seater hall which turned out to be somewhat oversized for the crowd we managed to draw. Since Harald Welte's OpenMoko talk was going on at the same time and not many people have yet heard of Akonadi, that was to be expected. Still, those who did turn up were interested and seemed to like what we presented. There were a few Gnome folks who said they'd help us pitch it to the Gnome PIM community, which was great. As part of this talk we code-named the state of Akonadi that comes with KDE4.0 RC2 (due out any minute now) आकाशवाणी (Akashwani) and released it to the world, so people can start playing with it, writing language bindings for it, extending it for new backends, new types and new use cases and start writing interesting applications on top of it.<br>

The second talk was a lot more technical and in depth, and thus the crowd was even smaller. We managed to interest at least one person enough to want to contribute, which makes this a great success. :)<br>

In the evening Kevin, Volker and I went to dinner with Ananth and Prashanth, both members of the kde.in community, to 13th Floor restaurant, on M.G. Road, at the top of a big mall building. Great food, great company, as has been the case for every meal all week.<br>

Now it's the morning of the last day of the conference, I'm looking forward to several talks today, Andrew Cowie's, Rusty Russel's, Ulrich Drepper's, lots of good stuff on today. And I'm sure there'll be more excellent food.