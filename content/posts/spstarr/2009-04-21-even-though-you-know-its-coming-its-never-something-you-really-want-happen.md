---
title:   "Even though you know it's coming it's never something you really want to happen..."
date:    2009-04-21
authors:
  - spstarr
slug:    even-though-you-know-its-coming-its-never-something-you-really-want-happen
---
Well, today I was let go after being at Platform Computing working on our Open Source software for almost 5 years.  It's been one of those up and downer days.  I read the FLOSS blog postings from all the different communities and see people being let go and now I'm one of them :-(

The writing was on the wall though for months, it's not like I didn't know this would happen. I just wanted to deny that It could happen, but it has happened.

So, for now. I will not be dedicating much time to KDE while I try to find new work elsewhere. I don't look at this as a sad day, but as a new beginning for me...

If anyone wants to hire a pretty smart Linux guy, please contact me :)

Shawn.