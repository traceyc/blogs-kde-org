---
title:   "From Plasma to BlackBerry 10/Qt and Cascades"
date:    2012-05-21
authors:
  - spstarr
slug:    plasma-blackberry-10qt-and-cascades
---
Speaking for myself and not for RIM

With RIM's serious push for Qt as part of the core frameworks and a more Open Source push for its ecosystem, it's the right time for me to jump in.

I Just started my initial porting of the Weather Forecast engine and applet to BlackBerry 10. It won't be Plasma based but pure Qt and Cascades UI with the kinds of effects that I really want to use.

I have to say it's pretty easy to get going. You go here: <a href="https://bdsc.webapps.blackberry.com/devzone/platforms/bb10">BlackBerry 10 Development</a>

Download the Simulator VM image for the BB10DevAlpha device, get VMPlayer and the BlackBerry NDK+IDE. Qt and Cascades builds are already in the VM image.

Although we're using VMPlayer, It's ok but mouse cursor is sluggish for some reason. I'd rather we had used VirtualBox.

The only real irritant which I hope is fixed is the IDE Cascades QML previewer. It requires legacy Mesa, jpg/png libraries not found in Fedora (I use rawhide but Fedora isn't supported only Ubuntu) any more but I can workaround this issue in other ways (maybe Qt Creator for QML previewing).

Since QNX/BlackBerry 10 are POSIX, porting my code won't be much an issue. At most, I have to just re-implement some methods similar to what Plasma has in pure Qt but that won't be difficult.

I already have the plugin mechanism working. The fun part will be the Cascades UI framework to look at. It is similar to QtUi but different enough for mobile use, takes the pain of layouts out, extends QML usage and adds a rich set of functionalities that are common across the platform which I plan to take advantage of when those APIs are ready. 

The embrace of Qt with QNX/BlackBerry 10 should be applauded and I hope more KDE and Qt developers look at the platform.

Porting parts of KDE to BB10 is possible but I don't know the logistics or restrictions that would be placed on it since we want a secure platform for people to trust. But I would certainly like to see KDE apps running on BB10!

Check out the Freenode IRC Channel #qt-qnx too.