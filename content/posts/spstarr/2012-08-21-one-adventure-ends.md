---
title:   "One adventure ends...."
date:    2012-08-21
authors:
  - spstarr
slug:    one-adventure-ends
---
It is sad to report, but I have been laid off at RIM/BlackBerry today.

I wish them all the best with Qt.

I now look for a new adventure, a new opportunity to help the Open Source community grow.

If you know of any great places that are hiring, please let me know :)

Thank you.

Shawn