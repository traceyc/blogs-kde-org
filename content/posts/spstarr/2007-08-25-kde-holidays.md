---
title:   "KDE Holidays"
date:    2007-08-25
authors:
  - spstarr
slug:    kde-holidays
---
Seems like I'm not the only one taking the KDE 'holiday' vacation days. Usually the last two weeks of August are typically quiet. I did hack up the testapplet locally to connect to the WeatherEngine, so far, it returns a list of available datasources (Ions) in a kDebug() :-). 

As I'm new to how Qt/KDE drawing / widgets work, I hope to design a dialog that will test out the dataengine fully so I can make sure there's no API issues with the Ion interface. Otherwise most of the time I've been reading other people's blogs and watching the commit changelogs on Plasma.

For all those who love screenshots or screencasts, I'm afraid there won't be one for a while yet. This project is complex and managing the datasource types is taking much longer than I had hoped. But the end result will be a very robust API for weather data.

Shawn.