---
title:   "Happy New Year! And a little present"
date:    2008-01-03
authors:
  - spstarr
slug:    happy-new-year-and-little-present
---
Well, the homestretch is near, I've had to dropped some functionality to get this finished for KDE 4.0.0. So, no Icons for this version. But, you can see It should replace most of the functionality kweather did. This applet does not support dropping onto the panel or If it does, it might not render correctly.

<b> * Update: Since this will not go into extragear, you will need to build it from the playground. Given the changes coming for KDE 4.1, it makes little sense to maintain two different versions which will diverge.</b>

I decided to just leave the name 'Weather Applet' for generic reasons. Maybe the name will change for KDE 4.1 we'll see.

Next, I have to write a HOWTO for people to write ions (data source backends) to get other weather sources from your local country (if available).

If you know of any countries that provide XML data or freely available data, do let me know. I'd be happy to write a backend to add to this list :-)

Here's a busy screenshot showing various places. Some of the data sources (ions) report warning/watch information.   Only the Environment Canada one at this time provides this information. 

Comments welcome, please note this is just for KDE 4.0.x, KDE 4.1 I will implement the features mentioned in previous blog postings. We just ran out of time folks :(

[image:3176 size="original"]