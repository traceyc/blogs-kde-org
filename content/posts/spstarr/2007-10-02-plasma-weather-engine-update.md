---
title:   "Plasma Weather Engine update"
date:    2007-10-02
authors:
  - spstarr
slug:    plasma-weather-engine-update
---
While the Plasma APIs have been in flux (and is beginning to settle down now). I went about implement displaying the wind speed in metres per second as requested by a KDE person. It converts mph to m/s or km/h to m/s. This will become a configuration option once the applet code is started.

I haven't been able provide that screenshot of the applets yet :( as mentioned above, things in Plasma have been going all over the place. There's a lot of regressions that are being worked out so I'm just focusing on the ions for now.

I will be going back to the BBC ion to see what can be changed. It's rather nasty in that parsing BBC's WAP/HTML they do not provide a static list of places whilst the other ions do have a list of places they can report weather from.