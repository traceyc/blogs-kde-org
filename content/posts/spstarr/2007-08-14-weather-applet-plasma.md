---
title:   "The Weather Applet for Plasma"
date:    2007-08-14
authors:
  - spstarr
slug:    weather-applet-plasma
---
Thanks to Chris Lee for adding me to the Planet!

First, an introduction. Hello, I'm Shawn Starr, I've been busily working on the weather engine for Plasma. The plan is to begin working on an applet as soon as some javascript bindings for the dataengine are ready to use. 

Currently, You can get weather data from Environment Canada (conditions + 5 day forecast and more) and NOAA's National Weather Service (conditions only). Work is underway with the BBC/UK MET Office ion.

You may have seen <a href="http://aseigo.blogspot.com/"> aseigo</a>'s screencast demoing the weather dataengine. 

I'd like to find out what people want in the weather engine/applet. 

For those living in Canada, the thinking was of mimicing the way The Weather Network does it's local forecast screens. Something with rotating 'screens' inside a plasmoid. Then there's the whole idea of themes. I'm open to ideas, I'd like to make the dataengine/datasource flexable for other people to write their own applets too. The more plasmoids the better :-)

Here is an overview of how the Plasma weather engine currently functions:

In order to make the engine flexable and modular and support multiple datasources (Ions). I split the concept of the dataengine and datasources. This made it easier to develop different weather datasources. For example, If you owned a USB weather device that reports data wirelessly back to a USB device you could parse the data and display it in the applet. 

I am still working out how the applet is going to determine how to handle all these types of datasources. I hope to begin testing that theory once the js bindings are usable. There should be fixed datasource key names that the applet can then determine based on which ion is loaded, the goal is to keep the datasource key names identical across all ions where the information is the same and provide the applet a way of displaying advanced weather information somehow.