---
title:   "Sometimes, you just have to do it"
date:    2008-01-08
authors:
  - spstarr
slug:    sometimes-you-just-have-do-it
---
<a href="http://technorati.com/claim/xkc4ngu5" rel="me">Technorati Profile</a>
You know, it's just in my nature. When someone falls, you help them up. When someone needs help you provide assistance. Well, the same goes in the online world. You see, ever since Aaron Seigo and Siraj Razick wrote up the CIA.vc applet, I sort of had a small crush for it. After watching it bitrot, stumble and cry for help. I thought it was time to make it smile and feel loved once again.

I bring you the updated CIA.vc applet. There are some small goals to come for this such as, adding a config dialog to display (or allow) you to type the CIA project you want to monitor and finally fix some of the rendering issues near the bottom of the plasmoid (if the current layout functionality in 4.0.x lets me).

Just remember, when you see code wallowing with sorrow and shame, cheer it up and make it grow and shine! :-)

[image:3188 size="original"]
