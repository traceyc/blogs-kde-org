---
title:   "The Fedora 9 Release party and KDE"
date:    2008-05-18
authors:
  - spstarr
slug:    fedora-9-release-party-and-kde
---
Well, a few days ago, I attended the release party for Fedora 9. There was a lot of people who showed up a few Red Hat folks who work on Fedora and packages. There were also a few users who showed up to try Fedora out for the first time. It was a big success! Thanks to all who came out

When I arrived, I was asked to demonstrate KDE for the audience. Seeing that I don't have KDE 4.0.x and that I'm using trunk ('4.1') I showed them some features in 4.1 giving them notice of the bugs they were about to encounter. All and all, the demonstration went ok otherwise. 

I have to say though, we have a lot of work to do for KDE 4.x to catch up with the GNOME folks as they have more things integrated together. Hopefully by a later KDE 4.x, we'll have more integration. We took a big risk in KDE 4.x but even though all the current pain, it will be for the better in the long run.

I'd like to also dispel FUD that I still keep hearing about KDE and Fedora. As you know, historically Red Hat/Fedora have been a GNOME shop, this remains true today, however, with Fedora 9, there is full KDE participation now. We have a KDE SIG group that maintains and charts KDE's course within Fedora. There is still some work to do to building more bridges, but I am glad to see Fedora reaching out to KDE as they have now.

Special thanks and greetings to: <a href="http://overholt.ca/wp">Andrew Overholt</a>, <a href="http://mces.blogspot.com/">Behdad Esfahbod</a> and <a href="http://bpepple.wordpress.com/">Brian Pepple</a> for making the party a success! See you for Fedora 10 :-)

[image:3471 size="original"]
The <a href="http://www.linuxcaffe.ca">LinuxCaffe</a>. Stop by for drink and food and geek fun!

[image:3470 size="original"]
Fedora people chatting and hacking

[image:3469 size="original"]
The bar area, feel free to pickup a Fedora DVD ;-)
