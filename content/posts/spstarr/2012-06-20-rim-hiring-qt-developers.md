---
title:   "RIM is hiring Qt developers!"
date:    2012-06-20
authors:
  - spstarr
slug:    rim-hiring-qt-developers
---
We're hiring Qt developers in Germany, for those who might not know, BlackBerry 10 is using Qt for our core platform with Cascades for our UI/UX framework.

Details are here: https://rim.taleo.net/careersection/professional/jobdetail.ftl?lang=en&job=256223



