---
title:   "Ontario Linux Fest 2007 and WeatherEngine changes"
date:    2007-10-14
authors:
  - spstarr
slug:    ontario-linux-fest-2007-and-weatherengine-changes
---
<h2>Ontario Linux Fest 2007</h2>

I had a good time at <a href="http://www.onlinux.ca/"> Ontario Linux Fest 2007</a>. This being the inaugural event there were some small annoyances like lack of plugs for laptops. They did have free WiFi and there was time for me to hack on the weatherengine while there. This conference has a mix of corporate and geek culture and slightly differs from OLS (Ottawa Linux Symposium) but it's likely to grow and evolve over time.

While there, I met <a href="http://www.marcelgagne.com/">Marcel Gagné</a> who presented <a href="http://aseigo.blogspot.com/">Aaron Seigo's</a> slides on KDE 4.0 upcoming features, frameworks (he had to skip some of the developmental slides as the audience level didn't appear to be too technical). He also gave a demo of an October 12th build of Plasma showing a few of the plasmoids and demoed the new KDE 4 port of kickoff, raptor and Dolphin. He didn't show the sexy new Oxygen style or window decorations though :-(

Although Marcel mentioned Plasma was still in heavy development, apparently, people just weren't paying attention. During Q&A, there were two negative comments. One being, "I hope kicker is still there, the panel looks dumbed down" and the other comment was, "Can I turn all this bloat (referring to Plasma) off?". 

With the blood boiling within me, I blurted out that kicker was going away because Plasma would be handling the panel now and to the other person I politely reminded them Plasma is still in heavy development and that things were changing rapidly.

I wish people wouldn't jump to conclusions and ridicule KDE/Qt as 'bloat' or criticize a project in such heavy development </rant with burning eyes>

I went to a few other talks/bofs and chatted with some CLUE (Canadian Linux Users Exchange) people about making sure we don't get evil things like DMCA in Canada. The conference was just one day and I won a Ruby pocket book (The prizes were on a first come first serve basis, so by the time I had won, that was the last prize left. When it comes to script languages, I'm a python guy :-)

[image:3028 size="original"]
What would a Linux conference be without posing with Tux? ;-)

[image:3027 size="original"]
And another shot

Sorry, my cell phone camera sucks, I didn't bring my digital camera.

<h2>WeatherEngine changes</h2>

A night before, Aaron helped me fix up the weatherengine and ions to be fully asynchronous. This means the backend is even faster now and much more flexable. The BBC ion plugin should be able to be finished now. I have also begun a very preliminary C++ plasmoid but there is no screenshot worthy to display yet.

I showed off the weatherengine goodness to Marcel and he wanted that info (ooohs ahhs) for any weather plasmoid he'd use :-)