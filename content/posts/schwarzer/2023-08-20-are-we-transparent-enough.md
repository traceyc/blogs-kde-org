---
title:   "Are we transparent enough?"
date:    2023-08-20
authors:
  - schwarzer
slug:    are-we-transparent-enough
---
We changed the default mouse click behaviour to double-click <a href="https://invent.kde.org/plasma/plasma-desktop/-/issues/72">for KF6</a>.
For me this was surprising, so I would like to raise a few questions here that might as well be answered with: "sure, we are", and I just did not see it because I am a bit out of touch with KDE development news (and modern ways of communication in general). Personal and biased opinion follows. But here are the questions first:

Are we handling such a prominently user-facing (as in <i>in your face</i>) changes transparently enough?

How do we decide for such (as in <i>groundbreaking</i>) new defaults?

Have the users (as in <i>many of them</i>) been asked?

<b>Update</b>: I want to stress again that I am talking about transparency towards normal users. I am aware that this has been discussed within the development groups involved for quite some time. Even though I did not stumble over those discussions then, they were happening and can be looked up now.

So, now for the personal opinion...
To me that change came a bit as a surprise both in it happening at all, as well as in the actual nature of the decision. I did not see any blog posts or mailing list topics regarding this before. I just found the already made decision at some point. And it felt weird. For me, the single-click behaviour, though surprising to users coming from Windows, has always felt like a good idea. On Windows I tended to weird-click sometimes, meaning, I tried to double-click, felt, that I would not be fast enough, cramped and then the intended double-click degraded half-way to a slightly longer single-click. So when I switched to KDE around 2004, I considered it progress when discovering the single-click behaviour. I even tried to enable it on Windows ... but since it is not the default, many apps do not really support it and either still use double-click or their work flows break. This is 15 years old knowledge of course so things might have changed since I stopped watching the Windows world with interest.

From what I see at work, Windows today uses some mixture of single and double-click depending on what you click. They also use a weird mixture of explicit Save button and auto-save depending on where you are doing your settings (as does the Web). And the world is still spinning. So people can adapt to even the weirdest behaviours.

The Web also uses single click. Back when the Web found its way into more and more households, people used to double-click on links (hyperlinks, as they were called back then). I remember that because I installed a lot of modems and Netscapes back then for non-techy friends and friends of friends and even enemies of friends. But the people adapted. These days the only one using double-click on links is your mom ... err, I mean, my mom. But she is even older than I am, so she is forgiven.

Then there are mobile platforms with their touch-based interfaces. They use single-tap. Users adapted. Not sure if there are people who would like to use double-tap to open files on their phones, but I do not know anyone who does.

So what do the people I know, think? I do not have many friends using KDE but all four of them use single click and like it. Three just switched from Windows in recent months and found it funny at first and nice shortly after. None of them chose to change the setting to double click even though I showed them how to do it.

On a side note, all of them changed the touch pad to <i>tap to click</i> but we did not change that one (<a href="https://bugs.kde.org/show_bug.cgi?id=429665">yet?</a>).

So in my little world, it does not make sense to <i>fall back</i> to the <i>ancient</i> double-click behaviour.

Of course, single-click behaviour has drawbacks. How to gracefully select a file would be the elephant in the room, I guess. The touch-interfacy long tap would probably feel weird (and hard to discover, unless you are cramp-clicking like me sometimes). So do the currently in-place select marker and the rubber band. Everyone hates at least one of them and more or less happily uses the other. But people adapt and go on with their lives. ... And so we will do with the new double-click behaviour in KDE. It just feels weird to me because single-click felt like progress and the recent change feels  like going back to spinning drives again instead.

Bonus question: will the select marker be disabled by default now since it does not make sense to have it? And will said marker be enabled automatically if people switch to single-click behaviour? Might feel weird without having it then.

Anyhow, enough with the <a href="https://tvtropes.org/pmwiki/pmwiki.php/Main/GrumpyOldMan">old man yelling at cloud</a>.