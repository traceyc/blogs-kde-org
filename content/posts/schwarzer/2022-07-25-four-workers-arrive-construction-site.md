---
title:   "Four workers arrive at a construction site ..."
date:    2022-07-25
authors:
  - schwarzer
slug:    four-workers-arrive-construction-site
---
Four workers arrive at a construction site to help. They each take a shovel and are eager to start shoveling. But what is that? They notice some dents and some dried cement on the shovels. So what now?

<b>Worker 1</b>

The first worker shrugs and starts shoveling. Better to work with 75% efficiency than not to work at all. Work needs to be done after all.

<b>Worker 2</b>

Another worker takes some time to look for a hammer and frees the shovel from the hardened cement and even flattens some of the dents. After two hours of fixing the tools this worker is able to work with 85% efficiency.

<b>Worker 3</b>

Then there is one worker who says: "Skrew it, I do not like how work is being done here so I will look for another construction site." This worker spends three hours looking for a construction site with new and shiny shovels and is able to work the rest of the day with 90% efficiency. Why not 100%? Well, every construction site has its own problems.

<b>Worker 4</b>

Meanwhile the last worker is busy waving around the shovel and complaining to everyone about the broken tools they have to use. This worker constantly demands a new shovel even though another worker made it clear that there would be no newer shovels any time soon and that nothing could change that. At the end of the day the efficiency of this approach lies at -160%. Not only was no work done by this worker because of all the waving and complaining. Furthermore, the constant complaining kept several other people busy who would have been able to do some work otherwise.

Dear contributors, please don't be Worker 4.