---
title:   "Video Recommendation: Coping with Other People's Code - Laura Savino"
date:    2023-12-26
authors:
  - schwarzer
slug:    video-recommendation-coping-other-peoples-code-laura-savino
---
As someone suffering from a latent burnout thingy which has become more imminent in recent years and as someone who is still struggling to develop strategies to alleviate its effects on health and general well-being, I wholeheartedly recommend everyone to watch this video and let those points sink. Yes, even if you are not (yet) affected. The video is not all about burnout but about strategies for sustaining long-term sanity.

<a href="https://www.youtube.com/watch?v=qyz6sOVON68">https://www.youtube.com/watch?v=qyz6sOVON68</a>

Enjoy. :)