---
title:   "Zoom widgets"
date:    2006-11-07
authors:
  - cristian tibirna
slug:    zoom-widgets
---
In a <a href="http://tsdgeos.blogspot.com/2006/11/67-vs-667.html">small poll (or sorta)</a>, Albert asks what kind of values are better in the zoom combobox for oKular.

IMO, zoom is best represented by either:
- a slider with major ticks in almost logarithmic scale, going from 10% to (wishful thinking) 10000%, with a spinbox or constrained textedit next to it to show the slider value and allow manual editing
- a zoom out button and a zoom in button (with the classical icons and obeying bidirectional gui guidelines) with a constrained textedit inbetween. Once again, zoom buttons should increase/decrease zoom value almost logarithmically (1, 2, 5, 10, 20, 50, 100, 200, 500, 1000% etc.)

IN order for these widgets to take lesser space on toolbars, they can be actually hidden and popped up from an unique zoom button ("lens" icon). AmiPro was doing this stuff perfectly 15 years ago.

BTW, such a zoom widget should go in kdelibs and be strongly recommended by the programming guidelines.

Comboboxes are a poor choice (aesthetically and usability wise) for zoom widgets.
<!--break-->
