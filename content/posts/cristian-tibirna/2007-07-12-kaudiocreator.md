---
title:   "kAudioCreator"
date:    2007-07-12
authors:
  - cristian tibirna
slug:    kaudiocreator
---
It's quite long since I want to write about this:

I used to use the audiocd: ioslave to rip my CDs. But since almost a year, I started to constantly use <a href="http://www.icefox.net/programs/?program=KAudioCreator">kAudioCreator</a>. Nifty little tool! Does one thing and does it extremely well. Congrats Benjamin. Hope you keep this one up with KDE4.

Now, I am partisan to Ogg, so I ripped all my 300+ CD collection to this wonderful format, using kAudioCreator. But, of course, not all can be rosy. Or I wouldn't have <a href="http://tibirna.blogspot.com/2007/07/stupid-power-of-majority.html">what to procrastinate about</a>.
<!--break-->