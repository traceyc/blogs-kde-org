---
title:   "OpenPrinting summit in Montréal"
date:    2007-09-24
authors:
  - cristian tibirna
slug:    openprinting-summit-montréal
---
The <a href="http://www.linux-foundation.org/">Linux Foundation</a> organizes this year's <a href="http://www.linux-foundation.org/en/OpenPrinting/SummitMontreal">OpenPrinting Summit in Montréal</a>.

I will be there on Monday and Tuesday, trying to represent KDE.