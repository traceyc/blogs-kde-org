---
title:   "(KDE and FreeNX) Better together"
date:    2005-06-21
authors:
  - cristian tibirna
slug:    kde-and-freenx-better-together
---
A few days ago, <a href="http://www.ukuug.org/bios+profiles/KPfeifle.shtml">Kurt</a> <a href="https://blogs.kde.org/blog/418">Pfeifle</a> very kindly offered me the opportunity to test <a href="http://nomachine.com/products.php">NX</a>. I'm an old guy and I thoroughly dislike buzzwords, but sometimes it's simply no way around particular ones. The expression I'm itching to use here is <b>disruptive technology</b>.
<!--break-->

The drive test, that Kurt so nicely provided, simply left me in awe. I connected, <i>from my home in Canada, through a DSL link, to a server located somewhere in Europe</i>. A full HEAD KDE session (on X). With almost <b>no noticeable slowdown</b>. It simply worked marvelously. Remark that we don't speak VNC here. Right now, while I slowly build this entry, I'm connected in a full X session, at a resolution of 1152x864 (the size of my local screen). I browse kdedevelopers.org in konqueror. I play with the latest versions of different KDE-Edu applications (as I need kdissert and a few others for the school of my son). It is thoroughly awesome. 

Each time I do this "let's play over there in Europe for a while" , I consistently start daydreaming. NX supports:
* session connection suspension and reactivation, AKA detaching/attaching (optionally from a different terminal/client),
* full X sessions,
* launching autonomous applications,
* VNC and RDP,
* CUPS printing forwarding,
* sound forwarding,
all this with intelligently compressed piping, so that running an X session from a machine 2 meters or 14000 kilometers away is indistinguishable. Imagine working on the same X session at work and at home. No more need for the meager (x)emacs/gnuclient patchwork, goodbye to text only screen session detaching and attaching OK, we can now all close our mouths.

The utter beauty of all that I preach here is that NoMachine, the company behind NX, publishes and develops the <a href="http://nomachine.com/ar/view.php?ar_id=AR10B00018">core components and protocol under the GPL</a> (of course, otherwise I wouldn't waste your time).

Kurt actually sounds the hunting horns on NX and <a href="http://developer.berlios.de/projects/freenx/">FreeNX</a> since quite a while. If you want a more detailed description, look at his <a href="http://conference2004.kde.org/cfp-devconf/kurt.pfeifle-nxaccelerateskdedev.php">aKademy-2004 presentation</a>. I am quite puzzled that people are slow to get involved.

Of course, FreeNX needs love. And where things could really get amazing, is on the front of integration of FreeNX with KDE. There is already some activity, in the form of kNX, but much work is still to be done:

* seamless integration of FreeNX client in KRDC;
* kiosk automatically comes to mind as it's a perfect match for remote terminals (think schools and universities): further streamlining the configuration and probably autodetection+autoconfiguration (based on knowledge of the amount of resources needed for the current NX connection);
* automatization of CUPS/KPrinter integration (so that user's local printers can be neatly displayed in the KPrinter that runs remotely, in addition to or instead of the remote ones)
* thorough optimization of the multimedia packets flow (especially sound, duplex please, for the benefit of applications like VoIP);
* make sure that the results of the current <a href="http://blogs.kde.org/node/view/978">excellent work on the X rendering techologies</a>, by KDE's own Zack Rusin work OK with FreeNX;
* develop thorough support for streamlined applications, in the vein of <a href="http://www.trolltech.com/products/qtopia/index.html">Qtopia</a> and <a href="http://opie.handhelds.org/cgi-bin/moin.cgi/">OPIE</a>, so that thin clients can connect to a FreeNX server and be served specific applications;
* and much more, as sky is the limit.

Please, just stop for a second and imagine how would you feel being on your daily bus or train ride and have your favorite portable device connected at home and seamlessly work in the session you just suspended a few minutes ago on your desk machine. Yummy, eh? I can't wait to have it happen. And you?

<small>Post scriptum: I thank to <a href="http://www.jackjohnsonmusic.com">Jack Johnson</a> for the idea of today's title (respectfully borrowed from the first piece on his last album). Johnson's music makes my summer.</small>