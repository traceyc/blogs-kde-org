---
title:   "The pinnacle of usability"
date:    2005-09-16
authors:
  - cristian tibirna
slug:    pinnacle-usability
---
Just saw a Yes/No dialog (in KDE, I specify, to avoid confusion) with tooltips installed on the Yes and No pushbuttons. The tooltips read "Yes" and "No" respectively. I'm flabbergasted!