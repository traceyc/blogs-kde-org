---
title:   "LinuxWorldExpo San Francisco 2005 - day 3"
date:    2005-08-15
authors:
  - cristian tibirna
slug:    linuxworldexpo-san-francisco-2005-day-3
---
After describing <a href="http://blogs.kde.org/node/1311">how I traveled to San Francisco</a> and the <a href="http://blogs.kde.org/node/1313">first</a> and <a href="http://blogs.kde.org/node/1324">second</a> days at the LinuxWorldExpo, here comes a brief synopsis of the last day on the Linuxprinting.org and KDE booths.
<!--break-->

Before starting, I should mention a fact that amused and cheered many at the end of the <a href="http://blogs.kde.org/node/1324">day 2</a>, about which I forgot to write yesterday. As I said, the second day seemed a bit slower than the first. Towards the end of the day, Nathan Krause had the idea of organizing a shout-out in which passers-by would express their attachement to KDE as loud as they could and in exchange they would receive from the KDE booth staffers a Xandros Linux CDs box. It all ended up with some startling shouting and and cheering, with loud "I love KDE"s resounding throughout the second floor. One of the <a href="http://www.snort.org">SNORT</a> guys, who had the booth just across from KDE's, proved to have one mightily handsome set of lungs and vocal cords and won indisputably.

Now on to day three. <i>The morning:</i>
It all starts the same, once again, with a very enjoyable breakfast in four. The difference is that I didn't manage to get the necessary 90 minutes to write my second day report before the breakfast either. Immediately after, we gather at the booth. The day announces itself even slower than the past one, in what concerns the number of visitors. I thus take the occasion to speak in more details the things that George identified as needed in the KDE printing technology.

Uli visits us for a while, although his plane is in just a couple of hours. I would say he has difficulties to depart from the group :-).

Kurt arranges a demonstration of FreeNX for <a href="http://foundation.gnome.org/about/">Timothy Ney</a>, the executive director of the Gnome foundation, and invites me to join them. Unfortunately the wireless networking (just as the .org pavilion wired networking) at LWE is particularly bad, with dropped packets and very slow links. Kurt will tell me later that he managed to secure a better link in one of the booths downstairs and was able to show to Tim how blazing fast a graphical desktop can be relayed through FreeNX from Germany to San Francisco. Mr. Ney is a very well composed person, enjoyable to speak to. We decide to meet later at the booths for a brief exchange of ideas but unfortunately the various (usual) happenings of the day deny us any such occasion.

From time to time, I go see if I can help Charles at the KDE booth. Today he is accompanied by Jason Katz-Brown and Nathan Krause once again. Almost all SimplyMepis CDs are already given away. The day before, a person from HP came by and offered to us to install a really small piece of HP hardware (and a huge LCD screen) to serve as an <a href="http://www.ltsp.org">LTSP</a> graphical terminal. We thus have for the most part of yesterday and for today a <a href="http://www.kubuntu.org">Kubuntu</a> installation. Many people are inquiring us about Kubuntu and curiosity seems to be high on this topic.

Eivind Throndsen of <a href="http://trolltech.com">Trolltech</a> stops by with a bag full of troll t-shirts, cool black ones with the "Code less. Create more" slogan on the front. I didn't have one in the end ;-). Eivind is a really nice person and we discuss a bit about Trolltech, Qt, KDE and the show. Seems things are working well for TT in the downstairs expo. According to Eivind, people are looking for crossplatform building tools. I can confirm this, since we also had a couple of people asking for KDE's skills in crossplatform development. I had to explain to those people that KDE is a desktop environment and doesn't specifically deal with this development topic in particular, but KDevelop can be easily configured to help with what they want. Maybe an idea for the KDevelop team.

Charles and the others go then to have lunch and I stay at the KDE booth to replace them. This gives me about one and a half hour of direct exposure to visitors. Some of the most interesting visits I describe shortly here. 

Ken Kameda, from Adobe (don't remember his title, something related to directing software development), comes and asks me quite a few questions about KDE <i>development</i>. I demo to him dcop, I explain the signals-slots mechanism, I describe different frameworks that we use (libraries, configuration, UI design and so on). We spend together more than half of an hour. I just hope I incited his curiosity enough that he will take a thorough second look at KDE's development goodies.

Quite a few people, both coming at the linuxprinting booth and at the KDE booth, expressed interest for color management and calibration. I think I will have to see what we can do about this in the KDE printing infrastructure.

A systems administrator for a nearby university (I think) expresses concern about the huge NFS traffic generated by konqueror on RedHat when browsing NFS shares. I tell him about fam and about the fact that work has already been done to fix this in the KDE side, thus it would be a good idea to check what RedHat does wrong about this in its KDE rpms.

Benjamin Reed from <a href="http://ibm.com">IBM</a> is the second person with which I have a quite lenghty discussion today. He starts by inquiring about <a href="http://meanwhile.sourceforge.net/plugins/">meanwhile</a> support in <a href="http://kopete.kde.org">Kopete</a>, a tool that he uses to replace Lotus Sametime. He wants to know if the plugin for this protocol would be included in the official Kopete distribution, so that it doesn't have to suffer from asynchronizations when a new version of Kopete gets released. I direct him to the kopete mailing lists. Benjamin also complains about a javascript bug he reported to our bugzilla, which went unanswered for more than a year. Arts is another concern for him. I tell him about plans to abstractize the sound platform in KDE 4. Another we discuss is the user switching. He argues that the current mechanism is not satisfactory. And he points out that kdm should deny the same user to open two sessions on the same machine, since this breaks in ugly ways the applications (because of shared config resources and file locking issues). I can only agree with him. This is an issue to be taken care of, user switching could be much easier than what we have now.

<i>The afternoon:</i>
When Charles comes back, I take advantage of the slowness of the show and, with the kind accord of my colleagues from the linuxprinting.org booth, I go pay a visit to the city of San Francisco for the first time.

When I come back near the closing time, I find Charles in the last preparatives for closing the booth. The big boxes for the large Ricoh printer are also parked aside the linuxprinting booth. We pass a few more time together removing the stuff from the booth and take some last photos, then decide to meet for the farewell dinner at 19h00.

I then go with Till Kamppeter on a one hour photo shooting spree along the Market street, towards the Port of San Francisco. This is a really handsome city and I'm glad I was given the possibility to visit it.

<i>The evening:</i>
We meet around 19h00 for the dinner. We go to the Oriental Pearl in the Chinatown. The party is formed of Till, Kurt, David Suffield (from HP) and his wife, Shiyun Yie (HP) and me. This is an excellent restaurant. We have a very constructive discussion during the particularly good service. We debate about shared corporate plus open source code management solutions. The HP Linux printing group is interested in knowing the best practices related to this. We also discuss open source and Free software politics, the positioning of KDE on the desktop and draw some colorful predictions about the future of computing ;-). This is a nice friendly corolary to this enriching and pleasant trip.

We walk back to the hotel and say "Good bye" for what might be a long time. I then go to sleep asap, a flight awaits me in less than 10 hours.
