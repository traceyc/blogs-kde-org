---
title:   "KDEPrint JJ (n.b. JJ=junior jobs)"
date:    2007-07-14
authors:
  - cristian tibirna
slug:    kdeprint-jj-nb-jjjunior-jobs
---
In order to heal some of my <a href="http://tibirna.blogspot.com/2007/07/insure-my-car-wait-better-tear-all-my.html">procrastination</a>-induced bruises, I decided to do a little social experiment, that tends to prove to myself (once again) that we, humans, are inherently good, only sometimes misguided (read below for the excitement).

So, my experiment is this: I will post here the current 4 JJ (junior-job) <b>bugs</b> (not wishes) at KDEPrint's bugzilla. Let's have some sort of soft bet, how much time will be required, from the moment of this post, for getting them fixed by eager future KDE heroes: 1 day? 10 days? 3 months? never?

Here are the JJs now:

<a href="http://bugs.kde.org/show_bug.cgi?id=51991">51991</a>
<a href="http://bugs.kde.org/show_bug.cgi?id=55072">55072</a>
<a href="http://bugs.kde.org/show_bug.cgi?id=117900">117900</a>
<a href="http://bugs.kde.org/show_bug.cgi?id=145016">145016</a>

If you're one of the young wolves and you're now decided on your pray (see above), feel free (and yes, invited) to ask me for support.
