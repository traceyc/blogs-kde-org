---
title:   "LinuxWorldExpo San Francisco 2005 - day 0"
date:    2005-08-10
authors:
  - cristian tibirna
slug:    linuxworldexpo-san-francisco-2005-day-0
---
Yes, I'm in SF now. Since yesterday. Which was day zero. Here is how it went:
<!--break-->

4h00 - After 4h of sleep (at the end of a hectic week with only 5h sleep/night), I leave Quebec towards Montreal to get the plane from there. The plane was at 9h10. The ride is a 2h30 one. Usually. 

6h10 - 20km before Mtl, I get into the largest highway jam I've ever seen. 

7h15 - last 3km took 50 minutes. I finally got off the highway somewhere near Mtl, 1 hour left to catch the plane.

8h10 - finally in the parking lot of the airport. Walking to departures is (of course :-(  ) blocked by construction sites. Big signs everywhere say I need to take a shuttle bus. Which comes 10 minutes later. 

8h25 - I finally run like mad to the checkin counter. But... I get refused the boarding, on reason I was too late. I get replacement flights but I won't be at 12h00 in SF as planned, to help with the boot. And I don't get a direct flight anymore either. My original flight leaves without me, 20 minutes <b>after</b> I already have replacements for it :-(

10h00 - takeoff to Toronto. I'm there at 11h20 and then get the flight to SF and finally land there at 15h10 local time, where I wait one freakin' hour for the stupid luggage to be delivered to the carousel.

16h40 - checkin at the nice hotel in which George Liu from <a href="http://ricoh-usa.com">Ricoh</a> so kindly reserved a room for me.

16h50 - I'm finally at the booth of <a href="http://linuxprinting.org">Linuxprinting.org</a>. George, David Suffield (from <a href="/hp.com">HP</a>), Patrick Powell (<a href="http://www.lprng.com/">LPRng</a>) and Till Kamppeter (Foomatic, linuxprinting.org) are already there (photos when I get back in Qc.). The .org pavilion is on the second floor in the Moscone Center (the Moscone West building). I'm already wondering how will people find the .org booths, given that no visible sign indicates its presence in the main lobby. There are quite a few equipments on the booth already: computers, screens, lots of cables, and three impressive printers (I'll get a list of the models tomorrow, don't want to screw their names). George and Patrick are configuring one of the printers to print/fold/stapple a booklet about linuxprinting.org.

17h30 - back to the hotel to refresh myself a bit. I'm already starting to sleep stading :-)

19h00 - With Patrick, George, Till, and now Hitoshi Sekine (Ricoh) and Uli Wehner (<a href="http://www.lanier.com">Lanier</a>), we go to a really nice dinner offered by Ricoh. Time to better know everybody and to plan a bit more how things should be done on the booth the next days. Charles Samuels joined us for the dinner too. This was the first time I was meeting him. Good to finally put a real face and a handshake on the name. Kurt Pfeifle came a few minutes later. We didn't need presentations either ;-) 

22h30 - finally in bed and sleeping sound. It was a full day. Starting bad but finishing good. That's all that matters.

Follows day 1: the main work begins.