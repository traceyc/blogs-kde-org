---
title:   "bad at weaving?"
date:    2004-04-23
authors:
  - cristian tibirna
slug:    bad-weaving
---
I was reading the <a href="http://www.linuxjournal.com/article.php?sid=7541">PyCon presentation at LJ</a> (very interesting stuff) and got idly browsing the very cool <a href="http://www.sauria.com/~twl/conferences/pycon2004/">SubEthaEdit notes</a> (I will probably comment at some other time on my fascination with Python and about the enticing principles behind SubEthaEdit).

What I want to mention here is that one of the presentations at PyCon caught my eye: "GTK and Gnome" (on second day). As much as I hate to admit it, my pride started to itch. I would really like to be able to say wholeheartedly that I'm maximally open-minded with respect to all this gnome/kde stuff/flamewar/constructive-rivalry, but apparently I'm not. Well, I definitely rationalize about it, but my visceral/emotional reactions are quite low leveled.
<!--break-->

Anyways, what got me thinking is that, hey, the KDE developers have a very good relation Python. And there are quite nice tools in KDE making use of Python or augmenting Python. But we didn't manage to publicize that at PyCon. And of course, this is bad (at least IMHO)! Lack of publicity! Neglect for the awareness the outer world has about our work/project/general-doings.

And then I tried to think about reasons. I'm far from having extracted a conclusion, but an observation is obvious: we're by far not as good as needed at larger-community weaving. I bet few of those living in the KDE/Python intersection realized that PyCon would be a nice place of publicity (healthily combined with learning and gaining conferencing experience). And of those, even fewer (none) had the time/energy/funding/volition to actually go there and do a presentation.

That's the observation. Now, the causes and the effects remain to be assessed.