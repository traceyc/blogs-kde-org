---
title:   "SQIL-tested"
date:    2005-11-20
authors:
  - cristian tibirna
slug:    sqil-tested
---
Well, <a href="http://sqil.info">SQIL</a> came, has been, and passed. As <a href="http://blogs.kde.org/node/1618">I was saying</a>, I <a href="http://giref.ulaval.ca/~ctibirna/play/trucs_astuces/">presented KDE</a> yesterday afternoon at the Laval University chapter of this pan-provincial manifestation.

I dare say all went well. The assistance was not numerous, but despite my inept management of presentation time (which made me do the planned 1h presentation take in fact 85 minutes), nobody left (on the contrary) and, above all, I've got 40 minutes of questions afterwards.

It was quite the occasion to meet persons interested in Linux in general and in KDE in particular. Unfortunately, my decision to go less active in 2002 preceded by just a few months a refervescence of Linux-centered activity in Québec. Thus, my come-back at the beginning of 2005 meant a need to re-connect with a grown and evolved local community. It's a slow process but it progresses well.

Now for the happenings of the presentation itself. Firstly, I don't own a laptop. Thus, I had to accomodate my bits of data on a laptop very kindly provided by the local LUG, <a href="http://linuq.org">LINUQ</a>. 

The first hoop: I've made my presentation with KPresenter "1.4 post" (actually the SVN head) and the file content proved incompatible with the KPresenter 1.4.1 that was installed on the borrowed laptop. Luckily I had prepared a HTML export. But of course, some of the punches were lost this way.

During the presentation, I needed to look for the Window Behavior module in KControl. I needed the "Advanced" tab from that module to demonstrate the active desktop borders. Well, the hick was that the KDE on the borrowed laptop was configured to run in French. For about 45 embarrassing seconds, I searched absolutely lost in KControl and found nothing. Searching for "window", "fenêtre", "kwin", "active", "actif", "border", "bord" gave nothing either. The fast search in KControl is utterly useless. Well, if all would go well, this world would be really boring, I guess. I am convinced now, though, that most than anything, we need to brainstorm, regulate, reprogram and user-test a new centralized configuration paradigm.

A person from the assistance asked when will we have an interface for manipulating PDFs <i>à la</i> <a href="http://www.pdfhacks.com/pdftk/">pdftk</a>. I think this is a very nice project for a new developer that wants to make her hand with KDE. I promise tutoring if somebody gets interested.

Other questions regarded using gecko with Konqueror and whether we plan on a copycat of "Looking Glass".

All in all, I found the experience interesting and refreshing (despite the rather heavy toll it seemed to have taken on my energy levels yestarday).

I need to thank to <a href="http://linuq.org">the organizers</a> for having me invited. I hope to be able to do more like this in the near future.
<!--break-->