---
title:   "Finally woken (again)"
date:    2005-05-19
authors:
  - cristian tibirna
slug:    finally-woken-again
---
After 7 years with my own (almost famous ;-)) build scripts and almost a year with kde-build, I switched today to kdesvn-build (many thanks, mpyne). It's definitely worth it, be it only for the colored output and for the progression notification. And ah!, I also switched to unsermake.

The other thing that pushed me was the strange (and long) series of failed trials to build with kde-build since the switch to svn. The svn update (that used a svn switch command in kde-build) was failing for half of the modules each time, the configuration was often screwed, in brief, I wasn't able to build for more than 2 weeks. Too long. I don't think it's kde-build's fault, mind you. It's probably just my bad luck (and lack of knowledge and debugging willingness ;-)). Now I'm a happy camper. Yes, give kdesvn-build a try. It also comes with <a href="http://grammarian.homelinux.net/kdesvn-build/doc.html">nice documentation</a>.

On another order of ideas, I (finally) bought (last week) 512M more memory for my embattled PIV-1.8GHz, 3 years old, and I'm very satisfied about this little investment. Now I can compile kdelibs and write stupid blogs at the same time, finally.

Concerning the title, it's a nod to <a href="http://jem-music.net">Jem</a>. I really like her "Finally woken" album. Go try it (it's streamed on her website) if you've come to like the british trip-pop kind of music.