---
title:   "Transmeta?"
date:    2005-08-19
authors:
  - cristian tibirna
slug:    transmeta
---
It is quite some time already since I think about getting myself some powerful PDA or a very light laptop. My boss at work wants a lightweight laptop for our research group too. The 4.7kg Dell Inspiron that we bought 4 years ago starts to show its age.

But I can't decide, mostly because of lack of info. For the laptop, I'm oscillating between IBM's X series, Sony's Vaio. PDAs are a much bigger headache.

The work machine will need at least 512M RAM (for running some demo FEM simulations with our rather large code). My personal machine will need to be able to occasionally compile bits of KDE. So, once again at least 512M RAM and a preferably snappy CPU.

While being in reflective mode, I happened over a few puzzling facts. 

1) At LWE-SF, last week, HP kindly borrowed to the KDE booth a thin client that was running LTSP connected to an Ubuntu server. Charles and I were curious what this really small puppy could run (only the cpu/memory/net/video unit was small, the TFT display was <i>big</i>, yay!). To our suprise, we managed to find out it was ... a Transmeta processor. Don't remember which one in particular, unfortunately.

2) While reading August's LinuxJournal, I fell on an EmperorLinux advertising that shows a laptop model made by Sharp and which runs Transmeta's 1.6GHz processor. 1.4kg!

Intriguing. Now, for the thin client, I can understand. But for the laptop? First, I thought Transmeta was out of play. Second, how do these processors compare with Celerons or with Pentium Ms? What is the viability of buying such a machine? 

I wish I could testdrive one of these...