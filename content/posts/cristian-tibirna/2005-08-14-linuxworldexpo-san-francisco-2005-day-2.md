---
title:   "LinuxWorldExpo San Francisco 2005 - day 2"
date:    2005-08-14
authors:
  - cristian tibirna
slug:    linuxworldexpo-san-francisco-2005-day-2
---
Now I'm back at home, after a very well filled-up days in SF then in Québec. I didn't manage to write down about the happenings in SF while still there and I'm a bit whisked about that. But hey, life is like that.

If you didn't read the first two parts of the story, go now <a href="http://blogs.kde.org/node/1311">here for day 0</a> and <a href="http://blogs.kde.org/node/1313">here for day 1</a>. (Then come back and) read about day 2 in what follows.
<!--break-->

<i>The morning:</i>
The second day starts almost the same like the first, with me writing past day's weblog and with an excellent early breakfast in four (Till, Uli, Kurt and me). About writing the weblog, I should mention that I don't own a laptop. Knowing this, George Liu insisted to pass me <i>his</i> titanium PowerBook for the duration of my stay. This eased quite a lot my work. 

After breakfast, we head to the expo floor. People start to move and stop by quite early. I think the keynote (or some speaches) were at 8h15 in this day. The visitors of the linuxprinting.org booth are quite neatly divided in three categories:
<ul>
<li>the "what is linuxpriting about" ones</li>
<li>the ones with "I have a little problem with printing from my linux box"</li>
<li>and the developers, which are either interested in meeting one of the printing maintainers or wanting to hear about the state of a particular development.</li>
</ul>

Till and George are very active at making the printers do things and trying different configurations and options. I promised in "day 1" log to list the printers we have in the booth. Here they are:
<ul>
<li>a <a href="">Ricoh Aficio CL7300</a> color laser printer with many functionality</li>
<li>a <a href="">Ricoh Aficio AP410N</a> b/w laser printer</li>
<li>a <a href="">HP OfficeJet7400</a> multifunction inkjet printer</li>
</ul>

George Liu and Hitoshi Sekine provided us with the Ricoh printers and David Suffield with the HP one. Till also has his personal portable HP printer but he chooses not to use it, given the bigger one that is on the booth. On the first day, Raph Levine had come with his Epson 2200 printer and printed many magnificent photos, with perfect color matching. These photo prints made many people exclaim "wow" during the three days.

On the KDE booth, Charles Samuels is as usual there. While in the first day he was accompanied by Nathan Krause and David Johnson, now he is with Jason Katz-Brown and with Nathan Krause. SimplyMepis 3.3.2 boot CDs made their appearance on the booth.

At dinner time, Patrick Powell, which came in the morning with a big unidentified package in his hands, reavealed to us the largest smoked meat sandwich I ever saw (but hey, I'm a canadian, not an US-ian ;-)) which he cuts in reasonable chunks, and thus makes lunch enough for 10 hungry people.

After taking in all this sheer energy, I go replace Charles and the others at the KDE booth so that they can go eat too. I remark that there are now Mepis t-shirts, thanks to the people from (tada) Mepis. Also, there area few boxes with different Xandros Linux distributions, which were very kindly donated to us by the Xandros guys, and that we can give as gifts to visitors. 

During my 1h continuous stay at the KDE booth, many visitors stoped by but maybe not as many as I would have thought. And we had a <i>busy</i> booth. Imagine the others. I think that this lack of affluence is due to the fact the the .org pavilion is on the second floor of the Moscone West expo building and there are no explicit signs downstairs that we're up here. Plus, they changed the senses of the escalators and today (compared to yesterday) you have to do a large detour before finding how to get to the upper levels. 

Visitors are of various Linux and KDE knowledge. Most of them have very nice words about different parts of our work. We display KPDF with some math formulae into it and I get quite some questions about doing math rendering with KDE. I mention Kile and KFormula. I didn't have any more "KDE is too bloated" events ;-)

During the morning, the kind Xandros guys have brought quite a few copies of the <a href="http://nostarch.com/lme.htm">Linux Made Easy</a> book, authored by Rickford Grant, published by No Starch Press and based on Xandros 3. While I am on the KDE booth, one of the Xandros representatives comes with the author and offers to autograph a few of the books we've got. Mr. Grant is a very kind gentleman. He signs (with different dedications!) about 10 books then we go on a discussion about how he came to Linux and about how he likes KDE.

<i>The afternoon:</i>
Charles is here so I go back to the linuxprinting booth. Kurt is away for his press point about the release of the latest FreeNX. Patrick comes from some impromptu meeting he had in the lower halls and he's quite upset. Seems he encountered somebody involved with professional printing which discarded Linux as a professional printing OS because of a professional level feature missing in CUPS. Unfortunately I forgot what was it about. But Patrick was quite furious, since he says LPRng supports that exact feature but people don't choose the right tool for the job at hand. I think I will have to look into KDE printing's support for different backends, to make sure we offer access to the tools available, as much as possible.

Early in the afternoon we, at the linuxprinting booth, get the visit of Kevin La Rue, vp of marketing at Linspire and Randy Linnell, vp of business devel at the same company. They are in a quite good mood and start to ask questions about what linuxpriting.org does and want to do in the future, about envisioned development and so on. I am a bit puzzled by such an approach; they behaved as if they are complete newbies to Linux. Yet, I manage to direct the discussion towards the need of better communication between Linspire and the Linux and KDE communities. They promise to help us with user feedback, for example about printing or about the KDE priting tools. They mention that they use KDE Printer Manager for managing printers. This is great. A discussion ensues about this wizard asking too many questions and about how drakeprint, the Mandriva tool, is much simplified in this respect. Yet another idea of future improvement to take note of.

All this day was quite full. I was particularly impressed how well we all got along. George and Uli have done the apology of Linux and how it supports printing to all visitors willing to listen. It is great to see how Linux passes the frontiers of corporations and communities and brings us all together for constructive colaboration.

<i>The evening:</i>
Patrick Powell proposes that we go together for the dinner. He calls his son for a restaurant recommendation and, after some googling, we decide to meet in the hotel lobby after 10 minutes, get his car and go to the italian Figaro, which is somewhere around Columbus boulevard in downtown San Francisco. We are five: Patrick, Till, Kurt, Uli and me. This is the last dinner we will have with Patrick and Uli, as they are set to leave on the third day (tomorrow) in the early morning.

When we meet in the lobby, we're six, as Charles is invited too. We <i>all</i> take Patrick's Civic to the restaurant. A very interesting ride, peppered with jokes ;-). The dinner goes quite well, and we even manage to exchange some more ideas, despite the loud but very skilled jazz band which plays just centimeters from us. We return to the hotel in the same happy formula and Patrick unloads us all quickly at a street corner near Market and 4th, as he wouldn't find any free parking spot so that we can say proper farewells to him. Patrick, if you read this, thank you very much for the very insightful pleasure of meeting you. Hope to see you soon.

It is already 23h00 and, although no alcohol involved (I don't drink), the heavy day and the loud dinner make me dizzy. Jet lag plays too probably. Thus I go to bed almost immediately. This is why I write this weblog only 4 days later.