---
title:   "Back and bracing myself"
date:    2005-05-25
authors:
  - cristian tibirna
slug:    back-and-bracing-myself
---
It's "official" now, I'm back to more than lurking in the KDE virtual world. I reactivated today, after almost a year of interruption, the mail flood from cvs and kwin. I also set myself a cap of minimum 1h and a max of 2h per day for coding and manual reading.

Let's hope I'll be able to back Lubos a bit with the ailing kwin b.k.o. wish tracking, as he desired earlier on a channel somewhere.
<!--break-->

I must say that I can understand now the reticence of the reticent kind of new aspiring developers when wanting to join KDE development. The codebase is HUGE. The quantity of technologies to learn and master could prove difficult to manage. Requires courage. Perhaps a good filter. But this doesn't mean there aren't things to do to lower the barrier. Or at least to try keep it steady, as it climbs constantly right now.