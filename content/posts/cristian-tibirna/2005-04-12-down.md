---
title:   "Down"
date:    2005-04-12
authors:
  - cristian tibirna
slug:    down
---
My general mood is _down_. I'd like a "Personal Rant" category here...

I am now compiling for the last time the KDE CVS HEAD source. No, I don't intend to kill myself. It's just that I'll switch to svn as everybody else.

Speaking of which, I have some sort of paralysis feeling when thinking of the CVS-SVN switch. I'm in awe with respect to the brave people who undertook all the work needed. I just wish the best to them. And, most importantly, that, _if_ there are problems, they don't declare themselves after many days of SVN usage, but rather in the first hours (which has, btw and unfortunately, a very low probability).

Back to my compile. It runs for more than 18 hours already. I postpone putting more memory in this machine (256M now, yeah I know... ) since more than a year already. The compilation has become a burden to such a point that I found myself avoiding to start development sessions because I knew I'd have to recompile and it would take many hours.

I'm not always down, though. Some 10 days ago, I had enough energy to undergo the learning of the basics of Ruby. Just so that I can give myself the tools for an eventual application development comeback, but using Korundum. Yeah, I know about PyKDE, but I really need to learn something new once in a (short) while.

What keeps my morale on _down_ mostly, perhaps, is the fact that I realized recently that I accepted the role of speechless/powerless spectator on discussions like moving to SVN, configuration systems etc.

But hey, the spring is near. Maybe I manage to get myself a big kick in the backside. I promise to let all know then too ;-)