---
title:   "Linux in government: CLLAP 2006"
date:    2006-05-17
authors:
  - cristian tibirna
slug:    linux-government-cllap-2006
---
Some time ago, I got a personal invitation to represent KDE with a booth at <a href="http://www.cllap.qc.ca">CLLAP</a> on May 23-24 2006. Altough this event happily intersects with my work and my (already legendary) lack of personal time, I decided to give it a go (read more...)
<!--break-->

It is my firm conviction that our work rhymes best with government missions and I still naïvely dream that only lack of better information/knowledge keeps governments from literally flooding us (FOSS creators) with requests and offers of collaboration.

Knowing that I wouldn't be able to assure all the coverage required by myself, I asked for help and the young coordinator of "KDE Francophone" websites, Frédéric Sheedy, answered candidly. Frédéric's enthusiasm is contagious and helped me keep the pace with all the organization required for the upcoming conference.

I managed to secure a generous sponsorship from <a href="http://cyberlogic.ca">Cyberlogic</a>, a very Linux-friendly computer company in Montréal, that accepted to lend to the KDE booth two laptops and a wall projector. This equipment will help us to neatly overcome the lack of large colorful banners that are usually characteristic to KDE booths in Europe.

I have quite a few Kubuntu CDs sent to me by <a href="https://blogs.kde.org/blog/57">Jonathan Riddell</a> and I will print some small posters and pamphlets on the awesome <a href="http://blogs.kde.org/node/1552">Ricoh sponsored</a> color laser printer. With all this material, I have big confidence the booth will be attractive and alive.

We're now on for the latest preparations. I'm looking forward to the encounters that this conference will mediate.

