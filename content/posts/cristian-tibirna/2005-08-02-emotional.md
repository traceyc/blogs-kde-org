---
title:   "Emotional"
date:    2005-08-02
authors:
  - cristian tibirna
slug:    emotional
---
<i>I'm not the emotional type</i>. But...

In a very distant past, I was co-author to <a href="http://developer.kde.org/documentation/books/kde-2.0-development/">the "KDE 2.0 Development" book</a>. One colateral outcome of this happening was that I got 5 copies of the book. My own annotated book is on my shelf, of course. The other 4 copies rested o a out-of-the-way shelf for the last 5 years. Now, my rational brain told me this is nonesense. Thus, I donated one copy to the library of <a href="http://ulaval.ca">my university</a>. I was resolute into putting the other 3 in the recycling bin. But I couldn't, even if (I still maintain) <i>I'm not the emotional type</i>...

What to do?

