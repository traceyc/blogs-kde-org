---
title:   "It still surprises me, after 10 years (re: KDEPrint junior job)"
date:    2005-08-25
authors:
  - cristian tibirna
slug:    it-still-surprises-me-after-10-years-re-kdeprint-junior-job
---
After posting <a href="http://blogs.kde.org/node/1356">a very short announce</a> about the available KDEPrint junior job 5 days ago, I received no less than 5 offers to help from Andre Gemuend, James McArthur, Arnould Brice, Alfredo Cerutti and Steve Starr. I even received three distinct sets of replacement screenshots.

I have 10 years of continuous open source participation. I started looong time ago with fvwm and lyx. I am in KDE since 1996. Still, I never cease to be suprised by the energy with which people are willing to join and help. It's refreshing to say the least.

In the meantime, the kind KDE sysadmins created a new mailinglist: <a href="https://mail.kde.org/mailman/listinfo/kde-print-devel">kde-print-devel</a> which everybody willing to help with improving the KDE printing technologies (be it by improving the documentation, helping to rule in bugs, to mend usability) is wholeheartedly invited to join. This list will also receive the kdeprint bug reports (and will function as a means of triaging). The already existing kde-print mailing list remains for user-side testing and proofing, as well as for passing around know-how.

I am plunging deeper and deeper in the KDE printing code and I am more and more filled with awe. It would be incredible (if the SVN wouldn't stand there as living proof) how much work <a href="http://www.kde.org/areas/people/michaelg.html">Michael Goffioul</a> has put in this code, <a href="https://blogs.kde.org/blog/418">Kurt Pfeifle</a> has put in documentation and the others (that I know less for now) helped with. As one satisfied user was saying, we wouldn't realize what a wonderful thing these people did unless their work was taken from us ;-). Luckily, they are kind enough to have gifted the world with it. The gift of altruism and soulfullness.

I go now digest my surprise and awe :-)
<!--break-->