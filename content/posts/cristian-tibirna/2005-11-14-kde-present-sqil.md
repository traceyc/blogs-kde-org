---
title:   "KDE present at SQIL"
date:    2005-11-14
authors:
  - cristian tibirna
slug:    kde-present-sqil
---
I will make a general presentation of KDE for the public that will attend the events of <a href="http://sqil.info">SQIL</a> (Semaine Qu&eacute;b&eacute;coise de l'informatique libre - Quebec's Week for a free computing). I was very kindly invited for this by <a href="http://linuq.org">LINUQ</a> the LUG of Qu&eacute;bec City. 

My presentation will be on Saturday, November 19th, from 14h00 to 15h00 (local time), in one of the conference rooms of Palasis-Prince Pavilion at Laval University here in Qu&eacute;bec. The tentative name is "Trucs et astuces avec KDE" (Tips and Tricks with KDE). I already gathered quite a bit of things that I find particularly practical in KDE. If you care to name your favorite trick, feel free to add a comment below.

You are all invited on Saturday ;-)

<b>Update:</b> As per <a href="http://www.linuq.org/index.php?id=46">latest schedule</a> I will do my presentation starting with 15h00.
<!--break-->