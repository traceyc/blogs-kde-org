---
title:   "It ain't broke, don't fix it!"
date:    2006-05-20
authors:
  - cristian tibirna
slug:    it-aint-broke-dont-fix-it
---
More and more often, I get the occasion for a violent rant. This means one of two things: either a) I become older and crazier or b) this world becomes too stupid and too crazy for me.

Being one of those happy beings that never manage to get enough time and which think sleeping and eating are a huge waste of time, I, normally, conform to all sorts of Murphy laws, get a nasty time wasting from time to time.

This evening, I planned to prepare the two laptops granted to KDE for the next show I go to. I won't mention the very kind sponsor and the show's name here, in the context of my bad rant. I will come to them and the deserved praise in due time.

Well, while planning those laptops, I decided to give a try to SuSE-10.1. Good occasion to showcase to the future visitors of the KDE booth the latest in KDE and Linux technology. While all the installation went smooth and things look polished and well thought out, there is one smelly turd in the flower garden: the new updating tool.

I usually stay away from speaking (good or bad) of commercial technologies and companies that promote them, especially when I can be accused of vested interest. But this time, I need to say it loud. Three hours lost of my almost non-existent free time warrant me the right to do it. Here it is:

Novell, if it ain't broken, don't fix it. Yast's update tools in SuSE up to 9.3 worked marvelously. I use it very often and I can struggle very hard to remember the last time I had a problem. The zenworks thingie that is meant to replace yast-update-online is simply <b>not working</b>. No way to add new repositories, updates die in the middle, total mess. Talk of over-engineering. Does really an updating system need a server-client architecture, with its own 200-command vocabulary, with daemons that need 221s to go from "sleep" mode to active mode?

Be sensible and either fix it, or give me back y-o-u.
