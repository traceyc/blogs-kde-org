---
title:   "Semiotics"
date:    2005-08-18
authors:
  - cristian tibirna
slug:    semiotics
---
We use all sorts of signs and symbols to communicate. And quite often, communicate we don't.

I was brushing quickly through a few useless magazines I inadvertently collected while at LWE-SF last week. One of these was Novell's. Which is actually interesting as it wasn't all Novell publicity but also quite nice Linux publicity. Yet, what caught my eye most thoroughly was a section cover for the part extolling their certification program. It was the photo of  the chest of a work-blue-shirt-clad guy who had the word "Trainee" embroidered above his breast pocket.

Well.... "trainée" in french (with only an accent more) means <i>slut</i>. Needless to say, I bursted into laughs seeing this, and all their intended channeling was lost on me, to say the least.

We all use symbols, but we often choose to ignore what would they do in contexts outside the originating ones.