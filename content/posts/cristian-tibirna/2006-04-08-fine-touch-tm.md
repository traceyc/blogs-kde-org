---
title:   "the fine touch (tm)"
date:    2006-04-08
authors:
  - cristian tibirna
slug:    fine-touch-tm
---
In the last months I bought two <a href="http://caipsgws001.can.ibm.com/store/addoptions?sku=1875M2U&tool=search&aud=LENOVO">T43</a> laptops for my work. Nice machines. We use them for road warring/presentations and even for a bit of <a href="http://www.google.com/search?q=fem">FEM</a> development. So, I usually just shrink windows and insinuate <a href="http://opensuse.org">geeko</a> onto them.

Today, though, I decided to go see what went new in the windows world in the last 9 years. (read more...) <!--break--> 

Not surprisingly, I got the same cluttered and irritating interface, where you search for minutes for the damn simplest configuration that has to be re-sanitized from its terrifying default. 

But, this being an IBM machine, it came with two nice touches that I found particularly funny to use and interesting:
- the excellent 15" screens at 1400x1050, so crisp that it hurts my eyes when I go back to my 10y-old CRT at home
- the IBM Access Connections

The latest is an excellent no-nonsense profile manager that allows for custom connection configurations, with automatic selection, based on sensing wired or wireless network availability. What is very nice about this is 
- the complete wireless configuration, supporting most authenticated connectivity options in the wild world out there
- the possibility to choose a default printer per profile
- the possibility to fix a default home page per profile
- the means of automatically starting a battery of applications based on the profile one uses.

All lean and nice and well integrated.

Granted, there are negative points too: stupidly crammed interface stemming from the default windows widget skin, too many animations on the main page of the application and a too long delay before popping up the context menu from the tray icon.

But what impressed me most and what pushed me to log here about it is the obvious care for putting together a complete and useful tool, while keeping it simple to understand by the normal user.

This is the kind of software I like to use. This is the kind of applications that we call "killer".

Where is the link with KDE? Well, obviously, we lack such a lean integration tool in our beloved desktop. It's easy to understand that the current linux landscape isn't very inviting for writing one, but this is perhaps the direction that the development should take now, after 10 years of developing libraries.
