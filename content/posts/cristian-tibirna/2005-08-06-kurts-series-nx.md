---
title:   "Kurt's series on NX"
date:    2005-08-06
authors:
  - cristian tibirna
slug:    kurts-series-nx
---
The <a href="http://www.linuxjournal.com/article/8483">third part</a> of <a href="https://blogs.kde.org/blog/418">Kurt Pfeifle</a>'s NX series in <a href="http://linuxjournal.com">Linux Journal</a> just hit the web. It is a very instructive piece of writing. It provides rich information about motivations, history, technology and utilisation of NX/FreeNX. It is very nicely written, reads in a rapid pace and uses a crisp clear and well balanced language. Make sure you look through it. You might even want to put it in your bookmarks collection, as it's a valuable resource.
<!--break-->