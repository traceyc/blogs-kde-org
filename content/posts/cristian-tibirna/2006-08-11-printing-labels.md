---
title:   "Printing labels"
date:    2006-08-11
authors:
  - cristian tibirna
slug:    printing-labels
---
I use linux since 1994. I pride myself into thinking I know well what kind of tool linux can be.

I am accustomed with (and get quite thrilled about) the notion that I will learn something new every day of my life, right until the last one.

Yet, I still get surprised by discoveries like <a href="http://www.linux.com/article.pl?sid=06/08/04/1651234">Linux.com's tips of how to print labels with Linux</a>. <b>This</b> was one function on which I had given up all hope when it came to Linux. Well, it feels very good to be proven wrong, sometimes.

I guess that my fault in the wrong perception was that I was expecting label printing functionality to be attached to graphical document creation applications. And I didn't find any. Which brings me to the fact that this is probably one missing (but nevertheless important) feature in KDE's set of apps. Perhaps an independent GUI application for printing labels, and some research in what kind of support is needed from the printing subsystem, will allow to add this functionality as a generic, unix-like tool (one tool for each small job). Oh well, another thing to add in my &lt;exaggeration&gt;250GB&lt;/exaggeration&gt; TODO file.
<!--break-->