---
title:   "Apple bought CUPS"
date:    2007-07-13
authors:
  - cristian tibirna
slug:    apple-bought-cups
---
News <a href="http://www.cups.org/articles.php?L475">here</a> and <a href="http://www.linux-watch.com/news/NS2045132320.html">here</a>

/me wide-eyed but neutral (still).

I don't have a good knowledge of the internals of the CUPS community, but from my ignorant viewpoint, Michael Sweet had the largest hand in that code. It's now mostly up to the community. Which is a great one, for having produced the most dependable printing platform 0$ can buy.
<!--break-->