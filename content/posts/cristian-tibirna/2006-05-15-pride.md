---
title:   "Pride"
date:    2006-05-15
authors:
  - cristian tibirna
slug:    pride
---
Seeing a body of the size of Gouvernment of Canada do the right thing can be a reason of pride.

<a href="http://www.statcan.ca">Statcan</a> introduced web-based census this year. But they had a limitation as to what browsers can be used. Needless to say, Linux browsers were ruled out by default. I'm pleased to advertize that they <b>timely</b> <a href="http://www22.statcan.ca/ccr02/ccr02_003_e.htm">fixed the false problem</a>.

Needless to say, choosing the right UserAgent in Konqueror was a functional workaround even before the fix, and I celebrate the wisdom of the KDE developers that came up with this neat solution. But this doesn't remove anything from the merit of the government for seeing the right path. Thanks, Statcan.
<!--break-->