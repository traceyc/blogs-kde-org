---
title:   "Back from the Linux printing Summit"
date:    2006-04-16
authors:
  - cristian tibirna
slug:    back-linux-printing-summit
---
I finally find some time to write on my being at the Printing Linux Summit in Atlanta from 10 to 12 April.

I was kindly invited by <a href="http://www.linuxprinting.org/">Till Kamppeter from linuxprinting.org</a> and kindly financed by OSDL and its partners (this time, Lanier and HP).

It was <i>the occasion</i> to meet <a href="http://blogs.kde.org/node/1930">Waldo</a> for the first time in all these (8?!) years. It was for me a pleasure and an honor.

I met for the first time <a href="https://blogs.kde.org/blog/931">Ellen</a> and <a href="http://weblog.obso1337.org/">Celeste</a> and we had very enjoyable 
discussions on <a href="http://openusability.org/forum/forum.php?forum_id=301">usability</a> (a very important topic for me) and other ideas. 

I met again <a href="https://blogs.kde.org/blog/418">Kurt</a> too. Always a pleasure and a great occasion to get an energy boost from his passion and efervescence.

I met again my friends from the <a href="http://blogs.kde.org/node/1311">LWE 2005 in San Francisco</a> : Till Kamppeter, George Liu and Uli Wehner. Always very interesting and pleasant. And I made new acquaintances. Met and discussed with Jan Muehlig (OpenUsability), Michael Sweet (CUPS), John Cherry (my kind host from OSDL) and Larry Ewing (father of the penguin).

Apart from allowing me to meet great people from which to learn and get inspiration and energy, the summit in Atlanta was an essential opportunity to work and make important progressions in my understanding of the Linux printing world (ever so necessary for what I attempt to do in KDE).

And it, most importantly, allowed me to come to or reinforce these conclusions/decisions:

<ol>
<li> upgrade KDEPrint of KDE >= 3.5 to CUPS-1.2 (already started, much still to 
be done)</li>
<li> upgrade KDEPrint of KDE >= 4 to Qt4 and kdelibs-4</li>
<li> redesign user-facing parts of KDEPrint in order to account for usability 
reports and build on things learned from the first, very successful, 
iteration of the code</li>
</ol>

There are then issues that were decided at the summit and on which active involvement will be required from KDE:
<ul>
<li>there will be, most notably, an attempt of creating a mechanism for standardizing printing dialogs across all Linux applications</li>
<li>we will have to come up with a way of providing simple means for printer drivers and applications to extend the printing dialog</li>
</ul>
Lots of work to be done, lots of development (code and otherwise) to be brought to good term.

Here is the opportunity for an ambitious young developer to get involved in KDE: come help me with all the work required in KDEPrint and glory lies ahead for you. Or, a bit more seriously, I promise you that you will learn great new things and you will get satisfaction from knowing that your efforts help refining the excellent tools written a few years ago by Michael Goffioul and help KDE staying on the top of things.
<!--break-->