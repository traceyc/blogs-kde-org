---
title:   "Kurt about LPS"
date:    2006-04-18
authors:
  - cristian tibirna
slug:    kurt-about-lps
---
LWN published <a href="http://lwn.net/Articles/179511/">a writeup of first two days </a> of the Linux Printing Summit in Atlanta, most excellently written by our own Kurt Pfeiffle. A great collection of information complementary to his <a href="http://blogs.kde.org/node/1934">previous blog entry</a>. Thanks Kurt!
<!--break-->