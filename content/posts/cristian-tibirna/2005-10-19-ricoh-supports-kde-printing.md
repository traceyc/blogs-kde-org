---
title:   "Ricoh supports KDE printing"
date:    2005-10-19
authors:
  - cristian tibirna
slug:    ricoh-supports-kde-printing
---
<a href="http://www.ricoh-usa.com">Ricoh USA</a> just delivered to me (as representing KDE) a nice color laser printer (CL4000DN). It looks very impressive and it prints like a fairy tale charm. The hero in this is George Liu, Linux engineer at Ricoh, believes in KDE's technologies and wants Linux to succeed (printing aspects withstanding). He energically advocates Linux at Ricoh.

Thanks to this, I will be able to test professional printing features that start to become available in CUPS-1.2. Thus, I will strive to continue the hugely successful work done by the master of KDE printing, Michael Goffioul. 

_And_ we'll get hot looking leaflets at the various shows around here (Canada).

Thanks George! Thanks Ricoh! Thanks Michael.
<!--break-->