---
title:   "FreeNX hurries up!"
date:    2005-06-30
authors:
  - cristian tibirna
slug:    freenx-hurries
---
Isn't it nice? FreeNX hurries up!

<a href="http://kde.org/">There</a> <a href="http://wikipedia.org/">are</a> <a href="http://kernel.org/">things</a> for which I care more than for most, simply because I feel that they are bent to drive the world as we know it to become a much better place. One other such thing is <a href="http://freenx.berlios.de/">FreeNX</a>

In a flurry of news these latest days, we find out that much improved <a href="http://mail.kde.org/pipermail/freenx-knx/2005-June/001416.html">FreeNX 0.4.1 has been released</a>  (announced at LinuxTag 2005 by <a href="http://dot.kde.org/1120050176/">Fabian Franz and Kurt Pfeifle</a>; zero-config integration with KDEPrinter technology, yay!), that the <a href="http://www.nomachine.com/sources/development/">version 1.5 of the GPL NX components</a> also became available and that <a href="http://google.com">Google</a> accepted to sponsor the "Summer of Code" task concerning <a href="mhttp://developer.kde.org/summerofcode/knx.html">making kNX better</a>. Our young new hero is Christopher Cook and he will be coached by <a href="https://blogs.kde.org/blog/418">Kurt Pfeifle</a> and <a href="http://aseigo.blogspot.com/">Aaron Seigo</a>. And it doesn't stop here: Fabian Franz will give <a href="http://www.ukuug.org/events/linux2005/programme/abstract-FFranz-1.shtml">a talk about FreeNX</a> at UKUUG's <a href="http://www.ukuug.org/events/linux2005/">Linux2005 Conference</a>. Let's hope he will catch even more eyes (and also developer brains) up there.

As a side (but important) note, while mentioning Google's Summer of Code, I'm really pleased to see the <a href="http://dot.kde.org/1120137340/">hugely intelligent and pragmatic list of projects</a> that KDE managed to submit and get approved to this cool and helpful development bash. Kudos to all the passionate kids that took the decision to code their summer away, to the hype seasoned guys that accepted to mentor them and, not in the last place, to the few members of the KDE e.V. that discretely managed the whole submission process with much modesty and dedication (girls and guys, I won't name names, as I'd forget some for sure, you know thyselves, many thanks!).
<!--break-->