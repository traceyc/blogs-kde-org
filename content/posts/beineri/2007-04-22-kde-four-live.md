---
title:   "KDE Four Live++"
date:    2007-04-22
authors:
  - beineri
slug:    kde-four-live
---
New <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> CDs are online. The <a href="http://blogs.kde.org/node/2774">last week unmentioned</a> problem with squashfs which led me to uploading a "DVD.iso" is history, now you can get the same content of a typical desktop setup in a 477MB ISO. Additionally I wanted to produce a DVD which contains all the kde* modules and koffice: to my surprise the ISO stayed within CD size with just 655MB. I guess the effort to have a more lean base system and patterns available in openSUSE 10.3 already pays off. So this time there are two CDs flavors available and no DVD. :-)<!--break-->