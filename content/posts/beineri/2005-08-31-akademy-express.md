---
title:   "Akademy Express"
date:    2005-08-31
authors:
  - beineri
slug:    akademy-express
---
The "Express" in this blog entry title both refers to me only being able to attend Akademy 5 days this year as also this being the only blog entry summarizing some thoughts and observations after the two initial ones which were supposed to start a daily series. Many general reports and description of the talks have been posted on <a href="http://planetkde.org">Planet KDE</a> and <a href="http://dot.kde.org/">Dot</a> already, I won't repeat those.

Instead of blogging, hacking or reading email I spent almost all time when being in the university with either attending talks or tracking people down for a longer chat or to at least say "hello" to unknown faces with familiar names. There were also already some more or less spontaneous pre-Hackfest BoFs and brainstorming sessions happening during the UserConf talks or after the talks of DevelConf had ended.

The weather was fine though a bit unfamiliar hot. I slept every night about 15min more with at the end still being below my daily average sleep amount. The organization mostly worked, a great thanks from me to Antonio, Victor, Cayetano, Ana and all the other helpers. The most annoying thing I didn't find a sufficient way to work-around were the on one-side flashlight reflecting keylaces.

I found it a little bit shocking how lacking knowledge of English among Spanish people except of the ones of our organizing team was: Whether receptionist, waitress, sales personal in an American fastfood company restaurant or taxi driver. Many didn't even know basic stuff like "yes" or "one". At the international airport every sign was in Spanish, German and English but the woman at the check-in (for a German company and a flight to Germany) didn't even know how to ask "Window or passage?" in neither German or English.

I sat during the flight on the left side which turned out to be the interesting one: the plane started to the south and made a curve to fly north soon after allowing a nice view on Malaga from the water. Later I could watch the sunset over the Atlantic in front of France's coast which we were flying along and saw Paris' lights by night.
<!--break-->