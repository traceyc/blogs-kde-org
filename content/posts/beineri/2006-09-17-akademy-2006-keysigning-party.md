---
title:   "aKademy 2006 Keysigning Party"
date:    2006-09-17
authors:
  - beineri
slug:    akademy-2006-keysigning-party
---
Wondered this morning why there is no GPG/PGP keysigning scheduled for next week's <a href="http://akademy2006.kde.org/">akademy 2006</a>. Obviously it was forgotten, so after a quick query with the organization team about a free time slot I just posted this to akademy-announce:

<pre>
What is/Why keysigning? => http://alfie.ist.org/projects/gpg-party/gpg-party

If you intend to participate please send a mail with nothing but your KeyID 
(or KeyIDs) in the body to binner@kde.org by Thursday, Sep 21st 14:00 GMT. 
Subject does not matter. If you have more than one key put each KeyID into a 
line of its own. Make sure your key can be found on hkp://subkeys.pgp.net/.

On that Thursday evening you will be able to fetch a text file giving the 
fingerprint of each participating key. Please check your fingerprint of your 
key. Also compute the MD5 (and SHA1 hash) of the list and bring a print-out
together with some form of government issued ID (passport or similar).

If you miss above deadline you can still get your key signed at the party by 
printing snippet_s_ with the fingerprint of your key to give to all people.
</pre>

So the magic number mumbling will happen again this year. :-)
<!--break-->