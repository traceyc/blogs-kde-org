---
title:   "Re: My Dad's Thoughts"
date:    2005-11-10
authors:
  - beineri
slug:    re-my-dads-thoughts
---
Jeffrey Stedfast is <a href="http://primates.ximian.com/~fejj/blog/archives/000035.html">strengthening FUD about KDE developers</a> on <a href="http://planetsuse.org/">some</a> <a href="http://planet.gnome.org/">planets</a>. Strange that in the first paragraph he only cites a point for proof, Novell dropping KDE on certain products, which actually was true last week.

Users choosing their desktop based on troll postings are, sorry, just foolish. Jeffrey starts an unmotivated attempt to draw a dividing rule between those unidentified trolls and KDE developers. He makes the separation rather ambiguous despite his experience when coincidentally talking to a KDE developer (even in real life).

Over the weekend I have read many comments under KDE and Novell stories, the revoked gnomedesktop.org story, the later hijacked one on it, Slashdot, the for anti-KDE trolls infamous OSnews and so on. I have seen many hurting comments (like KDE developers not being part of the Free Software community, and also about their future sexual life) posted by their readers.

I really fail to see how Jeffrey can claim a high moral position for GNOME desktop or its community in such things.
<!--break-->