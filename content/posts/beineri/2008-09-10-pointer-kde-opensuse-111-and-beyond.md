---
title:   "Pointer: KDE in openSUSE 11.1 and beyond"
date:    2008-09-10
authors:
  - beineri
slug:    pointer-kde-opensuse-111-and-beyond
---
Zonker has posted <a href="http://news.opensuse.org/2008/09/09/kde-in-opensuse-111-and-beyond/">on openSUSE News</a> and <a href="http://zonker.opensuse.org/2008/09/09/kde-in-opensuse-111-and-beyond/">his own blog</a> our decision that openSUSE 11.1 will be the last release to include the KDE 3.5 desktop. This is a compromise how to handle the KDE4 transition after <a href="http://lists.opensuse.org/opensuse-factory/2008-08/msg00396.html">long discussions</a> with many users.

Beyond, openSUSE 11.2 will contain only one KDE desktop - likely version 4.3.