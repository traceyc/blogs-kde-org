---
title:   "KDE 3.5 Release Candidate 1"
date:    2005-11-12
authors:
  - beineri
slug:    kde-35-release-candidate-1
---
<a href="http://dot.kde.org/1131747786/">KDE 3.5 Release Candidate 1</a> is finally released. Updating the <a href="http://developer.kde.org/build/konstruct/">Konstruct build script</a> was easy, but creating <a href="http://download.kde.org/unstable/3.5-rc1/SuSE">the SUSE packages</a> in time did cost me some hours of sleep which I in my current state should have better spent in bed. <!--break-->