---
title:   "On Terminal Emulator \"Performance\""
date:    2006-03-16
authors:
  - beineri
slug:    terminal-emulator-performance
---
Reading the GNOME 2.14 feature guide you are told that "<i>several important components of the GNOME desktop are now measurably faster</i>" - which is nice. As example the GNOME Terminal is mentioned which a graph says is three times faster than the previous version and now being four times faster than xterm. If I remember an early draft of this document correctly it was measured with "<i>time cat /usr/share/dict/words</i>" so what do you think was measured? How long it took to print all the characters in that file? Likely by no means.

The common way to implement a terminal emulator is to don't render all output to the screen but to use some more or less dumb screen refreshing algorithm which only updates the screen output after a certain amount of received characters but at maximum after some milli seconds and also after some other milli seconds value after no further character was received. That's enough to give the user the visual impression of a smooth output and that everything gets printed while actually most parts are skipped. Slightly changing these parameters or algorithm during development can make your terminal emulator slower or faster for above measuring. And other emulator implementation use other parameters or a tweaked algorithm resulting in less or more skipped output.

So draw your own conclusion if above graph says something about the real text render performance improvements made.
<!--break-->