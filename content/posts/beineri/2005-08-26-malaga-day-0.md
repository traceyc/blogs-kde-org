---
title:   "Malaga Day 0"
date:    2005-08-26
authors:
  - beineri
slug:    malaga-day-0
---
My <a href="http://ktown.kde.org/~binner/">KDE related activities</a>, besides visiting the <a href="http://blogs.kde.org/node/1333">local usability meetings</a>, increased over the last days again (finally implemented the feature to open blocked popup-windows within Konqueror). With the contract for my job starting next month signed, I can now be sure it will stay at a high level.

On Thursday morning I upgraded my <a href="http://www.opensuse.org/">SUSE 10.0 Beta</a> 2 test installation via <a href="http://www.opensuse.org/index.php/Update_SUSE_to_next_version">rpm-Update</a> to Beta 3 (over 1.6 GB traffic) and updated the <a href="http://www.opensuse.org/index.php/Product_Highlights">Product Highlights</a> wiki page. Noticed several bugs gone and GNOME session not starting anymore (btw Gtk+ 2.8 seems to really drive packagers of depending apps crazy). Discovered that the K menu got patched to include an application search which I don't think is very convincing as it simply disables the (sub) menu groups and entries which don't contain the search term.

Studied and worked until the last possible hour on the KDE control center. I didn't manage to implement everything from <a href="http://ktown.kde.org/~binner/kcontrol5/">my previous mockups</a>, its design allows most stuff only with large hacks/many changes. But at least the ugly tab widget will be gone in KDE 3.5.

Packaged my luggage in time and drove to the airport where I met Ellen. The flight arrived delayed as seem most flights to Malaga these day. First impression: it's really hot in Malaga (forecast for Friday is 38°C). Got picked up by the shuttle service together with eight other people and experienced my first Spanish traffic jam. During the drive almost felt at home with stores like "Aldi", "Bauhaus" and "Lidl". Received my key for the student appartment which is interesting: my room is on the first floor within an apartment which stretches over two floors. I have to enter the building on the second floor and walk down the staircase within the apartement back to level one.

Met a crowd of KDE people who already arrived earlier sitting outside, many familiar faces but also some new. Neither is Internet available in the student residence nor yet working in the university, so no reason to go there. A group of 10 people decided to drive down-town by bus to find some food. We sat down in a restaurant in the history city center, food was fine but also not opulent or cheap. The meal was a bit disturbed by some noisy lift vehicle driving around and staff removing canvas which were attached to the houses hanging over the streets. After that we walked a bit around and saw the big cathedral as touristic highlight of the night.

We drove back to the student residency with the last night bus and arrived there at about 1am. Just minutes before David Faure arrived as last one of this day. He somehow managed to take a bus at the airport which didn't drive into the direction of the student residency but for 45min into the opposite direction.

This text was written in the night from Thursday to Friday because I could not sleep. It was warm and you cannot keep the window open because of the noise of a near-by highway which seems to be very busy even at 3am. I hope not many people took photos of me today, I hopefully will be sleeping better and looking fresher during the next days. :-)
<!--break-->