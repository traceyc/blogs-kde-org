---
title:   "Meet Dr. Klash"
date:    2004-07-16
authors:
  - beineri
slug:    meet-dr-klash
---
I bet everyone running a development version has at least once met Dr. Konqi - but do you know his colleague Dr. Klash? How could I, you ask? He is sleeping right on your hard disk within kdelibs! Dr. Klash checks for accelerator conflicts in menus and widgets. To awake him put following into ~/.kde/share/config/kdeglobals (or the config file for the application you want to test):

<pre>
[[Development]]
CheckAccelerators=F12
AutoCheckAccelerators=false
AlwaysShowCheckAccelerators=false
</pre>

Now pressing F12 will show you the current accelerators conflicts and insert suggested new accelerators. With AutoCheckAccelerators enabled the dialog will popup automatically if a conflict exists and AlwaysShowCheckAccelerators can enforce the dialog to popup always.
<br><br>
A nice way to see such accelerators conflicts and proposals visually is the check style (kdesdk/scheck).
<br><br>
On a similar note, did you know that kdebugdialog has a --fullmode switch?
<!--break-->
