---
title:   "openSUSE 11.1: Updates via PackageKit and PolicyKit"
date:    2008-11-22
authors:
  - beineri
slug:    opensuse-111-updates-packagekit-and-policykit
---
For <a href="http://en.opensuse.org/OpenSUSE_11.1">openSUSE 11.1</a> the <a href="http://en.opensuse.org/KDE_Updater_Applet">KDE Updater Applet</a> will switch from the zypp backend to its <a href="http://packagekit.org/">PackageKit</a> backend by default. Authorization is done via <a href="http://websvn.kde.org/trunk/playground/base/PolicyKit-kde/">PolicyKit-kde</a>:

<img hspace=20 src="https://blogs.kde.org/files/images/policykit-kde.png">

A <a href="http://kde-apps.org/content/show.php/KPackageKit?content=84745">KPackageKit</a> package will be available in the online repository for those who don't like the YaST Qt Package Manager.

<a href="http://en.opensuse.org/openSUSE_11.1"><img hspace=20 src="http://counter.opensuse.org/11.1/small" /></a>
<!--break-->
