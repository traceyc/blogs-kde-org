---
title:   "GNOME 2.20.2 Declared Bug-Free"
date:    2007-11-29
authors:
  - beineri
slug:    gnome-2202-declared-bug-free
---
Just read in the GNOME 2.20.2 release announcement: "<i>This is the second update to GNOME 2.20.0. The update fixes all known and unknown bugs and crashers.</i>" I have a follow up question, what about my still <a href="http://bugzilla.gnome.org/show_bug.cgi?id=354688">unresolved bug report</a>, reported over one year ago as example for many continued violations of the GNOME Human Interface Guidelines?