---
title:   "New \"Klax\" Live-CDs"
date:    2005-04-27
authors:
  - beineri
slug:    new-klax-live-cds
---
With the release of the 5.0 series of the <a href="http://www.linux-live.org/">Linux Live</a> scripts and KDE 3.4 packages being available in Slackware-current I thought it was time for a quick update of my "Klax" Live-CD. Maybe it would have been quick if I would have known the differences to the 4.2 series and omitted some pitfalls and own stupidity. :-)
<br><br>
The result are two flavors of &quot;Klax&quot; - both based on Slackware-current and using Linux 2.6.11.7: 

<ul>
<li><a href="http://ktown.kde.org/~binner/klax/koffice.html">&quot;Klax&quot; KOffice 1.4 Beta 1</a> has a size of 193MB and allows you to test-drive Kexi, Krita and the improved well-known applications of KOffice.</li>
<li> <a href="http://ktown.kde.org/~binner/klax/">&quot;Klax&quot; KDE 3.4</a> contains KDE 3.4 final, KOffice 1.3.5 and K3b. All KDE packages are taken from Slackware-current (read: may miss some special library features). The DHCP, keyboard and sound problems are fixed. And its size shrank by 54MB in comparison.</li>
</ul>

One last thing: gdb is missing on the CD - of course intentionally to demonstrate the <a href="http://www.fsl.cs.sunysb.edu/project-unionfs.html">UnionFS</a> functionality ;-). Download <a href="http://www.slackware.com/pb/download.php?q=current/gdb-6.3-i486-1">the gdb package</a> and install it as root with "installpkg gdb-6.3-i486-1.tgz" into the running Live-CD system. Now you will until next reboot get stacktraces displayed when applications crash.
<!--break-->