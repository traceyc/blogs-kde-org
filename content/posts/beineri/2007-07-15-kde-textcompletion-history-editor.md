---
title:   "KDE TextCompletion History Editor"
date:    2007-07-15
authors:
  - beineri
slug:    kde-textcompletion-history-editor
---
Did you ever feel bothered by a wrong suggestion of the text completion after having entered a typo in an application name, web page url or some web form entry before? So did I regularly and the only GUI-way to "correct" it until now was to clear all entries via the context menu's "Clear History". Not really a satisfying solution. :-)

[image:2884 align=right][image:2883 align=right hspace=2]I decided to <a href="http://idea.opensuse.org/content/ideas/kde-textcompletion-history-editor">work on it on one day</a> of the Novell OPS Hack Week and added "Edit History..." to the context menu which brings up the small dialog in the second screenshot where you can delete faulty entries. This currently works for Konqueror's URL combo bar, khtml forms and the mini-CLI history.

The patches are already in the openSUSE KDE packages within the KDE:KDE3 project of the openSUSE build service and of course will be also in the upcoming openSUSE 10.3 Alpha 6 next week.
<!--break-->