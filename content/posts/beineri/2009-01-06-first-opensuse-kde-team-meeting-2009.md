---
title:   "First openSUSE KDE Team Meeting 2009"
date:    2009-01-06
authors:
  - beineri
slug:    first-opensuse-kde-team-meeting-2009
---
Tomorrow most people will be back from the holiday season, time to start again with the <a href="http://en.opensuse.org/KDE/Meetings">biweekly IRC meetings</a> of <a href="http://en.opensuse.org/KDE/Team">the openSUSE KDE Team</a>. This time we will look back to last year or rather to the <a href="http://en.opensuse.org/OpenSUSE_11.1">openSUSE 11.1</a> release shortly before Christmas and the biggest reported problems. And also, despite the <a href="http://zonker.opensuse.org/2008/12/15/discussing-opensuse-112-schedule/">schedule discussion</a> not being finished yet, we will start to collect and discuss <a href="http://en.opensuse.org/KDE/Ideas/11.2">ideas for openSUSE 11.2</a>.
<!--break-->
