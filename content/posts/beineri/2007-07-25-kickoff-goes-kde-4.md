---
title:   "Kickoff Goes KDE 4"
date:    2007-07-25
authors:
  - beineri
slug:    kickoff-goes-kde-4
---
Porting of Kickoff to KDE 4 has started: the first step is trying to make it work with the Kicker remains. Some code is still disabled but you can already search/browse applications and start them:

[image:2897 width=348 height=471]

If you want to help to get rid of KDE3isms, the code is in /home/kde/branches/work/kickoff_kde4-port/ - search for FIXME. :-)