---
title:   "\"KDE 3.5.4 on SUSE Linux 10.1\" for VMware Player"
date:    2006-08-07
authors:
  - beineri
slug:    kde-354-suse-linux-101-vmware-player
---
I have updated the <a href="http://developer.kde.org/~binner/vmware/">KDE VMware virtual machine</a> which I have been <a href="http://blogs.kde.org/node/1720/">offering for some months now</a>. It now contains the <a href="http://blogs.kde.org/node/2208">SUSE KDE 3.5.4 packages</a> installed on a SUSE Linux 10.1 base. Additionally the by default installed applications are updated to their current versions in the openSUSE Build Service and KOffice 1.5.2 is added. As said previously, don't forget to run YaST Online Update once in a while, or install the 'zen-updater' applet to be notified about updates. ;-)

You can <a href="http://developer.kde.org/~binner/vmware/">download it</a> from a brand new KDE server archived either as 742 MB flavor for Microsoft Windows or 685 MB for Linux.<!--break-->