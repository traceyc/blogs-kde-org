---
title:   "KDE Four Live Beta 3+"
date:    2007-10-21
authors:
  - beineri
slug:    kde-four-live-beta-3
---
What would a KDE4 Hack Week be without a new version of <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> released at its end? :-)

[image:3060 size=preview hspace=100]

This version has KDE 4.0 Beta 3+ (mostly 3.94.1 snapshot) packages from the <a href="http://en.opensuse.org/KDE4">KDE:KDE4 build service project</a> engrafted on <a href="http://news.opensuse.org/?p=400">openSUSE 10.3</a> (if you find your way within YaST you can turn it into a full openSUSE 10.3 installation). Extra plasmoids are included with the extragear-plasma and playground-base packages, no Amarok included as it didn't build. The Plasma setup looks only good at 1024x768 resolution - seems Plasma has still to learn about resolution dependence. SATA CD-drives and network setup should hopefully work better than before and auto-login is enabled.<!--break-->