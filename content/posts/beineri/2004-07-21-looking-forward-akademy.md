---
title:   "Looking Forward to aKademy"
date:    2004-07-21
authors:
  - beineri
slug:    looking-forward-akademy
---
I just read that Aaron is excited to go to aKademy and that he will meet KDE developers (and not few!) there. Let me say that I also look forward to get to know some new faces including Aaron, Fabrice and Waldo. And Eric should be interesting to watch (or better listen?). :-) I also discovered the names of <a href="http://www.kde.org/people/gallery.php">some KDE oldies</a> like Matthias H&ouml;lzer-Kl&uuml;pfel and Roberto Alsina who plan a comeback? All the rest of you please assume that we were either already at same place at same time or that I'm simply not aware of your registration.

Looking at the conferences schedules this year also the bosses (or should I say chief bosses?) of the SUSE and Trolltech KDE fellows will be there. Four days are filled with talks in two slots and I must state that compared to Kastle there are way more time slots where I can't decide yet which talk to visit. Which of course tells a good story for overall quality. I hope you all make your slides available after the talks!

PS: KDE 3.3 Beta 2 was not released today as one overhasty desktop linux news site reported.
<!--break-->