---
title:   "openSUSE 10.3 Alpha 6 Remarks"
date:    2007-07-20
authors:
  - beineri
slug:    opensuse-103-alpha-6-remarks
---
Yesterday <a href="http://lists.opensuse.org/opensuse-announce/2007-07/msg00005.html">openSUSE 10.3 Alpha 6 has been released</a> (<a href="http://en.opensuse.org/Factory/News#Changes_between_openSUSE_10.3_Alpha_5_and_Alpha_6">major changes</a>). As Coolo mentioned this is the first alpha release which feels <a href="http://en.opensuse.org/Bugs:Most_Annoying_Bugs_10.3_dev#openSUSE_10.3_Alpha6">really alpha</a> due to the package management refactoring. During the last month <a href="http://blogs.kde.org/node/2873">much other stuff happened</a> so not many visible changes besides version updates for the KDE desktop; the new 10.3 artwork went into Factory too late.

The <a href="http://download.opensuse.org/distribution/10.3-Alpha6/iso/cd/openSUSE-10.3-Alpha6-KDE-i386.iso">one KDE installation CD</a> (690MB) is progressing well: thanks to more package splitting and other fixes we were able to put besides few missing basic packages more application packages on it including digikamimageplugins, Kaffeine, KSudoku/KDE4, Konversation, KRecord, kdenetwork3-vnc and KPilot. More discovered opportunities like a smaller boot/ system and further OpenOffice.org splitting may free up to 20MB or more for applications until Beta 1. :-)

First time with Alpha 6 are <a href="http://download.opensuse.org/distribution/10.3-Alpha6/iso/cd/openSUSE-10.3-Alpha6-KDE-x86_64.iso">x86_64 one CD installation media</a> available - with some stuff (like KDE4 games) removed compared to the i386 version because binaries and libraries being bigger. Not released, at least this time (depending on received feedback), were the usual 5-6 CD installation sets so likely more people will use and test the single CDs.
<!--break-->