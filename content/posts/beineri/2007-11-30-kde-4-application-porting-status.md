---
title:   "KDE 4 Application Porting Status"
date:    2007-11-30
authors:
  - beineri
slug:    kde-4-application-porting-status
---
I created a <a href="http://techbase.kde.org/Schedules/KDE4/Application_Porting_Status">page on TechBase</a> to track the porting status of Qt/KDE applications, which are not part of the KDE 4.0 release, with the applications that distributions tend to have in their default installation as a start. Please help to gain and keep an overview by adding applications/updates.