---
title:   "Kickoff Buttons"
date:    2007-03-23
authors:
  - beineri
slug:    kickoff-buttons
---
Andy Koehler has created some <a href="http://www.kde-look.org/usermanager/search.php?username=connedy&action=contents">nice animated Kickoff buttons </a> in case you don't like the one that your distribution ships. :-) They look best at their native size but can be used with any: when you change the panel size the animation is not resized initially but a "dcop kicker Panel restart" will fix that.<!--break-->