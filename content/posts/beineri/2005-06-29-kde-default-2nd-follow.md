---
title:   "\"KDE is the Default...\" 2nd Follow Up"
date:    2005-06-29
authors:
  - beineri
slug:    kde-default-2nd-follow
---
I guess the news about <a href="http://news.com.com/Sun+cools+down+Linux+desktop+plan/2100-7344_3-5767030.html?tag=nefd.top">Sun cooling down Linux desktop plans</a> means that I can eat the <a href="http://blogs.kde.org/node/view/1120">promised cookie</a> now myself. And it seems <a href="http://www.murrayc.com/blog/tech/2005-06-08-17-10">GNOME realized</a> that some of their deployment claims are <a href="http://blogs.kde.org/node/view/1128">short on details</a>.

Talking about Sun, it was nice to see a <a hreF="http://www.opensolaris.org/jive/thread.jspa?messageID=3136#3194">Sun employee mention both GNOME and KDE</a> as arguments in a discussion about the advantages of Solaris Express over SchilliX.

I also had a closer look at <a href="http://www.gnome.org/~davyd/footware.shtml">GNOME's Footware page</a> which intends to list distributions that ship the GNOME Desktop. There are not less than seven distributions wrongly listed which according to Distrowatch ship libgnome but not Nautilus. Those should be removed or the table renamed to "ship GNOME Platform" maybe.
<!--break-->