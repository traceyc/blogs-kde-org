---
title:   "Novell Open Audio: openSUSE 10.2 Overview"
date:    2006-12-17
authors:
  - beineri
slug:    novell-open-audio-opensuse-102-overview
---
<a href="http://www.novell.com/feeds/openaudio/">Novell Open Audio</a> has a new half an hour <a href="http://www.novell.com/feeds/openaudio/?p=114">episode about the openSUSE 10.2 release</a> featuring openSUSE evangelist Martin Lasarsch (and also a new mention of the missed KDE episode).<!--break-->