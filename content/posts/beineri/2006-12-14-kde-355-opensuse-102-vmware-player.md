---
title:   "\"KDE 3.5.5 on openSUSE 10.2\" for VMware Player"
date:    2006-12-14
authors:
  - beineri
slug:    kde-355-opensuse-102-vmware-player
---
The <a href="http://en.opensuse.org/OpenSUSE_News/10.2-Release">openSUSE 10.2 Release Guide</a> is a nice description of the openSUSE 10.2 goodies whose box edition is meanwhile also listed by shops (<a href="http://shop.novell.com">North America/World Wide</a>, <a href="http://www.edv-buchversand.de/suse/">Germany</a>). I updated my popular <a href="http://ktown.kde.org/~binner/vmware/">VMware image to KDE 3.5.5 with KOffice 1.6</a> on an openSUSE 10.2 base. If you did not play with the new KDE start menu Kickoff and Kerry yet, now you have no excuse anymore! :-)
<!--break-->