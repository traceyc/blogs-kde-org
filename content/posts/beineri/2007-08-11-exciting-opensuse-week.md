---
title:   "An Exciting (open)SUSE Week"
date:    2007-08-11
authors:
  - beineri
slug:    exciting-opensuse-week
---
This week was so filled with events and news that it easily qualifies for the most exciting openSUSE week yet:
<ul>
  <li>On Monday the rush until <a href="http://en.opensuse.org/Roadmap/10.3#Thu.2C_Aug__9_____openSUSE_10.3_Beta1_release_.28feature_complete.29">feature and version freeze</a> of openSUSE 10.3 started in the evening.</li>
  <li>The <a href="http://news.opensuse.org/?p=85">Final Draft of the openSUSE Guiding Principles</a> was posted.</li>
  <li>At the same day the <a href="http://www.linuxworldexpo.com/live/12/">LinuxWorldExpo San Francisco</a> started with not only <a href="http://news.opensuse.org/?p=120">openSUSE being present</a> but also the announcements that both <a href="http://arstechnica.com/news.ars/post/20070806-lenovo-novell-partner-to-offer-linux-on-the-thinkpad.html">Lenovo</a> and <a href="http://www.desktoplinux.com/news/NS1990237723.html">Dell</a> will start to pre-load SUSE Linux Enterprise Desktop and offer support.</li>
  <li>Finally was announced that <a href="http://news.opensuse.org/?p=101">AMD has become a sponsor of the openSUSE project </a> by providing hardware for the <a href="http://en.opensuse.org/Build_Service">openSUSE Build Service</a>.</li>
  <li>The Build Service itself got a new <a href="http://news.opensuse.org/?p=102">distro download and package search</a> front-end with 1-Click Installation.</li>
  <li>On Thursday <a href="http://news.opensuse.org/?p=105">the openSUSE project turned two</a>. The birthday wishes keep arriving and show that <a href="http://news.opensuse.org/">openSUSE News</a> gets more known quickly.</li>
  <li>To celebrate we had also the <a href="http://news.opensuse.org/?p=106">Release of openSUSE 10.3 Beta 1</a> which among other stuff has a new greeter (<a href="http://developer.kde.org/~binner/SUSEgreeter/">web mockup</a>) to explain and link to the project better.</li>
  <li>The 'People of openSUSE' series started, with <a href="http://news.opensuse.org/?cat=15">already three interviews</a> being published.</li>
  <li>The news that <a href="http://www.groklaw.net/article.php?story=20070810165237718">SCO got a big kick</a> after all the years also made people happy.</li>
  <li>And still running the whole week-end, <a href="http://news.opensuse.org/?p=123">the first openSUSE Bug Slashing</a>.</li>
</ul>

Dunno how we can top this week. Maybe with a great openSUSE 10.3 release which allows us to push PCLinuxOS from rank #1 of the DistroWatch charts. :-)
<!--break-->