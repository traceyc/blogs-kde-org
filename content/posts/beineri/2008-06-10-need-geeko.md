---
title:   "In Need of a Geeko?"
date:    2008-06-10
authors:
  - beineri
slug:    need-geeko
---
<a href="http://ixsoft.de/cgi-bin/web_store.cgi?ref=Products/de/SUGECKO100.html"><img src="http://ixsoft.de/Web_store/Images/256/SUGECKO035.jpg" align="right" hspace="2" vspace="2"/></a>I am a proud <a href="http://developer.kde.org/~binner/family.jpg">owner of a Geeko</a>. Those seemed to be out of stock and not produced anymore. But now I spotted a <a href="http://ixsoft.de/cgi-bin/web_store.cgi?ref=Products/de/SUGECKO100.html">first merchant</a> which promises new Geekos for second half of 2008 - and not only a 35cm version but also a 120cm big one! :-) That's of course still not as huge as the <a href="http://developer.kde.org/~binner/huge-geeko.jpg">mother of all Geekos</a> in the SUSE office in Nuremberg.<!--break-->
