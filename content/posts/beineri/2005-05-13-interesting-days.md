---
title:   "Interesting Days"
date:    2005-05-13
authors:
  - beineri
slug:    interesting-days
---
On Wednesday evening I visited the <a href="http://www.opensourcejahrbuch.de/2005/lehmanns.html">podium discussion</a> for the <a href="http://www.open+sourcejahrbuch.de/2005/">Open Source Jahrbuch 2005</a>. Afterwards some speakers and visitors moved on to a nearby cafe and I chatted with Oliver Zendel of <a href="http://www.linuxtag.org/">LinuxTag</a> e.V. and also a bit with Eva Brucherseifer and Jan M&uuml;hlig about KDE and Appeal until early Thursday.

On Thursday <a href="http://www.linuxinberlin.de/">an event</a> trying to gather Berlin Linux companies to form an interest group to promote Linux in the region took place the first time. It was not that well visited, also didn't find the talks much interesting. So I went around visiting the few booths and talked to familiar faces of the <a href="http://www.belug.de/">Berlin Linux user group</a>. I also learned that several attendees planned to move on to another event which I wasn't aware of before in the evening.

That was a speech by Standford professor <a href="http://en.wikipedia.org/wiki/Lawrence_Lessig">Lawrence Lessig</a> which was organized by the <a href="http://www.bpb.de/">Bundeszentrale f&uuml;r politische Bildung</a> (Federal Agency for Civic Education). Lessig's talk was about culture, copyright and <a href="http://creativecommons.org/">Creative Commons</a> which he founded. The presentation was well worked-out. The second speaker of the day felt necessary to apologize for his in comparison boring slides in advance. The surprising end of the day was that free wine and fingerfood was offered after the discussion, in amounts that everybody who wanted could get more than drunk and sated. Certainly a nice way to invest German tax money into the education of citizens. :-)
<!--break-->