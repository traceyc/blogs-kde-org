---
title:   "Re: GNOME on NLD/SLES"
date:    2005-11-08
authors:
  - beineri
slug:    re-gnome-nldsles
---
<a href="http://rubberturnip.org.uk/index.cgi/2005/11/07#1131401687gnome_on_nld_sles">James Ogley writes</a>: "<i>NLD is Novell's enterprise desktop product, I'm pretty sure it's always used GNOME</i>". James, you obviously don't know Novell's products well. Neither is GNOME the only <a href="http://distrowatch.com/table.php?distribution=novell">desktop on Novell Linux Desktop 9</a>, <a href="http://blogs.kde.org/node/1120">nor is it the default</a>. And judging from the bug reports and hearsay there are customers using KDE on NLD. It will be interesting what Novell will tell them.

And a general note, if a company reduces its investment into "Linux on the Desktop" or applications it's sad independent from what desktop your prefer. It's a sign that the Linux desktop(s) are still waiting for the breakthrough.
<!--break-->