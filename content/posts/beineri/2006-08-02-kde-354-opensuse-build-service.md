---
title:   "KDE 3.5.4 in openSUSE Build Service"
date:    2006-08-02
authors:
  - beineri
slug:    kde-354-opensuse-build-service
---
<a href="http://dot.kde.org/1154521282/">KDE 3.5.4 was released today</a> and the <a href="http://en.opensuse.org/">openSUSE</a> <a href="http://en.opensuse.org/Build_Service">Build Service</a> has unsupported rpms of it:

  <ul type="disc">
    <li>
        <a href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.1/">SUSE Linux 10.1 YUM repository</a> (32bit and 64bit)
    </li>
    <li>
        <a href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.0/">SUSE Linux 10.0 YUM/YaST repository</a> (32bit and 64bit)
    </li>
    <li>
        <a href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_9.3/">SUSE Linux 9.3 YUM/YaST repository</a> (32bit and 64bit)
    </li>
  </ul>

Btw, we need more mirrors of the Build Service repositories. Please read <a href="http://en.opensuse.org/Mirror_Infrastructure">how to mirror</a> and get in contact with <a href="mailto:ftpadmin@opensuse.org">ftpadmin@opensuse.org</a> if you are able to help.
<!--break-->