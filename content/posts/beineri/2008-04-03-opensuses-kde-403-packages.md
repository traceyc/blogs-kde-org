---
title:   "openSUSE's KDE 4.0.3 Packages"
date:    2008-04-03
authors:
  - beineri
slug:    opensuses-kde-403-packages
---
<a href="http://dot.kde.org/1207154042/">KDE 4.0.3 is out</a> and openSUSE packages are available at <a href="http://en.opensuse.org/KDE/KDE4#Installation">the usual place</a> as is a new <a href="http://home.kde.org/~binner/kde-four-live/">"KDE Four Live" CD</a>.

Just want to note that these packages are less pure KDE 4.0.3 with every day we near the openSUSE 11.0 feature freeze. Last week-end I started a "Plasma 4.0 openSUSE" work branch in KDE SVN to combine our local patches, upstream backports and own features. The goal is to ship a KDE4 desktop which has not less functionality than a GNOME desktop ;-) - means eg adding / removing of panels, a simple way of being able to move plasmoids on panels and the theme selector are in there. You can find this stuff already merged into our KDE 4.0.3 packages. Some outstanding tasks are using the Kickoff look of trunk and dealing with the desktop icon issue. Currently there are of course also some glitches to chase down during the bug fixing months until the final release.

If everything turns out fine in the remaining time until the feature freeze we will have a KDE4 <a href="http://en.opensuse.org/KDE/KDE_on_openSUSE11.0#KDE_Applications_in_openSUSE_Patterns">desktop without any by default running KDE3 application</a>. That includes update notification applet, kpowersave, front-end for NetworkManager 0.7 and if you like Kerry. And as you are maybe already aware: YaST (installation, system configuration, live installer), package selector and X configuration tool are already using a Qt4-frontend respectively are ported to Qt4. :-)
<!--break-->