---
title:   "kdedevelopers.org is back"
date:    2005-07-13
authors:
  - beineri
slug:    kdedevelopersorg-back
---
Thanks to Geiseri for working hard after his return and the Drupal guys helping to bring the <a href="https://blogs.kde.org/">Developer Journals</a> back online. 

Happy blogging! :-)