---
title:   "\"KDE is the Default on Novell Linux Desktop\""
date:    2005-06-02
authors:
  - beineri
slug:    kde-default-novell-linux-desktop
---
Would I tell you this? No, I'm fair and don't try to cheat you. Whoever tried Novell's Linux Desktop 9 will know that it has no default desktop, you have to choose either KDE or GNOME during installation and there is no indication what to prefer. Is the GNOME camp simply uninformed or do they have dishonorable intentions? I keep <a href="http://www.gnome.org/~davyd/footware.shtml">reading from them</a> and hearing from them like in the "<a href="http://www.gnome.org/~gman/guadec_talks/101_gnome_things.sxi">101 Things to Know about GNOME</a>" talk at GUADEC that NLD's default desktop would be GNOME.

And a cookie to whom can give a link to a news/report about the proclaimed 1 million <i>running</i> GNOME desktops in China: I don't mean the initial <i>declaration of intent</i> (coincidentally dated inauguration day of that then new product) but something newer than 2003.
<!--break-->