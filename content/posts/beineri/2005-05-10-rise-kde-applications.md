---
title:   "The Rise of KDE Applications"
date:    2005-05-10
authors:
  - beineri
slug:    rise-kde-applications
---
It's nice to not only see <a href="http://dot.kde.org/1115479655/">increasing popularity of KDE</a> but to also see <a href="http://kde-apps.org/">applications for KDE prosper</a> and getting the recognition they deserve: last five <a href="http://dot.kde.org/">dot stories</a> are all about KDE applications people have written stories about. :-)