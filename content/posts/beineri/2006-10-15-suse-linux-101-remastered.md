---
title:   "SUSE Linux 10.1 \"Remastered\""
date:    2006-10-15
authors:
  - beineri
slug:    suse-linux-101-remastered
---
Seems it's up to me as nobody else blogs about it: <a href="http://lists.opensuse.org/opensuse-announce/2006-10/msg00001.html">SUSE Linux 10.1 "Remastered"</a> CD and DVD ISOs are now available (if you have still the original ISOs around, there exist delta ISOs against them). These have all the online patches which were released since the original 10.1 "goldmaster" release already integrated. This means no more three times restart of YOU during installation. :-) And also a more stable, slightly faster and delta-rpms for online updates supporting package mananagment. Of course all security and other released fixes are also integrated. And because first people already asked: this doesn't include newer application/desktop versions. You have to wait for openSUSE 10.2 (soon entering the Beta phase) for that or adventure the unsupported build service repositories for that.<!--break-->