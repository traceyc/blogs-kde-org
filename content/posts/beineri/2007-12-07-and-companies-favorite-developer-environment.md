---
title:   "And The Companies' Favorite Developer Environment Is..."
date:    2007-12-07
authors:
  - beineri
slug:    and-companies-favorite-developer-environment
---
The <a href="https://www.linux-foundation.org/en/2007ClientSurvey">2007 Linux Desktop/Client Survey</a> of the Linux Foundation closed recently. One interesting result:

[image:3129 size=original hspace=50]

Didn't someone claim that companies would prefer Gtk+ because of the license? :-)

<b>Update:</b> <a href=http://jamesthevicar.com/index.cgi/2007/12/07#1197015621favourite_developer_environment>James</a> claims that you may have to add Gtk and Mono numbers. This is wrong as the survey asked the participants to "Select all [answers] that apply". Assuming everyone with Mono also uses Gtk+ leaves only 7.7% to non-Mono Gtk+ applications.