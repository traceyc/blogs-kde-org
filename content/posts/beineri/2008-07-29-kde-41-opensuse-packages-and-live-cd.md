---
title:   "KDE 4.1 openSUSE Packages and Live CD"
date:    2008-07-29
authors:
  - beineri
slug:    kde-41-opensuse-packages-and-live-cd
---
<i>Warning: Lazy copycat from <a href="http://news.opensuse.org/2008/07/29/kde-41-released-with-opensuse-packages-and-live-cd/">openSUSE News</a> ahead!</i> :-)

The KDE team today <a href="http://dot.kde.org/1217341401">released KDE 4.1</a>. The KDE developers, including the <a href="http://opensuse.org/KDE/Team">openSUSE KDE Team</a>, have been working on it for the last six months. Lots of feedback from people trying out KDE 4.0 has gone into KDE 4.1, filling most of the gaps people experienced with the 4.0 releases. See the <a href="http://www.kde.org/announcements/4.1/">release announcement</a> for more information and screenshots.

<a href="http://home.kde.org/~binner/kde-four-live/KDE-Four-Live.i686-1.1.png"><img src='http://home.kde.org/~binner/kde-four-live/KDE-Four-Live.i686-1.1-preview.png' hspace='50' alt='KDE Four Live 1.1' /></a>

Regular <a href="http://en.opensuse.org/KDE/KDE4">KDE 4 Packages</a> and an openSUSE-based <a href="http://home.kde.org/~binner/kde-four-live">KDE Four Live CD</a> have been available throughout the whole cycle, and final versions of them are also available now. On openSUSE 11.0 you can use 1-click-install to get the KDE 4.1 desktop environment (for openSUSE 10.3 follow above link):

<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Desktop/openSUSE_11.0/KDE4-DEFAULT.ymp"><img src="http://files.opensuse.org/opensuse/en/d/dd/Kde4-ymp.png" hspace="50"/></a>

Or you can choose to install a more <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Desktop/openSUSE_11.0/KDE4-BASIS.ymp">basic KDE 4 desktop</a>. Developers can also optionally install the <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Desktop/openSUSE_11.0/KDE4-DEVEL.ymp">KDE 4 build dependencies</a>: all the packages you need to have installed for compiling KDE 4.x from source (experts only).<!--break-->