---
title:   "#opensuse-kde"
date:    2007-03-21
authors:
  - beineri
slug:    opensuse-kde
---
After the GNOME team made a <a href="http://lists.opensuse.org/opensuse-announce/2007-03/msg00000.html">big splash about their openSUSE involvement</a> some people thought it would be a good idea to found <a href="irc://irc.freenode.net/openSUSE-kde">#opensuse-kde</a> to not flood other #opensuse-channels with KDE talk anymore. So join for a more direct way than via the ever existing <a href="http://lists.opensuse.org/opensuse-kde/">opensuse-kde mailing list</a> (<a href="mailto:opensuse-kde-subscribe@opensuse.org">subscribe</a>) to <a href="http://software.opensuse.org/download/KDE:/Community/">involve in</a> or <a href="http://en.opensuse.org/KDE/Upgrade">stay up-to-date</a> with KDE on openSUSE development.

Interesting times lay ahead with openSUSE project's <a href="http://files.opensuse.org/opensuse/en/1/17/FOSDEM-10.3-talk.pdf">proclaimed goal</a> to be an early adopter of (<a href="http://blogs.kde.org/node/2600">parts of</a>) KDE 4 (<a href="http://software.opensuse.org/download/KDE:/KDE4/">weekly snapshot repository</a>) with the next openSUSE release 10.3.<!--break-->