---
title:   "openSUSE 11.1: Introduction to KDE4"
date:    2008-11-16
authors:
  - beineri
slug:    opensuse-111-introduction-kde4
---
Another small idea we realized for <a href="http://en.opensuse.org/OpenSUSE_11.1">openSUSE 11.1</a> is a link in the first-login greeter to a "<a href="http://en.opensuse.org/KDE/Introduction_to_KDE4">Introduction to KDE4</a>" page.

<img hspace=20 width=210 height=71 src="https://blogs.kde.org/files/images/OS11.1-introduction.png">

Originally written for the greeter (hence the current layout and shortness) the text ended in the wiki because we were past openSUSE translation freeze. The wiki solution also allows extension anytime (feedback and translations welcome). Its purpose is to give new KDE users and KDE3 switchers some (openSUSE-specific) explanations and hints for a good KDE4 start.

For further generic introductions and tutorials we happily refer to the nicely growing <a href="http://userbase.kde.org/">KDE UserBase</a>. :-)
<!--break-->
