---
title:   "Twitter Works - Sometimes"
date:    2009-08-07
authors:
  - beineri
slug:    twitter-works-sometimes
---
I started to <a href="http://twitter.com/Beineri">publish on Twitter</a> some days ago to not stay completely silent about the good stuff that is happening with <a href="http://news.opensuse.org/2009/08/06/unofficial-opensuse-kde-43-rpms-and-live-cds/">KDE/openSUSE</a>. Most of the time my posts even appear on other people's pages and in <a href="http://search.twitter.com/search?q=beineri">Twitter Search</a>. According to the "Find People" function <a href="http://twitter.com/search/users?q=Beineri&category=people&source=find_on_twitter">I don't exist</a> though.