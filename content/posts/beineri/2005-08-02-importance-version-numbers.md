---
title:   "The Importance of Version Numbers"
date:    2005-08-02
authors:
  - beineri
slug:    importance-version-numbers
---
KDE 3.5 Alpha <a href="http://developer.kde.org/development-versions/kde-3.5-release-plan.html">will be tagged in three days</a> and many applications in /branches/KDE/3.5 still show the version number they had in the 3.4.0 release. :-( I usually go trough all modules before releases and look for many applications whether there have been changes since last release and if the version number has been already increased. This is time-consuming and I don't feel that I should be doing it. Please maintainers of applications (or as fallback contributors to) increase version numbers regularly!

Without proper version maintenance you have additional effort to categorize bugs or callback bug reporters. Don't expect users to add source package information themselves or them having the same kdelibs/kdebase version installed as every other module. For proper bug management you have to also ensure that Bugzilla knows the versions for your product: add it with the <a href="https://bugs.kde.org/editproducts.cgi">Edit products</a> link. If your account has not the necessary rights contact me and I will either add the missing version or give you the rights.

How does a good version number look like? It doesn't have to match the KDE release number. Something in the form of x.y.z where z preferably matches the KDE release revision (nice to see if a version number has been already increased) and at least y being increased for every feature reelase. For the lazy ones (and if you don't mind matching your application version number the KDE release number) you can use the KDE version string as application version. Bad are version numbers which are not changed at all, maybe even for several releases (bad example: kwin).

On a related note, it's disappointing how few maintainers collect and list <a href="http://www.kde.org/announcements/changelogs/changelog3_4_1to3_4_2.php">changes for the release notes</a> of bugfix releases. There are several modules with no or only a short list of bugfixes but a longish 'all SVN changes' dump near by.
<!--break-->