---
title:   "A More Productive Kerry Beagle"
date:    2007-02-05
authors:
  - beineri
slug:    more-productive-kerry-beagle
---
Above is the <a href="http://linuxappfinder.com/blog/a_more_productive_kerry_beagle">title of a blog</a> I found linked at <a href="http://tuxmachines.org/">tuxmachines.org</a> (btw great site with a daily mix of desktop related news and blogs). Its author describes the interface changes in the <a href="http://www.kde-apps.org/content/show.php?content=36832">latest Kerry Beagle</a> release and likes them very much. He also suggests some improvements: I don't think Beagle supports "blank searches" but the other two items he mentions you will likely see soon in a Kerry release. :-)<!--break-->