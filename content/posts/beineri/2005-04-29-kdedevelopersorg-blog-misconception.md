---
title:   "The kdedevelopers.org Blog Misconception"
date:    2005-04-29
authors:
  - beineri
slug:    kdedevelopersorg-blog-misconception
---
<a href="https://blogs.kde.org/">kdedevelopers.org</a> hosts different content categories: polls, stories and blogs. As user roles "authenticated user" (working email address, hello spammers) and "KDE developer" (has to be manually verified as such) exist. Every "authenticated user" can submit stories to the story submission queue on which "KDE developer"s have to vote about whether they should be published - in practice nobody does. Stories even don't appear on the kdedevelopers.org homepage anymore, for story publications you better go to <a href="http://dot.kde.org/">dot.kde.org</a>. 

Unfortunately KDE contributors wanting to blog seem to regularly think that kdedevelopers.org only hosts blogs and that they as soon that they have an account created can blog. Wrong! "Create Content/Story" doesn't create a blog entry, read the explanation what "story" means! Those self-assumed bloggers then wonder where their posting disappeared to, get disappointed and don't blog, move to another blog provider (requiring one more account to be able to comment) or even start to write their own blog site (possibly without comment possibility).

You have to contact Geiseri or me to get the "KDE developer" role assigned to your account. This will allow you to moderate comments, access the story submission queue, create polls, upload images and to blog with "Create Content/Personal Blog Entry".
<!--break-->