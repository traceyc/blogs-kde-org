---
title:   "openSUSE 10.3 Home Stretch & KDE Four Live 0.4.1"
date:    2007-09-16
authors:
  - beineri
slug:    opensuse-103-home-stretch-kde-four-live-041
---
All efforts these days concentrate on openSUSE 10.3, the one CD installation medias and Live CD installer testing: the release candidate is <a href="http://en.opensuse.org/Roadmap/10.3">planned for Thursday</a>. The German box version is already listed and can be pre-ordered (eg <a href="http://www.edv-buchversand.de/suse/product.php?cnt=product&id=sus172">SUSE Shop</a>, <a href="http://www.amazon.de/Koch-Media-GmbH-openSUSE-10-3/dp/B000V7FCSC/">Amazon.de</a>). It will include a total of 16GB software for 32 and 64bit on two dual-layer DVDs this time!

I updated <a href="http://news.opensuse.org/?p=219">the KDE4 of openSUSE 10.3</a> packages a last time with an SVN snapshot of this week. And didn't forget to create a new "<a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a>" CD with them which works slightly better (eg more plasmoids work) than the previous version built from the KDE 4.0 Beta 2 release tarballs.
<!--break-->