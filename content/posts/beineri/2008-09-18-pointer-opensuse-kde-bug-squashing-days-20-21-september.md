---
title:   "Pointer: openSUSE KDE Bug Squashing Days (20-21 September)"
date:    2008-09-18
authors:
  - beineri
slug:    pointer-opensuse-kde-bug-squashing-days-20-21-september
---
The openSUSE KDE team is holding a <a href="http://news.opensuse.org/2008/09/11/opensuse-kde-bug-squashing-days-20-21-september/">Bug Squashing event</a> to work through the KDE bug reports in bugzilla.novell.com this weekend. Among the goals are reporting non-openSUSE specific bugs to bugs.kde.org and closing duplicates to reports within bugs.kde.org or bugzilla.novell.com - hope to see you! :-)