---
title:   "Multiple Patterns in Build Service Projects"
date:    2007-12-14
authors:
  - beineri
slug:    multiple-patterns-build-service-projects
---
Defining more than one pattern for a <a href="http://en.opensuse.org/Build_Service">Build Service</a> project finally works after some bugfixes by Michael. :-)

[image:3152 size=preview hspace=50]

Above you see how we use it in the <a href="http://download.opensuse.org/repositories/KDE:/KDE4/">KDE:KDE4 project</a>, eg "KDE 4.0 Build Dependencies" lists all packages you need to have installed for compiling KDE 4.0 from source.