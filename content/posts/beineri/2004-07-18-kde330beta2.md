---
title:   "KDE_3_3_0_BETA_2"
date:    2004-07-18
authors:
  - beineri
slug:    kde330beta2
---
That's the magic CVS tag which allows you to pull the KDE version from CVS which is supposed to become KDE 3.3 Beta 2. Start your build (perhaps with the help of <a href="http://grammarian.homelinux.net/kdecvs-build/">kdecvs-build</a>) and to write your "KDE 3.3 Preview" today so that your story is finished when KDE 3.3 Beta 2 will be announced. :-)

Coolo also created the release tarballs and made them available to the packagers for testing and building binary packages. Btw, don't listen to individuals who tell you that release tarballs are not tested at all! And let's hope that the delay for binary packages is worth the wait: For Beta 1 there were after over one week only binary packages created by Yoper and SUSE (only for last two distribution releases). Creating packages outside their distribution release schedules doesn't seem to have a high priority among distributors. Mandrake eg only recently contributed packages for stable KDE 3.2.3, weeks after its release.

Perhaps KDE should stop the waiting for binary packages completely and release right away (together with xdelta diffs and Konstruct) once the tarballs compile - at least for test releases with their tight release schedule.
<!--break-->