---
title:   "Novell / openSUSE at LinuxTag 2006"
date:    2006-04-30
authors:
  - beineri
slug:    novell-opensuse-linuxtag-2006
---
Joining the announcements about presences at <a href="http://www.linuxtag.org/2006">LinuxTag 2006</a> in Wiesbaden next week, let me spread that Novell will be back this year with a booth (hall 9, number 918). <a href="http://en.opensuse.org/">The openSUSE project</a> will be presented, <a href="http://www.edv-buchversand.de/suse/product.php?cnt=product&id=sus939">SUSE Linux 10.1</a> and the <a href="http://www.novell.com/suse/">SUSE enterprise product line</a>. Additionally thursday is <a href="http://www.linuxtag.org/2006/de/besucher/programm/freies-vortragsprogramm/donnerstag.html">the openSUSE day</a> with 9 talks about the openSUSE project and SUSE Linux - to be found in the free conference program. If you're interested in <a href="http://www.suse.de/jobs/">working</a>, doing an intership or writing your diploma thesis at SUSE research and development department visit the Novell booth on Thursday or Friday when the human resource department is available for questions.

Of the more KDE involved SUSE staff me, Will Stephenson, Cornelius Schumacher, Adrian Schröter and Duncan Mac-Vicar will be <a href="http://wiki.kde.org/tiki-index.php?page=Linuxtag2006">around between one and four days</a>. Catch us at the Novell booth, the KDE booth, at the KDinner or somewhere else :-)...
<!--break-->