---
title:   "SUSE 10.0 Release Candidate"
date:    2005-09-08
authors:
  - beineri
slug:    suse-100-release-candidate
---
<img align=right class="showonplanet" src="http://www.opensuse.org/images/7/76/Konqi-opensuse-small.jpg"> The <a href="http://lists.opensuse.org/archive/opensuse-announce/2005-Sep/0002.html">first release candidate</a> of <a href="http://www.opensuse.org/Product_Highlights">SUSE Linux 10.0</a> Open Source edition has been published today. You can <a href="http://www.opensuse.org/Download">download</a> it at <a href="http://www.opensuse.org">openSUSE.org</a>. The KDE task force team at SUSE, consisting of Coolo, Dirk, kAdrian, Seli, Will and me has squashed countless bugs for it. Early next week there will be nevertheless a second release candidate, to <a href="http://www.opensuse.org/Update_SUSE_to_next_version">which you can then update</a>, with more bugfixes. 
<!--break-->