---
title:   "#aKademy"
date:    2004-08-20
authors:
  - beineri
slug:    akademy
---
I'm attending the whole day the KDE e.V. meeting since 10am and it's still continuing (now 5.30pm). Luckily there was a long midday break with free delicious food (four possible choices including one vegetarian) at <a href="http://developer.kde.org/~binner/aKademy2004/day1/dscf0039.jpg">"Blauer Engel" restaurant sponsored by IBM</a>.<br>
Several attendants have their notebooks in front of them and are connected to Internet via cable or wifi and while listening or sometimes talking and voting in-between doing other stuff: Some watch the reactions to the KDE 3.3 release, read email, code or blog. :-) Others are chatting about what they hear on #aKademy channel at irc.kde.org. So if you want to participate remotely (recording/streaming is still being set up) or have questions about travel, location or organization consider to join and we may be able to help immediately.<br>
In short interesting but also exhausting - and the public 9 day program does not start until tomorrow. Let's hope everything works fine tomorrow with most attendees having been arrived and that the fun to work ratio increases.
<!--break-->