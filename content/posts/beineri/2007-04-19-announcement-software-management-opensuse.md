---
title:   "Announcement: Software Management for openSUSE"
date:    2007-04-19
authors:
  - beineri
slug:    announcement-software-management-opensuse
---
On SUSE Linux 10.1 it was required, on openSUSE 10.2 you can deselect it during installation, for openSUSE 10.3 it was planned to be made an optional install but <a href="http://lists.opensuse.org/opensuse-factory/2007-04/msg00137.html">yesterday Andreas Jaeger announced</a>:
<pre>
openSUSE is focusing on native software management by using YaST 
and libzypp, the package management library.
 
ZENworks Linux Management is Novell's solution for enterprise-class
resource management for desktops and servers. ZENworks components are
fully available and supported for SUSE Linux Enterprise based products
and not longer part of the openSUSE distribution.
 
Product development for both ZENworks and SUSE Linux Enterprise
concentrates on continued interoperability solutions for remote and
local software management.</pre>

PS: The next openSUSE 10.3 Alpha featuring no ZMD at all is planned for May 16.
<!--break-->