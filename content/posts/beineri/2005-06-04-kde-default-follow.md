---
title:   "\"KDE is the Default...\" Follow Up"
date:    2005-06-04
authors:
  - beineri
slug:    kde-default-follow
---
It's nice to hear that (big) companies actually check the number of Linux desktop deployments they are being told. Not good if then "200.000 desktops in Spain" are discovered as addition of some at a conference spread Live-CDs and some magazine accompanying CDs. Or some big proclaimed deployment in South America are actually Windows computers now setup to be able to dual-boot without anyone knowing on how many of them Linux is used after.
<!--break-->