---
title:   "Novell/SUSE Represented in KDE's Technical Working Group"
date:    2006-02-14
authors:
  - beineri
slug:    novellsuse-represented-kdes-technical-working-group
---
The KDE project <a href="http://dot.kde.org/1139614608/">has elected its first Technical Working Group</a>. Three Novell/SUSE employees are among the seven elected members (the others either didn't get elected, are busy with KDE e.V. board work or didn't candidate ;-) ). It seems you can still surprise some people with Novell/SUSE's continued strong KDE support hence this short note.
<!--break-->