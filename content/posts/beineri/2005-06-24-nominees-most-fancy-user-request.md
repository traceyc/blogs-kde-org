---
title:   "Nominees for Most Fancy User Request"
date:    2005-06-24
authors:
  - beineri
slug:    nominees-most-fancy-user-request
---
Good evening. Sometimes users file funny, far beyond the capabilities of KDE, or crazy wishes. And I don't talk about the "remove my neighbor country's flag" evergreen. And the nominees for "The Most Fancy User Request" award are:

<ul>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=34295">Bug 34295</a> titled 'No red spot on moon' from a time where the RedHat QA department was still concerned about KDE.</li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=106880">Wish #106880</a> titled 'KDE music download shop like Apple iTunes' for the best idea how to make KDE a successful business.</li>
<li>The <a href="http://lists.kde.org/?t=111942910200003&r=1&w=2">kde-devel thread</a> titled 'KDE development as a spectator sport!' proposing to introduce bets on eg whether a developer will lose SVN write access.</li>
</ul>

Do you feel that one submission is missing? Then quickly nominate it before the jury pronounces its verdict.
<!--break-->