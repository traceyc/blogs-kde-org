---
title:   "Distributor KDE Patches Collection"
date:    2004-09-28
authors:
  - beineri
slug:    distributor-kde-patches-collection
---
Did you always want to know what patches the distributions apply against vanilla KDE source? Did you ever receive weird bug reports with 'impossible' behavior or stacktraces for your application? Or wanted to forward port this feature you saw in screenshots, read about in reviews or used on a machine with that other distribution? But you had not the time to search for the sources or the bandwidth to download the complete source package just to get the patches?

Today I started a small <a href="http://ktown.kde.org/~binner/distributor-patches/">collection of distributor KDE patches</a>. Now you can browse what patches are applied, find easier the patch to forward port to KDE HEAD or discover what the users of a special distribution bugs. You can also download archives containing only the modifications and grep at home. At the moment the collection includes ArkLinux, Conectiva, Debian, Fedora, Mandrake and SUSE. Some distros only create a single large patch against the modules, for others you can already tell from the filename what it implements or what bug report it fixes.

You can mail me the URLs to the KDE source packages of other distributions which customize KDE and I may add them. Or even better send me a tarball with the patches so I can just dump it there. :-)
<!--break-->