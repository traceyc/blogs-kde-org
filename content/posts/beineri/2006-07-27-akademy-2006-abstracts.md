---
title:   "aKademy 2006 Abstracts"
date:    2006-07-27
authors:
  - beineri
slug:    akademy-2006-abstracts
---
After <a href="http://conference2006.kde.org/conference/program.php">the schedule</a> some days ago now also the abstracts for this year's upcoming <a href="http://conference2006.kde.org/">KDE Contributors Conference</a> are online. Let me point to the talks of my SUSE colleagues about <a href="http://conference2006.kde.org/conference/talks/16.php">Network Status Support in KDE and How to Use It</a> and our team project <a href="http://conference2006.kde.org/conference/talks/33.php">Kickoff - Start Menu Research</a> on which we spend much time during this too hot summer. Latter <a href="http://reverendted.wordpress.com/2006/07/25/post-from-nuremberg/">will be also topic</a> on <a href="http://www.novell.com/company/podcasts/openaudio.html">Novell Open Audio</a> soon.

Did you <a href="http://wiki.kde.org/tiki-index.php?page=Arrival%20%40%20aKademy%202006">already book your flight?</a>  :-)
<!--break-->