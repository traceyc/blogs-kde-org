---
title:   "openSUSE Build Service KDE:KDE4:* Repository Changes"
date:    2008-12-07
authors:
  - beineri
slug:    opensuse-build-service-kdekde4-repository-changes
---
<a href="http://en.opensuse.org/OpenSUSE_11.1">openSUSE 11.1</a> will reach Goldmaster status within the next days which also means that things move around in the <a href="http://en.opensuse.org/KDE/KDE4">KDE:KDE4</a> Build Service repositories:
<ul>
<li><a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/">KDE:KDE4:STABLE:Desktop</a> has been already updated to the KDE 4.1.3 packages as in openSUSE 11.1. We will use this repository to collect and test future online updates. Every Fix_is_Ready:11.1 patch should be added there. Please change your repositories from KDE:KDE4:Factory to KDE:KDE4:STABLE if you want to continue using 4.1.3 before we do next step.<br><br></li>

<li>As soon as SUSE Linux Enterprise 11 has been branched from Factory (maybe in one to two weeks) we will submit the KDE 4.2 Beta packages to openSUSE:Factory and they will appear in the <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Desktop/">KDE:KDE4:Factory:Desktop</a> repositories for older distribution releases.<br><br></li>

<li>When upstream has branched KDE 4.2 prior release (early January) and Trunk is open for 4.3 development then we will start to use <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE:/Desktop/">KDE:KDE4:UNSTABLE</a> again with regular KDE trunk snapshot updates.</li>
</ul>

We don't plan to have KDE 4.1.4 packages. If you know of important bug fixes/commits in 4.1 branch that should be in STABLE/cumulative KDE online update please notify us about it (bug report preferred).
<!--break-->
