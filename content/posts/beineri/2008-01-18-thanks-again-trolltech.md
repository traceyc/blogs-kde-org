---
title:   "Thanks Again Trolltech!"
date:    2008-01-18
authors:
  - beineri
slug:    thanks-again-trolltech
---
As announced just minutes ago at the <a href="http://www.kde.org/kde-4.0-release-event/">KDE 4.0 Release Event</a>, <a href="http://trolltech.com/company/newsroom/announcements/press.2008-01-18.1601592972">Trolltech’s Qt to be licensed under the GPL v3</a> - both Qt3 and Qt4. Thanks Trolltech, this saves the distributors and everyone quite some license compatibility mess headaches. :-)

"<i>I am very pleased that Trolltech has decided to make Qt available under GPL v3,</i>" commented Richard Stallman, author of the GPL and president of The Free Software Foundation. "<i>This will allow parts of KDE to adopt GPL v3, too. Even better, Trolltech has made provisions for a smooth migration to future GPL versions if it approves of them.</i>"
<!--break-->