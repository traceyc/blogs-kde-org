---
title:   "LinuxTag 2008 Upcoming"
date:    2008-05-11
authors:
  - beineri
slug:    linuxtag-2008-upcoming
---
In two and half weeks the likely biggest Linux Event kicks off: <a href="http://www.linuxtag.org/2008/en/">LinuxTag 2008</a> in Berlin. Four days of exhibition and more talks (the organizers say 240, German/English mixed) than ever before. I plan to be around all the time. :-)

On Wednesday everyone's darling, the multi-headed president of KDE e.V. and <a href="http://www.movieweb.com/news/37/7637.php">the galaxy</a>, <a href="http://trolltech.com/images/company/events/dd07/aseigo/image">Aaron Seigo</a> will give a <a href="http://www.linuxtag.org/2008/en/conf/events/vp-mittwoch/vortragsdetails.html?talkid=13">keynote about KDE4</a>. On Friday is a <a href="http://www.linuxtag.org/2008/en/conf/events/vp-freitag.html">day-long track with KDE talks</a>, on <a href="http://news.opensuse.org/2008/05/08/linuxtag-2008-2/">Saturday is openSUSE day</a> and there are separate talks about <a href="http://www.linuxtag.org/2008/de/conf/events/vp-samstag/vortragsdetails.html?talkid=194">Amarok</a> and <a href="http://www.linuxtag.org/2008/de/conf/events/vp-donnerstag/vortragsdetails.html?talkid=94">Kontact</a> planned. Additionally there will be of course KDE and openSUSE booths all the time.

Special tip: one can <a href="http://www1.messe-berlin.de/vip8_1/website/MesseBerlin/htdocs/www.it-profits/index_d/index.html">travel for 59 Euro</a> from everywhere in Germany with ICE to LinuxTag/IT Profits and back.
<!--break-->