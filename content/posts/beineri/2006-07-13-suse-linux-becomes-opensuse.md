---
title:   "SUSE Linux becomes openSUSE"
date:    2006-07-13
authors:
  - beineri
slug:    suse-linux-becomes-opensuse
---
The community was confused. The press was confused. The SUSE Linux Enterprise customers were confused. So it <a href="http://lists.opensuse.org/archive/opensuse-announce/2006-Jul/0002.html">has been decided</a> that the next release of what has been famous as SUSE Linux distribution will be renamed to openSUSE 10.2 to end this confusion. Hopefully everyone will be happy now?
<!--break-->