---
title:   "KDE to Stay on Novell Linux Desktop and SUSE Linux Enterprise Server"
date:    2005-11-11
authors:
  - beineri
slug:    kde-stay-novell-linux-desktop-and-suse-linux-enterprise-server
---
Maybe you haven't heard this yet as international pr[ess] is slow: After last week's reports that Novell plans to not ship the KDE desktop anymore on Novell Linux Desktop and SUSE Linux Enterprise Server products, the company got lots of feedback from its customers. And as is <a href="http://www.heise.de/english/newsticker/news/66068">reported</a> in <a href="http://derstandard.at/?url=/?id=2239023">the press</a> (German) Novell listens to them and reconsidered its desktop strategy: Novell will continue to ship KDE also in the enterprise products as supported option and it's said that it will be easy to choose KDE as your desktop. :-)
<!--break-->