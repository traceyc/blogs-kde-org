---
title:   "KDE 3.5 Beta 2: Klax Live CD, SUSE Packages and Konstruct"
date:    2005-10-18
authors:
  - beineri
slug:    kde-35-beta-2-klax-live-cd-suse-packages-and-konstruct
---
<a href="http://www.kde.org/announcements/announce-3.5beta2.php">KDE 3.5 Beta 2</a> has been announced and the full range of testing possibilities is available: My small <a href="http://developer.kde.org/~binner/klax/devel.html">&quot;Klax&quot; Live-CD</a> has been updated to KDE 3.5 Beta 2 and <a href="http://www.koffice.org/announcements/announce-1.4.2.php">KOffice 1.4.2</a>. The CD made <a href="http://distrowatch.com/2981">news on Distrowatch</a> and led to some <a href="http://shots.osdir.com/slideshows/slideshow.php?release=474&slide=3&title=klax+kde+3.5+beta+2+screenshots">screenshots on  OSdir.com</A>.  For building from source the &quot;Unstable KDE&quot; edition of the <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> script has been updated. And there are <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta2/SuSE/">packages for SUSE Linux 10.0 and 9.3</a> available (packages for 9.2 and 9.1 do also exist and may be uploaded if there is demand and once the KDE mirrors have fetched the current distributions' packages). And of course KDE 3.5 Beta 2 will be also on <a href="http://www.opensuse.org/">SUSE Linux 10.1 Alpha 2</a> which <a href="http://www.opensuse.org/index.php/Roadmap">will be published on Thursday</a>.
<!--break-->