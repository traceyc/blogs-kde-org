---
title:   "Special 'Planet KDE' and 'Developer Journals' Tags"
date:    2005-08-11
authors:
  - beineri
slug:    special-planet-kde-and-developer-journals-tags
---
It's nice to see the amount of people on <a href="http://planetkde.org/">Planet KDE</a> <a href="http://blogs.kde.org/node/1258">rapidly growing</a>, indicating a thriving KDE developer community. :-) Let me point out two special tags which some blog newbies don't seem to be aware off:

Images linked within your blog are not shown on Planet KDE unless you include '<a href="http://c133.org/blog/tech/KDE/youre_not_gonna_do_it_are_you.html">class="showonplanet"</a>' as tag within the image link. I think that's both to protect you from from too much traffic which may be caused by Planet KDE readers by default and to protect Planet KDE being swamped with all your last vacation photos. So please use it wisely and not for huge images.

If your blog is hosted on <a href="https://blogs.kde.org/">Developer Journals</a> you may will notice that by default only a trimmed version, likely the first paragraph, is exported for syndication. You can make it export all your lines (assuming that it's not mega huge) by inserting the delimiter '&lt;!--break--&gt;' under your last line as described when you "Preview" your post. 

The last may also happen for other blog software, please don't ask me what tag to use then. If you don't export your whole blog please watch how your export will look like and phrase your first sentence/paragraph such that it's obvious that there is more text if one follows the link from Planet KDE or RSS reader.
<!--break-->