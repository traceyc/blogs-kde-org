---
title:   "KDE 3.5 YaST-Repository"
date:    2005-11-20
authors:
  - beineri
slug:    kde-35-yast-repository
---
<a href="http://dot.kde.org/1131747786/">KDE 3.5 RC 1</a> has entered the <a href="http://www.novell.com/linux/download/linuks/index.html">supplementary/KDE repository</a> on ftp.suse.com and <a href="http://www.novell.com/products/suselinux/downloads/ftp/int_mirrors.html">its mirrors</a>. If you previously missed YaST definitions next to the packages hosted on ftp.kde.org, here you go. <a href="http://www.opensuse.org/">openSUSE.org</a> has instructions <a href="http://www.opensuse.org/Add_Package_Repositories_to_YaST">how to add package repositories to YaST</a>. Don't forget to turn on "Refresh" for the installation source if you run SUSE Linux 10.0 so you will not miss any future updates.<!--break-->