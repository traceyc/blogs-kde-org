---
title:   "openSUSE Lizards: Creating Custom Feeds"
date:    2008-12-06
authors:
  - beineri
slug:    opensuse-lizards-creating-custom-feeds
---
This is likely interesting for more <a href="http://en.opensuse.org/Members">openSUSE Members</a> blogging on <a href="http://lizards.opensuse.org">lizards.opensuse.org</a>: Yesterday Klaas asked me if he could make only <a href="http://lizards.opensuse.org/author/kfreitag/">his posts</a> to <a href="http://lizards.opensuse.org/category/desktop/kde/">the KDE category</a> appear on <a href="http://planetkde.org/">Planet KDE</a>. A short digging showed that WordPress allows to <a href="http://dailycupoftech.com/2007/07/25/creating-custom-wordpress-feeds/">create custom feeds</a>. So the solution was as as simple as
<pre>http://lizards.opensuse.org/?feed=rss&author=19&cat=5</pre>

You can find out the numerical IDS for categories by searching for "cat-item" in home page's HTML and for authors by looking at author links in the admin interface.
<!--break-->
