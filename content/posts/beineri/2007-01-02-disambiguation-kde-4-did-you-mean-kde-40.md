---
title:   "Disambiguation \"KDE 4\", did you mean \"KDE 4.0\"?"
date:    2007-01-02
authors:
  - beineri
slug:    disambiguation-kde-4-did-you-mean-kde-40
---
The last feature release of KDE is just over a year old, is well maintained and gains selected features about every two to three months. Still people long for the first release starting with the number 4 often called "KDE 4" - which is wrong. Let me try to disambigue: "KDE 4" refers to the whole life-time of the KDE 4.x based framework, like "KDE 3" does for now five years and six KDE 3.x releases. The "KDE 4.0" release will be just the first one in the "KDE 4" cycle.

"KDE 4" can be compared to "MacOS X" (although it will be an evolutionary update rather than as revolutionary as MacOS X was compared to System 9). "KDE 4.0" can be compared to "MacOS X 10.0", which was far from complete in term of frameworks and functionality compared to today's current MacOS X 10.4 release. The same will apply for KDE 4.0: not all features and goals the developers envision for KDE 4 will be implemented for KDE 4.0. The most important ones will be sorted out and will, some changes are possible and will be implemented later during KDE 4 and a few will turn out as impossible or even undesired.

There will be users (and young editors) who don't understand the difference of KDE 4 and KDE 4.0 and will call KDE 4.0 incomplete or a disappointment because it will not do all stuff they read about KDE 4 will be able to do and who expect the release cycle and development cycle to be something like "Microsoft Vista": a big dump of changes containing some new features to live with the next five years (and a lot of new features the competition introduced in the last five years).

KDE 4.0 will for sure not take full advantage of the potential of KDE 4 platform. Akonadi, Phonon & Co will show their power only as more backends and plugins become available. Can you remember when kparts and KIO slaves became essential in your daily work (eg fish slave creation)? It weren't the x.0 releases introducing them. Also KDE 4.0 will not contain all or the complete imminent frameworks of "KDE 4". Like much of the "social semantic desktop" functionality will be added only later, even beyond 4.1 - Nepomuk is a several year research project and nobody knows today what the final specification will be.
<!--break-->