---
title:   "An Exciting openSUSE Month"
date:    2008-06-07
authors:
  - beineri
slug:    exciting-opensuse-month
---
It has been several months since I blogged about <a href="http://blogs.kde.org/node/2932">exciting openSUSE stuff</a> happening (all about at the same time). The next few weeks enough long in the work things are on the home stretch to make June an openSUSE month: :-)

<ul>
<li>Our official <a href="http://forums.opensuse.org/">openSUSE Forums</a> will finally <a href="http://zonker.opensuse.org/2008/05/26/forums-update-still-working-towards-merged-forums/">go online next week</a> as a merger of three existing forums.</li>
<li>The release of <a href="http://en.opensuse.org/Build_Service">openSUSE Build Service</a> Milestone 1.0 <a href="http://en.opensuse.org/Build_Service/Roadmap">is imminent</a>. It introduces the technical means to allow <a href="http://en.opensuse.org/Build_Service/Collaboration">collaboration</a> for future openSUSE distro development.</li>
<li><a href="http://en.opensuse.org/OpenSUSE_11.0">openSUSE 11.0</a> will be released in less than two weeks with quite some major changes: a beautiful, simplified and fast <a href="http://news.opensuse.org/2008/06/05/sneak-peeks-at-opensuse-110-new-installer-with-stephan-kulow/">installation</a>; really fast and powerful <a href="http://news.opensuse.org/2008/06/06/sneak-peeks-at-opensuse-110-package-management-with-duncan-mac-vicar/">package management</a>; and it will be the first distribution to ship a basic usable and polished KDE4 desktop (Sneak Peeks story <a href="http://news.opensuse.org/category/sneak-peeks/">will appear here</a>)!</li>
</ul>

Besides those several stuff is progressing nicely: the <a href="http://lizards.opensuse.org/">openSUSE Lizards</a> blog site for <a href="http://en.opensuse.org/Members">openSUSE Members</a> is helping to grow <a href="http://planetsuse.org/">Planet SUSE</a>; our 'new' community manager <a href="http://zonker.opensuse.org/">Zonker</a> is calling for participation in the <a href="http://zonker.opensuse.org/2008/06/04/announcing-the-opensuse-marketing-team/">marketing team</a>; the <a href="https://users.opensuse.org/">User Directory</a> now handles membership applications; and also some other stuck projects/topics start to move again.
<!--break-->