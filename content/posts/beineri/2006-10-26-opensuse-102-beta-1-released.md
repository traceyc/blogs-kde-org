---
title:   "openSUSE 10.2 Beta 1 Released"
date:    2006-10-26
authors:
  - beineri
slug:    opensuse-102-beta-1-released
---
openSUSE 10.2 Beta 1 <a href="http://lists.opensuse.org/opensuse-announce/2006-10/msg00004.html">has been announced</a> marking the <a href="http://en.opensuse.org/Roadmap/10.2">begin of the feature freeze</a>. The release schedule btw fits this time nicely with many upstream releases: we have Kernel 2.6.18.1, X.Org 7.2rc1, KDE 3.5.5, KOffice 1.6, Firefox 2.0 Final, OpenOffice.org 2.0.4 Final, Python 2.5 and PHP 5.2rc on board (<a href="http://en.opensuse.org/Factory/News">other changes</a>). cups 1.2.5 is ready for late inclusion in Beta 2. When openSUSE 10.2 will be released in early December a <a href="http://en.opensuse.org/SUSE_Linux_Lifetime">2 year lifetime</a> will begin, months more than other free distributions offer.

[image:2483 align=right]On the right you can see a screenshot of the default KDE desktop with the SUSE-unique new start menu we developed and my improved Kerry frontend for Beagle. Also it shows the new KDE online update notification applet which doesn't require zmd as does the new console package tool zypper. You should really try the Beta 1 (and help to find all bugs) as I cannot describe all the tweaks and speed-ups done for the base system, package management and the KDE desktop. :-)
<!--break-->