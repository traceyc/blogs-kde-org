---
title:   "Distribution Development: Package Rebuilds"
date:    2007-11-28
authors:
  - beineri
slug:    distribution-development-package-rebuilds
---
I wondered several times in the past why Novell/SUSE people eg in Novell Open Audio interviews presented our internal autobuild system as something special. The relevant feature here is that both the legacy autobuild as well the new <a href="http://en.opensuse.org/Build_Service">openSUSE Build Service</a> have the functionality to rebuild all dependent packages everytime after a change. A new compiler version, a newly enabled security check, or an innocent looking change to a library can break larger parts of the distribution. <a href="http://en.opensuse.org/Factory_Distribution">openSUSE Factory</a> gets completely rebuilt every few days, and the same happens for older maintained products a bit more infrequently. It's an easy way to discover regressions and to prevent an inconsistent distribution so I couldn't imagine a distribution being created without such "feature".

Today I read the <a href="http://osnews.com/comment.php?news_id=18976&limit=no&threshold=-1">comment section of this story</a> where a user writes that Mandriva "<i>2007.1 Spring failed to rebuild all python defendant rpms after upgrading the core python install to 2.5</i>" and that the official Mandriva comment was along the line of "<i>Well you don't expect us to rebuild every package in the distro against each other for every release, can you?"</i>. Hard to believe, or? Adam Williamson of Mandriva jumped into the discussion with "<i>For 2008, we (well, it was about 90% me...) rebuilt the entirety of /main, which had never been done before. For 2008.1 I'm hoping to get /contrib done too. But it would be nice to have an automated rebuild of everything"</i>. So Mandriva doesn't manage to get its packages rebuilt once during a whole release cycle while SUSE does it every few days!?

Most strange quote of Adam: "<i>we already have a buildsystem that does everything useful that the openSUSE system does</i>". Really? :-)

How does the Debian/Ubuntu build system work? How does Fedora's Koji build the distribution?

<b>Update</b>: Really a coincidence, Novell Open Audio is bringing a <a href="http://www.novell.com/feeds/openaudio/?p=186">new episode about autobuild/build service</a> just today.
<!--break-->