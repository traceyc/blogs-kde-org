---
title:   "New KDE Live-CD Release Brings Back Desktop Functionality"
date:    2009-04-01
authors:
  - beineri
slug:    new-kde-live-cd-release-brings-back-desktop-functionality
---
There have been <a href="http://dot.kde.org/">endless complaints</a> about the KDE4 desktop shell missing certain functionalities like being able to have different wallpapers on each virtual desktop. The <a href="http://en.opensuse.org/KDE/">openSUSE KDE</a> team has now listened and worked hard to bring back all desktop functionality as you know it from KDE2. A <a href="http://home.kde.org/~binner/kde-four-live/KDE-Two-Live.i686-2.2.2-Build1.1.iso">technical preview in form of a Live-CD</a> (for i686 only) is now available. As additional bonus, Time Machine functionality was included too.