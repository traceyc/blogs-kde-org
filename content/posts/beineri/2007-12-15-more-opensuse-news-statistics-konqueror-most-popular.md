---
title:   "More openSUSE News Statistics: Konqueror Most Popular"
date:    2007-12-15
authors:
  - beineri
slug:    more-opensuse-news-statistics-konqueror-most-popular
---
After my <a href="http://blogs.kde.org/node/3132">previous blog</a> about <a href="http://news.opensuse.org/">openSUSE News</a> numbers I became curious and installed StatPress. Of course the site is mostly visited by SUSE Linux users (~53%), followed by just under 30% MS Windows users (XP outnumbers Vista by factor 7). But the positive surprise is that 40.4% use Konqueror to visit the site beating all Firefox versions by 0.3% (Internet Explorer 6+7 sum up to 12%). An outstanding number: 96% of the visitors use Google as their search engine. Among the most popular search terms are "opensuse 11[.0]" and "kde 4". :-)<!--break-->