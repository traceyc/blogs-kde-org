---
title:   "openSUSE 10.3 and KDE 4.0"
date:    2007-09-03
authors:
  - beineri
slug:    opensuse-103-and-kde-40
---
Did anyone miss a news splash these days about openSUSE 10.3 <b>not</b> shipping KDE 4.0 as default KDE desktop like <a href="http://liquidat.wordpress.com/2007/08/30/no-kde-4-for-fedora-8/">some other distro</a>? Or maybe not as we have not been telling everyone the last half year that we would. No, we didn't have a crystal ball but experience with our long time existing KDE4  packages. "<a href="http://fedoraproject.org/wiki/SIGs/KDE/Meetings/2007-07-10?action=AttachFile&do=get&target=fedora-kde-sig-2007-07-10.txt">So much for</a>" knowing what we're doing. ;-)

So what will we do? The unchanged plan is to install a selection of KDE4 applications <b>by default</b> on the KDE desktop of openSUSE 10.3 (some games, krfb and krdc) - also from the single i586 install CD. The openSUSE 10.3 repository will have the latest possible KDE 4.0 snapshot available. In the <a href="http://download.opensuse.org/repositories/KDE:/KDE4/">KDE:KDE4</a> Build Service project will we continue to have packages of weekly snapshots and the release. And of course we will have more "<a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a>" CDs. :-)
<!--break-->