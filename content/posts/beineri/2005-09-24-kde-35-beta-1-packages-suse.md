---
title:   "KDE 3.5 Beta 1 Packages for SUSE"
date:    2005-09-24
authors:
  - beineri
slug:    kde-35-beta-1-packages-suse
---
<a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta1/SuSE/">KDE 3.5 Beta 1 packages for SUSE</a> distributions versions 9.1 to 10.0 are available. These packages are unsupported and completely untested. We are nevertheless interested in package related <a href="http://www.opensuse.org/Submit_a_bug">bug reports</a> over at <a href="http://www.opensuse.org/">openSUSE</a> because KDE 3.5 Beta will be also the desktop of SUSE Linux 10.1 Alpha 1 which <a href="http://www.opensuse.org/Roadmap">will be published next week</a>. A few packages like kdemultimedia, kdenetwork and the translations are still missing but will be added once they are built.
<!--break-->