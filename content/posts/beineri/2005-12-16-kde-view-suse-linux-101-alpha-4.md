---
title:   "A KDE View on SUSE Linux 10.1 Alpha 4"
date:    2005-12-16
authors:
  - beineri
slug:    kde-view-suse-linux-101-alpha-4
---
With <a href="http://lists.opensuse.org/archive/opensuse-announce/2005-Dec/0003.html">SUSE Linux 10.1 Alpha 4</a> being announced let me list some KDE-related <a href="http://www.opensuse.org/Product_Highlights">interesting news</a> of it: it includes KDE 3.5 with several packages updated to last weekend's KDE 3.5 branch. The Qt 4 package's version is 4.1 release candidate 1. Digikam 0.8 and Taskjuggler 2.2 releases are of course not missing. 10.1 Alpha 4 includes a recent snapshot of KOffice trunk if you want to test eg Krita. And a work-in-progress version of a KDE frontend for NetworkManager is newly included. Expect some more good stuff in Beta 1. :-)<!--break-->