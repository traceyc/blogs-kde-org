---
title:   "Kepas - KDE Easy Publish and Share "
date:    2008-01-21
authors:
  - beineri
slug:    kepas-kde-easy-publish-and-share
---
Congratulations to Tom Patzig, currently serving in the <a href="http://en.opensuse.org/KDE/Team">openSUSE KDE Team</a>, for the first release of <a href="http://www.kde-apps.org/content/show.php/Kepas+-+KDE+Easy+Publish+and+Share?content=73968">Kepas</a>: "<i>Kepas is a KDE4 file transfer tool. It discovers your local LAN for buddies (KDNSSD) and lets you transfer files or Klipper entries from a tray icon or via drag'n'drop with the Kepas plasmoid.</i>" Please give him much feedback what to improve until 1.0! :-)

[image:3220 size=original hspace=50]

Credits for the original idea and implementation go to <a href="http://code.google.com/p/giver/">Giver</a> which was <a href="http://idea.opensuse.org/content/ideas/easy-file-sharing">born during our first Hack Week</a>.

The code is in KDE SVN within playground/network, hence openSUSE users can find it as part of playground-network.rpm (<a href="http://software.opensuse.org/ymp/KDE:KDE4/openSUSE_10.3/playground-network.ymp">openSUSE 10.3 One-Click Install</a>) in the KDE:KDE4 Build Service project.<!--break-->