---
title:   "Native Google Earth for Linux"
date:    2006-06-13
authors:
  - beineri
slug:    native-google-earth-linux
---
We knew that some big companies like Adobe or Google developed some of their applications for MS Windows with the Qt toolkit. But they didn't take full advantage of Qt's cross-platform sweetness. Until now. Google released yesterday <a href="http://earth.google.com/">Google Earth</a> Beta 4 and if you look closely at <a href="http://earth.google.com/download-earth.html">their download page</a> there is a Linux version available. This is not using some Wine wrapper as Picasa before but is a native Qt 3 application. Time for other companies to wake it? <a href="http://www.adobe.com/products/photoshopalbum/">Hello Adobe</a>!
<!--break-->