---
title:   "Some openSUSE 10.3 Hints"
date:    2007-10-06
authors:
  - beineri
slug:    some-opensuse-103-hints
---
Let me share some hints for openSUSE 10.3 which just came to my mind:

<ul>
<li>If you use GNOME and eg don't like the <i>yast2-gtk</i> package selector or miss the powerful Qt package selector, install the <i>yast2-qt</i> package and set in /etc/sysconfig/yast2 the WANTED_GUI variable to "qt".</li>
<li>If you want the Crystal-style YaST icons back, replace the <i>yast2-theme-openSUSE</i> with the <i>yast2-theme-openSUSE-Crystal</i> package.</li>
<li>If you miss the photo wallpapers, they have been moved to the <i>desktop-data-SuSE-extra</i> sub-package.</li>
<li>For really old wallpapers, think SuSE Linux 9.1, install the <i>gos-wallpapers</i> package.</li>
<li>And this seems to be the biggest complaint of some people: you can make the Geeko of the KDE start menu roll its eyes again:
<code>kwriteconfig --file kickerrc --group General --key KickoffDrawGeekoEye true"</code></li>>
</ul>
<!--break-->