---
title:   "Kickoff: Talk, Web Page, Source Code"
date:    2006-09-25
authors:
  - beineri
slug:    kickoff-talk-web-page-source-code
---
Yesterday, on the second conference day of <a href="http://conference2006.kde.org/">aKademy 2006</a>, the Kickoff talk was given by Coolo. It was well attended and received. Many questions afterwards including if simple calculations in the search line work like in mini cli (yes, they do). I took <a href="http://developer.kde.org/~binner/kickoff/akademy2006/">some few photos of the talk</a>. It was video-taped like all others talks and will be available for download later. The <a href="http://en.opensuse.org/Kickoff">Kickoff page in the openSUSE Wiki</a> continues to fill up with goals of the project, the findings of the usability study and description of the current implementation. Some people asked and it seems others are also not aware that the source code of Kickoff is hosted and developed within KDE's SVN for some weeks: you can find it in <a href="http://websvn.kde.org/branches/work/suse_kickoff/">branches/work/suse_kickoff/</a> - so get it and play with it!
<!--break-->