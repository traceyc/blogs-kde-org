---
title:   "openSUSE Survey Online"
date:    2007-02-18
authors:
  - beineri
slug:    opensuse-survey-online
---
The <a href="http://en.opensuse.org/">openSUSE project</a> is interested in knowing how you feel about the openSUSE project and <a href="http://en.opensuse.org/OpenSUSE_News/10.2-Release">the openSUSE 10.2 distribution</a>. Please take a few minutes to <a href="http://www.novell.com/productsurveys">fill out this survey</a> until April 30, 2007. You can enter in a drawing for some hardware gadgets as thank you for taking the survey.<!--break-->