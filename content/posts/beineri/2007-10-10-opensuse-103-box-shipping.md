---
title:   "openSUSE 10.3 Box Shipping"
date:    2007-10-10
authors:
  - beineri
slug:    opensuse-103-box-shipping
---
The <a href="http://en.opensuse.org/Buy_openSUSE">openSUSE 10.3 box</a> is shipping! Some subscribers seem to have received it already yesterday, today I received my employee copy (German version). I'm pleasantly surprised that it didn't take this time over one month after the release, and that the two DVDs are safe in a plastic cover:

[image:3021 size=preview hspace=100]

I hope that everyone who qualified through their contribution to this release with either code, translations or something else will receive their free box soon (English version may take a bit longer, dunno). :-)