---
title:   "openSUSE Survey Results"
date:    2007-05-03
authors:
  - beineri
slug:    opensuse-survey-results
---
The <a href="http://blogs.kde.org/node/2680">openSUSE Survey</a> results are in: thanks to the over 27k people who participated. And over 70% of those use the best desktop environment! :-) Read <a href="http://files.opensuse.org/opensuse/en/4/49/OpenSUSE_102_survey.pdf">the complete results</a> (PDF) if you have doubts which is it...<!--break-->