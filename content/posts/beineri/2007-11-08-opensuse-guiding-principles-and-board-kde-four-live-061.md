---
title:   "openSUSE: Guiding Principles and Board; KDE Four Live 0.6.1"
date:    2007-11-08
authors:
  - beineri
slug:    opensuse-guiding-principles-and-board-kde-four-live-061
---
As of today the openSUSE project has <a href="http://en.opensuse.org/Guiding_Principles">the Guiding Principles</a> finally <a href="http://news.opensuse.org/?p=509">taking effect</a>. Everyone can express support for them on the new <a href="https://users.opensuse.org/">openSUSE User Directory</a> site. Also the first <a href="http://news.opensuse.org/?p=498">openSUSE board has been appointed</a> by Novell. Congratulations to all members, especially the ones from the community. :-)<br>
Not so exciting, I have uploaded a new <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> CD containing 3.95.2 snapshot packages.
<!--break-->