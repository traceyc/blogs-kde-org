---
title:   "Don't Neglect kde-announce!"
date:    2005-07-28
authors:
  - beineri
slug:    dont-neglect-kde-announce
---
If you're the maintainer of a non-minor KDE application please don't think that posting your release to <a href="http://kde-apps.org/">kde-apps.org</a> is sufficient. Consider also to post a nice announcement including what your application is about and the highlights of the release to the <a href="https://mail.kde.org/mailman/listinfo/kde-announce">kde-announce mailing list</a>. Many news editors and magazine writers only read kde-announce and don't visit kde-apps.org! As an example see LWN of last week: <a href="http://lwn.net/Articles/144040/">KDE</a> v <a href="http://lwn.net/Articles/143873/">GNOME</a> announces.<br>
Outside KDE community the <a href="http://freshmeat.net/">Freshmeat</a> application index is the most important site to promote your application.
<!--break-->