---
title:   "\"KDE 3.5 on SUSE Linux 10\" for VMware Player"
date:    2006-01-02
authors:
  - beineri
slug:    kde-35-suse-linux-10-vmware-player
---
Announcing something which has been cooking already last year but got delayed by POTS bandwidth shortage over the holidays:<br>

VMware released a player for free with which you can run pre-built virtual machines. Somehow their virtual machine center offers under &quot;Novell&quot; only SLES and NLD images. :-? I created an image which contains a standard KDE desktop installation of SUSE Linux 10.0 OSS, upgraded to KDE 3.5 including KOffice 1.4.2 excluding non-KDE applications. It's a fully working installation so don't forget to install and try some additional KDE applications - and to run the security update when you're asked to. ;-)<br>
You can <a href="http://developer.kde.org/~binner/vmware/">download it from developer.kde.org</a> archived either as 588 MB flavor for Microsoft Windows or 537 MB for Linux.
<!--break-->