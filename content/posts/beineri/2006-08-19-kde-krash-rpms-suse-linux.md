---
title:   "KDE \"Krash\" RPMs for SUSE Linux"
date:    2006-08-19
authors:
  - beineri
slug:    kde-krash-rpms-suse-linux
---
The important warning first: this is only for developers and not openSUSE 10.2 stuff! KDE has released a <a href="http://dot.kde.org/1155935483/">first development snapshot of KDE4</a> and <a href="http://software.opensuse.org/download/repositories/KDE:/KDE4/">the KDE:KDE4 project in the openSUSE Build Service</a> has RPMs of qt, kdelibs, kdepimlibs and kdebase for SUSE Linux 10.0 and up. These are raw RPMs of the compiled stuff, not integrated with the distribution at all (won't install to /usr, won't let you choose the session from kdm etc.). Again, these snapshot and the RPMs are only for developers. If you don't know (or are able to figure out from kde.org documentation) how to change a test user account to start it then better don't consider to download.
<!--break--> 