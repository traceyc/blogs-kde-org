---
title:   "Kerry Beagle Bits"
date:    2006-11-19
authors:
  - beineri
slug:    kerry-beagle-bits
---
Last week I released <a href="http://kde-apps.org/content/show.php?content=36832">Kerry Beagle 0.2</a> in preparation for <a href="http://en.opensuse.org/Factory/News">openSUSE 10.2</a> (whose release candidate is <a href="http://en.opensuse.org/Roadmap/10.2">scheduled for Thursday</a>). I think this version is a nice improvement over version 0.1: new interface, more supported hit types and a more complete configuration module than the original beagle-settings. Thanks must go to Debajyoti Bera for adding more Beagle KDE backends (KAddressbook, KNotes, KDE Bookmarks), and my SUSE colleagues Robert Lihm for the nice icons and Sigi Olschner for being beta tester and giving usability advice. Finally thanks to the KDE translators for providing translations for the first time.

The older version 0.1 was recently mentioned in <a href="http://linuxappfinder.com/blog/linux_desktop_search">this "Linux Desktop Search" overview</a>. And while being at Linux World Expo Cologne I read news about Xandros pre-announcing their "Xandros Desktop Professional" including desktop search. So I walked over to the Xandros booth to play with their current development version: it turned out that they use Beagle 0.2.10 and Kerry - however only version 0.1. The next distribution <a href="http://wiki.freespire.org/index.php/Freespire_Roadmap">considering the inclusion of Kerry</a> seems to be Freespire. :-)
<!--break-->