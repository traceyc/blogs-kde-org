---
title:   "Pointer: KDE:KDE4:Factory:Desktop for 11.0 users"
date:    2008-09-16
authors:
  - beineri
slug:    pointer-kdekde4factorydesktop-110-users
---
As there seem to be people who continue, despite being told about dependency errors and packages that cannot be upgraded, to install newer KDE packages (and then wonder about a broken desktop) I want to point out this <a href="http://lists.opensuse.org/opensuse-kde/2008-09/msg00052.html">important service information</a> for users of the KDE:KDE4:Factory:Desktop repository in the openSUSE Build Service.