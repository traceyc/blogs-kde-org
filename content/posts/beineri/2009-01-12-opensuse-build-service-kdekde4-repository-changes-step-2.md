---
title:   "openSUSE Build Service KDE:KDE4:* Repository Changes, Step 2"
date:    2009-01-12
authors:
  - beineri
slug:    opensuse-build-service-kdekde4-repository-changes-step-2
---
A follow-up to the <a href="http://blogs.kde.org/node/3794">previous post</a>, the <a href="http://en.opensuse.org/KDE4">KDE:KDE4:Factory:Desktop repositories</a> now contain KDE 4.2 development snapshot (4.1.87) packages. This is also the repository that will contain KDE 4.2 RC1, KDE 4.2 and the first bug fix releases of KDE 4.2 in near future.

If you have been using the KDE:KDE4:UNSTABLE:* repositories for the last weeks you likely want to change them to KDE:KDE4:Factory:* + corresponding KDE:KDE4:Community repository NOW!

The next packages that will appear in the KDE:KDE4:UNSTABLE:* repositories will be snapshots from trunk representing very early KDE 4.3 development.
<!--break-->
