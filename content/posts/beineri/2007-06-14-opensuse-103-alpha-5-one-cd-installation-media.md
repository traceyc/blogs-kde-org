---
title:   "openSUSE 10.3 Alpha 5: One CD Installation Media"
date:    2007-06-14
authors:
  - beineri
slug:    opensuse-103-alpha-5-one-cd-installation-media
---
openSUSE 10.3 Alpha 5 was <a href="http://lists.opensuse.org/opensuse-announce/2007-06/msg00004.html">announced today</a> (<a href="http://en.opensuse.org/Factory/News#Changes_between_openSUSE_10.3_Alpha_4_and_Alpha_5">major changes</a>): most exciting novelty are additional one CD installation media giving you a rather complete English desktop. Compared to the default desktop installations from DVD most of the missing applications are games. Before installation (and of course after) from these medias one can register online repositories to get the full default SUSE desktop experience.

The 700MB KDE CD (<a href="http://download.opensuse.org/distribution/10.3-Alpha5/iso/cd/openSUSE-10.3-Alpha5-KDE-i386.iso">download</a>, <a href="http://developer.kde.org/~binner/openSUSE-10.3-Alpha5-KDE-i386.packages">package list</a>) contains almost all default KDE applications, including some KDE 4 Games, as well as Compiz, Firefox, Gimp and OpenOffice.org. Some few applications (eg Kaffeine and Konversation) had to be skipped for Alpha 5 but will be on the Alpha 6 CD - alone today we managed to gain over 10MB space on the CD by splitting packages more and fixing packaging errors. And the OOo.rpm is not even splitted yet... :-)
<!--break-->