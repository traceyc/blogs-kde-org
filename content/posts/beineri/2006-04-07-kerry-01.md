---
title:   "Kerry 0.1"
date:    2006-04-07
authors:
  - beineri
slug:    kerry-01
---
[image:1819 align=right]With the <a href="http://en.opensuse.org/Roadmap">upcoming SUSE Linux 10.1 Release Candidate</a> (really, no kidding!) and no further changes allowed for it I thought it was time to <a href="http://www.kde-apps.org/content/show.php?content=36832">release Kerry 0.1</a> including translations for 18 languages done by the Novell translation teams. The last days I already created <a href="http://en.opensuse.org/Kerry">a small homepage</a> in <a href="http://en.opensuse.org/">the openSUSE wiki</a> and took <a href="http://developer.kde.org/~binner/kerry/">some new screenshots</a>. The source is now <a href="http://websvn.kde.org/trunk/kdereview/kerry/">imported into KDE's SVN</a>, right next to <a href="http://websvn.kde.org/trunk/kdereview/knetworkmanager/">the one of knetworkmanager</a>.
<!--break-->