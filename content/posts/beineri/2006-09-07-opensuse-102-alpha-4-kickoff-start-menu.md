---
title:   "openSUSE 10.2 Alpha 4 with Kickoff Start Menu"
date:    2006-09-07
authors:
  - beineri
slug:    opensuse-102-alpha-4-kickoff-start-menu
---
<a href="http://lists.opensuse.org/opensuse-announce/2006-09/msg00000.html">openSUSE 10.2 Alpha 4 has been released</a> and it includes <a href="http://blogs.kde.org/node/2283">the new Kickoff start menu</a> for KDE:<br>
<img src="http://home.kde.org/~binner/kickoff/alpha4.png" align="middle"><br>
As you can see we changed some things because of the usability tests performed with the prototype: some users didn't find the search functionality, so we made it appear more prominent. Users didn't use the built-in menu help, so we removed it completely. Users didn't discover "Lock Screen" on the main slab so we moved it into the "Leave" tab and decided to put additionally the lock/lockout applet by default into the right panel corner. The "All Applications" browser gained an all-columns back button for easier browsing.

The version in openSUSE 10.2 Alpha 4 has some known flaws which were already fixed in KDE's SVN, eg it doesn't switch to the "My Favorites" tab when the menu opens. If you don't like the new menu at all, right clicking on the panel menu button allows you to easily switch back to the standard (but SUSE improved) KDE menu style.
<!--break-->