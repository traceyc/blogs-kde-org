---
title:   "LinuxTag 2007; Novell Sponsorship"
date:    2007-06-05
authors:
  - beineri
slug:    linuxtag-2007-novell-sponsorship
---
I'm back from vacation / LinuxTag 2007: met many openSUSE, Trolltech, KDE and GNOME people - more than expected. Jump over to <a href="http://home.kde.org/~binner/LinuxTag2007/">some pictures</a>. On other news, Novell will be Silver sponsor of both <a href="http://akademy.kde.org/">aKademy</a> and <a href="http://guadec.org/">GUADEC</a> this year. I will only visit aKademy though.<!--break-->