---
title:   "KDE - Defining the Linux Desktop for Ten Years!"
date:    2006-10-14
authors:
  - beineri
slug:    kde-defining-linux-desktop-ten-years
---
Today ten years ago the KDE project was founded. It certainly influenced the life of many people including mine. :-) The party at the <a href=http://events.kde.org/10years/>10 Years KDE event</a> to celebrate this anniversary started yesterday and ended today. At the end I was continously awake around 23 hours as I didn't want to miss any part of the event and had to arrive to Stuttgart in the morning first. Tackat <a href=http://dot.kde.org/1160834616/>wrote a nice report</a> about the whole day. Trolltech congratulated giving every attendee a t-shirt with the imprint "KDE - Defining the Linux Desktop for Ten Years!" increasing my geek t-shirt collection (how many do you have?). Also nice to meet <a href="http://www.kde.org/people/gallery.php">some early involved people</a> who are not actively contributing to KDE today anymore like Matthias H&ouml;lzer-Kl&uuml;pfel and Jono Bacon and to recognize how many people from KDE's first weeks are still active and have now their own or are leading employees of companies earning their money with Qt/KDE related development.<!--break-->