---
title:   "Freeze in the Mid of Summer"
date:    2004-07-15
authors:
  - beineri
slug:    freeze-mid-summer
---
Yesterday was the day the feature freeze for the KDE 3.3 release started and as expected some stuff was rushed into HEAD within the last allowed hours. Now the wait begins that everything compiles again and stabilizes so that KDE 3.3 Beta 2 can be tagged. Also yesterday the message freeze started which is unfortunate to say it polite. It's for sure not the best release schedule for people caring for user interface guidelines, the English proof-reading team, translators who right away started to translate and also don't expect every last-hour feature to be mentioned by the documentation writers in the handbooks.
<!--break-->