---
title:   "KDE 3.4 RC 1: Klax and Konstruct"
date:    2005-02-26
authors:
  - beineri
slug:    kde-34-rc-1-klax-and-konstruct
---
We <a href="http://lists.kde.org/?l=kde-www&m=110936367524033&w=2">lost the dot/wiki host</a> for the week-end thanks to the software wonder called Zope, so let me tell you for the case you don't know already that <a href="http://lists.kde.org/?l=kde-announce&m=110943281419778&w=2">KDE 3.4 Release Candidate 1</a> was released. I updated my small <a href="http://ktown.kde.org/~binner/klax/">Klax Live-CD</a> and also <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> so there will be hopefully someone testing it and reporting <a href="http://lists.kde.org/?l=kde-devel&m=110926701216829&w=2">showstoppers</a>. And no, not all known bugs will be fixed before the final release.<br>
Update: OSDir.com is the first to have <a href="http://shots.osdir.com/slideshows/slideshow.php?release=265&slide=1">screenshots</a> of KDE 3.4 RC 1.
<!--break-->