---
title:   "Some Interesting Summer of Code Ideas"
date:    2008-03-28
authors:
  - beineri
slug:    some-interesting-summer-code-ideas
---
Only three days left to apply as a student for the Summer of Code for <a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas">either KDE</a> or <a href="http://en.opensuse.org/Summer_of_Code_2008">openSUSE ideas</a>. So how about spending this week-end thinking what you could do this summer? Let me list some ideas I would like to see done:
<ul>
<li><a href="http://en.opensuse.org/Summer_of_Code_2008#Qt.2FKDE_Front-Ends_for_PackageKit">Qt/KDE Front-Ends for PackageKit</a></li>
<li><a href="http://en.opensuse.org/Summer_of_Code_2008#Qt.2FKDE_Front-Ends_for_PackageKit">YaST PolicyKit integration</a></li>
<li><a href="http://en.opensuse.org/Summer_of_Code_2008#YaST_PolicyKit_configuration_module">YaST PolicyKit configuration module</a></li>
<li><a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas#PolicyKit">PolicyKit Integration for KDE</a>
<li><a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas#KRunner">Using runners in Kickoff and a Xesam search runner</a>
<li><a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas#Plasma">International Clock plasmoid</a> (multiple clocks, kworldclock map, kweather infos)</li>
</ul>

</ul>
Or surprise with an application describing an own cool idea! But don't forget that already more than one application for some ideas exist so your application should really contain more than 5 sentences to get chosen: something about your skills, verbose plan what you want to do (not just copy'n'paste) and maybe some roadmap with planned milestones. And please keep in mind that the projects shall challenge you for 3 months, not just 1 week! :-)
<!--break-->