---
title:   "CeBIT 2008 Impressions"
date:    2008-03-09
authors:
  - beineri
slug:    cebit-2008-impressions
---
Yesterday I made an <a href="http://home.kde.org/~binner/CeBIT2008/">excursion to CeBIT</a>. Despite some major names missing and three halls staying closed it's the world's largest IT fair - and my legs remind me today of my marathon. Many halls were rather boring, the ones where the World Cyber Games took place were in opposite rather crowded. Dunno why watching others playing computer games is popular. Or the spectators were all their girl-friends who had free entrance on World Women Day. AMD wins the biggest bag contest. And many exhibitors seem to want to win in the 'Germany's Next Top-Hostess' contest.

openSUSE and KDE were present at the Novell booth and in the rather disappointing sized Linux Park:

<a href="http://home.kde.org/~binner/CeBIT2008/cimg2019.jpg"><img src="http://home.kde.org/~binner/CeBIT2008/thumbs/cimg2019.jpg" hspace="50"/></a><a href="http://home.kde.org/~binner/CeBIT2008/cimg2030.jpg"><img src="http://home.kde.org/~binner/CeBIT2008/thumbs/cimg2030.jpg" hspace="50"/></a><a href="http://home.kde.org/~binner/CeBIT2008/cimg2033.jpg"><img src="http://home.kde.org/~binner/CeBIT2008/thumbs/cimg2033.jpg" hspace="50"/></a>

Martin blocked the hall corridor with his openSUSE talks at the Novell theater. The last picture shows Michl giving a talk (in German) about the openSUSE project in the Linux Park forum. It will be repeated today at 15:15 CET and <a href="http://streaming.linux-magazin.de/aktuell.htm">streamed by Linux-Magazin</a> (records will be available later).
<!--break-->