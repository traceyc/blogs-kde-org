---
title:   "Lenovo Preloads with SUSE Linux Enterprise Desktop 10"
date:    2008-01-16
authors:
  - beineri
slug:    lenovo-preloads-suse-linux-enterprise-desktop-10
---
The Lenovo Thinkpads with SLED 10 SP1 preloaded <a href="http://shop.lenovo.com/SEUILibrary/controller/e/na/LenovoPortal/en_US/special-offers.workflow:ShowPromo?LandingPage=/All/US/Landing_pages/Info/08/Linux">start to ship these days</a>. Not yet available via web today you can already order <a href="http://lenovoblogs.com/insidethebox/?p=134#comment-37157">the first 6 configurations</a> (R61/T61 with 14.1" screens) via phone in the US. The availability in other countries can differ a bit, eg the R61 is said to start shipping in Germany on 22th January. More 14" T and R series models and selected T61 15" models with SLED offers will follow in February. :-)<!--break-->