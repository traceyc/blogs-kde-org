---
title:   "Maintainers' Release Duties"
date:    2004-09-07
authors:
  - beineri
slug:    maintainers-release-duties
---
Do you remember my blog entry about the software developer who put days of effort into a new version but then failed to raise the hour to properly announce his software (btw, meanwhile he has stopped its development without clear rationale)? Something similar can be watched in parts with some KDE software.<br><br>

Granted, the KDE procedure to release most applications only as part of big modules, packaged by the release coordinator and have them depend on the newest kdelibs doesn't enforce individual maintainers' release activities. Nevertheless there are some things you should really do as maintainer of an application/part within KDE: 

<ul>
<li>Increase the version number in time before a new release gets tagged. With the new kde-cvs-announce mailing list, to which every CVS account holder is forced to be subscribed, it will not be so easy anymore to miss those dates. If you increased the version number also update the version information for the corresponding component within KDE's bugzilla!</li>

<li>Keep a log of the applied bugfixes and new features for new versions and add them in time to www/announcements/changelogs/ (this location is not access restricted).</li>

<li>If you release your application also individually, either because you don't want to force your users to install the complete KDE module or for older KDE users after struggling with backward compatibility the whole time,  start to package in time. Usually you have at least one week before the official tarballs are made available and announced together with the first provided binary packages.</li>

<li>Update your application homepage if it exists! Sooner or later someone else (required write rights assumed) may update it but don't expect the same accuracy of information as if written by the application maintainer or someone deeper involved in the project.</li>

<li>Advertise the new application version at sites like kde-apps.org and freshmeat.net.</li>
</ul>

This said, I hope that the Kopete guys will finish the Kopete 0.9/KDE 3.2 tarball soon and I hope that the Kontact homepage will soon mention the new and great version 1.0 which is supposed to be a central part of the KDE 3.3 release.
<!--break-->