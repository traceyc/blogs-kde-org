---
title:   "openSUSE 11.0: Don't like Cashews?"
date:    2008-06-05
authors:
  - beineri
slug:    opensuse-110-dont-cashews
---
From the listening-to-users department, the openSUSE 11.0 Plasma desktop toolbox  contains more useful options as in KDE 4.0 but still many users want to hide it. Try this with our KDE packages on openSUSE 11.0 RC or equivalent from the build service: stop plasma (kquitapp plasma), search in ~/.kde4/share/config/plasma-appletsrc the "Containments" config group with the "plugin=desktop" entry, add "ShowDesktopToolbox=false" there and restart plasma. :-)

<a href="http://en.opensuse.org/openSUSE_11.0"><img hspace=20 src="http://counter.opensuse.org/11.0/small" /></a> 
<!--break-->