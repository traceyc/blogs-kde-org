---
title:   "Update on Konqueror the Browser"
date:    2004-02-09
authors:
  - arendjr
slug:    update-konqueror-browser
---
Okay, so I made some progress implementing the Google search bar. You can now find it in CVS in kdeaddons/konq-plugins/searchbar. As it's just a simple Konqueror plugin, it doesn't have to get in your way. It uses the default searchprovider as selected in Konqueror under the Web Shortcuts settings, so it's not limited to Google only. I hope to get some feedback from you so I can improve on it.

So, the next step is to optimize the toolbar layout for browsing (ideas like removing the Up button and such), for that I need to somehow let Konqueror save its toolbars per profile. Once that's done it should be very easy to make toolbar layouts that are optimized for separate tasks, like browsing. I'll keep you informed if I make progress.