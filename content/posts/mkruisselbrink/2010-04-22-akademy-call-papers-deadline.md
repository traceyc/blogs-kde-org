---
title:   "Akademy Call for Papers deadline "
date:    2010-04-22
authors:
  - mkruisselbrink
slug:    akademy-call-papers-deadline
---
<img src="http://akademy.kde.org/sites/akademytest.kde.org/files/images/akademy2010.png" align="right" />As you are hopefully already aware, the deadline for this years <a href="http://akademy.kde.org/">Akademy</a> <a href="http://akademy.kde.org/papers">Call for Papers</a> is approaching quickly. You have until <b>Friday, 23 April 2010</b> to get all your proposals for talks, lightning talks, workshops and BoFs in.

So start writing and get all your abstracts/proposals in on time!