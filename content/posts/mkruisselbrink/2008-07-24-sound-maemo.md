---
title:   "Sound on Maemo"
date:    2008-07-24
authors:
  - mkruisselbrink
slug:    sound-maemo
---
Continuing my efforts to get a decent KDE based system on a maemo based device, this week I've worked on getting sound to work. As maemo uses gstreamer for its media, my first attempt was to use the phonon-gstreamer backend. To get the phonon kcm to build without having xine installed (as I couldn't find any xine packages for maemo) I had to hack around a bit in some cmake files (why are the only options "no multimedia at all" and "multimedia with xine", and is there no "multimedia, but with a different phonon backend" option?), but I got it to compile. However when I tried to play sound I didn't got any errors, but I didn't hear any sound either (when playing music with amarok tracks finished as soon as I started them).

My second attempt was to compile libxine, and use the xine backend after all. Compiling xine and its phonon backend didn't bring any new problems, and after installing it on the n810, I finally got sound out of amarok:
<img src="http://93.157.1.37/~marijn/amarok_playing.png">

Actually, before I got amarok to start at all, I had some other problems to debug; the first time I tried to start amarok, it did nothing at all, using 100% cpu while making no progress at all. When I started amarok from gdb it turned out that some code in a libkjs global constructor was the culprit, somehow one of the assembly instructions in the initialization code for NaN/NaN_Bytes (not sure which of the two) would never finish. I guess I found the first problem caused by "There could be alignment issues with this, since arrays of characters don't necessarily need the same alignment doubles do, but for now it seems to work." in kjs/fpconst.cpp, although I'm not quite sure yet what the problem is. Fortunately after defining AVOID_STATIC_CONSTRUCTORS=0 to make it compile a different version of the code my problem went away, and I was able to start amarok.