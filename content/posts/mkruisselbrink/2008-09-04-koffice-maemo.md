---
title:   "KOffice on Maemo"
date:    2008-09-04
authors:
  - mkruisselbrink
slug:    koffice-maemo
---
As KOffice is supposed to be a lightweight office suite, I figured it would be nice to see how well it would run on maemo based devices. Thanks to Thomas Zander who replaced a lot of the double usage in koffice with qreals it was quite straightforward to get koffice to compile and packaged. Well, for the most part that is, I didn't manage to get kspread to link as apparently the old gcc version I'm using has some problems with inner classes in templated classes and duplicate symbols. After fixing some trivial issues, I could install koffice on maemo and run it:
<img src="http://93.157.1.37/~marijn/koffice/0b9dacfa39532f0b057383dc2f786213.png">

<img src="http://93.157.1.37/~marijn/koffice/f8b6299a57be8b1a9a9fa2c31a671b76.png">

As you see in these screenshots there are some clear user-interface problems; the dockers use way too much space for such a small screen, and somehow the toolbar is at the bottom instead of the top of the window (I have seen this earlier also with for example konqueror, so perhaps there is some general kde/qt mainwindow bug on maemo, but I haven't looked into that any further). So while the UI is far from suitable for these devices, drawing stuff with karbon actually works remarkably well. I think it is very realistic to create a slimmed-down UI for koffice applications to make a very usable office suite for maemo using a lot of the existing koffice code.
<img src="http://93.157.1.37/~marijn/koffice/5de287f2bfdffa44def17ca78131e4ef.png">

<img src="http://93.157.1.37/~marijn/koffice/e1e720f3ec4aecf82b98f277803cc355.png">

If anyone else want to try running koffice, maemo package are available in <a href="http://blogs.kde.org/node/3624">my repository</a>.