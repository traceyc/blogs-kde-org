---
title:   "Plasma running on a Neo1973"
date:    2007-12-14
authors:
  - mkruisselbrink
slug:    plasma-running-neo1973
---
[image:3150 align=left size=thumbnail hspace=20] As you can see in this image, I managed to get plasma to run on a Neo1973 phone (using an openmoko image for the rest of the software). The only changes I needed to make to kde code where changes related to qreal not being a double on an ARM processor (except of course some hacks to actually make it cross compilable).