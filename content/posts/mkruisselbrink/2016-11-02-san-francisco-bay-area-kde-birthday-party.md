---
title:   "San Francisco Bay Area KDE birthday party"
date:    2016-11-02
authors:
  - mkruisselbrink
slug:    san-francisco-bay-area-kde-birthday-party
---
Since several KDE folks were in town for the Google Summer of Code Mentor Summit they decided to come visit us as kind of an impromptu KDE 20th birthday party, and to meet KDE's newest/youngest disciple. She might still be a bit young to write code, but we'll see how long it'll take to change that. One of the reasons neither me nor blauzahl have been very active in KDE lately...
<IMG src="https://blogs.kde.org/sites/blogs.kde.org/files/Canon%20EOS%206D-IMG_2775.jpg">