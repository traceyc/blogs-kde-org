---
title:   "KDE packages for maemo"
date:    2008-08-21
authors:
  - mkruisselbrink
slug:    kde-packages-maemo
---
During the last couple of days I've been working on trying to get parts of kde packaged for maemo, to make development for others easier, and after some struggles, I finally succeeded in doing part of this. Most of this isn't available yet in the official extras-devel repository, because getting it to build in the buildsystem there would probably be quite some extra work, but I've uploaded the binary packages to my own repository. All the stuff in this repository is of course very experimental, and it might do all kinds of ugly things, but I at least managed to run kbuildsycoca4 after installing kdelibs5 from my repository. 

Currently available are packages for automoc, akonadi, phonon, strigi, soprano, kdelibs, and several other dependencies for those packages (like mysql for akonadi, and some debian build-system stuff). All those packages are created from trunk checkouts of svn (except phonon, which was created just before xine got moved to kdesupport/phonon), so stuff might be broken because of that too. Also included in my repository are packages for qt4.4.1, although for those I didn't do much, and I believe they will be available in extras-devel in a couple of days too. Several of these packages are already available in extras-devel, and more will probably follow soon, but for now hopefully this should get getting started with developing kde stuff for maemo already a bit easier.

One thing I had to do manually after installing the kdelibs5 package on my n810, was running <tt>dbus-uuidgen --ensure</tt> to generate a /var/lib/dbus/machine-id file, although I'm not sure why this was needed. After that I could just run kbuildsycoca4 without any problems. After I installed the kdelibs5 package with all its dependencies I still had 37.6MB available on my root fs, even though before installation the space available seemed only barely enough to fit everything on.

If you want to try out my (very experimental) packages, add the following line(s) to your /etc/apt/sources.list, and run apt-get install kdelibs5:
<pre>
# repository with experimental kde packages and dependencies
deb http://93.157.1.37/~marijn/maemo binary/
# extras and extras-devel repositories, if you haven't added them already
deb http://repository.maemo.org/extras/ diablo free non-free
deb http://repository.maemo.org/extras-devel/ diablo free non-free
</pre>Please let me know any problems, questions or suggestions you might have, but keep in mind that this is all very experimental stuff.