---
title:   "Hello planet"
date:    2008-06-24
authors:
  - mkruisselbrink
slug:    hello-planet
---
As this will be my first post published on planet KDE, I want to use this opportunity to introduce myself a bit. I am Marijn Kruisselbrink, a 22 year old computer science student from The Netherlands. My first significant contribution to KDE happened last year when I participated in the Google Summer of Code to implement music notation support in KOffice. Aside from that I have also done some work on improving KDE on Mac OS X, by writing OSX implementations of some of the abstractions KDE provides. Also I've helped with this years Akademy by writing a large part of this years (and hopefully many years to come) registration system.

So far for my previous contributions, this year I was once again selected as one of the students to work for KDE during the Google Summer of Code. This year I'll be working on improving how KDE in general and plasma specifically behave on small, high DPI and/or touch-screen only devices. The actual summer of code started already some weeks ago, but I had a bit of a slow start due to exams and stuff like that, but for the rest of the summer I really should be able to get some nice progress made, so expect some more blog posts from me with the current state, and progress updates.