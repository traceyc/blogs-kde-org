---
title:   "Rendering Engines Benchmark"
date:    2008-09-29
authors:
  - spart
slug:    rendering-engines-benchmark
---
Ever since Maksim Orlovich released <a href="http://blogs.kde.org/node/3476">benchmarks</a> of its excellent FrostByte flavour of the KJS engine, I've been curious to perform a synthetic benchmark of <b>KHTML/KJS</b>, to see what our whole stack is up to.

There would indeed be little point in having a smoking fast JavaScript engine if performance was going to fall apart at the next link of the chain, such as in DOM tree manipulation or Rendering, wouldn't it?

Well, what better time for conducting tests than now, at the eve of the KDE 4.1.2 release, where new generation browsers are stable and some new contenders want to enter the field...

I will therefore present results for the following engines/browsers :
<ul>
<li><b>KHTML/KJS</b> on Konqueror 4.1.2
<li><b>Presto/Futhark</b> on Opera 9.52
<li><b>Gecko/SpiderMonkey</b> on Firefox 3.0.1
<li><b>WebCore/V8</b> on Chrome 0.2.149.27(beta)/wine 1.1.4
<li><b>QWebkit</b> on Qt 4.4's Demo Browser
</ul>

As for the tests themselves, I did not want to bias this benchmark by crafting them specially, so I decided to use the readily available <a href="http://nontroppo.org/timer/kestrel_tests/">synthetic performance test suite</a>, consisting of a patchwork of external tests, that was independantly set together for testing the performance of Opera 9.5.

Before I proceed to the details, I am very happy to report up front that <b>KHTML/KJS</b> is the engine that showed the most consistent performance, and is definetly <b>the fastest overall</b> on the Linux IA-32 platform :-)

There are six tests available, that I will examine successively. Each test was run three times on an otherwise idle AMD X2 4600+ with 2 Gigabytes of ram; the arithmetic mean of those was kept as final result.

The first test is a JavaScript driven raytracer that is very nice for stressing evenly the JS, DOM and Rendering stack. It is therefore a good representation of DHTML performances:

<img src="http://ebooksfrance.org/~germain/khtml/benchmarks/test1.gif"><br>

KHTML scores a gold, Chrome a silver and Opera a bronze.

(as a side note, this really is a nice reward for all the work put in the careful optimisation of DHTML in our rendering engine - such as in direct layer translation, and deep optimisation of the positioned objects code path)

The second test is pure JavaScript computation :

<img src="http://ebooksfrance.org/~germain/khtml/benchmarks/test2.gif"><br>

Gold is for Chrome, silver for Gecko and bronze for KHTML.

The 3D cube test is majorly JavaScript, but with a significant Rendering component :

<img src="http://ebooksfrance.org/~germain/khtml/benchmarks/test3.gif"><br>

Chrome has the gold, KHTML the silver, and bronze goes to Gecko

The fourth test is also pure JavaScript, but with a DOM component:

<img src="http://ebooksfrance.org/~germain/khtml/benchmarks/test4.gif"><br>

Opera wins the gold, KHTML takes silver and Chrome is happy to go with bronze.

The fith test is for pure DOM manipulations :

<img src="http://ebooksfrance.org/~germain/khtml/benchmarks/test5.gif"><br>

KHTML takes the gold, Chrome is silver and Opera bronze.

The sixth is a batch of four tests aiming at testing the rendering speed of standard html elements, and of the very young Canvas element. It stresses both the DOM and either the HTML rendering stack or the Canvas painting stack (so it would probably make more sense to separate this latter test from the others but hey..)

<img src="http://ebooksfrance.org/~germain/khtml/benchmarks/test6.gif"><br>

Gecko scores a gold, Opera a silver and KHTML a bronze

Our bronze here shows off the work of Harri Porten, who contributed significant DOM speed ups and other optimisations.

Greetings,
Germain