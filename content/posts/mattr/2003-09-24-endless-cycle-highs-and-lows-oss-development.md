---
title:   "An endless cycle - the highs and lows of OSS development"
date:    2003-09-24
authors:
  - mattr
slug:    endless-cycle-highs-and-lows-oss-development
---
Here's the mandanatory blurb about having not posted a blog in awhile: "I haven't posted anything here in awhile, sorry bout that"
<br><br>
And now on to the bulk of the content. :)  (It's going to be rather long)
<!--break-->
I've reached the low point of my kopete development cycle. I have absolutely no desire to code on anything kopete related right now. There are many reasons why, but mostly it all comes down to when I got "assigned" to port all the plugings and protocols from the old ConfigModule framework to the new KSettings :: Dialog, etc. stuff. I only offered to help, as in do a few of the plugins, and I wouldn't have to worry about doing all of them. While this actually turned out to be the way it happened, I was definately pissed after the feature plan got updated and that particular item only had my name on it. Well, it's done now, and so I marked it such in the feature plan, only to be told, and I quote, "One could argue that fixing the layouts that are way too big now is also part of this though. In which case the work is certainly not done yet." Well, that's just too damn bad, somebody else is going to have to fix them, because I probably won't. So yeah, that's one thing. The other thing is people blowing me off like what I say doesn't matter. I know I haven't been coding on anything KDE related for very long (I got my CVS account in May, it's almost October now. That's less than 6 months), but I don't think that matters all that much. Bug #64223 was a direct result of what I felt was a blowing off by somebody on the Kopete team.  Anyways, I'm sure everybody who's reading this is thinking, "man, just shut the hell up, why are you bitching?", and so I'll stop, since these are my only two issues. Personally, I think they're just jealous cause I close more bugs than they do. ;)
<br><br>
And speaking of bugs, I've been closing quite a bunch of them lately. While some of them were KPackage bugs that Toivo Pedaste (I assume he's the maintainer) said were fixed, but forgot to mark as such, alot of them have been kopete bugs that I can't reproduce and Konqueror and Konsole bugs that I can't reproduce. :) Coolo however, is wayyyy ahead of me and I'm second on the list in terms of most bugs closed :) (I can gloat more if you want. ;) )  So, I think that's what I'm gonna fix do for awhile, is fix bugs. I'm sure it'll make coolo happy. :)  There's also no reason to keep bugs that can't be reproduced open. <br><br>

And in other news, there will be a Kopete 0.8 version for those people who are too chicken to upgrade to KDE 3.2 when it comes out. ;)  I might even put out a 0.7.3 release even though the 0.8 Betas (yup, there's going to be seperate betas too) will be out about the same time as I would release a Kopete 0.7.3. I'll probably start a thread on a Kopete 0.7.3 today or tomorrow on kopete-devel. <br><br>

Well, now that I'm done bitching, back to work. 