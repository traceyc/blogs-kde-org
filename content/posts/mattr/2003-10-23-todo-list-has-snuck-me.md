---
title:   "The TODO list that has snuck up on me"
date:    2003-10-23
authors:
  - mattr
slug:    todo-list-has-snuck-me
---
It's snuck up on me. It's been growing item by item until it can go unnoticed no longer. Yup, my TODO list has become so long, I can no longer ignore it. So, I thought i'd write it down. I imagine that I haven't gotten everything I need to do on here, but here's some of it. 
<!--break-->
<ul>
<li> Get rid of AIMBuddy, AIMBuddyList and AIMGroup from Kopete's Oscar protocol, and bunch of massive code duplication caused by these classes. :)
<li> Fix bug #66991
<li> Fix bug #66865
<li> Fix bug #66807
<li> Lots of bugfixes in Kopete. There's currently 48 open bugs and I'd like to help get that down to around 25 or 30.
<li> Learn Konqueror's code and fix some bugs there.
<li> Start an ODBC driver for Kexi so I can start using it at work
<li> Convince everybody at work that they should be using KDE. ;)
</ul>
Ok, so the last one is kinda facitious, but still, that's probably the longest todo list I've ever had in my six months of working on KDE. I imagine that there's more here than I can remember at the moment, so maybe I'll come back and add it to it. However, I don't think that I can ignore it any longer.