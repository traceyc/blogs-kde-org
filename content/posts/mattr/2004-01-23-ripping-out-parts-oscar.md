---
title:   "Ripping out parts of oscar"
date:    2004-01-23
authors:
  - mattr
slug:    ripping-out-parts-oscar
---
so, yup, i started ripping out parts of kopete's oscar plugin last night. The first thing to go was the old Kit API and so the AIMBuddy and AIMGroup classes are gone. abracadabra and *poof*. :) However, this means that contact list handling is completely broken right now. good thing i'm doing this in my own branch. :)
<!--break-->
<br><br>
I have some other plans for this branch as well. I'll probably merge it back to HEAD in March, depending on what I get done, but some of the things that I want to do are:
<ul>
<li>Decent rate limiting - I have some notes from Martijn and a good idea of how to do this already
<li>Better contact list handling - which was broken because of all the AIMBuddy and AIMGroup crap
<li>Add support for buddy icons (bug #54156)
<li>Add support for rendering the background color (bug #71800)
<li>Add support for sending the font face to contacts (bug #53016)
</ul>

I think i might already implemented bug #68545 with the change to KExtendedSocket, so i think i might close it. :)
