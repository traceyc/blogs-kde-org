---
title:   "Kopete 0.7 and a Yahoo bug"
date:    2003-07-30
authors:
  - mattr
slug:    kopete-07-and-yahoo-bug
---
How annoying. I fixed the yahoo bad account data log in bug that Bille (Will Stephenson) pointed out to me, but I can't commit for 0.7 because it has string changes. I suppose I shouldn't bitch much since i'm the one that imposed the string freeze. My original reasoning behind the string freeze was because it would give all the other kopete developers a small taste of what it would be like if/when kopete makes the move to kdenetwork. (not that we really needed the string freeze in the first place). So anyways, that change will go in HEAD when the branch is created in about 3 days from now.
<!--break-->
cvs2dist is pretty cool. I've been playing with it in order to get a feel of how to create a package from a cvs module since it's my first time. Heh, I know what you people are thinking, "Oh my god! You're the release dude for kopete 0.7 and you don't even know how to use cvs2dist". It's not that hard really, it's actually rather easy to create the packages, it's all the other crap you have to make sure you do. :)

I have my algorithms project finished as well as the homework due tomorrow in that class. I haven't started Operating Systems yet and it's due tomorrow too. Oh well, I'll get it done. I have all day tomorrow to work on it. I have to get my car inspected tomorrow though before the end of July since I don't feel like getting a citation.

Well, since everybody seems to have posted their todo list here, I suppose I will too.

Matt's todo list

- update to the latest libyahoo and port it to use QT and KDE rather than just straight C.
- clean up the API Documentation for Kopete some more
- clean up the english in some of the kopete dialogs. I couldn't get this in for 0.7 and so it'll have to wait till 0.8 or 1.0 or whatever the next damn version is going to be.
- help mETz out by hacking on OSCAR some (I know I keep saying this and I really mean to do this, just need to find the time)
- hack on kblog some since i won't want to use the web interface forever. too bad I can't get it to link. (libtool doesn't look in the right place for libkateutils.la. It should look in /usr/kde/cvs/lib which is in ld.so.conf, but it only looks in /usr/lib)

Forgot this the first time:

- Work on KTagit so it uses wheels' new tagging library taglib and get it ready for a 0.7 release