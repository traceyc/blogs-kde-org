---
title:   "A walk down memory lane"
date:    2003-10-07
authors:
  - mattr
slug:    walk-down-memory-lane
---
Ok, now that i've thought of a snappy title for my blog to get people to read it, I'll actually write something here 
<br><br>
I've had an interesting weekend. I was supposed to release Kopete 0.7.3 source tarballs on Sunday, but that didn't quite happen since I spent most of sunday troubleshooting hardware problems and building all kinds of source code. :)  I got some upgrades and things didn't go at all as planned. :(
<!--break->
<br><br>
I got a new Athlon XP 2600 on Friday (it was a present from my girlfriend. :) ) and so being the the geeky person that I am, it got installed that night. I spent about an hour or so taking the old processor out, putting the new one in, applying thermal paste to the new fan, etc. etc. (all the stuff that comes with getting a new CPU).  Finally, get it all put back together, boots up just fine, KDE loads (pretty damn quick, I might add). I'm thinking "I am invincible". hehe, so of course the first thing that I do is "emerge -u world --deep" since I hadn't updated my system in about three weeks due to lack of time and the anticipation of the new hardware. Watch it get though about 3 packages and then gcc segfaults! :( So I'm thinkin', "This can't be happening!!! Everything was fine!!!". Anyways, to keep an already long story semi-short, my memory didn't like being overclocked. I had PC2100 RAM before and the new chip uses a 333 Mhz FSB and so I spent about a day and a half (roughly) troubleshooting everything that I could think of and then had to go and spend 75 USD for a new 256MB DIMM. *sigh* <br><br>

Anyways, Kopete 0.7.3 should be out on Tuesday, barring any other unforseen circumstances. And now that the essential parts of KDE 3.2 alpha2 (libs,base,network,pim) have finished building, off I go to play (in a non-debug build, but oh well) 