---
title:   "7/22/2003 - I need more time"
date:    2003-07-22
authors:
  - mattr
slug:    7222003-i-need-more-time
---
Why is it that university professors always pile crap loads of work on at the end of the term? *sigh* So yeah, I got lots of homework and projects, and no time to hack. But i'll stop bitching now. :) 

Kopete 0.7 is getting close to being released, and I'm been pleased with all the positive feedback I've gotten on the Yahoo plugin. I have a local fix for the "invalid accounts don't disconnect" bug, but that fix triggers the select bug again (<a href="http://bugs.kde.org/show_bug.cgi?id=56028">56028</a>). I think this might be a problem with libyahoo2 and so it may not get fixed in time for 0.7. 
<!--break-->
<a href="http://bugs.kde.org/show_bug.cgi?id=58685">This bug</a> is in the top 20 of most hated bugs. Guess that ought to be fixed. :) 

And I just saw that KLogoTurtle just got imported into CVS by annma. How cool!! I need to dig out those old logo notebooks that I have (if I can still find them) and see how well it works. My friends and I would have a blast with the logo stuff in grade school, so much fun. :)

Anyways, my lunch break is over. Back to work for me.
