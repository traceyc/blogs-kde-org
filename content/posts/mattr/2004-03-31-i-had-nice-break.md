---
title:   "i had a nice break"
date:    2004-03-31
authors:
  - mattr
slug:    i-had-nice-break
---
I took a semi-break over the last couple of weeks and went hardcore back into coding on Sunday. Cleaned up Kopete's filetransfer API a bit (ok, removed one whole function), fixed a Gadu bug, and then added mail notifications to Yahoo. Not really sure what to work on next, but I get to run Linux (and therefore KDE) at work now, so I hope I can code there more.
