---
title:   "7/24/03 - Eureka!"
date:    2003-07-25
authors:
  - mattr
slug:    72403-eureka
---
Well, I still have no time to hack on KDE, (well, Kopete to be more specific) but that's ok since I just made a nice breakthrough with my algorithms project that's due on Monday night (6pm). I was having problems getting Kruskal's down from O(n<sup>2</sup> log n) to O(n log n), but i figured it out so I'm happy now. Now if I could only find a graph generator. :) 
<!--break-->
<br><br>
Hmmmm, the battery on my palm seems to have died, let's hope I didn't lose all my stuff. I had important email addresses in there! ........ hmmmm, nope, all my data went ka-put. I wonder how old my backup is. Well, can't find the backup either. Oh well, guess that dude from algorithms won't get that union and find algorithm i was going to send him. <br> <br>

Looking for a job sucks! I have one already, but I would prefer not to stay there after I graduate, since it's mostly database stuff and I don't wanna do that kind of thing. It's also getting <b>really</b> boring! I'd just rather be a software engineer or a sys admin or something along those lines.  Actually, it'd be really cool to get paid to at least do a little bit of work on KDE, or something linux related in general, but I don't think that I've been hacking on open source stuff long enough. I suppose I could go be Red Hat's KDE guy so they have some decent support for KDE in their distro, but i utterly hate Red Hat (for other reasons besides their fsck-up with KDE), so I probably wouldn't do that. <br> <br>

Well, I think that's all I can rant about today. Time to work on my project (ugh)
<br><br>
Added 7/25: How I forgot to file this under Rants, I don't know, so I fixed that. :/