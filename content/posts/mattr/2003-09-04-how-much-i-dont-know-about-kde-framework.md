---
title:   "How much I don't know about the KDE framework"
date:    2003-09-04
authors:
  - mattr
slug:    how-much-i-dont-know-about-kde-framework
---
I've been porting the Kopete config dialogs for the plugins for the last couple of days. Man, I knew that I knew very little of what the KDE framework offers, but I had no clue. :(  The stuff in kutils (The KConfigureDialog stuff) that Matthias Kretz has wrote is just plain cool. Sadly, I'm sure I've just barely touched the surface. 
<!--break-->
I've got the highlight and cryptography plugins ported and Olivier Goffart has ported the account config, which I was going to tackle next, but I'm glad he did it, since I didn't really have a clue of how to go about it.  I think I've got about 5 more plugins to port, and if each one takes about an hour, then that's about 5 hours or so. Maybe I can get that done tomorrow or on Saturday.  The first plugin I did (cryptography) took me about 6 hours, and the second plugin (highlight) about 3, but that's because it was late and I had a whole bunch of stupid mistakes. :(<br><br>

I've got a whole lot of other things on my TODO list, but that's for a different blog.