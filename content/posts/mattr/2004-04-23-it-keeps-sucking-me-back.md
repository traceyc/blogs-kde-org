---
title:   "it keeps sucking me back in!!!!"
date:    2004-04-23
authors:
  - mattr
slug:    it-keeps-sucking-me-back
---
hmmm, ok, so much for not working on kopete for awhile. I was going to try to take a break, but I can't! It's like freaking quicksand, I keep getting sucked back in, so I fixed a few bugs, and now I'm better. In other news, I've kinda gotten hooked on Bitstream Vera Sans Mono. It's a cool font for editing code in. Although finding a font size that's just right with this 112 DPI screen is somewhat nerve racking. I think i'll need to play with the DPI a little bit. (ugh)