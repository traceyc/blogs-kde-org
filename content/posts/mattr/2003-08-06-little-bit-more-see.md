---
title:   "A little bit more to see"
date:    2003-08-06
authors:
  - mattr
slug:    little-bit-more-see
---
There's a little bit more to see here now. I wanna write a blog, but I just don't have motivation at the moment since i have an exam in about 3 hours (must study!).

Ok, after spending about 20 minutes typing, I closed the other tabs I had open, and the page reloaded. :( 

I did pass my exam though, so that's good. I'll write a new blog tomorrow. Night!
<!-- break -->