---
title:   "Simon 0.4.90 beta released"
date:    2017-05-20
authors:
  - fux
slug:    simon-0490-beta-released
---
The second version (0.4.90) towards Simon 0.5.0 is out in the wilds. Please <a href="https://download.kde.org/unstable/simon/0.4.90/simon-0.4.90.tar.xz.mirrorlist">download the source code</a>, test it and send us <a href="https://bugs.kde.org">feedback</a>.

What we changed since the <a href="https://blogs.kde.org/2017/04/03/simon-0480-alpha-released">alpha release</a>:

<ul>
<li>Bugfix: The download of Simon Base Models work again flawlessly (bug: 377968)</li>
<li>Fix detection of utterid APIs in Pocketsphinx</li>
</ul>

You can get it here:
<a href="https://download.kde.org/unstable/simon/0.4.90/simon-0.4.90.tar.xz.mirrorlist">https://download.kde.org/unstable/simon/0.4.90/simon-0.4.90.tar.xz.mirrorlist</a>

In the work is also an AppImage version of Simon for easy testing. We hope to deliver one for the Beta release coming soon.

Known issues with Simon 0.4.90 are:
<ul>
    <li>Some Scenarios available for download don't work anymore (BUG: 375819)</li>
   <li>Simon can't add Arabic or Hebrew words (BUG: 356452)</li>
</ul>

We hope to fix these bugs and look forward to your <a href="https://bugs.kde.org">feedback and bug reports</a> and maybe to see you at the next Simon IRC meeting: Tuesday, 23rd of May, at 10pm (UTC+2) in #kde-accessibility on freenode.net.

<p><b>About Simon</b></p>
Simon is an open source speech recognition program that can replace your mouse and keyboard. The system is designed to be as flexible as possible and will work with any language or dialect. For more information take a look at the <a href="https://simon.kde.org">Simon homepage</a>.
