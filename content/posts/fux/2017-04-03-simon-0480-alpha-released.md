---
title:   "Simon 0.4.80 alpha released"
date:    2017-04-03
authors:
  - fux
slug:    simon-0480-alpha-released
---
The first version (0.4.80) towards Simon 0.5.0 is out in the wilds. Please <a href="https://download.kde.org/unstable/simon/0.4.80/src/simon-0.4.80.tar.xz.mirrorlist">download the source code</a>, test it and send us <a href="https://bugs.kde.org">feedback</a>.

Some new features are:
<ul>
<li>MPRIS Support (Media Player control)</li>
<li>MacOS port (thanks to René there is <a href="https://github.com/RJVB/macstrop/tree/master/kde/simon">a first Macports script</a<)</li>
<li>A series of bug fixes.</li>
</ul>

You can get it here:
<a href="https://download.kde.org/unstable/simon/0.4.80/src/simon-0.4.80.tar.xz.mirrorlist">https://download.kde.org/unstable/simon/0.4.80/src/simon-0.4.80.tar.xz.mirrorlist</a>

In the work is also an <a href="http://www.appimage.org">AppImage version of Simon</a> for easy testing. We hope to deliver one for the Beta release coming soon.

Known issues with Simon 0.4.80 are:
<ul>
<li>Base model download doesn't work as expected. You can search for some base models though (<a href="https://bugs.kde.org/show_bug.cgi?id=377968">BUG: 377968</a>)</li>
<li>Some Scenarios available for download don't work anymore (<a href="https://bugs.kde.org/show_bug.cgi?id=375819">BUG: 375819</a>)</li>
<li>Simon can't add Arabic or Hebrew words (<a href="https://bugs.kde.org/show_bug.cgi?id=356452">BUG: 356452</a>)</li>
</ul>

We hope to fix these bugs and look forward to your <a href="https://bugs.kde.org">feedback and bug reports</a> and maybe to see you at the next Simon IRC meeting: Tuesday, 4th of April, at 10pm (UTC+2) in #kde-accessibility on freenode.net.

<b>About Simon</b>
Simon is an open source speech recognition program that can replace your mouse and keyboard. The system is designed to be as flexible as possible and will work with any language or dialect. For more information take a look at <a href="https://simon.kde.org">the Simon homepage</a>.