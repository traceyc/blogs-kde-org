---
title:   "Holidays!"
date:    2008-08-05
authors:
  - uga
slug:    holidays
---
Such a boring title. And so obvious. But it's August: everyone around packs, get their kids in the cars and rush for miles until they reach to the hot summer beaches.... puagh! not my style. You won't see uga roasting his back under the sun just to look like the Guinness Record of Large Red Tomatoes. Life got other interests to keep my time busy. Different cultured and tourist-free destinations where you can taste the flavor of magestic culinaires not adapted to local aberrations, or you can just walk through, admiring their architecture and nature.

<img width=200 src="http://www.ugarro.com/gallery/d/48-3/crw_7109.jpg"/> <img width=200 src="http://www.ugarro.com/gallery/d/98-3/img_2760.jpg"/> <img width=200 src="http://www.ugarro.com/gallery/d/5097-2/img_4159.jpg"/> <img width=200 src="http://www.ugarro.com/gallery/d/142-3/img_6832.jpg"/>
<!--break-->

Unfortunately and unlike many other summers, for many reasons out of my control, I seem to have no much planning ahead for a couple of weeks. I also got job-work to do, but guess... also tons of spare time. And I sort of came back to do some coding. Yes! Really! Try it! It's better than wifi on the beach!

My best advise here: pick the application you <b>use</b> most, and fix those little annoyances. Yes, the one you use, not the one you usually code! You are a user of that app, so, after all, you know a bit about its most annoying bugs. Why not fix them. 

I picked on digikam myself. I spend so much time on photography these days that it was a no-brainer to choose it. It's my tool of preference for sorting all my photos, and those are quite a few! There are better utilities for edition (some are commercial and rather expensive), but in the area of sorting/searching I think it's one of the best and it's also improving in the area of edition. Finally it will receive a properly-done raw editor gui too! (see <a href="http://digikam3rdparty.free.fr/TourMovies/0.9.5-rawimporttoolforeditor.ogv">new raw tool from Gilles</a> )

Now well, after two days hacking, I just hope Gilles will actually read my patches, because after 2-3 days of chasing on the net (yes, FBI movies style), I seem uncapable of catching him up ;) Well, I think he really hates me because I'm so critical with photography apps. But hey, I'm a user, and meant to critizise developers :P

Given I won't hack much more in there unless he gives me a go-go, I will have to pick on a different app. Lets lottery begin!
