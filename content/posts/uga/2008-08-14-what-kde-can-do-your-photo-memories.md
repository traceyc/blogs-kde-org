---
title:   "What KDE can do for your (photo-)memories"
date:    2008-08-14
authors:
  - uga
slug:    what-kde-can-do-your-photo-memories
---
Photography has changed a lot since digital cameras broke into our lives and replaced film cameras. Camera makers have made great efforts to convince us that digital is better, and that new digital cameras are worth their money. 

Unfortunately, this isn't completely true. Being so cheap, people shoot thousands of photos a day, careless of their quality. why care? they just want to capture something for their memories. The answer is: check your memories of akademy 2008. Do you actually remember your friends there as being greenish and pale? Well, that's what some of the photos looked like =) 

So, after looking at the quality of pictures taken by people in Akademy 2008, and thanks to the suggestions of a few devels, I thought of writing a series of tutorials on how to fix your green friends... errrm... I mean your photos ;) using KDE

So lets begin! KDE comes to the rescue! 

In the following posts I aim at writing down some basic tutorials on how to fix your photos using KDE tools. I will be using tools available to everyone, including showfoto and krita possibly, and could be gimp too. It's not meant to be exhaustive, since that would take writing a whole book, but it will cover the most annoying and basic fixes that will get you started.

Using this post, I'd like to request kde fellows permission to use some of their akademy pics to write this series of tutorials. .

Anyway, good luck with your new photos!


