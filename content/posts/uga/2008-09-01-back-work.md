---
title:   "Back to work"
date:    2008-09-01
authors:
  - uga
slug:    back-work
---
Summer is gone. No more pictures or travelling, lack of time for much opensource... that's life. 

I will also be moving soon, and that will make my spare time even shorter. Lets hope there will still be some spare time during the weekends!

In the mean time, I'm seeing that many photographer applications and code are being added hideously to svn. I really wish those projects got more attention and advertising (and thus help), since, given the skills required for coding them, they require quite a few resources. I hope to be able to help those myself with some code, now and then.

That said, I'm pondering what to do with the photo tutorial series. planetkde is eating too much bandwidth off my server, by showing the whole blog body on each hit. That makes for 12GiB transfer per day (around 6-7k hits, I think) and it's unnacceptable for me, so I will have to stop them for some (short) time and try finding a proper workaround.

[o ] click!



