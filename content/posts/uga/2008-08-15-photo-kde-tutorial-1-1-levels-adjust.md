---
title:   "Photo KDE Tutorial 1-1: Levels adjust"
date:    2008-08-15
authors:
  - uga
slug:    photo-kde-tutorial-1-1-levels-adjust
---
This is possibly one of the most widely used and most simple method to adjust an image. It's so simple and effective that you will want to use it on all your pictures from now on, so keep an eye on this, and have fun.
<!--break-->

Lets see this sample photo from Akademy 2008, kindly donated by Sebastian Kügler:

<center><img src="http://www.kubuntu.org/~jriddell/uga/tutorial1/orig.jpg"/></center>

You'll clearly see, the photo is darkish. After all, it was taken during a presentation and possibly the room was pretty dark. But that's no excuse for a bad photo =)

Why is it dark? Lets open the photo in showfoto (digikam's editor) and see what's going on:

<img src="http://www.kubuntu.org/~jriddell/uga/tutorial1/issue.png"/>

On the top right, you can see the image's histogram. (I have pushed the "linear" histogram button to see it more clearly).
So what's the histogram? you'll, ask. The histogram is just a pixel count. It counts how many pixels there are for each gray level, and shows them in a graph. The left part of the histogram are dark/black colors, and the right part of the histogram are brightest colors. 

You can see that our image has the histogram concentrated on the left part. Thus, it's mostly black. The right part of the histogram isn't used at all, as shown in the figure. Why did this happen? Just because the camera failed exposing the image properly, or was inappropriately configured.

If a photo is visually pleasing, usually (not always), it covers most of the histogram, from black, to white.

So, is there a way to fix this, then? Of course there is, and it's a very easy one. Select the menu "Color->Levels Adjust": 

<img src="http://www.kubuntu.org/~jriddell/uga/tutorial1/menu.png"/>


You will see a new popup showing a tool to adjust the histogram output. There are several parts in it. On the top right, there are two histograms. The first one is the output/new histogram, and the second (bottom) one is the input/original histogram.

<img src="http://www.kubuntu.org/~jriddell/uga/tutorial1/fixing.png"/>


As you can see, I have adjusted the output histogram to cover it all, see? So how have I done this. Very simple:

There's 4 sliders in this tool. The first two sliders mark the beginning and end of the part of the histogram that I am interested on. I have moved them to match the full histogram of our original image.

The other two sliders mark the range of the histogram we want as output. We want the histogram to cover from black to white, so just move the sliders to the left corner and to the right corner.

Press "OK", et voilà, your nice photo is fixed. Congrats =)

<img src="http://www.kubuntu.org/~jriddell/uga/tutorial1/result.jpg"/>

