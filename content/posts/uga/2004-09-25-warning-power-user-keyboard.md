---
title:   "WARNING! Power user at keyboard!"
date:    2004-09-25
authors:
  - uga
slug:    warning-power-user-keyboard
---
There are some days when one shouldn't touch a computer keyboard. Today must be one of those days. But we'll never learn, and we'll keep doing these stupid things once, and again, and again. It must be Murphy who probably wrote:

"The chances you mistakenly delete data are directly proportional to the square of the time since your last backup"

And he was damn right. We do backups only when we see we have lost data recently, but after a while we forget about... and after a few months of important work, we screw things again.

Oh, yes, you guessed, I rm -rf'ed my home dir today. And I did press Ctrl-C,... But these rm versions of nowadays are really clever and they know what data to remove first to annoy you more. Isn't that ironic that this has happenned to a guy coding backup tools?

What I also noticed with this "nice" experience is that the more one knows about computers (read power users here), the more chances you get to loose your data. I'm not kidding. One tends to use bleeding edge unstable software, never uses kfm and instead goes straight to rm and move things in konsole, if we are forced to rm things in konqui we press 'shift' so it doesn't go to trash, and we even test unstable kernels and filesystems. We're just helping murphy with all those steps.

Lets see if I'll learn something from this and I can help out for these cases in my little utility. Maybe one day we'll upload our disks to cvs and recover our data too... sigh