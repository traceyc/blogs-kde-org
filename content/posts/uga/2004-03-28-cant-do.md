---
title:   "Can't do that"
date:    2004-03-28
authors:
  - uga
slug:    cant-do
---
Heh, 3 hours ago I was about to drop this little project. 3 hours later, I'm back thinking I can't do that. I can't just drop it and go away. I'm not that kind of person. I think blogging about one's complaints and stopping coding for 3 hours helps thinking better. Really, try it. I guess I didn't sleep enough, was stuck with the project and as a result I got a hit by the frustration. I was even thinking of joining the Gnom... nah! that's just not possible ;)

So what will I do to solve those library problems? Not sure, but even if I have to clone and rewrite ksvg to embed the code in, I'll do it. Anyway, enough of ranting for today. Oh, and sorry lyp :/

