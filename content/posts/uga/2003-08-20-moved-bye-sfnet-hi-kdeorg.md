---
title:   "Moved! Bye sf.net. Hi kde.org"
date:    2003-08-20
authors:
  - uga
slug:    moved-bye-sfnet-hi-kdeorg
---
At last. Krecipes is in kdenonbeta now. No more complaints about delayed anoncvs servers :-)

Oh, and we're heading for a 0.3 release <b>very</b> soon. With some very nice new features:
<!--break-->
<li>Recipes can be classified in categories</li>
<li>One can especify also number of servings per recipe</li>
<li>Authors are now acknowledged (one can especify one or more authors per recipe)</li>
<li>Initial i18n. Language support: English, Spanish, French and Euskara</li>
<li>UTF support</li>
<li>(Unfinished) Mysql database autosetup to make setup easier</li>
<li>In-place creation of new ingredients and units</li>
<li>Renaming of units and ingredients</li>
<li>Anddddd...... drums please!... <b>The best feature ever: </b>some of <b>KDE's nicest recipes</b> ever. Obviously acknowledging the respective authors ;-)</li><br>

Obviously, several bugfixes are included also, and I may have forgotten some new features. Anyway, next version 0.4 is expected to be the feature boom. You'll see.