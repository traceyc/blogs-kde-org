---
title: Amarok 3.0.1 released
date: "2024-06-02T20:00:00Z"
categories:
 - Release
authors:
  - amarok
SPDX-License-Identifier: CC-BY-SA-4.0
---

The Amarok Development Squad is happy to announce the immediate availability of Amarok 3.0.1, the first bugfix release for [Amarok 3.0 "Castaway"](https://apps.kde.org/amarok/)

3.0.1 features a number of small improvements and bug fixes, the oldest fulfilled feature request dating back to 2010 this time. Wikipedia applet, UI strings, and playlist generation and collection filtering
are among the components that have received multiple improvements in this release.
The efforts to both further polish the Qt5/KF5 version, and keep doing clean-up and preparations that bring a Qt6/KF6 version closer, have been ongoing and will continue.


#### Changes since 3.0.0
##### FEATURES:
* Added an option to copy image to clipboard in Wikipedia applet, and a clickable notification if a non-Wikipedia link was clicked.
* Added an option to select if track's artist is shown for entries under various artists / different album artist in context browser ([BR 276039](https://bugs.kde.org/276039), [BR 248101](https://bugs.kde.org/248101))
* Indicate which search option is active in Wikipedia applet ([BR 332010](https://bugs.kde.org/332010))

##### CHANGES:
* Amarok now depends on KDE Frameworks 5.78.
* Improve strings in user interface (incl. [BR 343896](https://bugs.kde.org/343896), [BR 234854](https://bugs.kde.org/234854))
* Reduce CPU usage by minimized/hidden analyzer ([BR 390063](https://bugs.kde.org/390063)) and other components.

##### BUGFIXES:
* Various tag match bias, APG and collection filtering related fixes ([BR 375565](https://bugs.kde.org/390063), [BR 406751](https://bugs.kde.org/406751), [BR 435810](https://bugs.kde.org/435810), [BR 382456](https://bugs.kde.org/382456))
* Ignore any infinite values when reading replaygain tags ([BR 486084](https://bugs.kde.org/486084))
* Avoid volume getting set to 0 when phonon-vlc playback is stopped ([BR 442319](https://bugs.kde.org/442319))
* Playlist duplicate track deletion fixes ([BR 313791](https://bugs.kde.org/313791))


### Getting Amarok

In addition to [source code](https://community.kde.org/Amarok/GettingStarted/Download/Source), Amarok is available for installation from many distributions' package
repositories, which are likely to update to 3.0.1 soon.
A flatpak is currently available on [flathub-beta](https://discourse.flathub.org/t/how-to-use-flathub-beta/2111).

### Packager section

You can find the tarball package on
[download.kde.org](https://download.kde.org/stable/amarok/3.0.1/amarok-3.0.1.tar.xz.mirrorlist)
and it has been signed with [Tuomas Nurmi's GPG key](https://invent.kde.org/sysadmin/release-keyring/-/blob/ea9dd9344eec1a443903fedd7229174c6ff3d9fb/keys/nurmi@key1.asc).
