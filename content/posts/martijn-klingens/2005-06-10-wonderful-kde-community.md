---
title:   "The wonderful KDE community..."
date:    2005-06-10
authors:
  - martijn klingens
slug:    wonderful-kde-community
---
What does it take to join KDE development?<br>
<a href="http://people.fruitsalad.org/martijn/kdepim-meeting-2005/slides/14.badminton.html"> <img src="http://people.fruitsalad.org/martijn/kdepim-meeting-2005/thumbs/14.badminton.jpg">Entertainment</a>?<br> <a href="http://people.fruitsalad.org/martijn/kdepim-meeting-2005/slides/15.food1.html"> <img src="http://people.fruitsalad.org/martijn/kdepim-meeting-2005/thumbs/15.food1.jpg">Delicious Food</a>?<!--break--><br>
<a href="http://people.fruitsalad.org/martijn/kdepim-meeting-2005/slides/23.peacocks2.html"> <img src="http://people.fruitsalad.org/martijn/kdepim-meeting-2005/thumbs/23.peacocks2.jpg">Wonders of Nature</a>?<br>
<a href="http://people.fruitsalad.org/martijn/kdepim-meeting-2005/slides/21.roadmap2.html"> <img src="http://people.fruitsalad.org/martijn/kdepim-meeting-2005/thumbs/21.roadmap2.jpg">Or meditation</a>?<br>
In case you wonder what these images are, I finally got around to upload my pictures from the KDE PIM developer meeting which took place Achtmaal, the Netherlands, two weeks ago. I put the full gallery <a href="http://people.fruitsalad.org/martijn/kdepim-meeting-2005/">here</a>, thanks to the kind <a href="http://www.fruitsalad.org/">Fruit Salad</a> crew for providing me web space without the bandwidth worries of my own ADSL.<br>
If you feel left out and also want to enjoy the pleasures of being a <a href="http://people.fruitsalad.org/martijn/kdepim-meeting-2005/slides/18.group1.html">KDE developer</a>, why not join the community?<br>
Google has a wonderful incentive for students all over the world to join open source development called <a href="http://developer.kde.org/joining/googlesummerofcode.html">Google's Summer of Code</a> that is just waiting for <i>you</i> to submit the abstract of your first addition to KDE!<br>
So far the promo-talk. However, despite being half-joking, the message is real: if you like KDE (and you do, or you wouldn't bother reading the developer blogs :)), why not help us out and become part of the community? We can always use talented artists, documentation writers, developers, marketeers and testers.<br>
Take a look at the <a href="http://developer.kde.org/joining/">KDE Developer's Corner</a> and start contributing! I for one am looking forward to seeing your part of this wonderful desktop in action. And perhaps next time you will be the one <a href=" http://people.fruitsalad.org/martijn/kdepim-meeting-2005/slides/10.table2.html">spinning the wheel</a>. ;-)
