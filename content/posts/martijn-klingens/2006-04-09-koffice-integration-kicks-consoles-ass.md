---
title:   "KOffice integration kicks the console's ass!"
date:    2006-04-09
authors:
  - martijn klingens
slug:    koffice-integration-kicks-consoles-ass
---
Today a friend of mine whom I haven't seen for a couple of weeks came by. Over dinner the subject came to the <a href="http://wiki.kde.org/tiki-index.php?page=multimedia-meeting">KDE Multimedia Meeting</a> that KDE-NL is planning and the fact that we're looking for sponsors. (BTW, if you're reading this and are interested in sponsoring, please drop <a href="mailto:klingens@kde.org">me</a> or one of the other organizers a note!)

Anyway, he told me to send the relevant information by mail and he would try to contact a couple of people. So I looked for the latest version of the sponsor letter, which happened to be a .tar.gz sent by Claire last week.

Having used Unix for way too long and having done sysadmin work for a living I usually save such attachements, extract them, manually launch the associated app, and clean up the mess afterwards. Not very efficient.

I decided to see how well this thing would work without falling back to the console.

<b>Wow</b>.

Or rather: <b>W-O-W</b>.

Whoever says that in order to use Unix for everyday work you need the console should think again. Old habits are hard to kill, and I will probably be using the console for a while to come.

But how much more efficient it has been to <i>not</i> use it has been quite a bit of an eye-opener.

I opened the tarball in Ark, saw three OpenDocument files and clicked the first one to view if that was the right one. OpenOffice is still my default office suite for reasons including for a large part laziness, so I braced myself for its 15+-second startup time.

Much to my delight I got the preview of the document in <b>one second</b>! Even nicer, Ark just loaded the KOffice part and used that to view the file. After all, I didn't want a complete office suite, I wanted a preview.

If KOffice lives up to its expectations and delivers tools that are feature-wise comparable to OpenOffice then the future will be KOffice. This integration totally rocks, and it's something that OpenOffice will have a hard time to deliver. Its code base has proven hard enough to KDE-ify (and it still doesn't feel entirely KDE-ish, since e.g. middle clicking on the scrollbar doesn't work as expected), breaking it up even further into embeddable components seems like a pipe dream to me.

In the mean time, KOffice already delivers.

And it's <b>fast</b>. About a second on a Pentium M running at its energy-saving 600 MHz is amazingly fast. Nothing preloaded, KWord wasn't even in the cache. *gasp*

So, if you want to hack on the office suite of the future, join the KOffice team! After this revelation I can't wait to see it become mature enough to allow a switch :)

(And this was using KOffice 1.4, I haven't even tried 1.5 yet. How much more will they deliver with that?)
<!--break-->