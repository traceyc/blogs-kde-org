---
title:   "Wanted: Brave Developers"
date:    2005-08-14
authors:
  - martijn klingens
slug:    wanted-brave-developers
---
<p><b>Introducing: KExtProcess</b></p>
<p>Those who have tracked my KDE commits this year will have noticed that most of them went to <a href="http://websvn.kde.org/trunk/playground/sysadmin/kextprocess/">KExtProcess</a>, my pet Swiss Army Knife for anyone who writes code that calls external applications.</p><br>
<p>Due to several reasons I haven't been able to put a whole lot of time in it, so it took me almost 5 months to get from version 0.3 to 0.4, while the amount of man-hours actually put into it probably doesn't exceed 80 hours. But at long last it's there now, <a href="http://people.fruitsalad.org/martijn/kextprocess/0.4/">KExtProcess 0.4</a>!</p><br>
<p>If you've never heard of KExtProcess before, I won't be surprised. The three previous versions were basically just my own internal milestones towards this version. KExtProcess 0.4 has been the target version where I wanted to give it more exposure, so if you are a developer and use any of the following technologies in your code, read on:</p>
<ul><li>KProcess</li>
<li>kdesu</li>
<li>ssh</li>
<li>KIO-Fish</li>
<li>su</li></ul>
<!--break-->
<p>KExtProcess is a library for application developers. Although I do have two demo applications, KExtProcess is <b>not</b> an application; it's purely a library.</p><br>
<p>KExtProcess is named and modelled after <a href="http://developer.kde.org/documentation/library/cvs-api/kdelibs-apidocs/kdecore/html/classKProcess.html">KProcess</a>, the KDE class for calling external applications.</p><br>
<p><b>Crossing the Border: Beyond KProcess</b></p>
<p>The really cool thing about KExtProcess is that it is called an <i>Extended</i> KProcess for a reason: it is not limited to the computer and the user account you're running your application with. That's right, it allows you to run processes as another user with <b>almost the same API as KProcess</b>!</p><br>
<p>With just a few lines of code you can easily run an application over SSH on your server, write to its input and read its output. Writing GUI applications for system administration is a lot easier. And it doesn't even require the user to have KDE installed on his or her server, even X is not required!</p><br>
<p>And since nobody is going to read on without a teaser image (and I need you, so you'll have to read on :) ), here's a screenshot of the demo application showing KExtProcess in action:</p><br>
<img src="http://people.fruitsalad.org/martijn/kextprocess/0.4/img/kep3.png"><br>
<p><b>Crossing More Borders: Multiple Hops</b></p>
<p>Many administrators don't allow logging in as root over SSH directly, and many remote systems in larger companies require 'club-hopping' over multiple servers instead of allowing direct connections from workstation to end-node.</p><br>
<p>As I have been working on exactly such a setup on my previous job I know the related problems all too well: KIO-SFTP and KIO-Fish cannot be used for most remote administration if you can't login as root remotely. KExtProcess is explicitly designed to overcome this problem by allowing the application to chain multiple steps together and treat them as a single logical process.</p><br>
<p><b>Crossing the Ultimate Border: Empowering the User</b></p>
<p>Compared to using KExtProcess for a direct SSH login to <i>root@server</i> it doesn't take a lot more code to login as <i><b>user</b>@server</i> instead and have KExtProcess then use a second <a href="http://websvn.kde.org/trunk/playground/sysadmin/kextprocess/lib/suprocess.h?view=markup">KExtProcess::SuProcess</a> step to 'su' to root.</p><br>
<p>But, that wouldn't be very flexible, no?</p><br>
<p>Nope, and that's why KExtProcess sports <a href="http://websvn.kde.org/trunk/playground/sysadmin/kextprocess/lib/profile.h?view=markup"><i>Profiles</i></a>, which hold all the steps needed before starting the process in a single entity. On top of that there's the profile editor that can be plugged from a GUI easily to have the user manage the profiles him- or herself.</p><br>
<p>Profiles are shared across KDE applications, so a profile that works for one application will work with all of them, maximizing the ease of use for the user.</p><br>
<p>GUI-wise the profile editor is not really finished yet. At <a href="http://conference2005.kde.org/">aKademy</a> I plan to sit down with <a href="http://aseigo.blogspot.com/">Aaron</a> and the rest of the usability gang to replace the dialog with something prettier. From a developer's point of view that doesn't make much of a difference as the API will not have to change much, if at all. In case you are interested in the current profile editor, here's a screenshot:</p><br>
<img src="http://people.fruitsalad.org/martijn/kextprocess/0.4/img/kep-profile-edit.png"><br>
<p><b>So Why do I Need You?</b></p>
<p>That's actually pretty simple: KExtProcess isn't of much value if nobody uses it. So I need developers who want to port their applications to it. I want feedback on the API, bug reports, applications using KExtProcess and ideally people who want to help out making KExtProcess kick even more ass in its next versions.</p><br>
<p>Be aware though, <b>KExtProcess is not production-ready yet</b>! It's ready for early-adopting developers now, but certainly not for end users on mission critical systems. The two most important known issues:</p>
<ul><li>If you enter passwords in a profile they will be stored in plaintext instead of inside KWallet. The obvious workaround is to not store passwords in the profile for now. Just leave the field empty and KExtProcess will prompt you for the password instead.</li>
<li>Newline handling is flawed at best. SuProcess and SSHProcess need PTY support even if you don't enable it for the 'real' process on the final hop. Data that comes back through a PTY, however, uses CR/LF rather than just LF, and currently KExtProcess 'solves' this by treating the two as equivalent. Needless to say that this basically prohibits the use of KExtProcess on binary data for now.</li></ul>
<p><b>Final Words</b></p>
<p>This blog entry is getting way too long already, so I'll end it here. The upcoming days I'll post additional blog entries with the rest of what I wanted to tell and some more screenshots. For now, if you're the application developer of, say, KSystemLog, KPackage or anything else that uses su, SSH or could otherwise benefit from distributed process execution, take a look at the source code of the <a href="http://websvn.kde.org/*checkout*/trunk/playground/sysadmin/kextprocess/examples/keptail/keptail.cpp?content-type=text%2Fplain">KExtProcess Tail Demo</a> to see the ease of use. I'll post a better example later this week, so stay tuned for more cool KExtProcess news!