---
title:   "KDE Four Multimedia Meeting Aftermath"
date:    2006-06-13
authors:
  - martijn klingens
slug:    kde-four-multimedia-meeting-aftermath
---
For most people the <a href="http://www.kde.nl/agenda/2006-05-k3m/index-en.php">KDE Four Multimedia Meeting</a> (K4M) was concluded on Sunday, May 28th, almost two weeks ago.

Sebastian put up his <a href="http://vizzzion.org/?id=gallery&gcat=KDE4MultimediaMeeting">picture gallery</a> during the event, Claire has put up <a href="http://elektron.et.tudelft.nl/~cmlot/articles/k3m2006/index.html">her pictures</a> as well.

Mine were still missing.
<!--break-->
There's quite a bit of work left for the organization team. Writing documentation for next events, creating a Final Report for the sponsors, having a post-K4M evaluation and finalizing the financial issues are just some of the tasks that we have to do.

Most of these are well underway now, so I finally found time to upload my <a href="http://people.fruitsalad.org/martijn/k3m-2006/">photo gallery</a> of the meeting. Enjoy!

In non-KDE news, my past two weeks have been very busy due to the mixture of busy work, the K4M stuff and preparing my ITIL Foundations exam last Friday (which I fortunately passed, and with a decent 36 out of 40 questions correct).

Needless to say I could use a bit of relaxing, so I was very happy when Claire asked whether I was able to go to the Da Vinci Code movie on Friday evening. Contrary to most reviews I actually liked the movie, as did Claire, Hendrik and Erica after we went there.

I continued my quest for relaxing on Saturday, first with a family meeting that basically involved spending a Saturday on a towel on the grass in the hot sun. Very hard work indeed :)

On the way home by brother and I first went into downtown Rotterdam for DJ Tiësto's performance for the Volvo Ocean Race. Whether you like trance music or not, the show itself was awesome, because the Erasmus Bridge and the surrounding buildings were used as projection screens for gorgeous <a href="http://www.skyscrapercity.com/showthread.php?t=361064">lighting effects</a>. (Pictures courtesy of my brother. How he managed to create sharp pictures on a wobbling bench in a dark night sky with a zoom lense is beyond me. I don't want to know how many pictures he had to throw away because they were blurred. Anyway, I'm happy with the result, so thanks for letting me link to them! :) )
