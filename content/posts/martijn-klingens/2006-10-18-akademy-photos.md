---
title:   "aKademy Photos"
date:    2006-10-18
authors:
  - martijn klingens
slug:    akademy-photos
---
It's about time: my photos from aKademy are online. :)

With aKademy being over for over two weeks already and the <a href="http://akademy2007.kde.org/">next aKademy</a> in <a href="http://www.cis.strath.ac.uk/">Glasgow</a> already announced that's long overdue.

There are not many KDE-related pictures in there because I didn't carry my camera with me during the first few conference days. I tried to compensate at the end by taking pictures of Aaron Seigo handing over the big Konqi to Marcus Furlong to thank him for organizing this excellent aKademy.

<a href="http://people.fruitsalad.org/martijn/akademy-2006/slides/7.konqi-02.html"><img src="http://people.fruitsalad.org/martijn/akademy-2006/thumbs/7.konqi-02.jpg" /></a> (See the <a href="http://people.fruitsalad.org/martijn/akademy-2006/index6.html">full gallery</a> for more Konqi images.)

The rest of the gallery consists of pictures from Dublin. I'm especially fond of two pictures I took at night:

<a href="http://people.fruitsalad.org/martijn/akademy-2006/slides/2.dublin-09.html"><img src="http://people.fruitsalad.org/martijn/akademy-2006/thumbs/2.dublin-09.jpg" /></a> and <a href="http://people.fruitsalad.org/martijn/akademy-2006/slides/2.dublin-13.html"><img src="http://people.fruitsalad.org/martijn/akademy-2006/thumbs/2.dublin-13.jpg" /></a>

If you want to see more, just <a href="http://people.fruitsalad.org/martijn/akademy-2006/index.html">visit the gallery</a>.

<b>EXIF</b>
For the technically inclined photographers who wonder why there's no EXIF-information, I have still been shooting film this year. The camera used is a Canon EOS 300V with Fuji Superia 200 ISO films.

As for the lenses, my main lense is the Canon EF 28-105mm f/3.5-4.5 USM, which I used for the bulk of the photos. For some zoom shots (notably the birds) I used the telephoto kit lense that came with the camera, the Canon EF 90-300mm f/4-5.6. Because of the low-light conditions most of the night shots were taken with the Canon EF 50mm f/1.8 lense.

One picture is special. The Wall of Fame is taken with a Sigma fish-eye lense, that I could kindly borrow from <a href="http://www.figuiere.net/hub">Hubert Figuière</a>. My own wide-angle stops at 28mm, which wasn't nearly enough to fit the wall in a single picture. Thanks, Hubert, for allowing me to take an 'impossible' shot!

<a href="http://people.fruitsalad.org/martijn/akademy-2006/slides/2.dublin-14.html"><img src="http://people.fruitsalad.org/martijn/akademy-2006/thumbs/2.dublin-14.jpg" /></a>

I haven't used any filters other than UV, though some of the pictures have seen minor retouching with Gimp before I put them online. I tried to leave most pictures untouched, or only cropped, however.

Enjoy! :)
<!--break-->