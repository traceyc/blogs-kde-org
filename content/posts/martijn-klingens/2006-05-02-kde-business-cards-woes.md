---
title:   "KDE Business Cards Woes"
date:    2006-05-02
authors:
  - martijn klingens
slug:    kde-business-cards-woes
---
In my <a href="http://blogs.kde.org/node/1949">last blog</a> I said that I expected my KDE business cards within a couple of days. One day later Adriaan and Sebas got theirs. I live at the other end of the Netherlands, I'm a West Coast guy, and as we all know the Netherlands are a huge country. It's <i>at least</i> 150 km from Adriaan and Sebas to my place. <i>*cough*</i> How long would that take a postal service?

Almost a week.

I guess they bridged the distance by tiptoe, because otherwise I just don't have an explanation for the 5-days waiting.

Or perhaps I do.

Package delivery services around the globe are all slightly different in their paperwork, but the basic idea is always that you fill in the data on a bunch of carbon paper sheets. You give one to the delivery service, keep one for yourself, stick one in the plastic bag and one is usually adhesive and is used by the postal service, especially if you're using normal postal delivery instead of some express service.

Judging by what I received Deutsche Post is no different. I shouldn't be able to know that, since I've never used them, I don't even live in Germany. But whoever prepared my package of business cards certainly didn't RTFM.

I got <b>all</b> of the sheets.

Inside the plastic bag.

Including the sticker that's supposed to show the address to the delivery man.

This bag is not meant to be readable to the postal service. So probably the package went to some central office where they opened the plastic bag to read the address, explaining the delay. In any case, I got the package with the plastic bag opened. I guess I should be happy the thing arrived at all :)

Being in the marketing working group and active at events has the privilege that I'm in the first batch of cards. The downside is that this batch went far from smoothly, and the text on our cards has slightly different formatting since we decided to change the text a bit.

Currently we're doing some more small batches, and hopefully we can offer the business cards to other KDE e.V. members in only a couple of weeks. If you want to see what you can expect, the public part of the process is almost finished and now visible on <a href="http://www.spreadkde.org/business_cards">SpreadKDE</a>.

(What you can't see is the amount of behind-the-scenes work that we still have to do to allow a smooth process, so please be patient. But the business cards are coming real soon now!)
<!--break-->