---
title:   "KDE4: Hints, Bugs, Fixes"
date:    2007-10-22
authors:
  - awinterz
slug:    kde4-hints-bugs-fixes
---
A running list of stuff about the KDE4 desktop -- typed in from little slips of paper I have laying around on my desk:
<ul>
<li>To make the desktop run faster, compile qt-copy without the -debug option
<br>
<li>My computer has a Radeon video card and the X server apparently uses an ATI driver. This driver seems have to problem with compositing. If you see "screen turds" and other strange graphics effects, especially with the menus and icons, try disabling compositing... which seems to be enabled by default on my system.
Do this by adding these lines to the end of xorg.conf and reboot:
<pre>
Section "Extensions"
    Option      "Composite"     "disable"
EndSection
</pre>
Oh, disabling compositing should also fix 'ksnapshot'.
<br>
<li>If you start a KDE4 application from a command line, you might notice a bunch of messages that indicate this app can't talk to knotify.  That's because 'knotify4' probably isn't running.  You can remedy this by running 'knotify4' from a command line. Also, I hacked my 'startkde' to run 'knotify4' for me when I login.  I plan to commit this to the official 'startkde' tomorrow, unless someone objects on the k-c-d mailing list.
<br>
<li>BUG: 'kwalletmanager' gives me this message "kwalletmanager(4467) KWalletManager::KWalletManager: kded not running?".  Then it displays a closed wallet in the systray. dfaure said he will fix this on Monday -- if I remind him. :)
<br>
<li>BUG: 'kmail' isn't remembering shortcuts between restarts.
<br>
<li>BUGLET: hmmm.. seems that instances of 'kded4' continue running after I quit my KDE4 session. 
<br>
<li>BUG: There is no nepomuk D-Bus service file.  We need one if we expect nepomukdaemon to start as needed; for example, from 'dolphin'.
<br>
<li>BUG: Alt-F2 doesn't work.  I expect that key sequence to start 'krunner'.
<br>
<li>WTH? KOrganizer is unbearably slow. I see no reason for this and KOrganizer ran great for me under a KDE3 desktop.  This problem needs some serious research. Does KOrganizer work ok for anyone?
</ul>