---
title:   "Too Many Bugs"
date:    2006-10-17
authors:
  - awinterz
slug:    too-many-bugs
---
I spent many hours the past couple months bug fixing two of my favorite KDE PIM applications (KOrganizer and KMail).  And I'm not even making a small dent in the bug pile.
<p>
700+ Open KOrganizer Bugs
<p>
2600+ Open KMail Bugs
<p>
Sure, many of them are wishes. But many are real bugs and crashes too.
<p>
It would really help if people could upgrade to KDE 3.5.5 (or at least KDE 3.5.4) and then let us know about bugs that seem to be fixed.  Every little bit helps.  If the bug is still present, any extra info you could attach to the bug report may also give us a hint.  Backtraces are invaluable when going after crashes.
<p>
No need to blast us with simple "bug still present in KDE 3.5.5" messages.
