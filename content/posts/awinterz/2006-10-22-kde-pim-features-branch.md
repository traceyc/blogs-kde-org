---
title:   "KDE-PIM Features Branch"
date:    2006-10-22
authors:
  - awinterz
slug:    kde-pim-features-branch
---
I just could not stand it anymore.  I had to have new features in KDE-PIM. So now we have $SVN/branches/work/kdepim-3.5.5+.
<p>
For those who want to play/test you'll find that KMail has templates (thanks to Dmitry) and the Kontact summary is getting some love.  I hope some proko2 features also make their way into this branch.
<p>
No telling when these features (and others I hoping to add) will make their way into the official KDE 3.5.x releases.  
