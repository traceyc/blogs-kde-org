---
title:   "Thank You Note"
date:    2008-07-10
authors:
  - awinterz
slug:    thank-you-note
---
As we near the first release ever of KDE-PIM for KDE4, I want to take this opportunity to thank the many folks who helped the kdepim team move to a stable release in a short 6 months.

On behalf of our small group of KDE-PIM developers, Thank You! to the KDE BugSquad, the people who hang out on IRC and help with quick testing of new code, the beta testers and bug reporters, the triagers, and everyone else who gave us kind words of encouragement. We really couldn't have made it happen without you.

If you asked me 6 months ago if we would see KDE-PIM released with KDE 4.1 I would have said.. I doubt it.  In fact, I probably did somewhere if you search our mailing list archives :)  We still have plenty of bugs to fix, and features to implement; but, for the first time in years I feel confident that KDE-PIM will enjoy a successful future in KDE4.  On to Akonadi and KDE 4.2!

Regards,
Allen, KDE-PIM Module Coordinator
