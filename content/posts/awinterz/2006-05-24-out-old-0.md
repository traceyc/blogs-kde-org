---
title:   "Out with the Old"
date:    2006-05-24
authors:
  - awinterz
slug:    out-old-0
---
<b>Bad News:</b>  As mentioned <a href="http://blogs.kde.org/node/2004">in a previous blog</a> my notebook's harddrive did pass silently yesterday afternoon.  This was my main KDE development system.

<p>
<b>Good News:</b>  I decided to use this opportunity to purchase a new HP Pavilion desktop system.  Yes, the new system will have lots more processing power and RAM, but I'm really excited about the 19" flat screen.

<p>
I'll eventually put a new harddrive in the notebook for other purposes.  But for now, the tired old-soldier is patiently awaiting further instructions.

<p>
<b>Bottom Line:</b> No KDE development for a least a week.
