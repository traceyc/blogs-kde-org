---
title:   "More KDE-PIM Features"
date:    2006-10-24
authors:
  - awinterz
slug:    more-kde-pim-features
---
In today's exciting episode of "The KDE-PIM Features Branch" we have

<ul>
<li><a href="http://www.kde-apps.org/content/show.php?content=36322">Monur's KMail tagging</a> patch that "...introduces custom tagging support for emails..."</li>
<br>
<li><a href="http://lists.kde.org/?l=kmail-devel&m=112454256600012&w=2">Johnathan's KMail Templates</a> patch "...There is a new special folder "templates"; messages can be copied into here just like any other, or saved here using the 'Message - Save as Template' menu option in the composer window.  This folder works similarly to the "drafts" folder; double-clicking on a message or choosing the "Use Template" option on the popup menu starts the composer on a copy of that message, but does not of course delete the original template message..."</li>
<br>
<li>From me, in the Kontact Summary:
  - Appointments RMB menu to Edit and Delete
  - To-Dos RMB menu to Edit, Delete, or Mark Completed
</li>
<br>
</ul>

Find all these goodies in $SVN/branches/work/kdepim-3.5.5+.
<p>
The more testing and feedback we get the better chance we have to include these new features in future KDE 3.5.x releases.  I'm planning to close down the branch for new features to be considered for KDE 3.5.6 on or about 1 November.
