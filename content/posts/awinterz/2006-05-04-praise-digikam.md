---
title:   "In Praise of DigiKam"
date:    2006-05-04
authors:
  - awinterz
slug:    praise-digikam
---
This is my first blog entry in a long, long time.

<p>
And I will use the occassion to praise and thank the <a href="http://www.digikam.org/?q=contact">digiKam developers</a> for their wonderful application.

<p>
I hadn't used <a href="http://www.digikam.org">digiKam</a> for some time, but yesterday several dozen digital pictures I needed to resize, crop, enhance, and email to family.  digiKam made the entire process easy and fun to do. A joy.  Thanks again digiKam team!

