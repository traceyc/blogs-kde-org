---
title:   "KOrganizer Printing -- Call for Help"
date:    2008-08-15
authors:
  - awinterz
slug:    korganizer-printing-call-help
---
Printing in KOrganizer is really in need of some attention.  Especially now that we are providing RichText capabilities in calendar descriptions and elsewhere.  You can even write journal entries entirely in RichText.  But the printing system doesn't know anything about RichText.

There are dozens of outstanding bugs related to printing in KOrganizer.

The job involves knowledge of low-level drawing primitives (move-draw, set pens and brushes.. stuff like that).

Since I rarely print (and never print calendars), and since this type of work does not interest me, I am hereby asking/begging for volunteers to become the "KOrganizer Master Printer".

Take <a href="http://bugs.kde.org/show_bug.cgi?id=167751">bug 167751 </a>for example.

The code in kdepim/korganizer/printing is waiting for you... 


