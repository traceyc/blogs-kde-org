---
title:   "api.kde.org is back"
date:    2010-08-03
authors:
  - awinterz
slug:    apikdeorg-back
---
<a href="http://api.kde.org">api.kde.org</a> is operational again.

I can help fix problems you might notice on the site (bad-links, etc), but please don't bother me about content -- contact the project authors, maintainers, module coordinators if you encounter missing/outdated/broken/wrong API documentation.

Bugs can reported to the <a href="https://bugs.kde.org/enter_sysadmin_request.cgi">KDE sysadmins</a> filed under the api.kde.org component.
