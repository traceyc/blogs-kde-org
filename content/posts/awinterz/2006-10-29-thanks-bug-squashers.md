---
title:   "Thanks Bug Squashers!"
date:    2006-10-29
authors:
  - awinterz
slug:    thanks-bug-squashers
---
I just stood back and watched in awe today.  Bug Squashers from all over the world came to the rescue of the PIM developers on #kde-bugs.  Over 100 bugs gone.  Wow!  The Bug Triage Days are a fantastic idea and a wonder to behold.

Speaking for all KDE-PIM developers, past, present (and future). I want to publicly thank all those who came out to help.  Let's do it again tomorrow.

I wonder.. does anyone want to volunteer to be the KMail or KOrganizer "Bug Triage" person?  This person(s) would be responsible for the triage of all new bugs: sort of a layer between the developers and the bug reporters?  We could maybe expand this to the KMail/KOrg Quality Team that manages the bugs, web presence, artwork creation, etc. No development experience would be necessary.  Just some organizational skills.  An idea anyway.

But, to the main point of this blog: Thank You Bug Squashers!