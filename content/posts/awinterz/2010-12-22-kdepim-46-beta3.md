---
title:   "KDEPIM 4.6 Beta3"
date:    2010-12-22
authors:
  - awinterz
slug:    kdepim-46-beta3
---
Today KDEPIM 4.6 Beta3 is tagged and released.

This is the first release in our departure from the normal KDE SC 4.6 cycle, <strike>but you should be able to build against kdelibs and kdepimlibs from either the beta2 or the upcoming first release candidate</strike> <b>UPDATE: building against the KDE SC 4.6RC1 kdepimlibs is required -- which should be available soon.</b>

Note that it does require the 1.4.90 Akonadi server which was just released yesterday.

Once the mirrors catch-up you should find the source code in <a href="ftp://ftp.kde.org/pub/kde/unstable/kdepim">ftp://ftp.kde.org/pub/kde/unstable/kdepim</a>.

We'll re-evaluate our progress (based on your constructive feedback) in a week or so and decide on our next release -- on our way to KDEPIM 4.6.