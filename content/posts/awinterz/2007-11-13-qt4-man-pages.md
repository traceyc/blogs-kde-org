---
title:   "Qt4 Man Pages"
date:    2007-11-13
authors:
  - awinterz
slug:    qt4-man-pages
---
Lazy Web warning:

I looked and looked.. I even tried to figure out qdoc3.  No joy.

Anyone know how to generate the man pages for Qt4?  Or where I can get a tarball with the man pages?

In Fedora Core6 there is an RPM called qt-devel-docs that has the man pages for Qt3, so man pages are possible.