---
title:   "KDEPIM 4.6.1 Released"
date:    2011-07-06
authors:
  - awinterz
slug:    kdepim-461-released
---
KDEPIM 4.6.1 is hereby released.
<p>
You can find it in <a href="ftp://ftp.kde.org/pub/kde/stable/kdepim-4.6.1/src">ftp://ftp.kde.org/pub/kde/stable/kdepim-4.6.1/src</a>
<p>
You'll need both kdepim and kdepim-runtime, and please make sure to have the most recent Akonadi, Soprano, kdelibs4, kdepimlibs4.6 and friends.
<p>
Also shared-desktop-ontologies (SDO) 0.6.x is required -- kdepim 4.6.1 will not build against newer versions of SDO.
<p>
See the list of <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=akregator&product=blogilo&product=kaddressbook&product=kdepim&product=kjots&product=kmail2&product=kontact&product=korganizer&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&resolution=FIXED&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-06&chfieldto=Now&chfield=resolution&chfieldvalue=FIXED&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0=">bugs we fixed since 4.6.0</a>.

<p>
I don't know yet if we'll make 4.6.2 release, as KDE SC 4.7 is fast approaching.