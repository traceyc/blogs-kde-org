---
title:   "Custom Templates for KMail"
date:    2006-10-29
authors:
  - awinterz
slug:    custom-templates-kmail
---
Thanks to Dmitry, the KMail in the $SVN/branches/work/kdepim-3.5.5+ branch now can do Custom Templates.  Look in the KMail configuration, under the Composer page where you'll see a "Custom Templates" tab.

I'm not sure if we still need the Templates folder feature... hmm...

Let us know what you think.  Please test and send your review to kmail-devel@kde.org

