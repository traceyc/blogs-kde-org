---
title:   "Tagging: KDE SC 4.5.1"
date:    2010-08-25
authors:
  - awinterz
slug:    tagging-kde-sc-451
---
A friendly note that KDE SC 4.5.1 will be tagged on 26 August.

So keep those bug fixes coming.