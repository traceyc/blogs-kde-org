---
title:   "KMail Hacking Days"
date:    2007-04-20
authors:
  - awinterz
slug:    kmail-hacking-days
---
This weekend we PIMsters are having a virtual meeting on #kontact; hopefully to give a lot of love to KMail for a potential KDE 4.0 release.  Most of the KDE PIM applications are in dire need of attention and, in their current state, have almost no hope of being part of a KDE 4.x release.

So, this is an attempt to put a lot of effort in a short amount of time into one of the more critical KDE applications.  Note that we need to know if there will be a kdepim 4.0 release by 1 June 2007.

A partial list of things to work on:
* port q3 to q4
* port kde3 to kde4 (i.e. port k3stuff)
* port deprecated code
* fix compile warnings
* fix layouts
* finish dcop -> D-Bus port
* test and make stuff work

If we get have some good results with this virtual hackfest I hope we can arrange a KOrganizer Hacking Days soon.
