---
title:   "KDEPIM 4.4.7 Preparations"
date:    2010-10-08
authors:
  - awinterz
slug:    kdepim-447-preparations
---
Bug fixes are still coming into the KDEPIM 4.4 branch, so I'm planning to make a 4.4.7 release.

Tagging in a week or two -- current estimate is 21 October.

Just keep this in mind and feel free to continue committing bug fixes (no new i18n strings and no new features) in the 4.4 kdepim branch.
