---
title:   "eGroupWare Resource (Going, going..?)"
date:    2008-08-04
authors:
  - awinterz
slug:    egroupware-resource-going-going
---
The eGroupWare resource in KDE-PIM hasn't gotten much love and attention in a long time.  It compiles, but we have no idea if it really works, nor do we have anyone on the PIM team who uses it.

This is an open call for either 1) someone to take over the maintainership of the eGroupWare kresource or 2) someone to at least help us by testing patches and giving feedback.

Else... we may be forced to remove this resource in KDE-PIM 4.2.

Volunteers please contact me directly or send a message to the kde-pim mailing list.