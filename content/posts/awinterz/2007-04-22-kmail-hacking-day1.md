---
title:   "KMail Hacking Day1"
date:    2007-04-22
authors:
  - awinterz
slug:    kmail-hacking-day1
---
The first KMail hacking day is coming to a close. It has been a fun time -- working with many oldtime PIMsters and several new contributors as well. I noticed that KOrganizer was getting a little attention too.  

Much of my coding time was spent adapting the toolbar to <a href="http://weblog.obso1337.org/2007/kde4-application-toolbar-specs">Ellen and Seele's guidelines</a>. After some sillyness on my part (like putting the Spam and Virus Wizards on the toolbar) I think I have things under control. Note: because the toolbar puts text under the icons you will want to use QAction::setIconText("ShortText") so the typically too long menu text doesn't go on the toolbar.

And while typing this entry I get a message from Ingo that KMail can now send again! Yippee.  Now we need the Qt4.3 patches which we think will fix TLS and SSL.

More fun tomorrow.