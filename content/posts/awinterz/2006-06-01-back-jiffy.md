---
title:   "Back in a Jiffy"
date:    2006-06-01
authors:
  - awinterz
slug:    back-jiffy
---
My new HP Pavilion desktop system "jiffy" arrived yesterday (exactly as FedEx promised).  Fedora Core 4 Linux installed painlessly without even 1 hiccup last night.  Today I am putting the finishing touches on the system: installing extra RPMs; configure printing and scanning; etc.

<p>
I kept Windows on a 12GB partition (so I can do my taxes with TaxCut).  Whoop-de-doo.

<p>
So, with a dual boot system I still have 50GB devoted to KDE development! Time to light-up the harddrive with 'svn up'.

<p>
Wait, there is one hiccup I just remembered:  the soundcard isn't being detected.  I need to investigate that one.  I won't be happy without <a href="http://amarok.kde.org">amarok</a>.
