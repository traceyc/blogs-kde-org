---
title:   "APIDOX"
date:    2006-07-22
authors:
  - awinterz
slug:    apidox
---
I spent quite a bit of time the past couple of days attempting to beautify and fix some minor bugs in the APIDOX generating stuff for KDE trunk.  Sure, I should have waited until [ade] returned from vacation.
<p>
But I didn't.
<p>
And, apparently I broke something badly -- <a href="http://www.englishbreakfastnetwork.org/apidocs/?component=kde-4.0">the APIDOX for KDE4 on the EBN</a> is completely empty this morning.
<p>
whoops.
