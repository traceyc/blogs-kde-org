---
title:   "HTML Signature in KDE-PIM Features Branch"
date:    2006-10-26
authors:
  - awinterz
slug:    html-signature-kde-pim-features-branch
---
As we (Dmitry and Johnathan) work out the new templates bugs and use cases, I decided to apply a patch provided by Jason Keirstead from more than 1.5 years ago that would satisfy one of our most requested KMail features: HTML Signatures.

We really, really need people to test these features if we have any chance to get them included in the KDE 3.5.6 release.

The code can be found in $SVN/branches/work/kdepim-3.5.5+ and it builds just the same way the regular old kdepim 3.5 branch does (using auto* tools).

Send feedback to the kmail-devel@kde.org mailing list or directly to me.
