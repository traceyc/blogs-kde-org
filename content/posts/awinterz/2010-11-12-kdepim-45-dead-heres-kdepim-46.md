---
title:   "KDEPIM 4.5 is Dead -- Here's to KDEPIM 4.6"
date:    2010-11-12
authors:
  - awinterz
slug:    kdepim-45-dead-heres-kdepim-46
---
Well, the ramp-up for KDE SC 4.6 has begun now, with soft freezes taking effect yesterday and the first beta due in about 1 week.


So... we have decided that there is no point to putting any more effort into the long-awaited KDEPIM 4.5.


99.9% of the KDEPIM team's effort will go into the 4.6 release.
0.1% effort remains for a probable KDEPIM 4.4.8 at the end of this month.


KDEPIM 4.6 !