---
title:   "Countdown"
date:    2006-07-14
authors:
  - rockman
slug:    countdown
---
Well, time for exams has come, only a few days and i'll be killed... err.. i mean.. oh.. whatever :D
I really don't feel to much ready, but we'll see.. maybe i'll be lucky now.
The nice news is that, whatever will be exams result, i'll restart coding on <a href="http://www.kmobiletools.org" target="_BLANK">KMobileTools</a>, probably after july the 20th.
Please, please, please: we <b><u><i>REALLY</i></u></b> need help, so if anyone would like to help us coding please contact me.

You'll find contacts on:
<b>IRC</b>: #kmobiletools on freenode network (ask RockMan, or Bochi if you're interested on gammu engine).
<b>Mail/IM/whatever</b>: my website is <a href="http://rockman.altervista.org" target="_BLANK">http://rockman.altervista.org</a>, there you'll find everything in the "Contact" page.
Current work to do is: 
<ul>
<li>Finishing and bugfixing AT engine (the more advanced at this time)</li>
<li>Doing a lot of work to get Gammu engine working</li>
<li>Fixing API codebase (i'll do this one by myself, but if anyone wants to help, it's really welcome..)</li>
</ul>
There are also some long term jobs.. i'm not really going to implement these now, since the lack of developers, but of course, if you feel you can do that, you're welcome:
<ul><li>SyncML stuff: i'd want to use <a href="http://www.opensync.org" target="_BLANK">OpenSync</a> stuff, please ask also on <a href="https://mail.kde.org/mailman/listinfo/kde-pim" target="_BLANK">KDEPIM Mailing List</a> since this is something i'd like to implement with them</li>
<li>FileSystem stuff: i already implemented a p2k kioslave, there's a kioslave for obex too on the kdebluetooth project (dead? someone wants to take it?), and there's also code around the web for siefs (siemens), and something else too. We'll have to fix all these, and do a common codebase like a mobile:/ filesystem, redirecting to p2k:/ obex:/ or whatever, depending on the phone.</li>
</ul>
We don't need some extraordinary coding skills: just ordinary C or C++ knowledge, and QT/KDE api (but imho the're really easy to learn, if you're already know a object oriented language..).
Anyway, it depends on the task you want to do: for gammu you'll need C, for AT Engine only C++, and a bit more advanced QT, for kioslaves an advanced kde knowledge, and so on.. asking directly to me will be better.
I'd like to release kmobiletools 0.5 in a month or so.. just hoping i'll find someone helping, otherwise it'll be really hard to do.
And this is a preview of the current status: <a href="http://www.kmobiletools.org/kmt" target="_BLANK">http://www.kmobiletools.org/kmt</a>
<!--break-->