---
title:   "Bluez 3.x and (kdebluetooth) pin helpers."
date:    2006-09-07
authors:
  - rockman
slug:    bluez-3x-and-kdebluetooth-pin-helpers
---
After being a bit disappointed that Bluez-utils 3.x simply doesn't accept old pin-helpers, like kbluepin from kdebluetooth, i thought about the best solution for this.
Of course, we should develop a new pin helper, which can communicate with bluez over dbus.. and with kde4 supporting dbus it will be very easy.. but in the meantime, you can enjoy <a href="http://www.kmobiletools.org/node/228" target="_BLANK">this patch</a>.
It modify the standard pin agent, so that, instead of giving it the pin as argument, you can give it the path of the pin-helper to execute.
I'm using it in the new version of the live cd of kmobiletools.
Hope you like it!