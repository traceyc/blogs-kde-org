---
title:   "Light coding"
date:    2006-06-21
authors:
  - rockman
slug:    light-coding
---
It's a really boring time.. too hot, too much things to study at university.. and very few free time.
So i had to stop working at kmobiletools (hope i'll work again on it soon.. maybe at the end of july), since it's too complex to develop now.. instead i'm working on some patches here and there on kde (i'll publish them soon, in another post), and to very smalls applications.<!--break--><br>
First i've done <a href="http://www.kmobiletools.org/node/163">CheckCalls</a>, a little daemon based on some kmobiletools code, that checks incoming calls, and do some actions depending on the number.
I'm using it on my little home server, to make it turn on the desktop PC while i'm not at home, by wake on lan.<br>
Then i also made a replacement for video preview, <a href="http://www.kde-apps.org/content/show.php?content=41180">MPlayerThumbs</a>
It's based on mplayer only, instead of the default one that's based on xine and arts.
So on x86-64 systems it can use the 32bit version of mplayer, which can load win32codecs.
<br>
On the university side, i'm trying to finally pass math, so it seems i'll be still very, very busy for a long time :P