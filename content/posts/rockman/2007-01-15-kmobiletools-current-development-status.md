---
title:   "KMobileTools - Current development status"
date:    2007-01-15
authors:
  - rockman
slug:    kmobiletools-current-development-status
---
Since many of you already noticed the lack of subversion updates, and also of the homepage too, i guess i should write it here too.
Yes, development is currently suspended (again).
I've some university exams until 15/02/2007, so probably there will be no more updates until then.
I must anyway tell you i'm quite disappointed. After all these years, kmobiletools still is a one-man project. It's shameful in my point of view that if someone is busy, for study, work, real life, or (why not) a girlfriend, the entire project freezes.
Even if i'm studying, i'm still here to encourage and give some references to a brave developer, so there's no point in totally stopping development. Also some time ago i received some mobile phones kindly donated (look <a href="http://www.kmobiletools.org/thanks">here</a>), so there's also this resource available.
It's open source: no secret development, no private servers.. everything is on the public kde svn, so why not keeping an eye on it?
So this is the point: i'll be back soon, but in the meantime, try thinking on helping this project too.
<!--break-->