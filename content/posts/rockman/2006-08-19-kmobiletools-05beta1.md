---
title:   "KMobileTools 0.5_beta1"
date:    2006-08-19
authors:
  - rockman
slug:    kmobiletools-05beta1
---
After a long wait and many code rewrites i finally release the first beta of the new KMobileTools.
Totally redesigned user interface, better codebase stability, full phonebook support, integration with KDE AddressBook and Kontact, better support for a lot of mobile phones.
I've prepared also a Live CD, LiveMobileTools, with everything (including kdebluetooth) integrated and ready-for-use.

<a href="http://www.kmobiletools.org/node/195">Here</a> you will find the original news from the KMobileTools homepage, the download page is <a href="http://www.kmobiletools.org/downloads">here</a>
Also i redesigned a lot our website, adding new pages. Hope you like it :P<!--break-->