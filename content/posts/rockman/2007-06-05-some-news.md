---
title:   "Some news..."
date:    2007-06-05
authors:
  - rockman
slug:    some-news
---
Well it's a long time since my last blog entry, but i was almost busy.
In my personal life, there were no big changes, so it's almost annoying blogging about it.
In the meantime KMobileTools had a great speedup, helped also by the joining of Matthias Lechner, who almost finished the gammu engine.
So <a href="http://www.kmobiletools.org/0.5.0-beta3">KMobileTools 0.5.0-beta3</a> was just released, bringing us very close to the final version. We also moved to extragear, since it's nearly stable code.
Also i've started the port to KDE4. Actually it's almost completed, but it still rely on QT3 Support.
But it's enough for the moment, so now KMobileTools is part of KDEPim, thanks also to <a href="https://blogs.kde.org/blog/2661">Pinotree</a> who helped a lot in the porting process, and to Allen Winter, the PIM coordinator :)
Development now was just a bit slow down, due also to exams incoming, but it's still active, i just committed some code to fix some bugs, and to improve compatibility with more phones.
