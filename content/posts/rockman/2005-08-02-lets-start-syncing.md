---
title:   "Let's start syncing"
date:    2005-08-02
authors:
  - rockman
slug:    lets-start-syncing
---
Holydays finally! And a lot more time to develop :)
And even if here it's too hot and humid (today: 40°), the last two days i spent a lot of time coding the two most requested features for kmobiletools: phonebook importing and exporting.
Of course, the first step was to add a new contact, with a proper dialog that let select you what numbers to add, where to save and so on..
After implementing the api to store a contact, it's easy to store N contacts.. and so an entire KAddressBook :-)
And the same thing applies to saving the phonebook to a file, or appending it to the KDE AddressBook, 'cause i already had the mobile addressees into a QPtrList.
The result is still a bit unmanaged, but already usefull: you can simply have a backup of your mobile phone on a file by pressing a key, and put it back again in the mobile; or you can merge the phonebook in KDE.
What we still need: something like metacontacts, so you can have multiple sources for each entry, and a way to handle duplicates. Also, i'd like to play a bit with OpenSync, since it maybe the real solution for integrating mobile phones data with kde.
And tomorrow, stop syncing, and let's go to the sea :)