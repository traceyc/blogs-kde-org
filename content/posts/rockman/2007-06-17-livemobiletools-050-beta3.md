---
title:   "LiveMobileTools 0.5.0-beta3"
date:    2007-06-17
authors:
  - rockman
slug:    livemobiletools-050-beta3
---
(<a href="http://rockman81.wordpress.com/2007/06/17/livemobiletools-050-beta3/">versione italiana</a>)
Finally after some troubles i could finish <a href="http://live.kmobiletools.org" target="_BLANK">LiveMobileTools 0.5.0-beta3</a>.
<div align="center">
<a href="http://www.kmobiletools.org/files/kmobiletools-beta3-1024.jpg"><img src="http://www.kmobiletools.org/files/kmobiletools-beta3-400.png"></a>
</div>
There are no major release hilights, just the ones in the <a href="http://www.kmobiletools.org/0.5.0-beta3">KMobileTools 0.5.0-beta3</a> release.. plus the addition of <a href="http://bluetooth.kmobiletools.org/1.0-beta3">KDEBluetooth 1.0-beta3</a> which should make the Live CD work better.

Enjoy it :)<!--break-->