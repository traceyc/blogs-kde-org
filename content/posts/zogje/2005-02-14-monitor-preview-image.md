---
title:   "Monitor preview image"
date:    2005-02-14
authors:
  - zogje
slug:    monitor-preview-image
---
 I think the new picture for the monitor used with the background preview doesn't entirely work as-is. This border looks weird. I tried to come up with a borderless version. This is a bit of a challenge because the code pretty much likes the actual preview to be square but the monitor really looks better with round edges.

I halfway managed to fix the problem for the background dialog, the panel dialog shouldn't be a problem either, but the monitor is also used for the screensaver configuration dialog and this is a problem because the screensavers themselves draw directly in the (square) window.

Here is a <a href="http://www.svn.net/bastian/kde/kcmbackground2.png">before</a> and <a href="http://www.svn.net/bastian/kde/kcmbackground1.png">after</a> picture of the background dialog.