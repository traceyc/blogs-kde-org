---
title:   "Portland covered by Gnome Journal"
date:    2006-04-23
authors:
  - zogje
slug:    portland-covered-gnome-journal
---
The <a href="http://www.gnomejournal.org/">Gnome Journal</a> has an <a href="http://www.gnomejournal.org/article/43/the-portland-project">interview</a> with me on <a href="http://portland.freedesktop.org">the Portland project</a> courtesy of Sri Ramakrishna. 