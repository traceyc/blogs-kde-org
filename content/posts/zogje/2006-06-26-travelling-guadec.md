---
title:   "Travelling to GUADEC"
date:    2006-06-26
authors:
  - zogje
slug:    travelling-guadec
---
Just wanted to say hi from Atlanta airport, where I'm waiting on my connecting flight to Barcelona, on my way to this years GUADEC.
Trying to get my outbound e-mail working but port 25 seems to be blocked (or my providers SMTP after POP3 is messed up). The tech support of AccessAnytime couldn't find "Atlanta" in their system and weren't much hep either... bunch of clueless f*cks.

I should also point out that the wireless on my Intel Centrino powered IBM T42 worked flawless in Linux. getting it to work from Windows was a lot harder (I hope that now the network knows my MAC it will work once I reboot)

There are a lot of delayed flights here in Atlanta due to bad weather. My plane seems to have arrived at the gate so I'm hopeful that it will depart on time. To those of you who will also bring GUADEC a visit, see you there. I will give a very nice talk about the Portland initiative on wednesday. There is quite some press interest in Portland as well.... exciting times.
<!--break-->