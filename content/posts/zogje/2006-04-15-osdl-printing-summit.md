---
title:   "OSDL Printing Summit"
date:    2006-04-15
authors:
  - zogje
slug:    osdl-printing-summit
---
What an exciting week! After many years I finally met Cristian in person at the OSDL Printing Summit that was held in Atlanta this week. The event was great fun and it was really nice to meet with Celeste, Ellen and Jan from OpenUsability.org as well. HP provided all the attendees with free lunches to go along with the free (as in speech, not as in lunch) HP printer drivers. They also provided  large bowls with really nice fruit. A shame that a lot of it was still left over at the end. Wish I had one of those bowls here at home. We had really nice facilities for the printing summit thanks to Ricoh/Lanier. Really nice to see so many hardware companies so supportive of Linux. The printing summit went better than I could have hoped for, everyone was really cooperative and I think a lot of common ground was found. I think there will be some very interesting printing related announcements the coming weeks. But now I need to catch up on some much needed sleep!<!--break-->
