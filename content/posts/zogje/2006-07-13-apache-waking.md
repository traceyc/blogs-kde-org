---
title:   "Apache Waking Up?"
date:    2006-07-13
authors:
  - zogje
slug:    apache-waking
---
Last month I <a href="http://blogs.kde.org/node/2081">complained</a> about Apache refusing to support OpenDocument. It seems there is progress in this area now that Eben Moglen published a
<a href="http://www.softwarefreedom.org/publications/OpenDocument.html">legal opinion</a> on behalf of the Free Software Foundation and the Apache Software Foundation giving OpenDocument a clean bill of legal health.

Unfortunately the related <a href="http://issues.apache.org/bugzilla/show_bug.cgi?id=37185">bug</a> <a href="http://issues.apache.org/bugzilla/show_bug.cgi?id=38301">reports</a> still don't seem to have received any attention.
<!--break-->