---
title:   "Pretending to be secure"
date:    2005-03-09
authors:
  - zogje
slug:    pretending-be-secure
---
<a href="http://www.zdnet.com.au/news/security/0,2000061744,39183862,00.htm">In France you can get prosecuted for reporting security problems.</a> I think this is a good approach to security. After all, recent studies funded by Microsoft have shown that Microsoft Windows is more secure than Linux because Linux had more security patches applied. It is obvious then that we can improve security on the Linux platform enormously by actively discouraging people to report security issues. This will result in less security patches and thus a more secure computing environment. It will also give us more time to work on fun stuff.
<p>
If we can convince more governments that reporting bugs is the moral equivalent of stocking WMDs in your basement it may very well be possible to outlaw bugreports altogether. This will mean that software will have no defects any more whatsoever, a huge improvement.
<p>
Cynical people may object to this approach, but they will be fighting an uphill battle. Experience in other areas has shown that this pretend-it-doesnt-exist approach can work very well: While European countries spend fortunes on CO2 reduction, the US government has addressed the issue much more effectively by pretending that global warming doesn't exist. A similar strategy seems to be developing for the US budget deficit.