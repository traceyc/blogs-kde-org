---
title:   "Portland print dialog explained"
date:    2006-06-02
authors:
  - zogje
slug:    portland-print-dialog-explained
---
No, the Portland Print Dialog isn't about <a href="http://www.j5live.com/?p=209">design by committee</a>. It's about letting the platform provide the print dialog (as opposed to the toolkit). If you run a GNOME desktop that will probably mean a Gtk Print dialog. If you run a KDE desktop that will probably a dialog based on KDEPrint. Incidentally, there already is a <a href="http://webcvs.freedesktop.org/*checkout*/portland/portland/xdg-utils/scripts/html/xdg-file-dialog.html">Portland file dialog</a>, and no it isn't designed by committee either (give it a try!).
<p>
To make these kind of dialogs really viable as a platform service there are still some barriers that need to climbed. In particular it will need to be possible to extend such a dialog in a toolkit-neutral and out-of-process way. That's currently not possible. I hope that we will be able to present a proof of concept of such an extensible dialog somewhen later this year. Until that happens it is indeed highly premature to talk about any of this in an LSB context.
<p>
Most of the other functionality currently provided by Portland's <a href="http://webcvs.freedesktop.org/portland/portland/xdg-utils/scripts/">xdg-utils</a> will be ready well in time for LSB 3.2 though. So there are enough other interesting Portland bits to talk about on <a href="http://freestandards.org/en/LSB_Summit#Day_two_.28June_2.29">LSB day 2</a>! 
<!--break-->