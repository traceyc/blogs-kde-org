---
title:   "DigiKam wins TUX award"
date:    2005-09-04
authors:
  - zogje
slug:    digikam-wins-tux-award
---
Congratulations to all the [http://www.digikam.org/Digikam-SPIP/|DigiKam] developers with winning the [http://www.tuxmagazine.com/node/1000150|TUX 2005 Readers’ Choice Award].
Being able to provide users with great software is what makes working on open source software for me one of the most rewarding activities. It's great when users acknowledge that they really like your software with an award like this. One of the nicest moments at aKademy this week was when a gentleman came up to David and me and thanked us for making KDE. He had been using KDE for a few  years and it had improved his computer live considerable.

KDE itself won the TUX award for best desktop environment, hardly surprising given [http://www.kde.org/awards/|KDE's track record], but heart warming nevertheless. It's not something we can take for granted though and there is still a lot in KDE that can be improved. I am excited about the increase in [http://dot.kde.org/1125757722/|usability driven design within KDE], I am looking forward to see the first results of it back in KDE 3.5! With these improvements I'm confident that KDE users will continue to rate KDE the best desktop in 2006 as well.<!--break-->