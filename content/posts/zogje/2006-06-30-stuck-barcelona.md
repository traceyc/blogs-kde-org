---
title:   "Stuck in Barcelona"
date:    2006-06-30
authors:
  - zogje
slug:    stuck-barcelona
---
Ola! So my return from GUADEC got shorted when the Seniorita at the Delta-air check-in counter looked at my passport and wondered why the laminated picture-ID part wasn't attached to the rest of the thing. I wondered along with her. Apparently dutch passports often break down like this if you tend to sit on them. The implication was that I wouldn't be able to get into the US with a passport like that. After rebooking my flight to Monday (cancelations willing I might even have a chance for a fight tomorrow) it was off to the dutch consulat here in Barcelona to get some replacement papers. Friendly people over there as well but still a bit of a scramble to get new passport-photos before they closed down for siesta. With no minutes to spare I made it back with new pictures of myself. It must be said that I look a bit overheated on them.

After spending the noon-hours (they have about 3 of them overhere) in the local starbucks, I could fetch my new temporary replacement passport. Now I hope that I will not get into trouble on re-entry for having my US work-visa in the old broken passport and not in the new one. We'll see how that is going to work out.

In the meantime I'm stuck in sunny Barcelona. Oh the agony ;-)
<!--break-->