---
title:   "Standards: FreeDesktop.org and beyond"
date:    2005-06-19
authors:
  - zogje
slug:    standards-freedesktoporg-and-beyond
---
I like Aaron’s suggestion to label more clearly the adoption status of the various drafts at FreeDesktop.org I also believe that at some point we must be able to say “this is something that is widely adopted and deserves to be a standard