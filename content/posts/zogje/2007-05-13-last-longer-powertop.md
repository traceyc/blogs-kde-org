---
title:   "Last longer with PowerTOP"
date:    2007-05-13
authors:
  - zogje
slug:    last-longer-powertop
---
Who doesn't want to enjoy the good things in life longer? I'm talking of course about the battery life in your laptop. Intel released <a href="http://www.linuxpowertop.org/powertop.php">PowerTOP</a> this week, a power monitoring tool for Linux. PowerTOP helps you identify which processes on your system keep your processor from going to deeper sleep states. Deeper sleep states consume less power and make your battery charge last longer. Visit <a href="http://www.linuxpowertop.org/index.php">www.linuxpowertop.org</a> and give it a try!<!--break--> 