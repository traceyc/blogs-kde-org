---
title:   "Microsoft gets it right..."
date:    2006-11-11
authors:
  - zogje
slug:    microsoft-gets-it-right
---
...finally. And no, I'm not talking about their dealings with Novell.
<p>
Last month Microsoft finally figured out how to work with open standards:
<p>
<pre>
Microsoft enhances Interoperability with Ecma Office Open XML Formats

(Oct 25, 2006) Microsoft is applying the Open Specification Promise (OSP) to Ecma Office Open XML to further enable the
implementation of these document formats, by anyone, forever. Microsoft already offered an irrevocable covenant not to
sue (CNS) to anyone wishing to implement the formats, and now implementers have the option to use the OSP or the CNS.
</pre>
See the <a href="http://www.microsoft.com/interop/osp/default.mspx">OSP FAQ</a> for details.
<p>
Critics are raving :-)
<pre>
“Red Hat believes that the text of the OSP gives sufficient flexibility to implement the listed specifications in software
licensed under free and open source licenses. We commend Microsoft’s efforts to reach out to representatives from the open
source community and solicit their feedback on this text, and Microsoft's willingness to make modifications in response to
our comments.