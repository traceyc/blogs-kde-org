---
title:   "language fetishists: try Eclipse"
date:    2005-08-19
authors:
  - zogje
slug:    language-fetishists-try-eclipse
---
Maybe you still think of it as an Java IDE, but with this weeks release of [http://www.eclipse.org/cdt/|Eclipse CDT 3.0], Eclipse has become a much more interesting C/C++ IDE as well. Eclipse has a powerful plugin architecture that allows all kinds of powerful tools, from content assistence, [http://subclipse.tigris.org/|SVN integration] to refactoring assistants. See what's [http://dev.eclipse.org/viewcvs/index.cgi/~checkout~/cdt-home/news/cdt30-whats-new/CDT-3.0-News.htm?cvsroot=Tools_Project|hot] for yourself.

Alexander Dymo has been doing some great work on [http://kde-eclipse.pwsp.net/|Eclipse-KDE integration] as part of his Google Summer of Code project, go give it a try!<!--break-->