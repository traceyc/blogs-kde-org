---
title:   "Open Source Graphics Drivers"
date:    2006-08-09
authors:
  - zogje
slug:    open-source-graphics-drivers
---
Those of you who are tired of battling with binary graphic driver blobs will be pleased to learn about Keith Packard's <a href="http://lists.freedesktop.org/archives/xorg/2006-August/017404.html">announcement</a> today. 

The Intel® 965 Express Chipset represents the first product family that implements fourth generation Intel graphics architecture. Designed to support advanced rendering features in modern graphics APIs, this chipset family includes support for programmable vertex, geometry, and fragment shaders. By open sourcing the drivers for this new technology, Intel enables the open source community to experiment, develop, and contribute to the continuing advancement of open source 3D graphics.

Following the release of this driver, future work will continue in the public X.org and Mesa project source code repositories. The project Web site, http://IntelLinuxGraphics.org/ , will serve as the central site for users of Intel graphics hardware in open source operating systems.

Update: More coverage at <a href="http://news.com.com/Intel+aims+for+open-source+graphics+advantage/2100-7344_3-6103941.html?tag=nefd.lede">CNET News.com</a>
<!--break-->