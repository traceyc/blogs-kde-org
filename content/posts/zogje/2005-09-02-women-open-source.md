---
title:   "Women in Open Source"
date:    2005-09-02
authors:
  - zogje
slug:    women-open-source
---
Danese Cooper [http://danesecooper.blogs.com/divablog/2005/08/catchuposcon_li.html|asks] where all the women are in Open Source. I don't know about all of them, but several of them can currently be found in Malaga at the KDE conference. Among them is Lauri who received one of the first ever KDE aKademy awards in recognition of all the hard work she has put in KDE. Congratulations Lauri, it's well deserved!

KDE has also the [http://women.kde.org/|KDE-Women] website for those that seek the company of some fellow women within KDE.
<!--break-->