---
title:   "KDE, Ruby and everything"
date:    2003-08-12
authors:
  - berkus
slug:    kde-ruby-and-everything
---
Well, my home KDE went mad and started crashing and hanging everywhere, the last and the most disappointing was menu selection hanging the machine tightly. Rebuilt whole system from scratch with much-much lighter CFLAGS (i.e. used -mcpu=athlon-xp -O1 instead of -march=athlon-xp -O3 |:=))
Now it works like a charm and not a bit slower than before.
While KDE was out of business my g\f found a home accounting program for win32 and guess what, this is the best home accounting software I've ever seen before. It includes a handy entry tool and useful graphs that display your income/expense as pie charts with separation into categories (screenshot pending). Seems like I have something to port to KDE.
My regular job takes all of my time leaving no gaps to play KDE anymore. The rest of the time I spend with my 3-month-old daughter. So I don't expect too much work on KDE side, but seems like I gonna install internet connection in September so I could have some immediate access to docu and collective developers mind from home, this will help me with development alot |:=)
On my regular job I'm playing with Ruby, and I love the language more and more every day. This is something I can call a language of my dream. Its elegant as Lua was elegant to me some time before, including easy bindings to C-like languages. Its handy as Perl is handy to script writers - actually, I transferred a bunch of ugly Perl scripts into a single Ruby script in one day and its much-much better and more flexible than it was before |:=).
Unfortunately, I was unable to test qtRuby bindings, so mixing Ruby with KDE is left for some time later.
Btw, found an awesome alternative library of Ruby software: http://freepan.org/ruby/by-cat/ (alternative wrt RAA)
Oh yes, reading a book in Xtreme Programming now and I've got an idea for a decentralized helper tool. Tool that will track the development in a server-less manner within a group of developers. WASTE looks like a good pretendent for a communication framework, since it is group-oriented and supports encrypted communication. Anyone vote for a better toolkit?

ps/ Nullsoft has terminated their license for WASTE ( http://www.nullsoft.com/free/waste/ ). Pity. But idea is still there and iirc I still have their demo sources somewhere |:=)