---
title:   "Configuration"
date:    2004-07-09
authors:
  - zack rusin
slug:    configuration
---
I've been away for a few days with some of my friends. So here's a few pictures of your truly away from computer. I'm actually wearing glasses on those (gray shirt, blue jeans).
Good shot of my crotch area : <a href="http://vortex.bd.psu.edu/~mkr137/stawy2004/100_FUJI/DSCF1669.JPG">here</a>, black and white photo : <a href="http://vortex.bd.psu.edu/~mkr137/stawy2004/100_FUJI/DSCF1680.JPG">here</a> .

Last Friday I decided that we need to have a configuration editor. I was fighting with an idea of finally finishing it for the longest time. Now a week later, we have such a tool. During the weekend I've spent a few hours rewriting KConfigEditor. Now I'm almost happy with. It's the only application that can be used to edit both GNOME and KDE configuration. Tomorrow I'm planning to add a view of changes and on Sunday a backend to create restoration and propagation scripts. After changing your configuration you'll be able to create a restoration script which will return your configuration to the previous form. Propagation script on the other hand will change the configuration to the state which it was after the changes (admins will be able to use it to propagate the changes on workstation of their users). A screenshot showing KConfigEditor edditing GNOME entries is here : <a href="http://extragear.kde.org/apps/kconfigeditor/kconfigeditor3.png">here</a>. So yeah, a nice tool to have. 