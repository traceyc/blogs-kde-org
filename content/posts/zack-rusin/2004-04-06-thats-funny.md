---
title:   "That's funny"
date:    2004-04-06
authors:
  - zack rusin
slug:    thats-funny
---
Come on people, the stuff that's going on lately is just plain funny:
<ul>
  <li>osnews posts the april fool's joke from dot.kde.org while adding "we had some people saying that this is April's fools news, but these claims are false. OSNews does not follow this tradition in general.". That made my day. Brilliant deduction, excellent coverage and top-quality flamewar as a result of a joke. That's funny.</li>
  <li>Havoc's and Miguel's "open discussion". What better way to solve technical problems than to bring them over to the tech-savvy people at slashdot? Don't get me wrong, it was interesting but the choice of the medium was just plain funny. On the other hand at least every second person posting on slashdot knows someone who knows someone who saw someone coding and that's exactly the type of expertise everyone is missing when making important decisions. Did I miss a memo where blogs were made to be decisions making places? I'll be putting some web-services classes in kdelibs soon. Got a problem with that? Post it in your blog so that we can discuss it. Come on, that's funny.</li>
  <li>Novell uses Qt. I don't think I've ever seen so many people so close to heart-attack. Things like that make bingo parties on Brooklyn seem innocent. One silly rumor and people went nuts. I mean, next time do what I do, install one of those wheels that you see in cages with hamsters where they run all the time. Oversized one, of course, and whenever you get all worked up, just start running in it. And bam, with two more rumors like that we solve: bad physical shape in the community and energy crisis. Hmm, that's such a brilliant idea that I think I might get a patent on that. With the stuff patent office is doing lately having "developers run in oversized hamster-wheels" a patented idea should be a no-brainer.</li>
</ul>

In other news, Aaron is coming over next week and we're going to have a hackfest at geiseri's place. I'm saying it in my blog because Aaron doesn't know that he's coming yet so I figured I might notify him. Especially that considering the distance he has to take his rollerblades out right now if he wants to get here by next Saturday.