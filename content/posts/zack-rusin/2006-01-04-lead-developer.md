---
title:   "\"Lead developer\""
date:    2006-01-04
authors:
  - zack rusin
slug:    lead-developer
---
My last blog entry has been caught by a few sites which described me as a "lead KDE developer". I felt the need to clarify this because I don't think it's fair to all other KDE developers. 

I'm not a lead KDE developer. KDE doesn't have a lead developer. We're all equals. In fact that's one of the most beautiful things about KDE! 

We (KDE) are making it so easy to join the project and everything is structured around the concept of equality. Maintainers have of course final saying in the development of individual parts but KDE itself is a collective of developers from around the world. All of whom have equal saying. The KDE "lead developer" is the community if you will.  

And yes, very soon we will be introducing a <a href="http://ev.kde.org/corporate/twg_charter.php">Technical Working Group</a> . It was just voted in by the e.V. Now we're going to have another vote to select 7 members of this group. To quote from our definition: " The purpose of the Technical Working Group (TWG) is to define and execute the official software releases of KDE. It should also support and guide the processes of taking technical decisions within the KDE project. It has to make sure that the open source development process is kept intact and the KDE community is involved in all important decisions."

I must say that I'm extremely happy to finally have the foundation for this group. The highly distributed nature of KDE is both a blessing and curse. It's a dreamland for developers already in, but makes it a little hard to find yourself when joining and makes it even harder for outsiders to get answers to even simple question of a technical nature (as many developers usually bring forward many different opinions and answers). Now if people will want to ask those question, they'll be able to get a definite answer from the KDE TWG. 
 
If you think about it, it's really beautiful how code affects the evolution of the social structure around it and how the social structure around code affects its development. 

<!--break-->