---
title:   "Image manipulation / LinuxWorld"
date:    2004-01-07
authors:
  - zack rusin
slug:    image-manipulation-linuxworld
---
I was talking to Rich today and he pointed me to a wonderful paper : http://www-sop.inria.fr/odyssee/research/tschumperle-deriche:02d/appliu/index.html . Please look at the image restoration one can achieve with this baby. The "Image Inpainting" examples are breathtaking! The whole thing is on my todo for KDE 3.3.
I also got reports from people that some effects from KImageEffect simply don't work, or even worse are crashing. As it seems a lot of them hasn't been tested. 

Also remember that a rather big group of KDE developers is coming to New York for the LinuxWorld Expo. George did a great job organizing this one. I haven't seen him since Nove Hrady and although we talk pretty much every day I can't wait to see his Canadian butt (preferably covered). Ian, in between doing awesome things with kjsembed and switching to Debian, is getting our banner ready. This expo is going to be a lot of fun. 
