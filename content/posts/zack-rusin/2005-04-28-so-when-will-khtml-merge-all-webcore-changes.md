---
title:   "So, when will KHTML merge all the WebCore changes?"
date:    2005-04-28
authors:
  - zack rusin
slug:    so-when-will-khtml-merge-all-webcore-changes
---
You can't even imagine how I hate that question. The truth is "most probably never". I just read the article on /. about Safari supporting the "all crack Acid2" test and people raving how great it is for KHTML. The truth is that  KHTML will probably never get those patches. What's most probably going to happen is that one of us will simply reimplement it from scratch (and at the moment the reality is that if it's not going to be Allan or Germain it's not going to happen). 

Code in Safari is hugely inconsistent and changes are always interdependent. There's basically no way of merging in one change without bringing a whole bunch of others in. And you know what? Don't even tell me about merging stuff like render_canvasimage.[h,cpp]. It outright uses OS X api's. We'll never be able to merge that in - someone will have to implement it. And what's going to happen when someone does? Some jackass on /. or some other equally stupid site will be praising Apple. 

In the past when someone spent long hours implementing something in KHTML, they at least got a "thank you" from people using Konqueror. Now it's "well finally! It was working in Safari. khtml developers are lazy". Where's the fun in that?

Do you have any idea how hard it is to be merging between two totally different trees when one of them doesn't have any history? That's the situation KDE is in. We created the khtml-cvs list for Apple, they got CVS accounts for KDE CVS. What did we get? We get periodical code bombs in the form of them releasing WebCore. Many of us wanted to even sign NDA's with Apple to at least get access to the history of their internal vcs and be able to be merging the changes incrementally, the way they can right now. Nothing came out of it. They do the very, very minimum required by LGPL.  

And you know what? That's their right. They made a conscious decision about not working with KDE developers. All I'm asking for is that all the clueless people stop talking about the cooperation between Safari/Konqueror developers and how great it is. There's absolutely nothing great about it. In fact "it" doesn't exist. Maybe for Apple - at the very least for their marketing people. Clear?
<!--break-->