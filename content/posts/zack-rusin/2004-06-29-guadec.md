---
title:   "GUADEC"
date:    2004-06-29
authors:
  - zack rusin
slug:    guadec
---
Although GUADEC ends tomorrow, I and Waldo are unfortunately leaving tonight. It was a fun time and I'd like thank GNOME's for the last few days. Thank you!

Most of all I'd like to sincerely thank Nat Friedman and David Neary. I arrived a day early so my hotel room wasn't ready and Nat basically adopted me ;) for the night and gave me a place to sleep at his room, while Dave took care of my bags. So, yeah, I really appreciate it guys!

I was very happy to be able to sit down with Dave Camp, Alexander Larsson, Jonathan Blandford and Waldo to work some of the MIME stuff out.

Accessability talks were really fascinating. I was happy that Harald Fernengel was there since he's the brave soul who will be implementing the most crucial parts of it on our side.

Today I've met Michael Meeks. Unfortunately I didn't have time to really talk to him, since it was right before my presentation. Shame, but it's not like we won't have any more chances to talk.

It was nice seeing Havoc again. I'm disappointed I didn't have too much time to talk to Owen.

In case you care at all, my slides are available at http://www.automatix.de/~zack/guadec.sxi and the video feed with my talk is at http://stream1.hia.no/ogg/dump/room1/ogg-theora-vorbis-high.2004-06-29-13:54:22.ogg but you need to have the Ogg/Theora codec to actually watch it.

Oh, and I think I didn't mention that Harry and Brad from the TT fame are staying for another day, it's just Waldo and I who are leaving :)