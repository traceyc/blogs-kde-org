---
title:   "Santa Clara"
date:    2006-02-08
authors:
  - zack rusin
slug:    santa-clara
---
I'm in Santa Clara for the XDevConf. Flying from cold and rainy Philly to sunny Santa Clara is quite a change. I wish I brought my skateboard.  

I couldn't get WIFI working in the hotel which was a little disappointing and I forgot to bring an ethernet cable. I went down to the reception and noticed that they charge $5 for renting a cable. I decided that I need to get something good to drink to forget that I'm about to pay $5 for renting something that I could buy for about $1. There's a neat juice store across the street so I went there. I started a conversation about the naming scheme they had for the juices (which was just sad) with the girl working behind the counter and I got my juice for free ("berry blitz with immunity boost" was the name). Equipped with that I went back to the hotel where I started talking to the girl behind the counter and got the ethernet cable for free. Can't complain. Next time I'll be aiming for not having to pay for the hotel ;)

Around 5pm <a href="http://airlied.livejournal.com/">Dave</a> arrived in the hotel and we went out. This was the first time we've met. He's an awesome guy so that was a lot of fun. Around 8 <a href="http://ajaxxx.livejournal.com/">Adam</a> arrived with David Reveman, Kristian Høgsberg  and Søren Sandmann and we went to an indian place to grab something to eat (well, "Dave and I" [insiders joke for a friend of mine] already ate so we were just serving as mental support - we all know how shy RedHat/Novell guys can get). On a sidenote given how few X/graphics hackers Open Source has, we really shouldn't be hanging out together ;) 

What really makes events like this great is seeing all those people face to face and getting to hang out together for a bit. Presentations are just a side-effect. On those smaller conferences everyone knows what everyone else is doing. So presenting doesn't make all that much sense - having discussions about problems and solving them together is what really matters. Officially the conference is starting tomorrow and I'm sure it's going to be a lot of fun. 

<!--break-->