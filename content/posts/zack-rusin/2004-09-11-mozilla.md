---
title:   "Mozilla"
date:    2004-09-11
authors:
  - zack rusin
slug:    mozilla
---
I think by now almost everyone knows that together with Lars we started working on the Mozilla code. 
<br/>
Essentially the story is that on Sunday night on a winefest in Ludwigsburg we started talking about having Mozilla run natively on KDE. To make the long story short, two days later I and Lars had an example compiling and four days later we had it rendering pages as a QWidget. It was fun. Real fun. This is not some experimental hack. We're looking at the whole thing rendering natively in Qt. This is the full-monty.
<br>
Since the announcement I'm expecting a lot of very weird comments, questions and flames. I'll try to answer some of the more common questions I've seen already here. So in no particular order:

<ol>
<li>Will KHTML support be dropped? Because the port has been done by Lars and me people seem to think it means we're dropping KHTML support. This is absolutely not true. As of the moment KDE policy towards KHTML stands as it always was. Nothing changes.</li>
<li>Will distributions start using it over KHTML? That's very probable. The bottom line is that while Mozilla (Gecko in particular) is supported by many enterprise focused companies, KHTML is not. So for distribution whose clients depend on those products it makes natural sense to set the Gecko renderer as default in Konqueror</li>
<li>Does it mean Firefox will run natively on KDE? Yes, that's essentially exactly what it means. We haven't only ported the Gecko but we wanted to make it as complete as possible. I do want to make Firefox a great browser for KDE users. In the coming weeks I'll be integrating KIO, KWallet and KCookieJar so I'm hoping we'll see more great things soon.</li>
<li>Will we be renaming Gecko? I don't even know how to answer to that. Gecko is Gecko. Does the GTK+ embedding engine in Mozilla CVS rename Gecko? Besides like I said our plans go beyond just Gecko.</li>
<li>Will we cooperate with Mozilla developers? Yes, by all means. I'd like to already sincerely thank a lot of Mozilla developers. Everyone we talked to has been very supportive and that's very exciting. I especially would like to thank Scott Collins. From my part you can be sure that the Qt port is not the only thing I'll be looking at in the Mozilla CVS...</li>
<li>Do we use KJS within Gecko? No</li>
<li>Will users see a difference when using Gecko KPart vs KHTML KPart in Konqueror? The goal is to make sure they don't. We want to have even the interface to the KParts almost the same. We want to make sure that users don't see a difference when switching between the engines.</li>
</ol>

I've already seen a lot of flames about which engine is better. So, please, just try to take it as it is: another option. If you don't like it you don't have to be using it. If you prefer Konqueror with KHTML, keep using it. If you like Firefox but always wanted to have it integrated with KDE, this will be your choice. If you like Konqueror but don't like KHTML and wanted to have Gecko KPart, this will also be for you. It doesn't mean that any particular choice is better or worse. People browse different pages, people use browsers for different purposes, whatever choice is your favorite one doesn't make it automatically the best possible choice, it only makes it your favorite. We're making Mozilla platform a fully fledged citizen of the KDE desktop. We're doing it for users, distributions and developers who simply like Mozilla, if you're not one of them, you have to remember that I won't be forcing you to be using it.
<br/><br>
Personally I'm very excited to be able to work with Mozilla developers. Unfortunately I had to move two days ago and I don't have internet connection at home right now which will slow me down quite a bit. I hope my connection will be operational on Monday or latest Tuesday and I'll be able to get at it full speed. Quietly I hope that with Scott's help (again big thank you) I'll be able to start opening bugzilla.mozilla.org bugs with the patches today. Lars will be away for the next four weeks having a lot of fun outside the computer world (the man is getting married! :) ) so if there's anything else that's unclear about the situation I'll do my best to clarify it.
<!--break-->