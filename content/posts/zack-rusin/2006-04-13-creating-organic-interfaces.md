---
title:   "Creating organic interfaces"
date:    2006-04-13
authors:
  - zack rusin
slug:    creating-organic-interfaces
---
V for Vendetta is so good. Aaron and I went to see it yesterday. On the way there we went to a really great (and cheap) vegan Chinese place and stopped by an arcade (remember those?). Coming back we got asked for money under a bridge. The irony of having people ask for money from two trolls under a bridge runs deep.

We keep working on infrastructure for Plasma and it's really neat to see it progressing every day.  At the moment a lot of people is missing QCanvas from Qt 4. Fortunately Qt 4.2 will have a fantastic replacement in the form of a QGraphicsView. <a href="http://blogs.qtdeveloper.net/">Andreas</a> did an amazing job with getting it in its current shape. QGraphicsView is actually a lot, lot better at being a canvas than QCanvas ever could. Today I added support for playing movies in the graphics view. Movies are just items on a canvas so you can compose on top of them, transform them and so on... A screenshot from a sample application is here: <img src="http://ktown.kde.org/~zrusin/dev/qgraphicsview_video.png"class="showonplanet"/>. You can see two semi transparent, sheared movies playing on a QGraphicsView. 

Now a little about how SVG, QGraphicsView and Plasma will be tied together. I'm working on incorporating SVG model into a QGraphicsView scene. That means that SVG will be parsed directly into a canvas scene. In turn this means that, since all canvas elements are selectable/movable/transformable, we get a SVG editor. I'm hoping to add scripting, video and audio support to the next version of QtSvg. Just like with QtDesigner people who really don't know how to code can create GUI's for their apps for Plasma we'll have the ability to create organic interfaces in the same way. We'll use SVG to actually store the data and just like with ui files, you'll be able to compile them to native code. Compiling we'll mean creating a C++/JS/Ruby/whatever code which creates the QGraphicsView scene for you and hooks it all up. 

Hopefully all this will fully replace all the custom HTML based widgets that we currently hack together. We'll have an all gui editor for those which should immensely speed up the process. So basically we're looking at a Flash like app, in the sense that you'll be able to do rapid development of custom vector graphics based interfaces with full multimedia support. Now that is cool.
<!--break-->