---
title:   "Summer coding"
date:    2005-06-03
authors:
  - zack rusin
slug:    summer-coding
---
We've put together some ideas for Google's <a href="http://developer.kde.org/joining/googlecodeofsummer.html">Summer of Code</a>. I think there's quite a few interesting entries there and we tried to make sure that there is a nice set of initial ideas. Personally I'd love to see people coming up with their own ideas (which is why I particularaly like the "framework/application addition" entries). If you never ever coded with KDE, don't worry, we'll assign mentors to you who will help you through out your project. 

I'll be attending the X.Org Developers Meeting on LinuxTag (Karlsruhe, Germany) in two weeks. Not surprisingly I'll be talking about the new acceleration architecture for X.Org that I've been working on. Both the driver model and the core design. A decent acceleration architecture for 2D is an interesting problem. You really need a concise set of goals to implement one correctly. You'd like to get the most out of the hardware while making the driver model as simple as possible. Implementing XAA was rather painful so most drivers were using code they developed for XAA in a lot of places, hence replacing acceleration architecture means that I have to remove a lot of that code. On one hand it makes the drivers somewhat nicer, but it's a boring work. I'm also contemplating what to do with DGA. Altough technically most primitives that the drivers reuse from XAA in DGA revolve around solid fills and screen to screen copying (which of course I do have, since they are rather essential) it's just not the most elegant solution to the problem and while changing a lot in the server maybe it makes sense to replace it with something nicer.

Once I'll be done with the acceleration architecture I'll have to shift my gears to Xgl and work on DRI a bit. It's a whole different animal with a whole different set of problems. At some point I'd like to start working on XVideo. Xv doesn't mix too well with a system that's layered on top of OpenGL, especially if you think about composition managers. Video overlays can not be used, since other things may need to be composited on top of the YUV data. It's a shame because back-end scalers are usually a quite powerful piece of hardware. We'll have to use front-end scalers or texture memory units to get the correct results. An interesting solution could be adding hooks to whatever extension will be the next version of Xv that say "hey switch modes,i'm going fullscreen,nothing will be composited on top of me" and the extension can fallback in drivers to use back-end scalers. Maybe I'm worrying preemptively though. All modern cards support YUV data as textures so it shouldn't be that bad either way.

On the more high level front, Aaron is already working hard on our next generation panel, Plasma 
<img src="http://aseigo.bddf.ca/dms/32/207_logotext.png"class="showonplanet"/>
How great of a logo is that? I'll be doing the composition code for my baby (where "my baby" in this context is defined as "Aaron") to make sure that the amount of eye-candy and coolness factor in Plasma blows your socks off. 

Oh, and a few days ago, they've been taking our pictures for Trolltech business cards so I "stole" a few of them (unfortunately they have the logo of the company that was taking them) because I like black&white pictures. 
<img src="http://ktown.kde.org/~zrusin/zack/troll_V5U0992.jpg" class="showonplanet" /><img src="http://ktown.kde.org/~zrusin/zack/troll_V5U1004.jpg" class="showonplanet" /><img src="http://ktown.kde.org/~zrusin/zack/troll_V5U1037.jpg" class="showonplanet" />
<!--break-->