---
title:   "Getting Qt Mozilla"
date:    2004-10-11
authors:
  - zack rusin
slug:    getting-qt-mozilla
---
<p>I just committed the Qt Mozilla code. To start testing it you need to: checkout mozilla, configure with --enable-default-toolkit=qt, make and that's it. Please do not, I repeat do not send me wishes or bug reports just yet. I'll simply ignore it unless there's a patch attached to it. There are two basic issues which have to be fixed before I'll be taking any requests (1) the toolbar is waaay to tall, 2) updates aren't sometimes propagated correctly). </p>

<p>Oh, and the easiest way to test it is not through ./mozilla, but by "cd embedding/browser/qt; make; cd tests; make; cd ../../../../dist/bin; ./TestQGeckoEmbed". I haven't yet committed the KPart so this will have to do.</p>

<p>Now that this code is in CVS it's going to be a lot easier for me and others to be working on it. I'm changing apartments again this Friday so I'm not sure how responsive I'll be during the weekend.</p>

<!--break-->