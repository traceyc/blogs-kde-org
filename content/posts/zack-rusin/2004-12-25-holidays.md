---
title:   "Holidays"
date:    2004-12-25
authors:
  - zack rusin
slug:    holidays
---
<p>The one thing I love about <b>any</b> holidays is the spare time. As some of you may know last week I started the kde-graphics-devel list. It's a huge relief for me because I felt bad about not having enough time to answer all the emails I was getting about image effects, new widgets and just a lot of KDE graphics related coding.</p>

<p>During the last two months I've been spending a lot more of my time in a hospital nearby (I volunteer there, talking, hanging out and helping out patients however I can). Unfortunately the time I'm spending there is a time taken from my KDE and Open Source programming time.</p>

<p>On the bright side I have a new project (yes, another one) which I'm fairly excited about. And that says a lot because I haven't been excited about anything I wrote in the last two years or so. I'm hoping to release a preview of Impresario around New Years (it requires Qt4 so you may think about compiling it before then ;) ).</p>

<p>The first child of kde-graphics-devel, the animated tooltips, just got into Kicker. People seem to like it a lot which is nice. Personally, I'd still like to have the icon in the tooltip animated somehow, but 2D transformations are too limited to create something really cool. The animations I wrote for it in the first version (all of which use some kind of a 2D transformation) are great if you want to piss someone off, but otherwise fail miserably. I have been testing this theory at work with Ian. I was never the biggest fan of the zooming buttons for a very simple reason: the idea was taken from another project and I think KDE can do better. A lot better. Which, I guess, is where the Impresario comes in ;)</p>
<!--break-->