---
title:   "Simply cool"
date:    2005-10-22
authors:
  - zack rusin
slug:    simply-cool
---
I haven't been blogging for a while. Mainly because I simply haven't felt like it and I've been going through some things lately. But I promised that I'm going to show some movies of things I've been doing. There's really no good way of showing OpenGL effects, vns2swf just doesn't handle it. So those will come later. 

Today we're going to talk a little bit about what we can do with ARGB visuals for Plasma. The problem was that we wanted to have custom shaped windows, whose shapes can be animating and anti-aliased. Something XShape can't handle. Therefore we made sure Qt works correctly with ARGB visuals. Now, the big question is "what are you talking about and what can I do with it?". So lets look at an example that I just recorded using vnc2swf (physos did the nice html wrapping of the files so if you post this blog somewhere have mercy on his server and try to mirror the files somewhere):
http://physos.net/zacks_videos/zacks_video_1.html (swf file at http://ktown.kde.org/~zrusin/cool1.swf )
As you can see we have a window shaped like gear, it holds normal Qt widgets (button, checkbox and labels in this case). Also note that after pressing the button we get a "notification" icon beyond our window. All of this is builtin in Qt. How do you get it? Well, you just fill your toplevel with a transparent brush and set its content propagation. Impressed?
Then lets do something cooler. Lets create a SVG file, lets create a widget and set its shape with the result of our SVG rendering (Qt 4.1 has native support for SVG. If I'm going to get more motivated I'm going to blog a little bit more about the funky things I did to QtSvg so I'm not going to go too deeply into SVG support). 
http://physos.net/zacks_videos/zacks_video_2.html (swf file at http://ktown.kde.org/~zrusin/cool2.swf )
The coliber is a SVG file, on top of it we slap a QCheckBox and connect it's toggled signal to a label. 
I've been talking a lot about future of graphics lately and one thing I always say is that all I'm doing is building a framework on top of which others can implement anything they want. We spent too much time hacking around framework to do cool stuff. Now our framework (X11 and Qt) support it all. Things like dashboard widgets would be now trivial to implement. In fact we can do it a lot better : connect SVG files with ECMA script and suddenly you get a lot more powerful framework. Isn't KDE 4 getting you all excited? ;)
<!--break-->