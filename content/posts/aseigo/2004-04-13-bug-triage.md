---
title:   "Bug Triage"
date:    2004-04-13
authors:
  - aseigo
slug:    bug-triage
---
the bug db is getting out of hand. we have millions of LOC, hundreds of apps, and goddess knows how many users at this point. we need to have a triage team. KDE Quality seems the right place to cherry pick people for it, but i think some education may be in order first. when i get back from my weekend sorte i'll try and write my long-awaited follow up to the WhatsThis HOWTO on Bug triage. but what i'm really wondering is this: would people show up for an online Bug Triaging workship ... say ... on irc, #kde-bugtriage and walk a bunch of people through the process, answering questions, offering reassurance and providing praise (or blame ;) as we go ... hum.