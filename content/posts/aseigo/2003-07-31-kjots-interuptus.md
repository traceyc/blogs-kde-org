---
title:   "KJots, interuptus"
date:    2003-07-31
authors:
  - aseigo
slug:    kjots-interuptus
---
Worked a bit more on KJots last night. I'm nearly finished with everything I'm going to do with it for 3.2, then I can move on to finishing up work on KsCD, then I can move on to TOM and KMail headers, attachments and folder properties. Insert regular bug fixing obsession sprees and the unpredictable random distractions in there and that's probably my KDE life from here to 3.2.

Today I won't be so lucky as to make much, if any, progress on that list. Good thing I was up to 3:00am last night, then. ;-)
<!--break-->
Yesterday I got an email from Eugenia of osnews.com fame. It was basically a feature request for KCalc (and GNOME's calculator) from her "OS engineer, used to work on BeOS" husband: copy and pasting of formulas into the calculator from external sources. She CC'd no less than 7 people in KDE and GNOME on that email. I politely responded with a thank you for taking the time to send the email, and that bugs.kde.org is the best place to register such feature requests so they don't get lost and can be triaged by the maintainer, blah blah blah... you know the drill. She replies (CC'ing all four KDE devs from her original CC list!) saying that she can't be bothered to register to submit a wishlist on bugs.kde.org. Apparently she set up accounts on three other bugzilla systems that day to report bugs and had had enough. Oh well.

At the same time, I can empathize with her. We really SHOULD have a common account DB for OSS bugzilla systems to use. The best would likely be a replicated DB that is kept locally on each Bugzilla system, though that would require more admin work and some DB table restructuring which probably means Bugzilla hacking too. So while we SHOULD, I doubt we WILL any time soon.

Oh, and I don't have email today! I wake up to discover that my mail server is not to be found on the 'Net. I rush down to where it is (thankfully only 15 minutes away) to find the ethernet cable dangling against the wall not even plugged into the server! Somone had unplugged it, no WONDER I couldn't reach it. Gah! So I plug it back in, but still no-go. 

Apparently the people who are hosting the DNS and manage that block of IPs did a whole bunch of network reorg the night before. Our mail server was using two too many IPs, thanks to our wunderkind Debian admin Cade (thankfully his security expertise is orders of magnitude better than his admin skills ;)). Cade happens to be in Greece at the moment with his girlfriend. So, lacking a way to get a hold of him, and not realizing that I had root access to the box, they just unplug it and reassign the IPs and just like that Huzzah! no email for Aaron!

I get on the phone with them, fix things up, and now I just have to wait for DNS propagation. GAAAAAAAAAH!

Oh, and did I mention I have a section of a project I need to finish up for work by tomorrow? Losing half the day to network stupdity helps, as does the football (soccer for you N. Americans ;)) game I have tonight.

KDE.... take me away!