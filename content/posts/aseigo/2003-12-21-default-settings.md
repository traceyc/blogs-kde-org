---
title:   "default settings"
date:    2003-12-21
authors:
  - aseigo
slug:    default-settings
---
in prep for 3.2, i've been looking at some of the default settings and fixing up some of the errant ones... like no desktop text shadows, or soft line wrap in kedit... lots of little fiddly stuff.. and working on fixes for editting bookmarks with the context menu (the dialog is a little lackluster) and for ensuring that important servicemenus like the mount options are in the top level and not buried in the Action submenu. 
<!--break-->

and since you clicked the "read more" link, here's an extra treat. or maybe it's an extra punishment to teach you not to click on the "read more" link. *shrug* in any case, it's Mahlah and myself at the company christmas party last weekend out in Banff. the Banff Springs Hotell is amazing. great free food and an open bar helped too. ;-P

[image:280]