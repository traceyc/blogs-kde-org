---
title:   "Pressie 3: KDE devel tools"
date:    2004-04-19
authors:
  - aseigo
slug:    pressie-3-kde-devel-tools
---
somewhere between 15 and 20 people attended the last presentation which wasn't bad given the overly technical nature of the presentation and the fact that it was the last of the day. i was tired and hungry (the food vendors closed down before i could answer all the after-presentation questions people had for me after the second presentation! bastards!) but it went well. i decided to take an informal approach on this one: no slides, just an interactive discussion. i covered the basics of the signal/slot architecture, XMLUI, KConfigXT, what a basic KDE app looks like and the various libraries one can use (kio, kdeui, etc). i then did a quick tour of KDevelop, did some basic form design in Designer and provided an overview the constellation of the many, many KDE devel tools. talked about developer.kde.org and how to get more info on things like the KDE build system. used KDevelop/kapptemplate to generate a C++ and a Python KDE app. people were duly impressed ;-)