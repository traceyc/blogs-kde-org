---
title:   "Bruce Peren's \"response\""
date:    2003-12-16
authors:
  - aseigo
slug:    bruce-perens-response
---
<p>Here are the points in the order they were presented in <a href="http://www.newsforge.com/software/03/12/16/0029234.shtml">Bruce Peren's reply</a> to <a href="http://desktop.kdenews.org/strategy.html">the KDE in User Linux proposal</a>, along with my thoughts on them.
<!--break-->
<b>It's All Emotional</b>

<p>Bruce first tries to explain it away by saying, "it's simply unbearable for their personal GUI not to be the one chosen for our project", after making references to emotional investments. He's right in that people do care for and about that which they create. He's wrong in that the &quot;emotional investment&quot; is why many of us had issues with the choice to ban KDE/Qt from his project.</p>

<p>The real reasons are that Bruce's declaration ignores market realities. User Linux will be a less useful operating system and is already less applicable to many of the people who wished to be involved in and support it. It will also lead to the reinvention of the wheel and wasted resources. Instead of using the best applications available, Bruce has doomed User Linux to mediocrity and spending vast amounts of effort on creating things that already exist.</p>

<p>Bruce's declaration will not stop others from supporting KDE as he noted. Ditto for MySQL. Bruce has guaranteed that there will be unneccesary variation and forking within the User Linux base if and when it materializes. So much for a uniform platform.</p>

<p>Finally, it also showed the rule-by-fiat nature of Bruce's project. I don't know about you, but I don't trust my time and energy to one man's decision making  process when it fails to draw upon the experiences and knowledge of those around him.</p>

<b>It Doesn't Affect Users</b>

<p>Bruce argues that users don't care what toolkit the programs they use are written in.He tried to pass it off as a developer issue that won't really affect anyone. Well, he's right in that users don't care what toolkits are used, but that is only true if things work. When a user tries to use an application and can't because her OS doesn't support that toolkit, she doesn't care why that toolkit isn't there, she only cares that it's rendered her system unable to run that program.</p>

<p>By not supporting KDE, User Linux will not be able to offer access to the thousands of KDE applications available. The same is true for MySQL. Explain that to your users.</p>

<p>Bruce actually got it completely backwards: toolkits don't matter to users, they just want everything to work. So it's up to the developers to normalize them as much as possible. This is a reality that the KDE proposal recognized and addressed with possible solutions.</p>

<b>It's an Economic Issue Part 1: Support</b>

<p>Bruce then tries to pass it off as an economic imperitive: if there's both KDE and GNOME then companies that wish to support User Linux may not be able to afford to. But this makes absolutely no sense.</p>

<p>User Linux will span a huge array of capabilities: from LDAP to web serving to databse servers to email services to groupware to desktops to web browsers to... If one expects a single company to service that whole array of items, that company either has to be very large or it has to subcontract work. If the company is very large, then the difference between supporting GNOME with KDE apps (or vice versa) and supporting only GNOME is inconsequential when compared to the costs of supporting LDAP and PostgreSQL and Mozilla.</p>

<p>Since Bruce envisions "a network of competing for-profit service providers" I can only assume that he isn't envisioning a small number of large shops that can do everything under the sun, but a large number of smaller shops that can work together. In this case, having a shop that provides KDE support which can be easily subcontracted to is the obvious answer, just as the GNOME shop that needs LDAP expertise will likely hire out for that expertise.</p>

<b>It's an Economic Issue Part 2: Royalties</b>

<p>Bruce then goes on to claim that it's an issue with royalties. Since Qt is dual licensed, you have to pay to write closed source applications. But this is not a royalty, it's a per developer license. Look up the definition of royalty in the dictionary and you'll see what I mean. Even after having this drawn to his attention, Bruce continues to use the word royalty because it aligns better with his desired message, because if it were a royalty then it would be a lot more expensive than it is.</p>

<p>But let's assume for the moment that a royalty and a developer license is the same, even though it isn't. Is the cost of the Qt developer license the mitigating factor in choosing to develop with Qt or with a free-as-in-beer toolkit? Looking at the dozens and dozens of commercially available applications written using Qt, it appears the answer is no.</p>

<p>Beyond the concern for the initial price of a tool, there's the question of the flexibility, quality and support of the product. Qt is hands-down a better toolkit that comes with much better tools for the developer than any other GPL'd toolkit out there, while rivaling the closed source toolkits (and surpassing most of those, too). Because of this for most development shops choosing Qt for their for-profit development is actually an economic win: what they save in time and make up for in a better product outstrips the license costs. There's also the issue that encouraging Free Software to be written by offering an incentive to do so (it's even cheaper) is a good thing.</p>

<p>But all of this really misses an even bigger point: if Gtk+ is available and the developer doesn't want to pay for a license then use Gtk+. Nobody is saying not to include Gtk+ or other free toolkits such as wxWindows; in fact, the KDE proposal paper specifically takes this reality into consideration and addresses issues of interoperability and ease of access.</p>

<p>Bruce is also ignoring the fact that many other parts of User Linux that people may wish to build upong will be GPL'd, including the kernel and OpenOffce.org.</p>

<b>Enterprises <i>Want</i> Closed Source Software</b>

<p>Bruce states that large companies want and need a large array of closed source software, and so therefore to make this body of software a toolkit must be made available for which it is zero cost to develop for. While the first part of that concept is probably accurate, the resulting conclusion is a farce.</p>

<p>If Bruce decides not to support Qt, then he's ensuring that all the closed source software that currently exists and is yet to be written on Qt/KDE will not be available to these enterprise customers! Bruce is trying to say that by giving choice, we must remove choice. Doubleplusgood, that!</p>

<p>I also find it curious that he states that we "need" to create a large pool of closed source software for enterprises. What are we doing writing all this Free Software then? Wasting our time? He says eventually we'll get to writing Free Software, but for now we'll need to write closed source software until we can "move up the application stack". This is completely at odds with his original white paper in which he stated that the goal was to leverage cost sharing to create "a fully supported and certified GNU/Linux system, without a per-seat fee, that meets the specific needs of their industry". Closed source software is not "cost sharing": it usually ends up getting licensed per-seat or per-site or not shared between companies at all. It honestly sounds to me like someone has a proprietary software play replete with vendor lock-in in mind. I'm sure that all those enterprise customers would rather pay licensing fees to Bruce for closed software rather sponser the development of this Free Software OS, even though the latter was the original idea as per Bruce's original white paper!</p>

<p>But this is all details when one asks the obvious question: does a free-as-in-beer toolkit actually create more closed source applications? If today's market is any indicator, the answer is no. Unless, of course, nobody is using Microsoft's development tools on Windows, which happen to cost money.</p>

<b>"I'm Brave and They Hate Me. Honor and Pity Me."</b>

<p>In a final stroke of spinmanship, Bruce counters with an emotional play. He says he did what he had to, and that it was the brave thing to do. It's also brave to step off a tall building with nothing to save you from the fall. Stupid, but brave; or desperate. One or the other.</p>

<p>Bruce also makes mention of taking an emotional beating himself. Thankfully we don't have to worry about it because Bruce is strong and has had a decade of practice dealing with it. Unfortunately, some of the comments on the mailing list were not very friendly. It would be great if everyone was always calm all the time. But that doesn't happen. It doesn't excuse it, but it does recognize reality. I've never once heard Linus complain about it, and he gets subjected to a lot more of it than Bruce could ever hope to. But I suppose when you are replying to a well thought out proposal, you may as well go for the emotional battery defense while you're scraping the barrel clean, right?</p>

<p>It's more than a little annoying to be dismissed because someone's feelings were hurt, or because the person feels they are brave enough to ignore you.</p>

<p>Bruce also reminds us that he's not a Qt-ogre because he's recommended Qt to others and is publishing a book through his book series title on Qt development. That's nice. Irrelevant to the discussion and decisions at hand, but nice.</p>

<p>All summed up, Bruce talked a lot about emotional responses in his article, but I don't think he was making observations. The discussions were, by-and-large, civil and factual. Several times people stated that they weren't speaking from an emotional place. It seems that Bruce was projecting quite a bit while writing his response.</p>

<b>Business As Usual</b>

<p>Bruce finishes by saying, essentially, that it's now business as usual in User Linux land: GNOME is the decision, end of story. Work will continue, best of luck to everyone else. Buh-bye. At least we know where Bruce stands, that's something he's very good at and something that I personally appreciate. There's very little beating around the bush with him.</p>

<p>It's a little sad that all the points above, and many more, were raised in fine detail with Bruce on the User Linux mailing list. None of it phased Bruce and so he regurgitated all of
his misleading arguments in his response piece without actually addressing the  KDE proposal itself at all! </p>

<p>Oh well... =) Here's looking forward to business as usual... which for me, at least, means a KDE desktop.</p>