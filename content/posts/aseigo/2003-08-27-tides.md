---
title:   "tides"
date:    2003-08-27
authors:
  - aseigo
slug:    tides
---
people wash in on the shores of KDE as others are washing back out... if i close my eyes i can almost hear the waves of people lapping against the sides of KDE's CVS.

ok, maybe that's a bit overly poetic. but it's very rewarding to find out that people are reading the WhatsThis tutorial and actually creating useful patches because of it. others are already asking about the Bug Triage tutorial. wonderful. i'll be posting something to kde-policies eventually with a draft of "bugs.kde.org Best Practices"; something to go along side the CVS commit policy. the Bug Triage tutorial will then draw heavily on that document.

as for people washing out... well... that happens for any number of reasons. sometimes people get busy, or get distracted, or have been doing it "long enough" and move on to other things. it's always sad to see them drift out and off our shores, but that's the nature of things.

what isn't pleasant is to watch as some are repulsed off the beachheads by the careless behaviour of others. i've watched thoughtless, sexist remarks hurt people to the bone. i've listened as people relate how they feel disenfranchised by the comments of others who have been able to attend N7Y when they haven't. i suppose it's unnavoidable in a project as large as KDE, but it would be nice if it was the utopia that appears in my dreams =)

in more interesting news (which says a lot since what follows isn't all that interesting) i've been wrangling kicker's amazingly annoying behaviour when it comes to background images. child panels now support them, vertical panels get the proper rotation, 1px wide graphics work on smaller panels, applet handles paint properly, the clock shows it properly, etc... lots of stupid figgly annoying crud. but i really want kicker to look good for 3.2. i also want to close some more kicker bugs.

kicker is out of the top 15 on b.k.o! huzzah! it would be cool to get kcontrol out of there as well. here's to wishful thinking.

sven is helping me redo kscd's interface. the goal is to bring it a bit closer to 2003 than it's current 1993-ish look... if we get to circa 2000 i'll be happy. =)