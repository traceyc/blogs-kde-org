---
title:   "fd.o redux"
date:    2004-02-02
authors:
  - aseigo
slug:    fdo-redux
---
Holy f***ing CHRIST people! I haven't read the blogs for a while due to being simultaneously busy beyond belief and rather uninterested in anything further than 3 feet from my own naval, but I decided to take a gander at what's been up here of late and ... it's crazyness! People are spouting off about fd.o when they obviously don't know the first thing about it. Let me cluebat some of you back onto this side of the International Stupid Line (which runs straight through Elbonia and the Far Side universe, IIRC).<!--break-->

First, try reading the specifications and look at who's been involved with them. Why, if it isn't some of our own great developers!

Next, try reading the specifications and look at where their inspiration came from. Why, if it isn't from some of our own great technology! Look at icon themes, the .desktop spec or even *gasp* DBUS for a few examples. Yes, that's right, DBUS is basically DCOP taken to the next level, namely without the Qt dependency (which allows using it in things like GUI-less apps) and with network usage in mind.

Finally, try thinking of actually USING an X11 based desktop without some of the standards at FD.o, and think about the future of the X11 desktop with even more of that. Things like the extended window manager hints, standardized clipboards, mimetypes, etc ... require something like FD.o to exist. FD.o is fairly unique in that it a) exists and b) isn't a stupid beaurocracy that's more intent on politicking it's members to death or extracting egregious fees from everyone who deigns cross the threshold. Most of the old complaints regarding Linux/UNIX desktops have evaporated (or are soon to evaporate) due in part to the unification that FD.o has been a part of.

Seriously: list 5 things that FD.o has influence KDE towards to KDE's detriment. Then list 5 things that FD.o has enabled to KDE's benefit (which is to say, our users' benefit). Compare the time it took to create those two lists. 

Yes, there are some stupid things on FD.o. Nobody and nothing is perfect, and I don't think anyone is calling FD.o a new Garden of Eden. But thankfully there is no "Thou shalt use everything on FD.o if you use anything from FD.o" commandment. And it's pretty damn easy to influence FD.o: sign up to some email lists and start contributing. "This is a family, not a conspiracy!" We are free to ignore what we wish, and influence what we may. Pretty nice, huh?

And let's put it all in perspective. FD.o isn't everything; the fact that people are KDE-izing the world of Linux/UNIX software outside of FD.o is a good sign to that effect. The fact that we have our own release schedules, our own feature lists and our own voices is another good sign. FD.o is one venue for accomplishing certain things, and it has been working rather well at doing what it does. The future of KDE does not hinge upon FD.o, but it's a great resource to help us get there.
