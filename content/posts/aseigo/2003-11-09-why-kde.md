---
title:   "Why KDE"
date:    2003-11-09
authors:
  - aseigo
slug:    why-kde
---
I'm going to try something different.  (*insert knowing laughter here*)  it seems the topic of "Why KDE" is coming up more and more often in my day-to-day life both on- and off-line. sometimes the actual topic is "Why open source" but i usually manage to steer it into "Why KDE" because KDE is something i'm rather interested in and it provides a great case study. through these conversations i've come across some interesting thoughts, and i'm going to record them here over the next little while, one per blog entry. here's #1.

Why KDE? because it is the font of desktop innovation. now, i can already hear the masses getting restless just waiting for a moment to say how this or that other desktop has created some innovation that KDE hasn't even gotten around to copying, let alone having innovated it. to which i say: what a wunderfully simplistic point of view.

i watched MS Windows blunder from one release to the next of their desktop system which were little more than variations on Windows 95. yes, each had some technical improvements, some additional features, but to be honest... nothing spectacularly interesting and new. ditto for Apple: OS7 (puke), OS8 (yawn), OS9 (more?)...

and then all of a sudden both Apple and MS start breaking out breathtakingly different products compard to their old fare. hrmm.... what could've motviated them both make such a move at this point in time? well, what changed in the desktop computing ecosystem? i believe the answer can be found by looking no further than KDE, Linux and the coming ascendency of the Free Software desktop. in the span of a few short years the usual suspects had a brand new competitor on the block  that was getting lots of media attention and which was cheaper, more secure, buzz-wordy and looked better. in many ways it was even technically superior. oh, and it was also transparent: the source was included. 

unlike BeOS and its kin, KDE not only stuck around, but is still growing in every metric: userbase, applications, capabilities, unique ideas, reputation... the unstopping nature of KDE has been vital in keeping the "innovate or die" heat applied to those in the busines of creating desktop environments.

had KDE never come along (and stayed around), would Apple have ever seen the reason to make OS X? or MS seen the need to create XP, draft Longhorn (and market it so heavily 2 years in advance), or create the .Net framework? or would it have just been business as usual with the status quo remaining because it was "good enough" and there was nothing threatening it?

what about GNOME? it was birthed as a reaction to KDE. in fact, KDE continues to inspire movement in GNOME even today, just as GNOME inspires movement in KDE. what started as a difference of perspective (to put it kindly for some of those involved) has evolved into an interesting outplay of coopetition and symbiosis. 

without KDE, it is likely that much of the recent flury of desktop innvoation would not have happened at all. therefore, everyone who would like to ensure that desktop computing continues to improve (and Lord knows it needs it) should get behind KDE: use it, deploy it, support it, create with it. 