---
title:   "Basing the future of free software upon demand"
date:    2003-11-15
authors:
  - aseigo
slug:    basing-future-free-software-upon-demand
---
What if, instead of basing our platforms, software and development paradigms upon chasing what we perceive the competition has today, we based our decisions upon user demand.

So when a group of people actualy need something, someone (probably people invovled that group having the need) make it. If nobody needs feature/library/component/technology X, nobody makes it and the platform isn't weighed down by it. If this sounds a lot like how things work in open source, it is.

Some development projects, mostly commercial ones (closed and open source), tend to try and think really hard of cool new things that nobody has yet, create those things, and then push those things as being critical and central to the software. Unfortunately, most of those same projects conflate "nobody needs" with "nobody has". Apple's new Expose windowing trick is a great example of something that nobody had, but that is actually useful to people. KDE's XMPLRPC daemon, as an opposite example, was neat in that pretty unique and rather cool but it wasn't exactly useful (in that people didn't use it). The same can be said about many of the features people like Microsoft trot out as being the next Have It Or Die feature.

When they trot out the next Have It Or Die thing there are those who believe them and, propelled by an irrational fear of being left behind, blindly try to clone it as quickly as possible. But then when the Have It Or Die thing flops (as happens more often than not), the cloners have just wasted their time and looked like twice the fool for having imitated the original fool. 

Now, I'm not saying that it's wrong to clone concepts and borrow ideas that arise elsewhere: that's called collaberation and inspiration; but such conceptual scavenging should be driven by actual user demand. This does not mean people need to be loudly asking for something before we decide to deliver it, but people should have an actual need for that thing we're going to deliver. The userbase may not actively realize they have that need, or may not be able to articulate it properly; those who create the technology for them have the job of doing that for them.

The Free / Open Source Software community has been creating based on demand for a long, long time. It works really, really well and has brought with it a nice ballance of innovation and conservatism that doesn't suffer from too many wasted dead ends. Those who are trying to focus on chasing tailights as anything more than an interim solution are throwing away one of the lessons we've been taught by experience. Those who would pick up any idea just because it exists elsewhere, as opposed to thoughtfully picking what to borrow and what not to borrow, are openning dangerous avenues that will lead to wasted efforts and less useful technology. Those who think the world of technology is moved by theoretically interesting inventions hasn't been paying attention.