---
title:   "presentation files"
date:    2004-04-19
authors:
  - aseigo
slug:    presentation-files
---
i forgot i had the presentations on my watch (it has 256MB of USB-accessible flash memory =) which, between devices:/ and fish:/, made it really easy to upload. i _think_ they are both the versions i used on the laptops, though i think i may have done some cleanups/typo fixing on each after "backing them up" from the laptop to my watch ;-) anyways. here they are:

http://bddf.ca/~aseigo/kde32.kpr
http://bddf.ca/~aseigo/kde_deploying.kpr

you miss quite a lot without my running commentary that goes along with them and my public-speaking antics, IMHO ;-) but you may get the general idea of things from the slides. or maybe not. someone came in and video taped the last 1/3rd or so of my the Deploying KDE presentation so maybe we'll see that online sometime. photos are forthcoming as well (don't have the digital camera with me right now).

enjoy.