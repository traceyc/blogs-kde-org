---
title:   "Try before you buy^H^H^Hcomment"
date:    2003-08-01
authors:
  - aseigo
slug:    try-you-buyhhhcomment
---
I discovered KDE had the option to rename files by clicking on the icon name (vs selecting them and then pressing F2). I'd like to have this turned on by default, rather than off, as it would address the #1 complaint I've received regarding single click mode. I suggested such on kfm-devel and kde-usability, and will be doing further testing w/RLUs (real live users) shortly...

The discussion that has ensued on those mailing lists has been interesting. I would suggest, however, that before one comments on a feature or concept  that one should take a minute to try it out first, especially when it is right there in CVS. Commenting blindly leaves you sounding less than informed and wastes others' time, right?