---
title:   "success begets ignorance"
date:    2004-02-23
authors:
  - aseigo
slug:    success-begets-ignorance
---
during the last week of moving house (while still working, of course..) i haven't had much contact with my usual "peer group", including the KDE people. let me just say that in their absence from my life i'm beginning to remember how amazing all those guys are. instead, i've been in contact primarily with people who randomly pop up or who come in through the office during the work day. with Linux and KDE taking off, education is definitely lagging behind. ignorance is frustrating, but at least it (unlike stupidity) is curable. some examples of the ignorance i've run into this week:

a guy opined that he wished wine would let him install Windows apps directly in Linux rather than him having to install them in Windows first then boot into Linux to run them. i then glanced over at our KDE/Linux desktop pilot systems that have (Unfortunately) several pieces of Windows software running under WINE on them and nary a lick of WindowsOS near them, and asked when the last time he'd used WINE was?

another fellow was all proud about how he was developing this closed source piece of software for Linux and how he was shipping copies all the .so's his app used, and statically linking every other library that he didn't ship with it. yes, this included glibc. he then went on, as i sat there mouth slightly agape, describing how he tests the success of his linking shennanigans: he installs it under Red Hat 6.2 and sees if it runs; THAT way, says he, he knows it's linking against his supplied libraries. after i suggested he just use ldd for that, he was conveniently quiet enough for me to suggest that shipping all his libraries with his tiny piece of software was not only just asking for support pains, it was an unnecessary and, from a bug fix and security standpoint, silly idea.

and of course, there's been the litany of confusion or ignorance over what KDE is. one example was the fellow contemplating moving his company's desktops from KDE to GNOME because Red Hat supports GNOME. actually, at first he thought it only supplied GNOME now with Fedora. besides being rather untrue, i asked him what he was running KDE on right now. answer: Red Hat. i asked him what had changed between now and then to change his mind: he wasn't sure. i informed him that Red Hat's support for KDE has actually improved in the last few years and that there was no shift in their current approach, which is largely desktop-agnostic with a bent towards GNOME for defaults. 

i know some people in the project don't care to create a KDE brand. presonally, i think creating a strong KDE brand would be deadly good for KDE. as would an educational program, which would also be deadly good for our users ;-)

so.. my personal observations for the week are:

 o moving sucks
 o success creates new problems, in this case wide spread ignorance
 o moving sucks