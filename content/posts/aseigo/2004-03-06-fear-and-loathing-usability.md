---
title:   "fear and loathing in usability"
date:    2004-03-06
authors:
  - aseigo
slug:    fear-and-loathing-usability
---
ok... so as the recent thread on kde-core-devel re: kcfg showed, people FEAR usability efforts. that's right, people FEAR it. that word was actually used in relation to the topic. they are afraid some people will screw it up, that KDE can't cope with a drive towards being more usable without becoming pure shit. i really feel like ranting about thickheads, but that would just be adrenaline speaking. as much as that would feel good ;-) , i'm going to try to be constructive instead: what can be done to gain the trust of people in KDE? or is that a pipedream?

how many times do i need to reiterate the "powerful, flexible AND usable" mantra before people hear it? or how could i say it in a way that people would understand and feel calm enough not to practice public knee-jerk stupidity? how many positive changes do we need to make for people to start trusting them? what sort of positive changes would help inspire trust?

i know other projects have abused their user population and done some really stupid things in the name of usability, and i know KDE is a powerful, configurable, flexible system which is something we need to protect. i like that aspect as much as anyone else. but just HOW does the failure of others translate to the impending failure of KDE?

these are not rhetorical questions. i need to find satisfactory answers.