---
title:   "aseigo. blog(\"kdedevelopers.org\"). stop();"
date:    2004-04-15
authors:
  - aseigo
slug:    aseigo-blogkdedevelopersorg-stop
---
my words have gotten me into trouble (some would say, "again"). apparently some people are truly offended because of the content of my blog. i can't rectify that now, nor can i fix other people's way of approaching the Universe. all the same, i don't want my personal thoughts to cast a pall of negative consequences upon KDE or any other Free Software project; so this will be my last blog entry on kdevelopers.org. i'll leave with some quotes that i found online while looking for some external wisdom on things. some i feel apply to me or to this situation. some are just interesting, or even funny. enjoy.

"To avoid criticism do nothing, say nothing, be nothing."
Elbert Hubbard

"People ask for criticism, but they only want praise."
W. Somerset Maugham

"Against criticism a man can neither protest nor defend himself; he must act in spite of it, and then it will gradually yield to him."
Johann Wolfgang von Goethe

"I have never found in a long experience of politics that criticism is ever inhibited by ignorance."
Harold Macmillan

"Observance of customs and laws can very easily be a cloak for a lie so subtle that our fellow human beings are unable to detect it. It may help us to escape all criticism, we may even be able to deceive ourselves in the belief of our obvious righteousness. But deep down, below the surface of the average man's conscience, he hears a voice whispering, 'There is something not right,' no matter how much his rightness is supported by public opinion or by the moral code."
Carl G. Jung

"Ours is an age of criticism, to which everything must be subjected. The sacredness of religion, and the authority of legislation, are by many regarded as grounds for exemption from the examination by this tribunal, But, if they are exempted, and cannot lay claim to sincere respect, which reason accords only to that which has stood the test of a free and public examination."
Immanuel Kant 

"It is strange that we do not temper our resentment of criticism with a thought for our many faults which have escaped us."
Author Unknown

"I'm deeply tired of the divisions within the larger Free Software community: invisible lines through which we dare not walk for fear of political reprisal."
Me.

"To err is human, to forgive divine."
Alexander Pope 