---
title:   "KDE, 4 year olds"
date:    2004-03-16
authors:
  - aseigo
slug:    kde-4-year-olds
---
i wish we designed KDE for 4 year olds. well, today's four year olds growing up in computer-centric societies, anyways. why? because they GET IT.
<!--break-->
watching my son use KDE, i realized that the Home metaphore for "your files" works very nicely. tonight he said, "i want to go to the home ..." and clicked on the little house icon to pull up the file manager. he loves looking at photos on the computer of people he knows. the whole "My Documents" thing or the claim that "the concept of a Home directory is so foreign and weird" is just a bunch of crotchety old people yammin' their lips. Home makes sense and it works when you don't come into it with preconceived notions, and My Documents requires the internalization of all sorts of preconceptions such as what a document is and that someone actually owns them. we all understand the concept of a "home" and how that thing works (we put our stuff there, sometimes other people's stuff goes there too, it's our base of operations ,etc), so it's a pretty damned good metaphor.

Konqi's world icon is effective too. instead of thinkng of it as "the web" or "the internet", think of it as the world outside your home. Peyton calls Konqi-the-browser "the world", because, well... that's what the icon is. it's the world. and that's rather accurate: while the home is our local place the world is everything else outside of that.

oh, and we don't close windows, or so i'm told. we ecks them. as in: "no, i didn't CLOSE the window, i exeded it!" when he said that i realized how funny it is that we force our metaphors so much: it's a window, so we CLOSE it. whatever. "close" is programmer speak for what you do after you open(2) it ;) Peyton is right: you don't close windows, you "x" them: as in "click on the x button which makes it go away". who needs a metaphore for that? oh yeah.. us oldies.

if all we had to do was make KDE usable for modern 4 year olds, we'd have it made. just think of the possibilities when dealing with people who Get It on such a fundamental and un-fucked-up level. too bad all these 20- to 90-somethings think they deserve to use computers that hold to bad metaphors invented by hippy-geeks in the 70s.

so remember ... it's a Home. not a "My" anything.