---
title:   "living room floors; LinuxFest NW; GNOMEs"
date:    2004-04-06
authors:
  - aseigo
slug:    living-room-floors-linuxfest-nw-gnomes
---
i slept on the living room floor last night. it felt good. a large body pillow on hardwood. mmmm. the computer played music to my sleeping head all night and the cats enjoyed not having to jump up onto the bed to harass me.

weekend after next i'm at <a href="http://linuxnorthwest.com">Linux Fest North West</a> in Bellingham, WA presenting on three topics: KDE 3.2 (and beyond), deploying KDE and developing with KDE. i'm driving out on  Friday the 16th: ROAD TRIP! ok, so it's "only" 10 hours of driving. close enough. i hope to meet lots of cool people and help provide some KDE promo and support in that neck of the US of A. i wonder if i should pre-announce my presence there somewhere KDE-centric, in case some KDE folk would like to get together ... 

and now for something completely different....

in his blog, Richard Dale moans about people hyperventilating over small differences between KDE and GNOME like file dialogs. i empathize. IMHO the #1 problem is that during development the two projects have different ways of approaching things.

KDE tends to look around and pick from what works if there isn't something already home grown. even when there is, cooperation is often a key point in decision making. look at the KOffice file formats, DBUS, XDG menus, etc, etc... this makes sense for KDE and for the larger desktop ecosystem.

GNOME tends to not care and just come up with their thing, whether or not its extensible beyond their own platform and whether or not it sucks. look at how they picked to switch button orders on dialogs. look at their new file dialogs. look at their VFS layer. yadda yadda yadda.

note that i used the word "tends", because both projects have acted out of character in certain situations. but generally, it's the way the two projects work.

personally i have nothing against trying new things and applaud those who are willing to give it a go. but i value consistency and a sane user experience over newness, and i also have a measure of respect for the travails of those who use a mix of apps (even though I don't personally do so).

predictions: watch what's going to happen with the new Evolution back end stuff. watch what's going to happen with KDE's multimedia layer in KDE4. watch the UI of KDE components like KDM (hint: it's already in kdenonbeta ;) and the UI of GNOME components like their file dialogs.

it's my pet peeve with the GNOME project, and one that i believe is a source of endangerment for the Free Software desktop as a whole. to be fair, i have pet peeves with the KDE project, but they aren't over issues that have extraproject ramifications. while the GNOMEs are stroking their collective chin over language issues, we need to remember the things that are truly important and lead by example by continueing to work on those things.

and no, i won't utter the word "GNOME" once in my LinuxFest NorthWest talks. i have a personal policy of not arguing in front of the kids ;-)