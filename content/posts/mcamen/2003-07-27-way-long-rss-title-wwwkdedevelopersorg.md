---
title:   "The way to long RSS title of www.kdedevelopers.org"
date:    2003-07-27
authors:
  - mcamen
slug:    way-long-rss-title-wwwkdedevelopersorg
---
After trying to fix the konq_sidebarnews layout for a full day now I am giving up --- for now. Otherwise I will go totally nuts.

@geiseri
Can you _please_ shorten the <title /> of the rss feed?
"www.kdedevelopers.org - KDE/Qt and  C++  Development" is just way to long for a tiny applet. And don't tell me to squeeze it to "www ...  ment"  ;-/

Feel free to extend the <description /> instead. Chances are that I will use this one for a more verbose ToolTip in the future.