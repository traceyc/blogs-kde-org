---
title: KleverNotes Version 1.0 official release 
date: 2024-06-05 
categories:
 - Release
authors:
  - Louis Schul
SPDX-License-Identifier: CC-BY-SA-4.0
---

![Screenshot of Klevernotes main page on desktop](https://cdn.kde.org/screenshots/klevernotes/main_note_desktop.png)

KleverNotes, KDE's Markdown note-taking and management application using Kirigami, is ready for its first release!

KleverNotes lets you create and preview Markdown notes while giving you the freedom to customize the preview from settings or using a CSS theme.

You can organize your notes however you want with a combination of categories and groups, which will be directly reflected on your system in the hierarchy of your KleverNotes storage folders.

Simply choose your storage location and you're ready to write!

You can print your notes, add small sketches and even create specific tasks for each of them, all from the application!

Notes are saved as Markdown files in your KleverNotes storage for easy access. 
They support the entire CommonMark specification with extensive syntax.
KleverNotes also introduces a small collection of opt-in “plugins” to extend basic markdown functionality, such as: 
code highlighting, note linking, quick emoji, PUML. 

## Special thanks
I would like to thank Carl Schwan who helped me through the incubator process, has set up the repository and the various KDE related things, fixed my code, and answered my many questions. The project would not be where it is without him.

## History
I started KleverNotes as a small personnal project to learn QML and C++ and motivate myself to take notes in class. After posting a few screenshots of my progress on Reddit, people seemed pretty interested, which inspired me to continue and redouble my efforts. Once it was added to KDE, my motivation grew even more, my final goal is now to be able to offer a simple alternative to QOwnNotes using Kirigami. (I actively use KleverNotes in each of my classes now btw 😬) 

## Final note
This release doesn't add anything special compared to my [last update](https://discuss.kde.org/t/klevernotes-ui-changes-performance-improvements-and-more), just UI tweaks from Carl, which makes the app better looking.
I just wanted to get things moving in order to officially push more updates in the future. 
A big one is in the works and should arrive soon once my exams are finished.

------

As always, I'll be more than happy to answer your questions, discuss potential features, or hear your point of view 😉


Link to the repo: https://invent.kde.org/office/klevernotes

Mirrorlist: https://download.kde.org/stable/klevernotes/1.0.0/klevernotes-1.0.0.tar.xz.mirrorlist
