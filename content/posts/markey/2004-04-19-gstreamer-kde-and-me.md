---
title:   "GStreamer, KDE and me"
date:    2004-04-19
authors:
  - markey
slug:    gstreamer-kde-and-me
---
As some of you might know, the amaroK project (an audio player for KDE) decided some time ago to implement an audio engine interface that makes it possible to use multiple media frameworks, which makes amaroK quite useful as a testing platform. Here I've tried to summarize my experiences with GStreamer so far (maybe mxcl can do the same for NMM sometime, since he's been hacking that engine): 
<br>
<br>

THE GOOD:
</dd>
<ul>
<li>
Architecture - I really like GST's overall architecture. It's a clean and efficient design, with the library nicely split into core and plugins. GStreamer's plugin framework is pretty flexible and powerful. It is possible to form complex structures by connecting components in a graph structure ("pipeline"), which is very intuitive. You can even build whole pipelines from the CLI with simple commands - pretty cool for testing stuff:
<br>
<br>
> gst-launch-0.8 filesrc location=/home/mark/foo.mp3 ! spider ! alsasink 
<br>
<br>
That's a complete command line audio player :)
</li>
<br>
<li>
Community - GStreamer has a very active, engaged community. In contrast to aRts, there's always a bunch of developers actively working on the project, it's easy to get support from the friendly folks on IRC and mailing lists, and there's an overall feeling of progress in air, which is really refreshing.
</li>
<br>
<li>
Completeness - There's already a huge pile of plugins, supporting pretty much any audio or video CODEC you could think of. And it's still growing fast, with new plugins being contributed all the time.
</li>
<br>
<li>
Performance - In general the developers have taken great care of a lean and efficient design, leading to decent performance. Of course this depends a lot on the actual plugin implementations, but in general GStreamer has a speedy feel to it. It does seem faster than aRts for many tasks. 
</li>
</ul>

THE BAD:
<ul>
<li>
Code - Frankly, for a KDE hacker GStreamer's coding style is a bit of a horror. You get the full C+GLIB monty: awfully_long_function_names(), constant need for (casting*) stuff, and ugly macros all over the place. The good news: it's possible to write plugins in C++ too. So, when you plan to contribute a plugin, you don't necessarily have to hack it in C (but you'll still have to mess a lot with GST's C interface, of course). I should also mention that there's Tim Jansen's really nice KDE::GST bindings, which you can use for KDE app development. Trouble is just, they need constant maintainance, and they're not in KDE yet.
</li>
<br>
<li>
Integration with KIO - To cut a long story short, it's very difficult, the two don't fit together well. Without a working KIO plugin, GStreamer will only be able to play local files - no shoutcast streams, no audio CDs, no frills. Tim Jansen started working on a KIO plugin some time ago, others have continued to hack it, but it's pretty complex and will take some time to become reliable.
</li>
<br>
<li>
Maturity - For a project actively in development for such a long time, and with an army of developers, overall stability is a bit of a let down; I consider 0.8 the first really usable version. And while things are getting better now, GST still suffers from occasional mem-leaks, non-descriptive error messages, and installation issues. Personally, I think the stability issues are partly related to the use of C.
</li>
</ul>

<br>
To sum it up, I've grown quite fond of GST lately, and I think the whole framework has been making some nice progress. I believe it is evolving into a powerful multimedia platform, useful for many types of applications. At the same time though, I'm also happy to see some alternatives arising recently, like NMM http://www.networkmultimedia.org and MLT http://mlt.sf.net. After all, competition can only mean a good thing.
