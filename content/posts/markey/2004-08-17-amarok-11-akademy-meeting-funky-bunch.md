---
title:   "amaroK 1.1 @ aKademy, meeting the funky bunch"
date:    2004-08-17
authors:
  - markey
slug:    amarok-11-akademy-meeting-funky-bunch
---
Heya, so I'm looking forward to <i>aKademy</i>, as it will be first real life meeting of the <i>amaroK development squad</i>. When you spend years developing stuff together in open source fashion, talking over IRC pretty much every day, it is kinda cool to meet up with your folks, exchange coding lore at the campfire, snatch some beer from the JuK developers, that sort of stuff.
<br>
<br>

In our luggage we will bring <i>amaroK 1.1</i>, and do a live release, like in the old days of amiga demo parties. Every group would bring a demo for the competition, and fix bugs until 5 minutes before the deadline, then release in totally exhausted state, and pray it won't crash running in front of 2000 people on the big projector. I love this community aspect of open source hacking, it is so much more fun than doing stuff alone. With amaroK we succeeded in forming something like a demo group, a motivated team of not only programmers, but also graphics guys, handbook writer and so on. Everyone's part of the team, and every significant contribution makes you an "author".
<br>
<br>
<i>So what is cool about amaroK 1.1?</i>
<ul>
<li>
For one, it is again much more polished and streamlined. There's a new <i>tag editor</i> with musicbrainz support for automatic tagging. We don't plan to steal JuK's show in this area. as JuK still is the superior tagging application, but we tried to gently enhance the tagging features while not bloating the interface too much. Then there's the new <i>xine engine</i>, which is working very well and decodes pretty much anything you throw at it. 
</li>
<br>
<li>
<i>GStreamer support</i>, my personal baby, has been integrated more closely with KDE. amaroK is the first app to provide KIO playing for GStreamer. E.g. you can play files directly from an ftp server, or via ssh. The engine is pretty advanced at this point, does smooth fade-outs and plays streams very well, with metadata support. Beta1 will not support aRts for decoding, as the plugin has still to be adapted to the new engine interface, but noone's crying tears for aRts, right? aRts is dead, deader than dead. 
</li>
<br>
<li>
<i>Automatic album cover download</i> is a slick new feature, with a bit of a wow effect. Fetches cover images and displays the image for the music you are currently listening to, along with useful information like how often the track has been played, which other tracks are on the album, and so on.
</li>
<br>
<li>
<i>Tons of</i> other new goodies, like a playlists-browser, enhanced music database (sqlite), and better dcop interface. And it <i>brews beer</i>, which JuK can't do yet.
</li>

</ul>
<br>
Meet you all @ aKademy!

