---
title:   "build.kde.org - Stable bump and bumps"
date:    2011-12-26
authors:
  - tnyblom
slug:    buildkdeorg-stable-bump-and-bumps
---
First, I've changed the branches that are being built on build.kde.org to reflect the branching of 4.8.

Second I've incorporated a build of Qt 4.8 branch that is triggered once a week to keep up with fixes there.

Third I've tried to get a build of Qt 5 up and running also, however this is where I've hit some bumps. I've been unable to get Qt 5 to build and as such not all other jobs are working :(

According to the FAQ the error I was first hitting can be avoided by building the submodules after building and installing qtbase, however then I get build errors in QtWebKit and if I try to omit that module somehow it is getting dragged back in by something yet to be determined.

So if anyone has any clues how to get a clean build of Qt 5 working please leave a comment.