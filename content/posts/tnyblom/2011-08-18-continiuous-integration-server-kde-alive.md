---
title:   "Continiuous Integration server for KDE - Alive"
date:    2011-08-18
authors:
  - tnyblom
slug:    continiuous-integration-server-kde-alive
---
Thanks to our fabulous sysadmin team KDE now has a Jenkins installation[1] that serves kdelibs (KDE/4.7), kde-runtime and kdepim* (all master).

The plan is to have this as a pilot and if that goes well expand to include all (main) modules, stable and master branches.


[1] <a href="http://build.kde.org">http://build.kde.org</a>