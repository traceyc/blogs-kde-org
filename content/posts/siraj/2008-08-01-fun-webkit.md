---
title:   "Fun with WebKit"
date:    2008-08-01
authors:
  - siraj
slug:    fun-webkit
---
At my new workplace <a src="http://www.collabora.co.uk"> Collabora </a>, I got the chance to play around with Webkit during the last few months. and one of the tasks we looked at was to improve and speedup webkit on QGraphicsView, And then produce useful ways to interact with Web contents, So yesterday, I was checking how responsive it would be to add live reflections of a webpage. 
Here is the video,  http://www.youtube.com/watch?v=T44QxXCQZzQ. And it wasn't bad as we expected. This is really easy with QWebView off screen rendering, But since this is a graphics Item, We had to do it differently.  The code is on a git repo and you can clone it from : http://git.collabora.co.uk/?p=user/siraj/stefani.git;a=summary. Ok now wondering .. "Stefani?" .  It's a playful toy we use to experiment with Web content on QGV. Stefani is built around QGraphicsItem with some super powers to render HTML contents. Basically, this QWebViewItem has the same API as the QWebView, but has some extra functions to produce bling. and one function there, emits a signal on content change, which allows us to render a reflection externally .  

One of my colleagues at  Collabora, Pierlux,  was working on Zooming effect similar to a popular mobile browser, to see his stuff visit (blog.squidy.info).

