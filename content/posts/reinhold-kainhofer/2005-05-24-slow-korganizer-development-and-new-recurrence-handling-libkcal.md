---
title:   "Slow KOrganizer development... And new recurrence handling in libkcal"
date:    2005-05-24
authors:
  - reinhold kainhofer
slug:    slow-korganizer-development-and-new-recurrence-handling-libkcal
---
As Bram noticed <a href="http://bram85.blogspot.com/2005/05/lets-return-to-korganizer.html">in his blog</a>, I haven't been working much on KOrganizer lately. That's for several reasons: 
<ul>
<li>Lack of time (as usual, since I have my day job <a href="http://www.fam.tuwien.ac.at">at university</a>, and several other <a href="http://www.jung-wien.at">hobbies</a>), 
<li> I needed some time off KDE after the release and all the time that I spent on writing the groupware resources in the last few weeks before the relase. And 
<li> Recently I started rewriting the whole Recurrence stuff in libkcal so that libkcal now (not committed yet) supports everything that rfc 2445 defines (multiple RRULES, EXDATEs, RDATEs, EXRULEs, even multiple EXRULEs, arbitrary combinations of the BY* components, etc.). It's not yet completely finished, but it looks really promising. (And no, don't worry, I won't submit it during the NL meeting and break everyone's kdepim during a pim-meeting)</ul>
<!--break-->

This new recurrence backend is implemented in libkcal, so when you load a calendar with arbitrary recurrences, the events will show up correctly even for recurrence types that korganizer isn't able to edit. The UI to edit recurrences are a completely different topic, and if I were in NL this weekend, I'd certainly talk to the usability guys/gals about the recurrence editor. The current one has several problems or shortcomings:

<ul>
<li> It looks terribly complicated for a simple thing. In evolution it looks so simple, but their approach has i18n problems, since it tries to generate a sentence from several widgets:<br> [image:1101]. 
<li> It allows you only to enter one recurrence, and even that is quite limited. E.g. you can't enter an event that recurs on every first and third sunday each month.
<li> Since KAlarm also uses a similar editor, I'd like to make the editor general and move it to libkcal, so other applications can also make use of it.
<li> Exceptions are only possible as dates in the editor, while sometimes you want to exclude only one specific time (relevant with sub-daily recurrences, like in kalarm)
<li> It's not possible to add additional recurrence dates. 
</ul>


The cool stuff about the new recurrence backend is that you can now create events with really sick recurrence rules, like:
<ul>
<li> The last workday of each month. (okay, that's quite usual, but the old version wasn't able to handle that at all)
<li> Every Friday the 13th, except in the months of July and August.
<li> every year on January 1st, but only if it falls in Week #1 of that year (Week #1 is defined as the first week that contains at least 4 days of a year).
<li> On all days that fall in calendar Week #53 if that year has 53 calendar weeks.
<li> Every 3 hours, on Mon-Fri each January.
<li> Every third saturday in June, at 8:30, 9:30 and 10:30.
</ul>

No idea if any of these are really needed anywhere (except for the last weekday of the month, which is typically pay day in the US), but it's still nice to have a full implementation of the standard.

