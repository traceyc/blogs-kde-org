---
title:   "Sad experience with Debian on laptop..."
date:    2007-12-06
authors:
  - reinhold kainhofer
slug:    sad-experience-debian-laptop
---
Until a few weeks ago, I had Kubuntu running on my Acer Aspire 5630 laptop (as described <a href="http://wiki.kainhofer.com/kubuntuonaceraspire5630">here</a>), and was more or less satisfied. It looked great, hardware support was satisfying, but I was missing the incremental package upgrades that I was used to on Debian (so that things break one small piece at a time, not everything at the same time when you do an upgrade). When, after upgrading to gutsy, the laptop would lock up every few minutes for a minute or so, I thought it was a Kubuntu problem and took it as the reason to setup Debian instead. BIG MISTAKE!!!!

After I had Debian installed, I realized how bad Debian's Laptop support really is:
<ul>
<li>KNetworkManager would not work with any WPA-encrypted WLAN networks (I can only connect to unencrypted networks); So after booting, I now need to run wpa_supplicant manually as root with the proper settings...
<li>The ACPI DSDT in the BIOS is broken on this laptop, so suspend and hibernate won't work. In Kubuntu, I could simply fix the DSDT.aml and put it into the initrd, where the kernel picked it up. Infortunately, Debian developers decided not to include that patch, so I can't replace the DSDT with the fixed one in the stock kernel. The patch is also not upstream, as described on the <a href="http://www.lesswatts.org/projects/acpi/faq.php">ACPI page</a>, because the kernel devs feel that inter alia <i>"If Windows can handle unmodified firmware, Linux should too."</i>. I think so too, but currently that's simply wishful thinking and does not have a bit to do with reality!!! I have yet to see one laptop where ACPI simply works out of the box in Linux! As a consequence, it seems that I will need to patch and compile the kernel myself for every new kernel upgrade (and of course also the packages for the additional kernel modules to satisfy the dependencies!)! The kernel devs again argue that <i>"If somebody is unable to rebuild the kernel, then it is hard to argue that they have any business running modified platform firmware."</i> Again, I agree, but just because **I AM** able to compile a kernel, does not mean that I should be forced to compile every kernel myself that I ever want to use!
<li>The Debian kernel also does not include the acerhk module, which is needed to support the additional hot keys on the laptop
</ul>

So, in short, I now have a laptop without properly working WLAN, no suspend and hibernate, and no support for the additional multimedia keys. Wait, what were my reasons to buy a laptop? Right, I wanted it for mobile usage, where I'm connected via WLAN, and simply open it, work two minutes and suspend it again...

I'm now starting to understand why some people say that Linux is not ready for the masses yet. If you are using Debian, it really is not ready, while with Kubuntu, all these things worked just fine out of the box (after I simply fixed the DSDT).

If having to recompile your own kernel every time it is upgraded is the price to pay for running Debian, I'm more than happy to switch back to KUbuntu again (which will cost me another weekend, which I simply don't have right now). The KUbuntu people seem to have understood that good hardware support is way more important than following strict principles (since the kernel devs don't include the dsdt patch, the Debian people also won't include it, simply because it's not in upstream... On the other hand, they are more than happy to patch KDE by self-tailored patches and cause bugs by these patches!!!).
