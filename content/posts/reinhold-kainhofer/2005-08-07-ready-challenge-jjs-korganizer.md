---
title:   "Ready for the Challenge? JJs for KOrganizer!"
date:    2005-08-07
authors:
  - reinhold kainhofer
slug:    ready-challenge-jjs-korganizer
---
As I wrote in my last blog, my to-do list for KOrganizer keeps growing and growing (sure, I implement / fix lots of stuff, but it seems that its still a long way to make KOrganizer perfect). So, I thought, I bet there are a bunch of interested guys and gals out there who were always interested in kdepim development, but never really dared to take on some open issue. 

So here is a short list of some Junior Jobs (JJs), which I think are not too hard to implement, and which would serve as nice small projects to get familiar with our code:

<ul>
<li>Add an "File->Import->Import Calendar Resource" item (see http://www.userbrain.de/kdepim/korganizer_menu.html, Suggestion 4 for the File Menu). This would basically do the same like the add in the resource view or in the kcontrol resource configuration..
<li>Write a "How to upload Hot New Stuff" (also see the above menu proposal by our usability expert Ellen)
<li>Add a menu item / toolbar button to let the user select a date to jump to. (see again the above proposal)
<li>Add a menu to quickly toggle the alarm for the selected item. In the agenda view's RMB (right mouse button) menu this functionality is already available, it's just not currently available to all other views.
<li>The default reminder time currently has only 5 possible values (in the config dialog). Extend this to use any hh:mm value (using the same time widget as the default length) and use this offset in the editor dialog.
<li>If you are working on a manually opened calendar file, and you import a file, the option to import it as resource should not be shown (since there is no resource calendar available).
<li>etc, etc.
</ul>

If you need any help or you want to work on any of these, or you have other ideas or questions, just join us either on IRC in the channel #kontact (on the server irc.kde.org), or at the mailing list kde-pim@kde.org...

There are also several open bug reports / wish list items at http://bugs.kde.org, so if you see something that fits you, just tell us. We'll definitely help anyone get familiar with our kdepim code!
And we can use anyone who want to help us make kde-pim even better!
<!--break-->