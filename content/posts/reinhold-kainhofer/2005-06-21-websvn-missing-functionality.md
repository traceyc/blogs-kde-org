---
title:   "websvn missing functionality"
date:    2005-06-21
authors:
  - reinhold kainhofer
slug:    websvn-missing-functionality
---
To be honest: While I really love the added functionality that subversion brings (offline diffs/reverts, atomic commits), websvn (or rather the viewcvs that we use on websvn.kde.org) just sucks for my use.

There are several things that I'm really, really missing compared to the old webcvs:

<ol>
<li> Annotations: Whenever I found something strange (in the code or in a backtrace), I'd go to webcvs and look at the annotated file, which shows me the line numbers together with the person that last changed that line. => You immediately know who to blame (well, mostly it's the case that I need to hide in shame myself ;-) )

<li> Now with websvn I couldn't even find the functionality to show line numbers with the code (the " (view)" simply shows the code without any line numbers!). Am I missing something, or is websvn really that bad in this respect?

<li> With webcvs it was easy to get the diff of the latest version in branch compared to the latest version in head. Since svn creates copies and doesn't tag, how can I compare the latest versions of trunk and branch in websvn, so I can quickly see which fixes have been backported?

<li> How can I see which were the branch points for the kde x.x branches in the past? That's needed e.g. to see if a particular fix happened before a release or after (and needs to be checked for backporting)

</ol>

I hope, I'm just missing something and that functionality is simply hiding somewhere...
