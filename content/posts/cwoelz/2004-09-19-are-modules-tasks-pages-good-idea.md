---
title:   "Are Modules Tasks Pages a good idea?"
date:    2004-09-19
authors:
  - cwoelz
slug:    are-modules-tasks-pages-good-idea
---
With the help of other pim developers, I updated the <a href="http://wiki.kde.org/tiki-index.php?page=Quality+Team+KDE+PIM">kdepim tasks page</a>. The kdepim tasks page was a very useful tool during the last release cycle, and is already helping the kdepim quality team and eventual newcomers to quickly see what needs to be done.<br>
Yes, we have a strong kdepim quality team, I am glad to say it. There are several people working on different areas of the pim applications, including the docs, websites, etc... who work silently, and are not explicitely involved with the quality team. However, at leat two guys, (me and Antonio Salazar), are already jumping in when needed. Antonio worked on Kontact docs during the last release cycle, and is working with KOrganizer whatsthis and hopefully with the docs too, and I wrote KPilot docs and whats this back then, and will do whatever is needed for this cycle (I did not decide yet). Some others started helping out in the last release cycle and submitted smaller contributions, and I hope to hear from them again (Hi Ramon and Jörg, how are you doing?). This effort started with the quality team annoncement six months ago.<br>
Unfortunately, it seems that other modules did not have the same results as we did. <a href="https://mail.kde.org/pipermail/kde-quality/2004-September/000991.html">My email to the quality list about the announcement for this release cycle</a> got no response. I offered help: "If you want to create or update your page to the next release cycle, I can help! Just drop me a line.". Nobody asked (yet).<br>
So if you think the tasks pages are a good idea, and you want to participate in the new release tasks annoncement, please drop me a line, or if you don't, I will go with with kdepim only.
<!--break-->