---
title:   "Hello, and what to expect from the Quality Team"
date:    2004-09-11
authors:
  - cwoelz
slug:    hello-and-what-expect-quality-team
---
Hello you all. I got into KDE development for fun, and I hope to have a nice journey while here. I have some nice ideas about why what we do is important, and I was already a financial contributor to Quanta before starting to help directly. You probably know me (or not) for my Quality Team work.

<br><br>
I found quite hard to discover something interesting to do inside KDE, and to learn my way in the free software jungle, as I am not a programmer and had very little contact with open source before. The process took two years, from a newbie user to an active contributor. (I have a very demanding job, that has nothing to do with software). KDE is my hobby and I love it!
<br><br>

But now to the main question: what to expect from the Quality Team?

<br><br>
The Quality Team has two main objectives
<ol>
<li>Support new contributors, programmers or not, helping them to
integrate to the normal KDE development process</li>
<li>Serve as a forum for recruitment, coordination and discussion of
development tasks</li>
</ol>
As part of (1), we recommend <b>focus</b> to new guys: one app or module
at a time, one new devel list at a time, try to leverage what you
learned (for instance, use the knowledge learned from the docs to write
whatsthis or vice versa, to do a ui review, write an article telling the
word why your app is a killer app etc. ) I try to act this way.
<br><br>
But a lot of people seems to miss what the objectives are, and
therefore, are disappointed with the results. I am not. As a matter of
fact, I am very happy. But don't trust my words, let's examine together
what was done in the last six months to fullfil these objectives.<br>
(Click the blog entry to read more)
<!--break-->
<br>
<br>
<b>Support</b>
<br>
<br>


To support and bring more newbies to KDE, we developed and are still
developing initiatives, contests (with the kde-artists team), <a
href="http://quality.kde.org/develop/howto">HOWTOs,</a> and <a
href="http://quality.kde.org/develop/cvsguide/buildstep.php">step-by-step
CVS building</a> and <a
href="http://quality.kde.org/develop/cvsguide/managestep.php">maintaining
CVS</a> guides, the <a href="http://i18n.kde.org/doc/doc-primer">kde
doc-primer</a> with the docs team, etc...
<br><br>
I am really happy with the results in this area, and I think we are
going in the right direction. We are working with kde-artists and the
docs team (I even consider myself part of the docs teams, as most of my
activities are documentation related), we have a permanent forum with
the explicit objective of helping new developers. We have better
communication (IMHO). I received a lot of feedback on the KDE
step-by-step building guide, so I guess new people like it.
A lot of effort was put in place since January to make the life of new
developers easier. If the FreeNX server offering KDE CVS desktops for
documenters and usability specialists becomes a reality, we (as KDE, not
only as quality team) will make a really big jump in ease of
development.
<br><br>

However, all these efforts not a guarantee of success. Logic tells us
that with lower barriers, we should see more people joining, which is
not necessarily true. Moreover, it may be hard to measure if more
developers are joining or not.
<br><br>
<b>Coordination</b>
<br><br>

The coordination effort is based on discussions on the list, promo work,
and <a href="http://quality.kde.org/modules/">lists of existing tasks on
the kde quality website</a> and lists of <a
href="http://wiki.kde.org/tiki-index.php?page=KDE+Quality+Team">open
tasks on kde wiki website</a>. The main part of this task is to help new
people to find a suitable task that is vacant. The point not to tell
people what to do, is quite the opposite, it's to take advantage of
their preferences and abilities, and find something they would like to
do. On the positive side I see:

<ul>
<li>The promo, quality, the docs and artwork teams are working together
to bring new people in. Nice initiatives appeared, like contests,
unified guides, etc...</li>
<li>Some module tasks pages like the <a
href="http://wiki.kdenews.org/tiki-index.php?page=Quality+Team+KDE+PIM">kdepim
tasks page</a> worked nicely to coordinate the work for the last release
cycle.</li>
<li>The Junior Jobs iniciative.</li> 
</ul>

Some initiatives did not work so well:

<ul>
<li>Nobody seems to care about lists of existing tasks. I wrote <a
href="http://quality.kde.org/modules/">descriptions and requirements for
different tasks</a>. Never got feedback on them. It seems that new guys
like to decide what to do themselves, and more, they seem to know what
they want to do before joining.</li>
<li>There are not so many new faces in the last three months. But I am
sure we can improve this in the following weeks. Stay tuned.</li>
</ul>

<br>
<b>The future</b>
<br>
<br>

The results will come over time, and will not be so visible, as newbies
tend to do whatever they want instead of what we want. It is
unreasonable to think that the Boring Tasks Nobody Wants To Do (BTNWTD)
will be solved by a permanent team (by definition). People <b>may</b>
solve them in the learning process, but then they will probably move on
to something more challenging.
<br><br>

Some iniciatives for the new release cycle:

<ol>
<li>Update of the module tasks pages and a make new call for developers,
for this release cycle.</li>
<li>Make a big call for new documenters, using the new doc-primer, the
contests, (and maybe the FreeNX desktop server?).</li>
<li>Create bug hunting days. But I will not organize that: I can't help
new programmers, as I am not one.</li>
</ol>
