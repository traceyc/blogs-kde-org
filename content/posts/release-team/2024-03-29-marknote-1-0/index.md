---
title: Marknote finally released!
date: 2024-03-29
categories:
 - Release
authors:
  - mbruchert
SPDX-License-Identifier: CC-BY-SA-4.0
---

[Marknote](https://apps.kde.org/marknote/), KDE's WYSIWYG note-taking
application, is finally ready for it's first release. Marknote lets you create
rich text notes and easily organise them into notebooks. You can personalise
your notebooks by choosing an icon and accent color for each one, making it
easy to distinguish between them and keep your notes at your fingertips. Your
notes are saved as Markdown files in your Documents folder, making it easy to
use your notes outside of Marknote as well as inside the app.

Thanks to Carl, Marknote now not only supports **bold** and _italic_ fonts, but also
lets you _underline_ important parts, <s>strike</s> out wrong parts, organise your
thoughts in various list types and organise your notes with titles, sections
and more.

![Marknote screenshot](https://cdn.kde.org/screenshots/marknote/marknote.png)

## Packager section

You can find the package on
[download.kde.org](https://download.kde.org/stable/marknote/marknote-1.0.0.tar.xz.mirrorlist)
and it has been signed with [Carl Schwan's GPG key](https://carlschwan.eu/gpg-02325448204e452a/).