---
title:   "Participate in WUD Berlin - CfP (German)"
date:    2006-09-07
authors:
  - el
slug:    participate-wud-berlin-cfp-german
---
... for the Germans around, especially Berlin-based ones:


*******************************************************
  Call for Papers and Participation - WUD Berlin 2006
      2nd World Usability Day (WUD) 2006 in Berlin
*******************************************************

<!--break-->

ZIELSTELLUNG

Dieses Jahr findet zum zweiten Mal der weltweite "World Usability Day" unter dem Motto "Making life easy" statt. Von den Veranstaltungen in über 70 Städten war der Berliner WUD im Jahr 2005 mit mehr als 100 Besuchern ein großer Erfolg.

Die Veranstaltung 2006 in Berlin steht unter dem Motto "Qualifikation - Kooperation - Integration". Gestalten Sie diesen Tag durch Ihren Vortrag oder Workshop mit!

Aus dem Motto ergeben sich die Themengebiete des Tages:

Qualifikation: Wie und wo kann man Usability-Kompetenz erwerben?

Kooperation: Wie arbeiten Usability Professionals mit Entwicklern, Designern, Produktmanagern und Kunden zusammen?

Integration: Wie wird Usability in Entwicklungsprozesse integriert?

Unser Publikum sind Interessierte an Aus-und Weiterbildungsmöglichkeiten im Bereich des Usability Engineering, Entscheider aus Wirtschaft und Verwaltung, die Usability in ihre Prozesse einbinden wollen und Software-Entwickler, die vor der Zusammenarbeit mit Usability-Professional keine Angst haben.

Leiten Sie diesen CfP bitte auch an Verteiler weiter, die Usability und Interaction Design zum Thema haben.

ABSTRACT

Reichen Sie bis zum 1. Oktober 2006 eine Zusammenfassung Ihres Beitrages ein: Titel und eine kurze Zusammenfassung (maximal eine halbe DIN A4 Seite) Ihres Vortrages. Schicken Sie Ihren Beitrag bitte an: konzeption@usabilitytag.de

VORTRAG/WORKSHOP

Der Vortrag sollte eine halbe Stunde nicht überschreiten. Längere Vorträge werden nur in begründeten Einzelfällen zugelassen.

VERÖFFENTLICHUNG

Die Veröffentlichung der Beiträge erfolgt digital als .pdf auf der Webseite des Berliner Usability Tages.

TERMINE

Einreichung der Zusammenfassung:  1. Oktober 2006
  Bestätigung der Teilnahme:  15. Oktober 2006
  Einreichung der Vorträge als .pdf:  10 November 2006
  Veranstaltungstermin:  14. November 2006