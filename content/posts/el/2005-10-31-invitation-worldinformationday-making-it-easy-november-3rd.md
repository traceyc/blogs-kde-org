---
title:   "Invitation: WorldInformationDay | Making It Easy | November 3rd"
date:    2005-10-31
authors:
  - el
slug:    invitation-worldinformationday-making-it-easy-november-3rd
---
As I mentioned before, there is finally a world-wide attempt to promote awareness of the benefits of usability engineering and user-centered design: Only three more days till World Usability Day! Around the world, there will be more than 70 events in 35 countries. WorldUsabilityDay is an initiative of the <a href="http://www.upassoc.org/">Usability Professionals' Organization</a>. On the official website of <a href="http://www.worldusabilityday.org">WorldUsabilityDay</a>, you can browse the events and will surely find one close to your location. 

If you happen to live around Berlin, feel free to join our whole day event at <a href="http://www.palisa.de">palisa.de, Berlin Friedrichshain</a> on November, 3rd. The Berlin event is mostly in the light of <i>Open Usability</i>. The <a href="http://www.usabilitytag.de/programm.html">program [German]</a> ranges from a general introduction to the field of usability to the live demonstration of several usability methods and panel discussions. The entry is free.



<i>Open Usability: Background information</i>

<i>Open Usability</i> aims at making usability more transparent. Processes, methods and results of usability work are publicly available and contacts to usability professionals are established. By this, education and further training for usability people is supported, and the benefit of usability is communicated to the outside world. 

Open Source Software is especially useful for Open Usability, as the publication of methods and results is not hindered by license politics. <a href="http://www.openusability.org">OpenUsability.org</a>, founded in Berlin, is a pioneer in the field of Open Usability.


<i>WorldUsabilityDay: Links</i>

 
 <a href="http://www.usabilitytag.de">Site of the Berlin event [German]</a>

<a href="http://www.usabilitytag.de">Site of all German events [German]</a>

<a href="http://www.worldusabilityday.org">Site of world-wide events [English]</a>


<!--break-->
