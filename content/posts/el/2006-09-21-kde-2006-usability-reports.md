---
title:   "KDE 2006 Usability Reports"
date:    2006-09-21
authors:
  - el
slug:    kde-2006-usability-reports
---
2006 isn't over yet, but the KDE Usability Project has already invested a huge amount of time in usability research. I've now put some of this work online, including reports about information design of the Human Interface Guidelines, about Amarok, Kaffeine, Kivio and What's This Help:
<ul>
<li>Visit the 2006 reports on <a href="http://usability.kde.org/activity/reports/2006reports.php">usability.kde.org</a>. </li>
</ul>

Still, this list is far from complete - there are numerous projects which are undocumented, such as usability tests with Kuroo during the German Linuxtag, user tests with the information architecture of Kubuntu's systemsettings, an evaluation of KWord, ideas to get a Fitt'er KDE, concepts for colour and font settings in configuration dialogs, participation in developer meetings like K3M, Kubuntu dev summit or KDE Four, as well as numerous small inspections and quick-advice in #openusability or #kde-usability. 

Thanks to all who spent so much time and passion on this!

<!--break-->