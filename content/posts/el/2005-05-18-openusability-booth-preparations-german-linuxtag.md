---
title:   "OpenUsability-Booth -> Preparations for the German LinuxTag"
date:    2005-05-18
authors:
  - el
slug:    openusability-booth-preparations-german-linuxtag
---
With our project <a href="http://www.openusability.org">OpenUsability</a>, we (1) will provide a booth at the <a href="http://www.linuxtag.org/2005/de/home.html">LinuxTag</a> in Karlsruhe/Germany, June 22. to 25. As usability people, it is our mission to make things more usable and to put a smile on each (inter)face. Therefore, we are also planning to make the LinuxTag a 'better place': On our booth, we will have sofas, free coffee, tea, and other drinks, possibly some relaxing music.... :)

But of course we'll also offer a more informative program:

- Live Usability-Testing
- On-the-fly Usability Support
- Info materials
- Discussion panel on Friday, 6/24, 13.00, room 2.08: Ways of integrating usability with OS Projects.
 
You are very welcome to visit us - whenever you are fed up with the fair and conference noise... :)


--------------------------------------------------------------------
(1) 'We' are <a href="https://blogs.kde.org/blog/938">janushead</a>, <a href="http://holehan.blogspot.com/">tina</a>, <a href="http://holehan.blogspot.com/">holehan</a>, bjoern and me.



<!--break-->





