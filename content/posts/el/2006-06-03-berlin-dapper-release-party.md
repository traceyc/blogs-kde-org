---
title:   "Berlin Dapper Release Party"
date:    2006-06-03
authors:
  - el
slug:    berlin-dapper-release-party
---
I much enjoyed the <a href="https://wiki.ubuntu.com/DapperReleaseParties">Berlin Dapper Release Party</a> yesterday at <a href="http://daniel.holba.ch/blog/">DanielHolbach</a>'s place in Berlin Neukoelln. He even served my favourite drink: Schonk - <a href="http://www.piaxa.de/index.html">Club Mate</a> with Rum =) 

Unfortunately I had to leave at 1 o'clock when the party just started to become really good and crowded to meet some friends who had come over to Berlin from back home. I crossed half the city by bike, and when I finally got there, they were tired and wanted to go to bed. 

eeeek!


<!--break-->