---
title:   "Contextual Help and its Effects on Human Working Memory Load"
date:    2006-01-29
authors:
  - el
slug:    contextual-help-and-its-effects-human-working-memory-load
---
Exactly three years ago I was busy writing my diploma thesis - it was about a help system which showed the user directly in the application where and how to perform certain tasks. This type of help is called <i>contextual</i> or <i>context sensitive help</i>. 

We had two different types of contextual help: One was bubbles that appeared next to buttons or entry fields, telling the user what to do there. The other was the mouse pointer itself, going into the application, clicking onto certain interface elements or entering text. Both types could visualise whole task sequences, not only single actions. 

For my thesis I conducted a user test where the performance after learning a software with a conventional tutorial was compared to the performance after learning with that contextual help system. Of course our expectation was that users better learn a software with our new help system. But why? 

<br>

<b> 1. The Role of Cognitive Load in Software Learning </b>

The utilisation of conventional help material while learning to use a complex software application routinely splits the learners' attention between different sources of information: Besides reading the instructions in a documentation, they have to search for interaction elements in the software interface and use input devices such as a keyboard and a mouse. The effects of such 'attention- splitting' situations have been thoroughly investigated in the scope of the cognitive load theory , a general theory of learning and problem solving. Assuming that working memory plays a crucial role in the act of learning, but at the same time is limited in capacity, it is important to free resources for relevant information elements - which are the contents to be learned. The total amount of information elements that have to be held in working memory simultaneously is referred to as cognitive load.

Learning material that splits the learner's attention induces irrelevant cognitive load, because redirecting the attention between different sources of information consumes working memory capacity. As a consequence, the total amount of cognitive load induced by the instructional task may exceed the limits of working memory and therefore hinder learning. 

The bubbles and mouse pointer movements in the help system introduced in my thesis guided the learner's attention towards related information elements of the instruction and the application interface and therefore freed working memory capacity for the important process of learning what they saw. As a result, the performance in the subsequent test was significantly better than among those who used a conventional online manual to learn the software.


<br>

<b> 2. Contextual and Conventional Help in KDE </b>

In KDE, we also have contextual help, namely <i>What's This</i>. It appears directly in the interface and provides the user with information about a button he is currently interested in.

Unfortunately, the current design of What's This has several disadvantages - the user does not know what interface elements it is available for, it disappears as soon as the user clicks into the software so he can't use it for reference, and as it can't be moved it sometimes covers the interface elements it refers to. As a consequence, users mostly have to fall back to the <i>Handbook</i>.

The Handbook is the general manual for the application and starts up with an overview page. It does not adjust to the current situation, the <i>task context</i>, and is referred to as <i>conventional help</i> in the literature. While conventual manuals are a good tool when learning a software step by step, they are not appropriate in situations when the user wants to have a quick information about the currently displayed interface element. 

Four Aspects are problematic in this context as they induce additional cognitive load:
<ol>
<li>First, the user has to find the relevant paragraphs.</li>
<li>Then, he has to understand which interface elements in the manual refer to the elements in the software.</li>
<li>Finally, he has to understand what is described there and how different steps relate to each other.</li>
<li>In the worst case, he still has to find out why actions described in the manual are currently not available or disabled.</li>
</ol>

<br>


<i><b> 2.1 What to do to better provide contextual help? </b></i>

In the Human Computer Interaction Workgroup, we've recently thought of ways how to increase the usage of the help functions, and to improve contextual help in KDE. We came to the conclusion that the user's attention needs to be guided to information that is relevant in the current context: We called that approach <i>Nested Help</i>. Nested help guides the user step by step from essential to more extensive information. By this, the cognitive load when searching for relevant help contents is limited (Aspect 1), and by using a consistent nomenclature the problems to identify which labels correspond to what interface elements are reduced (Aspect 2).

Our idea of a nested help starts in the interface, when the user moves his mouse over an interface element. Users who are unsure about the purpose of an interface element usually wait for the tooltip to show up. Together with the tooltip, we want to provide a hint that What's This is available for that element - for example a special icon in the tooltip. If the user realises that the information provided in the tooltip is not sufficient, he is pointed to the next level of help, namely What's This.

[image:1778 align=center width=600 class=showonplanet]

The user then activates What's This for this interface element, and gets more detailed information: Hints and tips, and - as in the below example - references to other interface elements like screenshots or links (in the example, the link 'System Options' opens the corresponding What's This). This further reduces cognitive load as the user is guided to each element described in the text (Aspect 2).

[image:1779 align=center width=450 class=showonplanet]

If the information still is not sufficient, the user can follow the link to the manual provided in the upper right corner. It leads directly to the page describing the current context of usage.




Freeing working memory by guiding attention with <i>Nested Help</i> will make it easier to understand the given information (Aspect 3). 

However, as long as a nested help does not dynamically adjust to preconditions, it is difficult to prevent problems described in Aspect 4 - finding out why actions described in the manual are currently not available or disabled. Technical writers have to find possible pitfalls and describe fallback solutions. 

<br>


<i><b>2.2 What needs to be done to make this approach applicable?</b></i>

All in all, this type of nested help will require high efforts from documentation and internationalisation: More information needs to be produced and translated, and unfortunately it is partly redundant (which may result in boring translation jobs).

Therefore, guidelines should be established to manifest which parts can be reused (automatically?), but also what kind of information should be given in What's This and what should be moved to the manual. Currently, there is a huge shift between the amount of information given in What's This - compare, for example, KPrinter and KControl!

Finally, it is questionable if What's This should remain in the GUI strings file, or if it should be moved to the documentation. The advantage of having it in the GUI strings is that developers can easily add a What's This hint while they are writing their code. On the other hand, to make What's This as efficient as described above, they need to be edited, relevant chapters in the manual need to be linked, and screenshots or internal links must be added. This is the job of technical writers who should not be forced to edit the GUI strings file.

... or don't they mind? Thoughts and comments appreciated!







 





