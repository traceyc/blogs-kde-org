---
title:   "Graphic artists and designers wanted in Berlin area"
date:    2006-10-23
authors:
  - el
slug:    graphic-artists-and-designers-wanted-berlin-area
---
In the scope of <a href="http://www.gimp.org/announcements/open-usability-gimp.html">OpenUsability's GIMP student project</a> we are planning a usability activity in Berlin between <b>October 31st and November 2nd</b>.

The goal is to gain a better understanding of the every day work of professional graphic artists, web and icon designers. We'd like to visit you at home or in your accustomed work environment, ask some questions and watch you do your usual tasks. All in all, this is not going to take longer than 2 hours. 

We are looking for 

<ul>
<li>professional <b>graphic artists, web or icon designers</b> </li>
<li> who live or work in the <b>Berlin</b> area.</li>
<li>GIMP experience <b>is not required</b>. We want to watch you using your accustomed tools in your familiar work environment - no matter if it's the GIMP, Photoshop or another tool.</li>
</ul>

If you are interested to participate or if you happen to know OSS-friendly designers in Berlin, please contact ellen at openusability org.

The interview will help the developers of GIMP tremendously! Your benefit will be the warm fuzzy feeling of having helped an open source project - as well as a great new version of GIMP when the time comes, with improvements based on the information gathered from your answers.


<!--break-->