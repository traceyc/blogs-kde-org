---
title:   "OpenUsability Sponsored Student Project"
date:    2006-08-13
authors:
  - el
slug:    openusability-sponsored-student-project
---
OpenUsability is proud to announce the offering of a series of sponsored student projects. They provide an excellent way for usability, user-interface design, and interaction design students to gain experience in the interdisciplinary and collaborative development of user interface solutions in international software projects.

We are looking for a student who works on the user interface for the next generation of the GNU Image Manipulation Program (<a href="http://www.gimp.org">GIMP</a>). During a three-month cooperation, you will closely work together with Peter Sikking,  principal interaction architect at <a href="http://www.mmiworks.net">M+MI Works</a>. Activities include methodically performing a full expert evaluation and analysis of the software, being fully involved in every decision, and performing the bulk of the project work. You will have a great opportunity to learn the ropes in interaction architecture in a project that matters.

For details and information how to apply, see <a href="http://www.openusability.org/studentprojects">www.openusability.org/studentprojects</a>.


A number of further student projects, including KDE and other projects hosted on OpenUsability, will be announced in November in the scope of the <a href="http://www.upassoc.org/worldusabilityday/index.html">World Usability Day</a>. Stay tuned!

<!--break-->