---
title:   "and finally... the beach!"
date:    2006-10-01
authors:
  - el
slug:    and-finally-beach
---
[image:2415 width=600 class=showonplanet align=center]


the scenery around dublin is really beautiful. however, after 10 nights in a dark hostel room, I'm really really looking forward to go home, to sleep in my own bed, have a shower in my own bathroom, and eat in restaurants where "vegetarian" is not a foreign word ;-)

<!--break-->