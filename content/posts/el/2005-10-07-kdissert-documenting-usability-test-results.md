---
title:   "KDissert for Documenting Usability Test Results"
date:    2005-10-07
authors:
  - el
slug:    kdissert-documenting-usability-test-results
---
At aKademy, Frank performed a <a href="http://wiki.kde.org/tiki-index.php?page=Live%20Usability%20Test%3A%20Kdissert&PHPSESSID=f70cc28e7386bd878b7bbac3dfda2c76">live usability test</a> with Thomas Nagy's <a href="http://freehackers.org/~tnagy/kdissert/">kdissert</a>, a powerful mindmapping tool for building texts.

I extended the usability test a bit and used kdissert myself to summarise the results. This is an extract of the mindmap:

[image:1524 align=center width=300 class=showonplanet]

There are multiple ways to structure usability reports<b>*</b>: You can structure it by the <b>software components</b> which are affected, by <b>general usability guidelines</b>  (consistency, user guidance, error tolerance), or you can structure them according to the <b>use cases</b> that were tested. Which one to select mostly depends on the target audience - developers prefer software components, usability people prefer guidelines and managers prefer use cases<b>**</b>.  

In this case I chose to structure the report according to the use cases. Each first-level leave of the mindmap corresponds to a task. The second level leaves illustrate the steps the users performed. The sequence of the steps corresponds the priority of each step.


[image:1523 align=center width=300 class=showonplanet]


The users' behaviour as well as suggestions can be described in more detail in a leave's comments field. 


[image:1525 align=center width=300 class=showonplanet]


But kdissert is not only a mindmapping tool. A text document, presentation or html file can be generated from the mindmap. In a text document, the comments will be shown in the corresponding subchapters. This makes the whole tool quite cool, as it supports both a visual arrangement of your ideas and the linguistic elaboration :-)


----------------------------------------------------

<b>*</b> Note that this case can not really be called a usability test as only one user was tested (it was a live test at aKademy and I've not yet had the time to test more users).

<b>**</b>This classification is, of course, over simplified ;-)

<!--break-->