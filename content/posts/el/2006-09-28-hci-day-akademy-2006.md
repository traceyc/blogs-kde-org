---
title:   "HCI Day @ aKademy 2006"
date:    2006-09-28
authors:
  - el
slug:    hci-day-akademy-2006
---
Yesterday we had our <a href="http://conference2006.kde.org/codingmarathon/hciday.php">Human-Computer Interaction Day</a> with a very rich <a href="http://wiki.kde.org/tiki-index.php?page=HCI+%40+akademy">programme</a>. There was an accessibility and a usability track, as well as a session on icon naming by the artists. 

Of course, I mostly attended (and participated in) the usability track which started with a <a href="http://wiki.kde.org/tiki-index.php?page=Reusable+User+Interface+Patterns+In+Settings+Dialogs">session on reusable interface patterns in settings dialogs</a> chaired by Florian. He introduced his <a href="http://www.uni-koblenz.de/~soma/patterns">Open Design Patterns</a> library he created in the scope of his diploma thesis (best viewed with firefox!). It shows best practices to common design problems, e.g. how to design the main navigation in settings dialogs, how to reset changes, etc. Especially the pattern of "advanced preferences" caused a long discussion about the advantages and disadvantages of the feature-richness we have in KDE.

After Florian, I chaired a BoF on <a href="http://wiki.kde.org/tiki-index.php?page=Grouping+and+Aligning+in+KDE4">Grouping and Aligning in KDE 4</a>. I mostly showed and further explained the screenshots I posted earlier in this blog. It's quite obvious that developers would have a hard time implementing the suggested design with the current Qt Designer. Let's see if we can do something about this.

Later, there was an accessibility talk by Emmanuel Lesser about touchscreen applications. He flew in from Belgium just to give this talk - wow!

Then, it was my turn again, giving a summary of the considerations of the marketing and hci working group about possible <a href="http://wiki.kde.org/tiki-index.php?page=Target+users+for+KDE4">target users for KDE 4</a>. There were no objections that KDE should target users who have higher claims when it comes to computing and who use their PC on a frequent base. Sebas will explain a bit more on the backgrounds in his session on Scientific research and KDE target markets today (16.00, Lecture Theatre 4). And everybody agreed that Aunt Tillie does not use KDE ;-)

Finally, Ken explained the problems the artists are faced when migrating to the new icon naming scheme as suggested by freedesktop.org. The oxygen icon set is already named by this new scheme, while KDE 3+4 applications still use the old one. This makes it difficult to simply replace the old icons by oxygen, and hinders testing the icons in their real environment (toolbars, menus, etc).

In the evening, we wanted to celebrate the anniversary of the idea of OpenUsability which was born three years ago at aKademy in Novre Hrady. Unfortunately, Jan Muehlig - the head behind OpenUsability - had already left, so we decided to postpone the "real" anniversary to next spring (that's the platform's anniversary). Let's have a big party in Berlin then!

<!--break-->
