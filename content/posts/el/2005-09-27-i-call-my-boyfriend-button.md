---
title:   "The \"I call my boyfriend\" button"
date:    2005-09-27
authors:
  - el
slug:    i-call-my-boyfriend-button
---
I never liked the idea that girls are technically less interested than boys or get less into technology. But this user survey we are conducting really keeps frustrating me!!!

Background: Two weeks ago, we started interviewing people on how they organise their documents and files on their hard disk. We asked random people who showed up for a web usability test, that means our participants covered all types of internet users between 18 and 50 years.

While most male participants showed a more or less systematic approach in file managing, most of our female participants had severe problems to create folders or to move files from one location to another. The problems came up with Konqueror (which is understandable as it's a new and uncommon usage environment), but also with Windows Explorer. Keeping in mind that each of the participants claimed to work at a computer 3 to 9 hours a day this is a surprising result.  

When <a href="http://www.lazs.de/">Bjoern</a> asked them what they do back home when they need to create folders, two out of six female participants said "I call my boyfriend". 

... OK, let's implement the "Call my boyfriend" button ... 
<!--break-->