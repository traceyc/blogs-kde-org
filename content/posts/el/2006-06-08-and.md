---
title:   "this and that"
date:    2006-06-08
authors:
  - el
slug:    and
---
** Sebastian, one of the blind participants of the Accessibility-Meets-Usability weekend, just told me that the development of Gnopernicus, a screen reader and magnifier for Gnome, has been stopped. <strike>I hope these are just rumours? It would be a pity.</strike> Instead, the development of <a href="http://live.gnome.org/Orca/">Orca</a> will be reinforced which is actually good news (thanks henrik for the clarification!).

** Aaron pointed me to this <a href="http://www.tomshardware.com/2006/06/07/a_continuing_work_in_progress/page3.html">article about usability in Linux projects</a> on tomshardware.com. It's great to read our work is getting such positive attention =) 

** Together with Bjoern from OpenUsability I'll attend the <a href="http://www.tossad.org/tossad/events/tossad_2006">tOSSad Workshop</a> in Como on Saturday, a workshop on Governmental, Educational, Usability, and Legal Issues towards Open Source Software Adoption in an enlarged Europe. I'm looking forward to meet <a href="http://people.kde.nl/gorkem.html">Gorkem</a> there who is very active translating KDE and works on the Turkish Linux distribution Pardus.


<!--break-->