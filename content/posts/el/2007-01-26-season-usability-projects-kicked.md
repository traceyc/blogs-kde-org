---
title:   "Season of Usability Projects Kicked Off"
date:    2007-01-26
authors:
  - el
slug:    season-usability-projects-kicked
---
In November, OpenUsability announced six <a href="http://www.openusability.org/season/0607/">mentored projects</a> for students of usability, user interface design, interaction design, or related. We received applications from all over the world - New Zealand, India, South Africa, Europe, South America, USA and Canada. Most of the applicants were highly skilled, and it was sometimes difficult to take a decision. Finally, the following teams formed up:

<ul>

<li><b>Basket</b>

Student intern: Frank Ploss, Germany
Usability mentor: Bjoern Balazs, Germany
Technical mentor: Sebastien Laout, France

Frank Ploss, student of computer science, performs a usability analysis and redesign of the <a href="http://basket.kde.org/">Basket Notepad</a> for KDE. He adopted the topic for his diploma thesis he just started to write and set up a <a href="http://basket.openusability.org">web page</a> where you can follow his progress.



<li><b>GeeXBoX</b>

Student intern: Francesco De Rose, Italy
Usability mentor: Celeste Lyn Paul, USA
Technical mentor: Benjamin Zores, France

The team had its first kick off meeting in the beginning of January and started its work on a redesign of the <a href="http://geexbox.org/en/index.html">GeeXBoX</a> multimedia system. One of the challenges of designing an interface for this kind of system is the low resolution (must be clear enough to read on a TV) and limited input devices (a joystick of simple controller should be enough).

</li>


<li><b>Open Printing</b>

Student intern: Andrea Alessandrini, Italy
Usability mentor: Peter Sikking, Germany


The goal of this project is to design first low, then medium, fidelity printing dialogs, using the information that is gathered by questionnaires and testing of the integrated usability part of the project. The project will start on the first of February.

</li>

<li><b>Okular</b>

Student intern: Sharad Baliyan, India
Usability mentor: Florian Graessle, Germany
Technical mentor: Pino Toscano, Italy

The team is currently in the project management phase and will soon advance the usability of <a href="http://okular.org">Okular</a>, the next generation document reader for KDE. 

</li>


<li><b>Serendipity</b>

Student intern: Prabhath Sirisena, Sri Lanka
Usability Mentor: Ellen Reitmayr, Germany
Technical Mentor: Garvin Hicking, Germany

We are currently planning a user survey to collect the major use cases for casual blogging with the <a href="http://www.s9y.org/">Serendipity weblog system</a>. The project progress will be documented in a <a href="http://85.10.193.9/serendipity/index.php/Main_Page">wiki on OpenUsability</a>.

</li>


<li><b>SIP Communicator</b>

Student intern: Fabiana Meira Pires, Brazil
Usability mentor: Jan Muehlig, Germany
Technical mentor: Emil Ivov, France

The team had it's first kickoff meeting on IRC and started its work to design a solution for transparent management of multiple VoIP and IM accounts with different functionality in a single application, namely <a href="http://sip-communicator.org/">SIP Communicator</a>.

</li>
</ul>


In addition to the "Season of Usability" openings, we found support for two more projects:

<ul>
<li><b>OpenWengo</b>

Student intern: Paula Bach, USA
Technical mentor: Jerome Wagner, France

Paula will work on one or more of the following tasks to improve the usability of <a href="http://www.openwengo.org/">OpenWengo</a>, a VoIP client:

+ Design a a quick launcher for essential functions of the application.
+ Integration of the download/update part with the plugin framework.
+ Addition and integration of 3-way video conferencing.
+ Addition and integration of "video smileys".

</li>

<li><b>Inkscape</b>

Student intern: Jennifer Ferreira, New Zealand
Usability mentor: Alan Horkan, Ireland
Technical mentor: Cedric Gemy, France

Jennifer is going to develop user models for the layout application <a href="http://www.inkscape.org/">Inkscape</a>.

</li>
</ul>

A big thanks to all students who showed so much interest in FLOSS usability! 

For those who were not selected: Please don't hesitate to apply again next season. In the meantime you might have a look at our <a href="http://www.openusability.org/openings">project openings</a> and lead your own OpenUsability project. Don't be deterred - working in FLOSS is first of all fun!



<!--break-->