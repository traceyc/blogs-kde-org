---
title:   "It should just print, no?"
date:    2006-04-05
authors:
  - el
slug:    it-should-just-print-no
---
During my three years of usability presence in Open Source projects, printing has twice been the stumbling block to extensive and hot-blooded discussions about the whole purpose of usability for Linux.

I much enjoyed the first one: Being relatively new to OSS usability, the public rant from the well-known Open Source evangelist Eric S. Raymond about the usability of printer installation - an essay he published under the title <a href="http://www.catb.org/~esr/writings/cups-horror.html">An Open Source Horror Story</a> was some kind of a promoter for my own work. Suddenly everybody cried for usability =)

The second one, however, was less funny: Displeased by his impression that Gnome tried to reduce complexity through cutting down functionality, for example in its printing dialogs, Linus Torvalds publicly <a href="http://mail.gnome.org/archives/usability/2005-December/msg00021.html">encouraged users to switch to KDE</a>. In the upcoming discussions, usability people were blamed to overshoot the mark, to reduce complexity to a degree where applications are no longer usable for above-average users. 


Why is it so hard to make printing dialogs that meet the users expectations? After all, it should just print, no? Famous last words... 

In an attempt to find a solution that meets the expectations of all involved parties - Linux distributions and the major desktops, printer manufacturer, network admins, professional designers and home users - the Open Source Desktop Labs <a href="http://groups.osdl.org/workgroups/dtl/desktop_architects/desktop_printing">have formed up a workgroup</a> that cares for the standardisation and simplification of printing efforts under Linux.

As technical reliability and a sane user interface design should go hand in hand, they invited OpenUsability to take part in the <a href="http://groups.osdl.org/workgroups/dtl/desktop_architects/desktop_printing">OSDL Printing Summit</a> next week in Atlanta. Celeste, Jan and me will go there and design UI sketches for all printing related tasks - installation, configuration, maintenance, printing and job observation. 

A tough job as I learned last Friday when I visited Kurt at <a href="http://www.danka.de/">Danka</a>, a print service provider, to get a private lesson that let me into the secrets of printing: Apart from the different positions how much functionality should be available, general security issues, permission handling, driver reliability, mixed-network support, adequate error detection and handling, particularities of different distros and printer manufacturers need to be considered.

However, before racking our branes about that, we'll pass a weekend at Celeste's place in Washington D.C. to do some KDE usability stuff. And visit the cherry blossom festival. And the White House. And everything. 

... my first time in the U.S., I'm  curious =)

<!--break-->





