---
title:   "KDE QA Meeting Results from a Usability Point of View :)"
date:    2005-12-12
authors:
  - el
slug:    kde-qa-meeting-results-usability-point-view
---
As Harald and Adriaan already mentioned I spent this weekend with some 'testing guys' in Hamburg to evaluate the possibilities of automated usability/accessibility testing. Also from my side the meeting was a big success, and not only because of the food and the gluehwein, or because I finally saw the countdown+text pedestrian lights in real life ;-)


[image:1674 align=center width=400 class=showonplanet]

It was great to see how quickly the froglogic people implemented some basic checks of the menu structure, and how easy it can be to apply the HI guidelines by that means. 

It's especially important as I know the KDE developers will have a hard time reading the guidelines (even if still a draft and far from being complete, the guidelines already span <a href="http://test.openusability.org/wiki_ou/index.php/Main_Page">quite a number of pages</a>). Going through all dialogs of an application, keeping the rules in mind and evaluating if they are implemented properly is an even more annoying task - and it is implausible to expect that from most Open Source developers. 

That's why I am really happy we will soon have those easy, automated checks :) 

and we even thought of a way to make looking at the test results interesting for you. Stay tuned!

<!--break-->
