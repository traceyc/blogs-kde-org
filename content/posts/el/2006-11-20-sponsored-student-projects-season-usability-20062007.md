---
title:   "Sponsored Student Projects: Season of Usability 2006/2007"
date:    2006-11-20
authors:
  - el
slug:    sponsored-student-projects-season-usability-20062007
---
<small>After a few weeks of hunting for mentors and projects we proudly declare the Season of Usability opened:

<b>Season of Usability 2006/2007 - Call for Student Application</b>

Season of Usability 2006/2007 is a series of sponsored student projects to encourage students of usability, user-interface design, and interaction design to get involved with Freee/Libre/Open-Source Software (FLOSS) projects.

FLOSS offers an excellent way to gain experience in the interdisciplinary and collaborative development of user interface solutions in international software projects. During a three-month cooperation, you will closely work together with experienced professionals and get insights in to their way of work. 

Each Season of Usability project has challenging goals and is mentored by an experienced usability specialist. The projects represent different fields of application, such as media players, document viewers, VoIP, printing, layout or blogging.

Browse the openings online: <a href="http://www.openusability.org/season">www.openusability.org/season</a>

Depending on your location, you will be invited to a meeting with the development and usability team. Otherwise the collaboration will take place via the established channels of OSS development - email, IRC, VoIP, and etc. 

Season of Usability projects are sponsored with $700USD. An involvement of 15 hours per week is expected. 

If you are a student of usability, user-interface design, and interaction design or related subjects send us your application for one of the project openings:

Contact: <a href="mailto:students at openusability org">students at openusability org</a>

Application Deadline: 2006 December, 15

Project Timeframe: 2007 January - March/April

Qualification: All projects require experience in interface and interaction design and/or usability analysis methods. However, this is meant to be a learning experience, so don't worry as long as you understand the basics in design and usability.

Further information: <a href="http://www.openusability.org/season">www.openusability.org/season</a>


Season of Usability is a joint effort of <a href="http://www.openusability.org">OpenUsability</a>, <a href="http://www.flossusability.org">FLOSS Usability</a> and <a href="http://www.aspirationtech.org">Aspiration</a> and the <a href="http://www.soros.org/initiatives/information">Open Society Institute</a>.


<!--break-->
