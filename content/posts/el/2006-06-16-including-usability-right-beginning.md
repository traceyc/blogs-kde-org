---
title:   "Including usability right from the beginning"
date:    2006-06-16
authors:
  - el
slug:    including-usability-right-beginning
---
Recently, I remembered this button:

[image:2103 width=200 class=showonplanet align=center]

Peter from the KOffice team seems to know what it is about. When he started to rewrite Kivio, the KDE flowcharting application, he immediately informed the usability group and asked for input regarding the user interface. Even before the first line of code was written - and ergo nothing could be tested. This was a good decision out of two reasons:

<ul>
<li>The usability people are notoriously understaffed and it takes a while till they accomplish what they promise, :-|</li>
<li> and designing a good UI means including user requirements <i>right from the beginning</i>.</li>

</ul>

In Kivio's case, Peter planned to add new features like advanced colour management, alpha gradients and blending. But is that what the users really need? Are there other things they miss? Is something terribly disturbing about Kivio, and what is really great? Who are the Kivio users after all, and what do they want to do with the application?

To prioritise features and functionality, it is important to answer those questions <i>before starting to program</i>. Relying on bug reports and wish lists is one way, but often not enough as only a small proportion of users reports feedback there. If your application addresses users outside the community, you need other means to learn what they expect from your tool.

For Kivio, Tina and me probed different ways to learn this. First, we sketched the space of <i>possible use cases</i>. It ranged from very casual usage of a diagramming tool up to professional usage by a graphic designer. We derived basic requirements for each of the use cases - probably you can imagine that a graphic designer has much different needs and expectations than an elder secretary. Together with Peter, we limited the scope to support (a) the current Kivio user base (whoever that is), (b) new users who need to create diagrams casually to frequently, but without the claims of a perfect graphic design.

Based on this assumption, Tina created a survey which aims at learning more about <i>current Kivio users (a)</i> as well as <i>potential Kivio users (b)</i>. 


<b>If you feel you belong to either of these groups, please support us by answering the survey! It will be online till July, 1st: </b>

<a href="http://test.openusability.org/kivio/survey">[ Click here to participate in the Survey ]</a>

With the use cases we have collected so far, Tina and me did <i>competitive evaluations</i> with other flowcharting applications. By this we learned about <i>best practices</i> and got quite a good idea of the <i>problems</i> we will be faced. Once we got the survey feedback and backed up our use case collection, we will start to make <i>sketches</i> of the future ui design of Kivio. Together with Peter, we will improve the design in an iterative process (see graph), both along <i>paper prototypes</i> and the actual <i>implementation</i>. This will require usability testing, but even more methods where users are not directly involved.

[image:2104 width=450 class=showonplanet align=center]

It is not always possible to integrate usability right from the beginning. The majority of KDE applications is running stable, and a complete redesign is beyond the scope. Still, the above iterative process can be applied: Once in a time, address your use base and ask them how they get along with your application. Ask them, what tasks they try to accomplish and what goals they have. Maybe, this will differ much from what you expected or what you read in the bug reports as you can address less technical people also. 

Even if notoriously understaffed, the usability group is there to give you a hand. We got a new survey tool (<a href="http://sourceforge.net/projects/uccass/">UCCASS</a>), so setting up questionnaries is becoming more and more painless for us =) Just ping us on IRC (#kde-usability).


<!--break-->