---
title:   "Season of Usability 2006/2007 - Application period ends in 1 week"
date:    2006-12-08
authors:
  - el
slug:    season-usability-20062007-application-period-ends-1-week
---
<i>To all students of usability, ui design, interaction design or related: </i>

Three weeks ago we announced the <a href="http://www.openusability.org/season">Season of Usability</a>, a series of sponsored student projects. We want to inform you that the application period ends in one week, on <b>December 15th</b>. So if you are interested to participate, <a href="mailto:students at openusability org">send us your application <i>now</i></a> - including a short cv/resume and a few paragraphs about your prior experience with regard to usability.

If you didn't have the time to apply this time: There is a variety of different OS software projects registered on OpenUsability, waiting for usability advice. Whenever you feel like you'd like to train your skills in real-life projects, contact me or browse the <a href="http://www.openusability.org/openings">open projects on OpenUsability</a>.



<!--break-->