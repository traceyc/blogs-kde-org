---
title:   "KDE HIG: Dialogs"
date:    2007-03-27
authors:
  - el
slug:    kde-hig-dialogs
---
Finally, the dialog section in the HIG has made some progress. Thank to Olaf who knows Qt Designer much better than I do, we found a way to create a clean dialog layout for KDE4. The trick is to use the same ratio of spacers on bottom of each group box. Read more in the <a href="http://wiki.openusability.org/guidelines/index.php/Design_and_Layout:Layout:Dialogs">guidelines</a>! 

We are planning to provide some video tutorials how to best use Designer - stay tuned. We'll also upload the ui files when we found a good location (the wiki allows only images).

I appreciate feedback on this, as well as on the other guidelines drafted on this page: "General" and "Modality". Please comment here or write a mail to ellen kde org.

<!--break-->