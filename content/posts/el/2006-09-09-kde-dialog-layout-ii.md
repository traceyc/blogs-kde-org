---
title:   "KDE Dialog Layout II"
date:    2006-09-09
authors:
  - el
slug:    kde-dialog-layout-ii
---
Summarising the comments on the previous blog entry and my own consideration:


<b>Top-to-Bottom</b> – Users do not want to think about the proper sequence of options. The layout should therefore support the workflow. Top-to-bottom is mostly perceived to fulfill this requirement. 


<b>Screen is broader than long</b> – a strict top-to-bottom approach wastes screen space as each element is presented in a new row. Given the number of options in current KDE dialogs and the fact that screens are broader than long, it still makes sense to put related options into a row where appropriate. This should not result in a grid layout – it must be clear that the reading direction is row-wise, not columns. 


<b>Indenting is required</b> – We have to decide between indenting or stretching user interface elements. Otherwise normal-sized input widgets will look lost on a broad page. Stretching is something we want to avoid as the relation between label and input element gets lost in some cases (see example below).

[image:2339 width=600 class=showonplanet align=center]



<b>Visual indicators support the recognition of relations</b> – for strongly indented or centered layouts, some kind of visual indicator is required that separates group. This may be space, lines or group boxes, or etc. Which ones do you prefer?

[image:2340 width=600 class=showonplanet align=center]
[image:2341 width=600 class=showonplanet align=center]
[image:2342 width=600 class=showonplanet align=center]



<b>Variable indenting</b> – neither a purely centered nor a design with a fixed indent is optimal. A large fixed indent unnecessarily increases the width of a dialog, while a small one makes the right of the dialog look empty (as many KDE dialogs are extremely broad atm). In a centered design, it may happen that the distance between left-aligned group label and options grows too high. 


[image:2343 width=600 class=showonplanet align=center]
[image:2344 width=450 class=showonplanet align=center]



If we could make sure that the width of dialogs does not become larger than, let's say, 600 px in "normal" font sizes, then the indenting would be much less problematic!



<i>Note: This is not the final layout. I'm looking forward to further ideas and suggestions.</i>