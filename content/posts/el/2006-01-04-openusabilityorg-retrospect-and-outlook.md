---
title:   "OpenUsability.org: Retrospect and Outlook"
date:    2006-01-04
authors:
  - el
slug:    openusabilityorg-retrospect-and-outlook
---
2005 is over - and it was a very eventful year for <a href="http://www.openusability.org">OpenUsability.org</a>: We had our first booth at the German LinuxTag, visited numerous developer conferences and Linux days to talk about usability and to provide live usability support. Also, we haven't frightened the KDE developers too much so they once more invited us to aKademy ;-) In September, we started our first regular regional OpenUsability Get-Together in Berlin, and are close to getting an e.V. 

We were most encouraged by the growing popularity of usability: Since the beginning of 2005, exactly 100 OSS projects have registered on OpenUsability, asking for usability advice. The number of members has tripled to almost 900 members now.

This shows us that there is a huge demand for professional usability support in the Open Source community - a cultural shift only few people would have expected 5 years ago. It is hard to express how happy we are about that :)
 
However, now, this cultural shift needs to be further extended, namely to the usability community! Unfortunately, we are still too few active usability people to support such a high number of projects.

One of our major goals for 2006 is therefore to get more usability experts involved by addressing universities (Professor Wandke from <a href="http://hupsy03.psychologie.hu-berlin.de/ingpsy/index_en.html">Humboldt University  Berlin</a> has shown interest - maybe a good example to be copied?!), and by visiting usability conferences, something we neglected in the past two years.

A second, important goal is to finally set up a better infrastructure on OpenUsability.org, provide more usability-related informational resources, better organise our workflows and resources, etc etc etc.

Uffz, usually I don't like New Year's pledges.... ;-)


 <!--break-->