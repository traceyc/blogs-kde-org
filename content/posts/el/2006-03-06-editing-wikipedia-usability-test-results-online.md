---
title:   "Editing Wikipedia: Usability Test Results Online"
date:    2006-03-06
authors:
  - el
slug:    editing-wikipedia-usability-test-results-online
---
I finally managed to summarise the Test results from the usability tests with the German Wikipedia (at least most of them - three sections are still missing):

You can either <a href="http://openusability.org/download.php/89/germanwikipedia_usabilitytest_edit.pdf">download the PDF</a>, or you can edit and comment the results yourself <a href="http://de.wikipedia.org/wiki/Wikipedia:WikiProjekt_Usability/Test_Februar_2006">directly on Wikipedia</a>  <small>!Upadet link! (after several steps the final URL now)</small>

The goal of the test was to learn about problems that arise when first using the Wikipedia Edit function, to inspect the given help, and to understand the users' expectations. Based on the results, suggestions how to facilitate the start for first-time authors are given in the report. There were a lot of interesting findings, such as lacking of direct help, editing of tables, placing of the edit links, and much more. Too many to list them here - see the report for further information ;-)

<!--break-->