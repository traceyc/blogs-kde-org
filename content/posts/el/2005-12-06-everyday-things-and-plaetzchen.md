---
title:   "\"Everyday Things\" and Plaetzchen ..."
date:    2005-12-06
authors:
  - el
slug:    everyday-things-and-plaetzchen
---
...  at the Berlin Open Software Usability Meeting, tomorrow evening, Wed, Dec. 7th, 20.00 h, at the Chaos Computer Club in Berlin, Marienstr. 11.


Back from my <a href="http://www.flickr.com/photos/el-in-mexico">wonderful vacation</a> we decided to have a less serious usability session tomorrow, and inspect several everyday things such as cashpoints, metro maps, usb sticks or coffee machines. Along with it, we'll have some Plaetzchen, and possibly Gluehwein *yam*

If you are around, you're welcome to join and show us <b>your</b> especially well or bad designed everyday things :-)

<!--break-->