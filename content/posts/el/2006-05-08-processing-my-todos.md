---
title:   "processing my todos..."
date:    2006-05-08
authors:
  - el
slug:    processing-my-todos
---
I sometimes wonder if my list of todos could grow any longer... Is it because I can't say no, because I'm interested in too many things, or because daily work too often keeps me away from my "fun" projects? Probably a mix of all =)

Even if last week's LinuxTag sure added more todo's, it much helped to solve others that were stuck during the last two months:

<ul>

<li> Talking with Sebas, Eva, Cornelius, Tina, Florian and Peter about goals and directions for KDE 4: We'll now create <a href="http://tina-t.blogspot.com/2005/06/personas-part-1.html">Personas</a> to provide a more visual and vivid base for the KDE 4 usability work.</li><br>

<li> Agreeing with Jonathan to work together on Kubuntu, providing a playground for the above KDE 4 goals, and as an option to <a href="http://openusability.org/download.php/95/OU_integration-KDE_linuxtag.pdf">integrate usability with the development process</a>. </li><br>

<li> Finding interested software projects and possible support for OpenUsability's <a href="http://blogs.kde.org/node/1772">"Summer of Usability"</a> idea.</li><br>

<li> Meeting the <a href="http://www.linaccess.org">linaccess</a> people and Mathias Huber from the LinuxMagazin to finally get the results from our <a href="http://blogs.kde.org/node/1823">Accessibility meets Usability meeting</a> published.</li><br>

<li> <a href="http://blogs.kde.org/node/1906">Printing</a>, unfortunately, made little progress - even if Kurt and Till were also at LinuxTag... </li><br>

<li> And finally: Making the OpenUsability booth once more the most cosiest place at the whole LinuxTag made everybody happy =) </li><br>

</ul>

<br>
[image:1991 align=center width=600 class=showonplanet]


<!--break-->"

