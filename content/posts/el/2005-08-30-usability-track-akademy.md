---
title:   "Usability Track @ akademy"
date:    2005-08-30
authors:
  - el
slug:    usability-track-akademy
---
Now that the whole usability crowd has arrived (Tina, Florian, Celeste, Thomas, Aaron, Jan and me) we proudly present the final schedule of our usability track :)
<br>
<b>Thursday, September 1st:</b>

15.00-16.00
<a href="http://conference2005.kde.org/cfp/marathon/collaboration.usability.php">Report on the collaboration between usability and developers (What happened last year?)</a>

16.00-19.00
<a href="http://conference2005.kde.org/cfp/marathon/bof.human.interfaces.php">BoF KDE Human Interface Guidelines</a>

<br>
<b>Friday, September 2nd:</b>

11.00-13.00
<a href="http://conference2005.kde.org/cfp/marathon/tina.and.florian-personas.php">Exercise Usability Methods: Personas</a>

15.00-16.00
<a href="http://conference2005.kde.org/cfp/marathon/live.usability.tests.php">Live Usability Test</a>


16.00-17.00
<a href="http://conference2005.kde.org/cfp/marathon/research.kmenu.php">Research-based Alternatives to the KMenu</a>

17.00-19.00
<a href="http://conference2005.kde.org/cfp/marathon/tina.and.florian-paper.proto.php">Exercise Usability Methods: Paper Prototyping</a>

<!--break-->