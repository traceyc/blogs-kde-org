---
title:   "Berlin Open Software Usability Get-Together"
date:    2005-06-13
authors:
  - el
slug:    berlin-open-software-usability-get-together
---
On Wednesday, we will have our first <b>Open Software Usability Get-Together</b> at the Chaos Computer Club in Berlin.

<b>Rationale:</b> There are many OS project members in Berlin, wishing for usability advice. And there is a strong usability scene who might want to contribute to OS projects. So why not starting a monthly get-together to introduce them to each other and encourage a long-term collaboration :-)

Today, I sent an invitation to the Berlin Usability list, and announced it on <a href="http://www.openusability.org/mailman/listinfo/berlin">berlin at openusability org</a>, the mailing list for these events. From KDE, Stephan Binner will attend - if more of you living around the area are interested to come, sign in to <a href="http://www.openusability.org/mailman/listinfo/berlin">berlin at openusability org</a> or contact me.
Topic this week will be "What is usability, how does it work, and why is it important for Open Software?". As examples for fruitful collaboration, Jan and me will show some KDE applications we've helped out during the last weeks. 

Oh, and after a long time, I've added a new chapter to the human interface guidelines: <a href="http://www.kde.org/areas/guidelines/html/widgets_status-bar.html">Status Bars</a>. I'd be glad to read comments, or to find volunteers to add other chapters and implementation hints :-)



<!--break-->



