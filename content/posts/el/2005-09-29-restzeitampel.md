---
title:   "\"Restzeitampel\""
date:    2005-09-29
authors:
  - el
slug:    restzeitampel
---
... they did it! Yesterday, the first "Restzeitampel" (remaining time traffic lights) was <a href="http://www1.ndr.de/ndr_pages_std/0,2570,OID1837154,00.html">put into operation in Hamburg, Germany</a>.

Typically German, they outmatched the approach I <a href="http://blogs.kde.org/node/1425"> described a few weeks ago</a> by adding the hint <b>"Green in [seconds]"</b> . Strange enough, they write <b>Green</b> in <b>red</b>!

I wonder if they ever heard of the <a href="http://en.wikipedia.org/wiki/Stroop_effect">Stroop effect</a>... Good luck, Hamburg drivers!

<!--break-->