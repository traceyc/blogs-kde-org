---
title:   "HIG: Keyboard Shortcuts"
date:    2007-04-18
authors:
  - el
slug:    hig-keyboard-shortcuts
---
As the result of a long discussion on kde-core-devel, I summarised the suggested application and system shortcuts for KDE4:

<ul>
<li><a href="http://wiki.openusability.org/guidelines/index.php/Appendices:Keyboard_Shortcuts">Standard Keyboard Shortcuts</a></li>
<li><a href="http://wiki.openusability.org/guidelines/index.php/Design_and_Layout:Controls:Shortcuts">About Shortcuts and Accelerators</a></li>
</li>
</ul>

<small>[The table isn't formatted yet - are there volunteers to make it look like <a href="http://wiki.openusability.org/guidelines/index.php/Design_and_Layout:Visual_Design:Cursors">these ones</a>? I really hate tables in MediaWiki...]</small>

The suggested shortcuts are a summary of the discussion on kde-core-devel. The alternate shortcuts we originally suggested are left out as far as possible, but we kept the shortcuts which are required to obtain full keyboard access.

One more question to the community: The KDE3 default scheme reserves several shortcuts for text completion - I wonder if they still need to be reserved, or if we should free them for applications?


Apart from shortcuts, the HIG made some progress in the following sections:

<ul>
<li>Feedback</li>
<li>Warning Messages</li>
<li>Dockers</li>
<li>Drag and Drop</li>
<li>Links</li>
<li>Window Layout</li>
</ul>

All accessible via the <a href="http://wiki.openusability.org/">front page</a>. Feedback appreciated (here or to ellen kde org).


<!--break-->
