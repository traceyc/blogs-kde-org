---
title:   "Tenor @ the Berlin Open Software Usability Meeting"
date:    2005-08-15
authors:
  - el
slug:    tenor-berlin-open-software-usability-meeting
---
As <a href="http://blogs.kde.org/node/1151">posted before</a>, we have regular Open Software Usability meetings in Berlin. This week, we'll talk about a future frontend for browsing with <a href="http://dot.kde.org/1113428593/">Tenor</a>, KDE's contextual linking engine. <a href="http://developer.kde.org/~wheeler/">Scott</a> will be there to provide us with information on Tenor's background and the data structure.

If you are around (and know reading German), you are welcome to join us:

[...]
<!--break-->

Wann? -> Mittwoch, 17.8., 20.00 Uhr
Wo? ->   relevantive
         Zehdenicker Str. 21, HH, 1.OG
         Berlin (Mitte)

Worum gehts genau?

Am besten anhand eines Beispiels: Stell dir vor, du brauchst dringend
ein PDF ueber data retension, das dir vor einigen Wochen von einem
Freund zugeschickt wurde. Die EMail selbst hast du geloescht, nachdem du
das Dokument abgespeichert hast. Nur wo hast du es abgelegt?? Auf einem
Fileserver, lokal, unter einem bestimmten Themen-Ordner oder in temp?
Statt alle Ordner einzeln zu durchsuchen (der Titel des Dokumentes ist
dir bloederweise entfallen), suchst du anhand der dir verfuegbaren
Kontextinformationen: Dem Namen des Freundes, per Email angekommen, PDF,
data retension. Und voila, da ist es :)

Das Konzept eines hierarchischen Filesystems wird angesichts der
zunehmenden Datenflut mehr und mehr inadaequat. Abhilfe sollen Projekte
wie Google Desktop Search, Apple's Spotlight, Gnome's Beagle oder KDE's
Tenor schafffen, naemlich Content- und Kontext-basiertes Speichern,
Abrufen und Bearbeiten von Information, einschraenkende Suche und Browsen.

Waehrend die Anforderung an die Datenstruktur - also welche
Kontextvariablen sinnvoll und brauchbar sind - recht gut geklaert ist,
fehlt ein systematisches Repertoire an Use Cases, durch die die
Anforderungen an das Frontend spezifiziert werden. In unserem Treffen
werden wir uns dieser Frage zuwenden, Ansatzpunkte definieren und
schliesslich Use Cases identifizieren.



