---
title:   "HIG, LinuxTag and summertime"
date:    2005-06-20
authors:
  - el
slug:    hig-linuxtag-and-summertime
---
After a relaxing and internet-free weekend at a lake (it's finally summer up here in Berlin!!), I've finished another chapter of the HIG: <a href="http://www.kde.org/areas/guidelines/html/windows_document-interfaces.html">Document Interfaces (MDI/SDI/TDI)</a>. Screenshots and examples are still missing, but any comments and ideas regarding the contents are highly welcome. Note that you can click the 'Click to show rationale' links to see details.

Now, we are busy preparing the OpenUsability booth for the German LinuxTag starting on Wednesday. There will be some live user tests with Kopete and Kontact, we'll provide usability support, and - as usability is about making people happy - offer some comfortable sofas and hot beverages. <a href="http://tina-t.blogspot.com/">Tina</a> and <a href="http://holehan.blogspot.com/">holehan</a> have created an awesome <a href="http://www.uni-koblenz.de/~soma/linuxtag/promo/poster_coffee.pdf">poster</a> and <a href="http://www.uni-koblenz.de/~soma/linuxtag/promo/flyer_a5.pdf">flyers</a> to make sure people will find us :-) 

Meet us at Booth X27!

<!--break-->