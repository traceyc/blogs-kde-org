---
title:   "\"Usability is a technical problem we can solve on our own\""
date:    2006-03-19
authors:
  - el
slug:    usability-technical-problem-we-can-solve-our-own
---
A common solution I hear in OSS projects to learn about relevant features is to track the users' interactions with the software: The menu items they click, the settings they check, the applications they use. Implementing a tool that is able to track such data seems to be a widespread idea among Open Source developers. The rationale is usually to provide usability experts (like me) with data that describes the actions and behaviours of users. 

A noble gesture - but of limited use for us. Such a tool is an attempt to solve a usability problem technically. But usability is not about technics, <i>it is about humans</i>. The human's interaction with a computer.

In order to understand the human's behaviour, we need to know about their <i>context of use</i>: Their goals, their tasks, their expectations and prior knowledge. Without that information, it is impossible to interprete data gathered by an action tracker. How can we know why users did not make use of templates when writing a document - didn't they find it, didn't they understand it, or wasn't it necessary in the scope of the task they were performing, e.g. making some quick notes? Did the users achieve their goals, or did they abort the task? The same accounts for settings: Did users click an option by accident or intentionally? Did they not configure something because it was irrelevant or because they did not understand it? And how do we know which kind of users sent in their answers? In the worst case it's only members of the community themselves, and the benefits are minimal.

Context-less usage data will return some low peaks, some high peaks, and some sequences of actions. Without any further information, it is of limited use, and learning about priorities and relevancy is impossible. 

As a late response to Frans Englich's article on NewsForge Frans Englich's article on NewsForge <a href="http://www.newsforge.com/article.pl?sid=04/07/07/1640244">Usability is a technical problem we can solve on our own</a> in July 2004 I hereby emphasise:
 
Usability is more than a technical problem. It requires extensive knowledge about the target users and the way they think.
 
<i>Don't underestimate the complexity of the human mind!</i>

<!--break-->
