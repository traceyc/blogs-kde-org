---
title:   "User Surveys to identify users' goals and tasks"
date:    2005-08-14
authors:
  - el
slug:    user-surveys-identify-users-goals-and-tasks
---
Last week, I came across a <a href="http://expressions.sourceforge.net/survey/">user survey</a> on blog clients. The aim of the study is to identify and understand the users' goals and tasks, tools and features they currently use for blogging as well as problems and changes they would like to see in their clients. The special thing about it: The developers ask all these questions before actually starting to program, namely in the planning phase. The project I am talking about has just registered on OpenUsability, the desktop and handheld blogging software <a href="http://expressions.sourceforge.net/">"Expressions"</a> (OSS, of course). 

Wow! This is exactly what a user centered design process should be like! Please support them by <a href="http://expressions.sourceforge.net/survey/">filling in their questionnaire</a> :)

<br>
<b>But what exactly is a user-centered design process?</b>

The user centered design (UCD) process aims at creating a software that supports the users' needs. Therefore, during the whole development process, different usability methods are applied to ensure the current state still meets the users' expectations and wishes. 

There are different models of user centered design process, e.g. the <a href="http://www-306.ibm.com/ibm/easy/eou_ext.nsf/publish/19">IBM User-Centered Design Process</a> or the <a href="http://www.grc.nasa.gov/WWW/usability/processcss.html">NASA User-Centered Design Process</a>. Basically they all perform typical usability methods at each project phase to ensure that a usable product is developed. 

<ol>
<li>Requirements Phase: User and Task Analysis

If the software should be used and understood by your users, you have to give them what they want and speak their language. Without knowing who your users are and what tasks they need to work on, it will be hard to accomplish this requirement.

Typical usability methods:
User surveys on interest in certain tasks, listing and prioritising needs, listing and prioritising tasks and features, observing users while accomplishing their tasks, creating use-case scenarios
</li>

<li>Design Phase: Competitive Evaluation, Walk-Throughs and Prototyping

Before implementing the software, a detailed concept is created to avoid pitfalls and significant changes in the UI architecture later in the development process. A good thing before starting is to check the problems of related applications by 'walking through' them alongside your use-case scenarios. Once the own concept is outlined, you can repeat those walk throughs for your own concept or basic prototypes.

Typical usability methods:
Competitive evaluation, cognitive walk-throughs, paper prototyping, usability testing of prototypes

</li>

<li>Development Phase: Iterative Usability Testing, Evaluations

During the implementation, the user interfaces have to be checked for consistency with existing guidelines. To make sure that the sofware is actually tangible for the users, several usability tests should be performed as the modules are developed.

Typical usability methods:
Guidelines consistency check, usability testing, evaluations.
</li>

<li>After-Release Phase: User Feedback 

When the software is finally adopted by real world users, it is important to analyse their feedback, the problems they were faced and the features they liked. This knowledge should influence the future releases of the software.

Typical usability methods:
Analysis of bug reports, feedback boards or forums, user surveys, listing of strengths and weaknesses, usability testing

</li></ol>
<br>
<b>But what if we are already in the development phase?</b>

The problem with this ideal user centered design process is that it seems to apply to software only which is designed from scratch. But what about all the existing KDE applications? Is it too late to start UCD for them?

Fortunately, it is not :) The whole UCD process is iterative, meaning that for each version, the presumed user groups, their tasks and expectations should be verified. Depending on the results, user requirements and concepts should be refined and improved by usability methods. If user requirements have not been collected in earlier releases, one may start at any point in the development process.

While the Expressions people did in the requirements phase, we are currently designing a questionnaire to identify and prioritise the tasks and features people are faced when using email, electronic addressbooks and organizers in different settings. We'll present it at aKademy - be curious ;-)


<!--break-->








