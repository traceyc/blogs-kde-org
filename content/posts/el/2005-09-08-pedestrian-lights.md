---
title:   "Pedestrian Lights"
date:    2005-09-08
authors:
  - el
slug:    pedestrian-lights
---
<br>
[image:1424 align=center width=300 class=showonplanet]

<br>

Some of you may have spotted the funny pedestrian lights in Malaga: When jumping to 'Go!', the small green man starts to walk and a countdown tells the pedestrian how much time is left to cross the road. When counting 8 seconds, the man starts to hurry: He runs to get to the other side of the street :)

In my course on Monday, I nominated this pedestrian light as an example of really good usability due to the clear visual feedback. My students taught me differently: In Denmark, those lights were removed because the displays could not be read when the sun was at a low angle. 

What does this tell us? There is no overall usability. What's fine for Spain not necessarily fits the needs of Northern European countries.

<!--break-->