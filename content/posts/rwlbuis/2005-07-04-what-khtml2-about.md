---
title:   "What KHTML2 is about"
date:    2005-07-04
authors:
  - rwlbuis
slug:    what-khtml2-about
---
I realize now my previous blog about khtml2 lacked explanation and introduction, sorry
for this :| The idea is *not* that WildFox and myself can do better than khtml on the html front.
In fact we know more about svg than html. So the idea is *not* to add new functionality to khtml,
but keep the functionality like it is.

So what is it? Basically an experiment to get khtml running on top of kdom, which is an independent
dom library. The cool things about kdom are explained <a href="http://websvn.kde.org/trunk/kdenonbeta/kdom/docs/WHYKDOM.txt?rev=412126&view=auto">here</a>.
<!--break-->
What should be interesting and useful to experiment with in khtml2 is cdf(compound documents),
which would allow xhtml+svg or xhtml+mathml, all in one document, no embed or object tags needed.
So see khtml2 as a testbed for better integration of technologies, both through better dom conformance
and compound document possiblities.

Hopefully this explains things better.
Cheers,

Rob.