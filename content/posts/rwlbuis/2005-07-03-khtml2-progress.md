---
title:   "KHTML2 progress"
date:    2005-07-03
authors:
  - rwlbuis
slug:    khtml2-progress
---
I promised to report on the khtml2 progress, on behalf of WildFox, since he didn't find the time
to report himself, having to go on a well deserved holiday :)
Basically a lot of code is "ported", and Niko started on a lot of RenderObject related code.
Because of this, a lot already shows up in screenshots, like lists, tables, general text, divs. etc.
Have a look at these screenshots (I am not able to put them inline, since most are too big... ) :

http://ktown.kde.org/~wildfox/ktown/khtml2-first-shot.png
http://ktown.kde.org/~wildfox/ktown/khtml2-second-shot.png
http://ktown.kde.org/~wildfox/ktown/khtml2-third-shot.png
http://ktown.kde.org/~wildfox/ktown/khtml2-fourth-shot.png
http://ktown.kde.org/~wildfox/ktown/khtml2-kdeorg.png

Cheers,

Rob.