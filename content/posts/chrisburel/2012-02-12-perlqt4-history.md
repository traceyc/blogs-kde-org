---
title:   "PerlQt4 - A history"
date:    2012-02-12
authors:
  - chrisburel
slug:    perlqt4-history
---
I started my first job out of college in July 2007, at a company in the entertainment industry.  I majored in animation in school, and at the time was still convinced I'd like to get into a professional animation position, so this was a great entry level job for me.  Although, the position had nothing to do with animation.  I was in charge of managing the company's disk and render resources, making sure that we got what we needed out of the render queue each night, and making sure that our disks stayed clean enough that people could keep working.

So, this company has implemented their own versioning system for individual artists' work areas, that is essentially numbered directories of files.  When you make a new version, all your current files get copied to a new directory.  No diffs are done between the files between versions, so it means that if you drop a 10 Mb file into version 1, and version your work area 10 times, that same file is duplicated 10 times on disk, wasting 90 Mb.  Being in charge of disk efficiency, this waste of space quickly came under my radar, and I began to write a tool to identify these wasteful files.

90% of our code at the time was written in Perl, and we had a couple of UIs that had been written using PerlQt3.  Qt4 was at about release 4.2 at that time, and there were no Perl bindings for Qt4.  So I was using PerlQt3 to try to write a UI to find these large files.  It was frustrating, because there'd be things I'd try to do with Qt3 that just were not possible.  But then I'd take a glance at the Qt4 docs, and I'd see that you could do it with Qt4.  If only there was a way I could use the newer version of Qt...

So I started thinking, what if I could just use the existing PerlQt3 code, but point it at the Qt4 library instead of the Qt3 one?  I mean, it couldn't be that hard, right?

All said and done, that's essentially what I did.  Except I couldn't do everything blindly and expect it to work.  I had to learn what the current code did.

So I read perlguts and perlapi and a bunch of other Perl internals documentation.  And I read about the new features in Qt4, and how they were used, so I could write tests to make sure the bindings were working.  And I learned about C/C++, memory management, scoping, make files, the C preprocessor, revision control... a lot for someone who majored in animation at an art school.

About 2 years after I started the project and finally got a release, the PerlQt4 project was installed at my office, and now I get to write apps for them using the bindings I built.