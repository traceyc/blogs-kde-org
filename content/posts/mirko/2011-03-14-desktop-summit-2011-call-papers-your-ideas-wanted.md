---
title:   "Desktop Summit 2011 Call for Papers - your ideas wanted"
date:    2011-03-14
authors:
  - mirko
slug:    desktop-summit-2011-call-papers-your-ideas-wanted
---
KDE and Gnome together again - the Desktop Summit this year will take place in Berlin, Germany, from August 6 to 12, 2011. It will be one of the biggest and most interesting Free Software conferences in 2011, and Berlin is also always worth a visit. You have seen the announcement, and the web site at http://desktopsummit.org, but you might be asking yourself how you can register, where to submit your talk, and how you can help with the preparations. Read on.
<!--break-->
<b>How to attend</b>
To attend the Desktop Summit, simply register at http://desktopsummit.org. Registration is free of charge. You will need to take care of your own travel. Hotel and hostel recommendations are listed on the web site. Please remember that lots of people visit Berlin in August, it is a prime tourist month. Book your room and travel tickets well in advance. To visit Germany, visas are required for citizens of many countries outside of the European Union. It usually takes a while until visa requests are processed, check with your authorities early, and apply in time. If an invitation letter is necessary, contact the Gnome Foundation or KDE e.V., depending on which project you are affiliated with. Travel subsidies are handled separately by the two organizations as well.

<b>How to present</b>
The Call for Papers is already open at https://desktopsummit.org/cfp. It will close March 25th. Presentations are planned to be either 30 or 45 min long. Any topic that is related to Free Software on the desktop is welcome. The desktop is considered to be a rather generic term, if your application is for a mobile phone or netbook, it fits as well.

Keep in mind that a large part of the Desktop Summit visitors will be new to the projects. At last year's Akademy in Tampere (Finland), about half of the attendees joined the conference for the first time. So while presentations about the latest and greatest developments of applications or frameworks are of course interesting, it is just as informative for the audience to present introductions to already established APIs of the Free Software desktop, or even to Free Software development tools.

The Desktop Summit paper committee is also actively soliciting submissions about Free Software in education and in governmental use. Non-technical talks are also welcome, since the projects consist of more than code. Visual design, interoperability, promotion or community related topics - the more variety, the better.

The conference is split into two parts. Saturday, Sunday and Monday (<i>August 6 to 8</i>) will be filled with presentations in multiple tracks (the exact number depends on the number of great talks submitted, so here is your challenge). Tuesday to Friday are called the Open Days, which are made up of work shops or BoFs, and hack time (yay). During the Open Days, about 20 different rooms seating 20 to 200 people are available. A call to register activities for the Open Days will be published later, and the remaining rooms will be made available on an ad-hoc basis. Planning ahead pays here as well.

<b>How to help</b>
Until shortly before the conference, KDE e.V.'s Claudia Rauch and the local team consisting of valiant Berliners and volunteers from the two communities will take care of the preparations. Before, during and after the conference, helping hands are very welcome. If you are interested in helping out, please contact the Desktop Summit team at ds-team@desktopsummit.org. Thanks a lot.

<b>Coding, fun, meet the community - what are you waiting for?</b>
Nobody attends the Desktop Summit for fun, right, so I did not mention that there will be plenty of parties and sight-seeing trips, and that Berlin is one of the coolest places to visit in Europe. Or hottest, since the conference will be in the middle of August. The Desktop Summit is an event prepared by the Free Software community for the community. It needs everybody's help and participation. Register today, submit your talk proposals, and do your part to make this the best Free Software conference in 2011. Thanks a lot! See you in Berlin!

<b>mirko@kde:~$ whoami</b>
http://de.linkedin.com/pub/mirko-boehm/6/49a/113
http://twitter.com/mirkoboehm