---
title:   "Leaving Town (not for good, no worries)"
date:    2006-06-19
authors:
  - mirko
slug:    leaving-town-not-good-no-worries
---
This morning, I went to Tegel Airport by bus. On one hand, this is becoming a routine fast. On the other, it is always enjoyable, because I love the city in the early morning light. The bus (TXL) goes past the Reichstag building and the brand-new Hauptbahnhof (central station). What struck me is the new, open culture when it comes to integrating the government buildings into the city never, a fact that never appeared to me earlier. When I first came to Washington DC, I was amazed at how the Washingtonians played baseball and flew kites right in front of the capitol. I admired the Americans for their openess. Today, there are bars and restaurants at the river Spree located right within sight of the big wigs. How is this for a change in political culture? And yes, you can have a beer outside in the sun and enjoy the view (this is Germany, remember, no brown bags required).

Other than that, I am looking forward to spending next weekend in NYC. New York and Berlin both have very similar appeal to me, being vibrant, diverse cities. And maybe the folks from big apple learn how to make delicious Döner Kebap during the soccer championship, that would be a great success.