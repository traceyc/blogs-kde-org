---
title:   "SuperKaramba and Plasma Packages"
date:    2008-03-03
authors:
  - dipesh
slug:    superkaramba-and-plasma-packages
---
With the great intro provided about <a href="http://aseigo.blogspot.com/2008/03/plasma-packages.html">Plasma Packages</a> to get <i>Apple's Dashboard Widgets</i> running, it follows a screenshot that shows <i>SuperKaramba</i> - yes, it works with legacy *.skz files.

<img src="http://kross.dipe.org/skplasma.jpg" width="856" height="481" />

btw, <a href="http://techbase.kde.org/index.php?title=Development/Tutorials/SuperKaramba#Embed_KHTML_Webbrowser">SuperKaramba example that uses KHTML</a> to display content :)
