---
title:   "The CTRL+M break-me option"
date:    2011-12-08
authors:
  - dipesh
slug:    ctrlm-break-me-option
---
A comment at the <a href="http://blogs.kde.org/node/4512#comment-9234">Calligra Words</a> blog-entry asked about re-adding the CTRL+M "Hide Menubar" option back to Calligra Words (and the KDEPim-applications).

That is a *very* bad idea.

The "Hide Menubar" option, known from many KDE3 applications, is the typical example of a break-me option. The user can hit it by accident and will have a hard time to restore the previous state. More so if you add a 2-letter shortcut like CTRL+M to that option which is easy to hit by mistake and not so easy to discover if you did not know about it before.

Our rather detailed <a href="http://techbase.kde.org/Projects/Usability/HIG/Menu_Bar">KDE Human Interface Guidelines</a> even explicit name the problem:

<i>Don't make the menubar 'hideable', users may not easily be able to make the menubar viewable again</i>

As cool as it may to hide the applications menubar it can be a rather destructive action if done by accident without an easy way to undo and restore how it was before.

Ask yourself why you like to add such an action to your application? If the target is to just remove the menubar from all the applications cause you do not need or like them then why should that be done for every single application manually? Why should there not be a global way that allows to disable (or enable) them all in one go? Maybe even with a possibility to display the applications menubar somewhere else outside of the application like in the Panel (OSX-like) or only on hover or only with a certain shortcut like CTRL+M... This is what widget-styles like QtCurve or Bespin allow already today. If your preferred style (e.g. Oxygen) does not allow that yet, then you can still create a bug-report for that or provide a patch. In any case it will work out-of-the-box for all our applications and does not require you do add and later enable such an option at every single application.
