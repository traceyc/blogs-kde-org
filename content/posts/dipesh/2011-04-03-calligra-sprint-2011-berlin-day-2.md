---
title:   "Calligra Sprint 2011 in Berlin, day 2"
date:    2011-04-03
authors:
  - dipesh
slug:    calligra-sprint-2011-berlin-day-2
---
After <a href="http://blog.cberger.net/2011/04/01/calligra-sprint-spring-2011-day-0/">day 0</a> on friday which I missed and <a href="http://blog.cberger.net/2011/04/02/calligra-sprint-spring-2011-day-1/">day 1</a> on saturday where I joined the Calligra 2011 sprint in Berlin in the evening after a longer <a href="http://blogs.kde.org/node/4402">journay</a> the last day of the sprint, day 2, is coming to and end.

Today Casper presented his work done on refactoring the layouting and rendering code in the Calligra textshape which is used in Calligra Words, Stage, the Chartshape and other Calligra applications and plugins to display, edit and save richtext. The work he did is impressing. We are finally moving away from the spaghetti-mess it was to a well structure rendering-enginee. The amount of needed code drastically decreased, the layout-speed improved by some factors and we are finally able to implement lot of the missing functionality like nested tables.

Within next days I will work on porting Calligra Words to the new textlayout-library, then we will work on regression-testing, getting proper unittests done, problems that may show up solved before merging the work done into Calligra master.

I wish I would have been able to make a picture of the energy that was flowing during the sprint. It feels good to be part of that :-)
