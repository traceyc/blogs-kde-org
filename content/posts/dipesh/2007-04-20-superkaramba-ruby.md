---
title:   "SuperKaramba with Ruby"
date:    2007-04-20
authors:
  - dipesh
slug:    superkaramba-ruby
---
Just some minutes ago the next generation of <a href="http://websvn.kde.org/trunk/KDE/kdeutils/superkaramba/">SuperKaramba</a> got Ruby support. That means, you are able to use Python and Ruby to write your karamba's now.

To try it out, fetch+install latest kdelibs, kdebindings/ruby/krossruby, kdeutils/superkaramba and run the <a href="http://websvn.kde.org/trunk/KDE/kdeutils/superkaramba/examples/rubyclock/clock.rb?view=markup">Ruby clock sample</a> with <i>superkaramba --usekross clock.theme</i>

The <a href="http://netdragon.sourceforge.net/api.html">Scripting API-Reference</a> of SuperKaramba can be used for both backends.
