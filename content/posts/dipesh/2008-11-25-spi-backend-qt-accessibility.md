---
title:   "AT-SPI backend for Qt-Accessibility"
date:    2008-11-25
authors:
  - dipesh
slug:    spi-backend-qt-accessibility
---
For the case you, dear reader, are not subscribed to the KDE-accessibility mailinglist (tststs), we received a rather interesting mail just yesterday; <a href="http://lists.kde.org/?l=kde-accessibility&m=122754243721567&w=2">Project Announce: AT-SPI D-Bus</a>

That project is done by <a href="http://codethink.co.uk/index.php?id=11">codethink</a> and sponsored by Nokia and Mozilla (and Novell seems to be in there somehow too). Result will be (hopefully) also that each Qt- and KDE-application will be able to access optional the same accessibility-infrastructure like e.g. GNOME is using today. For this Qt4 provides the <a href="http://doc.trolltech.com/4.4/accessible.html">Qt Accessibility</a> framework and the best; no additional work at the application-level is needed (well, with some exceptions as usual).

Yay, another good day :-)

p.s. what's going on with <a href="http://www.freedesktop.org">freedesktop.org</a>? Why does it take <b>6 month</b> to provide an svn-account for that project and <a href="http://pim.kde.org/akonadi/">akonadi</a> still waits for one since <b>more then a year</b> now iirc. Not enough admins or are the accounts managed using Active Directory?
