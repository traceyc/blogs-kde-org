---
title:   "Disabling Nepomuk in Akonadi"
date:    2011-07-29
authors:
  - dipesh
slug:    disabling-nepomuk-akonadi
---
When I upgraded my KDE-setup to 4.7.0 I run into the problem that when starting the brand new KMail based upon <a href="http://community.kde.org/KDE_PIM/Akonadi">Akonadi</a> that the Nepomuk-feeder started to index my imap-folders.

The Nepomuk-feeder is used to enable offline fulltext-search. A rather cool feature but the drawback is that it's needed to index all my mails. That can be a rather time-intensive operation if you happen to have a large imap-inbox like me has. In total I've approximated a few millions mails in there. I rarely use fulltext search and especially not on my imap-account. So I was looking for a way to disable that feature and to prevent the Nepomuk-feeder from running through those mails. With the help of a friendly Akonadi-developer I found a way I like to share with you.

<b>Disclaimer:</b> Note that the way is rather hacky and I strongly suggest to not do that if you don't need to. I am sure in near future there will be a better way. Probably even more sensitive defaults that don't try to index all of your mails but only those that are of particular interest (e.g. inbox and send mails).

So, here we go. Start the "akonadiconsole" application e.g. via KRunner (ALT+F2) and add the string "akonadiconsole" in there + press Enter to start the AkonadiConsole. The warning that pops up explaining that it's a developer-tool is meaned so. Be careful what you do with that application!

Once the akonadiconsole started you can remove anything that has "Nepomuk" in it's name from the "Agents" page. In particular this are the agents;

* Nepomuk Calendar Feeder
* Nepomuk Contact Feeder
* Nepomuk EMail Feeder

The last one was the interesting one for me. So, it should be enough to only remove that one to prevent Nepomuk from indexing your E-Mails. The other two agents are for indexing your Calendar (KOrganizer) and your Contacts (KAddressBook). They will not be used if not needed so you don't need to remove them if you either don't use those other PIM-applications or don't have large (I mean really large) amounts of data in there.

That was it already. Maybe it's needed to explicit remove the already running instances (not sure there since I just had to quit KMail and restart again and the Nepomuk-feeder process wasn't running anymore).

p.s. to make this more clear;

This blog-entry does NOT mean that I or anybody else suggest to not use Nepomuk. Nepomuk is a cool, innovative and needed technology! I am sure that this particular scenario will be solved in a better way soon. But till then I had the feeling I need to share this finding (cause I wasn't able to figure it out myself) and point at the great AkonadiConsole-tool. But remember that using that <b>developer-tool</b> is at your own risk. It's NOT an end-user tool! Thanks :-)
