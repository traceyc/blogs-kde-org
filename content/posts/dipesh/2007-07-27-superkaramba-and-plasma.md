---
title:   "SuperKaramba and Plasma"
date:    2007-07-27
authors:
  - dipesh
slug:    superkaramba-and-plasma
---
<a href="http://techbase.kde.org/Development/Tutorials/SuperKaramba">SuperKaramba</a> as Plasma Applet running 4 instances of the <a href="http://www.kde-look.org/content/show.php?content=24626">Aero AIO</a> theme.



Screenshot;
<img width="1024" height="768" src="http://kross.dipe.org/skapplet.png"/>
