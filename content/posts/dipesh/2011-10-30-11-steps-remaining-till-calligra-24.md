---
title:   "11 steps remaining till Calligra 2.4"
date:    2011-10-30
authors:
  - dipesh
slug:    11-steps-remaining-till-calligra-24
---
As Andreas note we are rather close <a href="http://dilfridge.blogspot.com/2011/10/towards-calligra-24.html">towards Calligra 2.4</a> which will be the very first release of the <a href="http://www.calligra-suite.org/">Calligra Suite</a>.

Our pretty cool <a href="http://quality.calligra-suite.org/">Calligra Quality Dashboard</a> lists 11 remaining release-critical blocker bugs that still need to be fixed till then.

Now is the perfect time to give the latest Calligra a try and report all your findings at our <a href="https://bugs.kde.org/enter_bug.cgi">bug-tracker</a>. Probably most interesting are serious data-lose issues on saving to our native OpenDocument formats, crashes and other major problems we can add to our top-priority list for either 2.4 or 2.5 which may finally be a <a href="http://blogs.kde.org/node/4446">silver-state candidate</a>.

On a side-note it is rather interesting that we are in fact releasing 3 different versions of our suite more or less in parallel. The 2.4 desktop-edition mostly known as office suite for the KDE desktop, the <a href="http://www.calligra-suite.org/news/calligra-provides-the-engine-for-nokias-harmattan-office/">Harmattan Office edition</a> shipped together with the N9 and <a href="http://www.shantanutushar.com/content/calligra-now-active">Calligra Active</a>. Following the spirit of the work done on <a href="http://dot.kde.org/2011/10/09/plasma-active-one-released">Plasma Active One</a> Calligra is btw also already <a href="https://build.pub.meego.com/package/show?package=calligra&project=Project%3AKDE%3ADevel">packaged and available</a> for <a href="http://merproject.org/">Mer</a>.

Calligra is far ahead it's competition in this field. Building products based on the community-driven Calligra suite (or Calligra frameworks which would probably match more) is easy and can be done with little resources within very short time. That is a huge achievement we aimed for when starting working on KOffice version 2 lot of years back. An achievement we finally reached and we are able to prove to have reached and that today and now.

Gratulations to everybody involved in making that happen :-)
