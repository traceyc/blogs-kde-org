---
title:   "Let's support OpenXML!"
date:    2006-12-20
authors:
  - dipesh
slug:    lets-support-openxml
---
as someone who likes to spend his free time on developing in the <a href="http://www.koffice.org">KOffice</a> project I am very much interessted in open standards. As we all know, KOffice was the <a href="http://dot.kde.org/1127515635/">first</a> Office-suite that publicly announced support for OpenDocument. So, why not repeat that success-story again with OpenXML? I say it is possible and at following lines I'll show you how easy it is.

[<b>disclaimber: this is meaned to be sarcastic!</b>]

<a href="http://blogs.msdn.com/rick_schaut/archive/2006/12/07/open-xml-converters-for-mac-office.aspx">Rick Schaut of Microsoft</a> provides us with some very good basic informations to allow us to calculate what we actualy need to get the support done. While his calculation was done for the port of the OpenXML-code from the MSOffice/Win32 to the MSOffice/Mac version, I guess we are able to reuse it for us to show how easy it would be. While some readers may now start to cry that I can't compare porting with writting something new from the scratch, let me add, that we don't develop against the Win32API but use the KDE-framework what in turn means, that we are in fact faster with a smaller team. But to be fair let's ignore that fact and just assume the MSOffice Mac-team is as fast with copy+paste+port as our team with writting new code :)

So, Rick provides us a nice intro how big that task is;

<code>Word, alone, has more than 1100 individual XML elements that need to be processed.  We do this processing by writing something called a "handler", and each one of these elements needs a handler.</code>

he then goes on with a more concret calculation;

<code>a team of 5 developers will implement 25 handlers a week, which means that we’d have all the XML handlers written in 44 weeks.</code>

Now it pays out that during my school-time I only sleeped at the english-lessons cause my within that years collected maths-knowledge and kcalc are helping me now to solve that rather complex task;

44 weeks / 4 = 11 months

So, 5 developers need around 1 year to get the thing done. hah! Oh, wait, he continues to say, that

<code>Nevertheless, we’ve taken a little less than a year to get the converters reading the new file format.  We still aren't writing the new file format [...] and I’ve completely left out all of the design and coding for the intermediate representation of the file.</code>

hmmmm... guess next time I should continue to read before I fire up kcalc. Okay, this time I do it better and at the following lines he wrote;

<code>In other words, we're almost halfway through the schedule, with less than a quarter of the development work done.</code>

Oh, 1 year is a quarter of the work. So, 1 year * 4 = 4 years? Wait, he writes more...

<code>This is just for Word.</code>

Ohhh, ok. We also have <a href="http://www.koffice.org/kspread">KSpread</a> and <a href="http://www.koffice.org/kpresenter">KPresenter</a> and... uh, don't let's count the other apps cause I don't like to risk an overflow in kcalc (but if you like, add also apps like <a href="http://www.koffice.org/kchart">KChart</a> or <a href="http://www.koffice.org/kformula">KFormula</a> to the list and also count things like testing, fixing, implement VBA, etc.)...

So, 5 developers need at least 12 years? hmmmm... could please someone who's more fit with maths show me where I made an error in my calculations? I mean, we achived the same for OpenDocument with _much_ less developers in a _much_ shorter time and we don't have 5 full-time developers that are able to develop 12 years on it cause in 12 years the OpenXML 1.0 specs may not be used any more anyway and something like OpenXML 4.0 is state-of-the-art then what means again 12+n years...
