---
title:   "SuperKaramba and Plasma, Part 2"
date:    2007-07-28
authors:
  - dipesh
slug:    superkaramba-and-plasma-part-2
---
After providing last time a <a href="http://kross.dipe.org/skapplet.png">screenshot</a> and a <a href="http://kross.dipe.org/skapplet.mpeg">screencast</a> (2.4MB, mpeg4) of SuperKaramba in action running the Aero AIO theme, here we go with a more concrete sample that connects SuperKaramba and Plasma together.

This Python <a href="http://techbase.kde.org/Development/Tutorials/SuperKaramba#Plasma">sample script</a> does use the Plasma TimeEngine to display the current local time - such kind of clock-examples will never end I guess :)

<!--break-->
<pre>
# Import the needed modules.
import karamba, PlasmaApplet
 
# Fetch the TimeEngine Plasma::DataEngine object.
engine = PlasmaApplet.dataEngine("time")

# We like to update it each second.
engine.setProperty("reportSeconds", True)
 
# Connect with the TimeEngine's "Local" source.
engine.connectSource("Local")
 
# This is the richedit widget we use to display something.
text = None
 
# This method just returns some text to display.
def getText():
    return "%s\n%s" % (PlasmaApplet.name(),engine.query("Local"))
 
# This method got called by SuperKaramba on initialization.
def initWidget(widget):
    global text
    karamba.resizeWidget(widget, 400, 400)
    text = karamba.createText(widget, 10, 10, 380, 380, getText())
    karamba.redrawWidget(widget)
 
# This method got called by SuperKaramba on update.
def widgetUpdated(widget):
    global text
    karamba.changeText(widget, text, getText())
    karamba.redrawWidget(widget)
</pre>
