---
title:   "Calligra Sprint 2011 in Berlin"
date:    2011-04-01
authors:
  - dipesh
slug:    calligra-sprint-2011-berlin
---
This weekend there will be the <a href="http://community.kde.org/Calligra/Meetings/Begin_2011_meeting">Calligra spring 2011 in Berlin</a> in KDAB's office in berlin where I usually work and play kicker and table tennis all of the week. The irony is that I very likely will not make it to the sprint. I was visiting some <a href="http://www.thailand-taucher.net/ueber-uns/">friends in Koh Tao</a> and I am now caught in a heavy storm in Koh Samui since more then a week now. My fly was canceled and the airport was closed, my visa was running out and cash problems raised up. While the later is solved now it's still unclear when and how I will be able to leave that island, head to Bangkok, pay for a new flight back to Berlin and finally be able to hack again. Within last days the streets here got float meters-high with water, the ocean was destroying large parts of the hotels near them and so we had to move from hotel to hotel in higher areas. In total more then 1500 ppl got evacuated from Samui and Tao alone and while the situation is better by every day and we even have power, warm water and Internet again I stay skeptic and wish I could be back again asap. In all those years I am traveling around the world that's the first time I will be damn happy to be home again. And who knows, with a bit of luck I will be able to join tomorrow or day after. Thinking of you, a Calligra hacker caught in heavy water.

Update:
I made it to Bangkok and stay now in the Khao San (the backpackers area) thanks to the great work done by Bangkok Airways and the amazing help provided by the Central Bay Resort as well as the friends I made here and last but not least KDAB. After getting confirmation from Bangkok Airways I went on to Air Berlin to get a fly back home asap. I reached them 4:30pm and they just closed 4pm. Even more worse the imigration central just closed 4:30pm and are only open monday-friday. A friendly offier sayed I can ignore the visa-problems since the airport-personal will know what happened and will provide what I need (a stamp to get out and back). The Air Berlin office will open tomorrow morning and then I will know more and probably on my way back.

Another Update:
I am back in the office now. Everything went smooth thanks to Bangkok Airways and Air Berlin. Yay. Hacking now :-)
