---
title:   "It's been a tough week but..."
date:    2003-09-28
authors:
  - bruggie
slug:    its-been-tough-week
---
... Kompare seems to be in a useful state again atm. I did a lot of hacking on it and moving a lot of code around and i think the code is a bit cleaner now and easier to maintain. <!--break-->

I also fixed some bugs:
<ul>
<li>scrolling in the view (it currently still does not work fully yet but i'll keep on improving it).</li>
<li>navigating with ctrl+up and ctrl+down, it only navigated into half of the differences in a file.</li>
</ul>

Outstanding bugs i will fix before 3.2 is released:
<ul>
<li>connectwidget redrawing too early (the QListViews are drawn in the eventloop) so the listviews are not populated yet</li>
<li>the scrollbars are not properly shown initially, again this is caused by the QListViews</li>
<li>uploading to a remote location after saving it locally</li>
<li>add documentation (high priority)</li>
</ul>

Stuff i need to test (and where you can help by reporting bugs to <a href="http://bugs.kde.org">http://bugs.kde.org</a>)
<ul><li>parsing all kinds of diff output (files with and without an endling newline) ed and rcs wont work atm, they are pretty hard to map to the current diffmodel. If you can supply patches for it that's ok with me too :) Normal does not work because i did not have time to implement it yet after rewriting the parser in the make_it_cool branch</li>
<li>downloading from remote locations</li>
</ul>

Stuff that i want to do after 3.2:
<ul>
<li>Add diff options to the compare dialog so you can choose the diff options you want to use when comparing.</li>
<li>Add inline difference support.</li>
<li>Optimize parsing, on a slow machine it is really slow, even on my dual amd mp 2400+ it takes a while before a large diff is parsed.</li>
<li>Maybe get rid of the qlistview and it's problems by using custom widgets, but that is a big maybe...</li>
<li>Add support for reading files with a different charset than the local settings.</li>
</ul>

Well, you're now up to date again, thanks for reading and please keep providing useful input :)

Update (October 6, 2003):
The first 2 outstanding issues are resolved afaict :)