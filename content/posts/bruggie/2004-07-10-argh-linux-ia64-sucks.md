---
title:   "ARGH, Linux IA64 sucks !"
date:    2004-07-10
authors:
  - bruggie
slug:    argh-linux-ia64-sucks
---
Yesterday at work we found some problem with backtrace on linux ia32 when it was being called from a signal handler. So i said lets see if this works on Itanium or if it is a more genereic Linux problem. Guess what, on Linux IA64 there is no signal generated on a divide by zero, the result is 0 and the program terminates normally... WTF ???

A bit more testing showed that the problem is in gcc. When the code is generated with icc (intel compiler) the signal is caught and a core is dumped. How is this Linux supposed to be ready for the Enterprise when basic things like this dont work ? I tested it on RedHat AS 3.0, SLES8 and SLES9, which means this problem is at least in gcc 3.2.x and gcc 3.3.x. I'll do some more testing with gcc 3.4.1 and 3.5 snapshots to see if anything changed at all and if not, i guess we'll have to dump gcc as compiler :(

Any way enough ranting for today, time to clean the house, my parents will show up in a few hours and stay the night. They are bringing my computers and other stuff i still had in the Netherlands. Later !