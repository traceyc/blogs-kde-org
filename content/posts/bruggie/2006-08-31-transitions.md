---
title:   "Transitions"
date:    2006-08-31
authors:
  - bruggie
slug:    transitions
---
Damn, it has been more than 2 years since I wrote an entry here. I simply have too little time to do anything code-wise for KDE. Last week I subscribed again to some lists to see what is happening in KDE land for 4.0 but I dont have the time to code. I've always had this idea to make a electronic schema/layout editor but if things keep going like they have been going then I'll never get around to it.

In these 2 years I got a steady job at Intel (well for the time being), a very nice and pretty girlfriend (she's a Linux geek and I absolutely adore her :D), a new house and 2 cats. No I will not post pictures of her, I'm with Jason on this.

I think my renewed interest for KDE comes from the fact that I am now working with Windows during the day and I need to have something to compensate for the loss of working with Linux during the day. My main desktop has already switched from Linux to Windows at work, I still have 2 linux desktops under my desk but I use VNC/Putty to connect to these. It is annoying to debug Windows Applications (when not having source) without proper logs but the debugger is so much better than anything under Linux when you actually do have the source. 

Enough for now, have to do some more work today. 