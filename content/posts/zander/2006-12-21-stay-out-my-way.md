---
title:   "Stay out of my way!"
date:    2006-12-21
authors:
  - zander
slug:    stay-out-my-way
---
In KWord1 we had the concept of text-runaround. This is basically the ability of KWord to detect there are other objects on the page and to make sure the text is not printed on top of other content.
In KWord 1 we just allowed runaround of square objects. Which is rather simple. I've been postponing to solve the issue in KWord2 for a couple of months now; being able to rotate text and to have circles, or worse, as objects I have to run text around made me wonder how to even start attacking this problem.

In a casual conversation I noted this problem to <a href="http://zrusin.blogspot.com/">Zack Rusin</a>, who within 5 minutes gave me the solution on how to attack this problem!  He even gave some example code that he typed in his irc command line :)
Now, 2 days and lots of math later I finished writing + debugging the full solution which is integrated in KWord-trunk; with the excellent result of having runaround that's much more powerful then I ever expected it to be.
I feel like a kid when I move a text frame past a circle or other curve and see the text being relayouted in real time to avoid those objects. Can't stop playing with it :)

Thanks fly to Zack for being cool (among other things), and also to Qt4 for making hard stuff a whole lot easier.

<img src="http://www.koffice.org/kword/pics/200612-Text%20runaround.png">

Now; for those that are curious on how I did this; here is a test app; <a href="http://members.home.nl/zander/sw/runaround_paths.cpp">runaround_paths.cpp</a>.<br>
Simply place in a dir own its own; then run <code>qmake -project && qmake && make</code> after which you can  start it.
<!--break-->