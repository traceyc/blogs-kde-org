---
title:   "Eben Moglen on Microsoft’s summer of fear"
date:    2007-05-21
authors:
  - zander
slug:    eben-moglen-microsoft’s-summer-fear
---
I just ran into this video from a couple of weeks ago where Eben very eloquently puts the finger on why the Novell/MS deal is happening and how its bad business for Novell.
<ul>Imagine a party who engages in recurrent threats every summer time for years on end. On a sort of annual Be Very Afraid tour.
</ul>

Video at: http://www.redhat.com/v/ogg/summit07_eben1.ogg

Its also quite fun to watch ;)
Here is a <a href="http://en.wikisource.org/wiki/The_“Be_very_afraid