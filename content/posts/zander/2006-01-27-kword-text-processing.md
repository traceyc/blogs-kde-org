---
title:   "KWord text processing"
date:    2006-01-27
authors:
  - zander
slug:    kword-text-processing
---
I know it sounds silly, but editing text in KWord used to be some sort of a drag; it was annoying to select text with the mouse, for example.
In the upcoming 1.5 release (not all fixes have made the beta) there are a set of changes in how KWord allows you to manipulate text.  Tell your friends and family; this release is going to stun a lot of people!

* Selecting the first character of a sentence (so completely to the left of the text) was rather painful in previous versions as it made you click in a 1 pixel line to actually work.  The beta allows you to click to the left of the frame as well to position the caret[1]. (which kind of worked in 1.4.2, but not really)
In the upcoming version this idea is expanded to the text mode (the mode that does not show pages, just text) and you can click on the left there as well as it shows a small blank space instead of placing the text to the left edge.  The beta still has issues there. I committed that text-view thing just today.

* KWord is a bit nicer in switching out of text-edit mode if you miss click.  Previously if you clicked on a the outline of the textframe your caret[1] dissapeared and you could no longer type until you clicked inside the text again.  Now clicking on the edge will cause the caret to be moved to that spot.

I would love feedback from serious writers, what could be done better and what is pretty good as is.  Please help me make KWord the easiest to use text editor there is.

Oh; another feature request I got from a professional writer (hi Jes :)  is to have an action to join lines.  People that use vim/emacs or Kate will recognize that.  Unfortunately its not possible to add that feature in the 1.5 due to the freeze.  But you can find a patch attached to this feature request: http://bugs.kde.org/show_bug.cgi?id=120577

In other news; I'm currently on a long traveling trip and I'm enjoying the wonderful weather and great people here. Later!

1) we text-editor people call the cursor a caret to distinguish it from the mouse-cursor....
<!--break-->