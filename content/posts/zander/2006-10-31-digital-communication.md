---
title:   "Digital communication"
date:    2006-10-31
authors:
  - zander
slug:    digital-communication
---
The best description I ever had about technical people was this sentence: Technical people find it completely satisfactory to reply to any statement a single sentence: "That's false!".
But this yes/no type of communication was not what I was aiming at with the subject. Its about being able to discuss subjects in the digital realms. Typically email and IRC.

Last week I had a really long talk on IRC talking about implementation strategies (> 3 hours). We ended up not agreeing on any outcome and at least I walked away quite frustrated. Thinking about it over the weekend I found that the talk should have been a discussion, but it didn't follow the principles of one. Believe it or not, there are actually rules for discussions. Any discussion should follow those rules in a rough manner or else they are doomed from the start. So whenever you reply to an email trying to discuss that new feature or trying to point out what someone did wrong, you are engaging in a discussion.

The problem with us technical guys is that the things we are talking about live for the majority in our minds. And showing others our thoughts is hard as, well, they are not expressed easily. That takes practice.

Any discussion begins with one party expressing his or her opinion about something. Be it the usefulness of a new button, or the way you program a solution.
If another party disagrees he can start a discussion by stating his opinion complete with arguments why the others opinion is not the most correct one in his mind. Note the two points that the reply should have. First the counter-opinion and second the arguments why its better. If you leave one of them out, then you are not discussing, you are complaining, which tends to be useless and annoying.

The discussion itself should be aimed at finding common ground and refining the argument and explaining the arguments better. Points of disagreements are locatd and there is basically a repeat of the initial steps. Which is to make explicit where you disagree and state the reasons why, in a way that the other can be persuaded to follow your line of thought and change his opinion.

Being aware of these points makes many discussions a lot more civil and tends to produce better outcomes. For example in the IRC talk I was missing any argumentation about why the other guy found my solution inadequate. Recognizing that and then asking him directly to convince me why he has problems with my ideas is a good tactic to guide the discussion to a satisfying ending. If he can't give any reasons then he has to accept that he will never persuade you to change your position.

Your opinion and your ideas are important, they can improve the open source product. So don't feel afraid to open a dialog. But it better be a well thought out opinion backed by facts that others can look up or you risk being marked as someone who just is complaining. So be sure to practice expressing your thoughts clearly to others. Its essential to work together.<!--break-->