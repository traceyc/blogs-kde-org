---
title:   "Losing faith in technology"
date:    2005-12-20
authors:
  - zander
slug:    losing-faith-technology
---
Here in Holland we have a 'chipknip' (chip-wallet) that is a very cool invention where you basically carry around electronic money and pay using an 'ok' button instead of giving out cash and collecting too many coins.

Its been around for about a decade already and the thing is really secure (I know since I worked with the designs back when I was doing my thesis at one of the companies that exploits them).  Today I found out that the saying that the weakest point makes or breaks the whole chain still makes sense.

I went shopping and at the cash register I payed with the chipknip. Even while i already had inserted my card it said 'insert card' so I thought not much of it and removed and reinserted the card.  The device responded with 'E 9,10  Press OK'.
I pressed OK, saw a "you payed" and went on packing my bag.
At this point I was surprised by the cashier saying that the payment failed, and looking at her display I indeed saw that the cash-register claimed my payment failed.  So I went on like, what?  it said the payment was OK, and I don't want to pay twice!

The devices that interact with your bankpass are always external as they are sold separately and checked by the bank to be tamper proof.  At this point I was suspecting that the cash-register was not interacting with this device properly.

The cashier didn't budge and said that she saw it failed on her display so it must have failed. Hmm, ok, I'll press that OK button again to get my groceries.  So I inserted the pass again and went through the ritual.  This time there was success.

I was getting confused if I might be mistaking at what I saw so I wanted to be sure. I went to the bank (which is 3 doors down from the shop I just came out of) and told them the story and asked if they had a device to see my recent payments.  I had such a device doing my thesis which posts a date and an amount for the last 5 payments made.  Good thing that after a little searching the bank had one as well.
Indeed; the amount of 9,10 Euro was payed twice today.  Great, I'm not loosing my mind!

The lady at the bank was very nice in that I could borrow this device for a couple of minutes and I headed back to the store. After some confusing looks I got my money back. The complaint that this must mean their cash-registers are far from fool proof was mostly ignored though.

I just hate it when things don't fail properly and cause problems.  Failing eloquently really is the best way you can tell software is designed with love and care :)
I'll be sure to wait until the machine says 'insert card' there next time.<!--break-->