---
title:   "Value as a choice"
date:    2006-11-06
authors:
  - zander
slug:    value-choice
---
Reading this funny transcript of bloggers philosophy (<a href="http://aseigo.blogspot.com/2006/10/measuring-value.html">aaron</a>, <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/307-Reacting-to-Aaron.html">ade</a>, <a href="http://tom.acrewoods.net/node/490">telex</a>). It reminded me of a blog that looks at the same problem from a different perspective, and only published days before this thread started.


Rob Weir wrote: <a href="http://www.robweir.com/blog/2006/10/when-language-goes-on-holiday.html">when-language-goes-on-holiday</a>
<ul>Let me set you a problem. I place before you a glass of water. Whether it is half full or half empty I leave to your imagination. What use is this glass of water to you? Certainly you can drink it. Or you could sell it to someone else. Or you could create a derivative option to buy the water, and sell this option to someone else. Or you could pledge the water as collateral for some other purchase. You have several options, several choices. But suppose you are thirsty. Then what do you do with this nice, cold glass of water? If you drink it, then you can no longer sell it, sell options on it, or pledge it. Drinking the water eliminates choice. So better not to drink it. Just let it sit there, on the table. But still you get thirstier and thirstier.</ul>
<!--break-->