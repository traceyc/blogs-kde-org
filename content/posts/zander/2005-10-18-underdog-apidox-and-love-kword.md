---
title:   "The underdog, ApiDox and love for KWord"
date:    2005-10-18
authors:
  - zander
slug:    underdog-apidox-and-love-kword
---
It apparently is pretty hard to write api-dox for your new classes, as is show by recent blogs from <a href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/103-Getting-Defect-Reports.html"> ade</a> and <a href="http://www.valdyas.org/fading/index.cgi/2005/10/18#apidox">Boudewijn</a>.
I fully agree with Boudewijn that it is the least interesting thing to work on, but I have been around long enough to know that dox is just like sex; even having a little is better then nothing at all.  And when its good, its really really good, inviting you to give back as good as you can..

I recently got more time for KOffice again, with KDEPim trailing kdelibs and getting it to compile is too much for me, I needed a change away from KMail.
People that know me longer know that in the time of KOffice1.0 I was the maintainer of KWord and a lot of code in there is mine.  Well, its been a while and I note that KWord was in need of a couple of good refactorings to cleanup cruft and bad design.  I leared a lot since I wrote those things :)

The approach is to write a simple 2-class API of my replacement functionality but just add the methods in the header file. Not do anything in the .cpp file just yet.  Then I start to change all the usages in the whole program to use the new api instead of the old one.
During this time I read code that actually uses the classes and notice how the new structure can be used more efficiently, so I expand my replacement API with new methods. But still no implementation.
After some time it all compiles.  But naturally, it won't link since I have no implementation yet.

At this point I write unit tests; see <a href="http://websvn.kde.org/trunk/koffice/kword/tests/KWPageManagerTester.cpp?rev=471424&view=markup" target="_other">koffice/kword/tests/KWPageManagerTester.cpp</a> for an example.  They define what the API does in the best way possible.  Corner cases are tested and expected values are tested.  If you write unit tests for your classes then API dox are of much lesser concern since you actually already documented the API for a coder, he just has to read the unit-test.  With the huge benefit that running the tests will tell you if the tests or the code needs updating, unlike dox which only ade can tell you are broken.

After writing unit tests and empty methods (to get it to link) I start to implement the functionality, running my unit tests to see more and more tests pass, until they all pass.

I can highly recommend this way of working to anyone that wants to write new classes or methods as its a fast, care free way of working that will give you much more bliss then good dox do since you will benefit from it yourself even more then others will benefit from good dox.
Oh, naturally I did add dox, but I did it the next day, as I feel writing about something needs some distance to not go into needless details. I.e. explain the usage, not the implementation of a method.

This was also the first time I wrote unit-test in C++, after doing it for years in Java. The API is quite simple if you are used to JUnit, but it did bring a smile to my lips :)
<!--break-->