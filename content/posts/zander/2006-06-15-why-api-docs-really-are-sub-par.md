---
title:   "Why api-docs really are sub-par"
date:    2006-06-15
authors:
  - zander
slug:    why-api-docs-really-are-sub-par
---
I recently saw a good saying;
<ul>if I have 4 hours to chop down a tree, I'll spent the first 3 sharpening my ax.</ul>

All KDE hackers I know are pretty bright and the above will sound true to them; having well working tools gets you better results, faster.

KDE continues to have less then optimal API docs, though, and I think thats because they are not seen as a tool to enhance your workflow with.  Many people still read the header files as a way to figure out which methods there are to call.  Optimizing the tools to find the header files instead of optimizing the online api docs.  Makes sense, in a way.

I like my workflow of having a couple of tabs with api docs open; one for each major library.  So I set out to optimize the html version of the tools of the trade.  There is a couple of things I really miss in the docs that KDE produces right now (which are available on developer.kde.org).
<ul>
<li>An overview of all available classes in the whole of the library.  So I don't end up looking through all subprojects.  Is it in kdecore or kdeui, or...?</li>
<li>A page where there is a classname with a 1-line explanation next to it.
I find it useless to have a multi-column alphabetically sorted list. If its too large I hit find anyway.  And with the 'hit slash and type your text' searching in konqueror thats trivial.</li>
<li>Inter project linking. If have some comments in kofficeui that has example code in their docs with a class from kofficecore, I want to be able to click on that class.</li>
<li>Individual pages should be sorted on most used, so public methods at the top of the page. For some reason they end up in the middle of the page on the kde docs</li>
<li>No menu on the top-left that makes me have white space on the left for the rest of the page</li>
<li>Visual enhancements that makes reading it as a reference a lot smoother, which doxygen 1.4.6 or higher does out of th box, we just don't use it</li>
</ul>

I set out to fix those issues because koffice was without any docs since the Makefile.ams were removed from svn and kde's doc guru didn't seem to have time to fix it.  Itch to scratch and all that :)
I ended up with a set of api docs that I think are very usable and a major improvement over the ones that are on developer.kde.org.  It uses the fact that as a user of a library I could not care less in which section the class is; I just want to use it.  So navigation by section is replaced by navigation by class.

When talking to the kde docs guru my comments were shrugged off; he basically told me if it does not fit in the kde layout he doesn't care about my efforts.  Fair enough.  But I did put the script in koffice svn and its generic enough to be used in all modules that want it without any modifications. So I hope that there are other KDE developers willing to give it a try and benefit from the increased usefullness of the docs we type.

It also appears that the docs-script for developer.kde.org are rather anal about forcing developers to learn proper doxygen tags. Like we don't have enough to learn already.  This apparently it not needed at all and doxygen can provide a class description without the developer having to type \brief in front of it, among other clear wins.

So, I hope I made a compelling sales pitch here; better output with less effort.  Don't you want that ? :)
Download the script here:
http://websvn.kde.org/trunk/koffice/doc/api/gendocs.pl?rev=HEAD&view=log

or watch its output here:
http://www.koffice.org/developer/apidocs/
<!--break-->