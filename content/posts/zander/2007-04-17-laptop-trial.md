---
title:   "Laptop trial"
date:    2007-04-17
authors:
  - zander
slug:    laptop-trial
---
Some 18 months ago I decided that I wanted to upgrade from my desktop to a laptop.  The machine was really slow, but the most important thing was that I intended to be able to travel with my main machine.  So, I bought a really cool looking and pretty fast HP laptop.   I didn't want to spent too much on a laptop and this one fitted the bill.
Soon after I found that HP does not equal quality;  the machine needed a bios upgrade, which actually came out some months before I bought it.  But naturally I only found out after installing Linux on it and there is no way to upgrade then.  The power supply has been replaced twice in those 1½ years and now the hinges broke so I use duct tape to keep the screen from separating from the machine.  All in roughly 18 months.   Hint; don't buy HP guys!

Last week I went shopping for a new laptop; I wanted one with a lot better support for Linux.  My HP has an ATI card which already freezes the machine when you relogin X11 a couple of times; a Wifi driver, openGL, card reader would really be good to have support for as well (all present but not supported on my HP).  The answer to those questions was pretty simple; go for Intel chipsets.  Intel is the main company that actually works with the open source people to get drivers running and fixed.
After searching through lots of specs, I had 3 machines to choose from, unfortunately the shop (mediamarkt) I went to had just one of them for sale.  A Fujitsu Siemens laptop.  So I took it home last Saturday and installed Linux (feisty AMD64) on it.  The first impression was that its really really fast :)  The support indeed is outstanding with just one package to install and a reboot later you have full resolution and OpenGL etc.  My Wifi also worked out of the box.
During the installation procedure the CDRom was in heavy use, and the sounds it made didn't make me feel very comfortable, though.  The design seems to be a bit fragile, which probaby means I'm going to get an even shorter lifespan out of this lappy than my former one. 
Other issues included; the design of the key layout doesn't really make sense to me.  I find it funny to see the 'backlight less' right of the 'backlight more' button. The connections were also placed quite annoyingly along the sides with almost no connections on the back.  Must be useful to someone, I guess.
So this didn't give me a lot of confidence.  Then I plugged in the earphones and tried the audio and that was the killer for me.   I could actually hear the HD ticking though the headphones!  (which was quite hard as the noise levels were doing their best to mask the ticking).
So, yesterday I returned the laptop and got my money back.  Hint; don't buy Fujitsu Siemens <b>ever</b> if you require any kind of audio quality.

The ease with which I could return the laptop (even after formatting the HD) at least shows me that the 'paradox of choice' is getting solved nicely. True, you get a lot of choice as a consumer nowadays but you get to try out anything you want, and educate yourself fully if you choose to take the time.

My search for the perfect lappy continues :)<!--break-->