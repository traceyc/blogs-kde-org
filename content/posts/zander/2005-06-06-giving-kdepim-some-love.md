---
title:   "Giving kdepim some love"
date:    2005-06-06
authors:
  - zander
slug:    giving-kdepim-some-love
---
Over a week ago we had the pim meeting in Achtmaal, which was a big success. From my perspective it was mostly because of meeting all those new kdepim people in real life for the first time.

The goal of my visit was to work on usability fixes, as I was getting more and more annoyed with the state of kdepim, developers tend to focus on fixing functionality before fixing GUIs. Yeah, I know; I can hardly blame them.
As always; time was too short and my issues-list probably far too long anyway, so in the last week I have been finishing some projects and yesterday I finally set myself to actually start coding C++ again.  Besides the usual pun (darn what a retarded language, you actually have to set your variables to zero manually for a new class!) it went pretty well.  Just like riding a bike :)

So I'm a little proud to have rewritten one dialog and committed usability fixes to 4 others.

Lets see if I can get my reported-bugs-count below 100 by fixing my own bug-reports first :)
<!--break-->

ps. kdemail.net in combination with kolab work like a charm! Thanks Matt!