---
title:   "Bug of the week"
date:    2006-08-03
authors:
  - zander
slug:    bug-week
---
In KWord the main library is called kwordprivate.  So doing a recompile after changing something inside of kword cmake has this nice gem:
<pre>make kwordprivate/fast install/fast</pre>

Works nice in general, except for this fantastic dependency problem:
<pre>touch KWFrameRunaroundProperties.ui
make kwordprivate/fast
Linking CXX shared library ../../lib/libkwordprivate.so
[ 11%] Generating ui_KWFrameRunaroundProperties.h</pre>

Doing the uic after the linking does not really have the effect I wanted :P  This made me laugh!  Really cool to see that we are apparently doing things that the buildsystem guys don't see in their test runs.
<!--break-->