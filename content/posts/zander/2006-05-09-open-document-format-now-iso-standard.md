---
title:   "Open Document Format is now an ISO standard!"
date:    2006-05-09
authors:
  - zander
slug:    open-document-format-now-iso-standard
---
Its a week ago that this happened, but I have not seen any kde-news or blogs about it.  So here goes;

KOffice native fileformat, Open Document Format is now ISO/IEC 26300 with just some bureaucratic things left, but no real way to stop this from going through. More on <a href="http://www.consortiuminfo.org/standardsblog/article.php?story=20060503080915835">Andy Updegroves blog</a>.

