---
title:   "ODF; battle lost, but victory seems unavoidable"
date:    2005-12-30
authors:
  - zander
slug:    odf-battle-lost-victory-seems-unavoidable
---
As a KOffice core-developer I am certainly very interrested in how <a href="http://en.wikipedia.org/wiki/Opendocument">OpenDocument</a> is gaining traction in the state of Massachusetts.  After all, KOffice is one of the contenders for the eyeballs of state workers after ODF becomes mandatory Jan-1-2007.

From the moment the policy to use ODF in Massachusetts was made public, we have seen people oppose the policy. (<a href="http://www.groklaw.net/staticpages/index.php?page=20051216153153504">brief history)</a>
If you followed the fight, you will agree it looks like a bad soap with personal attacks, underhanded claims and loads of FUD being spread to reverse the policy from taking effect.  At least some of the issues have been contributed directly back to Microsoft fighting ODF and pushing its own closed format instead.

And last weekend the first public battle was lost, the main man to have lead Massachusetts to choose ODF based on merit and freedom has put in his resignation as a direct result of the soap and personal attacks that have happened since the policy came under attack.  Peter J. Quinn has earned my respect and gratitude, and I am sorry to see him go.
Listen to this and you will agree he was the right man for the job; <a href="http://www.softwaregarden.com/cgi-bin/oss-sig/wiki.pl?OpenFormatMeetingSept2005">OpenFormatMeetingSept2005</a>

In an official statement the spokesman for Governor Mitt Romney wrote:
<ul>''We are moving steadily towards that deadline and we expect no changes in those rules, Under the Aug. 31 initiative, the state would require all documents produced by the state's executive branch to be stored in a new, universal computer format, called OpenDocument.''
</ul>

I'm definitely not into (US) politics, but I hear from <a href="http://www.groklaw.net/article.php?story=20051228095613324">good sources</a> that this is a statement from a very relevant person and since the statement basically said ODF is a go, this is good very news.  Nobody can look into the future, but after what has happened already and the governor being determined to continue in the set course, I'd say the chance that we will end up with Massachusetts using ODF is very good.

If nothing else, this undeterred attitude makes it easier for governments all over the world to attack the problem with much more public knowledge backing the decision that walking away from Microsoft can be good for you.  See this article, for example; <a href="http://64.233.183.104/search?q=cache:o9udwTyJ55kJ:www.bangkokpost.net/Database/07Dec2005_data01.php+&hl=en&ie=UTF-8">ODF: A Matter of Sovereignty</a> as a good article stating that a dozen governments are looking at following the ODF road to success.

At the end of 2005, there is one thing I will remember this year for, its the year when a formerly undisputed trueism stopped being true.  We can no longer say "Nobody ever got fired for choosing Microsoft".
Or, in clear words, in this year software vendors started to compete on merit again.
<!--break-->