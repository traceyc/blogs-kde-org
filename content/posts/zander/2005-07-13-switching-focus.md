---
title:   "Switching focus"
date:    2005-07-13
authors:
  - zander
slug:    switching-focus
---
Over the last weeks I have been spending time on KDEPim and KMail in particular. Various usability and feedback issues have been fixed that bothered me before and I find KMail to be a lot more pleasant to use,  fixing bugs beats working around them any day!

I have been struck with the complexity of C++ and moc and all that.  Steep leurning curve!  It took me hours to implement something that I would have done in 15 minutes in Java.  It turned out i hit various bugs in GCC and limitations of what moc can do.  Whats more is that I wanted to shift focus again, so I created an applet for our worldwide.kde.org website since that has been growing and usability has gone down at the same time.
Worldwide.kde.org has so many names up that it becomes useless without a zoom function in most of europe, but also outside.  So I made a zoom function.

I downloaded the huge <a href="http://earthobservatory.nasa.gov/Newsroom/BlueMarble/" target="_new">nasa BlueMarble</a> image of earth (21600 pixels horizontal) and wrote an applet that ships with a 600x300 jpeg and allows you to zoom in on it all the way up to the full res image which means you still have a nice picture at 200x zoom.
I found it quite funny that 2 days after I finished this applet google came out with their new google.earth.  Well, mine actually runs on all platforms, not just Windows.

Next I got the coordinates and names as used on Worldwide from <a href="http://physos.org" target="_new">Rainer Endres</a>, who maintains the Worldwide database.  So I wrote a parser and renderer for that data.  Which was the easy part.  Then I spent about a whole day writing and tweaking a placement algorithm so names don't overlap and make things unreadable.  I think I succeeded quite nicely there :)

After some more bugs are squashed expect it to be shown on Worldwide, as Rainer liked this addition.  For now, here is a screenshot:
<img class="showonplanet" src="http://members.home.nl/zander/worldwide1.png"><br>and for the impatient; here is a jar you can start using 'java -jar':  <a href="http://members.home.nl/zander/worldwide.jar">worldwide.jar</a>
<!--break-->