---
title:   "Slow hacking and the Eiffel Tower."
date:    2005-08-01
authors:
  - zander
slug:    slow-hacking-and-eiffel-tower
---
Boy, what a week.
I spent 2 days at the Krita Hackaton.  I went there with the background that I know about KOffice and saw Krita ones or twice so I could contribute at least some technical and usability knowledge. What I found was that Krita is actually the most promising KDE application I currently know of.  I did mostly usability consulting and some hacking to fix the rotating of the toolbox so you can drop it to be a simple toolbar, if you want.  Didn't do much more hacking since I borrowed an older laptop and compiling was slow.

I've graduated in computer technology with differentiation multimedia 8 years ago now; and have experience in a lot of professional image and print based technologies.  When the Krita guys were telling me they have color spaces of more then 256 levels per color and have CMYK color spacing that already means Krita is more mature then any open source application.  Photoshop tends to be the baseline for professional graphics applications for the last decade and Krita has various hi-level print features it does not have.  And the ones Photoshop does have are only there for the last year or so.
See why I'm excited about this promising new application?

Every now and then I talk to a graphics artist and discuss what pre-pres and professional printers are missing in applications for Linux. For some reason most Linux graphics users have been under the impression that Gimp is good enough and the conversation tends to die out quite quickly when I correct them to explain certain things being impossible or just plain hard in Gimp.  I have no beef with Gimp at all; the people that claim Gimp is good enough for professional use is who I have a problem with.  Please let me explain before you start flaming :)

I don't consider myself to be an expert in printing, but I know the market to conclude that high-end companies need feature X, Y and Z.  Now if a user comes up to me and says his favorite application is ready for that market; but features X and Z are not present I already loose a little respect for this users opinion.  After all; if he never bumped into the need for those features I'm not sure we are talking about the same things.

So all those people saying  Gimp is good enough for professional use make the professional graphics artist loose trust in Linux and open source.  "Bunch of amateurs" is not an unheard statement.

Why this little rant?  Because Krita just last week put in CMYK colorspace in there with various color depths and full access to all filters.  This is a seriously impressive feature needed for professional print.  If you know of any companies that do this kind of stuff; please let them give the Krita to be released in a couple of months a swirl (or download and compile from svn, naturally).  And please let them contact us to give feedback.

This was also the week I took the high-speed-train from Amsterdam to Paris (4h trip). I went with a couple of friends and played tourist in this beautiful city.
After arriving at 23:30 we went to a bar and has some beers and Ingrid, easily the most fluent French speaking of our group, convinced the waitress to bring some snacks.  It was really good; cheese, olives and popcorn (not at the same time) were brought regularly.  When we got the bill we initially thought she had billed those snacks, since the tab was higher then we expected it.  Much higher. We were shocked and left without much of a tip.  Only the next morning we were sober enough to make the conclusion that she has been a really good hostess and has given us a lot of nice snacks for free!  It turned out that a halve-liter beer cost over €8 in Paris (11USD).
Unfortunately we never saw her again to thank her for the nice evening.

Using the Paris underground was, ehm, interresting. Its not hard to figure out why the London one is seen as simpler if you do some KDE usability.  Basic rules like consistency (use similar concepts for similar items) and learnability (make things obvious, even for starters) are broken.

The lines are numbered and also have a color.  This is great in principle since some people are better in numbers then in colors and visa versa.  Unfortunately some places only show the numbers which makes you have to memorize the numbers anyway.  No more advantage to having colors. I also saw lots of little things that are only becoming obvious after made a mistake ones and figuring out that (for example) a two-line-name were in fact two station names.  A visual way to show its an enumeration would have helped in that case.

Oh, how little features and details make a difference for starting users.
<!--break-->
Naturally; obligatory image: <img src="/system/files?file=images/eiffel.preview.jpg">