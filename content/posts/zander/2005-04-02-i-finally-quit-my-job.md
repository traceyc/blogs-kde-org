---
title:   "I finally quit my job"
date:    2005-04-02
authors:
  - zander
slug:    i-finally-quit-my-job
---
I'm probably a rare example of someone that has worked in the same job for over 6 years already, where the job was the first one I got after finishing university. <!--break--> At birthday parties of friends I know for over 15 years there was always someone that recently changed jobs, and I got a look of unbelieve when I told them I _still_ worked at the same company.

Hé; what can I say; the work was great!  Building user interfaces and technically cool solutions in a market where all the competitars are still doing everything by hand giving you a perpetual 1.0 feeling.  Who would'nt want that?

Since the small startup was taken over by the biggest competiter a year ago things have been going down hill; I basically did not feel at home anymore for various reasons I don't feel like posting on a blog :)

So we both decided we should part ways and I signed the last papers a couple of days ago;  I finally left.

Instead of frantically trying to get a new job, I'm probably going to experience a long vacation first.  After having worked for just about my whole life (from 14-up, including my student years) I think I deserve a break.  So if anyone knows of a great destination to go for a month or two, leave a comment!
Now; how hard will it be to arrange a trip to Japan..