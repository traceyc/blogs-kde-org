---
title:   "Tabs"
date:    2007-05-20
authors:
  - zander
slug:    tabs
---
When KWord 2.0 is to be released we expect it to be on top of Qt4.3 which is in beta right now.  As you may know the new text engine for KOffice is based on the one in Qt, which is new in the 4 series.  Naturally, the engine in Qt is not feature complete at all for all the complex stuff that an application suite like KOffice needs.

I showed how I added complex lists in an earlier post, and here I want to show some recent work in tabs.  I guess we all know the concept of tabs; to move the next word to a pre-defined position. Excellent way to align text.
There are left-tabs, right-tabs and center-tabs in most word processors.  KWord also has tabs that align on a specific character (like a comma or dot to align currency).
The underlying engine, Qt-scribe only knows about left-tabs, though.  I requested that the trolls add it about a year ago but the solution slipped and will not end up in the 4.3 release.

But I really think we should support other tabs, so I investigated a way to build the tab-definitions we need on top of the ones that Qt has.
In theory its possible.  We first do a layout run of the sentence since that gives us the exact positions and distances for each character. Using the character widths of all the text I want to right-align I can then calculate where the text should be left-aligned to make it appear right aligned at my tab position.
Using that calculation I add a left-aligned tab to the list of tabs and Qt will do the rest for us.

The practice is a lot more complex than that, though.
In all text processing applications you can set different tab-positions per paragraph.  Qt is no different.  The trick I discussed in the previous paragraph, however, is not per paragraph.  If I have a right-aligned tab at 10cm and I have 5 lines in that paragraph the actual left-tab I calculate can be different on each line due to the different length of the words that I align. The effect I have is that I don't have a set of tabs per paragraph, but a set per visual line.  Which Qt can't represent.
Using the Qt API to paint or access layout data suddenly became a lot harder as I have to update to the correct set of tabs every time I access a different line in a paragraph.  Which means, for example, I have to do the painting of a paragraph in KOffice myself instead of just calling the one in Qt.

This is a bit unfortunate, and I had hoped to avoid it by having this feature added directly in the Qt codebase instead of hacking it on top.  My fault, I guess, for not sending in a patch.  I am pretty pleased with the results, though.  It does what I want and without noticeable slowdowns.

I even got to limit most of the code to the plugin, with just some 10 lines of additional code in an exported library. Which means that eventually moving this code to Qt will not be a big problem in our public API.

Mandatory screenshot:
<img src="http://www.koffice.org/kword/pics/200705-kword-tabs.png"><!--break-->
