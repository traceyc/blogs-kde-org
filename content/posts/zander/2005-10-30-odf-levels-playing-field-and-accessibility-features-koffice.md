---
title:   "ODF levels playing field and accessibility features in KOffice"
date:    2005-10-30
authors:
  - zander
slug:    odf-levels-playing-field-and-accessibility-features-koffice
---
Unless you have been living under a rock you have seen that Massachusetts chose for the OpenDocumentFormat in a very open and clear decision process leading to a lot of fuss and accusations, mostly coming from the closed source camp that obviously has a big problem if the number one reason for customers buying Word is eliminated, which is Lock In.
OpenDocumentFormat (ODF) eliminates this nicely and has a very cool by-product that all of a sudden KOffice and OpenOffice can again compete on a level playing field with MSOffice.

The number one problem any review of either open source Office Suite addressed was interoperability with the .doc file format. No longer!  Finally the KOffice package can compete on its own merit.

The last set of accusations and basic FUD surrounding the ODF acceptance in Massachusetts is that the applications implementing ODF are bad for blind people and other people with disabilities.  This is a hot topic, and if true could put a big peg in the decision process since in most countries its legally required to provide a good working environment for people with disabilities.  You can understand how upset some politicians can get when the government would suggest to ignore that.

The requirement is naturally not a surprise and some weeks ago we got a request to specify how accessible KOffice actually is.  Our KDE Accessibility gurus directly delved into the issue and found that back then KOffice could be rated pretty poorly. KWord worked fine if you had a simple document, but insert a table or any frames and you were lost.  That was 2 weeks ago.  Due to the cool work of Gary Cramblitt today most usage can be done by any user that does not want to touch the mouse which means that with the proper (and standard) accessibility software users with disabilities can actually use it. Now, this is the best example of the strength of competition combined with open source, right there. Hah!

A couple of days ago I <a href="http://blogs.kde.org/node/1569">blogged</a> about an accessibility feature that is poorly implemented in khtml.  The idea, however, is good and was immediately picked up by Gary and combined with KOffice applications to provide a very fast and easy way to navigate around the sometimes complex UI hierarchies.  More text and a screenshot can be found in <a href="http://www.canllaith.org/svn-features/svn-koffice.html">this</a> great article from Canllaith about KOffice as it is now in svn.

The work is ongoing as there are many little things that can be better, if you want to help, email us or hop on #koffice.


Oh, and a big thank you flies to <a href="http://www.canllaith.org">Canllaith</a> for her perseverance and great work on the <a href="http://www.canllaith.org/svn-features/svn-koffice.html">This Month in SVN about KOffice</a> which required compiling the KOffice svn even while half-way through someone committed a reorganization moving various files and making the compilation and installation more then a bit tricky.

<!--break-->