---
title:   "Just home and ready to fly!"
date:    2006-10-06
authors:
  - zander
slug:    just-home-and-ready-fly
---
Like many I came home from aKademy with a little flu thingy and I fell asleep the moment I got home last sunday night.
I slept a lot this week trying to get rid of this bug but also just because I really was low on sleep anyway so I had to replenish :)  So next to sleeping and talking on IRC (hi Kat!) I didn't do a whole lot this week. Nobody again say you don't need a holiday if you don't have a steady dayjob!

After several 14 hour nights of sleep I feel much better and just in time since tomorrow morning I'll get on the train to the airport again.  I'll fly to Boston for 3 days to attend the Text Layout Summit where lots of people from different free software projects come together to talk about joining forces and getting the best text layout possible.
I'll be there to represent KOffice.

So, its the first time I'll be flying to the USA, and I hope the customs will not give me a hard time. But I do look forward to going to MIT and talking about fonts, glyphs and lots of things that most people don't even want to know. We'll be talking on how to get KWord (among others) to be able to render all texts known to men.  But I'll leave the hard stuff to tronical who will be representing Qt.

This is held at a Gnome conference, so just like we invited lots of people to ODF at aKademy, Gnome is doing the same for the Text Layout Summit. I like the path we are on.

Wish me luck and have a good weekend!<!--break-->