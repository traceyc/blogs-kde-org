---
title:   "Text Layout Summit"
date:    2006-10-13
authors:
  - zander
slug:    text-layout-summit
---
Last weekend I was at Gnome Live, Boston to attend the Text Layout Summit.
The Text Layout Summit is a meeting intended to further all of Free software text rendering, both to screen and for print.

As I am a KWord developer I'm particularly interested in making sure KWord will be able to properly render all the notoriously difficult stuff that a good internationalized application should support.

On this end I got quite some insight. For example Edward Trager is quite the expert in non-latin scripts and could draw us quite some texts which shows problems and ideas I would not have come up with myself.
For example, in the ODF spec I noted that the feature drop caps (to show a character 'dropped' over several lines at the a start of paragraph) had a length option. I wondered why it allowed more then one character, until I saw that in languages like Thai multiple characters make up one unbreakable glyph, and a different language would even have a whole word that's unbreakable due to a line extending from the first char to the last char in the word.

The last day was the most interesting one, as it extended beyond learning from others to where the various open source players got together and reuse code and effort to make sure we will reuse the most basic logic to do text layout.
Both the gnome and kde libraries that do layout (pango and Qt respectively) do a substantial part of what is needed for the goal of supporting all scripts known to man. But both have parts that don't work yet and little details where the behavior is slightly different. As KWord in the 1.x series used its own text-layout engine I know from experience users will not understand why konqueror or KWrite show things correctly and KWord shows something buggy.

Work has been started to move so called 'shapers' to a shared library where both Qt and Pango build on top off. This is tightly coupled with reusing different font engines needed to parse specific fonts and font features.

The end result will be that we have gotten quite a big step closer to the goal of supporting all scripts known to man, and we do it in such a way that all free software will be able to use the same library so a user will not have to ask himself why text layout in KWord looks different from Firefox.

More close to home; I also had some talks with Simon on all the features I need in Qt to do proper text layouting in KWord. Nothing like a face to face meeting for things like that.  I'm optimistic that Qt4.3 will be able to support KOffice2 with really good support for languages and all those OpenDocument Features that we lacked in the past.
<!--break-->