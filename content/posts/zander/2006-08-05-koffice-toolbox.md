---
title:   "KOffice ToolBox"
date:    2006-08-05
authors:
  - zander
slug:    koffice-toolbox
---
Most graphics applications have a toolbox in one way or another. A floating window that contains a lot of tool buttons for, well, the tools in that application.
Karbon was the first in KOffice to have one, Krita followed last year. KWord practically speaking already had one, but I personally saw the toolbar with 5 icons to insert new things never as such.  Even while I was the one that made it stick to the left by default, its funny how that works.
In KOffice 2.0 we plan to make the toolbox something that is used in all KOffice applications.

In KDE4, using Qt4, I had the unpleasant surprise that the old code stopped working. The toolbars in Qt4 can no longer float (which I think is a design bug in itself, but thats off topic here). There is a replacement in Qt, the QDockWidget. Unfortunately its got a lot of bugs and missing features.

One cool feature we had in KOffice 1 was that if you dock the toolbox at the top or bottom of your window it rotated so it would not take up a horrendous amount of space. This was easy since there is a signal emitted in QToolBar that states the orientation changed.  I needed to find a new way for the QDockWidget, though.

Here is what I came up with;
<code>
void KoToolBox::showEvent(QShowEvent *event) {
    Q_UNUSED(event);
    Qt::Orientation orientation = Qt::Vertical;
    QWidget *parent = parentWidget();
    while(parent) {
        QMainWindow *mw = dynamic_cast<QMainWindow *> (parent);
        parent = parentWidget();
        if(mw == 0)
            continue;
        switch (mw->dockWidgetArea(this)) {
            case Qt::TopDockWidgetArea:
            case Qt::BottomDockWidgetArea:
                orientation = Qt::Horizontal;
                break;
            default:
                break;
        }
        break; // found it, lets stop.
    }
    myBoxLayout->setDirection(orientation == Qt::Horizontal ?
            QBoxLayout::LeftToRight : QBoxLayout::TopToBottom);
}</code>

I won't claim prizes, but it works, and thats the most important thing :)
<img src="http://www.koffice.org/kword/pics/200608-kwordDock_vertical.png">&nbsp;<img valign="top" src="http://www.koffice.org/kword/pics/200608-kwordDock_horizontal.png">

There is one pretty cool feature in QDockWidgets that I liked very much; if you set a dockWidget on the mainwindow <code>
    mw->addDockWidget(Qt::LeftDockWidgetArea, toolBox);</code>
and your toolbox has a proper name (using QObject::setObjectName()) the mainwindow will place it by default on the left, like you asked, but will persist the last place the user had it when closing the application and restore that position automatically.  Pretty neat!
<!--break-->