---
title:   "GUI Libraries"
date:    2005-07-16
authors:
  - zander
slug:    gui-libraries
---
My first project in KDE was maintaining KOffice.  This was years ago already.  Later I was forced to use Java at work and have been doing that for some 6 years now.  I recently left that job, but I still have a nice chunk of work from that job available as an open source project I started and worked on during that time together with some colleagues and volunteers.

This open source project is adding to Java/Swing all the nice things I was used to having in KDE. Think i18n and signal/slots and xmlgui as well as things like a GroupBox or colorbutton class.
I really tried to make it a marriage of technologies; best of both worlds.

Last Monday I did a <a href="http://uic.sourceforge.net/index.phtml?target=releases/release-2.0.phtml">2.0 release</a> of the collection of tools/framewords.  This was more to fix the API then anything else since there were occasional bugfixes but no major changes for some time.  Call it a signal to say we find it stable.
I'm quite surprised that in 5 days we already have over 250 downloads. This software may be more popular then I thought :)

So, what is this blog doing on kdedevelopers.org?
Well, I'm mostly pretty proud of a mature and really well received piece of work.  But there are lots of ideas in there that KDE would be able to learn from.  I did not just rewrite KDE technologies there, but also reused Java technologies and ideas from even other places.
For example; the recent <a href="http://blogs.kde.org/node/1241">collapsible groupbox</a> addition from Danimo has been in there for some time now, including animating collapse:

<img src="http://members.home.nl/zander/collapsable3.png" class="showonplanet">
(and <a href="http://members.home.nl/zander/collapsable1.png" target="_new">1</a> <a href="http://members.home.nl/zander/collapsable2.png" target="_new">2</a> <a href="http://members.home.nl/zander/collapsable3.png" target="_new">3</a> <a href="http://members.home.nl/zander/collapsable4.png" target="_new">4</a>)

Other ideas are the tooltip placement on a kSqueezedTextLabel so the text on the tooltip is exactly in the same spot as the text on the label.  All the way up-to making signal/slots multithreading with various ExecutePolicies.  A really powerfull concept unique to the User Interface Collection (http://uic.sf.net).

For me the parser that reads QtDesigner files and outputs Java code still has meaning even if I don't really program Java Swing that often anymore.  The outputter can be retrofitted to output Java code for the KDE-qt bindings for Java quite easilly overcoming one big stumbling block for Java programmers that want to use KDE.

<!--break-->