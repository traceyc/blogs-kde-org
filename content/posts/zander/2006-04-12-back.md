---
title:   "Back"
date:    2006-04-12
authors:
  - zander
slug:    back
---
I have been away from the blog sphere for some odd 3 months. Unlike most I don't have the excuse of being too busy, I have a rather different excuse.  I have been out of the country and mostly away from internet during that time :)

To be exact; I have been travelling around the most beautiful country of, ehm, planet earth for 3 months.  The country is New Zealand and unlike most travelling brochures the images you may have seen (well everyone saw <a href="http://imdb.com/title/tt0167260/">LOTR</a>, naturally) pale in comparison to the real thing.  I loved every moment I was there.  Not to mention that I was the first KDE person to actually meet our famous <a href="http://www.canllaith.org">Canllaith</a>, the only KDE dev living in NZ.

I understand the EU has had the coldest winter it has seen in over a hundred years, so I didn't miss much.  Since I left the weather in NZ has been pretty sad as well;  someone suggested that maybe it mourns for me.  How lovely a thought!

The great thing about travelling is meeting new people, seeing beautiful places and having no deadlines.  I could honestly live like that for the rest of my life; although saying goodbye to brief-but-good friends is something I could never get used to.
The bad part about travelling is that all those little things that make your life easy are missing.  Upon coming home I noticed that unlike my expectations I did not miss the Dutch food most.  The thing I missed most was having a lot of good herbs and good knives to cook with.  It was having access to all my clothing, my books and music again.  Basically I was missing having my own place to live.
Travelling for 3 months is enough for me, then I want to get back to a 'normal' lifestyle again.

The first (computer related) thing I did when I got back was upgrade my laptop as X11 was a cvs version and a rather flaky one.  Now I have openGL and better support for my mouse-pad and my cd-writer is actually able to <i>write</i> cds :)  Debian still rocks; the upgrade went pretty nice, and while I had to drop out of X, I didn't even have to reboot!

I'm slowly getting back into KOffice hacking, I intend to continue fixing bugs in KOffice 1.5 for a little while before I make the jump to get KDE4 and all that running, which is needed for KOffice trunk.  While actually using KWord (yes, I sometimes use my own software: shock, horror!) I found some issues that need fixing, so I will work on them first.  Its the small things that make things feel so much better, afterall :)

Oh, yes, I did get back in time to enjoy the KOffice 1.5 release.  I certainly did help a lot making KWord the best it can be; but I only really spent half the time available on it. And people tend to agree its a LOT better than the 1.4 one.  So; here is to hoping I can spent a lot of time on KOffice 2.0!
<!--break-->