---
title:   "When marketing doesn't know whats going on.."
date:    2006-04-26
authors:
  - zander
slug:    when-marketing-doesnt-know-whats-going
---
Today I got an email from a friend asking me if I knew about Sun open sourcing Java.  Naturally I wanted to reply that thats been a long teaser from Sun where they will never do it, but they let on enough people to hope it might happen one day.  Pleases all parties, I guess.

Then I read down to the link this friend got his info from, its actually a <a href="http://www.sun.com/smi/Press/sunflash/2006-04/sunflash.20060424.2.xml">sun.com URL</a>, so I got curious.

The quote my friend was talking about was a generic 'whats Sun' paragraph hidden at the end of the announcement.  I wold have thought they had a stock one to copy paste, but apparently thats not the case and someone had to write one from the top of his head and make a stupid mistake. The quote is:
<ul>
Sun's vision is more relevant today than ever before and is embodied in the product and service breakthroughs it has recently brought to market - from the 'pay-per-view' utility computing Sun Grid and the eco-friendly 32-processor-on-a-chip Coolthreads system, to Sun's innovative software pricing model for the Java Enterprise System and <b>the open sourcing of Java[tm]</b>, the Solaris[tm] Operating System and the UltraSPARC T1 chip. 
</ul>

I find that so hilarious; that the marketing department got confused after Sun has been saying <i>no</i> and <i>maybe</i> after all these years :)
<!--break-->