---
title:   "All the FUD..."
date:    2006-11-17
authors:
  - zander
slug:    all-fud
---
Since the Novell / Microsoft deal a lot of people have told stories on how Novell should not have assumed there are patents, and how the deal is written to sidestep the GPL with regards to patents.
And all this time Novell was putting out FAQs, press releases trying to state that its not about patents at all, and how Novell is sure that there are no patents on the GPLed software Novell ships.

Well, here is what Microsoft says about this;
<ul>"We've had an issue, a problem that we've had to confront, which is because of the way the GPL (General Public License) works, and because open-source Linux does not come from a company -- Linux comes from the community -- the fact that that product uses our patented intellectual property is a problem for our shareholders. We spend $7 billion a year on R&D, our shareholders expect us to protect or license or get economic benefit from our patented innovations.
And we agreed on a, we call it an IP bridge, essentially an arrangement under which they pay us some money for the right to tell the customer that anybody who uses Suse Linux is appropriately covered.
</ul>

http://blog.seattlepi.nwsource.com/microsoft/archives/108806.asp
<!--break--->
Maybe now Novell will understand why people have equated this contract to selling out.