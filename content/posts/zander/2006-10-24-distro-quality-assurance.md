---
title:   "Distro Quality Assurance"
date:    2006-10-24
authors:
  - zander
slug:    distro-quality-assurance
---
In Linux land we have an awful lot of choice in distro's.  Choice is good as it ensures you have a better chance of getting exactly what you want.
In something as big as a linux distro (more often then not it ships man CDs worth of data) I feel that the effects are not helping people out anymore.  Choosing your distro is just based on too many things.  How hard is it to upgrade packages? Can you upgrade in a year time? Is the directory structure easy to understand? How easy is it to maintain your hardware? Is all the hardware supported?
And on the other side of the spectrum you have security patches and package management philosophies.
Most people don't look at that when choosing a distro, but in my experience there is something wrong with each distro I saw. And I bet its because the different distros don't steal enough from each other.  Stealing package tweaks from other distros because they give a better user experience is a good thing.

The latest differentiation I saw is how and when to ship a package.  Debian is famous for shipping a package only when its found to be well tested. Their stable release is really rock solid, and a package will only make it into the 'testing' section if it does not have known issues. In other words; it takes a little while after release before you can get it, but you can virtually count on your latest update not breaking things.

In KOffice we just closed a bugreport on spell checking not working in KWord. The reason for that, it turns out, is that Gentoo moves towards the opposite side of quality control. In this case KDELibs 3.5.4 could not have been shipped as is because a dependency was not available yet. Instead of letting it wait, the packagers choose to ship it but exclude spell checking. Which made it build even though the dependency was not there. Leading to spell checking missing in KWord.
http://bugs.gentoo.org/show_bug.cgi?id=152417

Choosing the distro for your machine clearly means more then most people judge it on. The quality control to ensure a minimum of bugs users are exposted to is high in a distro like Debian. On the other end of the spectrum we have a distro that makes choices that I would equate to not having any QA process at all.

After seeing a handful of weird bugs every week on IRC from gentoo, I'm struggling with my conviction that people can run my (LGPLed) code any way they like.  With that I mean I don't mind people doing with it whatever they do, maybe have a logo in our software "NOT designed for...".  :)

What do you think?  Can we help distros provide a better user experience, or, towards the ridiculous side, should we close bugs from some distro as is?
<!--break-->