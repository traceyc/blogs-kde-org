---
title:   "KOffice Textlayout"
date:    2006-08-16
authors:
  - zander
slug:    koffice-textlayout
---
KWord will get a new textengine in version 2.0, it will be based on Qt4s scribe engine. And I've been working on this for the last week. Scribe gives me the ability to implement just the actual sizing and positioning of lines. Which means a minimum of code needed to do cool things. I like that.

Maybe people remember I posted screenshots with text layouting already being done. Thats true, its just that that code was simple (and incorrect) proof-of-concept code and I removed it as it has served its purpose. Starting from scratch; take 2.

In order to implement testcases to see if I am doing the right thing, I needed to find out how to do proper linespacing and check basic findings like how much text fits on a line.  I have a history in DTP and pre-press, so I have a good idea how to do it properly, but I don't want to just assume my memory does not fail me.  What I did was grab a commonly known piece of text called copyfit. Which is a textfile with random text but with numbers every 50 chars.  Its traditionally used to see how much text fits into a frame.

I loaded that text in various layout engines and made sure the font and size were the same as well as the dimensions of the frame its in.  The result should be that all engines generate the same result. Since the input is exactly the same.

What I found was pretty sad; the 5 different engines I tested all generated very different results.
The tested items fall into two parts; how many characters fall onto a line (kerning etc) and how many lines fit in the specified frame.

I have no idea how to test for 'correctness' in the amount of characters that fit on a line.  I would appreciate people that open the file (<a href="http://members.home.nl/zander/copyfit/copyfit.txt">text</a>, <a href="http://members.home.nl/zander/copyfit/copyfit.odt">ODF</a>) and take a screenshot in applications like InDesign or FrameMaker. Both are leaders in the field.
Linespacing is a bit easier; there are simple rules for that.  Each line takes its font height, and between lines there is a little space. Typically 20% of the height of the line before.

I tested:
<a href="http://members.home.nl/zander/copyfit/writer113.png">OpenOffice 113</a>: 17 lines
OpenOffice 203 (<a href="http://members.home.nl/zander/copyfit/writer-windows2.jpg">windows</a> & <a href="http://members.home.nl/zander/copyfit/writer203.png">Linux</a>): 20 lines
<a href="http://members.home.nl/zander/copyfit/kword16.png">KWord 1.x</a>: 20 lines
<a href="http://members.home.nl/zander/copyfit/scribus1332.png">Scribus</a>: 19 lines
<a href="http://members.home.nl/zander/copyfit/Qt4.png">Qt4</a>: 18 lines

The frame is just over 283.5pt in height; so a 12 point font and a default linespacing will lead to allow 19.9 lines.  Which means (since we shouldn't show incomplete lines) that the only one doing linespacing correctly is Scribus. Its the farthest off in amount of characters per line, though.

So, here is what I have so far; it does linespacing according to theory and the amount of characters per line is not that far off from OpenOffice (which people that move docs from one to the other will like).
<img src="http://members.home.nl/zander/copyfit/kword2.png">

To move that last line to the next frame is a TODO for tomorrow :)
<!--break-->