---
title:   "web publishing with KDE"
date:    2005-11-03
authors:
  - zander
slug:    web-publishing-kde
---
Recently I've been having some photographs that I make and want to share them online. Just one or two when in a IM/IRC conversation for example.
So, what I do is take the pic and make it smaller for web publishing (no need to place 750Kb images just to get your point across) and then I upload to a personal webspace it and paste a ULR in the IM window.

That should be automated, I thought after a couple of images! <!--break-->
So, I created a drop box folder on my desktop and intend to drop images in there after which some software should do the rest.   I used dnotify(1) to pick up on the new image and then use the command from imageMagick to convert it.  ImageMagick is installed on most systems, so that should work without problems for anyone that likes to use this script.
Next; the KDE part; I upload it using kfmclient.  This is usefull so I can upload it to my webdavs server. Anyone can chose the access method needed for his/her particular situation and I'm sure KDE will support that, no problem :)
Added bonus; kwallet usage means no typing of passwords more then ones per session!

I added this to start on login;
/home/zander/.kde/env/dropbox.sh
<pre>
#!/bin/bash
killall dnotify 2> /dev/null
dnotify -RC /home/zander/Desktop/images\ Dropbox -e /home/zander/bin/dropbox_private.sh &
</pre>

And the private script that is then called automatically is:
<pre>
#!/bin/bash

umask 077
cd /home/zander/Desktop/images\ Dropbox/

for i in *
do
    if test -n "`echo "$i" | grep .part`"; then continue; fi # still uploading
    # scale to /tmp,  the $$ expands to a unique process ID
    convert -scale 1024x800 -quality 50 "$i" /tmp/image-convert$$.jpg
    if test -z "`echo "$i" | grep .jpg`"; then #force jpeg; so rename if its not a jpg
        i2="$i.jpg"
    else
        i2="$i"
    fi
    kfmclient move /tmp/image-convert$$.jpg "webdavs://drive.home.nl/fullPath/$i2"
    rm -f "$i" # remove original

    #put the URL in the clipboard
    dcop klipper klipper getClipboardContents > /tmp/clipboard$$
    dcop klipper klipper setClipboardContents "http://members.home.nl/zander/tmp/$i"
    # reset the url to the one we just had; clipper will allow the 
    # user to actually fetch the right one
    dcop klipper klipper setClipboardContents "`cat /tmp/clipboard$$`"
    rm -f /tmp/clipboard$$
done
</pre>

It works amazingly well for me, and shows how easy it is to automate KDE / Linux so the computer does not make you work, but the other way around.  I hope you find it of use and can maybe even use it for inspiration.