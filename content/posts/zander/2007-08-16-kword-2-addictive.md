---
title:   "kword 2 is addictive!"
date:    2007-08-16
authors:
  - zander
slug:    kword-2-addictive
---
KWord in trunk (we recently got an <a href="http://www.koffice.org/news.php#itemKOffice20alphareleased">alpha2 out</a>) is not the same piece of software as it was in KOffice 1.x, its gotten such a makeover both internally as externally that its largely unrecognizable (in a good way). This also means that old known functionality may have problems due to it being new code.

When I woke up this morning I found this on #koffice

01:24  * astan zz.
01:24  &lt;BCoppens&gt; night :)
01:47  &lt;astan&gt; hm. is it possible to insert text frames in kword yet?
01:58  &lt;astan&gt; ok, i've just made my first commercial document using kword2 ;) a receipt for this bicycle i'm buying for my book café.
01:58  * astan zz for real.

I guess that after compiling KOffice from svn and taking that extra half hour to do real work on it, KWord passed some sort of test :)<!--break-->