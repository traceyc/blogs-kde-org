---
title:   "KWord 2.0 progress"
date:    2006-06-23
authors:
  - zander
slug:    kword-20-progress
---
After working on Flake for the last couple of weeks I refocussed on KWord earlier this week.
KWord needs an amazing amount of refactors to get the best out of the new features both Qt4 and <a href="http://wiki.kde.org/tiki-index.php?page=Flake">Flake</a> provide, so this left me with a dilemma on where to start and how to approach this best.  I decided to go for the mechanic ways.  You know what I mean if you ever saw a mechanic who took a whole car apart, with a garage full of parts and starts to reassemble it afterwards.  I am doing the same in software.
Good for me that I don't have to take kword apart first, I'll just copy the parts I need :)

I've started working on the basics, a canvas, a document and pages first. Not only for the obvious reasons but also because they needed to be reworked anyway. In the new revision I put in some new features that we kind of had support for in 1.5
<b>Page Spreads</b>.  This is the concept of having 2 pages that the user edits as one.  Consider the usage that you have a big image that you want to print over two pages. You would really like to have 1 image in your document and position across the two pages so it gets printed properly. This always was impossible in just about all word-processors.  KWord now has this high-end feature.  See here:
<img src="http://www.koffice.org/kword/pics/200606-KWord-PageSpread.png"><br>

After pagespreads worked I went on to put text on the pages, as already visible in the previous screenshot this also has a nice new feature.  The margins for left/right have gotten a little more intelligent and now allow you to set it to be mirrored between left and right pages.  So you get a small border closest to the binding creating a much nicer booklet output.

Last point I want to show today is how KWord 2.0 will benefit from the new Qt text engine by eliminating the most complained about problem; having correct WYSIWYG.  See this screenshot for 1 piece of text being shown in two different zoom levels and they both look excellent!
<img src="http://www.koffice.org/kword/pics/200606-KWord-txtZoom.png"><br>

Have a good weekend! <!--break-->