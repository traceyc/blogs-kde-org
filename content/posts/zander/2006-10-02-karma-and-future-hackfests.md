---
title:   "Karma and future hackfests."
date:    2006-10-02
authors:
  - zander
slug:    karma-and-future-hackfests
---
I'm just back from Dublin, and this is the first time I actually grabbed a pen and started doing some blogging.
Back home I realize that I really need some more sleep; going out drinking several pints of Guinness every day and still getting up quite early got me down. So now I'm hugging a cup of tea in front of the computer slowly getting through the day.

At aKademy I had a really good and productive time.  I spoke to lots of people, many of which I never saw before.  One recurring theme was that people know me by face (I didn't have my namebadge on most of the time) and lots of people have some experiences working with me together on either a KOffice issue or a usability issue.  The 'karma' thing is real;  good things happen to you when you are good to others.  So many friendly faces and good times either in technical discussions or while at pubs/dancefloors.  And, yeah, I've seen people being ugly have bad things happen to them. Not many of them at aKademy, luckely :)

In my observation aKademy 2006 was much more a place where people start to create common ideas and directions than former years.  I've seen this grow over the years where we used to just have long hacking sessions (n7y is a good example) with one or maybe two people behind a keyboard, while we now have higher level talks about designs etc while most of the actual hacking is done back home.
This shows quite clearly the need to have high-bandwidth communication available.  Real life meetings are as high-bandwidth as it gets and at least I don't have any time (or energy) left to do real implementation discussions after that.  Lets see how we can have more smaller and more directed hacking meetings over the year.  That would be good.

Hack on!<!--break-->