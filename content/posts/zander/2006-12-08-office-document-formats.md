---
title:   "office document formats"
date:    2006-12-08
authors:
  - zander
slug:    office-document-formats
---
Since the OpenDocumentFormat (ODF) became a real ISO spec back in May, a lot of things have happened and continue to happen. The industry is really recognizing this open standard and many are already supporting it, where a large section even make ODF mandatory.
I expected pick up to be slower, given how Office has such a huge majority of the market.  If I look at how slow pick up of Linux on the desktop is (slowly but steady growth over quite some years) its very refreshing to see people recognize ODF as the better format so massively.
In the last months I've seen ODF adoption in Malaysia, India, Brazil, the French Parliament [<a href="http://www.assemblee-nationale.fr/presse/divisionpresse/m01.asp#TopOfPage">FR</a>|<a href="http://www.heise.de/ open/news/meldung/81665">DE</a>], in <a href="http://www.earthtimes.org/articles/show/news_press_release,20660.shtml">Finland</a>, The <a href="http://www.computerworld.com.au/index.php?id=1267497878&eid=-219">Spanish region of Extremadura</a>, the <a href="http://news.zdnet.co.uk/software/applications/0,39020384,39276978,00.htm">Belgium Government</a> and soon <a href="http://www.inside-it.ch/frontend/insideit?XE7lhitk4AZh79is3NZRVVvcdpnHs0vJ5iY3vcZ99ZdaDhi4Ln1YGSdokeV3">the Swiss</a>. With some grass roots conversion happening in Holland as well (<a href="http://www.trouw.nl/hetnieuws/economie/article566504.ece/Grote_steden_eisen_alternatief_voor_Microsoft">Dutch</a>)!

So, yesterday marked the day that Microsoft's format actually passed the vote and got OK-ed by ecma, 20 to 1.  It votes just twice a year, and it was very unsurprising that the format was voted on positively, even while there have be almost no changes made due to problems with the proposal.

As open standards go, this process definitely doesn't quality Office Open XML as, well, open in my eyes.

Novell has foolishly promised to write a converter, and add some patents in the mix as well. There a are hundreds of articles written about how that was a bad move, so I won't add my bit.
Whats foolish is that its practically impossible to write an importer for Microsoft's format that is anywhere near complete, due to the fact that its not just a fileformat, its an accumulation of all things that happened in the last 10 years with MSOffice. Including <a href="http://www.robweir.com/blog/2006/10/bit-about-bit-with-bits.html">bitfields</a> and <a href="http://www.robweir.com/blog/2006/10/leap-back.html">exceptions</a> like "interpret this bit as X when it comes from Mac, as Y when it comes from Win32 and even different when its written before date Z".
No wonder the format is 7000 page long!

What happens if the MS format becomes an official standard? Then all office suites that want to support it will spent the next years spending time writing and testing their import filters.  That may give you a familiar feeling since that's what many office software has been forced to do for the last half a dozen years or so.  Following Microsoft instead of honest innovation.

<a href="http://blogs.adobe.com/shebanation/2006/12/open_xml_one-way.html">Andrew Shebanow</a> shows how Microsoft has given the numbers to let us calculate how supporting the MS format will take about 150 manyears. Assuming you already have the features. Which kind of proves what many competitors of Microsoft already know; what Novell thinks a contract said is most probably not what Microsoft will make it turn out to say.
<!--break-->