---
title:   "New feature; time to completion; 18 months"
date:    2007-03-30
authors:
  - zander
slug:    new-feature-time-completion-18-months
---
In KWord we always had a very simple way to structure pages.  We just stored the height of a page and when there is a frame at position 10000 you can calculate its at, say, page 12.  Naturally, this only gets you so far and we had requests for things like differently sized pages and pagespreads.
So, in October 2005 ago I wrote a Page class and a PageManager class. Which was released in KOffice 1.5.  This already gave features like being able to have a document start from page 10, instead of always from page 1.  But unfortunately there never appeared a GUI for it, and thus users could not use it.

The code from back then is still in KWord2 and today I finished the last feature to actually use all the power that the concept of one data object per page gives. So, this officially is the longest I ever took on writing a feature and actually bringing it to completion :)
Naturally the whole redesign of the core of KOffice came in between, as well as plenty of other things (I travelled to the other side of the world for some months, for starters) so I'm not a slow coder, honest!

Here is a full list of advantages that this change brings us;
<ul>
 <li>Every page in a document can have a different size, including being landscape vs. the normal portrait rotation.</li>
 <li>A document can start at another pagenumber then the default number 1.</li>
 <li>We allow page spreads; which is basically 2 pages back to back so you can place one image on both.</li>
 <li>We can switch between left / right margins and page-edge / binding-edge margins.  Meaning that it is possible to have odd and even pages get mirrored margins.</li>
 <li>Margins can be different for each page.</li>
 <li>Each page has a so called bleed.  This is required for professional printing.</li></ul>
Related new feature;<ul>
 <li>A frame can be specified to only appear on odd or even pages instead of on each page.</li>
</ul>

Obligatory screenshot;

<a href="http://www.koffice.org/kword/pics/200703-kwordPageSpreads.png"><img border="0" src="http://www.koffice.org/kword/pics/200703-kwordPageSpreads_thumb.png"></a><!--break-->