---
title:   "GPLv3"
date:    2006-11-04
authors:
  - zander
slug:    gplv3
---
Some months back I read about the GPLv3 for the first time.  Just like many I was compelled by the arguments of Linus against it more then by the arguments from the FSF for this new license.
Reasons for that were that the EU doesn't allow software patents as well as the silly example of Tivo which is pretty far fetched.

The other day I talked to some KOffice developers and we discussed the software patents issue and how that would actually work in real life;

<ul>
An etch-a-sketch maker releases a tool that has a good looking front end for his product but is using some of Kritas code underwater.
The code being GPL he releases all his changes on the website for us to port upstream if we want.

Using GPLv2
The company added some GPL code to the Krita sources. They release the diffs, but point out that we can't use that code in our upstream since the toymaker patented it.
They release the etch-a-sketch with this feature and if you want to replace the software on the device you will be forced to do so without the patented feature. Loosing functionality in your 'upgrade'.

Using GPLv3
Same company added some GPL code to the Krita sources. They release the diffs.
Since the company wants to use Krita on its device they loose all rights to enforce the patent and Krita can therefor use the patented diffs as soon at the company has released at least one version of its etch-a-sketch.
</ul>

Last week the <a href="http://www.novell.com/linux/microsoft/openletter.html">letter</a> from Novell came out about how it signed a deal with Microsoft which allows Novell to use any of Microsofts patents in the code it releases. In fact, its encouraged to do so, for example for writing support for the Office2007 fileformat and the samba software package.
See here what <a href="http://www.eweek.com/article2/0,1895,2050848,00.asp?kc=EWEWEMNL103006EP17A">Ballmer</a> said:
<ul>Microsoft CEO Steve Ballmer said his company is open to talking to other Linux distributors about reaching mutual patent coverage deals similar to the agreement signed Nov. 2 with Novell.
 Such talks would be a good idea, Ballmer suggested, since now only Novell's SUSE Linux customers are the only Linux vendors that have any assurance that Microsoft won't sue for patent infringement....</ul>

This, to me, looks awfully like the etch-a-sketch example above, Novell wants to differentiate itself from the other distros and now can alter GPLed code in such a way that nobody else can sell it, or even copy it without being open to patent litigation from Microsoft. And if you knowingly infringe a patent then you pay 3 times the damages! Thats the law.

So, suddenly the problems are not that far off anymore, and GPLv3 seems like a good idea to allow everyone to use my code and still require them to give back when they build on top of it, patents or no patents.
<!--break-->
What do you think?