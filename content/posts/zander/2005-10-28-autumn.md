---
title:   "autumn"
date:    2005-10-28
authors:
  - zander
slug:    autumn
---
The weather is changing, but we still have plenty of sun. In other words; great outdoor-photography time!

The pictures were taken near Schoorl, which is walking distance from the sea. We did not actually walk to the sea as I had a birthday party that evening and we did not make everything too hush hush.  2 hours walking did do great things for our appetite.
<a href="http://members.home.nl/zander/images/mos.jpg"><img src="http://members.home.nl/zander/images/mos_thumb.jpg"></a>&nbsp;<a href="http://members.home.nl/zander/images/heide.jpg"><img src="http://members.home.nl/zander/images/heide_thumb.jpg"></a>&nbsp;<a href="http://members.home.nl/zander/images/paddestoel.jpg"><img src="http://members.home.nl/zander/images/paddestoel_thumb.jpg"></a>

<!--break-->