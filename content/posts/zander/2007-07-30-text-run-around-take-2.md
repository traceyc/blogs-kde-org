---
title:   "Text run-around take 2"
date:    2007-07-30
authors:
  - zander
slug:    text-run-around-take-2
---
Some time ago I blogged about how KWord can now run around the outline of any shape you place in its text flow.  One thing I have had on my TODO ever since was allowing a user to create a custom run-around outline that is separate from the outline of the actual shape we run around.

Yesterday morning I woke up and realized that actually its not that hard to do, and even while it was a feature for post-2.0 I sat down and did it anyway.  Just 2 hours later I had a working prototype and this morning I dotted the I's and committed the stuff.

It works like this;
when you insert new content in KWord it is always embedded in a frame. Just like in KWord 1.x.  The difference in 2.0 is that if you insert a star or some vector based shape the outline is not square it will follow the contour of the actual content.  If that shape is on top of a text shape the text will then position itself a to avoid the outline giving a bit more organic view.

Now if you insert a photo, you always have a square outline. As is the nature of images.  Which means that you still don't get the result you want, namely that text runs around the contour of your content.  So, you have to tell KWord what your contour is, as it can't find out itself.

If you look at the following screenshot you will notice the black vector line which I drew inside of KWord to make the outline actually be around the flower instead of around the whole picture. And the text runs around that outline.  This black line will not be printed, it is only for editing purposes.

<img src="http://www.koffice.org/kword/pics/200707kword-custom-runaround.png">

This shows the flake concept very strongly, you can edit the vector outline inside your KWord using the same tool as you'd use in karbon, and as a bonus you get to see the text reposition in real time while editing. Pretty neat.