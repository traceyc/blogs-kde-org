---
title:   "Flake, the lib that saved KOffice"
date:    2006-10-17
authors:
  - zander
slug:    flake-lib-saved-koffice
---
I often get the question what Flake is. And as the answer is getting clearer in the heads of the people designing it, I thought I'd write down an introduction.

Flake is the component library for KOffice.  The goal is to allow spreadsheets to be embedded in KWord and the text-engine to be embedded in Krita.

In order to reach that goal we have to look back a little bit; in the KOffice1.n days (which lasted for the last 6 years) we had this kind of embedding based on widgets. This makes sense in some way since a widget really is a way to have a generic component that can be reused in other places easily.
Widgets have some shortcomings, though.  The main shortcomings are:
<li>a widget is always square</li>
<li>you can't rotate it.</li>
<li>widget sizes are measured in pixels which makes it awkward to print at high resolution.</li>

In KOffice we came up with <b>Flake Shapes</b>.  A shape is a generic component that, just like a widget, you can reuse in other applications.
Additionally we allow each shape to be zoomed, rotated and skewed.  The shape doesn't even know its being deformed, which keeps it simple to program a shape.
A shape is basically an area on screen or on paper that displays its own kind of information.

Example shapes here are a vector shape.  Think a circle or even a butterfly shape. Another example is a text shape. Anything from a one liner to a whole page of text.

Shapes are designed to follow the Model View Controller (MVC) mechanism.  Multiple shapes are each a view to the model. The controller is the Tool, which will be looked into in a bit. I can have 5 shapes showing all one large text document and these make up a 5 page book with that text.  You can also reuse a shape to show it at a different zoom level. For page previews, for example.

Each shape is a separate component. Shipped as a service (aka plugin) making it easy to share plugins with other applications.  Its gotten ridiculously simple to have a styles based text in krita or kivio now, for example.

The second big part of flake are <b>Flake Tools</b>.  If you want to show text you need a text shape, but the question quickly arises on how you can alter the text, or type new text.
For this goal the service for the textShape will ship a tool that is capable of converting key and mouse events into the proper effects on the text shape.
In short, there is a tool that is used to type and select/alter text.

Similarly there is a tool to move any shape around, as there is another tool to create and manipulate vector paths.
Tools are service based, like shapes are, so again you can use those in any application that can load those services.

The depth and broadness of Flake goes quite a bit further.  Its got nested shapes and its got methods for generating dialogs for user input in a generic manner, etc.  We designed loading / saving on a per shape manner as well as copy paste. But we are still looking for an implementor.

More info can be found at the <a href="http://wiki.kde.org/tiki-index.php?page=Flake">wiki</a>
And <a href="http://www.koffice.org/developer/apidocs/libs-flake/">further docs</a><!--break-->