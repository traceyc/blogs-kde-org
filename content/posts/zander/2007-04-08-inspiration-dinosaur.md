---
title:   "Inspiration from a dinosaur."
date:    2007-04-08
authors:
  - zander
slug:    inspiration-dinosaur
---
If you would have been around in the KWord 1.0 days you would remember that a big influence in the industry was by Framemaker.  And for a reason; it has always been the only GUI application capable of holding a whole book, including lots of images and complex features.  The set of features was well thought out and based on a decade of experience.

One thing that has been on my wishlist for about 5 years now is to do proper anchored frames.  In KWord 1.x we had inline frames, which is just one type of anchored an frame.  We supported an image or bigger things right inside the text flow. Which is pretty useful.
For usecases like having an image referenced in text, and you want to show next to the paragraph it is not enough, though. Framemaker had a set of features where you can specify the alignment of an anchored frame.  With things like 'TopOfFrame' and 'TopOfParagraph' where the anchored frame will automatically be placed based on those variables.

I'm pretty happy to have gotten a pretty well working implementation of this feature.  In KWord2 (trunk) you can now anchor any flake shape to a text position and choose from a dozen alignment methods to place it perfectly for your use case. Which opens up a lot of possibilities for users to explore.  For example you can now place a formula or chart right aligned of the text, and have the top of your formula align with the paragraph.  Automatically moving it when you type more text.

KWord and OpenOffice Writer are the two major free word processors applications.  They also both have OpenDocument Format as their native fileformat.  One thing I hear a lot is that loading a file created in Writer has layout problems. Things like text next to an image in Writer shows up underneath the image in KWord.  This is now a thing of the past; we will be able to load those files and do the run around correctly.  From playing with OOWriter, it seems KWord supports about twice as many features as OOWriter currently does.  So we turned the tables :)

While writing this feature I was using the mouse to move the anchored frame around; which naturally is not something you should allow to happen as the text flow would be decided the place of the anchored frame.  But it gave me an idea that turned out to be pretty sweet.  The power of anchored frames comes from their strong ties to the text.  And therefor you want your image or other anchored frame to be close to the line or paragraph that you inserted the anchor into.
What I did was if you move the anchored frame around it looks which alignment rule fits best for the new position and it sets that position.   Meaning you can drag your anchored image to the right and it will align right.
This really turned out to be a good way to alter the alignment properties without people having to look at a dialog with 12 radio buttons to choose from. It also creates better structured and more portable documents.

You know what the coolest feature is?  When you use one of the arithmetic shapes that you can alter from a star to a flower or have a sharper point for that arrow.  And you use one of those shapes as anchored shape.  Which means you can alter the shape, rotate it etc. And the text will just run around it again.  No longer loading/saving orgies between word processor and vector application!  Which means you can choose any workflow you want, it just works.

Have a good Easter!<!--break-->