---
title:   "Konqueror browsing tip"
date:    2005-10-29
authors:
  - zander
slug:    konqueror-browsing-tip
---
I'm sure I'm not the only one typing incorrect urls in the location bar sometimes.  Typing a .com where it needed a .net or similar.
You only find out you mistyped it when you go to the page. So you go back and fix the line.

Next time you try to type the url, Konqueror has a tendency to propose the wrong url (the one you visited one time, instead of the one you visited later and browsed on for an hour).  This is especially annoying if you don't use the dropdown list auto completion.

Solution; open the history browser by pressing F9 and selecting the history tab that appears on the left.  Search for the incorrect site and right click it to "Remove Entry".
After this you will not be bothered with that incorrect url anymore.

Happy browsing!
<!--break-->