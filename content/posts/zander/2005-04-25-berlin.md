---
title:   "Berlin"
date:    2005-04-25
authors:
  - zander
slug:    berlin
---
I'm in Berlin for week,  enjoying a city that many friends said is definitely worth visiting.  Well, they were right :)<!--break-->
The first thing I noticed was the traffic.  I took my car to Berlin and the first day (without a good map) I got lost all the time. Totally frustrating since I normally navigate in new cities without any problems.  Normally, when I miss a turn, I just take the next one and make sure to go left and right one more to get back to the right road.  This strategy completely breaks down in Berlin since almost no road is straight (nor circular, for that matter).

At the end of the first day I bought a city-plan.  A 200 page one, so every road can be found.  I didn't get lost yet :-)

The blog from Jan is completely right, people who care about usability (and learnability) see issues everywhere.  Saturday I was surprised reading a sign of the name of a road which in Berlin often have a range of the housenumbers under them.  This one had a range of '41 - 1'.
Since we were looking for number 40, at first glance we were saying this was the wrong direction, and we should be heading back, but a second look made us aware that the range was 1 up-including 41.  It was confusing to see it written highest number first..  Definitely a learnability issue!

A little story about nothing in particular,  just me having my very personal perception on life, the universe and everything!  Now lets see if we can convince one lovely friend to start blogging as well.  You know who you are :)