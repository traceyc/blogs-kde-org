---
title:   "KWord text progress"
date:    2007-03-29
authors:
  - zander
slug:    kword-text-progress
---
I've been a bit quiet lately. Sorry for that, I was more focussing on getting nice things done which was needed for me to make sure my open source efforts stay enjoyable :)

Today I finished up a new feature for KWord that especially technical users may find interresting.  In the past most new features in KWord were mostly about the big things;  graphics, pagespreads and all that DTP work.  I've had some people send out frustrated emails stating that they want to have some focus on actual text features instead of that dtp work.  Fair enough.

So, I added a feature to KOffice2 for references.  A reference is a special kind of variable in the text that looks just like normal text but automatically updates its contents when needed.  Most often seen for things like the page number.
The new feature is that you can insert a marker anywhere in the document and insert a reference to that somewhere else in the text.  You can for example say "See image&nbsp;3 on page&nbsp;4" and when you insert another image or your image moves to page 5, the reference will automatically update.
Currently the reference shows only the pagenumber.  Lets see if a volunteer can be found to make it more intelligent and featurefull ;)
This new feature additionally allows for a real index, the ones you tend to find at the back of a manual.  Which I think we should create scriptable bindings for generate.  So someone can write that functionality in Python or in Java.
<!--break-->
