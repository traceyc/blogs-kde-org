---
title:   "Programmers should do PR top"
date:    2007-01-12
authors:
  - zander
slug:    programmers-should-do-pr-top
---
The <a href="http://dot.kde.org/1168284615/">dot story</a> about KWord/KOffice was received pretty well. We hit 100 comments just now :)
One thing that people noted was that the screenshots we posted were not the best quality they could be.  There are various font rendering issues that show up in them.  We basically got lots of people shout at us for promissing better font technology while the same basic problems we had in the 1.x series showed up!

And they are right.  There is certainly work to be done in scribe; the text rendering engine. As well as in the libraries it uses.  But that doesn't do justice to the amazing work already done.  We actually see kerning showing up properly for fonts where we didn't have such before.  So lets select those fonts for KWord screenshots! And not fonts like DejaVu which apparently uses a different kerning method.

As a followup to my last blog about counters, I got an email which seems to be a coincidence on the timing front. The email was from someone interrested in finding out how well KWord would be supporting the Right-to-Left languages and lists and any combination of those two.
So I took the opportunity and made our text engine use the BiDi features required for that, as well as implemented several list-styles for foreign scripts.
The effect is that I could reply to the email with a "Well, we support all of those features now!"!

So, let me introduce you to these nice screenshots;
http://www.koffice.org/kword/pics/200701-Chapters.png
http://www.koffice.org/kword/pics/200701-Counters.png
http://www.koffice.org/kword/pics/200701kword_with_gentium.png
<!--break-->