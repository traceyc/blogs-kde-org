---
title:   "KWord refreshments for 2.0"
date:    2006-05-30
authors:
  - zander
slug:    kword-refreshments-20
---
I've been blogging about the library flake a little over the last weeks; its a library thats going to be the graphical object handling library for KOffice 2.0.

The <a href="http://wiki.koffice.org/index.php?title=Flake">Wiki</a> page shows the full set of features, but the first feature there lists:
<ul>
model view; multiple views of the same Shape, with different zoom levels.
</ul>
I've retrofitted KWord to start using this library just recently and just minutes ago I committed the change to actually create new shapes in KWord.  Currently those are just simple squares; but the plan is to make the tool create actual KWord-Frames soon.

Funny thing is that the multiple-view stuff already kind of works out of the box; I didn't need to do much for that and I've got a screeny to prove it!
<img src="https://blogs.kde.org/system/files?file=images//kword-flake.preview.jpg"><br>

Work continues, I'll keep you uptodate on further milestones :)<!--break-->

Update; wiki link fixed.