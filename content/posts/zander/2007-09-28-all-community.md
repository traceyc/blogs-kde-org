---
title:   "All is in the community."
date:    2007-09-28
authors:
  - zander
slug:    all-community
---
As you probably know, I work on KOffice. Have been doing that since before the first release and I did most of the (flake)libs and KWord work in the sprint to the upcoming Koffice2.0 release.

A big reason for working on KOffice is that its fun!  Another, at least equally big reason is that I believe KOffice is the best open source office suite for the long term.  And so follows the question; <b>What makes a successful open source project?</b>.

If we look at what others have said on the subject we come to the conclusion that the things that count most are these 3 points;<ul>
<li>Good idea</li>
<li>Running code</li>
<li>A community</li></ul>

Today I want to highlight the "community" part, as various others have been doing on blogs in the last couple of days.  Without community there is no future in the project, since new contributions come from the community members. So without community the new contributions (code/docs/bugreports) dry up. Its that simple. Sustaining a community next to creating great code is a balancing act.

What I love about FLOSsoftware is that you can create a great piece of work and others will try to build on top of it, including improving your code. The most obvious way of doing that is reporting bugs, but a lot of people are actually providing feedback on your work as you do it. Which creates a really great atmosphere of working where you create stuff that you could not have created were you working on your own in a closed source setting.  I always appreciate people giving <a href="http://blogs.kde.org/node/2496">constructive</a> feedback on my work.

As a stark contrast I have been getting more bothered by the rise of people that seem to feel that just writing code is good enough.  If someone feels the need to provide constructive feedback, or just even ask why a certain solution was chosen I have been amazed by the responses such community members write.  It tends to be along the lines of "This is my code, let me do what I like in peace!"

You might have seen this yourself, but examples always help (removed all names for obvious reason).

<b>Edit</b>; it turned out that using two real life examples was not a good idea, I removed them to refocus this article on the real content <b>edit</b>.

If you have the attitude that you don't question other people you will naturally see the very healthy and normal open source attitude of peer review as personal attacks.  For the simple reason that the only reason you would ever discuss items is if you disagree with them.  So others must obviously disagree with you if they ask you about your work.

These two ways of thinking (the collaborative and the "I do my part" ways) are obviously in conflict. Whenever differently minded community members meet bad emotions and disruptive events happen.  I've been in a couple of those in the last month and I can tell you they are very much draining me of my creativity.
Worse, I note that this behavior is spreading;  people that are behaving in the way that what they do is obviously the right thing and feedback is not going to lead to an open discussion make other people more closed minded as well. And we loose a bit of our community every time such a conflict happens.

What do you think we can do to improve this situation?<!--break-->