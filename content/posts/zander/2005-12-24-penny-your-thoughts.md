---
title:   "A penny for your thoughts!"
date:    2005-12-24
authors:
  - zander
slug:    penny-your-thoughts
---
As if you did not hear already, KOffice is more alive than ever.  Its honestly brewing with activity.  Krita has gotten more new features in the last couple of months then are set to go into Longhorn and the commit rate is growing each week.  It shows; there has been commercial interrest in making KOffice stay ahead of the curve.  There now is a <a href="http://dot.kde.org/1135283071/">competition for ideas</a> with a prize for the best or top-3 totalling $1000.

While I certainly feel I belong to the KOffice group, I'm not in the organization and I won't be reviewing the entries.  Hell, I might even enter myself.
What I see the competition is asking for is cool interaction methods and workflows.  I kind of expect some will detail the new menu thingy that m1crosoft is pushing for KOffice, but thats not very innovative, now is it?

What would be nice is if people do research and propose a really nice way to do everyday things which currently take too much manual labour.  Some ideas that could use more research and pondering might be these;
* Based on a page-template reorganize a page in KWord, with all its frames and colors and everything to look like that template.  The proposal should go into things like how to figure out what to do when not enough, or too much text is available.
* Come up with an easy to use and innovative way to select objects that are overlaying.  So if you have an image and a text area and a big colored frame all at approximately the same spot, how can you alter the settings of a frame that below the others.

Well, you can see that I'm a KWord fan, but you get the idea. So, openGL interaction methods and other cool ideas are rewarded with a prize; if you take the time to actually make a little more then just an idea out of your ideas. :)<!--break-->

Now if only a company would hire me to do KWord developing, that would really be something!