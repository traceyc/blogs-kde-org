---
title:   "Listening to entertaining biology"
date:    2007-02-11
authors:
  - oever
slug:    listening-entertaining-biology
---
Today, Midas Dekkers read the last of 1250 entertaining audio columns on man and nature. Many of his columns are available on the website of <a href="http://www.vroegevogels.nl/">Vroege Vogels</a>. You can download either an mp3 of the two hour show of more than 100 mb in size, or you can stream the wma version and skip to the column. That's at least the theory. Under Linux, I was so far unable to conveniently listen to the wma stream. Now I've found VLC and made a small perl script that calls VLC from the URL.
<pre>#! /usr/bin/perl -w
use strict;

if (@ARGV != 1) {
    print "Usage: $0 &lt;url>\n";
    exit;
}

my $url = $ARGV[0];
my $offset = "";
if ($url =~ m/(\d+):<a></a>(\d\d?):<a></a>(\d\d?)/) {
    $offset = $1*3600 + $2*60 + $3;
    $offset = "--start-time ".$offset;
}

system("vlc $offset $url");</pre>

<a href="http://cgi.omroep.nl/cgi-bin/streams?/radio1/vara/vroegevogels/20070128-08.wma?start=1:28:14.158">Here</a> is an example of a URL that you can play with this script.
