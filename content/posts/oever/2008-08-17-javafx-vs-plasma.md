---
title:   "JavaFX vs Plasma"
date:    2008-08-17
authors:
  - oever
slug:    javafx-vs-plasma
---
Today I came across <a href="http://www.weiqigao.com/blog/2008/08/05/watch_javafx_sdk_run_on_linux.html">an article</a> explaining how to run <a href="http://www.javafx.org/">JavaFX</a> on Linux. I managed to install the sdk by downloading <a href="https://cds.sun.com/is-bin/INTERSHOP.enfinity/WFS/CDS-CDS_Developer-Site/en_US/-/USD/ViewProductDetail-Start?ProductRef=javafx_sdk-1.0-pre1-oth-JPR@CDS-CDS_Developer">the Mac version</a>. Next, I started the stopwatch example:
<code>cd /tmp
unzip javafx_sdk-1_0-pre1-macosx-universal.zip
export JAVAFX_HOME=/tmp/javafx-sdk1.0pre1
export PATH=/tmp/javafx-sdk1.0pre1/bin/:$PATH
cd /tmp/javafx-sdk1.0pre1/samples/StopWatch
unzip StopWatch.zip
cd StopWatch
ant run</code>

<img src="http://www.weiqigao.com/blog/images/javafx-sdk-running-on-linux.png"/>

This stopwatch uses 90% of my CPU when running. It is implemented using one png file for the background and six fx files. These FX files are a mix of code and graphics. Here are two excerpts that show this:
<code>        frontContent = Group {
            id: "Front"
            content: [
                // MAIN DIAL
                Group {
                    translateX: 40 translateY: 60
                    cache: true
                    content: [
                        Circle {
                            centerX: 140
                            centerY: 140
                            radius: 140
                            fill:LinearGradient { startX:0 startY:0 endX:1 endY:1
                                stops: [
                                    Stop { offset:0 color: Color.web("#3c3c3c") },
                                    Stop { offset:1 color: Color.web("#010101") }
                                ]
                            }
                        },
                        // Tick Marks
                        for (i in [1..numOfMarks]) {
                            Rectangle{x: 0-2 y: 108 width: 4 height: 13
                                    fill: Color.web("#9fff81")
                                    rotate: (360/numOfMarks)*i
                                    translateX: 140
                                    translateY: 140}
                        },</code>
<code>public class StopwatchWidget extends CustomNode {
    private attribute model:StopwatchModel = StopwatchModel{};
    private attribute theme:Theme = LightTheme{model:model};

    public function create():Node {
        return Group {
                content: frontContent
        }
    }
}</code>

The first fragment looks like a mix of Java, JavaScript, JSON and SVG. The second one like a mix of a Java and Javascript.

To me it seems that using SVG and Javascript would be much simpler and show a cleaner separation of code and graphics. So here is my challenge to you: reimplement this stopwatch with SVG and some programming language in either a webpage or as a plasma widget. The result should not use more than 20% of my CPU with an active stopwatch.

Update: here is an example of an animated clock in pure SVG. It's not a stopwatch yet, but it is very close.
<object src="http://tavmjong.free.fr/INKSCAPE/DRAWINGS/clock.svg"><a href="http://tavmjong.free.fr/INKSCAPE/DRAWINGS/clock.svg">animated clock in svg</a></object>
<!--break-->
