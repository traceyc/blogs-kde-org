---
title:   "Chilling in Prague"
date:    2008-05-16
authors:
  - oever
slug:    chilling-prague
---
Today is day one of <a href="http://foscamp.org">FOSSCamp</a>. FOSSCamp is an unconference, which means there is no set program. The program consists of writing entries into the empty program grid. This is refreshing approach which seems to work rather well for one particular use case: recognizing and fixing problems. 

I've attended two particularly useful sessions. The first was about Xesam and the second was about KDE/GNOME interaction. Now these sessions mainly consist of sitting around a table talking about a particular subject. Since there are many knowledgeable people around, you hear many interesting facts and suggestions. Here are a few that are relevant to Strigi in an unordered list.

<ul>
<dt>common index file format</dt>
<dd>Distributions want to install preindexed files such as documentation. All programs that implement Xesam would ideally implement support for one common format. Such a format may be heavily optimized for reading only and there should be a mechanism to tell the indexer about the presence of the index.</dd>
<dt>standardizing uris</dt>
<dd>Xesam specifies that hits are identified by their uri. This allows flexibility. It allows an indexer to have many different types of information. It can have mails behind <tt>imap://</tt> or files on another computer with <tt>fish://</tt>. These custom uris are, however, not standardized at all and it is unclear how a Xesam client should deal with a hit containing such a file.</dd>
<dt>ultra lightweight</dt><dd>Gustavo Babieri wrote <a href="http://staff.get-e.org/?p=users/barbieri/lightmediascanner.git;a=summary">a lightweight indexer</a> for indexing music files on the Nokia Internet Tablet. I should have a look at that code. He claims we need a c interface to Xesam to remove DBus overhead on embedded systems.</dd>
<dt>management interface</dt>
<dd>To make Xesam attractive for program authors, we need an API that the allows applications to ensure that particular files are indexed.</dd>
</dl>

The simple Xesam API held up well under discussion which is a good sign.


</ul>

By the way, my current employer, <a href="http://www.panalytical.com">PANalytical</a>, allows me to book one day of FOSSCamp as work even though I'm here for personal interest in Free Software.
<!--break-->