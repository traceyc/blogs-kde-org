---
title:   "Strigi 0.5.11"
date:    2008-07-20
authors:
  - oever
slug:    strigi-0511
---
A new version of Strigi, the desktop search, is <a href="http://www.vandenoever.info/software/strigi">available</a>.
This is a bugfix release. It fixes some annoying issues seen in KDE 4.1. Check out the <a href="http://www.vandenoever.info/software/strigi/ChangeLog">ChangeLog</a> for the details.

Strigi has currently good basid Xesam support, but no good dedicated search interface for KDE 4.1 yet. This is ongoing work.

If you are interested in helping out with Strigi, just drop by on #strigi. We are currently focussing on bug fixes, Xesam support and KDE integration.
