---
title:   "writing applications with SVG"
date:    2009-04-28
authors:
  - oever
slug:    writing-applications-svg
---
Recently I received positive feedback on my program <a href="http://www.vandenoever.info/software/cubetest/">CubeTest</a>. The program is being used in primary schools to help children to achieve better spatial insight. There is a <a href="http://www.turnhout.be/docs/bestanden/Cubetest.pdf">teachers manual</a> on-line.

CubeTest was originally written in Qt3 and ported to Qt4 later. Because some cube decorations are SVG images, the Qt4 version needed to use Q3Picture, a class for backwards compatibility with Qt3. The renewed interest prompted me to suggest to add CubeTest to KDE-Edu and clean up the code. Now I was faced with a chose: keep Q3Picture or move the cubes to a QGraphicsView.

I chose neither. Instead, I started an experiment with SVG. SVG is, in theory, well suited for this type of application. Since SVG was recommended by W3 in 2001, adoption was slow. Currently however, Firefox, WebKit and Opera support SVG rather well (as I found out and you will see).

So I started porting CubeTest to an SVG application. That's right, an SVG <em>application</em>. SVG, like HTML, supports embedding of javascript and for CubeTest purposes this more than suffices. The entire application can be one SVG file (unless you add raster graphics like png images).

An excellent advantage of writing an application in SVG is that you can use it as a widget in Qt easily but also allow people to run it from their web browser or as a KDE plasmoid. You can wrap an SVG application in a Qt program with 10 lines of code:

<pre>#include &lt;QtGui/QApplication>
#include &lt;QtWebKit/QWebView>
int
main(int argc, char** argv) {
    QApplication app(argc, argv);
    QWebView *view = new QWebView();
    view->load(QUrl("file:///tmp/cubetest.svg"));
    view->show();
    return app.exec();
}</pre>

This application will show a window with the SVG application running inside it. Resizing and mouse events are handled as you would expect they would.

In conclusion, <a href="http://ktown.kde.org/~vandenoever/cubetest/cubetest.svg">here</a> is the CubeTest port as it currently stands. To view it, use Firefox >= 3, Arora >= 0.6 or Opera >= 9.6. Or use the 10-line program above with Qt 4.5.

<a href="http://www.vandenoever.info/software/cubetest/svg/cubetest.svg">CubeTest SVG</a>

(you can rotate the cubes)
<!--break-->
