---
title:   "Working on KOffice"
date:    2009-07-01
authors:
  - oever
slug:    working-koffice
---
Today is the first day of my employment at a wonderful company called <a href="http://www.kogmbh.com">KO GmbH</a>. KO GmbH provides services around software dealing with office documents, notably <a href="http://www.koffice.org">KOffice</a>. I'm excited to have found such an inspiring job working in Free Software. 

At the KOffice 2009 Sprint in Berlin last June, I got to meet many of the KOffice developers and was impressed by the productive atmosphere. In my job at KO, I'd like to help KOffice become enterprise-ready, by which I mean, that I want help the KOffice team make a reliable and flexible office suite.

My role in the company is software architect. The business cards Tobias Hintze, our CEO, sent me just say 'architect'. That inspired me to spend some time on this first workday to pose for a picture that goes well with <a href="http://www.hotellounge.com/songs/thearchitect.html">a dEUS song</a>  about <a href="http://en.wikipedia.org/wiki/Buckminster_Fuller">Buckminster Fuller</a>, the architect after whom the <a href="http://en.wikipedia.org/wiki/Buckminsterfullerene">buckyball</a> was named.

<img src="https://blogs.kde.org/files/images/a2.jpg" alt='The Architect'/>

<a href="http://www.last.fm/music/dEUS/+videos/5146639">Listen to 'The Architect'.</a>

<!--break-->
