---
title:   "Akademy again"
date:    2013-07-11
authors:
  - oever
slug:    akademy-again
---
Last year I skipped Akademy and felt sorry about that. So this year I simply have to go. It will be great to immerse in the talks about and related to KDE. My interest is currently on getting things done, semantic technologies and file formats. It's holiday, so a bit less JavaScript for a week and a bit more of everything else.

We just put a browser based <a href="http://www.webodf.org/blog/2013-07-10.html">WebODF editor demo</a> live, so I can enjoy Akademy and let my mind linger on different topics for a week.