---
title:   "Rejoice: A fresh Strigi"
date:    2009-02-02
authors:
  - oever
slug:    rejoice-fresh-strigi
---
Another epic owl release! Granted, not as epic as <a href="http://bjacob.livejournal.com/9510.html">Eigen 2.0</a>, but still very nice.
<a href="http://kde-apps.org/content/show.php/Strigi?content=40889">Strigi 0.6.4</a> gives some nice index speed-ups and fixes a few annoying bugs. There is one new feature: <a href="http://blogs.kde.org/node/3853">LZMA support</a>.

See the <a href="http://www.vandenoever.info/software/strigi/ChangeLog">ChangeLog</a> for more details.

The coming months will see more smaller Strigi releases. They will have emphasis on useful analyzers and improving KDE integration.

If you want to catch up with me to discuss desktop search, file formats or semantic technologies, I will be at <a href="http://www.fosdem.org">FOSDEM</a> this weekend.


<!--break-->