---
title:   "Strigi Image Search"
date:    2006-09-28
authors:
  - oever
slug:    strigi-image-search
---
Development of search technology is advancing at a mindmaming pace. The groupphoto of aKademy is now powered by Strigi. This allows you to search developers in the picture. You can search for names, projects or vaguely related ideas such as 'undulating'.

Of course this is not really Strigi doing the work. It's javascript scanning the imagemap and adding a searchbox.
<a href='http://static.kdenews.org/jr/akademy-2006-group-photo.html'><img width='600px' src='http://static.kdenews.org/jr/akademy-2006-group-photo-big.jpg' alt='groupphoto'/></a>

<!--break-->