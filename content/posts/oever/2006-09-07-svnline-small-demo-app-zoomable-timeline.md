---
title:   "SvnLine: a small demo app for a zoomable timeline"
date:    2006-09-07
authors:
  - oever
slug:    svnline-small-demo-app-zoomable-timeline
---
This evening I ported a widget I wrote with HTML and javascript to Qt 4.2. I wrote this widget last year. It can show a timeline into which you can zoom by scrolling the mousewheel. This is useful for a number of things like calendar information, browsing files by creation date or size and as shown in this application, for looking at SVN commits.

<a href="http://www.vandenoever.info/software/svnline.tar.bz2">SvnLine</a> retrieves the SVN log as XML and shows the messages on a time line.
Here are some screenshots showing the new widget as a QGraphicsScene:
<a href="https://blogs.kde.org/system/files?file=images/svnline1.png"><img class="showonplanet" src="https://blogs.kde.org/system/files?file=images/svnline1.preview.png"/></a>
<!--break-->
<a href="https://blogs.kde.org/system/files?file=images/svnline.png"><img src="https://blogs.kde.org/system/files?file=images/svnline.preview.png"/></a>