---
title:   "Good karma"
date:    2009-10-25
authors:
  - oever
slug:    good-karma
---
This weekend I visited my parents in law, because my wifes paternal grandmother celebrated her 90th birthday. I noticed that the laptop they use was still running Kubuntu Feisty with OpenOffice 2.2. On this machine, reading emails, managing photos, surfing the internet and working on office documents are most important. Digikam is used for photos. Kmail and konqueror from KDE 3.5 are installed and a mix of OpenOffice and Microsoft Office 97 on wine is in use for editing office documents. in short, a horribly outdated setup of more than two years old. IT is still moving fast.  Feisty was not a long term release and no updates for it anymore.

So in a slightly reckless move I decided to update the machine to the next Kubuntu: karmic koala.  This meant going to KDE 4.3.  To my relief the install went very well.  All important settings for digikam and kmail were migrated automatically.  Dolphin is really nice and more intuitive for non-professional users. The kwin effects add a nice touch of class (translucent wobbly windows). Plasmoids on the desktop (photo frames and weather forcast) were very well received.

In short: good karma! Thank you very much, Kubuntu team.

(my last two blogs were written on the Nokia N900 which has a good keyboard)
<!--break-->
