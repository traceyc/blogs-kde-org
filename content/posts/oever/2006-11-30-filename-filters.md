---
title:   "Filename filters"
date:    2006-11-30
authors:
  - oever
slug:    filename-filters
---
Flavio Castelli added a nice feature to Strigi: the ability to include or exclude files from the index based on their file name. This feature has been in Strigi since the last two releases. And while Flavio has been busy writing a report about it, I have smoothed out the feature a bit and made it more universal so that it now also works on names of files that are embedded in other files. Here is a screenshot that shows the dialog for configuring these filters. (input from the usability teams is welcome ;-)

<img src="https://blogs.kde.org/system/files?file=images//filters_0.png"/>

The screenshot is of the current version in SVN. We'll have a new release shortly.

<!--break-->

