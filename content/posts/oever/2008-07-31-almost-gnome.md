---
title:   "Almost a gnome"
date:    2008-07-31
authors:
  - oever
slug:    almost-gnome
---
While reading <a href="http://esr.ibiblio.org/">ESR's blog</a> I came across <a href="http://www.easydamus.com/character.html">a questionnaire</a> to find out what kind of D&D character I'd be. I've only once joined in on a D&D role-playing evening and found it was not for me. This query is interesting though because of the many questions you have to answer and for which you have to make up your mind about which of the given options applies best to you.

Here are the results. Scroll to the bottom to find out about the <a href="http://altijdeenmening.web-log.nl/mijn_weblog/images/kabouter_15_kopie.jpg">gnome</a> part.

<b>I Am A:</b> Lawful Good Human Wizard (5th Level)
<!--break-->
<br><br><u>Ability Scores:</u><br>
<b>Strength-</b>13<br>
<b>Dexterity-</b>14<br>
<b>Constitution-</b>14<br>
<b>Intelligence-</b>15<br>
<b>Wisdom-</b>15<br>
<b>Charisma-</b>17
<br><br><u>Alignment:</u><br><b>Lawful Good</b> A lawful good character acts as a good person is expected or required to act. He combines a commitment to oppose evil with the discipline to fight relentlessly. He tells the truth, keeps his word, helps those in need, and speaks out against injustice. A lawful good character hates to see the guilty go unpunished. Lawful good is the best alignment you can be because it combines honor and compassion. However, lawful good can be a dangerous alignment because it restricts freedom and criminalizes self-interest.<br>
<br><u>Race:</u><br><b>Humans</b> are the most adaptable of the common races. Short generations and a penchant for migration and conquest have made them physically diverse as well. Humans are often unorthodox in their dress, sporting unusual hairstyles, fanciful clothes, tattoos, and the like.
<br><br><u>Class:</u><br><b>Wizards</b> are arcane spellcasters who depend on intensive study to create their magic. To wizards, magic is not a talent but a difficult, rewarding art. When they are prepared for battle, wizards can use their spells to devastating effect. When caught by surprise, they are vulnerable. The wizard's strength is her spells, everything else is secondary. She learns new spells as she experiments and grows in experience, and she can also learn them from other wizards. In addition, over time a wizard learns to manipulate her spells so they go farther, work better, or are improved in some other way. A wizard can call a familiar- a small, magical, animal companion that serves her. With a high Intelligence, wizards are capable of casting very high levels of spells.
<br><br>Find out <a href='http://www.easydamus.com/character.html' target='mt'>What Kind of Dungeons and Dragons Character Would You Be?</a>, courtesy of Easydamus <a href='mailto:zybstrski@excite.com'>(e-mail)</a><br><br>

<code>
Alignment:
Lawful Good ----- XXXXXXXXXXXXXXXXXXXXXXXX (24)
Neutral Good ---- XXXXXXXXXXXXXXXXXXXXXX (22)
Chaotic Good ---- XXXXXXXXXXXXXXXXXX (18)
Lawful Neutral -- XXXXXXXXXXXXXXXXXXXXXXX (23)
True Neutral ---- XXXXXXXXXXXXXXXXXXXXX (21)
Chaotic Neutral - XXXXXXXXXXXXXXXXX (17)
Lawful Evil ----- XXXXXXXXXXX (11)
Neutral Evil ---- XXXXXXXXX (9)
Chaotic Evil ---- XXXXX (5)

Law & Chaos:
Law ----- XXXXXXXXXX (10)
Neutral - XXXXXXXX (8)
Chaos --- XXXX (4)

Good & Evil:
Good ---- XXXXXXXXXXXXXX (14)
Neutral - XXXXXXXXXXXXX (13)
Evil ---- X (1)

Race:
Human ---- XXXXXXXXXXXXX (13)
Dwarf ---- XXXXXX (6)
Elf ------ XXXXXX (6)
Gnome ---- XXXXXXXXXXXX (12)
Halfling - XXXXXX (6)
Half-Elf - XXXXXXXXX (9)
Half-Orc - XXXX (4)

Class:
Barbarian - (-23)
Bard ------ (-21)
Cleric ---- (0)
Druid ----- (-25)
Fighter --- XX (2)
Monk ------ (-2)
Paladin --- XX (2)
Ranger ---- (0)
Rogue ----- (-2)
Sorcerer -- XX (2)
Wizard ---- XXXX (4)</code>