---
title:   "ODF visualization using WebKit"
date:    2010-06-24
authors:
  - oever
slug:    odf-visualization-using-webkit
---
Today is day 1 of of the <a href="http://blogs.kde.org/node/4249">OdfKit Hack Week</a>. We wrote a list of things we want to achieve this week. In order to avoid embarrassment, we'll spare you the details and go straight through to an explanation of how you can use WebKit (or any modern browser) to visualize ODF documents. The general idea is to incorporate the ODF XML into a live HTML document.
&nbsp;
<b>Step 0: load content and styles into an HTML document</b>
&nbsp; 
ODF files come in two flavors: single XML files and XML files in ZIP containers. Most people use the ZIP form exclusively. In both cases there are two XML elements of importance: &lt;document-content/> and &lt;document-styles/>. These are comparable to HTML and CSS respectively. We'll avoid the details of how we load these elements into the DOM tree for now and simply state that we have two javascript variables:
<code>
 var documentcontents;
 var documentstyles;
</code>
&nbsp;
<b>Step 1: put the document contents in the web page.</b>
&nbsp;
Let's start from a simple HTML document with a &lt;div/> element where the ODF element &lt;document-content/> can be imported.
<code>
 <html>
  <head><title>My ODF Document</title></head>
  <body>
   <div id='contentxml'></div>
  </body>
 </html>
</code>
The importing can be done like this:
<code>
 documentcontents = document.importNode(documentcontents, true); // deep import
 var div = document.getElementById('contentxml');
 div.appendChild(documentcontents);
</code>

The result is that the DOM tree has now mixed namespaces: elements from the ODF namespaces are imported into the HTML namespace. HTML viewers display the text inside the foreign elements even though there are no rules for rendering the foreign elements.
&nbsp;
<b>Step 2: adding document styles</b>
&nbsp;
Even though the added elements are in a foreign namespace, it is still possible to use CSS to give them styles. This is possible by <a href="http://www.w3.org/TR/css3-namespace/">using namespaces in CSS</a>:
<code>
  <style type="text/css">
    @namespace text url(urn:oasis:names:tc:opendocument:xmlns:text:1.0);
    @namespace table url(urn:oasis:names:tc:opendocument:xmlns:table:1.0);
    text|p, text|h { // define ODF paragraphs and headers as block elements
      display: block;
    }
    text|list { // reset the list numbering for each list
      counter-reset: list-num;
    }
    text|list-item {
      display: block;
    }
    text|list-item:before { // prefix each list-item with a roman numeral
      content: counter(par-num, upper-roman) ". ";
    }
    table|table-row { // style ODF table rows as table rows
      display: table-row;
    }
    table|table-cell { // style ODF table cells as cells
      display: table-cell;
    }
  </style>
</code>
This simple CSS example already improves the visualization of the ODF elements a lot. It is, however, very general. There are only general ODF styles, but no user-defined styles. No panic! CSS selectors are flexible to allow these too. Here is an example of specific ODF style ported to CSS. The original ODF
<code>
  <style:style style:name="MyItalicStyle" style:family="text">
    <style:text-properties fo:font-style="italic" />
  </style>
</code>
becomes
<code>
  <style type="text/css">
    @namespace text url(urn:oasis:names:tc:opendocument:xmlns:text:1.0);
    text|p[text|style-name="MyItalicStyle"] {
      font-style: italic;
    }
  </style>
</code>
So you see that it is possible to display ODF documents in a web browser or web widget. There is a demo with a more elaborate visualization at <a href="https://demo.webodf.org/demo/">our demo page</a>.
<!--break-->
