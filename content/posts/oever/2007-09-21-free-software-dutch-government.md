---
title:   "free software in dutch government"
date:    2007-09-21
authors:
  - oever
slug:    free-software-dutch-government
---
2007 has seen huge events for Free Software. The java source code was (mostly) released by SUN, ATI/AMD opened up to the Free Software community, and the Dutch government has started using the term Free Software (vrije software) in its plans.

Especially the latter is a sweet <!--break--> sweet change in attitude. Earlier today, Richard Dale <a href="http://blogs.kde.org/node/3005">blogged</a> from Tenerife about the ever raging discussion about the use of the terms "Open Source" and "Free Software". This reminded me of a wonderful crusade I held with Richard Stallmann (<a href="http://jimmysweblog.net/2004/10/richard-stallman.jpg">desktop background picture</a>) for Free Software in the Netherlands. It coincided wonderfully with the work of <a href="http://www.gendo.nl/index.php?option=com_content&task=blogcategory&id=29&Itemid=32">Arjen Kamphuis</a> who lobbies the Dutch government about Free Software. Now, five years later, the Dutch authorities are still moving, albeit slowly, in the direction of freedom.

Let's go back to spring 2002. The <a href="http://www.greens-efa.org/en/issues/?id=14">european Greens</a> organized a conference about software patents in the buildings of the European Commission in Brussels and as a fervent proponent of innovation, I went there. At the time, there was a threat that the EU would enact a law legalizing software patents and the Greens conference was meant to discuss the effects on society of such a law.

One of the speakers was Richard Stallman, who gave a speech about Free Software. Like most attendees, he spoke about the stiffling effects of software patents on innovation.

Since, at the time, the Netherlands was rather ignorant about Free Software and Stallman is an excellent speaker, I sought him out and convined him to come to the Netherlands to enlighten Dutch lawmakers about the necessity of opening up the logic that steers our computers.

He happily accepted and I went and organized meetings with policy makers of the ministry of interior affairs and the representatives of the Green Party. Especially the former meeting was very effective in that it unleashed the unyielding logic of RMS on government employees with power. This is where, to my fancy, the term 'free software' took hold in Dutch government. Seeing it used in official documents now, confirms this fancy.

Nevertheless, <a href="http://www.minez.nl/dsc?c=getobject&s=obj&objectid=153181&!dsname=EZInternet&isapidir=/gvisapi/">progress</a> is slow and we should continue forcing local governments to work with open standards. Even the <a href="http://groenlinks.nl">most active party</a> still uses the term "open source" and causes confusion. It should be made clear that the government should be open in its communication with its citizens and that it should use open standards for this communication. Publishing parliament proceedings in a closed format will not do!

We need to keep talking to government officials in the right terms. Remember your freedoms and right on open communication! The Dutch government is lagging more and more and even the newest plans, using the term 'free software', are not enough. So, dear friends, continue demanding openness!
