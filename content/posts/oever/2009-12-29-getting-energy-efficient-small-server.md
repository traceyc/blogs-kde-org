---
title:   "Getting an energy efficient small server"
date:    2009-12-29
authors:
  - oever
slug:    getting-energy-efficient-small-server
---
For mirroring my backup drive, central data store for devices, music playing and a webserver for experiments, I'd like to run a small server at home. I want this server to be energy efficient, easy to modify, robust, silent and run customizable free software. It should have at least 500 GB of storage, but 1 or 1.5 TB is better. You can buy very low-energy computers such as the <a href="http://en.wikipedia.org/wiki/Fit-PC">Fit-PC 2</a> (6 watt) or the <a href="http://en.wikipedia.org/wiki/Linutop">Linutop 2</a> (8 watt). Energy costs for machines that run constantly can be roughly estimated by doubling the power draw in watt, so running a device that uses 8 watt constantly costs about 16 euro a year.

Until recently the computer I used most was a Dell X1 Latitude laptop. That machine is now 4.5 years old. At the time, I chose it because it is a laptop with no fan and hence very silent. It is still better than any atom based netbook. So I would like to use this laptop as a server. UPS and screen are integrated which is a nice plus. The machine has a 1.8" disk built in. It is not possible to replace it with a disk of at least 500 GB. I wanted to know the energy cost of adding more storage to the X1. So I did some power measurements with an 2.5" external disk (Toshiba, 160 GB) and a 3.5" external disk (TrekStore 500GB). I measured on my current main laptop, a Lenovo X200s too.

Lenovo X220s (console, idle, low brightness unless otherwise specified)
Adapter only:  6 W
Console, low brightness: 19 W
Console, high brightness: 21 W
100% cpu and high brightness: 40 W
mounted 2.5" disk: 24 W
active (dd) 2.5" disk: 28 W
mounted 3.5" disk: 37 W
active (dd) 3.5" disk: 41W

Dell Latitude X1 (console, idle, low brightness unless otherwise specified)
Adapter only: 0 W
Console, low brightness: 15 W
Console, high brightness: 19 W
100% cpu and high brightness: 23 W
mounted 2.5" disk: 17 W
active (dd) 2.5" disk: 21 W
mounted 3.5" disk: 32 W
active (dd) 3.5" disk: 37 W

The 2.5" disk uses USB for power. The 3.5" disk has a separate adapter which is included in the power measurements. The device used for the power measurements is a DEM1379.
The idle 3.5" drive uses 13-15 more watt and the active drive uses 13-16 more watt. The difference is as large as power usage of the entire server. So I am now wondering if there are more energy efficient external 3.5" drives.
<!--break-->
