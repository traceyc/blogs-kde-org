---
title:   "Histograms for easy searching"
date:    2006-12-05
authors:
  - oever
slug:    histograms-easy-searching
---
Photo applications such as <a href="http://kphotoalbum.org/">KPhotoAlbum</a> have shown that navigation by histograms <a href="http://kphotoalbum.org/kphotoalbum-large.jpg"> can be very convenient</a>. Prerequisite of such navigation is that you have fast access to numerical properties of the items you want to navigate. In Strigi, many numerical properties such as modification time, size, embedding depth, width or height are indexed. This enables Strigi to quickly make histograms of these properties. By clicking on a bar in the histogram, the user can quickly and intuitively focus on a subset the items that are shown.

Today I've committed code to SVN that allows any application talking to Strigi to easily get out histograms. With the call
<pre>
histogram getHistogram(query, fieldname, labeltype)
</pre>
you can do this. <tt>histogram</tt> is a list of string,count pairs. <tt>fieldname</tt> can be <tt>mtime</tt>, <tt>size</tt> etc. and <tt>labeltype</tt> can be either <tt>numerical</tt> or <tt>time</tt>.

Here are two screenshots:


<a href="http://blogs.kde.org/node/2564"><img src="https://blogs.kde.org/system/files?file=images//hist1.preview.png"/></a>
A histogram showing the distribution of 'depth' for the hits resulting from the query. The depth is the nesting depth of a file. 0 means the file is not nested. 2 means the file is embedded in an embedded file.

<a href="http://blogs.kde.org/node/2565"><img src="https://blogs.kde.org/system/files?file=images//hist2.preview.png"/></a>
A histogram showing the size distribution for the hits resulting from the query.


<!--break-->