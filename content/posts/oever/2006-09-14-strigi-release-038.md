---
title:   "Strigi release 0.3.8"
date:    2006-09-14
authors:
  - oever
slug:    strigi-release-038
---
<p>After more than two months of frantic development, a <a href="http://www.vandenoever.info/software/strigi/">new Strigi release</a> is available. This release has so many desirable features that you cannot help but upgrade. The numbering (0.3.8) is leading up to 0.4.0 which will be released around the time of Akademy. There I hope to start working on KDE4 integration.</p>
<p>So what's new?</p>
<dl>
<dt>DBus support</dt><dd>Query Strigi from any application.</dd>
<dt>Smoother GUI</dt><dd>It's easy to write your own GUI to Strigi, but the one we write is now quite a bit smoother.</dd>
<dt>Inotify support</dt><dd>Know about file changes instantly.</dd>
<dt>jstream://</dt><dd>Strigi indexes mail attachments and other nested files. With the KDE3 kio_slave you can open these files in any KDE3 application, no matter how deep it is.</dd>
<dt>PDF indexing</dt><dd>using pdftotext, but even faster and more complete parsing is coming soon...</dd>
<dt>Word indexing</dt><dd>using wvWare</dd>
<dt>much more...</dt><dd>and if you're still not satisfied, just mail us.</dd>
</dl>
<a href="http://www.vandenoever.info/software/strigi/"><img width="489" src="http://kde-apps.org/content/pre3/40889-3.png"/></a>
<!--break-->