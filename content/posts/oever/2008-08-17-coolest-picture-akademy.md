---
title:   "Coolest picture from Akademy"
date:    2008-08-17
authors:
  - oever
slug:    coolest-picture-akademy
---
The coolest picture made at Akademy 2008 was made by Nepomuk developer Gunnar Aastrand Grimnes. This is the main square of Mechelen stitched together from 19 pictures. Check out <a href="http://farm4.static.flickr.com/3147/2770776723_88b2558b3a_o.jpg">the details</a>. You will probably be able to buy it as a clock from the Mechelen tourist office soon.
<a href="http://flickr.com/photos/gromgull/2770776723/in/set-72157606641230628/"><img src="http://farm4.static.flickr.com/3147/2770776723_b9c96293f7.jpg?v=0" alt="Mechelen square"/></a>
<!--break-->
