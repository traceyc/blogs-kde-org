---
title:   "Porting a simple KFilePlugin"
date:    2007-03-13
authors:
  - oever
slug:    porting-simple-kfileplugin
---
Since last monday, the class KFilePlugin has been removed and all the KFilePlugin implementations have to be ported. As an example of how to port a KFilePlugin, I will rewrite <a href="http://websvn.kde.org/trunk/KDE/kdegraphics/kfile-plugins/dvi/kfile_dvi.h?view=markup">KDviPlugin</a> (<a href="http://websvn.kde.org/trunk/KDE/kdegraphics/kfile-plugins/dvi/kfile_dvi.h?view=markup">h</a>, <a href="http://websvn.kde.org/trunk/KDE/kdegraphics/kfile-plugins/dvi/kfile_dvi.cpp?view=markup">cpp</a>)as a Strigi analyzer.

On first inspection, we see that KDviPlugin only implements KFilePlugin::readInfo() and not KFilePlugin::writeInfo(). This makes it easier to port it; we only need to write either a StreamThroughAnalyzer or a StreamEndAnalyzer.

But which of the two? To determine this, we look again at the code. From one of the comments in the code, we gather that the maximum amount of data that is read by KDviPlugin is 270 bytes. This means we can implement a StreamThroughAnalyzer. For any filetype for which you read only a few kb from the start of the file, it is better to implement a StreamThroughAnalyzer, because these can work in parallel. So we will implement a DviThroughAnalyzer.

The plugin extracts two items from DVI files: "Comment" and "Pages". The first is a string, the last is an unsigned integer. (two other items, "Type" and "Modified", are also extracted, but they apply to all files and can be left out).

Now we can start of on the code. We will implement a plugin, so we do not need a header file. The code is commented so that you may understand what it does.

<a href="http://websvn.kde.org/trunk/KDE/kdegraphics/kfile-plugins/dvi/dvithroughanalyzer.cpp?view=markup">Here is the result.</a>

Now you have to add an entry to <a href="http://websvn.kde.org/trunk/KDE/kdegraphics/kfile-plugins/dvi/CMakeLists.txt?view=markup">CMakeLists.txt</a> to build and install the plugin.
<!--break-->