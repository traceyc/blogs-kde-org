---
title:   "Make and CMake"
date:    2006-11-28
authors:
  - oever
slug:    make-and-cmake
---
At aKademy, David Faure presented <a href="http://web.davidfaure.fr/scripts/makefromemacs">a script</a> for calling make from emacs when you write code in a different directory from the one you build in. Since I use vim and bash, I had to adapt it to work for me. One important aspect for vim users is that if you call ':make' from vim, it calls the first 'make' it finds in the path. This call is associated with niceness like jumping to the right error lines after calling ':make'. The script I paste here should occur in your path before the real make (usually /usr/bin/make) and it should be called 'make'. When called, it will move up in the directory hierarchy until it finds a directory called 'build'. It enters there and calls the real 'make' with the arguments you passed.

<pre>#! /bin/sh

# try to find a 'build' dir above the current dir
OLDDIR=$PWD
REALMAKE=/usr/bin/make

enterdir() {
  while ! test -d "$PWD/$1"; do
    cd ..
    if test "$PWD" = "/"; then
      # no dir 'build' was found, we give up
      cd $OLDDIR
      return
    fi
  done
  cd "$PWD/$1"
}

# find the build dir if this dir does not contain a Makefile
if test ! -e Makefile; then
  enterdir 'build'
fi

$REALMAKE $@</pre>

<b>Update:</b> I just discovered that <tt>makeobj</tt> by Coolo does almost the same.