---
title:   "CubeTest in SVG progress"
date:    2009-05-01
authors:
  - oever
slug:    cubetest-svg-progress
---
The first episode of '<a href="http://blogs.kde.org/node/3948">programming in SVG</a>' led to some nice bling improvements in KDE. Aaron showed how to put an <a href="http://aseigo.blogspot.com/2009/04/cubetest-in-browser-pffft.html">SVG program in a plasmoid</a> and Ariya taught us the incantation to make the desktop <a href="http://ariya.blogspot.com/2009/04/transparent-qwebview-and-qwebpage.html">shine through</a> such a plasmoid. And last but not least Remco Bloemen mailed me with a <a href="http://www.vandenoever.info/software/cubetest/svg/v2/cubetestallinone.svg">working demo</a> that hows how to include arbitrary data in an SVG application with <a href="http://en.wikipedia.org/wiki/Data_URI_scheme">data URIs</a>.

Work in CubeTest 2.0 has progressed. The current version has nice animations that revert the cubes to a more ordered state by clicking the corners. The next steps will be to add the question generator and a style switcher and more styles.

Programming in javascript in SVG is rather different from programming C++. There is no need to implement a function for painting. This is similar to using QGraphicsView. There is also no type checking and no compiling. There is no way to print debug output to the console. I have not looked for a javascript debugger yet. I'm sure there is one that lets you set breakpoints and inspect data values.

A issue that I need to look into is internationalization. How does the SVG application know the current locale and how can user messages be switched accordingly.

A note on conformance and speed. I have found very little difference between the Firefox, the WebKit (Qt, Arora, Chromium) and the Opera SVG implementations. If the application works in one of the browsers, it will very likely work in all of them. CubeTest does not use fancy SVG features though.

An finally a request to look at performance. The latest version has a small counter that displays the number of frames that can be shown in one animation. The maximum is 10. Chromium scores 10. QtWebKit scores 2 (very slow). FireFox 3.5b4 and Opera score 4. The slow performance of QtWebKit 4.5 is not related to my graphics card; I tested on Intel and ATI.

What do your browsers score? Does your mobile browser display the svg properly?

Check out the <a href="http://www.vandenoever.info/software/cubetest/svg/v2/cubetest.svg">new version</a>.

<a href="http://www.vandenoever.info/software/cubetest/svg/v2/cubetest.svg"><img src="http://www.vandenoever.info/software/cubetest/svg/v2/cubetest.png" alt='cubetest screenshot'/></a>
<!--break-->
