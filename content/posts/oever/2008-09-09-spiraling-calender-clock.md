---
title:   "Spiraling calender clock"
date:    2008-09-09
authors:
  - oever
slug:    spiraling-calender-clock
---
After blogging about clocks a few times, let me post <a href="http://ktown.kde.org/~vandenoever/spiral.xhtml">some code</a> for a clock. Click on the image below to see it. Your browser must support SVG.

<p style=''>This clock was inspired by <a href="http://www.kde-look.org/content/show.php?content=82071">an entry on kde-apps</a> which was <a href="http://blogs.kde.org/node/3647">blogged about</a> by Cornelius Schumacher. A lot ofthe HTML, SVG and javascript was helped by <a href="http://kamhungsoh.com/000d.xhtml">the  SVG+javascript clock</a> page by Kam-Hung Soh.</p>

<p>The page should render ok in Firefox 3.0, Opera 9, and maybe Chrome. The warped text is enabled by the SVG feature <a href="http://www.w3.org/TR/SVG/text.html#TextOnAPath">Text on a Path</a>.</p>



<a href="http://ktown.kde.org/~vandenoever/spiral.xhtml"><img style="border:0px" src="https://blogs.kde.org/files/images/spiralclock.png"/></a>
<!--break-->
