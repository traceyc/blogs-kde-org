---
title:   "The Semantic Desktop: Document Annotation"
date:    2007-08-10
authors:
  - oever
slug:    semantic-desktop-document-annotation
---
KDE 4 will have more semantic technologies than any KDE version before it. As an average user you may be baffled by the use of funny terms all the time. 'What are semantic technologies?', you are thinking. Many people that have seen what Nepomuk will do in KDE 4 will think that semantics is all about tagging and rating of files. This is the visible part the current state of the Nepomuk-KDE work Sebastian Trueg is doing. 'So what's the big deal?' you will think next, because to be honest, tagging and rating are not all that special. Nice, sure, but not mind blowing.

And what about Strigi? People say it will have semantic search. But what's the big deal about that? Currently, Strigi will let you search in specific fields. For example, you may look for a word in the title of a song. This is partially semantic. You can tell the search engine that you want a song title. Again, you say, ok nice, so this is semantics?

Yes and no. Semantics is all about meaning. We expect the computer to understand what we mean. To make this possible we create a sort of dictionary and fit what we find on the desktop into this dictionary in the right categories. Doing this automatically possible is for a lot of data, but not for everything.

In this blog, I'd like to point to you to a semantic application that lets you annotate you documents in a semantic way. What is show in this application is the future of your computing experience. It shows how you can tag words in a text. For example, you can select the name of a person and tag it as someone you know or when you see the word 'house' you can specify that it describes a type of music and not a type of building.

The application I am talking about is called <a href="http://gate.ac.uk">GATE</a>. 
Here is a nice movie that walks you through the possibilities of the application.

<a href="http://gate.ac.uk/demos/gate-tutorial-all/part-one-all-with-annic-and-ontology_controller.swf"><img src="https://blogs.kde.org/files/images/gate_1.png"/></a>

