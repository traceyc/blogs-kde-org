---
title:   "FOSS in Dutch government"
date:    2007-12-12
authors:
  - oever
slug:    foss-dutch-government
---
Today Dutch parliament discussed plans of the ministry of Economic Affairs to encourage Free and Open Source Software in government. <i>All</i> major parties seem to understand the issues. Even news agencies are talking about 'vrije software' which is the right term ('Vrije' means 'free as in freedom').

It took five years for Economic Affairs to answer to a parliamentary request to do this. In the meantime, the (previous) government supported the attempted European legalization of software patents despite the wishes of parliament.

So progress is slow and not so steady, but currently moving in the right direction. The current achievement is that the central government has a policy that FOSS must be preferred over commercial software, unless there no equivalent FOSS program. The policy is not law and does not hold for local governments directly.

There will be an office where people can complain if they cannot communicate with the government using open standards. So non-compliance of government agencies will be measured.

Here is little more background information from <a href="http://start.groenlinks.nl/laat-consumenten-zelf-hun-software-kiezen">the party that initiated this</a>.

<code>Actieplan Heemskerk
Woensdag wordt het actieplan "Nederland in open verbinding" door de Tweede Kamer behandeld, dat tot doel heeft het gebruik van open standaarden en open source software bij de (semi-)publieke overheid te versnellen. Heemskerk wil de interoperabiliteit binnen de overheid te vergroten door het gebruik van open standaarden en de afhankelijkheid van leveranciers verminderen door het gebruik van open source software. Zo wil hij bovendien een ‘gelijk speelveld op de softwaremarkt bevorderen'. 
Onderdeel van het actieplan is de invoering van een ‘comply-or-explain and commit'-principe, die voorschrijft dat overheden (rijksdiensten vanaf april 2008 en overige overheden en instellingen vanaf december 2008) zich moeten verantwoorden wanneer ze open standaarden niet ondersteunen en moeten beloven dat in de toekomst wel te doen. Op januari 2009 moeten er daarnaast implementatiestrategieën zijn geformuleerd voor de aanbesteding, inkoop en het gebruik van open source softare door alle ministeries. Een jaar later moeten die er ook zijn voor semi-overheden in het onderwijs, de zorg en de sociale zekerheid.
Tot slot schrijft het actieplan voor dat ODF uiterlijk in januari 2009 wordt ondersteund door alle ministeries en ‘mede-overheden'. De OpenDocument-indeling (ODF) of het OASIS Open Document Format for Office Applications, is een open standaard voor het opslaan en uitwisselen van tekstbestanden, rekenbladen, grafieken en presentaties.</code>
<a href="http://www.computable.nl/nieuws.jsp?id=2251897">source</a>
