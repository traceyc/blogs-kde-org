---
title:   "An additional requirement on kdelibs"
date:    2007-03-05
authors:
  - oever
slug:    additional-requirement-kdelibs
---
I just commited a change to the main CMakeLists.txt of kdelibs to require Strigi. There is no code actually using it, this will come next monday (promise!). Adding this dependency now will allow us to iron out potential problems with compiling Strigi that might pop up when many KDE developers try to compile it.
To compile Strigi you need at least the following development packages: zlib1g-dev, libbz2-dev, libssl-dev, libmagic-dev, and libexpat1-dev. These are the names of the packages in Ubuntu Edgy. On some distros libmagic-dev is called file-dev or file-devel.
<!--break-->