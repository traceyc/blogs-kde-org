---
title:   "view archives and emails with attachments as folders"
date:    2006-12-12
authors:
  - oever
slug:    view-archives-and-emails-attachments-folders
---
The screenshot below seems boring, but it is not. It means something nice and will start a useability discussion. The topic is 'files in files'. How should the gui deal with files in files? Well, so far, KDE does a sloppy job of dealing with them. Different file types have different kio_slaves and some have none.

Since Strigi can search in files that are nested in other files, it should also allow the user to open these files. For this to work, it has a kio_slave called jstream:/. What this does is, it offers a common interface for opening embedded files read-only. Today I expanded this kio_slave a bit to allow the user to uses it to browse his home-directory too. Files in that contain other files are shown as folders and can be opened as folders.

<img src="https://blogs.kde.org/system/files?file=images//folders.preview.png"/>


In KDE4, this code can be used to provide fast browsing in archives. It is easy to program with and if you miss a particular file format you can add it support for it to Strigi. This will kill two birds with one stone: Strigi (and deepgrep and deepfind) will be able to search in this file format and the user will be able to look inside the archives and open files in there without extracting the archives.


<!--break-->