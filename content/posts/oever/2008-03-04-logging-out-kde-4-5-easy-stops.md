---
title:   "Logging out of KDE 4 in 5 easy stops"
date:    2008-03-04
authors:
  - oever
slug:    logging-out-kde-4-5-easy-stops
---
Today we are helping novice KDE users to log out of KDE 4. Logging out of KDE 4 is nowhere near as hard as with some other popular programs. Here you do not need esoteric keyboard commands like '&lt;esc&gt;:q!' or 'ctrl-X followed by ctrl-c'. In KDE 4 you can easily log out by using your mouse.

<img src="/files/images/logout1_0.png" alt="Step 1" align='left'/>

<div style='padding:10px;margin:2em;display:block;'>Stop 1: move you mouse to the bottom left of your desktop and click on the picture of the K in the cogwheel.</div>

<br clear='all'/>

<img src="/files/images/logout2_1.png" alt="Step 2" align='left'/>

<div style='padding:10px;margin:2em'>Stop 2: Now you see the 'kickoff menu'. From here you can <i>start</i> programs. But you can also <i>stop</i> using KDE. Move your mouse the the button labelled 'Leave'.</div>

<br clear='all'/>

<img src="/files/images/logout3.png" alt="Step 3" align='left'/>

<div style='padding:10px;margin:2em'>Stop 3: Hmm, that thing called 'Leave' was not really a button. Clicking does not help you, but simply hovering (moving your mouse over it) does help. By doing that, you unlock a new batch of buttons. And there are many of them too. Such a plethora of choice: there are various degrees of 'stopping with KDE'. You can choose between:
<dl><dt>Logout</dt><dd>This means: stop all programs and stop KDE but do not stop the computer.</dd>
<dt>Lock</dt><dd>This means: keep all programs running and stop other people from using this computer.</dd>
<dt>Switch user</dt><dd>This means: let someone else use the computer while your programs keep running in the background.</dd>
<dt>Shutdown</dt><dd>This means: turn off the computer.</dd>
<dt>Restart</dt><dd>This means: turn off the computer and start it again immediately. What the computer does when it restarts depends on how you installed it.</dd>
</dl></div>

<br clear='all'/>

<img src="/files/images/logout4.png" alt="Step 4" align='left'/>

<div style='padding:10px;margin:2em'>Stop 4: We decide we want to 'Logout' so we move the mouse to the button 'Logout'. You can press it. It really is a button.</div>

<br clear='all'/>

<img src="/files/images/logout5.png" alt="Step 5" align='left'/>

<div style='padding:10px;margin:2em'>Stop 5: Now your screen becomes gray and a single window stands out in the middle. Are we logged out now? No we're not. Not for another 58 seconds. You can check this from the countdown on the bottom of the window. If you want to experience the full satisfaction of logging out click 'Logout' once more.

Goodbye! Come back quickly!</div>

<br clear='all'/>
<!--break-->