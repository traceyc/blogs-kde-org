---
title:   "Looking for green plants"
date:    2008-07-26
authors:
  - oever
slug:    looking-green-plants
---
Do you know about <a href="http://dbpedia.org">DBpedia</a>? It's a project that lets you perform complex queries on the content of wikipedia. I've been playing with it a bit and want to share some examples. Try to come up with cool queries!

To query DBpedia, there is <a href="http://dbpedia.org/snorql/">convenient form</a>. Queries are performed in SPARQL. SPARQL is a query language that takes some getting used to. So I'll start with a few simple examples.

A query for <a href="http://dbpedia.org/snorql/?query=SELECT+%3Fa%2C+%3Fb+WHERE+{%0D%0A++%3Fa+foaf%3Aimg+%3Fb%0D%0A}">all articles with an image</a>:
<code>SELECT ?a, ?b WHERE {
  ?a foaf:img ?b
}</code>

A list of <a href="http://dbpedia.org/snorql/?query=SELECT+%3Fa+WHERE+{%0D%0A++%3Fa+skos%3Asubject+%3Chttp%3A%2F%2Fdbpedia.org%2Fresource%2FCategory%3ADrugs%3E%0D%0A}">all drugs</a>:
<code>SELECT ?a WHERE {
  ?a skos:subject <http://dbpedia.org/resource/Category:Drugs>
}</code>

<a href="http://dbpedia.org/snorql/?query=SELECT+%3Fa+WHERE+{%0D%0A++%3Fa+dbpedia2%3Alicense+%22LGPL%22%40en%0D%0A}%0D%0A">Everything with an LGPL license</a>:
<code>SELECT ?a WHERE {
  ?a dbpedia2:license "LGPL"@en
}</code>

<a href="http://dbpedia.org/snorql/?query=SELECT+%3Fa+WHERE+{%0D%0A++%3AChair+rdfs%3Alabel+%3Fa%0D%0A}%0D%0A">All translations of 'Chair'</a>:
<code>SELECT ?a WHERE {
  :Chair rdfs:label ?a
}</code>

<a href="http://dbpedia.org/snorql/?query=SELECT+%3Fa%2C+%3Fb+WHERE+{%0D%0A++%3Fa+dbpedia2%3Adisambiguates+%3Fb+.%0D%0A++%3Fa+dbpedia2%3Aordo+%3ACarnivora%0D%0A}%0D%0A">All carnivors that had their name stolen</a>:
<code>SELECT ?a, ?b WHERE {
  ?a dbpedia2:disambiguates ?b .
  ?a dbpedia2:regnum :Animal
}</code>

<a href="http://dbpedia.org/snorql/?query=SELECT+%3Fa%2C+%3Fb+WHERE+{%0D%0A++%3Fa+skos%3Asubject+%3Chttp%3A%2F%2Fdbpedia.org%2Fresource%2FCategory%3ALinux_kernel_hackers%3E+.%0D%0A++%3Fa+dbpedia2%3Aspouse+%3Fb%0D%0A}%0D%0A">All married kernel hackers</a>:
<code>SELECT ?a, ?b WHERE {
  ?a skos:subject <http://dbpedia.org/resource/Category:Linux_kernel_hackers> .
  ?a dbpedia2:spouse ?b
}</code>

<a href="http://dbpedia.org/snorql/?query=SELECT+%3Fa%2C+%3Fb+WHERE+{%0D%0A++%3Fa+dbpedia2%3Aredirect+%3Fb+.%0D%0A++%3Fb+dbpedia2%3Aregnum+%3APlant%0D%0A}%0D%0A">All pages that redirect to a plant</a>:
<code>SELECT ?a, ?b WHERE {
  ?a dbpedia2:redirect ?b .
  ?b dbpedia2:regnum :Plant
}</code>

A query for <a href="http://dbpedia.org/snorql/?query=SELECT+%3Fa+WHERE+{%0D%0A++%3Fa+dbpedia2%3Aregnum+%3APlant+.%0D%0A++%3Fa+dbpedia2%3Acolor+%22green%22%40en%0D%0A}">all green plants</a>:
<code>SELECT ?a WHERE {
  ?a dbpedia2:regnum :Plant .
  ?a dbpedia2:color "green"@en
}</code>

<!--break-->