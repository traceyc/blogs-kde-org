---
title:   "Converting ODF documents to PDF with WebODF "
date:    2011-02-24
authors:
  - oever
slug:    converting-odf-documents-pdf-webodf
---
It is quite common that one wants to send <a href="http://en.wikipedia.org/wiki/OpenDocument">ODF</a> files to people that lack the software to display ODF. One workaround is to convert the ODF to PDF. Most office suites that support ODF can export to PDF. To compare how different office suites do this conversion one can use the website <a href="http://officeshots.org/">OfficeShots</a>. This website offers the ability to perform this conversion in many office suites at once and to compare the results.

<a href="http://webodf.org">WebODF</a> wants to play with the grown-ups. So I have extended WebODF with the ability to convert from ODF to PDF. Here is a small script that shows how to do this conversion for a file <tt>/home/user/file.odt</tt>:

<pre>
# compile WebODF
git clone http://git.gitorious.org/odfkit/webodf.git
mkdir build
cd build
cmake ../webodf
make
cd ../webodf/webodf
# perform a conversion
FILE=/home/user/file.odt
cp "$FILE" .
FILE=`basename "$FILE"`
../../build/programs/qtjsruntime/qtjsruntime --export-pdf render.pdf "odf.html#$FILE"
ls render.pdf
</pre>
<!--break-->
