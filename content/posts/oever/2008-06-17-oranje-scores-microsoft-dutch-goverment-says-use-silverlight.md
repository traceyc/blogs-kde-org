---
title:   "Oranje scores for Microsoft, Dutch goverment says: 'Use Silverlight'"
date:    2008-06-17
authors:
  - oever
slug:    oranje-scores-microsoft-dutch-goverment-says-use-silverlight
---
Today, the Dutch minister for Education and Culture, <a href="http://nl.wikipedia.org/wiki/Plasterk">Plasterk</a>, has defended the Dutch broadcaster NOS. NOS is broadcasting the games of the European Championship football using Microsoft malware <a href="http://www.daniweb.com/blogs/entry1418.html">Silverlight</a>. This means that in order to view this broadcast you need to install this software on your computer. You can only install the software on computers with Microsoft Windows or on Apple computers.

<img src="https://blogs.kde.org/files/images//slaveware.jpg" alt='Another goal for Bill.'/>

This practice is in disagreement with the plan <a href="http://www.ez.nl/content.jsp?objectid=153180">Nederland Open in Verbinding</a> for Free Software in the dutch government. 'Nederland Open in Verbinding' means 'The Netherlands Openly Connected'.This plan was accepted unanimously in parliament. It says that any new to be developed software in the Netherlands must be produced under an open source license and must use open standards. Now Plasterk says we should be connected via encrypted proprietary data streams.

Arda Gerkens <a href="http://www.geencommentaar.nl/parlando/index.php?action=doc&filename=V070822580.pdf">has asked</a> Plasterk why he is letting NOS disregard open standards.
He <a href="http://www.minocw.nl/documenten/25596.pdf">has answered</a> that NOS needs DRM. DRM stands for 'Digital Rights Managements' and is a way of controlling you. It is designed to make sure you do cannot do everything that you like with certain data. <a href="http://www.gnu.org/philosophy/can-you-trust.html">DRM</a> requires that there is a small policeman in every computer. If there is no policemen there, then you cannot access the data.

This is fine with Microsoft. They love to be the policeman (just like the government). They love to tell you what is allowed and what is not allowed on your computer (just like the government). For example, it is fine for you to use Microsoft software. It is not ok for you to use other software. That is why they make it difficult to use other software. The dutch government is helping them. Making Silverlight available via a website of the Dutch government is a big blow to any good intentions they have made.

Plasterk says that 99% of computers are already capable of using Silverlight. They should download it a fast as possible. Who cares about open standards and interoperability? Let's give Microsoft a large installed base for their Flash competitor so they can own the internet. Way to go, Plasterk!

<!--break-->



