---
title:   "Alpha version of Office Viewer for Nokia N900 available"
date:    2010-01-19
authors:
  - oever
slug:    alpha-version-office-viewer-nokia-n900-available
---
Today, Nokia released the first public version of the office document viewer for the Nokia N900 phone. It <a href="http://maemo.org/packages/view/freoffice/">was uploaded</a> to the Maemo repositories. This version supports text files, spreadsheets and presentations in OpenDocument format (ODF) and Microsoft Office formats. The viewer requires the latest update (PR1.1) to the N900 software. You can install 'Office Viewer' by adding the maemo-devel repository to your N900 catalogues:
<dl>
   <dt>Catalog name:</dt><dd>Maemo Extras-devel</dd>
   <dt>Web address:</dt><dd>http://repository.maemo.org/extras-devel</dd>
   <dt>Distribution:</dt><dd>fremantle</dd>
   <dt>Components:</dt><dd>free</dd>
</dl>
Then the application 'freoffice' will be available in the category 'Office'. The install is 9 megabytes.

With the viewer, you can open multiple files at once, open office documents from your e-mail, search in office files and copy and paste from your documents. A very nice feature is the ability to give presentations with the phone. Here are some screen shots of the viewer running on the N900.

<a href="https://blogs.kde.org/files/images/Screenshot-20100119-164321.png"><img src="https://blogs.kde.org/files/images/Screenshot-20100119-164321.thumbnail.png" alt="Presentation"/></a> <a href="https://blogs.kde.org/files/images/Screenshot-20100119-141352.png"><img src="https://blogs.kde.org/files/images/Screenshot-20100119-141352.thumbnail.png" alt="Spreadsheet"/></a> <a href="https://blogs.kde.org/files/images/Screenshot-20100119-164450.png"><img src="https://blogs.kde.org/files/images/Screenshot-20100119-164450.thumbnail.png" alt="Text Document"/></a> <a href="https://blogs.kde.org/files/images/Screenshot-20100119-141912.png"><img src="https://blogs.kde.org/files/images/Screenshot-20100119-141912.thumbnail.png" alt="Overview"/></a>

The <a href="http://websvn.kde.org/trunk/koffice/tools/f-office/">code for this viewer</a> is available in the KOffice repository. New releases of the viewer will be uploaded to the repository as KOffice progresses towards version 2.2.

The viewer has a simple user interface and responds quickly to user input such as page changing and scrolling.
<!--break-->
