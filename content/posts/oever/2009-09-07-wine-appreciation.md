---
title:   "Wine appreciation"
date:    2009-09-07
authors:
  - oever
slug:    wine-appreciation
---
While sipping from a Vignes de Nicole and nibbiling on some <a href="http://www.alp-spilau.ch/heukaese.php">Heukäse</a>, I am thinking about Wine. Not the liquid version, but the software project.

As a reader of Linux Weekly News, I noticed that the Wine project makes very frequent releases. I looked up its <a href="http://source.winehq.org/git/wine.git/?a=history;f=ANNOUNCE">release history</a> and saw that Wine has made a developer release every fortnight for the last four years. The 11 years before that the releases were approximately monthly. Each of these development releases comes with an announcement with a long list of the changes that happened in these two weeks. This dedication is due to Alexandre Julliard who has made all of those releases.

Releasing so regularly is a sign of health. Still, within the Wine team, people were dissatisfied with the occurance of regressions in their code. So <a href="http://wiki.winehq.org/PatchWatcher">patchwatcher</a> was written. Patchwatcher is a set of scripts that do pre-commit checking of patches. There is a <a href="http://winezeug.googlecode.com/svn/trunk/patchwatcher/readme.txt">good overview</a> that explains the design of the system.

The system picks patches from the patch mailing list and tests them. The results of the tests are available publicly and the maintainer will only commit a patch when no tests fail due to the patch. This conservative approach pays off. Not all developers are subject to this regime. If they introduce a regression, git bisect is used to find the problem.

The rigor of Wine development is inspiring. I will browse the <a href="http://wiki.winehq.org/Developers">Wine developer wiki</a> more often.

<!--break-->