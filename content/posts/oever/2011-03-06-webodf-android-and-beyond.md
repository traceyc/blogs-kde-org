---
title:   " WebODF on Android and beyond"
date:    2011-03-06
authors:
  - oever
slug:    webodf-android-and-beyond
---
ODF support on phones and tablets is not good right now. Work is being done to improve this by the Calligra project, but <a href="http://webodf.org/">WebODF</a> can provide a solution too. To prove this, I built a small wrapper application that gives Android the ability to read ODF files. This application is available in the <a href="http://gitorious.org/odfkit/webodf/trees/master/programs/android">WebODF repository</a> and I've also put the <a href="http://www.webodf.org/redmine/attachments/download/3/WebODF.apk">installable application</a> online.

To reach more phones and tablets, such as the iPhones, iPad, Blackberry and Symbian phones, we could use <a href="http://www.phonegap.com/about">PhoneGap</a>. Making a PhoneGap application from WebODF code is a nice way to get started on cross-device development and to help out with the adoption of ODF. If you want to give this a try, <a href="http://gitorious.org/odfkit/webodf">check out the WebODF code</a>, <a href="http://www.phonegap.com/">get PhoneGap</a>, read the code for the Android example to see how to adapt the WebODF runtime and get hacking. Good luck and have fun!
<!--break-->
