---
title:   "Pit stop"
date:    2007-03-14
authors:
  - oever
slug:    pit-stop
---
First off, I'm not a fan of formula 1 races. Nevertheless, some sort of analogy can be gotten from it to describe the adoption of Strigi in KDE. Ever since Strigi moved to the directory kdesupport in the source repository, many people have started contributing to it. It feels like I've driven into a pit stop and found that after emerging from it, my car has a lot of mechanics on top of it. And they are not siting idly, they are working on improving the car while it is speeding along while I'm trying to keep it on track. In addition to that, they are whispering in my ears and suggesting alternate routes for driving more elegant, taking shortcuts, being nice to other drivers and for <a href="http://nl.wikipedia.org/wiki/Ritsen"><i>ritsen</i></a> into the caravan that is KDE.

