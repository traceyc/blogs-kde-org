---
title:   "Spring cleaning: Strigi becomes a meta-project"
date:    2010-03-21
authors:
  - oever
slug:    spring-cleaning-strigi-becomes-meta-project
---
A couple of large commits changed the organization of the Strigi project. As you probably know, Strigi provides the code to extract data from files and also allows for fast searching for files. We have reorganized the project to be a meta project. It is now split into five projects that can be compiled independently: libstreams, libstreamanalyzer, strigidaemon, strigiclient and strigiutils. This move has been done to make it easier for other projects to use the library parts of Strigi. KDE, especially Nepomuk, depends on libstreamanalyzer, which in turn depends on libstreams.

This reorganization has brought along a big cleanup of build files in the project. The resulting libraries and executables are essentially the same as the are in last release: this reorganization just moves the files and changes the build system the libraries more pronounced. Especially the Tracker developers should benefit from this move. They have requested a way to use libstreams and libstreamanalyzer without needing to use the rest of Strigi.

The versioning and release schedule of the five Strigi components will stay the same. The next release will come as a big tarball and as five small tarballs. To get all five parts, run
<code>
  svn co svn://svn.kde.org/home/kde/trunk/kdesupport/strigi
</code>
To get just the libraries run
<code>
 svn co svn://svn.kde.org/home/kde/trunk/kdesupport/strigi/libstreams
 svn co svn://svn.kde.org/home/kde/trunk/kdesupport/strigi/libstreamanalyzer
</code>

We are now considering how to best move the project to git.

I leave you with a picture of one of my chickens and a link to a <a href="http://community.kde.org/GSoC/2010/Ideas#Stream_Analyzer_based_on_Data_Structure_Descriptions">nice Summer of Code idea</a>.
<img src="https://blogs.kde.org/files/images/Wyandotte.preview.jpg" alt="Wyandotte chicken"/>
<!--break-->
