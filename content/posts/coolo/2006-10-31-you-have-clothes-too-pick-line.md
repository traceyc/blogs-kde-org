---
title:   "The \"you have clothes too?\" pick up line"
date:    2006-10-31
authors:
  - coolo
slug:    you-have-clothes-too-pick-line
---
<p>I've seen it before, but yesterday the thought stroke me: What use does the condom box outside of the laundromat next to the Hauptmarkt have?</p>

<p>I have my theory (and the single student in the laundromat in my neighbourhood that looked as if she was bored and open for a chat kind of proved it), but I ask myself: is it really justifying such an open box? After all you might appear pretty needy if you go outside and buy a couple :)</p>

<p>And yes, I felt the urgent need to write about it even though I have more important things to do :)</p>