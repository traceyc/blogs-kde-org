---
title:   "Back to Future"
date:    2005-07-14
authors:
  - coolo
slug:    back-future
---
As I said <a href="http://blogs.kde.org/node/1211">before</a>, I'm ringed now. Our families and friends joined us celebrating our love. 

<img src="http://ktown.kde.org/~coolo/Before-Rom.jpg" width="400" class="showonplanet">&nbsp;&nbsp;<img src="http://ktown.kde.org/~coolo/Rom.jpg" width="400" class="showonplanet"> 

Two days later we made a trip to Rome and everyone claiming it is cool, was damn right. It wasn't even too hot as many claimed, at least I consider it more awful back in Nuremberg where it's the same temperature, but doesn't cool down towards the evening. Anyway, we visited all the places one has to visit and had a lot of fun making pictures, so we have plenty of material to bore grandchildren later.

What is pretty obvious at the above two pictures: Maren can change hair colour depending on the environment, which makes her my chameleon of choice ;)

<!--break-->
