---
title:   "What you miss if you don't watch F1"
date:    2006-08-28
authors:
  - coolo
slug:    what-you-miss-if-you-dont-watch-f1
---
The Formula One is more exciting than any since Hill retired. But to top this, RTL decided to field test new commercials during the "break" 10 gaps to go. In case you missed it, you should look at <a href="http://youtube.com/watch?v=u66ZFMWyfNs&mode=related&search="> this</a> - especially if you're male :)
<!--break-->