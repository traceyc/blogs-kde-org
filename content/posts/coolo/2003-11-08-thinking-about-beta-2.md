---
title:   "Thinking about beta 2"
date:    2003-11-08
authors:
  - coolo
slug:    thinking-about-beta-2
---
Now that the first dust about the first beta is settled, my thinking of how to continue gets more and more in foreground.<br>
<!--break-->
<br>
Bug fixing continues at a nice rate, but there are still some show stopper bugs that were fine for beta1, but are not for whatever comes after:<br>
<ul>
<li>flash content missing (bug:65868 )</li>
<li>quite some khtml regressions, that show up on quite some pages (e.g. bug:66490, there are others)</li>
<li>kwin hides cookie windows (and korganizer even windows) because of focus stealing prevention</li>
</ul>
<br>
These bugs _have_ to be fixed, there is no way around it. But in general
there are of course the two golden rules of releases:<br>
<ul>
<li>you'll never get it right</li>
<li>whatever you fix, it might break other things</li>
</ul>
(it might be they have downsides originally - I forgot then :)<br>
<br>
Anyway, I think if just the right ten bugs are fixed, we could release tomorrow. <b>If</b> - in reality we might end up with another beta before christmas and the final somewhere in june ;(
