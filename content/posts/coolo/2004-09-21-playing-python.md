---
title:   "playing with python"
date:    2004-09-21
authors:
  - coolo
slug:    playing-python
---
I'm not one of those guys that promises their girl friends to not touch the computer when on vacation, but
sure I promised to do only fun things :)

I had two projects I took with my laptop (that I take with me for photographic reasons - in case you wonder): clucene to write kugle and unsermake. As writing kugle without internet and chat is boring, I decided I go for unsermake. 

Unsermake is in some kind of splitted personality since quite some time. On one side it has to know about everything make knows to write good Makefiles. On the other hand, it has to write it out so make can do it's work on it's own. And after all, all we need from make is some variable expanding and calling shell scripts after checking file stamps. I thought, that can't be that hard - and it turned out to be both easier and harder than expected.

First let me explain the basics: unsermake [-c] still generates Makefile.in files, but these are way simpler than the ones you might know. They are just there so configure got something to replace. As soon as configure ran, unsermake will read both Makefile.am and the replaced configure values and be a make in itself.

This allows much easier and much safer code in the target generation - especially in the area of conditions, I know the values behind if and else and that's a big advantage.

On the other hand I'm still struggling with getting make -j working. That's really tough graph theory and that wasn't enough fun to do on my vacation. So if you got input on that, let me know.
 
<i>Thanks for those that send me their suggestions. make -j works now. Not perfectly, but it works.</i>
