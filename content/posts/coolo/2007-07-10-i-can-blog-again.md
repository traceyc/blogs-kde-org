---
title:   "I can blog again!"
date:    2007-07-10
authors:
  - coolo
slug:    i-can-blog-again
---
There is just so much I wanted to tell during the offline weeks :)

<ul>
<li>Didn't miss akademy, had bad weather myself
<li>Our future is still "in utero"
<li>My faster booting project is <a href="http://ktown.kde.org/~coolo/bootchart.png">finished</a> - I blame you can't boot faster into a konsole window without optimizing for a specific setup
<li>Won "Best Overall Project" in <a href="http://idea.opensuse.org/content/blog/hack-week-award-winners">the hack week event</a> together with <a href="http://www.tat.physik.uni-tuebingen.de/~rguenth/">Dr. Richi</a>
<li>Dirk is the admin hero of the week, the month and the year
</ul>
<!--break-->