---
title:   "10 inches in one year!"
date:    2008-08-15
authors:
  - coolo
slug:    10-inches-one-year
---
Tomorrow is the last power-of-two day in the year and this happens to be Felix's birthday (if you remember 2^4*2^3=2^007 - a year has passed). I just finished burning a DVD with the movies we recorded and a slideshow of the best pictures and it's a strange feeling seeing him grow up in 5 1/2 minutes.

But this date is more important than that: Maren returns into her job on monday and I'll stay home with him for two months. In this time I will still be part of the KDE and the openSUSE community, but won't have a lot of time for either.

So it will be an adventure for myself and I guess I'll return into official duties with a different view on life :)

<img src="http://ktown.kde.org/~coolo/Tignale_354.jpg"/>
