---
title:   "cvs2cl rocks"
date:    2004-09-05
authors:
  - coolo
slug:    cvs2cl-rocks
---
Time passes by too quickly for me to remember what I fixed the last week within icecream.
But for the RPM changelog I need to know, so I looked for some tool to create Changelog files
from cvs history and found the perfect one: cvs2cl.pl

It's really great and got several options - including one that allows mapping CVS accounts
to full address and even got an option to ignore commits with CVS_SILENT in them. *sweet*

So now I have an icecream Changelog of 700 atomic commits and wonder that I forgot 90% of it.
E.g. that we wrote the first monitor way before the scheduler even scheduled. Or that the daemon
used to call teambuilder, so I could test the client with make -j10 and have the scheduler always
return localhost.

