---
title:   "Accessibility at work in Trysil"
date:    2006-07-05
authors:
  - coolo
slug:    accessibility-work-trysil
---
I get so seldom to blog about such an important topic as Accessibility, but I think this is worth noting:

Gunnar's IRC client is reading incoming messages alound using kttsd and it's puzzling the first time you hear it, but after some time you notice how much fun is related to it. Just see here (slightly edited)

<pre>
[16:52] &lt;[ervin]&gt; hahahahahahahahahahah
[16:52] &lt;pmax&gt; muahahaha
[16:52] &lt;[ervin]&gt; OH OH OH OH OH OH OH
[16:52] &lt;boemann&gt; well i've been maintaining it
[16:52] &lt;[ervin]&gt; OH OH OH OH OH OH OH
[16:52] &lt;pmax&gt; OH OH OH YES YES MORE
[16:52] &lt;harryF&gt; YYYYYYYAAAAAAAAAAAIIIIIIIIIIIIII
[16:52] &lt;[ervin]&gt; OH YEAH OH YEAH OH YEAH AGAIN!
[16:53] &lt;[ervin]&gt; tak tak tak tak tak tak
[16:53] &lt;pmax&gt; SLAP IT!  HARDER!
[16:53] &lt;Bille&gt; shoo bee doo bee doo wop
[16:53] &lt;aseigo&gt; do my hard baby
[16:53] &lt;[ervin]&gt; OH OH OH OH OH OH OH
[... possibly offensive content removed :]
[16:54] &lt;icefox&gt; :(
[16:54] * [ervin] want's his toy back
[16:54] &lt;Schmi-Dt&gt; I have temporarily turned text-to-speech off now.
</pre>
<!--break-->