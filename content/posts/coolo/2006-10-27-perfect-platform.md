---
title:   "The Perfect Platform"
date:    2006-10-27
authors:
  - coolo
slug:    perfect-platform
---
<p>
I like how the dot article stresses Kubuntu as the perfect platform for KDE4 development. 
While I can't say much about it (only knowing that David spreads his "install that list of 
packages to make kubuntu useful" txt file to everyone complaining about broken compilations while 
SUSE had a "KDE Development" selection since about forever) I'm relieved that the dot article
did not touch openSUSE's position as the perfect platform to <b>use</b> KDE.
</p>
<p>
On related notes: I used my week off from work to do something fancy and made kpat kick ass again 
(I started in Dublin with it after being so heavily inspired by the work Mauricio Piacentini showed
me). So let me share some screenshots:
</p>
<p>
<a href="http://ktown.kde.org/~coolo/bla17.png"><img src="http://ktown.kde.org/~coolo/bla17.png" width="50%"></a>
</p>
<p>This is KDE (if you forgot how kpat looked like before, check <a href="http://ktown.kde.org/~coolo/bla16.png">here</a>. It might look on screenshots not like magic, but it is magic as it will use as much screen space as you give it and rescale and move the cards as necessary, which helps a lot in game play.</p>
<p>
And this is using the very nice card set "white" of <a href="http://planet.nicubunu.ro/">Nicu</a>, which I prefer for it's clean look. But as taste differs, I also imported quite some more SVG card sets I could find and even converted the KDE3 default card set to SVG:
</p>
<p>
<a href="http://ktown.kde.org/~coolo/classic.png"><img src="http://ktown.kde.org/~coolo/classic.png" width="50%"></a>
</p>
<p>
The rest of the week I was busy to fly a kite with my niece (and yes, that's the Jever lighthouse :)</p>
<p>
<a href="http://ktown.kde.org/~coolo/pilsum.jpg"><img src="http://ktown.kde.org/~coolo/pilsum.jpg" width="50%"></a>
</p>
<p>
Unfortunately I got a flu from her towards the end of the week, so I was down for another 3 days, so I almost missed the 10.2 feature deadline. But 10.2 beta2 will have the work I wanted to do as my flu was accepted as a valid reason by AJ.
</p>
<!--break-->