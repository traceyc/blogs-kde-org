---
title:   "Icecream died"
date:    2007-01-08
authors:
  - coolo
slug:    icecream-died
---
On December 25th the icecream scheduler died (as we found out today). This is the last log message:
19:43:07: put 281278480 in joblist of tangelo

So the scheduler worked fine for over 281 Million compile jobs (roughly 5400 KDE compilations from scratch) and then died - without any hint on what he died on. I guess Dirk is right, the winter in germany is just too hot for icecream.
<!--break-->