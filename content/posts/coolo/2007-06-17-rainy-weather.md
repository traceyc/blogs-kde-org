---
title:   "Rainy weather"
date:    2007-06-17
authors:
  - coolo
slug:    rainy-weather
---
After a week of about no sleep due to aweful heat, it was really rainy around here. So what are you doing with a rainy weekend? Right, improve openSUSE boot time :)

So with a little optimisation of things already in place (no magic, no new white paper material) I got the boot time for a default (KDE) installation of alpha5 (tried the one CD install, but had to install quite some things from factory to make it usable - e.g. Nimbus Sans looks horrible as default font) down from 32s to 26s. That's an improvement of roughly 20%!

These 26 seconds are basically split between these:
  7s : load the kernel and mount / ro
  6s : udev initialises all hardware elements and loads all modules
  3s : do basic setup (initial firewall blocking, saving kernel logs, ...)
  2s : start services necessary for X
  3s : preload some X files to improve visual startup
  5s : actual kdm startup including X server

I think the biggest potential is actually in not having to wait for _all_ hardware to initialize before remounting /. When I accidently disabled these boot steps I booted in 18s - of course incomplete in general, but I had a working kde startup. 

Now on to the magic.
<!--break-->