---
title:   "Just two calories?"
date:    2008-04-08
authors:
  - coolo
slug:    just-two-calories
---
<img src="http://ktown.kde.org/~coolo/img_0809.jpg"/>

At times when we have only 512MB Compact flash, we always had to download the pictures off the camera, but with these affordable 4GB cards, it can take a while before we see the need to download. So here is my favorite.