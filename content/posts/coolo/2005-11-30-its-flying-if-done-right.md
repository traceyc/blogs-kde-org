---
title:   "It's flying - if done right!"
date:    2005-11-30
authors:
  - coolo
slug:    its-flying-if-done-right
---
It's great to see the progress fontconfig does performancewise after such a long time without much progress. And it's great
that Dirk and me are referred to as "perf guys" in the <a href="http://cvs.freedesktop.org/fontconfig/fontconfig/ChangeLog?rev=1.109.2.66&only_with_tag=fc-2_4_branch&view=markup">fontconfig changelog</a> now :)

One thing we learned while profiling fontconfig: most algorithms in there are (expensively) linear to the number of fonts, but there are even some more costly than that. So if you've been proud so far on your font collection, rethink your position: Make yourself aware that every font you have on your system, a KDE application spends roughly 100.000 (pentium) instructions on trying to find out if that is the best font. So if you have 1000 fonts on your system that you don't use in any application, that are 100 million instructions wasted. 

Now the problem is that KDE doesn't need one font, but about five (monospace, sans serif fonts in different sizes). And as QFont is per process, every KDE/Qt application does this calculation from scratch. It very much depends on your setup of course, but let's assume we do this for 10 applications in KDE startup. So doing 50 font matches (way too few, but I'm just trying to make a point): this makes 5 Billion instructions <b>wasted</b> on KDE startup. Now divide this 5 by number of Ghz you have to see how many seconds you waste. So go - deinstall that fonts! 15 fonts ought to be enough for everyone :)

<img src="http://ktown.kde.org/~coolo/callgrind-horror.png">
<!--break-->
