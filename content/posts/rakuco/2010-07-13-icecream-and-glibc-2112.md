---
title:   "Icecream and glibc 2.11.2"
date:    2010-07-13
authors:
  - rakuco
slug:    icecream-and-glibc-2112
---
In case you use <a href="http://en.opensuse.org/Icecream">Icecream</a> (the distributed compile system which makes your coworkers' machines compile KDE for you :)) and happen to be using a glibc >= 2.11.2, you might notice that you are not able to send compilation jobs to other machines while still being visible to the scheduler and receiving jobs from other hosts.

It turns out that some commits to <a href="http://sourceware.org/git/?p=glibc.git;a=history;f=elf/ldconfig.c;h=b4af31e5b52285dd99a8a18c48d879ac21577a22;hb=HEAD">ldconfig</a> made the script which created the environment sent to all other clients not work as expected because of a missing directory.

So until a new stable Icecream release is made (the current one is 0.9.4), make sure to apply <a href="http://websvn.kde.org/?view=revision&revision=1131079">this commit</a> to your package to keep things running smoothly.
<!--break-->