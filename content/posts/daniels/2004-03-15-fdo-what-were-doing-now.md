---
title:   "fd.o: what we're doing now"
date:    2004-03-15
authors:
  - daniels
slug:    fdo-what-were-doing-now
---
In short: Xizzle. xserver gained another DDX (Driver-Dependent X: essentially anything for an X server that isn't the definition of an atom, or any other core, shared functions) - Xizzle. It's a fork of XFree86, autotooled, and with other goodness. The loader is going away. The cruft is being kicked out of the tree. This will form the crux of the first stable xserver release, which will be made before the end of the month (tick, tick, tick).

There are new projects being added every day, and new efforts, such as the groupware collaboration list danimo asked me to set up a couple of hours ago. There's a new common clipart project, led by very cool people; more input stuff, et al. X.Org is powering towards a monolithic release - I'd be very surprised if they didn't released their XFree86 4.4-based (but license-clean) monolithic tree by the end of the month.

The platform work has mainly stalled on X, and also because I'm being paid to work on xlibs/xserver/xapps, but not the platform. Same goes as to why the X stuff isn't packaged for Debian yet; uni and real life has also been very busy (see <a href="http://fooishbar.org/daniel/blog">my blog</a> for that).

In short, we're kicking arse. Much cool stuff is happening, and very quickly. Signal/noise is quite incredibly high, so jump on a few commit lists, or hang around IRC (freenode, #freedesktop), if you want to get a good idea of what's going on.