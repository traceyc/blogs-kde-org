---
title:   "Life, and other unfortunate incidents"
date:    2003-07-30
authors:
  - daniels
slug:    life-and-other-unfortunate-incidents
---
So, what have I been doing while MIA?

I've moved house on 15min notice, for a start. I'm incredibly busy with school (assessment tasks, exams, you name it), and trying to get some work done while I still can. I don't even have a real net connection - if I drag my computer downstairs, I can get dialup to a NATed IP. Wa-hey.

I still post occasionally to -c-d, but the moderation can take up to a week, so half of them lately have ended up rejected because the thread ended with consensus 3 days before it was moderated. :\

Listening to lots of Drum & Bass and Acid lately. Lots of Squarepusher, bit of Diesel Boy, Joshua Ryan, Shapeshifter, Dom & Optical, and more. DnB, when combined with lots of sugar, makes me push out lots of code. ;)

-d, out