---
title:   "Truth and Truthiness"
date:    2006-08-20
authors:
  - stefan teleman
slug:    truth-and-truthiness
---
<p>So, <a href="http://www.opensolaris.org/">OpenSolaris</a> got <a href="http://news.com.com/IBM+takes+potshots+at+OpenSolaris/2100-7344_3-6106360.html">zing'ed</a> at LinuxWorld by IBM. Of course, that sent Sun's
PR Department to <a href="http://www.theregister.co.uk/2006/08/16/ibm_sun_open_source/">DEFCON 5</a>.</p>
<!--break-->

<p>On first appearance, this could just be IBM's PR Department "engaging our
competitor's peers in a constructive yet competitive corporate dialogue"
[mouthwash, please]. Instant Blogger Blast. How Web 2.0. I found the whole
thing funny.  Mr. Frye is not even entirely off the mark in his remarks. I just
don't happen to think it takes one full year to "Evaluate Subversion".</p>

<p>Then there's <a href="http://digg.com/linux_unix/Dan_Frye_An_Open_Letter_to_IBM_s_Gutless_Coward">THIS</a>. Now, this is a problem. It is a problem in and of itself.
First, it is entirely inappropriate to use crass personal attacks and insults
in a public debate, regardless of how indignant, adamant or passionate
one might feel about their position. Second, the author of this post attempts
to assert legitimacy by availing himself of his OpenSolaris Community membership.
For the record: the author of this literary masterpiece does <b>not</b> speak for me.
I seriously doubt he would be speaking for the OpenSolaris Community at large.
The author speaks only for himself, and poorly, at that. Too many spelling mistakes.
I am apalled that noone from the OpenSolaris leadership (is there one ?) has
stepped forward trying to distance the Project from this disgrace.</p>

<p>It's been a pretty rough month for OpenSolaris. First, there was bad news from
<a href="http://groups.google.com.au/group/google-code-hosting/browse_frm/thread/d13e9f848b543309/3f376c34abc3ce20?tvc=1&hl=en#3f376c34abc3ce20">Google</a>. No CDDL project hosting, because of lack of license traction.</p>

<p>Then there's the <a href="http://groups.google.com/group/linux.debian.devel/browse_thread/thread/e0e83b6eb40517ef/d5373069df3ff6db?q=cddl+debian+cdrecord&lnk=nl&">DFSG/CDDL/GPL dysfunction</a>. Depending on your type of
sense of humor, you might find it funny, or it might make you furious, or nauseous.
Executive Summary of the Debian thread: CDDL licensed software goes in non-free,
because CDDL is incompatible with GPLv2.</p>

<p>Both these topics have been mirrored with corresponding flamewars on the
<a href="http://www.opensolaris.org/jive/forum.jspa?forumID=13&start=0">opensolaris-discuss</a> mailing list. Seek and ye shall find.</p>

<p>What's going on here ?</p>

<p>It turns out that a little tidbit of information unearthed from Debconf6 might shed
a little light on this mystery. Information publicly available in Ogg/Theora format, <a href="http://meetings-archive.debian.net/pub/debian-meetings/2006/debconf6/theora-small/2006-05-14/tower/OpenSolaris_Java_and_Debian-Simon_Phipps__Alvaro_Lopez_Ortega.ogg">here</a>.
<b>WARNING: this file is 112MB in size.</b></p>

<p>In this video, we can watch <a href="http://www.answers.com/topic/danese-cooper">Danese Cooper</a>, one of the authors of the CDDL, clearly
explain that the CDDL was purposely written to be incompatible with GPLv2, at the
insistence of Solaris Kernel engineers (according to Danese, the Sun engineers
threatened to quit if CDDL was made compatible with GPLv2). At least, that's how I
understand Danese's comments. Am I wrong ?</p>

<p>I am not a license nut. As far as I am concerned, as long as the license requires
the re-publishing of source code modifications, I am fine with it. If it happens to be
the GPL, then it is the GPL. However, I remember clearly, throughout last year, when
the Sun PR machine went into turbo mode explaining to the world that it is not the
CDDL which is incompatible with the GPL, but the opposite. For references in support
of this claim, please search in Google. You'll get many hits).</p>

<p>A little more than a year ago, I had expressed <a href="http://os.newsforge.com/os/05/06/07/1812203.shtml?tid=10&tid=132">my hopes and expectations</a> about
the OpenSolaris project.</p>

<p>Was I wrong then, too ?</p>
