---
title:   "Marble on Solaris"
date:    2007-08-28
authors:
  - stefan teleman
slug:    marble-solaris
---
So, i had a productive IRC chat this morning with Tackat, who introduced me to
<a href="http://edu.kde.org/marble/">Marble</a>.

Marble is part of <a href="http://www.kde-edu.org/">kde-edu</a>, and can be built either as a KDE4 application, or just
as a standalone QT application. It runs very nicely on Solaris with QT 4.3.0:

<a href="http://www.stefanteleman.org/marble/marble.sc0.jpg">marble-screenshot-0</a>
<a href="http://www.stefanteleman.org/marble/marble.sc1.jpg">marble-screenshot-1</a>
<a href="http://www.stefanteleman.org/marble/marble.sc2.jpg">marble-screenshot-2</a>
<a href="http://www.stefanteleman.org/marble/marble.sc3.jpg">marble-screenshot-3</a>

Marble isn't just a desktop app. It is also a QT designer plugin:

<a href="http://www.stefanteleman.org/marble/designer.marbleplugin.sc0.jpg">marble-plugin-screenshot-0</a>

:-)




