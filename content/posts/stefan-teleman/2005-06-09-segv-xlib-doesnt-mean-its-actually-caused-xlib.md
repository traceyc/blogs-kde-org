---
title:   "a SEGV in Xlib doesn't mean it's actually caused by Xlib ..."
date:    2005-06-09
authors:
  - stefan teleman
slug:    segv-xlib-doesnt-mean-its-actually-caused-xlib
---
the Subject: line is very Zen-like. :-)  i can hear the sound of my left hand clapping in the woods. 

Konsole for 3.4.1 was driving me nuts. it would crash every 3rd time i'd start it, but only on Solaris 10 x86, not on UltraSPARC. not the first time, and not the second time. only the third time, always in the same spot:

t@1 (l@1) signal SEGV (no mapping at the fault address) in XCheckTypedWindowEvent at 0xfcb960b8
0xfcb960b8: XCheckTypedWindowEvent+0x0064:      cmpl     %esi,0x00000014(%eax)
(dbx) where
current thread: t@1
=>[1] XCheckTypedWindowEvent(0x80e64e8, 0x360000a, 0x16, 0x8045390), at 0xfcb960b8
  [2] QApplication::x11ProcessEvent(0x8045860, 0x8045530), at 0xfd3c3b71
  [3] QEventLoop::processEvents(0x8124978, 0x4), at 0xfd3dbfd5
  [4] QEventLoop::enterLoop(0x8124978), at 0xfd453e29
  [5] QEventLoop::exec(0x8124978), at 0xfd453d99
  [6] QApplication::exec(0x8045860), at 0xfd43f533
  [7] kdemain(0x2, 0x8045ca0), at 0xfeb29e68
  [8] main(0x2, 0x8045ca0, 0x8045cac), at 0x8051322
(dbx)

well this required some head scratching.

so, after some digging around, i decided to take a very bold step, and actually implement the constructors in sessioniface.cpp and konsoleiface.cpp (which don't even exist). nothing fancy, just vanilla constructors/destructor/assignment operator. then i changed the constructors for Konsole and TESession to initialize their immediate superclass(es):

Konsole::Konsole(const char* name, int histon, bool menubaron, bool tabbaron, bool frameon, bool scrollbaron, QCString type, bool b_inRestore, const int wanted_tabbar, const QString &workdir )
  : KMainWindow(0L, name),
  KonsoleIface(QString("konsole")),
  action2session(),
  session2action(),
  // ...

TESession::TESession(TEWidget* _te, const QString &_pgm, const QStrList & _args, const QString &_term, ulong _winId, const QString &_sessionId, const QString &_initial_cwd)
   : QObject(0L, QString("TESession")),
     SessionIface(_sessionId.latin1()),
     sh(0L),
     te(0L),
     // ...

and now the SEGV disappeared without a trace. transparent Konsole, antialiased fonts, XRender support, Midnight Commander (i don't really use it, but i *could* if i wanted to) :-P

these little things in life which can make such a huge difference. like constructors, for instance. :-P




