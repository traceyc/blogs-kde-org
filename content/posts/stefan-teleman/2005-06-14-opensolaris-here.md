---
title:   "OpenSolaris Is Here"
date:    2005-06-14
authors:
  - stefan teleman
slug:    opensolaris-here
---
OpenSolaris is <a href="http://www.opensolaris.org/">here</a>. :-)
Check it out, it's really cool.

<a href="http://www.opensolaris.org"> <img border="0" style="margin: 8px;" alt="OpenSolaris: Love at First Boot" title="OpenSolaris: Love at First Boot" src="http://www.opensolaris.org/os/about/buttons/love1st_os_blu_180.gif"></a>



