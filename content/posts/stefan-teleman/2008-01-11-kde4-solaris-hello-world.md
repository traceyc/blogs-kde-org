---
title:   "KDE4 On Solaris : Hello World!"
date:    2008-01-11
authors:
  - stefan teleman
slug:    kde4-solaris-hello-world
---
KDE 4 on Solaris came to life today on my Opteron box.

Screenshots of running KDE4 [ kdelibs/kdepimlibs/kdebase-workspace/kdebase-runtime ]:

<a href="http://steleman.smugmug.com/gallery/4147491#242071870">KDE4Solaris</a>

[ my Smugmug gallery ].

Built with Sun Studio 12, QT 4.3.1 and the Apache Standard C++ Library.

OpenGL/Compositing effects are enabled. I Frankenstein'ed the S10U4 Xorg by
adding a bunch of X Extensions Libraries. I was very bold and enabled

Option "AddARGBGLXVisuals" "True"
Option "AllowGLXWithComposite" "True"

And now the real work begins. :-)








