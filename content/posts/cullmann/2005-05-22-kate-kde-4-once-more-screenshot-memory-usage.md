---
title:   "Kate & KDE 4, once more, screenshot + memory usage"
date:    2005-05-22
authors:
  - cullmann
slug:    kate-kde-4-once-more-screenshot-memory-usage
---
Kate now kind of works, I can actually use it again for hacking on itself, even if ioslave are here still a problem, but at least it works with files given per command line. More amazing is that painting now is again back to usable, beside some artefacts while scrolling, but hey, not hacked that much on such stuff since long :)

Here is the current state of art: http://www.babylon2k.de/pub/kde/kate-kde4-20050522.png

An other interesting thing is the memory usage, therefor, here are the kernel stats of two kate processes,
one running kate from trunk, with debug compile of kate & kdelibs + release build of qt, one running
kate of kde4 branch, with kdelibs & qt build with debug on:

Name:	kate for kde HEAD
FDSize:	256
VmSize:    48804 kB
VmLck:         0 kB
VmRSS:     31456 kB
VmData:     6092 kB
VmStk:        84 kB
VmExe:         4 kB
VmLib:     36576 kB
VmPTE:        68 kB

Name:	kate for kde KDE4 branch
FDSize:	256
VmSize:	   37848 kB
VmLck:	       0 kB
VmRSS:	   24600 kB
VmData:	    3428 kB
VmStk:	      84 kB
VmExe:	       4 kB
VmLib:	   29600 kB
VmPTE:	      56 kB

Seems like not that bad, given that doublebuffering is on for everything in the kde4 thingy and I still have done no further optimizing ;) btw., this might be no complete 1:1 comparison, I don't use same style and perhaps my trunk kate loads some more settings and stuff in the background, this might make the memory size bigger, too, but some megs seems to be saved, cool :)