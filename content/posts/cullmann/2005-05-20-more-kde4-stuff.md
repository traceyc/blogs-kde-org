---
title:   "More KDE4 stuff"
date:    2005-05-20
authors:
  - cullmann
slug:    more-kde4-stuff
---
hi,
kde 4 quick porting went on, people are working to get kdelibs in better shape, kdebase follows. Still lots of problems are around, as
many layouting stuff is broken, ioslaves still have problems and so on, but at least you can see the progress ;)

more screenshots around at http://www.babylon2k.de/pub/kde, like more up to date kate shots and a kcontrol screenie, only few modules there 
already compile.