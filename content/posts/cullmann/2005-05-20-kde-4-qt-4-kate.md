---
title:   "KDE 4 & QT 4 & Kate"
date:    2005-05-20
authors:
  - cullmann
slug:    kde-4-qt-4-kate
---
Hi,
guess this is good news for people liking Kate and co, it now works with the kdelibs 4 and qt 4 port we started
in the kde4 branch. It is still far from usable, as I guess some stuff in kdelibs still needs finetuning, as toolbars,
menus and co are horrible broken and my layout managment is not that well working atm, too, but it is still a good start.
Porting the part or the Kate app wasn not too hard, and big parts of kdelibs seems to went easy over to qt4, too, beside some
hairy stuff like xmlgui which will need more work from people knowing what to do ;).
Given the fact that Kate app porting using the QT 4 compat stuff did not take much longer than 1-2 hours last evening and 2 commits
and even does do something, the compat features of qt4 are not that bad. But me still thinks it is very inconsistent to have QStackedWidget in QT4 but not Q(H|V)BoxWidget, but only the Q3(H|V)Box stuff, as I think automagic boxing is a cool thing to have instead to use the layout stuff per hand for such easy tasks ;)

here some screenshots, btw.
http://www.babylon2k.de/pub/kde/