---
title:   "Xorg, keyboard and mice"
date:    2009-05-27
authors:
  - antonio larrosa
slug:    xorg-keyboard-and-mice
---
Yesterday I learnt about two new options for my xorg.conf :

<code>
        Option          "AllowEmptyInput" "off"
        Option          "AutoAddDevices" "off"
</code>

I must thank the X Strike Force guys from Debian for that knowledge :). Yesterday I tried the latest Xorg packages (7.4) that are available for sid, and my keyboard and mouse stopped working. It seems that Xorg now relies more on what HAL tells it about the available input devices than on its own configuration (as explained <a href="http://wiki.debian.org/XStrikeForce/InputHotplugGuide">here</a>, thanks <a href="http://ekaia.org/blog">Ana</a> for the link), and those two options together force X to continue using the input devices configuration from xorg.conf instead of using HAL.

I know the solution would be to configure HAL correctly, but that would take longer and yesterday I didn't have the time for that (I have a very custom mouse configuration for my trackball marble fx, that I guess it won't be easy to write in HAL).

Hope it helps everyone who find that same problem when upgrading their X servers.

PS: Yes, I know it's been a long time since I don't write anything here, but I'll pretend I do, and write as if nothing happened instead of garnish the post with the typical "I'm going to try to write more often" that I will probably not fulfill anyway (and not because I wouldn't want to)
<!--break-->
