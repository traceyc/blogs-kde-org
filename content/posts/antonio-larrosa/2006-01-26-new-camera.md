---
title:   "New Camera"
date:    2006-01-26
authors:
  - antonio larrosa
slug:    new-camera
---
The 3 wise men (thanks Seele for the translation :) ) were wise and kind enough this year as to bring me a new camera that will replace my old (and broken since a friend of mine sliped it to the floor) Canon Powershot A70 that so many good moments has shared with me. After looking at _many_ other cameras, I found that there were not many that matched the features I wanted and still were in the price range I could afford, and finally I chosed the <a href="http://www.dpreview.com/reviews/canona620/">Canon A620</a>.

I have to say I'm _very_ happy with that decision, it's a great camera, has tons of manual settings to use when the lighting is bad or simply I want to take pictures the way I want, the pictures are clear (I finally bought a tripod, which is something I should have done much earlier), and mainly, it has a 1 cm. macro that is <A HREF="http://www.imaging-resource.com/PRODS/A620/FULLRES/A620MAC.HTM">impressive</a>, also what I read about the battery usage was real, I've been using the camera for 2 and a half weeks and I haven't changed the batteries a single time yet (and that includes the testing days when I was just making pictures of everything and trying all the parameter combinations).

I still have to do a trip in order to make some good pictures that I can show, but in the meantime, I'll explain what happened to the broken camera, since I find it quite strange.

As I said, it fell to the ground . The external metal was a bit brushed and even a bit bended below the flash lamp, but it continued working. A few days before the akademy in August, it started doing strange things, like keeping the display blank or showig garbage, but the problem always dissappeared in a while. The bad thing was that that problem was happening more and more often each time. When the screen was black, all photos taken were black, but I could see photos in the memory card without problem. Other times, the CCD only captured pink lines. Those were nice the first times since they were a kind of psychodelic lines:

[image:1767 size=original]

But when you want to take a real picture quickly and those lines appear or even worse, similar one-pixel height pink lines that cannot be seen in the LCD viewfinder appear into a real picture... I can assure you it's not nice. Fortunately I found a way to fix the camera (temporarly) each time those pink lines appeared: the solution was to press with the thumb and index finger in the bottom and top of the camera at the same time. In theory, nothing was moving in there, but probably some loose contact was fixed with that.


