---
title:   "Compactons, rain and coding style guides"
date:    2005-10-11
authors:
  - antonio larrosa
slug:    compactons-rain-and-coding-style-guides
---
Finally, I've chosen the subject to research for my DEA (the investigation that precedes the thesis of a PhD). The title is "Computational analysis of the propagation of <a href="http://scienceworld.wolfram.com/physics/Compacton.html">compactons</a> in non-lineal transmission lines". I don't know much about compactons yet, but I'm looking forward to start working on that.

Compactons have the nice effect that when two of them interfere in each other's path, they are not modified by the other one, basically because of its compact support. Another curious effect of nature is that effect where water falls from the sky... I think it's called <a href="http://en.wikipedia.org/wiki/Rain">rain</a>.

Yes, it's raining in Málaga today. It's an important event since it hasn't rained in many months, and politicians were even talking a few weeks ago about having water restrictions if it didn't rain a lot in the following months.

So, finally, Autumn arrived to Malaga, and fall is usually a dark season, some would say that it's even sad, but it's really not as sad as some decisions that got into the coding style guide of the company where I work.

Basically they wanted to add a rule to the section that explains how to name the member functions of a class. The idea was to have an abbreviation of class names added as a prefix to every member function.

Fortunately, I managed to convince them to apply that rule only to GUI related .NET classes. Since I don't do .NET at work, then I'm safe and I can continue developing code that at least can be read without unneeded noise.

Ah, and yes, that conflicts with the whole concept of virtual functions, but they don't seem to care about that as much as about the autocompletion mechanism of the coding editor showing all the functions of a class grouped together. 

Otoh, it's true that they added an exception that allows to use the abbreviation of a base class as a prefix of an overloaded method so that you can use virtual methods. Of course, that means that then you can no longer assume that you know where a method is implemented by looking at the prefix.

Yes, you can laugh now, but it's the real life.
<!--break-->