---
title:   "The week after THE week"
date:    2005-09-10
authors:
  - antonio larrosa
slug:    week-after-week
---
This week has been quite tiring. First, my boss didn't allow me to take Monday as a personal free day in order to clear up the things we left from the akademy at the university. The Monday started with a couple of "pressure" meetings with by boss (just him and me) explaining me why I should work harder than ever and blah blah. So I've only been able to clear up things from the akademy after work, on afternoon/evenings.

On Thursday there was a local festivity in Malaga (the saint of the city), so everyone on the city was on holiday... except me, of course.  We have to offer maintenance of the systems at work, so I had to go to work. If that's not enough, On Friday, I had to send to my teachers at the doctorate two papers that I was supposed to write during the summer. Of course, during summer I had <a href="http://conference2005.kde.org">other things to do</a>, and the previous days I was overloaded with work, which means that basically I started writing one paper on wednesday evening and then continued on thursday evening.

I planned to finish both during the night. And maybe I could have done it if it wasn't for OpenOffice, which doesn't autosave the edited documents, my machine crashed at 3:30am, I lost everything I had edited until that moment, and I had to start again. At 7:00am I finished the first paper (fortunately I had (virtual) company, because in other case, I would have given up much earlier), sent it and went to sleep for an hour and a half, when I went to work.

Also, on Friday there was what we call in spain a "bridge holiday", which means that when there's a day with holiday (the local festivity), a day of work (friday) and a day of holiday (saturday-> weekend), then the day in the middle is also converted into holidays, so many people didn't go to work on Friday neither. But since I had to work on Thursday, I also had to work on Friday. Fortunately, that means I can get two free days next Monday and Tuesday, so I'll go back to work on Wednesday.

In the meantime, I'll have time to send the HP servers, send the merchandise back, return the coffee, etc. (no, it's not over yet!)

Taking in mind that I'm very tired lately (today, a friend even asked me if I was on drugs or something :) ), and that I didn't have any real holidays, I've been thinking that I'd like to travel far, maybe even to find another job abroad, but at least I'd like to take some days out of here. Let's see when I'll have time... I think it won't be before December :(, but I'll make a trip far from here in "not too many" months, that's for sure.

Btw, I've already uploaded <a HREF="http://developer.kde.org/~larrosa/photos/aKademy-2005/images.html">my pictures from the aKademy </a>.

Also, I've been invited by the organizers of the iParty (in Castelló, in the east coast of Spain) to give a KDE talk there on the 1st of October. There was a friendly invitation so I accepted even if I don't think I'll have recovered from the aKademy by that time yet. Anyway, I think it's important to show everyone that KDE is more than what it seems to be, and talks are good for that.

And now it's time to sleep, finally, one day without having to wake up at any specific hour. I'll turn off the phones and sleep for at least 18 hours... Good night!