---
title:   "Books"
date:    2006-01-26
authors:
  - antonio larrosa
slug:    books
---
Some days ago I was talking with a friend and he told me I should read a book that talked about the subject we were talking about. I had to answer I have too many books in my queue waiting at home for me to find some time to read them. Maybe if I put them together in a list I'll realize that the list is growing too long and I start finding (or making) time to read them...

<ul>
 <li><a href="http://en.wikipedia.org/wiki/Cryptonomicon">Criptonomicon</a> - I have the three books and I stopped reading it around the third quarter of the second book.
 <li><a href="http://en.wikipedia.org/wiki/Gödel,_Escher,_Bach">Gödel, Escher, Bach:An Eternal Golden Braid</a> - I've read part of it, but I'd love to read it completely (and find some time to think about it). It's really interesting.
 <li><a href="http://en.wikipedia.org/wiki/Mort">Mort</a> and <a href="http://en.wikipedia.org/wiki/Moving_Pictures_(novel)">Moving Pictures</a> - Terry Pratchet books from the Discworld series. I bought these books after I read <a href="http://en.wikipedia.org/wiki/Small_Gods">Small Gods</a> which Simon Hausmann recommended me  and I enjoyed a lot.
 <li>Key Philosophical Writings of <a href="http://en.wikipedia.org/wiki/Descartes">Descartes</a> - I've always been an admirer of the <a href="http://en.wikipedia.org/wiki/Discourse_on_method">Discourse on Method</a> since I learnt about it and its usage of doubt, so I'd like to read it some day.
 <li>El Genio Incomprendido - By Federico di Trocchio, it's a popular science book that is quite interesting, and talks about geniuses that history usually treated bad.
 <li>The complete original unedited works of <a href="http://en.wikipedia.org/wiki/Lewis_Carroll">Lewis Carrol</a> and the complete original unedited works of <a hef="http://en.wikipedia.org/wiki/Edgar_allan_poe">Edgar Allan Poe</a> - I have to confess it, I have a weakness for original unedited works, and I have a weakness for everything related to Lewis Carroll. The great thing about these books is that even if the books are big (>1000 pages each with small print), the stories, tales, poems, etc are usually quite short, so they can be read little by little (in fact, I already read the Lewis Carrol one, but I just added it here to give strength to the argument about my weakness :) ).
 <li>Darwin's Children - by <a href="http://en.wikipedia.org/wiki/Greg_Bear">Greg Bear</a>. It's not really a book I have, but I bought it for my brother's birthday, and it looked interesting so I'll have to ask him about it :).
 <li>Debunked! - by Henri Broch and <a href="http://en.wikipedia.org/wiki/Georges_Charpak">Georges Charpak</a> (Nobel Prize in Physics), which was translated to spanish as "Become a warlock, become a wise man" and debunks myths and pseudoscience.
 <li>And some other books I'm sure I forgot right now, but I was anxious to read.
</ul>

:( I'm afraid that list is too long and I don't have much time to read lately, in fact, I should be finishing the two presentations for the OSWC instead of writing this, but what the hell...