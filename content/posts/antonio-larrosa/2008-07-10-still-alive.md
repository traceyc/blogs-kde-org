---
title:   "Still alive!"
date:    2008-07-10
authors:
  - antonio larrosa
slug:    still-alive
---
It's been a long time since my last entry here, so I think I'll start by giving an update of my life lately. I've been a bit away from KDE development for too long (but not from the KDE community fortunately). Some of you probably know what it's like: long work weeks that makes the recent EU proposal of <a href="http://english.capital.gr/news.asp?id=520294">a 65 hours work week</a> look as a game (my personal record is around 96 work hours in a week and never below 50 hours), deadlines, more deadlines ... you know how it goes. 

Anyway, on the bright side of life, I started to do more exercise from some months ago (biking mostly) which is quite convenient at relieving stress even if in Spain riding a bike is a high risk sport. Also, I'll start teaching at the University of Málaga in the next academic year (which starts in September here), which is something I've been longing to do for years. I'll teach "Real Time Systems" to industrial engineers and "Operating System Design" to computer engineers. I've given lots of talks and workshops about KDE and KDE development (more than 70 the last time I counted them), but of course this is totally different and I'm sure it will be a nice experience (for me, and I hope, for the students too). Any tip is appreciated :).

Well, it's been so long since my last entry that I could keep writing for ages, but I better leave something for other posts (I really hope I can improve the frequency of my updates :) )

Ah, and last year I missed it, but this time I've managed to arrange my holidays in such a way that I can I say ... <a href="http://akademy2008.kde.org/">[image:3551 nolink=1 size=original style="vertical-align: top"]</a> . Hope to see you there!
<!--break-->


