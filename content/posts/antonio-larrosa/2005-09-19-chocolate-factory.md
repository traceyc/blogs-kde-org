---
title:   "The chocolate factory"
date:    2005-09-19
authors:
  - antonio larrosa
slug:    chocolate-factory
---
This weekend I've been finally able to rest for a while. On saturday morning I went to get my car to be repaired but the person that I needed doesn't work on saturdays. On the afternoon I planned to send the aKademy awards to the winners, but it seems the post offices are closed on saturday afternoons... great. I'll send them on Monday afternoon after getting out of work.

That left me some time on the evening to get out with some friends to see Charlie and the Chocolate Factory, which made me think, chocolate factories are cool... in the abstract sense of it.

Chocolate factories are what everybody wants (at least most probably if you're a kid of up to a certain age), but as we can see in the movie, it doesn't matter how hard you may want something, maybe what you really need is something else that you didn't even think about. 

That makes me think of software (ok, or maybe it's the other way around, I don't think it matters much). As a developer, I'm most of the times very sure of what I want my application to do, or what ui it should have. But it's only as a user that I can be sure of what my application needs to do or what ui it needs to have.

That's why I think it's so important that we use our own applications and are critical with them, specially when the development starts. Sometimes we've been using them for so long that we get used to some things that are not easy to understand for users.

Changing the subject, I've started using kimdaba around a week ago to organize my pictures. Well, I've been wanting to use it since meeting Jesper at Nove-Hrady (two years ago), but since I saw Jesper using kimdaba again at this year's akademy, I gave it a real try now . It's quite useful and works really well. I'm still adding metadata to all my old photos (6466 images), but it has a very useful user interface, so that's going quite fast, and I'm enjoying seeing old photos.

The only problem I see with it is: is the interface as "intuitive" for everyone as it's for me? (I like it, but I'm not sure everyone will think the same, and even I needed to use some minutes to get used to it). And also, when a new user sees that interface, we know he wants to use it but... does he think "I want to use that" ? That's probably something kimdaba needs some help with, but for me, it's already a very good application. Highly recommended for everyone who has more than 5 pictures.