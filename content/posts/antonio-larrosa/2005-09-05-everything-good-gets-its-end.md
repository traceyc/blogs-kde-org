---
title:   "Everything good gets to its end"
date:    2005-09-05
authors:
  - antonio larrosa
slug:    everything-good-gets-its-end
---
It's said that everything good gets to an end, and that's what happened to the akademy: Finally, it's over.

Celeste asked me during the party at the beach if I was glad the akademy was over, after thinking about it for a day, I should say I'm not. I'm glad the work is over, but I'd have liked it to continue for at least some couple of weeks with all of you here :).

To everyone who has come, I'd like to say thank you, specially to Rainer and Mirko, who have helped a lot to organize this akademy and have integrated extremely well into the Linux-malaga group. And also I'd like to thank everyone who has helped this Sunday repackaging everything and leaving everything as it was in the labs and classrooms we used.

The week as been quite tiring, but at the same time, I've been able to meet many good friends from many years ago and I've been able to know (better in some cases, or for the first time in others) other very nice persons that have joined recently the KDE community, haven't been able to come to previous akademies, or just I didn't talked with them much in previous editions.

Personally (and I know this only applies to me), what I disliked about the akademy is that I'm not omnipresent, so I couldn't go to a single complete talk (except for the opening session and the Trolltech keynote, but I would have liked to go to many others too) and I couldn't go to a single BoF session, even if I would have liked to go to the Agile methods session, some usability sessions and some others.

Finally, I'd also like to thank some persons that are not members of Linux-málaga, but who have helped a lot to make this akademy a reality. I'm talking about Alex Exojo, Victor Barba (from Barcelona) and Luiso, Pau and Victor Fernandez (from Valencia), all of them came to the aKademy as participants and we recruited them to work on the streaming and video recording of the conferences, in which they worked really hard, showing that they're all very professionals and what is as important (at least for me), very nice persons to work with. Of course, the same can be said from the Linux-malaga persons, who have worked for many months and specially in the last week to organize the event. It's been a pleasure to work with Antonio Rueda (montuno), Cayetano (HnZeKtO), David (sefokuma), Eduardo (kynes), Javier (shanky), Jesús (ed-obelix), Juanmi, Luis (failure), Pilar (azulema), Victor (Livingstone), (in alphabetical order) and some others that I'm sure I forget. It's been also nice to count with the presence of Vanessa as our german translator (I wonder why Martijn and Helio talked so much with her then... :) )

Well, I've said it's over, but in fact, there are some things left:
Returning the HP servers, the compiler cluster, the rented PCs, the coffee machine, sending the awards, pay some invoices... But with a little luck, we can care about that during this week more calmly.

Now, I should go to sleep, since I have to get up in 4 hours to go to work, routine, schedules, meetings, ... and I need to write two articles about the state of the art in embeeded Linux and about an automated SQL tutor for my PhD classes before next friday (any help on that is appreciated :) ).

Did I say I already miss you all? Feel free to come back to Malaga when you want to visit!

Hope the everyone has (had) a good travel home and that the sick ones get better soon :)

PS: I'll try to post tomorrow the pictures I made and the embarrasing videos :)