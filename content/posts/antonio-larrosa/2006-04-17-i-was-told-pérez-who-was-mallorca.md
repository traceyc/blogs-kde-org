---
title:   "I was told by Pérez, who was in Mallorca"
date:    2006-04-17
authors:
  - antonio larrosa
slug:    i-was-told-pérez-who-was-mallorca
---
"I was told by Perez, who was in Mallorca, and there the hapiness never ends during night and day", that's what an old spanish song says :) , and I had the opportunity to confirm it during last weekend (7th, 8th and 9th) when I went to Mallorca invited to give a talk at <a href="http://bulma.net/">Bulma</a>'s <a href="http://jornades.bulma.net/">3rd free software conference</a>. Bulma is one of the most important LUGs in Spain (if not the most)

The conferences were very interesting and I had a great time there. I saw there many old (and new) friends, among them, <a href="http://www.amayita.com">Amaya Rodrigo</a>, who I haven't seen for a long time. Amaya was the only person I knew in Madrid when I gave my first talk many years ago. Since then, she's been very active in the Debian and GNOME projects which makes our talk even more interesting. We talked about Mao, a really interesting card game that everybody loves (at least, everybody who has played it :) ), and we made a deal: If I write a Mao client application following her specifications (it must support network play and have a chat window), she'll switch to KDE. She even signed the deal with her gpg key at <a href="http://amayita.livejournal.com/70934.html">her blog</a>. Now I only have to find time to develop it...

On Sunday there was no direct plane to Málaga, so I took one to Madrid, stayed there for some hours meeting some very good friends who I always enjoy to see and having lunch with them, and then I went back to Málaga in the afternoon. On Sunday evening I was completely worn out, and the tiredness lasted for a couple of days, since work consumed more than 30 hours on the 3 first days of the week.

Anyway, it was worth it (I mean... getting tired during the weekend, not getting tired at work :) )
<!--break--> 