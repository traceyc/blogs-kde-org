---
title:   "Travel problems"
date:    2006-09-22
authors:
  - antonio larrosa
slug:    travel-problems
---
Finally I arrived to my home. I've been for the last three days having a wonderful time at the Canary Islands with many great people (both, the organizators and the other speakers) at the 1st Free Software event of the University of La Laguna. The problems started when the plane from Tenerife to Madrid was delayed, so I arrived too late to Madrid and I couldn't get on the plane to Málaga. After being put in another plane (that also was delayed), I've arrived to my home something like half an hour ago, and I have to be at the airport again in less than 2 hours and a half to get a plane back to Madrid, and then another one from there to Dublin. This will be a loong night...

Ok, now if you really read the previous paragraph, you might be thinking "why didn't you just stay in Madrid for the night and took directly the plane from Madrid to Dublin instead of going to Málaga and then back to Madrid?" Easy, because Iberia didn't allow me to do that. They told me I MUST take the plane from Malaga to Madrid or the whole ticket would be cancelled. So they prefer their passengers to do stupid things like travelling to Malaga just to take a plane back to Madrid rather than being reasonable and allow them to take the plane in Madrid, and get a seat in the Malaga-Madrid plane paid twice (by me, and by whoever uses it).

But well, we're talking about airline companies, they do stupid things by definition...
Anyway, see you in Dublin!

PS: Now you know why I'll look more like a zombie than like a living person.
PS2: As a curious thing, I met my sister-in-law at the airport of Madrid, who was arriving from Malaga, and a friend that I hadn't seen for a long time and who also lost the same plane than me.