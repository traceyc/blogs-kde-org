---
title:   "two months, and a day"
date:    2005-04-01
authors:
  - njaard
slug:    two-months-and-day
---
My flight back to California is on the second of June, from Heathrow. I leave San Francisco  back to England on the 14th (arriving on the 15th) of September.

That gives me three and a half months of attempting to avoid saying things like "That's five dollars and 50 <i>pence</i>."

<!--break-->