---
title:   "on DRM in KPDF"
date:    2005-02-20
authors:
  - njaard
slug:    drm-kpdf
---
By now there's quite the debate (<a href="http://aseigo.blogspot.com/2005/02/drm-and-open-source-software.html">aseigo</a>, <a href="http://tsdgeos.blogspot.com/2005/02/mantainership-is-heavy-rock.html">tsdgeos</a>) regarding the implementation of DRM in KPDF.

Rather then getting a Blogger account (ahem), I'll post here, where I already have an account.

I feel we have a <i>moral obligation</i> to not implement copy-protection in our software. It is not our job to police our users. Sparing the technical debate ("it won't work anyway because it's open source"), the fact of the matter is that just because the proprietary software does it doesn't mean we should.

Those who design or implement DRM are wrong.

It's that simple.

The wishes of the producers of content are inferior to the moral right to fair use. Not necessarily legal right, as that has been more or less destroyed in the US and EU.
<!--break-->