---
title:   "The Fall Within"
date:    2005-02-04
authors:
  - njaard
slug:    fall-within
---
My notebook was returned from the repair-shop on thursday.

On Friday it locked up hard again.  Exact same symptoms as before.

I know I shouldn't let a mere computer get me down, but I depend on this thing for finishing university.  I haven't the money with which to buy a new one.  I don't know how long I can use borrowed hardware.

This is when I wish I didn't depend on one at all.  At least I wish I were settled down somewhere so I didn't have the need for a portable computer.

At least khtml can <a href="http://bugs.kde.org/show_bug.cgi?id=97806">render</a> right-to-left justified text now.
<!--break-->