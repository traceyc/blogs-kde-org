---
title:   "I should have known better"
date:    2004-12-14
authors:
  - njaard
slug:    i-should-have-known-better
---
A few weeks ago, my computer locked up. I'm a linux user, so this is unusual.  I wasn't even doing something very intensive.

I should have taken it as a signal of impending doom.  A foreshadowing of the nightmare to come.  It began to rain -- at least clouds in my mind started to.  On this island, actual tangible rain is to be expected, but brainrain is still quite unheard of. My hopes, that little bit of optimism in me, left to a mere memory.

In the aftermath, a new motherboard is in order, and some data will be sorely missed.  Lots of data.  The emptiness I feel is much the same as the one felt by the disk drive.  It's not the disk drive to blame, either.

If you're expecting a response to your email sent in the last week, you might want to consider sending it again.
