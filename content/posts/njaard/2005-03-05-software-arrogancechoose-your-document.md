---
title:   "Software Arrogance/Choose Your Document"
date:    2005-03-05
authors:
  - njaard
slug:    software-arrogancechoose-your-document
---
Lots of programs in and out of KDE suffer from a condition that I call software arrogance.  Consistently, I find such applications completely unusable and thus refuse to use them. Software arrogance is when a program feels it is more important than its user's document.
<br /><br />
How is it that a program is written such that the user may feel that the importance of the application is greater than of eir document?  First of all, the document has what is seemingly a minor role in comparison to its container.  This is obviously apparent in MDI (window-in-window) applications like KDevelop in which you can minimise source windows.
<br /><br />
What? Why would you want to minimize the source code window but still have visible all the tools for editing the source code?
<br /><br />
It is reasonable to want to show two at a time, and to hide one while looking at another.  Then lots of horrible user interface restrictions become apparent as a result of the two documents becoming inseparable from the other.  You can't move one of the documents to another virtual desktop, the taskbar becomes meaningless, and, like KDevelop and Microsoft's Visual Studio, your application uses valuable real estate like Starbucks.
<br /><br />
Now, some of you may say that you like this window-in-a-window thing because logically, when you use KDevelop, you aren't editing just a source file, but you're editing your entire project, and each window of source code you're editing is fundamentally linked in that regards.
<br /><br />
So, I ask, what really is the document? Is the document the project, or is it the source window? Hopefully, at this point in my rant, you'll agree that both are the document. However, KDevelop (and for that matter, Kate) don't treat the project as the document, they just treat the source code as the document, and then add a few tools to manage the project (almost like an afterthought).
<br /><br />
Take a look at this <a href="http://ktown.kde.org/~charles/kate.png">screenshot</a> of Kate, and let's look over some of the silly things in it.

<ul>
<li>There are <em>ten</em> menu items</li>
<li>What's the difference between a File and a Document, in the menu (apparently it lets you select the file to edit)</li>
<li>In the File menu, when you save, does it save the project?  It doesn't, but why not? Why make it so easy to not save something?</li>
</ul>

We should actually let the user know that there is both a <a href="http://ktown.kde.org/~charles/katefiles.png">project</a> and also a <a href="http://ktown.kde.org/~charles/kateedit.png">text window</a>. Both of these would be the documents, and it's clear what everything actually is for. (Both of these screenshots are fake)
<br /><br />
For now, KWrite remains a great text editor, and it doesn't have the flawed Project stuff.
<br /><br />
Maybe my next rant will be on how stupid "IDEAL" is, like how it hides the document when you click on a tab.
<!--break-->