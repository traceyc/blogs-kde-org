---
title:   "Just One Week"
date:    2004-12-28
authors:
  - njaard
slug:    just-one-week
---
On Tuesday of next week, I'm off for the Netherlands!  I hope I'll get to see some snow there.  Last friday, the tickets for the Sirenia (I mean, Tiamat) <a href="http://www.013web.nl/ProgrammaDetail.asp?color=33CCFF&amp;id=1020">gig</a> showed up.  Furthermore, I found the web site for <a href="http://www.pain.cd">PAIN</a>, which doesn't really help as the links to the samples are all broken.
<br><br>
Today, <A href="http://metz.gehn.net/">Stefan</a> and I had a vast discussion (argument) over the design of Noatun 3 (which is currently being developed in the make-it-snow branch of cvs).  I want the playlist to be directly manipulated when you go to another song or whatnot, Stefan <i>seemed</i> to disagree.  Turns out we agreed almost entirely and so then I just had a few minor suggestions to make to his refactorings.
<br><br>
I haven't been developing much KDE stuff at all recently, so upon completion of my current uni course work, my plan will be:

<ul>
<li>Finish that last bit of Photobook so that it knows which mimetypes it can open (currently I hardcoded JPEG)</li>
<li>Start taking an interest in Noatun make-it-snow</li>
</ul>

I just need to finish this one last assignment for uni and I'll be done until the next semester.  So I'll get to it now. I now return you to your regularly scheduled programming.

<!--break-->