---
title:   "Why do good people do bad things?"
date:    2005-10-09
authors:
  - njaard
slug:    why-do-good-people-do-bad-things
---
<code>
arkimedes:~# apt-get install dbus-qt-1-dev
(stuff skipped)
The following NEW packages will be installed:
  dbus-qt-1-dev dbus-qt-1c2 hicolor-icon-theme kdelibs-bin kdelibs-data
  kdelibs4c2 libarts1c2 libaspell15 libfam0 libjasper-1.701-1 libnetpbm10
  libopenexr2c2 menu-xdg netpbm
0 upgraded, 14 newly installed, 0 to remove and 2 not upgraded.
Need to get 19.9MB/21.0MB of archives.
After unpacking 69.5MB of additional disk space will be used.
Do you want to continue [Y/n]?
</code>

Ok, I have to investigate. Why does DBUS's Qt bindings want to link to all of Qt?

So I grab the DBUS source code, compile it, do an <tt>ldd libdbus-qt-1.so</tt> -- to my dismay, it links to libkdecore.so!  What gives?

So, upon further investigation into the DBUS build system, I find, much my dismay, the following:
<code>
dnl linking to kdecore will give us a bit of help from libtool
if (! kde-config &gt;&amp; /dev/null); then
    have_qt=no
else
    kdelibs=`kde-config --install lib --expandvars 2&gt;/dev/null`
    if test -z $kdelibs -o ! -f $kdelibs/libkdecore.la; then
        have_qt=no
    else
        DBUS_QT_LIBS="$kdelibs/libkdecore.la"
    fi
fi
</code>

Brilliant.  They link to KDE just because KDE links to Qt, and they need Qt. Sheer brilliance.  I'm impressed.

(it should be noted that the cvs version of DBUS doesn't have this problem).

So, I submitted a <a href="http://ktown.kde.org/~charles/dbus-qtlinking.diff">patch</a> to the Debian maintainers (as that's the distribution I use).  I'm sure they'll reject it, as they always do.

<!--break-->