---
title:   "Why do people hate KDE?"
date:    2005-02-12
authors:
  - njaard
slug:    why-do-people-hate-kde
---
Well, Konqueror (khtml) has been <a href="http://www.howtocreate.co.uk/browserSpeed.html">proven</a> to have the fastest free HTML renderer available for Linux.

So why is it that the people on <a href="http://slashdot.org/articles/05/02/11/2016227.shtml?tid=95&amp;tid=1">slashdot</a> refuse to not only mention it in the "article", but with the default settings and no user account, I can't even find a mention of it on the entire page (with the comments).

What do we need to do for these people to demonstrate that our technology is in fact superior?
<!--break-->