---
title:   "Strict Is Just Another Quirky Mode"
date:    2006-06-12
authors:
  - carewolf
slug:    strict-just-another-quirky-mode
---
Some might have seen the summary before, because I posted this blog last week, but pulled it again because I wanted to double check that my recommendation is sane in MSIE. Turns out my assumption was right so here it is again:

Let me start with a rant. I see more and more websites using strict DOCTYPES, apparently webdevelopers have some misguided notion that this is most correct thing to do and guaranties the most consistent result. Nothing could be more wrong!

Let us examine the theory. Strict more is a mode for the browser to implement correct standard behavior. The browser will implement 'transitional' and 'quirks' mode to handle every broken site it gets delivered and provide 'strict' mode as a tool for webdevelopers to check if their design works when interpreted as strict as the browser is capable of. This means in theory that when strictness bugs are detected in the browser implementation, they are always fixed in strict mode, but only fixed in transitional and quirks if no important websites depend on the bug. This means for a website that uses strict DOCTYPE that the website can be rendered differently for each new browser version and suddenly end up looking broken, because it actually relied on a bug. For this simple reason, websites should never be published with strict DOCTYPE. Strict doctype is only a development tool.

Now for the facts: There are many websites everywhere that uses strict doctypes. Even nerdy technical minded ones like slashdot makes this stupid mistake. This of course means that browsers cannot fix bugs in strict mode without breaking websites (and breaking websites is the last thing a browser developer want), and with so many important websites relying on bugs in strict mode, strict becomes more and more quirky. 

For Konqueror this basically means: 
In quirks mode we implement IE5- bugs.
In transitional mode we try to act as sane and accepting as possible.
and in strict mode we implement Mozilla bugs (under the assumption that webdevelopers that use strict at least test in both MSIE and Firefox).

The next thing that comes up when I tell people this is: "But, this is all fixed with XHTML right?"... Nope, XHTML is much much worse! Since XHTML doesn't really work in MSIE when served correctly 99% of all XHTML sites out there doesn't follow the XHTML standard in even the most basic ways (delivered with text/xhtml+xml and starts with <?xml> tag). As a result all the browsers treat sites with XHTML doctype in either a special XML-quirky transitional mode or simply as HTML4 transitional, and thus bugs are not detected and thus websites depend on being treated quirky. 

So... What's to make of this: I don't really know. Except that the idea of making new strict types doesn't really work in practice, and just results in more and more weird modes in the browsers; all with their own set of quirks. The only clear lesson is for web developers: Just use HTML4 transitional.
<!--break-->