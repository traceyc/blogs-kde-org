---
title:   "Carewolf on Safari"
date:    2005-04-08
authors:
  - carewolf
slug:    carewolf-safari
---
It looks like my thesis will be late (Gee, what a surprise), and since I soon have to buy the apartment I've been renting, I've been looking for a job, and finally found one.

The job is to port a specialized spellchecker for dyslexic to Mac and Linux with Mac being the first priority. It is actually pretty cool software, it is even context aware, which will probably be the hardest part to just plug in.

As part of the job I just received a MiniMac yesterday, and I've been playing with to see what it can do:

First the MiniMac is really sweet looking; a nice little package :)
When booting up the first time it starts a pre-installed installation routine. Maybe I've been inhaling a little too much of the Apple fanatics hype, but I had actually expected Apple to be nice people, but when I saw the inescapable registration I was really disappointed. I suddenly had to use my Windows skills and entered "Fuck you" as first name, "Steve" as last name and with "support@apple.com" as email, making sure the "please spam me" check box was checked. They were actually even more clever than Microsoft here, because my phony registration was duplicated to be my personal info in the address book. I was also surprised it couldn't detect that I had no ethernet link at the time when it started searching for a DHCP server on LAN, but OTOH Debian detects that is no ethernet connection and sends a DHCP ping over the non-existing connection anyway.

After the installation was completed it booted into OS X and I could start tampering. First I connected my ethernet and tried to go online; no luck. Okay, it was no link detection, so I need to setup it up again. Unfortunately there was no LAN or DHCP option in the direct configuration. After searching for it, I tried the help wizard, and oddly here in the "for dummies" wizard there was the option not available in the direct power-user interface! 

I should note that while doing all this I noticed a few really annoying things. First the interface is very slow. I feels almost like a GNOME application except that no half-drawn intermediate widgets are shown. It just takes 100-200ms for every widget to react. The MiniMac is hardly the most powerfull Mac, but I kinda surprised that my 3-4 year old Athlon 800MHz/256MB with KDE 3.2 beats the crap out of it performance wise. The second thing is the ever present acceleration in the interface. The mouse compared to Unix and Windows default is very slow initially but with a much higher acceleration which makes it feels almost eratic. More interestingly the mousewheel is also accelerated. I am actually starting to think that is a good idea, just implemented horribly by Apple because I can only chose between scrolling 1 line (single click) or 10 lines (double click). I also noted a qute little feature, the suppress toolbars and crap button in window decoration. We should have that in KDE, at least as a standardized short-cut.

I finally got the internet going and could start Safari. This was what I've been waiting for, as a KHTML developer I really wanted to see how Safari was.
Again I was disappointed with the speed. The odd thing is that the progress bar almost gives the impression that it is loading the page that is slower. I just don't understand how the MiniMac on the same connection can be 3-4 times slower than Konqueror to load the same page. I am starting to suspect the rendering speeds and scrolling through webpages is also very jumpy (although I must confess I getting really used to my smoothscrolling hack for KHTML). What rocks though is the fonts. I can't stress this enough. The webpages just looks much more professional in Safari, much more authentic, the news sites seem looks like printed articles. The fonts are soo sweet. at least at a distance. If the contrast with the background is not good they can easily seem ghostly (not blurry, they are very sharp just hard to pick out).

The next part is surf bugs.kde.org with Safari and figure out which of our bugs they have fixed, so we can search for the fix in the humongous diff.
<!--break-->