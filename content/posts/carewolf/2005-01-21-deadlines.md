---
title:   "Deadlines"
date:    2005-01-21
authors:
  - carewolf
slug:    deadlines
---
Lately I've been busy with many different projects, and deadlines are approaching. On the 2nd of February we enter full feature freeze and there are many things I "need" to have done by then. Too make things worse I go on ski-vacation on the 28th, and also have deadlines for my <i>real</i> (non-hobby) projects in February.

The things I hope to have finished for 3.4 are full CSS2.1 support in KHTML. With the white-space completed just before new-year (we are now in a very exclusive club of browsers that support pre-line and pre-wrap), I just need to complete generated content: counters and quotes. In aKode more and more people are pushing me for additional features, after it has become a direct option for playback in JuK. The AAC/MP4 decoder looks realistic to complete as FAAD2 is easy to use even if completely undocumented, I will just have to postpone the maturing(bug-fixes) of OSS and ALSA sinks to after feature-freeze; and when do get time to write the Amarok engine? I should also update Ogg/FLAC decoding in TagLib because the silly people in FLAC decided to change the format between 1.1.0 and 1.1.1 (what do you expect of developers who would change source API between 1.0.2 and 1.0.3?)

At least the good thing about being a "lazy son of bitch" is that I am pretty immune to stress. Most of my time the last week have been used the on new Hearts of Iron II where I am showing the world the might of Nazi Germany. As a runner up for my time is my new girlfriend, then KDE, then my study. I just have a strange feeling I am supposed to reverse my priorities...
<!--break-->
