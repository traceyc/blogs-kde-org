---
title:   "Yes, GNOME sucks, but please use it if it suits you"
date:    2005-12-13
authors:
  - carewolf
slug:    yes-gnome-sucks-please-use-it-if-it-suits-you
---
Seems Linus has started <a href="http://www.osnews.com/comment.php?news_id=12956">a new little flamewar</a> :D

Among KDE developers we have always stated that anti-GNOME flamewars are only among users, and while it is technically true. It doesn't prevent users who also developers in other projects to join them. I will just comment here and hopefully not contribute to the fire.

Personally I agree with Linus basic critisism of GNOME, but not his conclusion. GNOME has always been the underdog and while they have gained a lot of recent corporate support (people always root for the underdog, right?), they very much still are among linux (power-)users and developers. Being the underdog puts a lot of presure on you, especially if you have fewer developers and therefore fewer resources. You just don't stand a chance catching up by following the leader, this has forced GNOME to go a different route than KDE.

Sure I hate everything that GNOME does different from KDE; because KDE is doing things the Unix/Linux way of empowering the user and taking pragmatic decisions, but GNOME has taken chances that KDE could not take: They have risked alienated parts of their users in order to make a distinct interface, that may or may not attract users that might not otherwise use Linux. This is good for the community as a whole! 

The only really bad thing about GNOME is that a few of their developers (hello, monkey-boys!) have never let go of the "There can be only one"-attitude, and continue to attack and attempt to eliminate KDE, however futile that may be.

I can understand how the Ximians provoke a general hostile environment that would provoke some users to strike back at GNOME in general, but it is wrong: Yes, GNOME sucks, but please use it if it suits you!