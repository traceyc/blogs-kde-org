---
title:   "QtWebKit 2.3.0 is out"
date:    2013-03-15
authors:
  - carewolf
slug:    qtwebkit-230-out
---
Good news everybody!

QtWebKit 2.3.0 was <a href="https://gitorious.org/webkit/qtwebkit-23/trees/qtwebkit-2.3.0">tagged</a> in gitorious yesterday with the <a href="https://gitorious.org/webkit/qtwebkit-23/archive-tarball/qtwebkit-2.3.0">tarball</a> available there as well

For those of you that don't know: QtWebKit 2.3 is a port of QtWebKit from Qt 5 to Qt 4.8. It has most of the web-facing features, stability fixes and performance improvements that QtWebKit in Qt 5 has, it has skipped anything Qt 5 specific such as QQuickWebView, but has almost all improvements on the WebKit1 side (QWebView). QtWebKit 2.3 also maintains API and ABI compatibility with QtWebKit 2.2 from Qt 4.8, and is thereby an easy drop-in replacement. The released version 2.3.0 has roughly the same WebKit version and patches as Qt 5.0.2.

Note that QtWebKit 2.3 is not an official Qt release, nor will be. We recommend users to upgrade to Qt 5, but for those stuck with Qt 4.8 for the time being and who is using QtWebKit, I would personally recommend trying out QtWebKit 2.3. I have also had great feedback from the developers of the Rekonq, and Qupzilla browsers, that also recommend users to try out 2.3, several distributions are either packaging or planning to package it, the first being Arch Linux who has  been packaging QtWebKit 2.3 since the betas.

Note that the sources are only buildable using the build-webkit tool, and requires the QTDIR environment set even if only to /usr. The basic build-command is "Tools/Scripts/build-webkit --qt --release --no-webkit2". If you are packaging to x86, you might also want to add --no-force-sse2 since the library would otherwise default to using SSE2 math. Additionally you can use --qmakearg="CONFIG+=production_build" to link with less memory. Finally I have added an option to include WebP support by adding DEFINES+=HAVE_LIBWEBP=1 to the qmakearg. After building the you need to go to WebKitBuild/Release and run make install.

If you have any questions, you can check my earlier posts about QtWebKit 2.3, ask on webkit-qt@lists.webkit.org mailing list  or catch me on FreeNode IRC #qtwebkit.
