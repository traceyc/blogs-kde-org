---
title:   "KHTML 3.5.6 the most CSS3 supporting browser"
date:    2007-02-05
authors:
  - carewolf
slug:    khtml-356-most-css3-supporting-browser
---
Well, at least according to <a href="http://www.css3.info/blog/khtml-356-is-the-most-css3-compliant-of-all/">css3.info</a> . It is nice to when the changes you make, make a splash :D

Though seriously the most unique CSS 3 support we have are the CSS 3 Selectors I have meddled with for two years, the rest is mainly imported from WebKit in some form (such as text-overflow:ellipsis and CSS 3 Background and Borders), sometimes the imports has been easy sometimes painfull. 

I makes me reflect on what to do with KHTML. I am still torn between WebKit/Qt and KHTML. Personally I am starting to have much less time to devout on KDE hacking, and there are always a lack of developer resources in KHTML. It would be really nice to be able to offload much of the work to other companies. OTOH Apple are still a pain to work with. I've submitted a few patches to them and had them rejected because; they lacked regression-test output (when the regressiontest only ran on Mac OS X. Fortunately Lars Knoll later wrote a new for Qt), or because they don't like where I place my spaces in the code. It seems even a tech-oriented company like Apple can't escape a bike-sheding middle management that makes rules that wastes valuable developer time and resources. 

I am still going to try to work a little with Apple on WebKit/Qt. A lot of what is happening in WebKit is very positive, and it is nice for a change to get feedback on your patches, even if the feedback is mostly criticizing the color of the bike-shed.