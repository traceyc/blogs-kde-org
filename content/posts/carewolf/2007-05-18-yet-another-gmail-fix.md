---
title:   "Yet another GMail fix"
date:    2007-05-18
authors:
  - carewolf
slug:    yet-another-gmail-fix
---
As some might have noticed GMail was updated this morning. As usual this broke the standard view Konqueror, but only if you use the recommended method of spoofing as Firefox. 

Fortunately I traced it down and fixed it in no time at all :D

Here is the patch for your convenience (apply in kdelibs/khtml):
<code>
--- /home/carewolf/html_elementimpl.cpp 2007-05-18 10:48:33.000000000 +0200
+++ html/html_elementimpl.cpp   2007-05-18 10:50:36.000000000 +0200
@@ -562,6 +562,13 @@

 void HTMLElementImpl::setInnerHTML( const DOMString &html, int &exceptioncode )
 {
+    // Works line innerText in Gecko
+    // ### test if needed for ID_SCRIPT as well.
+    if ( id() == ID_STYLE ) {
+        setInnerText(html, exceptioncode);
+        return;
+    }
+
     DocumentFragment fragment = createContextualFragment( html );
     if ( fragment.isNull() ) {
         exceptioncode = DOMException::NO_MODIFICATION_ALLOWED_ERR;
</code>
<!--break-->