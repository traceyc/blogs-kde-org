---
title:   "Simulacrum"
date:    2005-05-24
authors:
  - canllaith
slug:    simulacrum
---
Lately I've been looking at various Linux photo management apps in preparation for writing a comparison. After some struggling I managed to get digiKam working under Slackware. The hotplug scripts are a very nice touch. When I plug my camera in, hotplug launches a usbcam script that sets the permissions on the device for the current user and launches digiKam, which immediately connects to the camera and shows you a window of thumbnails you can import from your camera. 

<img src="http://www.hoult.org/~canllaith/blog/digikam.jpg" class="showonplanet"/>

I set it up for a friend of mine on her Fedora laptop and she seems pretty impressed with it too. Tip: remember to put your camera in ptp mode, else hald will continously mount it when you plug it in until you want to tear your hair out ;)

This program is absolutely lovely. I could rhapsodise all day about how well put together and feature rich digiKam is. For anyone who hasn't tried it yet or has perhaps been put off by attempting to resolve the dependancies by hand - it's worth the effort!

Some photographs from my walk around the harbour yesterday:


<a href="http://www.hoult.org/~canllaith/blog/panorama.jpg" target="_blank">
<img src="http://www.hoult.org/~canllaith/blog/panorama_thumb.jpg" class="showonplanet"/>
</a>
(click for full size)
Panoramic view of the harbour

<img src="http://www.hoult.org/~canllaith/blog/hedgehog.jpg" class="showonplanet"/>

Bruce pointed out a baby hedgehog in the path in front of us. He was running away on spindly little legs that seemed absurdly skinny to support such a rotund ball of quills. We caught up with him and he posed prettily for some photographs then allowed me to stroke him. New Zealand wildlife is amazing. I can't imagine attempting to touch an Australian wild animal without losing my hand ;)

Thanks to those who told me how to get images to show up on the planet

<!--break-->
