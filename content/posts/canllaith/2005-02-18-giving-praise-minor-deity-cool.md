---
title:   "Giving praise to the minor deity of Cool."
date:    2005-02-18
authors:
  - canllaith
slug:    giving-praise-minor-deity-cool
---
I'm making plans to possibly move to Sydney sometime within the next few months. For those of you in the US, New Zealand is not a part of Australia, and I even need a passport to go there. It's not a huge move since the countries are so similar and I am actually Australian to start with, but moving country at the best of times is a Big Pain. 

It will be nice though to be able to get:

a: golden gaytime icecreams
b: solo lemon drink
c: baked beans in ham sauce

Not having these things in New Zealand has made living here quite a trial. You don't know what these things are? No! You haven't lived!

I'm also looking forward to seeing what the various Syney LUG are like, and being 'on the spot' so to speak for various linux and OSS conferences in Australia. I'm applying for work with an employer who is very sympathetic and interested in OSS which is Double Plus Good. 
I'm hoping I can really put some quality time into pursuing a further involvement with KDE this year. The only downside to this really, is it's Sydney! Unfortunately, they wont relocate their entire operation to NZ for me, even though I asked them nicely. 

So anyone who is reading, fingers crossed for me and prayers to the deity of your choice!

:)