---
title:   "Trepidation"
date:    2005-04-06
authors:
  - canllaith
slug:    trepidation
---
Job interviews really, really suck.

The traditional upbringing of an Australian fundamentalist christian renders you almost incapable of 'selling yourself' effectively in an interview. As a child, you learn that to state your own talents is 'bragging' or 'being up yourself'. It makes it very difficult as an adult to then learn how to market yourself to an employer. They ask you 'So, why should we give you this job?' and you look at them blankly. 'Uh.... um, cause I wouldn't suck at it too much?'

I'm not in the country, so it was choppy webcam, crackly sound and strange accents. Oh lord. I could barely hear them and that definitely impacted on my ability to answer questions effectively. I sucked, badly. I got the 'So why does your resume show that you moved around so much in the last few years?' question as usual. As though it's a bad thing to travel. I feel drained and downright sick from panicking about it, but I guess it will all work out in the end =) There are other jobs, and who knows? Maybe I didn't look like a *complete* idiot, and might get it anyway. Maybe.

=)