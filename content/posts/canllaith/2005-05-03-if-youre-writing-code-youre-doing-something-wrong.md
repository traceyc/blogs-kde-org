---
title:   "If you're writing code, you're doing something wrong"
date:    2005-05-03
authors:
  - canllaith
slug:    if-youre-writing-code-youre-doing-something-wrong
---
This evening I went along to a Mac developers meeting. We were invited to sit in a circle and introduce ourselves one by one.

"Hi, I'm Jessica and I'm a Linux geek and open source hacker. I don't use macs."

It was primarily a presentation about Web Objects. It was interesting, although it mostly made me think 'I must get around to reading up on Ruby on Rails'. One quote from the presentation was 'If you're writing code, you're doing something wrong'. I tried really hard not to smirk. I've always found it amusing the style of marketing Apple seem to focus on: 'If you're too stupid to use a PC, this is the OS for you'. In the buzz of mingled conversation afterwards I heard KDE mentioned a few times.

On mentioning KDE I saw reactions I'd observed in the past. Some people there had impressions of KDE from early versions, mostly of interface inconsistency between KDE and non KDE applications like Netscape. Even though these versions are quite a few years old these negative imprints are retained - and even worse, expressed as opinions to others who may not have tried Linux or KDE at all. I'd like to think of some effective ways to promote what KDE's strengths are today to Unix user/developer groups. 

An interesting night. 

<!--break-->

