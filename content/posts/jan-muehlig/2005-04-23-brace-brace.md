---
title:   "brace brace"
date:    2005-04-23
authors:
  - jan muehlig
slug:    brace-brace
---
<img src="http://www.openusability.org/bracebrace.png" class="showonplanet" />
While usability guys normally can never sleep (because there is always something to complain about, if only the dream), they at least can laugh sometimes. As in a plane recently. A wonderful instruction table for emergency thingies. Perhaps this is only funny if "brace" is not in your vocabulary (in German then, you would read it "bratze"). However, what I saw reminded me more of something like audio terror.