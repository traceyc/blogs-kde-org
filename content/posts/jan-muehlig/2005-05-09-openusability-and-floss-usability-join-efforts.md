---
title:   "OpenUsability and Floss-Usability join efforts"
date:    2005-05-09
authors:
  - jan muehlig
slug:    openusability-and-floss-usability-join-efforts
---
The <a href="http://www.flossusability.org/">FLOSS-Usability</a> project is a strong initiative to bring developers and usability folks together. Their first "Sprint" took place in February in San Francisco and it was a amazing workshop <a href="http://www.userbrain.de/sprint.jpg">(image)</a>. Eugene Eric Kim then brought up the idea of usability mentoring or caretakers who support the projects if the developers want it. This "matchmaking" was and is one of the basic ideas of <a href="http://www.openusability.org" >OpenUsability</a>, so that we decided to put our efforts together. That's in fact georgious news, because the Floss-Usability initiators have so much speed and they are very excellent in getting people together and keeping things rolling.<!--break-->