---
title:   "Status of KJSEv4"
date:    2005-06-01
authors:
  - geiseri
slug:    status-kjsev4
---
Oh man, these last few weeks have been rough.  I got a new toy, but that is an entirely different blog all together.  So here is the lowdown on what I got working with KSJEmbed in the last few weeks:
<ol>
        <li> Qt signals and slots can now be connected from KJS.
        <li> Static methods now work on bindings.
        <li> QWidgets and QLayouts can be created from custom plugins.
        <li> Qt UI files can be loaded and manipulated from Qt
        <li> Qt slots and Properties can be called from Javascript.
</ol>
What remains to be done in order of priority:
<ol>
        <li> Automatic creation of bindings via some sort of script.
        <li> Enum support.
        <li> Support for all QVariants (pending really on 1).
       <li> Connect Qt signals to javascript slots.
        <li> Mapping Qt events to javascript events.
       <li> Ability to add bindings at runtime via plugins.
        <li> QPainter, QDom and QSQL bindings (pending on 1 again).
       <li> javascript console 
                I really want to do this in javascript and we should 
                have enough in place to start on this.
        <li> better system io support.
                I want a way to be able to issue shell commands and
                process the output similar to the bash `` feature.
                Also I want the ability to read and write text files.  A simple
                readline/writeline support should be fine imho.
        <li> libffi support so we can transparently call C based libraries.
                I played with this some, its a neat idea, but it is limited only
                to C methods, and really starts to putter out when you call
                methods that dont have POD types.
</ol>
I have posted a link to the current snapshot:
http://www.sourcextreme.com/projects/kjsembed/snapshots/kjsembed-2005-06-01.tar.bz2

If people are interested in helping out please let me know.  Really all of the 
hard hard work is getting finished, what remains is for people to start 
working on examples, bug hunting.  I am pretty busy now but ideally development can pick up in the next few months, now that Qt4 is in current development I could probably start talking about merging my work in to KDE again.