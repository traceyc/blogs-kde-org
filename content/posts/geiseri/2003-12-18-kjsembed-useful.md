---
title:   "KJSEmbed is useful!"
date:    2003-12-18
authors:
  - geiseri
slug:    kjsembed-useful
---
Well up until now most KJSEmbed examples we have had seemed very contrived.  This morning we got into a problem with building buttons for our web site.  We decided it was not worth doing in PHP since the buttons never changed, but none of us had the talent to build them by hand.  So enter "cvs:[kdebindings/kjsembed/docs/examples/buttonmaker/buttonmaker.js|ButtonMaker]" :)

[image:274,middle]

ButtonMaker is ~100 lines of ECMAScript that provides a nice script interface to building buttons for our web site.  While Qt does most of the dirty work, its impressive that in about an hour I was able to crank out a script that would fulfill an actual need.  This gives me a certain comfort that KJSEmbed will prove useful for KDE 3.2.