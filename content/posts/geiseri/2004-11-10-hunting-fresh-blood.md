---
title:   "hunting for fresh blood..."
date:    2004-11-10
authors:
  - geiseri
slug:    hunting-fresh-blood
---
So one of the reasons I went to Mexico last week was to get more Latin American developers involved in KDE.  So it looks like I got one and thats cool.  We need more developers on this side of the world.  We already have some cool guys, but we could get more.  Maybe November should be "Hook a friend" month.  I wonder what would happen if each of the *American devs got one new developer involved with KDE?  Would we get cool new features? Would we get new top bug fixers?  Might be fun.