---
title:   "This is a test post from KBlog"
date:    2003-07-21
authors:
  - geiseri
slug:    test-post-kblog
---
This is a test post from KBlog.  If it works it will be ready for CVS.  Currently there are issues with the post manager.  The selection dialogs need some refinement. 

There are also some big UI issues, and some missing code, but I think there is enough here that other developers will want to get a start.

Editing posts seems to work also.