---
title:   "DCOP Widgets ideas"
date:    2003-07-20
authors:
  - geiseri
slug:    dcop-widgets-ideas
---
DCOPWidgets are widgets that are accessble via dcop.  You can load a UI file that was built in designer and then access it from any lanuage that can talk via dcop.
<!--break-->
<b>User Interface File:</b>
The first step is to create a UI file for use with your program in Qt designer.  Every widget you wish to access from dcop should have a name so KDE can build the interface for you.<br>
<IMG SRC="https://blogs.kde.org/images/img_25620ade90eae95168ebdfaa2ed42aca" WIDTH="799" HEIGHT="548" BORDER="0" />
<HR>
<b>Shell Script:</b><br>Next you write your script to manipulate the UI, and interact with your system.  DCOPWidgets mapps most common Qt signals to events that you can handle from a case statement in the event loop.

<pre><small>
#!/bin/bash
UI="file:///home/geiseri/dcopwidgets/src/test.ui"
WIDGET="Test"
ID=""
./dcopwidgets --name $WIDGET --file $UI | while read line
do
        case $line in
        exit.Released)
                dcop $ID $WIDGET exit
        ;;
        Starting.*)
                ID=`echo $line | awk -F. {'print $2'}`
        ;;
        go.Released)
                USER=`dcop $ID $WIDGET lineInput`
                proc=`ps -x -U $USER`
                dcop $ID $WIDGET setmlEdit "$proc"
        ;;
        MyChoice.Released.1)
                dcop $ID $WIDGET setlineInput "root"
        ;;
        MyChoice.Released.0)
                dcop $ID $WIDGET setlineInput "geiseri"
        ;;
        *)
        ;;
        esac
done

</small></pre>
<HR>
<b>DCOPWidget in action:</b><br>Once it is all in place, run the script and the form comes to life.  As you can see from the UI file above each Qt widget has a dcop interface so that you can manipulate it just like you can from C++.<br>
<IMG SRC="https://blogs.kde.org/images/img_6b7cc86bc7f87bb02f15796ac8dd2b81" WIDTH="614" HEIGHT="491" BORDER="0" />
<IMG SRC="https://blogs.kde.org/images/img_529b4e6f9cf2c947f0f0ea012c2cee46" WIDTH="384" HEIGHT="514" BORDER="0" />
<br>
This is only the crude begining.  Im still adding more support for more widgets and building documentation.  Stay tuned for more power from DCOP.