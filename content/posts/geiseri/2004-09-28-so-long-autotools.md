---
title:   "So long autotools!"
date:    2004-09-28
authors:
  - geiseri
slug:    so-long-autotools
---
So I am fed up with autotools.  Its possibly the worst possible build system I have ever had the displeasure of being abused by.  Its probably because I never got into m4 macros, or testing the size of an int of my machine every time I try to build a project.  So finally with the arrival of Package Config and QMake I can rid myself of this horrible mess.
<br>
Anyone looking at the QMake documentation might discount it immediately as a serious build system.  Since its VERY simple.
<code>SOURCE += main.cpp
CONFIG += application debug thread
</code>
Is a very simple case.  Even the "CONFIG" portion is optional though because it will use the current Qt configuration if none is specified.
<br>
"So okay I can build Qt apps, what about KDE apps?" you might ask.  Well this is where a neat application kde-config comes into play.
This application will tell us all sorts of things about kde as long as the binary is in our path. 
<tt>kde-config --expandvars --install lib</tt> will return location of the KDE libraries live.  To get a full list of all the path elements kde-config returns just issue a <tt>kde-config --types</tt>.
<br>
Now with this handy program we can use the <tt>system( )</tt> directive in QMake to populate things like our include paths.
<code>QMAKE_LIBDIR+=$$system(kde-config --expandvars --install lib)
LIBS += -lkdecore -lkdeui
# For KDE 3.3
KDEPREFIX=$$system(kde-config --prefix)
INCLUDES += $$KDEPREFIX/include $$KDEPREFIX/include/kde
# For KDE 3.4
INCLUDES += $$system(kde-config --expandvars --install headers)
CONFIG += debug
SOURCE += main.cpp kdeapp.cpp
HEADERS += kdeapp.h
</code> 
One other thing to note, since we don't have the brain damages of autotools to deal with anymore, the #include "kdeapp.moc" is no longer needed to get the appropriate moc files generated.
<br>
I have imported one template into KDevelop cvs for those who are on the bleeding edge.  If you dig into QMake you can find tons of neat helper functions, custom compiler directives, libtool and package config support. It's really a worth while thing for developers to look at.  If for nothing else than the ability to remove almost 2mb of useless crap from your source project, or the issue of new versions of autotools trashing your precious admin directory thats deprecated long before you get it...  Build systems are evil, but some are less evil than others.