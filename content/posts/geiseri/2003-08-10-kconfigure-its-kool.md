---
title:   "KConfigure - its Kool"
date:    2003-08-10
authors:
  - geiseri
slug:    kconfigure-its-kool
---
So my wife wanted to install a kde app a last night that she found on source forge... okay no biggie, i told her to just get the RPM off of the SuSE cd...  one problem, the program was not there, soooo i told her to download the source and install it... BIG MISTAKE!*

okay tar -zxf && ./configure && make && make install may be easy enough for you average geek but my wife (an electrical engineer who programs and maintains industrial equipment all day) was lost after the "x".  So to find a fix for this badboy since being the sysadmin here was getting old.  Enter KConfig (<a href="http://kconfigure.sourceforge.net/">sourceforge<a>) a GUI wrapper arround the ./configure && make && make install process.  Note it says it needs KDE 2.x, but i was able to build it with both 3.0 and head.  Now the process was simplified.  She was able to down load the file, drag the file to her "unpack file" icon on the kicker that untarred the archive, click on the "configure" file and follow the wizard.

Now the cool part (my wife is a smart lady, but she is a fish out of water when it comes to silly unix commands), Kconfigure spelled out the options is a clear manner.  It was nice enough to make things clear without dumbing it down for my wife.  It supports profiles so it was able to keep a set of options so she only had to enter values once and just use the same profile for other kde apps.  Make and make install where also very clear, even allowing her to install as root with the correct password.

In short I was pleased, my wife now has KPlayer and we are all happy :)

* Yet more evidence that linux can be detrimental to your love life ;)