---
title:   "Windows development just got tolerable...."
date:    2004-06-11
authors:
  - geiseri
slug:    windows-development-just-got-tolerable
---
There has always been this inane call to port KDevelop to windows so we can entice windows developers to Qt.  Personally I think this call is bullsh*t, and think it makes about as much sense as saying "More people will use Macs if they port Adobe Photoshop to Windows."  None the less, I still have to do windows development for some customers...

So I did what any sane person would do, and used KDevelop. 
[image:484]
Yes, I use KDevelop for Windows development.  No, I don't use Linux to develop and then recompile on windows.  In fact I do not have Windows installed at all. No, I did not port KDevelop to Windows.  Instead I patched QMake to better deal with cross compiling, and then made a makespec file to use mingw32 as a cross compiler, and wine as a tester/debugger.  The resulting binaries work perfectly on windows with no ill effects.  In my opinion philosophicly and economicly this is the best way to go.  Why pay Microsoft to develop apps for Qt?  With this setup, I can use Commercial Qt to develop my windows apps without paying the Microsoft tax, and I can use an ANSI compliant compiler without penalty.  

This is the future people, we don't need to port more Linux apps to Windows, we need to make Linux a viable platform for ALL software development.  Ideally the Trolls will get the message and promote this behavior more too...