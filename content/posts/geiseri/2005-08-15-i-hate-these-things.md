---
title:   "i hate these things..."
date:    2005-08-15
authors:
  - geiseri
slug:    i-hate-these-things
---
At 10pm yesterday my father in law phoned me to tell me that his father was in the hospital with pneumonia, and will most likely not be going home.  He has been quite sick for some time, and over the past 3 years more and more complications from cancer, chemotherapy, surgeries etc have really tired him out.  Still, its hard even when you can see it coming.  This left me the painful task to breaking the news to jess...

So after a long evening of tears and comforting we booked my wife a ticket to get back there and I dropped her off at about 4am this morning.  Since we didn't solidify plans until about 1am we opted to just keep going. I went to Meeting and then came home a crashed.  I was shockingly lucid for being so tired, but all I could think about was the fact that this was for the best, no matter how much it hurts.  I also was thinking about my wife's guilt that we moved half way across the country to pursue careers and our various adventures.  She is the only one in her family who went to get a degree, and she is the only one in quite a few generations to leave her home town for reasons other than military service.  Needless to say we are quite removed from the day to day adventures.  My family has historicly been pushed to follow their dreams and aspirations, so beyond our bi-yearly meetings we are used to being far and wide.  So this feeling of guilt for not being "close" doesn't pain me the way it gets to her.  

So I left her crying at the airport and I booked a ticket for Thursday to join her.  I have to shore some things up here for her with respects to school in two weeks, as well as some pressing tasks at work.  I am still planning on Malaga, since the ticket is non-refundable, but I feel kind of guilty doing so.     

Oh well enough rambling for now...  

