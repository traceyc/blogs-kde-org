---
title:   "finally some visual progress...."
date:    2008-01-29
authors:
  - geiseri
slug:    finally-some-visual-progress
---
So finally i have some visual progress to the wiki editor.  Right now it is more of a wiki viewer, but its progress :)

[image:3236]

So far the basic character formatting is working great.  Thanks Thomas, Tobias and Hamish for your help.  Tables are next.  Well see how that goes.  It looks like it will be harder because I don't know the table size up front.  I think I can create the cells on the fly, but well see.  Its not clear how to create a table from the docs.  Lists look easy though.  Nesting lists are still broken, but that is a parser issue and not a rendering issue.

It is always exciting to see your app take shape though, and even more exciting when you get the GUI parts working!