---
title:   "Order from Chaos"
date:    2008-10-30
authors:
  - geiseri
slug:    order-chaos
---
I have been facinated with mind mapping now for about 3 years.  I have been using it on a regular basis for trying to organize disjointed thoughts, make coherent sense out of brainstorming sessions, and lately for every day design documentation.  For those of you not familiar with mindmaps check out the <a href="http://en.wikipedia.org/wiki/Mind_map">wikipedia link on the subject</a>.  

KDissert annoyed me at the time, and freemind was pretty slow. So a little over two years ago I started to play with Qt's graphics view and started a mind mapping toy. Its grown a bit over the last few months as I use it more and more every day at work.

So let me now introduce you to Flo.
<img src="http://www.geiseri.com/screens/welcome_to_flo.png"/>

Flo is a basic mind mapping utility that can be used to quickly organize brainstorms into hierarchical outlines.  So far I have been able to use it on a daily basis. It has the ability to import freemind files and export Docbook, Wiki, or Text outlines.  It also has an attempt at generation of PDF slides but that kind of got morphed into MyPoint.  Feature wise it has the basics, undo/redo, autosave, spell checking, printing, group operations and auto layout of nodes. Qt makes it all painfully easy.  The hardest part was finding an icon set that didn't suck as well as a spell checker library that ran on windows.

I admit the UI needs some help but I feel its getting close to a releaseable state.  I am shooting for the end of November for a release into the wild.  If anyone has any hints or mockups of what I could do with the UI feel free to send them my way :)  



