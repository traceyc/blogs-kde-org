---
title:   "KDE on Windows...."
date:    2004-12-09
authors:
  - geiseri
slug:    kde-windows
---
The lie:
Step 1) Porting KDE applications get Windows users used to KDE applications.
Step 2) Once they are used to the applications they migrate.

The reality:
Okay this is "Programmer Logic".  Now lets look at the way business works.  Rule #1 the cheapest thing is to do nothing.  The second cheapest thing is to do as little as possible.  This is why all those "Temporary Solutions" also known as "Hacks" or "Kludges" always last for friggen years.  What porting apps to Windows does is keeps those people who could possibly migrate to KDE for the use of a single killer app to NOT migrate to KDE.  If the only compelling reason for Linux is cost, why would they bother beyond the bare minimum.  

Next lets apply "Marketing Logic".  Okay one of the big cornerstones in marketing is to aim for markets that you can significantly stand out in.  How many image viewers, text editors, and other utilities out there exist on Win32 vs the Linux Desktop?  I will leave an afternoon at tucows.com as an exercise to the reader to answer that one.  Well just say if you think the number of CD burning utilities for KDE is absurd, you will have a good laugh on the windows side.  So the point here is that we will have no compelling marketing advantage other than we are "Free as in beer".  Now lets see, what are the odds that the market that wants "Free as in beer" will ever buy something?  For those of you who might have slept through economics, I'll say its not so high.

Lastly lets explore the message we are sending by providing KDE applications on Win32.  Basicly we are saying "We think our application is great, but the platform of KDE and Linux sucks so bad, we are going to ship on Windows so you don't have to use it"  There is a myth that "If you make an application available for a source platform from a target platform that it will increase the adoption of the target platform."  Lets ask Apple how well that worked out for them when Adobe ported their applications to Windows.  Or Quicktime or iTunes... it was always about gaining market share of their other products.  They actually attribute the iPod to increase in the Macintosh market in past years.  I wonder why HP sells an iPod only for Win32...  again ill leave this as an exercise for the reader.

So this is it.  Its my take as a businessman, and my take as a developer for KDE.  I _do_ write cross platform apps in Qt, and do use GPL projects on win32.  Why?  BECAUSE I WANT TO MAKE MONEY and I do what my customers what.  There is NO reason about adoption, etc, etc.  My customers biggest concern is not about its portability.  They care about its ability to be less expensive, less buggy and faster to develop.  Oh by the way it will run on Linux and MacOS X too is okay, but RARELY ever brought up.  Then again I do most of my Win32 development with Linux, and if Wine had support for write resource, I would not need Windows at all.  

So if the people of the Win32 port wish to make money, all the power in the world to them.  They should not lie to others and themselves though, they are truly NOT helping adoption of the Linux desktop, they are helping the adoption of their app.

PS. I'm not responding to comments here, because the same people will say "He is just hates my project/me/my pet/my shoes/etc." or that "I don't see the future" etc...  I gave my reasons, and I gave evidence.  Please share yours, or at least your corner cases of some shop that adopted Win32 KDE so they could move to linux _later_... and actually did...