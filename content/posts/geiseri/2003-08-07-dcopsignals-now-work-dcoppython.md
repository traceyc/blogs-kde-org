---
title:   "DCOPSignals now work in DCOPPython"
date:    2003-08-07
authors:
  - geiseri
slug:    dcopsignals-now-work-dcoppython
---
Okay so I couldn't sleep last night, so I spent the night playing with dcop python and found that dcop signals where missing.  Needless to say I added them.  

cvs:[kdebindings/dcoppython/test/signal.py|This] short example script shows off the happy new functions in action.

Now I really have to get this damn KScriptEngine done for Python, this blows shell away for speed, and seems to be very reliable.  I cant wait until n7y, by the time that is over we should have KJSEmbed and Python script engines done.

The other amusing thing is that now that <a href="http://www.kde-look.org/content/show.php?content=5810" target="parent" title"KDE L00K">karamba stuff</a> can actually integrate into KDE seemlessly :)