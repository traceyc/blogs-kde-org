---
title:   "we need to abandon freedesktop.org"
date:    2004-01-28
authors:
  - geiseri
slug:    we-need-abandon-freedesktoporg
---
In the beginning it was a neat idea.  They wanted a common set of standards for interop on the desktop... Then somehow it all went wrong, somehow implementations started to pollute these standards.  This has caused quite a quandary, now somehow we have these standards that are bound to technology, some of which we could argue is pretty poor technology.  IMHO this is reason to abandon efforts to follow these standards until they become standards once again, instead of implementations.  

In the last 3 months KDE has integrated more with GTK and Gnome without these standards, this furthers my annoyance with freedesktop.org.  While things like dbus and their xserver are interesting, they are implementations and imho inappropriate for a standard.  It would have been far more effective if there was a protocol established that projects could implement natively.  If we choose to use common implementation cool, otherwise don't trap people into them.

I don't see a reason to follow standards that cause use to be forced to use a platform.  It seriously sounds like a Microsoft approach to things.  "You can work with our platform, if you are running on it"...

oh well... the nice thing about standards is there are so many to choose from.