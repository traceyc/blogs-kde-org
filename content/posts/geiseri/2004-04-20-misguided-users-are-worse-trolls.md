---
title:   "misguided users are worse than trolls..."
date:    2004-04-20
authors:
  - geiseri
slug:    misguided-users-are-worse-trolls
---
I love open source.  Software development is my hobby and passion.... its my release and my work of art.  Personally I take great pleasure when people out there say they run KWeather, or like some other feature in KDE that I have worked on....

What I cannot stomach though are users who insist on spreading lies and FUD at every opportunity because they are too lazy or stupid to find out what we are doing here.  A while ago I asked on IRC why all these stupid people where on the Internet... you would figure people that friggen brain damaged wouldn't be able to type.... I was told all typing did was raise the bar slightly... So in fact we have the cream of the crop morons on the Internet now.  Personally since I was told its illegal to stab or electrocute people over the Internet i propose we move to Dvorak keyboards or some other format that will filter these people out...

Its not that i don't want to hear negative things about KDE, I know we have problems... and i know there are genuine trolls out there.  i like trolls they are usually at least creative and kinda fun to parade around once you give them a good trashing... its the misguided meat heads to blindly repeat the same thing over again until they go back to watching American Idol or some drivel...

in summary, I like most users, I really like users who send bug reports, I love users who send fan mail about my apps, and I adore users that become developers, the only ones I hate are these fear mongering brain damaged users just trying to find a way to declare how horrible the platform could possibly be...

oh well... not like this rant changes anything, but it feels much better =)