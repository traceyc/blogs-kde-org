---
title:   "Annoyances with Kolab and Outlook"
date:    2003-07-26
authors:
  - geiseri
slug:    annoyances-kolab-and-outlook
---
Slowly progress is made, we finally got our plugin to load cleanly in Outlook proper this week now that we moved from MS's crappy STL attempt to STLport.  There are still some issues with using STL in DLLs but we can login and dont get 5000 assertions from their buggy STL.

Now that Kolab is 1.0 though things can get more stable.  We had been targeting generic IMAP for the longest time since we could never get a stable server.  Now we have had a stable server now for a few days and we can do some testing.  If all goes well we should have a demo at n7y at the end of August.  Now I guess we need to work on iCal and vCard parsers :P

I cant wait to do some KDE programming to ease my brain....