---
title:   "Python + DCOP = Cheap Public Relations"
date:    2003-08-07
authors:
  - geiseri
slug:    python-dcop-cheap-public-relations
---
So i was mucking with dcop some tonight and playing with the new python bindings.  The new bindings are much faster and are self contained so you can use them from PyGTK or PyQt.  So I was hacking some and came up with a trival app that I was able to drag and drop almost directly from KDCOP.

<pre>
#!/usr/bin/python
import pcop
import dcop

m_KWeatherServiceWeatherService = dcop.DCOPObject( "KWeatherService","WeatherService")
replyQStringList = m_KWeatherServiceWeatherService.listStations()

for stationID in replyQStringList:
        date = m_KWeatherServiceWeatherService.date(stationID)
        name = m_KWeatherServiceWeatherService.stationName(stationID)
        temp = m_KWeatherServiceWeatherService.temperature(stationID)
        report = name + " " + temp + " at " + date
        print report
</pre>

This simple app will then output:

<pre>
Bremerton National Automatic Weather Observing / Reporting System 18°C at Wednesday 06 August 2003 11:55 pm
Pottstown, Pottstown Limerick Airport 22°C at Wednesday 06 August 2003 11:54 pm
Philadelphia, Northeast Philadelphia Airport 22°C at Thursday 07 August 2003 12:54 am
Milwaukee, General Mitchell International Airport 21°C at Wednesday 06 August 2003 11:52 pm
Longview, Gregg County Airport 29°C at Wednesday 06 August 2003 11:53 pm
</pre>

Now this was cool for about 5 minutes of work...  Now to see what I can do with Kate and the Python KScriptEngine....