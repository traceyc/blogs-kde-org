---
title:   "promotion..."
date:    2004-04-19
authors:
  - geiseri
slug:    promotion
---
I've been working on these articles for KJSEmbed and have loved the response I've gotten so far.  We have generated new interest in the project and gained a few followers...

it makes me think of all the untapped potential out there in KDE. we have this very nice desktop environment, sure it has a few glitches, but we are getting there.  what we seem to be missing is promotion.  now people say we need a multi-million dollar marketing campaign, some people say we need a company to support us... but sadly i don't see these things happening any time soon....  maybe instead we can write more articles. maybe we can show our managers about solutions.   i dunno... but i think us showing off our stuff to the world will do more than complaining about lack of exposure ;)