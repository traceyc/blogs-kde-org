---
title:   "Path to perfection... ( a plea for feedback )"
date:    2008-11-02
authors:
  - geiseri
slug:    path-perfection-plea-feedback
---
“Perfection is reached not when there’s nothing left to add, but when there’s nothing left to remove.” - Antoine de St. Exupery

I have been frustrated with the UI of Flo now for the past week.  As mindmaps grow larger, I keep feeling the UI get crowded. Now maybe there is nothing that can be done to fix this.  I don't know, but I wonder what you in the audience think.  I feel my input is now rendered useless because after 2 years I am so trained on how it works I don't notice the UI braindamages.
<!--break-->
<img src="http://www.geiseri.com/screens/Flo.png"/>

So can anyone provide me with mockups of a cleaner interface?  What would you remove from the toolbar and hide in a menu?  What would you do with that "Property" editor on the lower right hand corner?  Is it better served as a floating dialog? Is the title editor better served as an in-line editor on the mind map?  Or should it be on a dashboard of the main UI.  Should the top toolbar be smaller?

Also for any of you who are interested I have subversion opened at: http://svn.geiseri.com/svn/projects/flo.  You will need at least Qt 4.3, perl and HunSpell's development libraries installed.  On windows I have included HunSpell. 


