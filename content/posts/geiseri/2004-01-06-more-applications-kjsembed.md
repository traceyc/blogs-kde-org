---
title:   "More applications in KJSEmbed"
date:    2004-01-06
authors:
  - geiseri
slug:    more-applications-kjsembed
---
Well it has been a busy two weeks. I have been fixing a few bugs in kjembed here and there and pushing its limites with my two scripts...
<!--break-->
oh yeah i have a new script [Envelope Maker|EnvelopeMaker]. 

As the name implies it makes envelopes.  Basicly we needed a nice way to make one off envelopes here at the office, so I whiped up this script.  The PS code portion came from a python script I have been nursing along for years after porting it from a perl script i found on usenet :)

At any rate ButtonMaker [image:290,middle] is getting ever cooler.  You can now generate custom colored and sized pillboxes on the fly.  Im working on makeing the shape more flexable, but since it now does 99% of what I want I am not sure that will happen.

At anyrate, look for KJSEmbed scripts comming to you this spring...
