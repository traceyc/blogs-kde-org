---
title:   "Things I learned in México"
date:    2004-11-04
authors:
  - geiseri
slug:    things-i-learned-méxico
---
<ol>
<li> Burritos are not Mexican food. ( I was crushed on this revelation )
<li> "Ian" (e-in) is an insult in Spanish. ( J-Pablo taught me everything I know about the language )
<li> The only words J-Pablo knows in English are "you f*cking bastard". ( There was some other grunting and mumbling but I was told this is normal for him )
<li> There are no women in Chile, only llamas. ( I will never go to a porn site in .cl again )
<li> Mexicans do not actually practice canabalism. ( I had to sort this one out for all my inbread friends in Texas )
<li> Crossing the streets in Mexico is their solution to population control. ( It's like lemmings with people and busses man )
<li> Duncan and J-Pablo can not sing along to "Guns and Roses".  (I was told this was a form of Latin American torture.)
<li> HP has superior technology. ( At least when it comes to picking out their booth babes. )
<li> Mr. Potato is the most popular application in México. ( I have no clue how this happened )
<li> Tequila is better than llama piss (pisco). ( I think there is a connection here between this one and #4, but I will leave that as an exercise for the reader )
<li> J-Pablo need to get more issues of linux journal. ( Either that or we need to stop drinking Nescafe )
<li> Never steal j-Pablo's tacos. ( I have never seen someone get so upset over missing tacos, then again I think he was still drunk at the time )
</ol>
<!--break-->
Basicly this week has been quite a bit of fun.  Here at the XpoLinux I had a choice of staying in a hotel, or staying with a host.  I wanted to meet new people in Open Source so Duncan and I chose to stay with a host.  So they put us up at a Gnome developer's house.  Needless to say, we drank way to much, and had a blast.  J-Pablo is a great host even if he sings badly.
<br>
It was nice to see the OSS community in .mx growing.  I think it is horribly underestimated by .us and the rest of the world.  While they are no silicon valley, they do have a growing tech center in Monterrey.  I think we also scored a few more Mexican developers and someone who will do a Latin American localization of KDE (something sorely needed).
<br>
KDevelop was a wild success, and it made quite a powerful impression for people who think GUI programming is difficult.  I blame Microsoft and Sun for this, but that is another story. At any rate there is an opportunity here, and I think KDE is going to be there.  
<br>
For the record there where no Gnomes, Dragons, or Llamas killed in the process of my educational experiance.