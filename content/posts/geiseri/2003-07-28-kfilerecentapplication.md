---
title:   "kfile::recentApplication"
date:    2003-07-28
authors:
  - geiseri
slug:    kfilerecentapplication
---
We have this nice class kde:KRecentDocument (we dont use it nearly where we should but thats another rant ;) ).  As I have been working at making KDE more usable I found we dont track recently run applications the way we should.  We seem only to track them via the kicker.  While this is nice, its useless for those of those of us who use krun, or the quickstart applet.

To work arround this I have been playing with kde:KRun and using a class krecentApplication, a class almost identical to kde:KRecentDocument.  The cool thing here is that recently used applications that start from all over KDE start to appear in the kicker menu.... once we have this data, phase two starts, but more on that later...