---
title:   "Progress is slow but sure."
date:    2003-09-27
authors:
  - geiseri
slug:    progress-slow-sure
---
Man its been almost a month now since i posted...

Well n7y was great and i got to start on a ton of stuff that im just now finishing.  The coolest one IMHO is KJS support in KDevelop 3.0. Currently I can create a new project in KDevelop to edit KJSEmbed scripts, and the debugger is loaded. [image:203,middle].  

The next step will be to get code completion and maby the function wizard to work properly.  I hope I can get that finished tomorrow. KJSEmbed::KJSEmbedPart() seems to give me a fair amount of access to the JS context so I might be able to use that to get some information about the script without having to parse it.  Maby ill get lucky ;)

Also this weekend I want to move the KUIViewer into kdesdk, so I can also use that part to debug UI files for use with KJSEmbed.  More on that later....  first some sleep and get this bad boy to do some code completion...