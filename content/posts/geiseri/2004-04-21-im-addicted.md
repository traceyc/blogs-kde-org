---
title:   "im addicted..."
date:    2004-04-21
authors:
  - geiseri
slug:    im-addicted
---
This last weekend I finally broke down and wrote my second most favorite game in the whole wide world "Daleks" ie "Robots" for those Americans who got the magazine "Creative Computing"...
<!--break-->
At any rate I have now completed the game per the original spec.  The story goes that "Dr. Who" is stuck trying to escape from these robots called Daleks.  Now these dudes are not too smart, they just want to "Exterminate!", so they are prone to running into rubble and each other.  The good doctor is not exactly defenseless.  He is armed with a "sonic screwdriver" capible of destroying a dalek, and the ability to teleport.  Needless to say its been a riot! i truly have had so much fun both coding it and playing it.  I used KJSEmbed to write it to see how it pushed the language.  I had to fix a QBoxLayout bug in HEAD, but other than that it works very well.  Now I just need to decide how to release it.  Transpose it into C++ for inclusion into KDE games.  This in fact might be a nice demo to see how well KJSEmbed prototypes.  Any way well see... I now have to get past level 20. :)
