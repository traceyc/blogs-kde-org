---
title:   "Linuxworld banner art is FINALLY finished"
date:    2004-01-12
authors:
  - geiseri
slug:    linuxworld-banner-art-finally-finished
---
I never thought this day would ever come, but finally I got enough together so I could get the banner for linux world ordered.  Its only been on hold for 3 weeks now as I've tried to extract useable artwork out of various KDE people.  Finally after some sincere and direct prodding tackat was able to produce a pdf file that was able to scale to the size needed for the banner.  

[image:306]

At 7' wide by 1' tall this sucker should look pretty good.  Maby this year KDE can look like a real project.  In the past I've been embarrassed by our presence at trade shows.  Its like we just where in town one day and decided to occupy a booth...  This year George Staikos was at the helm and he did an awesome job getting everything together.

We still have to get through the show, but for the first LWNY since 2001 we have real organization.

Now... to get those booth presentations all lined up...

 