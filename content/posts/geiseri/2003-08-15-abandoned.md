---
title:   "abandoned..."
date:    2003-08-15
authors:
  - geiseri
slug:    abandoned
---
Okay so after years of holding out and waiting as my powerpc linux systems get more and more obsolete, ive given up on SuSE.  They refuse to maintain anything other than ia32, not that i dont blame them.  They are a company and a companies first job is to make money.<!--break-->  Because of this im going back to debian.  Debian and I have a real love/hate relationship.  I love it for simple things like that remote boot weather monitor station in the back yard.  It boots from a bootrom and chugs away on a nice 12mb image...  But i HATE it for doing desktop work.  Im just not a tinkerer, so im not into editing 30 files, reading 6 howtos and still dicking with my settings just to get X to work on a strange monitor, that SuSE's wizard autoprobes... So thats the rub...  Debian a pain in the ass to use, but at least you have the opportunity to use it...  Yes i know there is gentoo, but again it ignores my annoyancees with debian.  Really I hate to dick with my system to configure it.  I hate mucking with settings just to get things to work.  Yes Debian 3.0 is better at detecting network cards, and sometimes sound... but 2 months later a sound card that worked in SuSE still wont work on my other laptop.  Bah, ive got to pack and get ready for n7y... maby Zack and George have better ideas of how to make this laptop usable again...

DISCLAIMER: before some pimple faced debian brat takes this as a debian troll, id like to say i have nothing against debian as a dist, but i really have problems with some of the maintainers.  if this post offends you, deal :)