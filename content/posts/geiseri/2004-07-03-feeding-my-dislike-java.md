---
title:   "Feeding my dislike for java..."
date:    2004-07-03
authors:
  - geiseri
slug:    feeding-my-dislike-java
---
So it seemed like a good idea 3 months ago.  We needed a utility that would create Palm data files, and embed images in them. We had a java pdb generator already so we figured hey, parse in the data file and images and we are home free. Sounds simple right? WONG!
<!--break-->
What I found was a one way trip to hell.  It seemed that after some struggles to get the 8-16mb files to generate without the JVM running out of memory, we got it to generate these files in about 2 minutes, and only use 128mb of ram.  Toying with Java's image IO we further got the image sizes down, so that the file would write without an out of memory exception.  Then then after another bug where randomly the JVM would die if the destination directory on the NT server wasnt there and the java app had to create it i said UNCLE..

So called up TrollTech and got a Qt license for the project, we wasted about 2k in my time already, might as well stop the bleeding.  So I ported it to Qt from java.  First observations where I had to manage the deletes and clear out pointer arrays... okay yeah yeah java people actually knowing what the hell your program is doing is not a defect in the language! You can balance curyly braces, how hard is it to balance new and delete?  

Second i noticed all the inane casting in and out of the vectors and lists went away.  now that the code was clearer and smaller i could work on it again... the last nugget of pure joy was the Qt image io...  the QImageIO is like pure heaven compaired to the god damn color models and crap java makes you go through...

So the damage was done, the unit tests took about 4 hours to port, and the main app 3 hours. the unit and validation tests confirmed operation... shockingly the entire function and logic of the application remained completely unchanged.

So what did 2 grand get me?  Well with Java the app took 2 minutes to process 1300 records. It takes Qt/C++ 16 seconds (yeah what slushdot meathead said java was faster than C++).  With Java the app took up about 128megs of ram under NT, with Qt about 8mb (shocking what actually managing your memory yourself can get you ). And the best part, Qt's image IO is so awesome, with Java the generated data file was 3.0mb, with Qt it was 1.9mb... to java's credit, when we used the imagemajick jni interface it was about 2mb.  maybe Sun needs to invest in some the Trolls some...

Now to be fair, im not an awesome java programmer, but I cannot see how i can have more code that does less in a language that is suppose to be easier than C++...  then again i didn't buy the marketing hype, i bought Qt.