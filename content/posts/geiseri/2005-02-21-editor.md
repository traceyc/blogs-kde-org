---
title:   "Editor"
date:    2005-02-21
authors:
  - geiseri
slug:    editor
---
Okay so zack and i where at lunch talking about KDE 4.  One item that came up was the idea of a simple editor part.

Currently KOffice has the KoShell, but imagine having something with the Kate interface that could edit images, text, simple rich text, sounds, etc...  This would by no means replace KDevelop or Quanta, but it would provide a neat unified interface for users to edit files.  Konqueror currently gives us this same behavior for read-only parts.  It has made KDE a very useful and consistent desktop environment.  My wife never knew she had an image viewer until one day she accidentally clicked on it.  Konqi gave her all that.  Would this help new users and KDE consistency by having a unified simple editor?

Opinions

