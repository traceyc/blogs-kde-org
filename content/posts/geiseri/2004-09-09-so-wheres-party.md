---
title:   "So where's the party..."
date:    2004-09-09
authors:
  - geiseri
slug:    so-wheres-party
---
One thing I always come back from the KDE meetings in Europe from is how friggen spread out the US developers are.  Every year Zack and I try to get a few east coast KDE devels together for a hackfest.  Last year we had a cool meeting with Adam and Hammish, but ironicly Zack could not make it...

So I wonder when or if we should try it again this year.  Ben talked to me about having a hackfest in July, but it just didn't work out with me and Zack.  We have enough bodies to have quite a party, and rumor has it if i turn the heat on in my house, its a nice place to have a hackfest ;)

So how about a KDE Halloween party in Philly?  Ill provide the party location and the chips ;)