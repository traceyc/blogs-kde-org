---
title:   "My approach to text based presentation software"
date:    2008-02-18
authors:
  - geiseri
slug:    my-approach-text-based-presentation-software
---
I was inspired by <a href="http://blogs.kde.org/node/3280">Alexander's blog</a> about text based presentation software.  This has always been a sore point of mine.  See the problem with most software is they are focused on making it pretty but not the content.  This imho leads to a wasteland of bullet points with no flow, and people who just read their points right from the slides.

So a few years ago I hacked together a tool that took an input file and generated a post script file that I could display with KGhostView...

So fastforward 3 years and Alexander's blog and it inspired me to fix it up a little.  Since I have grown an affinity for wiki, I updated the input format to take a small subset of wiki formatting. I also made it export PDF since Qt can now do that.

So the end result is <a href="http://trac.geiseri.com/wiki/MyPointMain">MyPoint</a>.  Its still one step above a hack, and the code is a mess, but it works.  If anyone finds it useful please let me know.  I can be coerced into expanding on it if people use it more.

Right now it takes optional background and layout template files to customize the output.  These are pretty limited, but that is because I am more concerned with the content than the prettiness factor.  The wiki format gives me a fast way to generate an outline and the fact each point must be one line helps keep me short an concise.

So please check it out and let me know what people think...
