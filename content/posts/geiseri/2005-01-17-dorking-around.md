---
title:   "Dorking around..."
date:    2005-01-17
authors:
  - geiseri
slug:    dorking-around
---
Yeah so I think this is some evidence as to why I don't have any friends ;)
<a href="http://www.wxplotter.com/ft_nq.php?im"> 
<img src="http://www.wxplotter.com/images/ft/nq.php?val=5771" alt="I am nerdier than 90% of all people. Are you nerdier? Click here to find out!"> </a>

In other news, I am almost finished with my KScript how-to for application developers and users.  KDevelop is now pretty well supported and Kate is usable too.  Must finish my last outstanding KJSEmbed bugs in the next two weeks... man I need more time.