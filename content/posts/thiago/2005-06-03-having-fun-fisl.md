---
title:   "Having fun at FISL"
date:    2005-06-03
authors:
  - thiago
slug:    having-fun-fisl
---
So I arrived this morning in Porto Alegre for the <a href="http://fisl.softwarelivre.org/6.0/index.php?language=en">FISL</a>. Morning's been nice: we've met the Trolltech representative here and had some productive conversations. I am looking forward for some talks tonight, including one by Scott Collins.

If you don't know what FISL is, take a look at their webpage. The acronym "FISL" translates to Interational Free Software Forum, and is held yearly here in Porto Alegre, southern Brazil. It started as a government initiative, but has now taken a life of its own. According to the webpage, almost 4400 people have visited this year, so far.

Helio Castro and Andras Mantia are here too. Bug them to tell their stories. More on this later on, as I have more time to write.