---
title:   "Ironies or flawed logic?"
date:    2005-06-21
authors:
  - thiago
slug:    ironies-or-flawed-logic
---
Aaron said in his <a href="http://aseigo.blogspot.com/2005/06/lessons-in-irony.html">blog</a>:

<ul>
irony: too much water causing not enough water
</ul>

So I have another:

<ol>
<li>Swiss cheese has holes</li>
<li>The more cheese, the more holes</li>
<li>The more holes, the less cheese</li>
<li>Conclusion: the more cheese you have, the less cheese (!!)</li>
</ol>

<!--break-->

I know I had something important to talk about, but I forgot.<br>

<small>I am posting this here because I didn't want to register to submit a comment to aseigo's blog</small>