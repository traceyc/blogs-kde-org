---
title:   "aKademy day 3: It all goes routine"
date:    2005-08-29
authors:
  - thiago
slug:    akademy-day-3-it-all-goes-routine
---
Day 3 in Málaga (for me) was a bit routinely for me. I woke up rather late, considering the previous night's party and went to Dirk's talk on Firefox and KDE. I missed some interesting talks, like the KCall one, that intended on attending. I think I'll just have to watch the videos when they are made available.

We had the keysigning party yesterday, with lots of attendees -- around 40 or something, but with some notable exceptions (I'll have to track those down and exchange fingerprints). But it was interesting to see the line move, and to find out the real names for some KDE <a href="https://blogs.kde.org/blog/1192">contributors</a>.

We then went for dinner in town, which is similarly priced to the University residence, but with a much nicer scenery (if you get what I mean). Plus, the wireless there actually works! No less than 11 APs in range of the café where we had dinner.

By the time I had got back, Waldo had arrived safe and sound to the residence, after being lost. They were engaged in a discussion about scripting at that time. And Aaron was also there, after having secluded himself for the better part of the day. (and, of course, everyone that arrived exclaimed "Aaron is alive!" or "Where have you been"). The discussion than shifted to usability with the arrival of Celeste Paul, but somehow got changed into other subjects I wouldn't dare blog about.

What looked like would be a good night's sleep actually was shortened, because I went to bed at 2 am, and had to get up before 7 to attend the keynote speech by Trolltech's CEO.

On a side note, I had an interesting and fruitful meeting yesterday afternoon. More details on that as they become available.