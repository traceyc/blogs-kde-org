---
title:   "The same and not the same"
date:    2006-01-07
authors:
  - thiago
slug:    same-and-not-same
---
In a follow-up to <a href="http://c133.org/blog/">clee's</a> <a href="http://c133.org/blog/personal/headpigeons.html">recent blog</a>, I'd like to add I'm in a similar, but not exactly equal situation.

I've quit my current job this Monday and I'll be moving to Norway in a couple of weeks to work for Trolltech. And I'm also waiting for the second half of Battlestar Galactica's season. :-)
<!--break-->