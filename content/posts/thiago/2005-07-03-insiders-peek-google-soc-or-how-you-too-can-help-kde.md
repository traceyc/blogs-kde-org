---
title:   "Insider's peek on Google SoC -- or how you too can help KDE"
date:    2005-07-03
authors:
  - thiago
slug:    insiders-peek-google-soc-or-how-you-too-can-help-kde
---
<p>I thought I'd let the world know how I came to be managing a bit of the Google Summer of Code thing on the KDE side. I certainly had not expected it. And I think it's useful for contributors to know how they can join and help: it doesn't take much, just some free time and willpower. I won't lie, though: knowing the right people and being "in the loop" helps.</p>

<p>If I were to summarise the recommendation, I'd use <a href="https://blogs.kde.org/user/view/175">fab</a>'s words:</p>
<p align="center"><i>Think big, act small.</i></p>
<!--break-->
<p>It all started on that Monday when the announcement came in and caught us all by surprise. Eva Brucherseifer and <a href="https://blogs.kde.org/user/view/14">Zack Rusin</a> contacted Google and set everything up. We came up with a few proposals of our own and placed them on <a href="http://developer.kde.org/joining/googlesummerofcode.html">http://developer.kde.org/joining/googlesummerofcode.html</a>. The sysadmins also set up the bounties(AT)kde.org email alias in order to receive the proposals and make suggestions. I sent a proposal idea of my own, but it was deemed too specific.</p>

<p>Initially I did some promo activity, by placing the notices on our two channels on Freenode (<a href="irc://irc.freenode.net/kde">#kde</a> and <a href="irc://irc.freenode.net/kde-devel">#kde-devel</a>), as well as sending to a couple of mailing lists I subscribe to and the Brazilian-Portuguese KDE website (<a href="http://br.kde.org">http://br.kde.org</a>). Then I joined the people helping students with their proposals. It did take some time to find the right people to approach though.</p>

<p>Then, after the proposal submission deadline was past, a week went by, while Google and other admins decided how to proceed. Finally, a vote was opened for a few people from the <a href="http://www.kde-ev.org">KDE e.V.</a> and I was among them.</p>

<p>On June 25th, Google released the <a href="http://groups-beta.google.com/group/summer-discuss/browse_frm/thread/8b5b85e38fc8efbc/62024b5ec92784f7">number of stipends awarded</a> per organisation participating. Through our admins, we got to know the 24 proposals accepted for KDE and began asking for volunteers for the projects.</p>

<p>On the next morning, lots of people had volunteered. But I saw that no one had put together a list of who had volunteered for what. So I took it upon myself to do that: I opened KSpread and collected the names. And that's how I started to actually <b>manage</b> things: a small action that led to taking the responsibility for the mentors.</p>

<p>After that, it was up to me really to bring the mentors together, to compile a list of them, to prepare stories for the <a href="http://dot.kde.org">dot</a> (<a href="http://dot.kde.org/1119719572/">1</a> and <a href="http://dot.kde.org/1120137340/">2</a>), to prepare a <a href="http://developer.kde.org/summerofcode/">website</a> for the proposals, etc. Why was it up to me? Because I noticed no one else was doing it and I had the necessary conditions to do it:</p>

<p>I had the information, the means and the will.</p>

<p>So that's it: everyone can help in small steps. Join whatever area of KDE that you feel deserves more attention and do what you can, however insignificant it may look to you at first. You don't need big plans. With a bit of help from everyone, we can accomplish a lot.</p>

<p>But please don't forget that you also need legitimacy: don't do anything that you're not supposed to do. If in doubt, contact someone else who may be able to offer advice. Contact me if you don't know who to contact, and I'll help you find someone to help you.</p>
