---
title:   "Report on IDG's Developer's World, Aug 17th"
date:    2005-08-20
authors:
  - thiago
slug:    report-idgs-developers-world-aug-17th
---
Here goes a summarised report of the talk I presented. The presentation (in Portuguese) is at http://developer.kde.org/~thiago/presentations/IDG-17-08-2005.pdf (source and beamer theme: 
<a href="http://websvn.kde.org/trunk/promo/presentations/2005_08_SaoPaulo_IDG_Developers_World_pt-BR">trunk/promo/presentations/2005_08_SaoPaulo_IDG_Developers_World_pt-BR</a>)

The conference was IDG Developer's World, held here in São Paulo on Aug 17th. The target audience was mostly corporate people. <a href="https://blogs.kde.org/blog/74">Helio</a> asked me whether they were IT managers or higher up: I can't tell. I know I saw a lot of badges with company names, including consulting companies, but I don't know what positions these people hold.

My talk was about "how an open source community works", with special emphasis on KDE of course. I presented more or less who is in communities, how we work, and the power structure. And I gave one brief overview of what the KDE project is and the applications we have developed.

People seemed interested, despite some heads visibly nodding off. I asked the staff how we were split and I was told it was about 70 people in my talk and 75 on the one next door. That one was no less than the conference sponsor: Microsoft. And neither room was crowded, so I didn't get stuck with: I was chosen.

The talk lasted for 45 minutes and then I opened for questions. I was asked how I had got involved, what motivates me and if I was being paid. Contrast that to the talk before mine, by an (apparently) well-known name in the Microsoft world: he got no questions at all.

What people seemed most impressed about is the size of the project. I got interesting reactions when I said we had reached 450,000 commits, or that we had had 5k commits in the week between my writing of the presentation and the actual presentation. (I think I also got some laughs when I said developers aren't known for their communication skills)

I was really nervous when it started. Helio had thrown me to the lions there: mostly corporate people, managers and decision makers (I couldn't confirm). Not only that, it was my first real presentation to real strangers, and not being supervised by a manager. However, I think in the end it went fine. My father, at least, was very pleased with my talk :-)

Quick facts:
<ul>
<li> the very first question the journalist asked was "What is KDE?" 
(to which I awkwardly replied, pointing at my laptop, "this is all KDE. All you see here is KDE")</li>

<li> the journalist also asked some tough and extremely vague questions like "how many open source communities there are", "how many contributors are there to KDE", etc. The only one I could really answer was "how many Brazilian contributors are there to KDE"; the other ones I gave some figures out of the blue and asked not to be quoted on.</li>

<li> I got free lunch and a free laser pointer pen. My sister has already comandeered it for her own presentations, though.</li>

<li> I got to meet Gustavo Niemeyer, of Smart (www.smartpm.org) fame, but unfortunately neither had their GPG key fingerprints available. He watched the end of my presentation (his flight was 1h late) and I didn't get to watch his (I had to get back to work)</li>

<li> as was expected, I was asked for a business card</li>
</ul>
<!--break-->