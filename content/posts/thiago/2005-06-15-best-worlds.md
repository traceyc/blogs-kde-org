---
title:   "Best of the worlds"
date:    2005-06-15
authors:
  - thiago
slug:    best-worlds
---
So what would the best buildsystem be, according to me?

<pre>./configure
make
make install</pre>

In case you've been startled, read on.
<!--break-->
First of all, let's differentiate the system configuration checker (a.k.a. configure) from the builder (a.k.a. make). I don't mind if it is the same tool that does both jobs, but in my mind they are separate things. But I didn't say autoconf and make/automake.<br>
<br>
The build system has to be described by a simple file. I don't need complex rules to tell my build system that library libfoo is composed of foo.cpp, bar.cpp and baz.cpp. I would much rather see a simple <tt>NAME = VALUE</tt> based file, as much as possible. So writing a Makefile.am would be best for me.<br>
<br>
Any complex rules required by the build system (DCOP, kconfig_compiler, kdeinit, --enable-final) would be handled by a backend: a script of some sorts that would parse my simple <tt>NAME=VALUE</tt> file.<br>
<br>
This build system would produce simple, concise output when nothing is wrong, along with a progress indicator:<br>
<pre>10% compiling kdecore/kapplication.cpp
11% compiling kdecore/kurl.cpp</pre>
<br>
It would also know the difference between compile jobs and non-compilation jobs. That is so that I can tell it to run two linkers at once along with 6 compilers, one linker and 7 compilers, or 8 compilers (imagine a dual-processor computer on an <a href="http://websvn.kde.org/trunk/kdenonbeta/icecream">icecream</a> network with other 5 computers/processors). Not only that, it would query icecream to find out how many compilation slots are available --- at <a href="http://conference2005.kde.org">aKademy</a>, computers join and leave the network at all times.<br>
<br>
I don't mind it not doing comparison on the pre-processed output, ignoring whitespace. We have distcc for that, and if needed be, icecream could do it too. If it did that, though, it would have to save the files somewhere outside the build tre, just like distcc: I rm -rf the build tree before starting a new build.<br>
<br>
It should also, on “<tt>make install</tt>