---
title:   "aKademy day 2: First day of conferences"
date:    2005-08-28
authors:
  - thiago
slug:    akademy-day-2-first-day-conferences
---
As <a href="http://dot.kde.org">the dot</a> published <a href="http://dot.kde.org/1125166012/">yesterday</a>, the KDE World Conference 2005 started with an opening ceremony where local government officials attended, as well as aKademy Assembler Antonio Larrosa and KDE e.V. president Matthias Kalle Dalheimer. The impression I get is that government everywhere supports Open Source/Free Software in one level or another, so this is good news for the movement.

I watched that ceremony and a few other talks, including the NX one by Kurt Pfeifle and the collaboration one by Mark Shuttleworth. I could also manage to read over 800 emails (or mark as read), but my backlog of kde-bugs-dist emials is growing. I'll have to find time for that either today or tomorrow --- not to mention the paper I have to write.

I also joined the discussion groups on IPC and on scripting for KDE4. On the first one, we discussed to some extent what level of integration we need to port KDE to DBus and how it would interoperate with a KDE3 environment, which is DCOP-based. Unfortunately, we could not progress much further because, in the end, there are several technical questions that need to be answered and some key people for the discussion were not present. We plan on following up on that in a day or two.

The second discussion group, the one about scripting, even though difficult, did make some progress. We settled that we want to support primarily only one scripting language for KDE4: JavaScript 1.5. But, before stones are thrown at me by Python-lovers, Ruby-lovers or lisp-lovers, let me make the point even clearer: we want to support that language primarily, meaning that it will be available in all KDE applications that support scripting at all. Other languages may be supported by individual applications wherever the need for it arises. In fact, that decision isn't different from the one the <a href="http://plasma.kde.org">Plasma</a> developers had already reached: they decided to support JavaScript in core and to offer support for other languages through plugins. The engine to be used will still be evaluated by the binding team, so stay tuned for news from them.

What then followed was a social event sponsored by Novell/SuSE on the Málaga centre, on a club called "El Liceo". It was supposed to start at 22:00, but I couldn't arrive there before 23:45, due to Málaga taxis being really lousy. Antonio called for two cabs for us, but they never came. We then stopped one on the street near the residence and some of us went, and asked the driver to radio for one more. That one never came either! You can guess how I was really annoyed by the thing, so by the time I got to the club, I was really close to calling it quits and going back to the residence to get a good night's sleep.

I got to the residence at around 2:00 am and went straight to bed.