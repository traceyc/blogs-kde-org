---
title:   "Happy Birthday"
date:    2006-10-14
authors:
  - thiago
slug:    happy-birthday
---
It's been quite a while since I've blogged. You know, life gets in the way...

But this is a good time as any to say it: Happy Birthday KDE. May the next 10 years be more groundbreaking and memorable than the last 10.

Congratulations for everyone involved. We have come a long way since Matthias' original post on USENET.
<!--break-->