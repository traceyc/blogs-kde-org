---
title:   "Days -1 and 0 in Málaga: Humanisation of the Contributors"
date:    2005-08-27
authors:
  - thiago
slug:    days-1-and-0-málaga-humanisation-contributors
---
This is my first blog in Málaga and my intention is to write one each day, retelling that day's events. I hope to keep up with this for all the days I will be here. As you may or may not be aware, this is my first KDE event outside of Brazil. Until this Wednesday, I could count on one hand the number of KDE contributors I knew: Helio, Martin Konold, Andras Mantia, Josef Spillner and Gustavo Boiko.

Day 0 was actually my travel day. I left my home in São Paulo at 13:30 (-0300) for the airport and got on board at 16:40. After more than 11h inside the airplane, I arrived in Paris, where it was raining and a bit chilly. There I met with Helio, Volker Krause, Gaël Baudoin and Fred Emmott for another flight, this way to Málaga. We were welcomed by the shuttle people and driven to the residence for checking in. I have no major complaints about the flight (my baggage arrived safely, which is more luck than some people here had...), except that Air France could use some improvement on their Economy Class entertainment programme: hello!? only dubbed movies!?

Upon arriving at the residence, we met with danimo and eva and some others who were going to the beach. That's what when the "humanising" started: finally putting faces to the names long known. We then proceeded to registration, settling in in our room (which are, by the way, quite nice: two beds, kitchen, bathroom, living room, all for 16 € per night), a trip to the supermarket to buy water, etc. 

Then we gathered some people to go to the beach. You know how that goes: a group starts to form and then suddenly everyone joins in. I know we were 17 people, but I surely can't remember now who was there. We took the bus to the city centre and came back later on, after grabbing a pizza and starting a discussion or two --- according to Aaron, we're now getting more contributors wanting to do something than we can actually coordinate, so we have to work on that.

One of the most funny things is that every time we pass by the cable company's logo, someone stops and wants to take a picture. You will understand when you see one of them (note to self: download the photos from the camera). First time it was Seli who suddenly stopped in the middle of the street, looking downwards and exclaimed a certain F-word...

Day 1 was the e.V. meeting day. I took the opportunity to wake up late and to connect to the Internet and check some emails before the meeting. Unfortunately, there's no way I can read 1700 emails within 2h, so I left most unread. I hope to have more time for that later on.

The meeting itself was quite interesting. This was my first meeting as well, so I didn't know what to expect. It took longer than I expected, but shorter than what I was led to believe it would. People were mentioning ordering breakfast for the next morning when we left, but we actually ended up leaving the meeting room before 23:00. On our way back to the residence, we decided to stop for an icecream (helio, dfaure, dirk, Seli, clee, danimo, and I), so we arrived at 1 am on the residence.

So it's now Day 2 and the opening ceremony has already started. I'll write about today tomorrow.
