---
title:   "And D-BUS can control a KDE application already"
date:    2006-06-06
authors:
  - thiago
slug:    and-d-bus-can-control-kde-application-already
---
So I was working on kdelibs over the weekend, trying to clear up a few problems with the D-BUS implementation in KDE. I found that the KMainWindow interface worked almost perfectly out of the box (it would have worked perfectly if I hadn't used a dash [-] in the object path in the first place).

Here's what the object looks like: [image:2079 hspace=10 align=right class="show-on-planet" size=preview]

<pre>$ dbus local.kmainwindowtest /kmainwindowtest/MainWindow_1
method void local.kmainwindowtest.KMainWindow.appHelpActivated()
method void local.kmainwindowtest.KMainWindow.configureToolbars()
method void local.kmainwindowtest.KMainWindow.setCaption(QString caption)
method void local.kmainwindowtest.KMainWindow.setCaption(QString caption, bool modified)
method void local.kmainwindowtest.KMainWindow.setPlainCaption(QString caption)
method void local.kmainwindowtest.KMainWindow.setSettingsDirty()
method void local.kmainwindowtest.KMainWindow.slotStateChanged(QString newstate)
method void local.kmainwindowtest.KMainWindow.slotStateChanged(QString newstate, bool reverse)
property readwrite bool com.trolltech.Qt.QWidget.acceptDrops
property readwrite QString com.trolltech.Qt.QWidget.accessibleDescription
property readwrite QString com.trolltech.Qt.QWidget.accessibleName
property readwrite bool com.trolltech.Qt.QWidget.autoFillBackground
property readwrite bool com.trolltech.Qt.QWidget.enabled
property read bool com.trolltech.Qt.QWidget.focus
property read bool com.trolltech.Qt.QWidget.fullScreen
property read int com.trolltech.Qt.QWidget.height
property read bool com.trolltech.Qt.QWidget.isActiveWindow
property read bool com.trolltech.Qt.QWidget.maximized
property readwrite int com.trolltech.Qt.QWidget.maximumHeight
property readwrite int com.trolltech.Qt.QWidget.maximumWidth
property read bool com.trolltech.Qt.QWidget.minimized
property readwrite int com.trolltech.Qt.QWidget.minimumHeight
property readwrite int com.trolltech.Qt.QWidget.minimumWidth
property read bool com.trolltech.Qt.QWidget.modal
property readwrite bool com.trolltech.Qt.QWidget.mouseTracking
property readwrite QString com.trolltech.Qt.QWidget.statusTip
property readwrite QString com.trolltech.Qt.QWidget.toolTip
property readwrite bool com.trolltech.Qt.QWidget.updatesEnabled
property readwrite bool com.trolltech.Qt.QWidget.visible
property readwrite QString com.trolltech.Qt.QWidget.whatsThis
property read int com.trolltech.Qt.QWidget.width
property readwrite QString com.trolltech.Qt.QWidget.windowIconText
property readwrite bool com.trolltech.Qt.QWidget.windowModified
property readwrite double com.trolltech.Qt.QWidget.windowOpacity
property readwrite QString com.trolltech.Qt.QWidget.windowTitle
property read int com.trolltech.Qt.QWidget.x
property read int com.trolltech.Qt.QWidget.y
method bool com.trolltech.Qt.QWidget.close()
method void com.trolltech.Qt.QWidget.hide()
method void com.trolltech.Qt.QWidget.lower()
method void com.trolltech.Qt.QWidget.raise()
method void com.trolltech.Qt.QWidget.repaint()
method void com.trolltech.Qt.QWidget.setDisabled(bool)
method void com.trolltech.Qt.QWidget.setEnabled(bool)
method void com.trolltech.Qt.QWidget.setFocus()
method void com.trolltech.Qt.QWidget.setHidden(bool hidden)
method void com.trolltech.Qt.QWidget.setShown(bool shown)
method void com.trolltech.Qt.QWidget.setVisible(bool visible)
method void com.trolltech.Qt.QWidget.setWindowModified(bool)
method void com.trolltech.Qt.QWidget.show()
method void com.trolltech.Qt.QWidget.showFullScreen()
method void com.trolltech.Qt.QWidget.showMaximized()
method void com.trolltech.Qt.QWidget.showMinimized()
method void com.trolltech.Qt.QWidget.showNormal()
method void com.trolltech.Qt.QWidget.update()
method bool org.kde.KMainWindow.actionIsEnabled(QString action)
method QString org.kde.KMainWindow.actionToolTip(QString action)
method QStringList org.kde.KMainWindow.actions()
method bool org.kde.KMainWindow.activateAction(QString action)
method bool org.kde.KMainWindow.disableAction(QString action)
method bool org.kde.KMainWindow.enableAction(QString action)
method void org.kde.KMainWindow.grabWindowToClipBoard()
method qlonglong org.kde.KMainWindow.winId()
method QString org.freedesktop.DBus.Introspectable.Introspect()
method QVariant org.freedesktop.DBus.Properties.Get(QString interface_name, QString property_name)
method void org.freedesktop.DBus.Properties.Set(QString interface_name, QString property_name, QVariant value)</pre>

The <tt>local.kmainwindowtest.KMainWindow</tt> interface is a side-effect I'll have to correct eventually: I wanted to get all the QWidget slots public, but that also made the KMainWindow slots public as well.

With that object, you can do nifty things like raising, lowering, minimising, restoring, hiding or showing a window, setting it to full-screen and normal mode or even copy it to the clipboard. Unfortunately, you can't move windows yet, because the <a href="http://doc.trolltech.com/4.1/qwidget.html#geometry-prop">geometry</a> and <a href="http://doc.trolltech.com/4.1/qwidget.html#pos-prop">pos</a> properties take QRect and QPoint (respectively), which aren't supported in D-BUS for the moment.

This is all cross-framework as well! 

Here's what the /MainApplication object currently looks like (it's the KApplication object):
<pre>$ dbus local.kmainwindowtest /MainApplication
method void org.kde.KApplication.quit()
method void org.kde.KApplication.reparseConfiguration()
method void org.kde.KApplication.updateUserTimestamp()
method void org.kde.KApplication.updateUserTimestamp(int time)
property readwrite int com.trolltech.Qt.QApplication.cursorFlashTime
property readwrite int com.trolltech.Qt.QApplication.doubleClickInterval
property readwrite int com.trolltech.Qt.QApplication.keyboardInputInterval
property readwrite bool com.trolltech.Qt.QApplication.quitOnLastWindowClosed
property readwrite int com.trolltech.Qt.QApplication.startDragDistance
property readwrite int com.trolltech.Qt.QApplication.startDragTime
property readwrite int com.trolltech.Qt.QApplication.wheelScrollLines
property readwrite QString com.trolltech.Qt.QCoreApplication.applicationName
property readwrite QString com.trolltech.Qt.QCoreApplication.organizationDomain
property readwrite QString com.trolltech.Qt.QCoreApplication.organizationName
method QString org.freedesktop.DBus.Introspectable.Introspect()
method QVariant org.freedesktop.DBus.Properties.Get(QString interface_name, QString property_name)
method void org.freedesktop.DBus.Properties.Set(QString interface_name, QString property_name, QVariant value)</pre>

One interesting thing you may note is that, if your application is a KUniqueApplication, you automatically get a org.kde.KUniqueApplication.newInstance method.

And when you're done with your test, you can just quit it:
<pre>$ dbus local.kmainwindowtest /MainApplication quit

[1]+  Done                    ./kmainwindowtest
$</pre>
<!--break-->
