---
title:   "aKademy Day 4: free wifi"
date:    2005-08-30
authors:
  - thiago
slug:    akademy-day-4-free-wifi
---
Day 4 in aKademy was the first day of the developers' conference. It started with the <a href="http://wiki.kde.org/tiki-index.php?page=Trolltech%20Keynote%20Talk">keynote speech by the Trolltech CEO</a>, Eirik Chambe-Eng, entitled "Trolltech, KDE and the number 42", though somehow the number 42 went missing...

It was a very interesting presentation, giving a bit of insight on Trolltech's current and future situation. The company is a prime example of Open Source contributor and one of the few that has a working business model. Besides, it's a win-win situation for everyone, just like it was said in the talk: KDE gets an awesome toolkit to build upon, plus paid developer time and Trolltech gets showcasing, publicity and testing for Qt.

What was most interesting, though, was the fact that they plan on hiring around 30 people between now and the end of the year.

I also watched the asynchronous programming talk by David Faure and Till Adam, and the multithreading talk by Mirko Böhm. While his ThreadWeaver class is still located in kdenonbeta, I plan on having it moved to libkdecore real soon: I do the exact same thing in my networking classes in libkdecorenetwork and it would certainly be best if we didn't duplicate code for that functionality.

I thought I had missed Zack's talk when I got back, but fortunately (for me), it was transferred to Wednesday.

I spent the rest of the day in the computer room, trying to catch up on email. Then we went for dinner: what I had planned on being a quiet evening, with just a sandwich from the nearby Carrefour turned into another night in town. Danimo, Aaron, Ivor, Chris, Allan (carewolf), Martijn, Marcel, and I went to have dinner at a place called Taberna del Obispo, where we ate dinner and managed to find a hotspot in order to <a href="http://blogs.kde.org/node/1387">blog</a>. We were 8 people from 8 different countries. What's more, our waitress not only knew about Linux, but was Polish too (so, ninth nationality).

And just when I thought we were heading back, Allan announces it was his birthday so he wanted to buy everyone a beer. So there we went again to another place. In fact, we drank every last bottle of Alhambra beer they had... (ok, granted, there were only 5 bottles left when we arrived).

Finally, upon arriving at the residence, there were people discussing something (as always), so I joined and ended up going to bed at 2 am --- but not before calling my parents and spending 10 minutes on international call on international roaming (so, around 30 € for one call).
