---
title:   "KDE Docs"
date:    2005-06-30
authors:
  - thiago
slug:    kde-docs
---
This is just a short note to congratulate <a href="http://physos.org/2005/06/27/docskdeorg-01/">physos</a> and everyone involved for the great new look <a href="http://docs.kde.org">docs.kde.org</a> now has. Even after all these years, KDE contributors continue to amaze me in terms of the quality of the material they produce.

Congratulations and keep up the good work!
<!--break-->