---
title:   "Opportunity for new developer to work in kdelibs"
date:    2006-11-14
authors:
  - thiago
slug:    opportunity-new-developer-work-kdelibs
---
Yeah, the Title of this blog sounds like a job opportunity... but that's intentional. I am proposing a job opportunity -- in the FOSS style :-)

When discussing with Aaron Seigo tonight about some old functions in KApplication that we wanted to move or remove from KDE4, we came up with the conclusion that we instead needed a new class. We've called it KAutoSaveFile and its purpose is to allow application writers to easily:

<ul>
<li>Write unsaved data to a temporary file, in case the application crashes or the power goes out or something drastic like that.
<li>Determine if there is such unsaved data left behind and offer the user to recover it.
</ul>

Fairly simple, eh? Well, we'd like to have someone to do that for us. Someone fairly motivated by open source and KDE, with good knowledge of C++, preferrably someone who isn't already a KDE core developer (knowledge of the KDE library policies is certainly welcome). I'll be coaching you and helping you along the way with my own ideas about this. Think of this like a very short Summer of Code or Season of KDE. But without any money.

And to kickstart this little adventure, we've already prepared the <a href="http://websvn.kde.org/trunk/KDE/kdelibs/kdecore/kautosavefile.h?view=markup">header file</a> for the class! All you have to do is write the .cpp file for it.

If you're interested, just let me know.
<!--break-->