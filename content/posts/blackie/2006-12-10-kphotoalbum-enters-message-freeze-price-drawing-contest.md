---
title:   "KPhotoAlbum enters message freeze + price drawing contest"
date:    2006-12-10
authors:
  - blackie
slug:    kphotoalbum-enters-message-freeze-price-drawing-contest
---
<p>Today KPhotoAlbum entered message freeze, and is now ready to be
translated. The release will happen Dec 31th 2006.</p>

<p>To bring a bit of attention to the great work the translators do, I've
started a small prize drawing contest which those who have made a complete
translation of KPA participating. The prize is $100.</p>

<p>See http://www.kphotoalbum.org/news.htm#item0043 for details.</p>
<!--break-->