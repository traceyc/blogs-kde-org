---
title:   "A video is worth a thousand screen shots"
date:    2005-09-24
authors:
  - blackie
slug:    video-worth-thousand-screen-shots
---
Every time a new KimDaBa user shows up, I hear the same sentence over and
over again: <i>This is exactly the software I need, I just wish I had found
it a long time ago</i>.

Now if KimDaBa is that good, why isn't everybody in the whole world using
it? One reason might be its name, and I am considering renaming it to
something boring but more obvious. 

Another reason might be that it is different to many other photo managing
applications in some ways. This is not a bug, but simply because it
approaches the problem differently, which is it's strength.

To remedy the problem with the initial learning curve, I've recorded a
number of introduction flash videos, which includes voice to explain what
the user sees on the screen.

Have a look at http://ktown.kde.org/kimdaba/videos/, who know, you might be
my next happy user.
