---
title:   "Finally Weekend!"
date:    2006-08-12
authors:
  - blackie
slug:    finally-weekend
---
<p>Finally Weekend! Not much happening at work, actually that little that
when getting home in the evening I'm all tired. But now it is weekend, time to loong day of KDE development :-)</p>

<p>I started the weekend rather nasty by eating too much Nachos, and now I'm too
full. Next I guess I will be coding for a few hours, then it is off to the
movies to watch <t>World Trade Center</t>. I'm a bit in conflict about it, but
after all it is kind of common history. I wont eat popcorn in the cinema though!
First because it wouldn't feel right for such a movie, and second because I
still try to loose some weight (no the image associated with my blogs is
not how I look today :-o )</p>

<p>I also started the coding weekend out by a suboptimal implementation of
a clone method, check it out:</p>

<pre>
    CategoryItem* clone() const
    {
        CategoryItem* result = new CategoryItem( _name );
        for( QValueList<CategoryItem*>::ConstIterator it = _subcategories.begin(); it != _subcategories.end(); ++it ) {
            _subcategories.append( (*it)->clone() );
        }
        return result;
    }
</pre>

<p>Got it? Well the problem is that I don't append children to the
<i>result</i> but to the object i try to clone. Darn it took my computer 15
minutes to coma out of that come after it started eating a lot of
memory.</p>

<p>Finally, a message to, my perhaps only reader, Steve: Tell Jenny that she
won't stand a chance tomorrow in badminton :-)</p>

<p>Happy Weekend, everyone!</p>
<!--break-->