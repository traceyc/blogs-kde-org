---
title:   "Artists Attention please, join the KPhotoAlbum splash screen contest!"
date:    2006-08-24
authors:
  - blackie
slug:    artists-attention-please-join-kphotoalbum-splash-screen-contest
---
<p>Today Christoph Moseler managed to push me over the edge. Several times people have complained that the KPhotoAlbum splash screen was a combination of ugly, ugly and non relating to KPhotoAlbum. So far I've managed to tell people to go and take a hike, but today I gave up :-)</p>

<p>Therefore, I started a <a href="http://www.kphotoalbum.org/splashscreen.htm">contest to make a new splash screen for KPhotoAlbum</a>. I hope lots of talented artist will participate.</p>

<!--break-->