---
title:   "KPhotoAlbum preview with support for Android"
date:    2014-05-14
authors:
  - blackie
slug:    kphotoalbum-preview-support-android
---
OK, let me admit, it has been a while since I put serious amount of work into KPhotoAlbum. Life has come in the way you see :-)

However, it was life (this time my daughter now almost 3 years old) that forced me back in the game. I still don't trust her with my laptop, so a tablet version of KPhotoAlbum was needed with which she could watch all our images.

The result is a KPhotoAlbum client for Android. The client serves images from a running version of KPhotoAllbum on the desktop. I just released the first preview of the work yesterday, along with a <a href="http://youtu.be/-vty4ZCiLpQ">video showing it in action</a>. See this <a href="http://kphotoalbum.org/index.php?page=news#item0083">blog for details on how to get a copy of it running on your computer.</a>
