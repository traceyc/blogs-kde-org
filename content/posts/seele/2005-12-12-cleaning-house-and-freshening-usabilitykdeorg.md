---
title:   "Cleaning House and Freshening Up @ Usability.KDE.org"
date:    2005-12-12
authors:
  - seele
slug:    cleaning-house-and-freshening-usabilitykdeorg
---
i spent a part of this weekend working on the <a href="http://usability.kde.org">kde usability</a> website. unfortunatly it doesnt get updated very often, and considering we have dated 'recent news' on our home page.. the content looks quite dated.

anyway, some of the things i am working on is to improve the site as a public portal for people interested in the project, developers looking for usability information,and press looking for information about the kde usability project.  there arnt many examples of our work on the site, and finding informatino about the work we *have* done is a bit of a chore.  most of it is hosted on <a href="http://openusability.org">openusability</a>, but there are many reports and tests which have been done outside of the core crew.

having this collection of recent work will act as a good reference for developers, provide a public image for kde usability, give other usabiltiy engineers a taste of what kind of work we do, and be a good place for current usability engineers to publish their work and get feedback.  there has been a ton of work done in the past year, so lets show it to the world.

although there will be some restructuring, unless you compare the two site you wont notice much of a cosmetic change.  im slowly swapping out content so there arnt any holes or broken links, so you might not notice a difference at all until you look for old or new information.  ive surfaced some important information pages from the hierarchy to make them more visible and accessible from the top level and added some usability resources, articles, and links.  the most of the content and work will be with the usability reports of which i havnt started.

last week i put out a call for usability reports to create a forum for our work.  el was wonderful enough to send me some of the work shes done which isnt strictly on openusability.  even if its a link to an external report or website or blog entry, pass it on to the mailing list! [kde-usability at kde dot org].  

lastly, i would like to put some bio information on the page to those willing to participate.  almost every kde project has information about its developers, and for the usabiltiy project this is important!  working with developers requires creating a relationship to get to know the developers and project better, and a good way to start is introducing yourself on the website.  if you would like to be listed on usability.kde.org and have contributed to kde usability in the past year, please forward your information to the mailing list [kde-usability at kde dot org].
<!--break-->