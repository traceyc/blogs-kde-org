---
title:   "Visiting DC"
date:    2006-04-05
authors:
  - seele
slug:    visiting-dc
---
<p>This and next week will be busier than average.  Tonight I met up with pmax (who was in town for biz) with <a href="http://www.neomantra.org">Justin</a> to drink some beers and play some darts at a local bar.  Its too bad pmax wont be in town long enough to visit downtown, the cherry blossoms are in full bloom (last night's storm had spared them for yet another week) and its always nice to go site-seeing in the nation's capital.</p>

<p>I've been running back and forth between client offices and I have a long day in Baltimore tomorrow.  The day will be even longer in York, PA on Thursday as I complete on-site user surveys.  Travel doesnt normally bother me (unless it is via plane), however I feel like a chicken with my head cut off (how many of you understand this expression?) this past week.</p>

<p>Later this week El and Jan will be in for the weekend to do some usability geeking before we head to Atlanta for an OSDL meeting.  A friend from Boston will also be in Thursday for the weekend, so it will be quite a time.  After the short trip to Atlanta things should slow down for a bit for a well needed breather.  Perhaps I will be able to go back to blogging on a regular basis then :)</p>

<!--break-->