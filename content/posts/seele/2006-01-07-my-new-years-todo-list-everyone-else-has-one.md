---
title:   "My New Year's TODO List -- Everyone else has one"
date:    2006-01-07
authors:
  - seele
slug:    my-new-years-todo-list-everyone-else-has-one
---
Oh hell.. why not. Maybe if I say I'm going to do something on public forum, I'll actually get it done :)

Of course, I shouldn't give myself the <i>entire year</i> to do some of these things, so maybe I should set an expiration on each item.

<b>Put up the KDE Usability Project tests and reports I gathered in December</b>

I think December was a month off for everyone.  I did get a little work done here and there, but one thing I started and didn't get a chance to finish was reorganization of <a href="http://usability.kde.org/">usability.kde.org</a>. Many of you provided me documents or links to usability tests and reports from 2005, I just need to put them up.

Let's try to do this (or at least get started putting documents up) by the end of January

<b>Start organizing and writing conference submissions <i>now</i></b>

Alas, I think there were 6 conferences I wanted to write submissions for and I only got to two of them.  Part of the problem was lack of time, and lack of time yielded the problem of lack of material composed, and lack of material composed yielded the problem of lack of submission.  I need to plan and manage my time better.

Start scanning deadlines and planning reports now.  Annual conferences are typically held around the same time of the year with the same deadlines, so I should know most of them by now :P

<b>Start gathering and organizing tests, references, notes, etc. for my thesis</b>

Although my thesis is probably another year away, I seriously doubt three or four months will be enough time to test and analyze data, collect and evaluate references, and write a 50-100+ page graduate thesis.  I'm in the data everyday anyway, so I might as well start taking notes.

This doesn't have a solid expiration, but I should start as soon as tomorrow with bookmarking possible references and thinking about user test design.

<b>Get back in to KDE-Edu</b>

The <a href="http://edu.kde.org">Edutainment Project</a> was one of the first projects I got involved with when I joined the KDE development community.  Sporadically over the past year or so I provided them with some feedback about their interfaces and a few reports.  They are a wonderful group to work with and very welcome to feedback which is going to help improve children's success and acceptance of the software.

Ideally, I would like to have another interface and contextual evaluation done by the end of March.

<b>...and then there's the HIG</b>

Poor El, I think she's done the most work on the HIG without much help (from me included).  I wish I had a solution for the lack of participation.  I thought maybe putting it on a wiki and making it more accessible for people to edit would help -- I think I edited it for about a month before I stopped.  We really need to get a move on the thing, at least get a draft out the door by the end of the year.

We need to make some serious progress in the next 6 months.  In particular I will take responsibility for the Toolbars, Menus, and Labeling sections, and try to have at least debatable information by the end of March with a preliminary draft by the end of June.

Overall, I think this is a pretty ambitious list of things to get done this year (mostly in the first half).  Cheers to motivation!

<!--break-->