---
title:   "Usability survey participation needed!"
date:    2005-09-12
authors:
  - seele
slug:    usability-survey-participation-needed
---
Some of you may have first heard of this at aKademy.  Jan and I are conducting a simple survey of developers in order to get a better idea of your development style and how we can make the Human Interface Guidelines (HIG) better and more attractive for you to use.

The survey is only a few questions and will only take about ten minutes of your time.  Jan and I can be reached on IRC in #kde-usability or #openusability (janushead and seele respectively).  Once we collect enough surveys we will compile our results and give you our report.

Thank you for your participation!