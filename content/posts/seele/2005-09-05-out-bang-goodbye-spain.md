---
title:   "Out with a BANG!  Goodbye Spain"
date:    2005-09-05
authors:
  - seele
slug:    out-bang-goodbye-spain
---
I have to say that my first aKademy, first time travelling abroad, first time in Europe, first time in Spain, first time meeting so many absoultely cool people was amazing.

Saturday to Sunday in particular were a bit insane and a whole lot of fun.

Late Friday night (early Saturday morning), some of us headed to town to see what was going on.  We came across this very popular square with a TON of people.  This was the *it* place if you were 20-30 and looking for a good time.  Atmosphere included a cabanna bar with every beach drink known to many, three-to-one female-to-male ratio, an obelisk covered in lizards, half nekked girls, and lots of drunk people (who all thought the car behind us was the toilette).  Over time, the square collected more and more drunk people until you couldnt see from one end to the other.  I also got the most interesting drink ever: the bar tender in the cabana cracked open a real-live coconut (this is especially interesting to me because I love coconut and they are very expensive in the States) and made a huge drink and poured it in it.  Talk about presentation.

The next morning was spent recovering and saying goodbye.  Early in the evening they kicked us out of the labs so we could prepare for the beach party.  No one could prepare me for that.  Combine one-part geek with an equal-part of alcohol, mix well in the Mediterranean and you get a crazy party with nekked, drunk, geeks running around in the sand.  The entertainment factor was high, but I had to say *NO* to swimming with them many, many times ;P

Ah, but the watch is ticking.  In less than 4 hours I have to be on a plane back to the States.  Its not long enough time to sleep, so a few of us head back to Malaga's center and party at a club like its 1999 (ok, so its 2005..).  We are having an absolute great time when I happen to look at my watch.  Uh oh, its after 0530 in the morning which only leaves an hour and half for us to gather up, go back to the Residence and for me to go through security and customs to make my flight.  Quickly we gather up, and we make a quick stop to the Residence before heading to the airport.  Checking in took quite a bit of time, and I managed to make my flight with five minutes to spare.  

That is the way you should leave Europe, using every minute available in the meantime.

More about my trouble with flying during my trip is documented on my <a href="http://weblog.obso1337.org/?p=274">personal blog</a>.  Summary: you need more than an hour scheduled to transfer through CDG :P

Overall I had a wonderful time with everyone, Spain is beautiful, and I cant wait until I can meet up with all of "yinz" again!