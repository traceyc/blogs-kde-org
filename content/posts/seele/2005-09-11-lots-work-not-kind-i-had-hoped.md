---
title:   "Lots of Work (but not the kind I had hoped)"
date:    2005-09-11
authors:
  - seele
slug:    lots-work-not-kind-i-had-hoped
---
I managed to get a lot of work done today, but not the kind I was aiming for.  Things are still insane here at my job in DC so instead of the KDE Usability work I was planning on getting done (sorry Jan, maybe Monday) I get to deal with more crap updates and fight the battles which were usually reserved for the middle of the week (FYI: Just because it VALIDATES for &sect;508 Accessibility  it does <strong>not</strong> mean it is accessible!).

People think that just because our Undersecretary was kicked out of Louisiana, they can make content updates which are inaccessible, unusable, and unreadable.  You would think they would learn from our little <a href="http://politics.slashdot.org/politics/05/09/08/1212237.shtml">Slashdot party</a> from the other day.  

Thank you Coolo for all the jokes, <a href="http://cgi.ebay.com/FUNNY-FEMA-L-black-t-shirt-novelty-hurricane-punk_W0QQitemZ8333603375">here is a bit more humor for the day</a>.  I will spend the last few hours of my shift listening to a movie (actually its a wonderful concept album by Vangelis) and continuing my vigilance over goverment content.  If only I really made a difference :(