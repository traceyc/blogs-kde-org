---
title:   "Closing the Information Gap of Our Users"
date:    2005-11-15
authors:
  - seele
slug:    closing-information-gap-our-users
---
Lack of information about the user is one of the largest hurdles towards OSS usability.  User research hasnt been a typical part of the KDE development process, software is available for download anonymously at no cost, and not many marketing or demographic studies are done. 

This effects research and development of a product:  1) You dont know your user, so its difficult to develop goals for the system. 2) There is no product or feature yet, so you cant user test it. 3) Valuable information about the user and their habits, preferences, satisfaction, ability, etc. are unknown which make future development and innovation difficult.  Sure we have marketing goals of a user of A-type of B-age in C-industry who typically performs D and E on their computer, but we dont have any detailed information about those users.  Infact we dont have any detailed information about our current users.

<b>Call for Code</b>

Sometimes the best way to gather information from the user is to make it as easy as possible for them to participate.  

For example: the KMenu is high on the list of things-we-need-to-fix-but-dont-know-how.  At aKademy I answered the why-it-doesnt-work, but could only minimally answer how-to-fix-it.  User information about usage of shorcut menus like the KMenu is limited.  You can do usability tests and surveys until youre blue in the face, you cant gather long-term user behaviour patterns based on recording a few tasks and asking a few questions.

. o (What would be super-fantastic is if someone could write a patch for kde-libs to keep track of how a user launches an application (via CLI, KMenu, Kicker/Desktop shortcut) and how often.  I'll put this on my wishlist.)

The KMenu isnt the only part of KDE which could benefit from some custom code to gather information.  Infact, kde-libs might not be involved in all solutions.  It might be as simple as creating a small application to better facilitate user surveys, or collect user test data.  Anything that would help facilitate the gathering and research of information about our users.  There are virtually no OSS usability or accessibility tools available, and industry software is *very* expensive.

<b>Volunteer!</b>

The problem is most usability specialists are not developers (If we were, you would probably see more usability bugs get fixed sooner).  We are at your mercy when we submit usability issues or fixes or requests for adding or removing features.

So if any of you great contributers out there are looking to help participate in KDE usability (or KDE HCI for that matter), get in touch with us!  We desperatly need user information to use at the *beginning* of the development cycle.  It will save blood and sweat on user testing, usability reports, and bug fixes.  What youll get is user-happiness++ because of a more user-centered design process from start to finish.