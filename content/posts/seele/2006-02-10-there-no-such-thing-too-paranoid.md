---
title:   "There Is No Such Thing As \"Too Paranoid\""
date:    2006-02-10
authors:
  - seele
slug:    there-no-such-thing-too-paranoid
---
According to a <a href="http://news.com.com/2100-1030_3-6037598.html">recent C-Net Police Blotter</a>, email surveillance without any evidence of criminal behavior has been approved. The new law only allows monitoring of email headers (hence justifies as 'constitutional'), but the fact that they can freely monitor and log email traffic is alarming.

Also, our president seems to think <a href="http://www.nytimes.com/cfr/international/slot2_020506.html">its ok to monitor phone telecommunications</a> in the name of 'fighting the war on terror'.  My question is how do they get through all the datanoise to find these 'terrists' without violating regular citizen's privacy?

Remember Cindy Sheehan at the <a href="http://www.chron.com/disp/story.mpl/editorial/outlook/3646018.html">state of the union address</a>? Now it seems as if protesting is an act of terrorism.  

Where was that logic on the anniversary of Roe v. Wade a few weeks ago when we had thousands of pro-life protestors waving full-colored posters of dead fetuses in D.C?  They'll blur out a boob on T.V. but its ok to wave pictures of dead, mangled corpses in public.  If I were protesting rape and incest of children, would I be able to carry a poster of an dead, 11-year-old boy who was sexually molested?  I'm not suggesting it is in good taste (I certainly wouldn't do this, but follow my logic here). Your first argument would be that dead boy is someone's child.  According to pro-life advocates so is that fetus, yet that hasn't stopped them.

Free country to do what now?

Sometimes this place is ass-backwards.  You can sue someone for almost anything in this country.  The freedom of speech only applies if its pro-government or else youre a terrorist.  If you dont support the war you dont support the troops.  A computer is a tool only to steal media, look at porn, or hack in to another computer.  You can murder someone on T.V. but 'suggestive' sexual acts get censored.  

We're a nation of prudes in love with war. We are with war with cancer, sex, drugs, terrorism, obesity, iraq, nature, and our neighbors.  Bush <a href="http://www.washingtonpost.com/wp-dyn/content/article/2006/02/09/AR2006020900892.html">claims a terrorist plot on LA was thwarted</a> yet there is no way of proving this.  This constant manipulation by the government is making me think twice about our recent history.  What if we really didnt go to the moon in 1969?  I wouldnt be suprised.

What's next?  A tinfoil hat.

<!--break-->