---
title:   "Weekend work.."
date:    2005-10-02
authors:
  - seele
slug:    weekend-work
---
The weekend thusfar has turned out to be pretty productive.  Sunshine, KMenu work, SVN access.. I got a lot done, and I still have a few more waking hours!  This has been the first weekend in a few months I havnt had to travel or work, so I am enjoying every minute of it (and spending it WORKING anyway :)

Earlier today I went frisbee [disc] golfing with some peeps and got a much needed dosage of sun.  Fall has finally arrived (to my dismay later than in Pittsburgh) so the weather was wonderful to be outside in.

<a href="http://blogs.kde.org/node/1506"><img src="http://gallery.lebwog.com/seele/albums/Miscellaneous/kmenu_small_001.thumb.jpg" align="right" width="150" alt="some notes on the KMenu" border="0" /></a> I've been struggling to find time to work on the future of the KMenu, I havnt been able to get much done since preparing for my BoF at aKademy.  I spent some time translating notes from my messy chickenscratch to something more readable so I can reference them in the future without much trouble.  The interaction concepts I came up with are becoming more and more difficult to translate in to sketches as I dive deeper in to more detailed interfaces.  

My goal is to have enough specification and direction to do low-fi prototype testing to test my interaction theories, but its difficult to define "soon" when youre worried about hurricanes hitting land from weekend to weekend. 

I thought this would finally be my first work-free weekend since the beginning of the season; I spoke too soon.  I should learn to turn my mobile off on the weekends.

Hopefully tonight I will get some much needed updating to the <a href="http://usability.kde.org/">KDE Usability Website</a>.  I havn't touched it since last April.  I managed to get my SVN password reset (thanks coolo) so maybe it will stay more current than how I was keeping it in the past year.