---
title:   "define: Featuritis"
date:    2005-10-18
authors:
  - seele
slug:    define-featuritis
---
Featuritis is a condition in which the functionality of an application increases with each revision (and such, complexity increases).  The functionality that aggravates this condition is usually that which a single user (or group) requests (or is given) without consideration of the implications it has on the rest of the users, interface, and experience the application is meant to address.  

Don Norman goes in to great length about this in <i>The Design of Everyday Things</i>: "Complexity probably increases as the square of the features: double the number of features, quadruple the complexity. Provide ten times as many features, multiply the complexity by one hundred."

Adding functionality and features to an application are not bad things, if the benefits are kept in balance with the necessary learning, memorability, and ease-of-use required of the application.  These additions become negative when the features obstruct unrelated tasks (cluttered or confusing interface), are not worth the training time (a lot of learning for a simple or infrequently used task), cost more to develop than will return (ROI), and etc.

As <a href="http://headrush.typepad.com/creating_passionate_users/">Creating Passionate Users</a> describes it: 

<img src="http://headrush.typepad.com/photos/uncategorized/featuritis.jpg" width="440" alt="Featuritis curve" />

This is where good usability engineering comes in to save the day.  Good user profiles, requirements analysis, usability and ROI goals can help identify a good feature from a bad feature.  Some features may be harder to evaluate and may require additional user profiling or usability testing to see if it is really something users want.  Ideally, features which may cause featuritis can be identified early on, and eliminated or evolved to provide a better experience with the application.

The moral of the story?  Just because you can, doesn't mean you should.  Bigger isnt always better if you can't use the tool. And sometimes you may just need some TLC from your friendly usability geek.