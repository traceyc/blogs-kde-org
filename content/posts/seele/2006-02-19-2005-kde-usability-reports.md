---
title:   "2005 KDE Usability Reports"
date:    2006-02-19
authors:
  - seele
slug:    2005-kde-usability-reports
---
Although <a href="http://blogs.kde.org/node/1733">earlier this year</a> I had hoped to get the reports up by the end of January, that they got up by mid-February isn't too bad.  The 2005 reports page is <a href="http://usability.kde.org/activity/reports/2005reports.php">here</a>.

Reports were added for: KBruch, KMail Recipients, Koffice Startup, Kopete, KOrganizer, KPrinter.

There have been more reports than this, particularly through <a href="http://openusability.org/softwaremap/trove_list.php?tc_id_parent=55&tc_id=57">OpenUsability.org</a>.  These were just the ones which were specifically sent to me to get posted to the site. Also, much of our contribution is through talking and working with the developers and not necessarily preparing web-ready deliverables.

I also cleaned up some of the links to the XML contribution style we dont really use (OpenUsability.org however does, and reports can be submitted through them).  I also removed some of the links to the older reports while I clean up the structure, and I will eventually add them to the archives.

BTW, I also have a Jabber account on kdetalk.net (celeste at kde dot org).  I have never used Jabber before, so I'm still figuring out what this is about.

<!--break-->