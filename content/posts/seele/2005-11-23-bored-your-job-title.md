---
title:   "Bored With Your Job Title?"
date:    2005-11-23
authors:
  - seele
slug:    bored-your-job-title
---
<a href="http://www.girlwonder.com/jobs.html">Redefine yourself</a> with this interaction architect job title generator.

My favorite: Information Deity

Runner up: Digital philosopher