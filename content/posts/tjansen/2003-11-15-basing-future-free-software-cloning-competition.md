---
title:   "Basing the future of free software on cloning the competition"
date:    2003-11-15
authors:
  - tjansen
slug:    basing-future-free-software-cloning-competition
---
OSNews has <a href="http://www.osnews.com/story.php?news_id=5137">a poll and discussion</a> about integrating <a href="http://www.go-mono.com">Mono</a> into Gnome. As KDE may face a similar decision at some point in the future - what to do when KDE's technology is not competitive anymore - I thought i write my thoughts down in my blog instead of the OSNews forum.
<br><br>
In the last weeks I have spent a lot of time on taking a close look on <a href="http://msdn.microsoft.com/nhp/default.asp?contentid=28000519">.Net</a> and <a href="http://msdn.microsoft.com/longhorn/">Longhorn</a>, and I think it is quite nice. Certainly not perfect, and there are many things that I would have done differently, but it seems to contain less brain damage than Java. They certainly took a good look at Java and fixed its problems and shortcoming while designing C# and the .Net APIs.
Assuming that the Mono guys can complete the .Net clone and its GTK integration and get it stable,  Mono+Gnome would be a development platform that's much more powerful than the current Gnome, and for applications that can make use of the .Net platform's advanced features (like their XML, SQL and Remoting support) also better than today's KDE. Mono will be also able to make use of libraries, both free and propritary, that have been written for Microsoft's platform. Developers who are used to .Net will feel home on Gnome. And books written for .Net will also help Gnome developers.<br>
I still think that it is a bad idea. <!--break-->Why? A cloned framework puts innovation in Microsoft's hand. You still have the chance to write better applications, but the innovation of the platform will always trail behind. What advantage does free software have when its innovation is limited to that of its competition?
<br><br>
Sure, cloning gives developers a common goal, similar to Linux's original goal of cloning a Unix-kernel. And Linux has been very successful in replacing Unix. But the .Net situation is different. Unlike Unix, Windows and .Net are under active development. Microsoft recently announced massive improvements to .Net, and it is likely that this will continue. Linux has two advantages over Unix: it is free and it is better. Mono will just be a free clone that trails at least a year behind. And that will also be the public perception, Mono will look like a cheap imitation and not the real thing. Even if the original may be so good that an imitation is still nice, it's like saying "Hey, we aren't capable to produce something better than Microsoft, so just clone their work". For some people this may be fine, but it is certainly not a plan for world domination.<br>
Mono could, of course, try to add additional APIs to .Net. GTK# is an example for this, since it is a replacement for Winforms and certainly better than the original in many ways. But as these new components are also free software (assuming they are under LGPL or BSD, like Mono) all improvements to Mono will also be available for the proprietary .Net. So by adding new components you can't make Mono better than .Net, you will always improve both.
<br><br>
Maybe it comes down to the question: why are you working on free software? If you only do it because hacking some code is fun, the Mono solution is ok for you. If you do it because you want to create a free alternative to proprietary software, Mono is a fine solution as well. But I do it because I want to create better software. Hopefully so much better that in the future the world's information flow is not solely controlled by companies whose main interest is increasing shareholder value. Being free is an important aspect for software, but being technically better is not less important for archieving this goal. And Mono is no solution for that.



