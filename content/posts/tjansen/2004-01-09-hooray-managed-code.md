---
title:   "Hooray for managed code"
date:    2004-01-09
authors:
  - tjansen
slug:    hooray-managed-code
---
For a long time I have hoped that managed code will beat statically compiled code one day. Managed code can make software more secure, CPU-architecture-independent and makes it easier to generate executable code. The only remaining problem is the performance. Theoretically managed code should be faster than native code that the linker does not understand, because of the better optimization opportunities. But in practice and especially in public perception it always trailed behind. <!--break-->
The <a href="http://www.osnews.com/story.php?news_id=5602&page=3">benchmarks published by OSNews</a> suggest that, at least in the commercial space, managed code is already competitive and can beat unmanaged C code. The nice thing about the OSN article is that it's probably the first time that someone was so (stupid|brave) to violate Microsoft's licensing agreement and publish benchmarks for C# - but even Java did very well against gcc. 

Unfortunately the usefulness of the benchmarks is very limited. They do not measure important things like the quality of inlining, virtual method invocation (or more important for virtual methods: how good the VM is at inlining them at runtime) and the cost for object allocation. And it ignores language-specific features that cost performance. A good example is Java's immutable String class which causes object allocations for all modifing string operations. But it still gives hope for a better future. Congratulations, managed code :)
