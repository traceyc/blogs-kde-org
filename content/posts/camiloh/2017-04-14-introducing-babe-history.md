---
title:   "Introducing Babe - History"
date:    2017-04-14
authors:
  - camiloh
slug:    introducing-babe-history
---
https://babe.kde.org/

<img src="https://blogs.kde.org/sites/blogs.kde.org/files/playlist_mode_friend.png">


This is my very first post for KDE blogs and it is also my very first application. So when I sit down to think about what to write about I thought I would like to tell you all about how and why I wanted to start coding and then why I decided to create a  (yet another (i know)) music player, specially made for KDE/Plasma.

So here comes the story:

I've been using Linux/GNU for almost ten years now, that was when I was still in high school, I always thought Linux based distros looked so cool and kind of mysterious, so then I decided to wipe off my Windows XP installation and move to Ubuntu, since then I have not looked back and I'm glad because not only i found a great OS but also a great group of communities behind it. I first got involved with the community by making GTK/CSS themes and small icon sets.

<img src="http://img00.deviantart.net/f7c9/i/2014/195/f/0/classic_gtk_by_kxmylo-d7qoltu.png">


 Let's say I always found the visual part the most interesting, so I tried all the available desktop environments, visual appealing applications and themes. Among those apps, I always liked to check out the default music players of each distro and their set of multimedia applications. 
I can say I've pretty much tested almost all of the Linux music players that have appeared in the wild. Some looked cool, others boring, and they  worked... some others were buggy as hell... and many others were a very nice and complete tool to manage your local music collection but didn't look that great or well integrated.

Anyway, I finished high school and then went to University to the Arts program. Two years ago I also started the Computer Science bachelor program and then began to start developing small console apps. By then I was using elementary OS, because it looked nice and polish, and then was went I first wanted to create my very own music player to satisfied my own needs and also to learn a new graphic toolkit (GTK3)

<img src="https://raw.githubusercontent.com/milohr/babe-music-player/master/Screenshot%20from%202016-05-01%2020%3A07%3A41.png">
<img src="https://raw.githubusercontent.com/milohr/babe-music-player/master/Screenshot%20from%202016-05-01%2020%3A08%3A06.png">

I wanted to have a simple tiny music player that resembled a playlist where I could keep my favorite (Babes) music at the moment, I didn't care much about managing the whole music collection, as I didn't have much local music files anyway.

I did what I wanted and then I stopped developing it. By the time I tried Plasma once again and I liked it very much, the new Breeze theme looked awesome and the tools were much more advanced than the ones from elementaryOS and then I decided to stay. :)

I kept on using my small and kind of broken music player, but then I found myself using a lot the youtube-dl to get my music, given that most of the music I listen to is music that I've found/discovered while watching another (YouTube) music videos. That's when I decided to once again go back to Babe and make it fetch my favorite YouTube music videos. But by then I was using KDE/Plasma instead of a GTK based D.E. So i started learning about the Qt framework because I wanted Babe to look good and well integrated in the Plasma desktop.

<img src="https://github.com/milohr/BabeIt/blob/master/history/Screenshot_20170101_163232.png?raw=true">
<img src="https://github.com/milohr/BabeIt/blob/master/history/Screenshot_20170111_151435.png?raw=true">

My plans for Babe-Qt were simple: fetch my favorite music and then play it in a tiny interface. But, oh well, my music collection started to grow and then I decided i could make use of a collection manager integrated when needed, to be able to create another playlists besides from my favorites (Babes)... and then I implemented a full collection view, artist, albums, playlists and info view.

<iframe width="560" height="315" src="https://www.youtube.com/embed/AK_IjI3kDDY" frameborder="0" allowfullscreen></iframe>

The "info" view then became really important: I wanted to be able to get as much information of a track besides the basic metadata information, I wanted to know about the lyrics, the artwork, the artist and the album. And even eventually I wanted to be able to find similar songs... and that is what Babe is now trying to aim at, but that's something I will tell you in a next blog post... I want to introduce a contextual music collection manager.


<img src="http://orig09.deviantart.net/9f0f/f/2017/084/5/c/screenshot_20170325_221020_by_kxmylo-db3jwcb.png">


That's it for now, but I will be writing to you all back soon, and letting you know about:
-Current state of Babe
-Planned features
-The future
-Conceptual ideas

;)




