---
title:   "About Babe"
date:    2018-01-20
authors:
  - camiloh
slug:    about-babe
---
I've been working on a small music player named Babe for a while now, it started as an idea of a bare simple music player, and as the time has passed it has become a project I hope to expand beyond a simple music player.

<img src="https://blogs.kde.org/sites/blogs.kde.org/files/babeh.png">
<a href="https://babe.kde.org/"> Babe </a>

Last year I presented the project to the KDE community at Akademy 2017 that took place in Almeria-Spain, I wanted to highlight back then the idea of a multimedia desktop application that was aware of its content by making use of the wide knowledge that could be found on the internet, by making use of a music information retrieval (MIR) system called Pulpo together with AI techniques. 

The main idea has been to be able to discover and rediscover your own music collection by managing it contextually. Since then I've been working on such idea, and last month I made an initial release for Babe 1.2, which is still missing a proper tar release, but here's an  experimental AppImage you can try (on distributions with up to date glib packages)
<a href="https://drive.google.com/open?id=1xx3KcCSJ5NOQ2Rg_eZ6QOjy5QP7PMZjP"> AppImage preview </a>

and if you feel like compiling here's the 1.2 release branch: 
<a href="https://phabricator.kde.org/source/babe/browse/release-1.2/"> Babe 1.2 release branch </a>

Anyway, back to the subject.
This initial release makes use of qwidgets, which is better suited for desktop applications than it is for mobile or touch devices.
At a Akademy BoF session I had the opportunity to talk to some KDE contributors and devs about the future of Babe, and the idea of porting Babe to Kirigami, the QML framework for convergency, sounded very attractive to me, so I decided that I would be,  eventually,  porting Babe to QML/Kirigami in order to take part on the future of Plasma.

And then, after the release of Babe 1.2, I started to work on the porting and it is going pretty smooth; so far I got a working version that runs on Android, Plasma mobile and GNU Linux desktops and makes use of the same Pulpo MIR system. So you will have the same convergent music player with tons of features to connect between your devices.

<img src="https://phabricator.kde.org/file/data/nienz5vsuv66anv2qs7o/PHID-FILE-iwzh7yueypcxe6ro2bzs/exm.png" >
<a href="https://phabricator.kde.org/source/babe-qml/"> Babe QML/Kirigami preview </a>

The main idea for Babe is still the same, but slowly evolving: 
A simple place to keep your fav music at hand, yes... and a contextual manager to let you rediscover your music and discover new one. So Babe, with help of Pulpo , will crawl the internet for you and found data on music information repositories to stablish relationships between your music file and generate suggestions. 

You can even try the semantic search by making use of tags like: "similar: sam smith", "like: canadian", "tag: spanish", "lyrics: make me cry" and even more, "artist: _______",  "album: _______",  "genre: _______" , "playlist: _______" ...



Babe will also integrate in the future with online music streaming services, but for now there's a working extension for Chrome/mium and Firefox that connects to Babe, that makes use of youtubedl tool to collect your favourite music from sites like YouTube, Vimeo, etc...
Once collected, Babe will take care of the rest.
<a href="https://addons.mozilla.org/en-US/firefox/addon/babe-music-player/"> Babe add-on for Firefox </a>

And for last I want to write a little about something I've been working on, a Babe online platform to share music information and music interests with friends or other users, that idea for the platform is to have a free an open place to be able to connect to your friends throughout music, to share playlists, music lyrics and playback annotations, comments, etc...  And one idea that might could happen or not, due to copyrights issues, is streaming one-on-one the song your currently listening to, meaning you could stream to some friend or user what you are listening at the moment, similar to a live radio station.

However, it will be a very interesting project for me to develop within the KDE community and I hope to share it with you all pretty soon to make it better.

And that's it for now, I shall be making soon another blog posts writing a little more in depth about Pulpo, and the technical aspects of the projects around Babe and about the QML/Kirigami porting progress.

Let me know what you think, the comments are open.
And hopefully I will meet some of you at Akademy 2018.

