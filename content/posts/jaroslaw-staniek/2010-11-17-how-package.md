---
title:   "How to package?"
date:    2010-11-17
authors:
  - jaroslaw staniek
slug:    how-package
---
Linux fragmentation requires extra effort from packagers, and extra communication efforts between software vendors and packagers. To lower this unnecessary pain I shall remind, how to package Kexi?

Kexi depends on many packages that are highly optional, only suggested for "full installation", which in turn is rarely needed in real world. So each database driver ideally should be packaged separately for the best user experience. For example there is no need for user of file databases to install PostgreSQL or Sybase package(s). Did I mention Oracle?

<img src="http://farm4.static.flickr.com/3591/3286507734_37c0cc514c_m.jpg"/><br/><small>(<a href="http://creativecommons.org/licenses/by-nc-nd/2.0/">by-nc-nd</a>) <a href="http://www.flickr.com/photos/darwinjus/3286507734/">darwinjus</a></small>
<!--break-->
It's worth mentioning that we have this up-to-date HOWTO in KOffice for several years already in the source tree:

<a href="
http://websvn.kde.org/*checkout*/trunk/koffice/README.PACKAGERS">http://websvn.kde.org/*checkout*/trunk/koffice/README.PACKAGERS</a>

I just sent friendly reminder specifically to <a href="https://bugs.launchpad.net/ubuntu/+source/koffice/+bug/676398">Ubuntu</a>.

Developers tend to design KOffice with principles of modularization in mind. Unfortunately the apps can be unmodularized by deploying them as monolithic blobs. I guess that better to fix this in advance than the changing packaging specifications later. 

Furthermore while developers, including me, try to make an app feel right in other environments, e.g. GNOME, they try to lower set of dependencies. It's enough for me to hear complaints that "the app is good but before installation my system installed entire KDE on my desktop!". While the KDE Platform constantly improves in this area, packaging application's as monolithic piece may degrade these achievements.

Can I dare to admit that plugins are only plugins if they are optional in deployment? If these are part of the monolith, they are at most just runtime optimization based on delayed-loading pattern.

Sometimes I am wondering if there's a way for our software have cross-distribution check execution for extra factors like proper splitting, existence of all needed plugins. To make live easier for all the parties. So far we're doing what the platform provides, i.e. we're checkin for plugin versions and just ignoring too old or incompatible plugins. Deployment is in fact a part of our development process at least to some of us. So it's sometimes risky to entirely leave this task to distributions, the overworked people having thousands packages on their desks. 

I have heard KDE Platform would eventually have some sort of recommendations/guidelines published for distribution what parts of KDE identity, we as KDE, would like to have preserved (be it graphics or the way applications are run and combined, etc.). I'd love to be part of such small organized effort at applications level too. As a 3rd-party example (for the look & feel) let's see <a href="http://meego.com/developers/ui-design-guidelines/handset/ui-customization-guidelines">UI Customization Guidelines for MeeGo</a>.