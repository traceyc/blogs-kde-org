---
title:   "Future of Kexi Table View"
date:    2011-05-31
authors:
  - jaroslaw staniek
slug:    future-kexi-table-view
---
As a follow up of his promising <a href="http://labs.qt.nokia.com/2011/03/10/qml-components-for-desktop/">Qt Quick Components for Desktop</a> post, Jens shares with us results of his <a href="http://labs.qt.nokia.com/2011/05/26/table-view-with-qt-quick">Qt Quick flavour of the QTableView class</a>. This motivated me to think what would be most future-proof if not the best way of finally porting the old Qt3Support-based <a href="http://api.kde.org/calligra-api/calligra-apidocs/kexi/html/classKexiTableView.html>KexiTableView</a> class which with all related classes builds up data/database-aware library utilized by Kexi for presenting tabular data in various contexts. It's so big and complex that I left this task as the very last item while enjoying porting Kexi to Qt 4. 

It's definitely time to port and refresh this code. Qt3Support means a no-go for mobile targets and this is in part why Adam Pigg contributed with abstraction allowing to plug separate much simpler implementation of the tabular view.

<img src="http://kexi-project.org/pics/2.2/rc1/kexi-2.2-rc1-tableview-optimized-images.png">

What a coincidence, just a day before the Jens' post I enjoyed a discussion with Sebas. 

The requirements for the new implementation are mostly the same as before, the table view contains a lot of data, sometimes more than spreadsheets, have custom delegates for viewing and editing, supports column sorting and row filtering. And the model view is database-oriented (current implementation appeared before Qt4 model/view was announced). There are plans for asynchronous data loading.

So my question was (also asked in the <a href="http://labs.qt.nokia.com/2011/05/26/table-view-with-qt-quick/#comment-22271">comments</a>), regarding performance, whether to stay with Qt Graphics View like the original Plasma or look at QML/QtDeclarative. I definitely would like to avoid QScrollArea. The target is desktop usage, while only stripped-down version of this widget would be used on mobile devices.
<!--break-->
So far it looks like using Qt Graphics View in a way how Plasma does so is the way to go, which Jens correctly calls "more static". If we look at the requirements, Qt Quick gives no particular benefits here, it can just serve as alternative implementation e.g. for simple mobile editors or just data viewers. In this case some model code could be shared as a nice exercise.

What do you think? 

When would you see the new implementation is rather open question. For now the classic Qt3Support-based solution just works, what is important.


And in other news, 

<a href="http://developer.qt.nokia.com/groups/qt_contributors_summit/wiki/Schedule"><img src="http://get.qt.nokia.com/marketing/summit_01.png"></a>