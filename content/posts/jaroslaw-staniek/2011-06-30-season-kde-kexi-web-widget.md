---
title:   "Season of KDE: Kexi Web Widget"
date:    2011-06-30
authors:
  - jaroslaw staniek
slug:    season-kde-kexi-web-widget
---
Kexi Web Browser Widget is a form element that lets the user to put full-featured web browser's box onto the Kexi Form surface using single drag and drop and then load the address from the database. The challenge has been accepted by <a href="http://shreyapandit.blogspot.com/">Shreya Pandit</a> within the <a href="http://blog.lydiapintscher.de/2011/04/25/announcing-season-of-kde-2011">Season of KDE</a> program for the Calligra project. 

<img src="http://kexi-project.org/pics/2.4/alpha2/shreya.png">

As someone relatively new to Qt/KDE Shreya shows really nice progress, so below I will explain one of the use cases that is already possible with her new form widget.
<!--break-->
<b>The use case</b> 
<p>
User wants to store a set of bookmarked web sites (links) with descriptive names. Then she expects to have easy way to browse through the bookmarks and see the web sites previewed. Browsing capabilities within the same window (without opening new one) would be a plus.
</p>

<b>Modular approach</b> 
<p>
All this is now possible in Kexi. For more modular design, Shreya inserted the widget implementation into a separate Form Plugin which is compiled and loaded only when Qt WebKit is available on the system. Kexi already provides API for such plugins. Naturally, the API simplified the process of binding the form widget to a database field.

<b>Step 1: Define data</b>
There are just two steps. In step one user creates table, which at least offers Name and Url columns. Then she fills the table some content, i.e. the bookmarks.

<a href="http://kexi-project.org/pics/2.4/alpha2/kexi-2.4-web-widget-data.png"><img src="http://kexi-project.org/pics/2.4/alpha2/kexi-2.4-web-widget-data_sm.png"><br>Designing the data source: table with bookmarks, click to enlarge</a>

<b>Step 2: Design the form</b>
User creates a new empty form, and inserts Text Box for the Web Site's Name and the newly available Web Browser for presenting the web site for given address (URL) obtained from the database table.
For data source binding, Property Pane is used.

<a href="http://kexi-project.org/pics/2.4/alpha2/kexi-2.4-web-widget-form-design.png"><img src="http://kexi-project.org/pics/2.4/alpha2/kexi-2.4-web-widget-form-design_sm.png"><br>Form designer, click to enlarge</a>

<b>The result</b>
By pressing the <i>Data</i> mode button, user can start to browse through the bookmarks. Name box is filled with expected text and the Web Browser presents the live page for given address.

<a href="http://kexi-project.org/pics/2.4/alpha2/kexi-2.4-web-widget-form-data.png"><img src="http://kexi-project.org/pics/2.4/alpha2/kexi-2.4-web-widget-form-data_sm.png"><br>The form presenting data: one bookmark at a time, click to enlarge</a>

<b>Where from here?</b>
The Web widget will provide many more properties, in particular: ability to set visibility of the navigation bar.

In the long term, the it could be possible to provide KDE web bookmarks system as dedicated data source for Kexi, so the bookmarks would be easy to bound to the form as a special type of database table (view). Other idea is to store snapshots of web pages in the database for either for archiving needs or for offline use. Moreover, in addition to forms, there are plans for implementing web boxes support in Kexi Reports (printouts) and ODF documents.

For discussing many more details we have set up dedicated <a href="https://groups.google.com/forum/?hl=en&pli=1#!forum/season-of-kexi">Season of Kexi</a> group.

<b>Conclusion</b>
By selecting task that is appropriate to the current skills, developers can quickly start contributing to the Kexi project, what can further increase motivation.

<b>The code</b>
<p>
The code is not present in Kexi 2.4 snapshot but in <a href="https://projects.kde.org/projects/calligra/repository/show?rev=kexi-web-shreya">kexi-web-shreya</a> branch of the Calligra repository. community.kde.org offers article on <a href="http://community.kde.org/Calligra/Building/Building_Calligra">how to compile Calligra applications</a>.
</p>