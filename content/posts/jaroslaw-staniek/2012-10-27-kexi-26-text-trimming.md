---
title:   "Kexi 2.6: Text Trimming"
date:    2012-10-27
authors:
  - jaroslaw staniek
slug:    kexi-26-text-trimming
---
Kexi 2.6 now shows indication that edited or pasted text is trimmed if maximum length for field has been reached. See also for the same feature in tabular view. The rationale for this feature is to make sure user knows that data has been only partly entered into the database.

<b>Text trimming in tabular view</b><br>
<a href="http://www.youtube.com/watch?v=Qdd7CaBonq0"><img src="http://kexi-project.org/pics/2.6/kexi-2.6-text-trimming-in-tables_sm.png"></a>

(click for video above or download <a href="http://kexi-project.org/pics/2.6/kexi-2.6-text-trimming-in-tables.ogv">ogv file</a>)

<b>Text trimming in forms</b><br>
<a href="http://www.youtube.com/watch?v=wjyGsWxnAEs"><img src="http://kexi-project.org/pics/2.6/kexi-2.6-text-trimming-in-forms_sm.png"></a>

(click for video above or download <a href="http://kexi-project.org/pics/2.6/kexi-2.6-text-trimming-in-forms.ogv">ogv file</a>)

Like other features, underlying code is heavily shared between tables and forms despite deep differences between implementation of tabular view (canvas-based) and form view (widget based).


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.calligra.org/"><img src="http://www.calligra.org/wp-content/themes/calligra/images/calligra-logo-200.png"></a>