---
title:   "Fruits of Calligra Sprint"
date:    2011-04-05
authors:
  - jaroslaw staniek
slug:    fruits-calligra-sprint
---
Last week was big for Calligra:

<ul>
<li>March 30: <a href="http://www.calligra-suite.org/news/calligra-suite-the-first-three-months/">The first three months of the Calligra Suite</a> is a report showing activity in all possible areas related to Calligra, and creation of new areas like the new Braindump application, new maintainers for applications, refreshed <a href="http://www.calligra-suite.org">application names</a>, project home page, project git repositories, and finally Kexi Mobile, Calligra Mobile. More to come.<br><br>

<img src="http://kexi-project.org/pics/ads/calligra/calligra-bar-300.png">
<br>
</li>

<li>April 1: <a href="http://blog.cberger.net/2011/04/01/calligra-sprint-spring-2011-day-0/">The first Calligra sprint started</a>, <b>the biggest technical event in the history of KDE Office technologies to date</b>:<br><br>

<br><img src="http://kexi-project.org/pics/ads/calligra/kde-office-sprints.jpg">
<br>
</li>
</ul>
<!--break-->
<ul>
<li>April 2: <a href="http://blog.cberger.net/2011/04/02/calligra-sprint-spring-2011-day-1/">intensive and nicely prepared discussions in areas common to all applications:</a> frequent releases strategy, marketing (new logos!), analyzing strategies for taking advantages of Qt-only approach for Calligra libraries (wherever applicable)[*], aligning with the new KDE libs' mobile profile initiative, reports from usability tests, new UIs[*], approaches to modern document embedding.

<br><br><img src="http://kexi-project.org/pics/akademy/2011/berlin_sprint/calligra-usability-tests.jpg">
<br>
</li>

<li>April 3: <a href="http://blogs.kde.org/node/4403">"Applications" day of the Calligra Sprint</a>, i.e. works mostly splitted to <a href="http://en.wikipedia.org/wiki/Birds_of_a_Feather_%28computing%29">BoFs</a>, with presentation by Casper (Words' text layout refactoring), short presentation of light-speed developments (Kexi integration with maps using Marble widgets)[*] delivered by Radek, our <a href="http://lists.kde.org/?l=calligra-devel&m=130033023201354&w=2">newest member</a>:

<br><br><img src="http://kexi-project.org/pics/akademy/2011/berlin_sprint/kexi-marble.jpg">
<br>
</li>

</ul>

[*] more on that in future blog posts

Also the good: nice food and beer as always. Met Kexi developer <a href="http://kexi-project.org/ppiggz.html">Adam Pigg</a> for the first time after all the years <a href="http://en.wikipedia.org/wiki/Real_life">IRL</a>, yay! Amazing organization led by Thorsten Zachmann, KDABians and sponsorship by KDE e.V., Nokia and KDAB. All in now very comfortable KDAB offices.

<strike>The bad:</strike> No earlier than week ago noted to fellow Calligra hackers that while my ASUS laptop is getting 6 years old, it still does the job. Then on first day of Calligra Sprint it decided to almost burn itself near the graphics controller, so I got funny random-puzzle-like screen. The only way to use it in graphics mode was to <i>ssh -X</i> to it from another machine ;P. <b>Update:</b> surprisingly enough, the laptop has AUTOREPAIRED itself... i.e. the graphics display is OK now. No idea what happened... the only think I did to it is some accidental shaking while on the train to Warsaw. Should I say "Thanks, <a href="http://rozklad-pkp.pl/?q=en">PKP</a>?"