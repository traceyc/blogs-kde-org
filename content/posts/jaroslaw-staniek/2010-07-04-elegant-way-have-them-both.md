---
title:   "Elegant (!) way to have them both"
date:    2010-07-04
authors:
  - jaroslaw staniek
slug:    elegant-way-have-them-both
---
Do you remember the <a href="http://blogs.kde.org/node/2955">tabbed toolbar</a> thing that was once born in Kexi? It's an attempt to optimize the UI when collection of actions depends on context. This part of development goes well, even while there is no framework for use by other KDE 4 apps. Those still use the KDE 3 technology, i.e. KXMLGUI leading to rather big menus.

What's understandable many users prefer to use menus, and even more of them would ideally use both toolbars and menus. Certain actions are hard to express using icon or icon+short name.

Read on to see a proposal to have both of the worlds.
<!--break-->
Only few out there probably know that Zoho office has a fresh concept. Here's normal tabbed-toolbar-like interface:

<img src="https://kexi-project.org/pics/blog/2010/elegant-way-have-them-both/1.jpg">


Now the interesting - the same interface provides menu. Just click the arrow:

<img src="https://kexi-project.org/pics/blog/2010/elegant-way-have-them-both/2.jpg">


If you ask me, it's simple and can work. It's possible to add shortcuts and keyboard navigation goodness.

Here's more explanation: <a href="http://translate.google.com/translate?hl=en&ie=ISO-8859-1&sl=pl&tl=en&u=http://grzglo.jogger.pl/2009/03/05/zoho-writer-wzorowa-organizacja-paska-narzedziowego/&prev=_t&rurl=translate.google.com&twu=1">google PL->EN translation</a>. Let's hear what you think in the comments!