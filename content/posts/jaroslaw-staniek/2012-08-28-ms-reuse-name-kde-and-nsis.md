---
title:   "MS to reuse name from KDE and NSIS"
date:    2012-08-28
authors:
  - jaroslaw staniek
slug:    ms-reuse-name-kde-and-nsis
---
Oh my. Microsoft has been using the "Metro" name to describe their new UX without prior careful checking for trademark infringement. PR blunder: Teutonic retailer Metro AG <a href="http://arstechnica.com/information-technology/2012/08/microsoft-metro-out-windows-8-style-ui-in-amid-rumors-of-a-trademark-dispute/">had to threaten them</a> with legal action just two months before Win 8 release, when I bet some folders advertising Metro were already printed out.

Would it happen again? In its corporate rush, MS <a href="http://www.theverge.com/2012/8/10/3232921/microsoft-modern-ui-style-metro-style-replacement">picked new name</a>: Modern UI. Better they could safely stay with typically long "Windows 8 UI Style" term because <i>Modern UI</i>'s been already taken not just once:

<ul>
<li><a href="http://nsis.sourceforge.net/Docs/Modern%20UI/Readme.html">The Modern UI of the popular NSIS installer generator</a>. This usage started at least in 2003.
<li>In KDE's context it's all our <a href="http://blogs.kde.org/2011/04/06/kde-5-menu">Modern Menu</a> and GUI in general giving web-like experience without browser's compromises, discussed on the <a href="http://community.kde.org/Calligra/Meetings/Fall_2011_meeting/Minutes#Integration_BoF">Calligra Sprint</a>. This usage started in early 2011.</li>
</ul>

Bon appetit, MS. Being inspired is good.

Regardless you take this post seriously or not, I'm planning to continue with Modern UI use in my works, both as name and design/implementation. ;)

<br>
<img src="http://farm1.staticflickr.com/186/471012331_ccb815ea2b.jpg"><br/>by <a href="http://www.flickr.com/photos/soulair/471012331/lightbox/">soulair</a>