---
title:   "Fruits of CSS2: Office Forms"
date:    2012-01-04
authors:
  - jaroslaw staniek
slug:    fruits-css2-office-forms
---
(this is a continuation of the Fruits of Calligra Suite Sprint #2 series)

2012 started and I am still summarizing ideas from the Sprint. Today an integration idea: Office Forms for Calligra apps.

Forms are useful for improving data entry. Alternatives like entering data into spreadsheet cells can be too error-prone or tedious for the end-users. Forms reuse the GUI paradigm that is most probably already known and accepted by the user.

First, it is good to know forms were historically handled in popular office software. Extra functionality in MS Office or OpenOffice/LibreOffice still tend to be added using form elements (widgets) like buttons directly embedded into the document. 

<a href="http://kexi-project.org/pics/blog/2012/ooo_invoice.jpg"><img src="http://kexi-project.org/pics/blog/2012/ooo_invoice_sm.jpg"></a><br/><i>OpenOffice with embedded invoicing functionality. Note the buttons floating within the spreadsheet area.</i>
<!--break-->
Quick look from the bird's-eye view shows multiple issues.

There is clear escape from the document paradigm to the application paradigm, thus mixing the document and the app functionality can be observed. Not only there is no model-view approach. On the plus side is that the user can just open the document which is a self-contained file, easy to manage and transmit. But this comes at cost. To start fresh with a clean document, the whole original document with the GUI and business logic code is just copied. This is not a surprise as the documents with forms are distributed as document templates. Because of that copying there is no organized way to upgrade the GUI and the code when the authors provide a new version.

And let's not forget: users are gernerally unable to notice which part of the GUI comes with document and which is provided by the application itself. So there is a break in the familiar GUI paradigm. User may accidentally move or delete a button. The form elements affect the presence of printouts. Various document formats deal with this by employing whole set of workarounds like locking position and modification of a form widget and excluding them in printouts. This further contributes to complexity of the solution.

So at the Calligra Sprint instead of drafting a set of workarounds I outlined idea for the solution (see the mockup below):
<ul>
<li>Enable creation of flake tool-compatible side panes that could be then switched on by user demanding custom functionality with a few mouse clicks. Documents that demand given functionality would be able to show/hide the panes too.</li>

<li>Plugin approach. Plugins could be deployed as single file bundles, versioned and separated from documents. Business logic would use JavaScript (via QtScript) to fully escape from platform dependency and to address major security issues. This approach blends with the idea of including JavaScript APIs for document integration generation and data visualization within the Calligra Engine.</li>

<li>Since ODF allows to embed custom object files, the plugin bundles could be embedded into the files for easier deployment. For this some kind of fall-back to the version embedded in document would be needed. But once installed at app-level, that installed version would be used as long as version,  vendor, whatever matches. Side-by-side installation of various versions would be a plus.</li>

<li>GUI builder and extra runtime functionality may be delivered with the help of Kexi Form designer, which is a higher-level data-oriented reimplementation of Qt Designer in development since 2004.</li>

<li>Effect: the document is not cluttered, stays just as a document. The scripted functionality can be bind to the application's structure at runtime instead of injecting into document structure directly.</li>
</ul>

<a href="http://kexi-project.org/pics/blog/2012/calligra-forms-mockup.jpg"><img src="http://kexi-project.org/pics/blog/2012/calligra-forms-mockup_sm.jpg"></a><br/><i>A mockup of Calligra Tables with embedded invoicing functionality. The sample custom form has been moved to a separate sidebar.</i>

There is extra rationale. It was never  too realistic for the Calligra project to support <a href="http://en.wikipedia.org/wiki/Visual_Basic_for_Applications">VBA</a>/<a href="http://en.wikipedia.org/wiki/StarBasic">StarBasic</a> and forms. Even StarBasic perceived as a drop-in replacement for VBA never achieved compatibility and continued its life as a distinct scripting environment. Only simpler VBA solutions are similar under StarBasic. Even assuming extremely good will, achieving reasonable compatibility is very hard. So VBA is Windows+MS Office-only. Microsoft itself has admitted this at least twice: by giving up with delivering VBA for its Mac Office, then it skipped scripting topics in its MSOOXML specifications by declaring scripting as implementation-defined area. That was rather honest move.

Then there is question about the ODF. Currently reusing any partial support for scripting/forms defined by ODF is not worth the effort in my opinion. While working on applications we start with requirements and usability. Reusing incomplete standards here is nice-to-have but at this stage definitely it cannot have higher priority than practicality and usability of the solution.

This does not mean I am ignoring importance of standards. As the case of <a href="http://en.wikipedia.org/wiki/OpenFormula">OpenFormula</a> shows, ODF is vital in its iterative approach especially when the problem and the scope is well defined and rather narrow. So users would eventually see some complete standard in the area of forms going behind the level of XForms. Cooperation is getting more possible after the collapse of the controlled corporate structures of OpenOffice.org (but let's appreciate the huge contributions of Sun) and emerging of LibreOffice as a free software project.

<a href="http://calligra.org"><img src="http://kexi-project.org/pics/ads/calligra/calligra-logo-150.png"></a>