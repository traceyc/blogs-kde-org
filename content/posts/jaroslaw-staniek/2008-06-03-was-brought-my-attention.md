---
title:   "That was brought to my attention..."
date:    2008-06-03
authors:
  - jaroslaw staniek
slug:    was-brought-my-attention
---
<a href="http://liquidat.wordpress.com/2008/05/30/kde-at-linuxtag-2008-day-2-taking-over-the-world/">liquidat has posted some notes</a> on what was presented regarding KDE-PIM on LinuxTag 2008. As people started to ask about native look, let me add three more screeenies, all from May 2008.

First, the native KMail on XP with disconnected IMAP accounts:

<!--break-->

<a href="http://windows.kde.org/pics/kdepim/2008-05/kmail_xp_may_2008.png" target="_blank"><img src="http://windows.kde.org/pics/kdepim/2008-05/kmail_xp_may_2008_.png"/><br/>Click to enlarge</a>

Native Kontact with KMail page on Vista:

<a href="http://windows.kde.org/pics/kdepim/2008-05/kontact1_vista.png" target="_blank"><img src="http://windows.kde.org/pics/kdepim/2008-05/kontact1_vista_.png"/><br/>Click to enlarge</a>

Then, native Kontact with KOrganizer page on Vista, fed with resources from Kolab server:

<a href="http://windows.kde.org/pics/kdepim/2008-05/kontact2_vista.png" target="_blank"><img src="http://windows.kde.org/pics/kdepim/2008-05/kontact2_vista_.png"/><br/>Click to enlarge</a>

The former two pictures were used by Till in the LinuxTag presentation about Kontact.

<a href="http://kexi-project.org/pics/kde/kdepim/2008_05/js_kde_lt_.jpg"><img src="http://kexi-project.org/pics/kde/kdepim/2008_05/js_kde_lt_.jpg" align="right"/></a>For the last 6+ months I've been working on making native KDE-PIM stronger as a crossplatform beast, mainly on Windows. Thanks to awesome, spontanic works of the <a href="http://windows.kde.org/">KDE on Windows Team</a>, we can say that the first KDE-controlled distro has emerged. 

On the other hand, core KDE-PIM developers made it possible that cryptographic features are already present in KDE-PIM on all three platforms. Equally important is fact that <a href="http://kolab.org/">Kolab</a> server solutions are accessible ewerywhere too. And finally, we have still (typically in the Qt world) a single-source code software project, prepared for inclusion of <a href="http://pim.kde.org/akonadi/">Akonadi storage services</a> appearing in KDE 4.1.

The KDE-PIM involvement, being an extraordinary work experience, obviously also cut off many of my resources devoted previously for KOffice/Kexi, but stay tuned. There are important works like <a href="http://www.piggz.co.uk/index.php?page=blogentry&id=30">Reports</a> and <a href="http://blog.binaryhelix.net/2008/05/20/kexi-web-forms/">GSoC-sponsored Web Forms</a> that I just simply cannot miss.
