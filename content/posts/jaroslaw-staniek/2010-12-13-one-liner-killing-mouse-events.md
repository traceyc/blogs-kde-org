---
title:   "One-liner: killing mouse events"
date:    2010-12-13
authors:
  - jaroslaw staniek
slug:    one-liner-killing-mouse-events
---
Tomaz <a href="http://liveblue.wordpress.com/2010/12/13/qt-tip-trick-masking-widgets/">shares a hint with us</a>:
<b><i>Have you ever tried to create an overlay on top of some QWidgets to make them not acessible, but still visible to the user? I’m not saying about setEnabled(false), but to create a semitransparent – mask on top of some widgets with something inside, a modal dialog or so.
[..]
aseigo had already told me that it was possible  by using a QGraphicsView...</i></b>
<!--break-->
No need to use a cannon to kill a fly.

Just use this *elegant* call:

<b>myOverlayWidget->setAttribute(Qt::WA_TransparentForMouseEvents);</b>


Other calls like setAutoFillBackground() may be still needed of course but you can use plain widgets.