---
title:   "64k"
date:    2010-09-17
authors:
  - jaroslaw staniek
slug:    64k
---
This week I noticed: I am part of Kexi/KOffice Development Team for 65535 hours now. With fine breaks of course.

It's possible to check at the bottom of <a href="http://websvn.kde.org/trunk/koffice/kexi/doc/dev/CHANGELOG-Kexi-js?view=markup">this file</a>.

64k should be enough for everyone. But not for me :)

PS: <a href="http://www.koffice.org/news/koffice-2-3-beta-1/">2.3b1 released!</a>