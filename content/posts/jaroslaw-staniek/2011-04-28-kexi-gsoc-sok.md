---
title:   "Kexi GSoC & SoK"
date:    2011-04-28
authors:
  - jaroslaw staniek
slug:    kexi-gsoc-sok
---
Expect more entries from the front of <a href="http://community.kde.org/Kexi/Junior_Jobs/Map_Browser_Form_Widget">Kexi integration with maps using Marble widgets</a> since this task has been selected for the Google Summer of Code 2011! Radek Wicik, who just recently joined the Calligra team works on this project. His early attempts have been demonstrated in April during the <a href="http://blogs.kde.org/node/4404">Calligra Sprint</a>. And he shares his progress info on dedicated <a href="http://rockfordsone.blogspot.com/">blog</a>.

<a href="http://rockfordsone.blogspot.com/2011/04/marble-in-kiexi-forms.html"><img src="http://kexi-project.org/pics/2.4/alpha1/kexi-2.4-map-widget_sm.png"><br>Radek's implementation of Maps as Kexi Form widgets</a>
<!--break-->
There's more to add. KDE is rather giant project within GSoC with 51 slots and many more apps and proposals. It is not surprise we have no more slots for Kexi but there is another great initiative, <a href="http://blog.lydiapintscher.de/2011/04/25/announcing-season-of-kde-2011/">Season of KDE</a>. It is nicely aligned to the GSoC timeline, so student that have not fount their places in the GSoC 2011 program, have been contacted. Thanks to this opportunity our second student, Shreya Pandit, agreed to work on <a href="http://community.kde.org/Kexi/Junior_Jobs/Web_Browser_Form_Widget">Web Browser support for Kexi</a>.

More information about goals of the projects:
<ul>
<li><a href="http://community.kde.org/Kexi/Junior_Jobs/Map_Browser_Form_Widget">http://community.kde.org/Kexi/Junior_Jobs/Map_Browser_Form_Widget</a></li>
<li><a href="http://community.kde.org/Kexi/Junior_Jobs/Web_Browser_Form_Widget">http://community.kde.org/Kexi/Junior_Jobs/Web_Browser_Form_Widget</a></li>
</ul>

<br>
<img src="http://dot.kde.org/sites/dot.kde.org/files/GSoC2011_300x200_px.jpg">