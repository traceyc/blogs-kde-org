---
title:   "Can we learn something from GNOME startup time analysis?"
date:    2005-10-13
authors:
  - jaroslaw staniek
slug:    can-we-learn-something-gnome-startup-time-analysis
---
I mean these analysis:
http://www.gnome.org/~lcolitti/gnome-startup/analysis/

Tell me if I am wrong but has KDE something to gain in this department?


<!--break-->

Using mmapped data structures and/or RAM-disk? Using Suspend-To-Disk-Like trick so most, once prelinked, libraries can be saved to disk and restored on next startup just in one second by loading one large file?

Or, unlike in GNOME, it's no longer KDE problem, and we should rather look at things like faster loading shared libraries?
