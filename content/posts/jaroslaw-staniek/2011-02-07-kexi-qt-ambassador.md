---
title:   "Kexi: Qt Ambassador"
date:    2011-02-07
authors:
  - jaroslaw staniek
slug:    kexi-qt-ambassador
---
<a href="http://qt.digia.com/qtambassador/"><img src="http://kexi-project.org/pics/ads/kexi-qt-ambassador.jpg"></a>

<b>Thanks for the opportunity of <a href="http://qt.digia.com/qtambassador/">presenting Kexi</a> as the official Qt Ambassador Project! <a href="http://kexi-project.org">Since 2002</a> it benefits from the solid foundation of Qt (and KDE of course)!</b>

Just one note: Are you developer interested in adding features? Want to join the (paid) <a href="http://techbase.kde.org/Projects/Summer_of_Code">Summer of Code Project</a> (even if you're not very skilled yet or have no clear ideas)? Would you like to attend the <a href="http://www.desktopsummit.org">Akademy 2011</a> conference or Calligra developer meetings? Or are you a user with lots of innovative suggestions? Good :). Drop a note in the comments or write to kexi@kde.org.
<!--break-->
<!-- words words words words words words -->