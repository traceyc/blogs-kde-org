---
title:   "Kexi in February"
date:    2010-03-02
authors:
  - jaroslaw staniek
slug:    kexi-february
---
What's new, based on <a href="http://identi.ca/kexi">identica notes</a>:

<ul>
<li><a href="http://blogs.kde.org/node/4156">Thoughts on deploying SQLite</a> turned out to be work in progress. Valuable input from our distro friends. A special <a href="http://techbase.kde.org/Development/Kexi/Build#SQLite_packaging">wiki page</a> has been created. See the last item of this entry ;)
<li>KoReport, Kexi's rpt backend has undergone some refactoring to make it more generic.
<li>Fix number 1, charts working again in the designer.<br/><img src="www.piggz.co.uk/kexi/chart1.png">
<li>Working through the huge list of issues krazy has found with the kexi codebase, not glamorous, but necessary!
<li>Refactored the report renderers, and given better class name, to make the report library suitable for adoption over all of KOffice.
<li>Do you want well working MSAccess to Kexi converter? <a href="http://mail.kde.org/pipermail/kexi/2010-February/000667.html">It's up to you - request it!</a>
<li>KOffice now has a suite-wide reporting library! (placed in koffice/libs/koreports/)
<li>Kexi switched to "system" SQLite again. We want to play with distros well, but will check SQLite features at compilation to maintain high quality standards.
</ul>

(brought to you by <a href="http://www.piggz.co.uk/index.php?page=topnews">Adam</a> and myself)
<!--break-->
