---
title:   "New year at a new desk"
date:    2009-01-05
authors:
  - jaroslaw staniek
slug:    new-year-new-desk
---
New year means some snow and cold noses here in Warsaw, and a new <a href="http://www.polishmarket.com/next.php?id=61632">job</a> to me, this time in the mobile industry, what has rather diversified my day, and that's good. Happy 2009 to you, to your friends and family.

But I am on the KDE board too, it's not going to change. What's recently time-consuming for me is <a http://wiki.koffice.org/index.php?title=Kexi/Porting#Forms_Refactoring">refactoring</a> of the <a href="http://kexi-project.org/pics/1.1/custom_widgets.png">Native Kexi Forms</a>. The most serious and anticipated decision is dropping the (implemented in 2004..2005) idea of the forms component reusable  at a rich API level for other applications. The idea has introduced too man layers after months of development, too many to have things maintainable, with just proof-of-concept KFormDesigner being the only app using the framework except Kexi.
<!--break-->
Since 2005 things have changed, Qt Designer gained its own reusable libraries. Before someone asks - we don't use them in Kexi Forms not just because that would affect the licensing (LGPL) or because of backward compatibility required by the current Forms' XML format (which is ~95% the same as Designer's but the 5% makes the difference). My point (or read it as feelings) is that while there would be no technical overhead in reusing Qt Designer, we would have overhead counted in man-months.

The first step, mostly finished now was to remove Qt3 Support dependencies. A bit late? True, but not too late. The code using the meta objects and properties is too fragile to receive massive changes in one go. Unlike <a href="http://api.kde.org/koffice-api/koffice-apidocs/kexi/koproperty1/koproperty/html/index.html">KoProperty</a> that received rather massive rewrite to Qt4's model/view API (I am not 100% happy with the results yet ) since last summer, Forms are more complex and I am employing strategy of incremental improvements for them. But after all, did I mention that continuous advances in many other parts of KOffice (if not just entire KDE 4) work as true motivation?