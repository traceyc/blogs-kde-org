---
title:   "Deaaronify Service"
date:    2006-08-11
authors:
  - jaroslaw staniek
slug:    deaaronify-service
---
If you, like me, have trouble with slower reading text lacking capital letters, but still want to be up to date with all that buzz, there are good news: "Deaaronify" service has been started :0)

<!--break-->

Just type any url as an argument, e.g.:

http://kexi-project.org/tmp/deaaronify/?http://aseigo.blogspot.com

It's powered by an unoptimized php script.
Have fun!
