---
title:   "Sybase and MS SQL Server Support"
date:    2007-12-28
authors:
  - jaroslaw staniek
slug:    sybase-and-ms-sql-server-support
---
From KDE4-enterprise-functionality dept.: As already noted in the <a href="http://commit-digest.org/issues/2007-12-16/moreinfo/746685/">Commit Digest</a>, another database driver (i.e. data provider) has been <a href="http://websvn.kde.org/trunk/koffice/kexi/kexidb/drivers/sybase/">built</a> on top of Kexi's database abstraction layer (<a href="http://www.kexi-project.org/docs/svn-api/html/namespaceKexiDB.html">KexiDB 2</a>, codenamed Predicate). It is <a href="http://en.wikipedia.org/wiki/Sybase">Sybase</a> driver, also aimed at handling <a href="http://pl.wikipedia.org/wiki/Microsoft_SQL_Server">MS SQL Server</a> connections.

<!--break-->

Even better news is that these goodies come from the very newest Kexi developer, <a href="http://www.kde.in/index.php/Sharan_Rao">Sharan Rao</a>, which I have met at <a href="http://akademy2007.kde.org/">Akademy 2007</a>. Before he has been working on <a href="http://uml.sourceforge.net/index.php">Umbrello UML Modeller</a>, which is related to databases too.

As MS SQL Server is practically a fork of Sybase, it is accessible by rather similar connection protocol. Therefore both servers are handled by the same KexiDB driver. Its functionality, now in experimental stage, is implemented on top of so-called 'dblib' layer of the <a href="http://freetds.org/">FreeTDS</a> library, which is a result of reverse-engineering of the servers' closed TDS protocol.

<a href="http://kexi-project.org/pics/2.0/alpha5/kexidb-sybase-tables-test-1.jpg"><img src="http://kexi-project.org/pics/2.0/alpha5/kexidb-sybase-tables-test-1-sm.jpg"/></a><br/><i><a href="http://websvn.kde.org/trunk/koffice/kexi/tests/newapi">KexiDBTest</a> application's output - creation of a new database and test tables on a Sybase server (click to enlarge)</i></br>

<a href="http://kexi-project.org/pics/2.0/alpha5/kexidb-sybase-tables-test-2.jpg"><img src="http://kexi-project.org/pics/2.0/alpha5/kexidb-sybase-tables-test-2-sm.jpg"/></a><br/><i>Successful result of the tests (click to enlarge)</i>

One of the fundamental rules for development of Kexi and underlying frameworks is avoiding discrimination against various operating systems and hardware platforms. This is the reason for not using native Windows libraries. Like in the case of import function for MS Access files (mdb), where (unlike of the current OpenOffice.org Base fro Windows, which employs Windows-only library) we are using <a href="http://mdbtools.sourceforge.net/">MDB Tools</a> library, one that is portable to most operating systems and architectures.

<a href="http://kexi-project.org/pics/2.0/alpha5/kexidb-sybase-result-1000.png"><img src="http://kexi-project.org/pics/2.0/alpha5/kexidb-sybase-result-sm.png"/></a><br/><i>Database created on the Sybase server and opened with the KexiDBTest application (click to enlarge)</i>

The Sybase/MSSQL driver has been rather developed for KexiDB/Predicate, not directly for Kexi itself, so we maintain ability of transparent access to the databases from any KDE 4 code. What's interesting, for convenience Sharan utilizes <a href="http://en.wikipedia.org/wiki/SQL_Anywhere">SQL Anywhere</a> for his tests, i.e. an embedded Sybase version -- single-file-based, similar in terms of approach to already well-known SQLite database.

PS: Let me ask you - <b>do not hesitate to <a href="http://kexi-project.org/contact.html">contact</a> us if you know someone with working Sybase and/or MSSQL installations that could be accessible remotely from time to time for the purpose of testing the driver.</b>

<b>Greetings and best wishes for the new year!</b>
