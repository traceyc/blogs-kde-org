---
title:   "KDE Forms in Your Browser"
date:    2007-03-27
authors:
  - jaroslaw staniek
slug:    kde-forms-your-browser
---
How to get more developers on our board? No doubt, it's easier to get them as we support more languages. Kross helps to utilize this strategy, especially for Python, Ruby, Javascript and (soon/less or more) Java developers. 

<!--break-->

There is however a large group of developers without any big KDE offer available. PHP devs. How to make them wow "hey, KDE makes my live easier as PHP user!"? Neither I am thinking about PHP-bindings - like php-gtk - it's so artificial IMHO, <b>nor</b> about dev environments for preparing scripts and managing web projects like Quanta.

There's one <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007">KDE GSoC</a> project that spoils PHP devs: <a href="http://jacek.migdal.pl/gsoc2007/">Web Interface for KDE Data Forms (KOffice/Kexi)</a>. 

What kind of animal is this? It's updated proposal (since GSoC 2006) by Jacek Migdał, allowing PHP devs to export data models to a server backend and switch presentation layer from rich Qt/KDE data widgets to web interface. We may be able to control transformation from (extended) Qt Designer's XML to XHTML. Isn't this a place where both - desktop and web worlds - meet each other?

You can design the data models once using WYSIWYG tools like Kexi (and later maybe: Umbrello/KDevelop), the forms - by Qt Designer/Kexi From Designer. The goal is to use more already available KDE tools and perform less server-side 'tricks'. It's even not about databases, a flat data model defined a KSpread's OpenDocument sheet is also well suited for publishing. 

A part of  the project is also about KDE's own web server implementation (!) based on <a href="http://www.lighttpd.net/">lighttpd</a>. Why this? Every desktop user could be able to set up her/his own service in almost no time, without playing with configuration settings. 
It's especially about Intranets. More advanced folks that want to make the resulting web app publicly available, will more likely use automatic uploads to their Apache web server. but the lighttpd server is interesting case because of simplified configuration.

AJAX support is also on the TODO. Take a look at a mockup using Google's API:

http://jacek.migdal.pl/gsoc2007/kexiwebforms_demo/

<img src="http://kexi-project.org/pics/2.0/webforms/webforms2007.png"/>


Update: the mockup actually is not based on Google's API to avoid incompatibility with Konqueror.
