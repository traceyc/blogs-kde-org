---
title:   "KDE-Windows-How-To-Join -HOWTO"
date:    2008-01-24
authors:
  - jaroslaw staniek
slug:    kde-windows-how-join-howto
---
Just decided to write some info here as more and more people are interested in joining the KDE project, at least as active users that want to provide valuable feedback. In particular they are asking about various most-used applications like KOffice or KDE-PIM/Kontact.

First, there is a dedicated techbase page http://techbase.kde.org/Projects/KDE_on_Windows

Read on for more hints...
<!--break-->
We have also "KDE-PIM on Windows" dedicated page http://techbase.kde.org/Projects/PIM/MS_Windows

This is a subpage of http://techbase.kde.org/Projects/PIM, where you can find some more general information about the PIM development. Kontact for KDE 4 is maturing fast (with KDE 4.1 as a target) but is not yet ready for 4.0 regardles of the platform.

Note that many of the works are performed in a platform-independent way - that's why I have provided the link to more general information.

Lastly, I have just created a special page for contributors (testers and developers):
http://techbase.kde.org/Projects/KDE_on_Windows/How_to_Help

Note (as usually) that the fact you have filed a wish or bug report, does not actually means it will be addressed next day. The Windows version approaches beta stage. 

<b>Update (2008-01-25):</b> Just updated the "How to Help" page - described the list of Windows-related components and added step-by-step instructions for "Querying existing reports" and "Sending a new report" sections.

<img src="http://techbase.kde.org/images/thumb/d/d9/Kde-windows-bugzilla.png/550px-Kde-windows-bugzilla.png"/>