---
title:   "KDb 3, KProperty 3, KReport 3, Kexi 3 and special offer during Akademy"
date:    2016-09-04
authors:
  - jaroslaw staniek
slug:    kdb-3-kproperty-3-kreport-3-kexi-3-and-special-offer-during-akademy
---
We're pretty close to "beta" releases of the KDb 3, KProperty 3, KReport 3 frameworks as well as Kexi 3, the data-oriented app creator. So while I am not in person @ Akademy I though a "special offer" during this special time would be in order:  if anyone has questions or needs assistance in planning port to one of the APIs offered, ping me on IRC or mail me. And also later :)

<b>What's in the box and when to use the toys:</b>

- you use <a href="https://community.kde.org/KDb">KDb</a> if you want to move from QtSQL (that sure is a Qt gem but no longer adds new features); but there's more -- if you want to empower your users with visual Table and Query and Report/Form designers -- all on top of KDb, automatic database creation and free jumping e.g. between MySQL, PostgreSQL and SQLite (first such feature known to me not requiring writing conversion scripts by hand), free CSV import and export for large data (no need to be limited by the size of spreadsheets), you're get that it all too, the GUIs are part of <a href="https://community.kde.org/Kexi">Kexi</a>
<a href="http://kexi-project.org/pics/3.0/kexi-3.0-alpha-sql_editor.png"><img src="http://kexi-project.org/pics/3.0/kexi-3.0-alpha-sql_editor.png" width=218></a>

- you use <a href="https://community.kde.org/KProperty">KProperty</a> if you want to move from, say, a QTreeView/TableView-based hand-made property editing GUI to a KF5-like flavour of property editor widget and models, functionally like the one from Designer (or the Qt Property Broweser Framework)
<a href="http://kexi-project.org/pics/3.0/kproperty-windows.png"><img src="http://kexi-project.org/pics/3.0/kproperty-windows.png" width=136></a>

- you use <a href="https://community.kde.org/KReport">KReport</a> if you want to move from hand-made generating of HTML/PDF/ODT/printouts based on data source/templates and mail merge-like features, as well as if you want to empower your users with visual Report Designer that produces the report documents supported by the with KReport API, if you plan to depend on Javascript extensions for all that, consider this as a bonus from life :)

<a href="http://kexi-project.org/pics/3.0/kreport-windows.png"><img src="http://kexi-project.org/pics/3.0/kreport-windows_sm.png" width=180></a>

For now to get the code visit the KDE git repos. To get the API docs please run Doxygen on the source code.
