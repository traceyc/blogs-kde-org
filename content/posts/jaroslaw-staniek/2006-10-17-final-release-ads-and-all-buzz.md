---
title:   "Final Release With Ads And All That Buzz"
date:    2006-10-17
authors:
  - jaroslaw staniek
slug:    final-release-ads-and-all-buzz
---
One picture tells the whole story:

<a href="http://kexi-project.org/wiki/wikiview/index.php@Kexi1.1.html"><img src="http://kexi-project.org/pics/1.1/kexi_1.1-640-pc.jpg"></a>
(<a href="http://kexi-project.org/wiki/wikiview/index.php@Kexi1.1.html">link</a>)

Big thanks to the friendly KOffice team, in particular to Cyrille who deals with the time-consuming tasks related to the release!

Kexi's going to jump from 1.1 to 2.0 version next time, so even the version numbers will be synced among KOffice apps. 

For those impatient, I've updated the <a href="http://kexi-project.org/screenshots.html#1.1" target="_blank">screenshots</a>.