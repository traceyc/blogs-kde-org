---
title:   "Hidden b.k.o-phab links"
date:    2016-02-04
authors:
  - jaroslaw staniek
slug:    hidden-bko-phab-links
---
Well, consider them unhidden now: if you missed a structured place to link <a href="http://bugs.kde.org">bugs.kde.org</a> big/wish reports with <a href="http://phabricator.kde.org">KDE Phabricator</a> tasks, look no further.

One or more "See Also" fields of the bugzilla (top-right side) are useful points of integration. Example: <a href="https://bugs.kde.org/show_bug.cgi?id=358636">link</a>, see the screenshot below.

Unfortunately the opposite way does not work yet: Phabricator won't display bugzilla links. But if you use the "BUG:____" lines in your git commit messages they at least appear in summaries of Differential revisions (<a href="https://phabricator.kde.org/D890">example</a>).

Thanks to <a href="Ben Cooksley">Ben</a> who tirelessly enabled the links.

PS: These weeks mark end of my 13rd year in KDE!

<img src="http://kexi-project.org/pics/blog/2016/b-k-o-phab.png">