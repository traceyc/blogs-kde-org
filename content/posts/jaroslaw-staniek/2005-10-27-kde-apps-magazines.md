---
title:   "KDE apps in magazines?"
date:    2005-10-27
authors:
  - jaroslaw staniek
slug:    kde-apps-magazines
---
I've looked at an issue of <a href="http://www.chip.pl">CHIP</a> magazine (yes, it helps me to be up-to-date with average user's PoV). CHIP is currently not the only magazine offering "Open Source" section. Among other stuff there are contests for selecting a group of best general-purpose FOSS apps.

OK now, nice for us, right? 
<!--break-->

But after a minute, I noticed there are 0 (zero) KDE apps on their list. Guess why? 

Most of these the apps mentioned in CHIP have win32 port. Thus, openOffice.org, The GIMP and Mozilla/Mozilla Firefox and a few PHP-based content management systems were among the winners.

BTW, it's not CHIP editors' conspiracy. Most contests are based on readers' votes... As you may imagine they could have never know there are apps available like K3B (there was even no such section) or Amarok (yes, they have chosen XMMS because it's still Winamp-like by default). 

So while KDE rules in every issue LinuxMagazine, it's not (yet) the case for Joe Average's magazines.

BTW, there is new kde-windows@kde.org <a href="https://mail.kde.org/mailman/listinfo/kde-windows">mailing list</a>. Above <a href="http://mail.kde.org/pipermail/kde-windows/2005-October/000053.html">topic</a> is discussed there too.