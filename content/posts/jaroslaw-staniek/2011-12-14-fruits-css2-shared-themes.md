---
title:   "Fruits of CSS2: Shared Themes"
date:    2011-12-14
authors:
  - jaroslaw staniek
slug:    fruits-css2-shared-themes
---
(this is a continuation of the Fruits of Calligra Suite Sprint #2 series)

You don't need to become a seasoned web developer to know the value of styling documents with the <a href="http://en.wikipedia.org/wiki/Cascading_Style_Sheets">CSS</a>. This Sunday the technology turns 15 since its initial release.

We describe <a href="http://en.wikipedia.org/wiki/Presentation_semantics">presentation semantics</a> separately of document (or data in general) and this makes us feel empowered as professionals. More often than not when we look over the shoulder of an inexperienced user who clicks <b>BOLD</b>, <i>ITALIC</i> in an office suite, playing with font size combo box and what not, we feel overwhelming urge to say "wait, you can do this so much easier".

And then when we start spreading the <b>document styles</b> religion, you know what? That is not thought-out.

Read on to see why.
<!--break-->
The reason is both as simple as unnoticed by many. For years we have been operating in documents world without any equivalent of CSS in major office apps. (Sure, some of you would find some rare or forgotten counter-examples and challenge me but I said "major office apps" and I don't mean DTP apps)

Basically NONE of the apps employed Document Themes, as I call CSS equivalent in documents, abstraction over document styles. Until 2005. 

Then it all started for many casual users. In January 2005 <a href="http://www.appleinsider.com/articles/05/01/11/macworld_apple_unveils_iwork_05_productivity_suite.html">Apple has started its iWork suite</a>:

<i>[..] Both Pages and Keynote 2 take full advantage of the advanced typography and graphics engine of Mac OS X, are seamlessly integrated with iLife '05. They include a collection of themes and templates that make it easy to produce stunning results in minutes.</i>

<img src="http://kexi-project.org/pics/blog/docthemes/numbers-theme.png"><br/><b>iWork's spreadsheet with a theme applied</b>

Shortly after (haha...), in 2007 Document Themes and Quick Styles <a href="http://blogs.msdn.com/b/eric_rockey/archive/2006/05/10/594139.aspx">appeared in a new MS Office</a>. Quoting after this <a href="http://en.wikipedia.org/wiki/Microsoft_Office_2007#Themes_and_Quick_Styles">good and generic Wikipedia description</a>:

<i>"Microsoft Office 2007 places more emphasis on Document Themes and Quick Styles. The Document Theme defines the colors, fonts and graphic effects for a document. Almost everything that can be inserted into a document is automatically styled to match the overall document theme creating a consistent document design." [..]</i>

<img src="http://kexi-project.org/pics/blog/docthemes/theme-colors-mso.png"><br/><b>Selectors for theme colors in MSO 2007 separated from standard colors (two different themes)</b>

<img src="http://kexi-project.org/pics/blog/docthemes/theme-fonts-mso.png"><br/><b>Theme-aware font picker in MSO 2007 separated from ordinary font list</b>

It is good to know that Themes are more than just sets of styles, possibly saved in a template. Quoting after <a href="http://blogs.office.com/b/microsoft-word/archive/2008/10/28/behind-the-curtain-styles-doc-defaults-style-sets-and-themes.aspx">office.com blog</a>:

<i>[..] "Themes are simply a new level of abstraction available to Styles. Specifically, Themes let you to use variables instead of constants when you specify the properties within a Style. For example, let's say I'm creating a new Style called "Tea," and I want to specify the color property.</i>

<i>With the introduction of Themes I have two choices:
<ul>
<li><i>make the color absolute (ex. green)</i></li>
<li><i>make the color relative (ex. Accent 3).</i></li>
</ul></i>

<i>If I choose the former, Tea's color won't be affected when I change the Theme of my document. If I choose the later, Tea's color will be affected when I change the Theme of my document. This comes in handy when you want to make a compelling and consistent change to the overall look and feel of your document in a few clicks."
</i>

<img src="http://kexi-project.org/pics/blog/docthemes/theme-doc-mso.png"><br/><b>Document themes selector in MSO 2010</b>

Yes, most of the times you want these features in any app that create rich content. As in many other aspects, good defaults make our work easier. Back in 2007 MS added Document Themes support to seven its apps in one go, including the database and project management apps. (They also added handy <a href="http://en.wikipedia.org/wiki/Microsoft_Office_2007#Themes_and_Quick_Styles">Quick Styles</a> but this is another story)

To know how widely a document theme can be applied across applications, imagine a theme for table with border styles, cell colors and padding. The same theme can be used by a spreadsheet document and tables presentation and finally in a wordprocessor's table. They would be thus visually connected. We're getting close to DTP world but without alienating average users.


<b>What's in Open Document Format</b>
To my knowledge ODF does not codify sets of connected styles what would be a building block for document themes. The only related term is styles inheritance which is used in single families, e.g. Heading styles 1, 2, 3, and so on can inherit from common Heading style, for example to share common font. But for example there is nothing that restricts or suggests number of fonts used in a document or template.

In no way this is critics of ODF. Perhaps themes would appear in next generation of ODF thanks to demand from the community and community involvement. By then applications have to handle and define themes on their own. It would be good to have single definition, probably not the one hard-coded into MSOOXML formats but at least similarly functional. I will be polite enough saying that the way how MSOOXML defines themes does not look like how standard should look like.


<b>But we have templates!111!!!</b>
While document templates are medium for storing styles, they do not replace document themes. Using template-like technology for styling to replace the Internet's HTML/CSS technology would be like deploying pre-formated HTML documents with placeholders for user content without style/layout/content separation. Document templates are very useful when they define content, let it be invoice, formal letter or any document with predefined structure.

Usage of document templates appears to be at least problematic when we need to start with empty documents because the only explicit value of template documents carrying no content are styles embedded inside. But documents do NOT inherit definitions from a template. Documents are just static copies of one selected template or another.

Even existence of <a href="http://en.wikipedia.org/wiki/OpenDocument_technical_specification#Templates>extra filename extensions</a> (ott is a template for odt, etc.) is a mere naming sugar used mostly by OS shells to make easy distinction between opening document and copying document (creating document from template). And finally, changing style definitions in a template will only change styles of FUTURE documents that will be based on that template. These semantics remember the release date of the Terminator 2 movie and is very much unlike CSS.

Users are confronted with a case when some styles reside in one document and other set of styles is a part of another document or template. So once again there is no real equivalent of CSS. Through the years various Style Organizer dialogs have been implemented in office suites only adding complexity to this kind of software.

Templates cannot even emulate most crucial features of themes: their dynamism caused by close separation of style definition from content definition. Good typography in documents can be achieved by having set of styles. But as noted in description of the ODF limitation, only loose set of styles is available with minor inheritance features.


<b>What's in FOSS?</b>
LibreOffice still does not support themes and I have not heard about any plans. Funny enough there is Tools-&gt;Gallery that looks like Themes feature but it's a gallery of bitmap items for use in bullets or elements of HTML 2 (!) web pages. Clicking on graphic background item inserts a floating bitmap instead of formatting background for a spreadsheet's cells.

Calligra apps and AbiWord or Gnumeric so far follow what comes with ODF, what means zero theming functionality. Basically all the apps offer is a style selector and editor presenting styles defined in the document (and copied from a template). While standardized around ODF, this functionally matches features of the closed-source competition from the pre-2005 era.

For many years now charts in LibreOffice or in Calligra have implied some sort of color themes for data series. But I believe even this is largely defined at application level though and just copies a functionality of older office apps.

<b>What now?</b>
So what to do? I was aware of many massive TODO years ago but just now I think it's good opportunity to push for making KDE software produce professionally-looking documents. And any content in general. While we can see so much effort put into the GUI elements, content is not less important. It is what users creates and so often the reason he fires up an app. GUI, the shell, is the envelope so we cannot stop just at it.

At the Calligra Sprint #2 I presented the vision and reasoning so in theory things should go smoother now even if a lot of effort is put into the Calligra Engine, a machinery running on the backstage to make your document viewing and editing fly. Wish us luck implementing global Calligra Themes and apply them anywhere we can. 

Many mockups could be prepared but I only found time for three, showing the vision in practice (actually for Kexi these are real screenshots):

<a href="http://kexi-project.org/pics/blog/docthemes/kexi-theme-design.png"><img src="http://kexi-project.org/pics/blog/docthemes/kexi-theme-design_sm.png"></a><br/><b>Reporting in Kexi with theme applied (visible in the font faces and size and in the color of the page header)</b>

<a href="http://kexi-project.org/pics/blog/docthemes/kexi-theme-view.png"><img src="http://kexi-project.org/pics/blog/docthemes/kexi-theme-view_sm.png"></a><br/><b>Resulting report with data populated, ready for printing</b>

<a href="http://kexi-project.org/pics/blog/docthemes/words-theme-mockup.png"><img src="http://kexi-project.org/pics/blog/docthemes/words-theme-mockup_sm.png"></a><br/><b>The same document theme shared by Calligra Words, document heading shared the same style the one seen in the Kexi report</b>


<b>Summing up, proposed ideas and requirements for the Calligra Suite:</b>

<ul>
<li>Themes define every visual aspect of document
<li>Goal: enabling users to work with styles via themes
    <ul>
    <li>Default theme could be selectable globally in Calligra
    <li>In some aspects theme can define GUI elements that are not well defined by QStyle/KStyle, e.g. spreadsheet grid and cell styles (Calligra Tables) and database tables grid (Kexi) can have common look defined by a theme
    </ul>
<li>Support for themes in:
   <ul>
   <li>documents (odt, ods, odp)
   <li>reports of any types, especially reports used in Kexi and Plan
   <li>Kexi views, besides reports also forms and tables. This means that also form widgets could be themable and thus receive more web-like look if this is expected by users.
   <li>graphical elements like frames, diagrams (Flow)
   </ul>
<li>Themes affect any properties of ODF styles - character, paragraph, table styles (fonts, colors, margins, padding)
<li>Themes are applicable to existing styles without manual tweaking by users
<li>Themes are stored separately of documents is dedicated format and versioned. Only for compatibility with other office suites, styles are generated into the document but references to original themes are kept in custom XML tags. This is allowed by ODF.
<li>Thus, themes may be shared through the KDE Get Hot New Stuff and/org other means
<li>To design a theme one should not need to edit XML representation, it should be possible using dedicated theme editor.
</ul>

<a href="http://calligra.org"><img src="http://kexi-project.org/pics/ads/calligra/calligra-logo-150.png"></a>