---
title:   "Too many tasks before the deadline"
date:    2006-09-06
authors:
  - jaroslaw staniek
slug:    too-many-tasks-deadline
---
This time I am using the blog to ask for some help. 

I need someone who is able to convert HTML documentation about form desinger (one really important chapter from user's POV, about 35KB) to docbook for Kexi. Not much time left before the <a href="http://developer.kde.org/development-versions/koffice-1.6-release-plan.html">documentation freeze</a> (friday evening, 8th sept). 

<!--break-->

The file is uploaded <a href="http://websvn.kde.org/*checkout*/branches/koffice/1.6/koffice/kexi/doc/handbook/html.tmp/05_04_00_form_designing.html">here</a>. There are no screenshots, so if you want to see them to get the idea about the context, take a look at the <a href="http://www.kexi.pl/doc/pl/05_04_00_form_designing.html">polish original</a>.

Please contact me on #koffice or #kexi or mail to js at iidea.pl for details.

Thanks for reading.

<i>Update: Doc freeze is pushed to Wednesday, 13. And thanks to Aron Stansvik who managed to quickly so large 35KB file, and did it very well. The offer is of course still open for the future.</i>
