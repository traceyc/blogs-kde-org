---
title:   "Mini quotes"
date:    2009-12-13
authors:
  - jaroslaw staniek
slug:    mini-quotes
---
I have self-backed policy of not mentioning competition if not really necessary, let it be G or M. So as a minimal effort I just quote these carefully selected bits (bias included!) instead of commenting the recent <a href="http://www.pwnage.ca/?p=433">story</a>:

<i><b>
[..] I think there is no reason to work on another package which is equivalent to Qt. If you want something like Qt, use Qt.
</b></i>
&nbsp;&nbsp;--<a href="http://www.linuxtoday.com/news_story.php3?ltsn=2000-09-05-001-21-OP-LF-KE">Richard Stallman, September 5, 2000</a> about then-disappearing need for creating Qt replacvement (aka project Harmony) after GPLing Qt
<!--break-->
<i><b>
[..] We evaluated writing a free Qt replacement, but reimplementing an API would most likely result in less efficient software and would have taken too long to implement. GNUstep, Wine and LessTif were other projects that had attempted to <u>reimplement a proprietary API</u> and just had a limited success after a long development history.</b></i>
&nbsp;&nbsp;--<a href="http://primates.ximian.com/~miguel/gnome-history.html">Miguel de Icaza, ~2000</a> mentioning that the story of reimplementing proprietary APIs in FOSS did not start just with appearance of mono & moonlight

<i><b>As much as I love Gtk+ and the Gnome desktop, our contributions for our desktop applications and for Gtk+ come mostly from from folks developing on Linux for Linux (with a handful of exceptions). And we are a small fraction of desktop developers.</b></i>

<b><i>In my mind what is interesting to me about building applications with Silverlight is that <u>we can create an ecosystem of free software applications that run on all three major platforms: Windows, Linux and MacOS</u>.</b></i>
&nbsp;&nbsp;--<a href="http://tirania.org/blog/archive/2009/Nov-12-2.html">Miguel de Icaza, November 12, 2009</a> on "next generation API for building GUI applications"

I think we can congratulate Trolls that the Qt/Windows, Qt/*nix and Qt/MacOS products and communities are vital and have been for so many years, and also congratulate the KDE community as a whole that the KDE/Windows and KDE/Mac projects are well established.

<i><b>
[..] The most important piece of news from last week's PDC was Microsoft's decision to turn Silverlight into the <u>universal platform for building cross platform applications</u>.
</b></i>
&nbsp;&nbsp;--<a href="http://tirania.org/blog/archive/2009/Nov-23.html">Miguel de Icaza, November 23, 2009</a> after visiting MSPDC09


He is apparently quite excited with capatibilities of .net+silverlight as "Universal GUI toolkit", and advertising it for needs are already covered by long available Free Software offerings like Qt and KDElibs.

The story about "replacements" proposed by Miguel does not end with replacing GUI toolkits or C++/Java with C#, there are his nice words about replacements for the <a href="http://tirania.org/blog/archive/2008/Apr-02.html">OpenDocument format</a> and <a href="http://tirania.org/blog/archive/2009/Sep-10.html">Sourceforge/FSF/whatever</a> - all this is syndicated on the Planet G and that is, I feel, exactly what's been <a href="http://www.pwnage.ca/?p=433">criticized</a> by RMS.

Update (Dec 16): aside from the technical concerns (portability), this is more or less how the mono/moonlight business plan appears to _me_:

<a href="http://dilbert.com/strips/comic/2009-12-09/" title="Dilbert.com"><img src="http://dilbert.com/dyn/str_strip/000000000/00000000/0000000/000000/70000/5000/900/75990/75990.strip.gif" border="0" alt="Dilbert.com" /></a>