---
title:   "Stability update for Kexi 2.2.2"
date:    2010-09-20
authors:
  - jaroslaw staniek
slug:    stability-update-kexi-222
---
It is not clear if there will be another update within KOffice (2.2.3) released in 2.2 series, so for convenience of distributors, following recommended patch fixes stability recently identified issue in the default Kexi database engine:

<a href="http://kexi-project.org/wiki/wikiview/index.php@Kexi2.2_Patches.html#sqlite_stability">link</a>

I use the blog entry because a 5 or more reports have been filed for the same bug. Thanks for each of them.

You'll find MS Access fix as a bonus too.

To help, please simply tell maintainer of your distribution, they do great work but just could have missed the patch. Without the cooperation, distributed development won't work, really.

Thanks!

PS: let me know if you we're expecting source code tarball instead
PS2: yes, single 'make tarball' could solve the issue, help welcome
