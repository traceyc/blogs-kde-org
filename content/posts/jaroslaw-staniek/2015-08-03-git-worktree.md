---
title:   "git-worktree"
date:    2015-08-03
authors:
  - jaroslaw staniek
slug:    git-worktree
---
[Skip if you're not a git user]

You will often want to have more than one build environment in parallel, for example if you want to work on stable and various feature branches. Recommended way so far was to use a git-new-workdir script. This solution saves space (e.g. ~300MiB for a calligra branch instead of ~1200MiB) and time. 

A cool update from the git guys. If you are user of the <i>git-new-workdir</a> as advertised <a href="https://community.kde.org/Calligra/Building/3#Working_with_Multiple_Versions">here</a>, git 2.5.0 allows to replace it with a new <a href="https://git-scm.com/docs/git-worktree">git-worktree</a> built-in command.

It apparently works on Windows too because:

<i>"A replacement for contrib/workdir/git-new-workdir does not rely on symbolic links and make sharing of objects and refs safer by making the borrowee and borrowers aware of each other. Consider this as still an experimental feature; its UI is still likely to change." (<a href="https://github.com/git/git/blob/22aca1b3ac10af7188dccf033b44a36926f04d4b/Documentation/RelNotes/2.5.0.txt#L25-L27">link</a></i>)

Edit: of course 2.5.0 isn't the officially supported version so it's not always available for you in a binary form. There's previous solution for Windows: <a href="https://github.com/joero74/git-new-workdir">https://github.com/joero74/git-new-workdir</a>.

<img src="https://farm8.staticflickr.com/7150/6539567727_3f2144d345_b.jpg" width="1024" height="768" alt="Expo en Witte de Wit"><br/><a href="https://www.flickr.com/photos/anagangulo/6539567727">CC BY-SA 2.0 anagangulo</a>
