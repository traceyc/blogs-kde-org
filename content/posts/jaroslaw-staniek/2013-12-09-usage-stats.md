---
title:   "Usage Stats"
date:    2013-12-09
authors:
  - jaroslaw staniek
slug:    usage-stats
---
<i>Kexi usage statistics</i> is an experiment started two years along with Kexi 2.4. The information helps to make certain decisions related to design and direction of the project. Today I'd like to show you first results.

On every startup of Kexi, background job sends anonymous information to project's <a href="http://kexi-project.org">kexi-project.org</a> server. User's privacy is a top priority here; users need to <u>opt-in</u> by enabling <i>Share usage info</i> option at Kexi startup. For detailed idea and plans behind the effort, click the "?" mark in the startup window, here's <a href="http://kexi-project.org/docs/contribution_help.html">a copy of the text</a>. Naturally, the raw data is never presented online.

<a href="http://kexi-project.org/pics/2.8/kexi-share-usage-2.8.png"><img src="http://kexi-project.org/pics/2.8/kexi-share-usage-2.8.png" width="300"></a>

Currently there are four groups of information that can be shared. Only first group is required, others are recommended; more you share, you became better contributor, as in gamification ;)
<ul>
<li>Basic info. Enabling this will include your usage of Kexi in the global statistics. It contains anonymous identification number (must-have for tracking number of unique users), Kexi version, KDE version, operating system name.
<li>System and hardware info. Enabling this helps the Kexi Project to identify typical operating systems and hardware that should be supported. It contains Linux operating system distributor, description, release, version, and machine type.
<li>Screen information. Enabling this helps Kexi Project to better know real display parameters used with Kexi, so it can be better optimized. 
<li>Regional settings. Enabling this helps Kexi Project to know how wide support for regional settings should be. It contains user's language, country, date and time formats, and usage of right-to-left layout.
</ul>

<br><br>

<a href="http://kexi-project.org/pics/2.8/kexi_usage-2012-2013.png"><img src="http://kexi-project.org/pics/2.8/kexi_usage-2012-2013sm.png"></a>

<br><br>

It remains to be seen what would the results look like for the rest of Calligra and for KDE apps in general. Naturally, the data only include people that agreed to support the user feedback program, so real data would look completely different. If you have questions or would like to join the experiment with other apps please leave a word in the comments below. And if you use Kexi, and have not the enabled the feedback feature, please consider taking this easy step.
