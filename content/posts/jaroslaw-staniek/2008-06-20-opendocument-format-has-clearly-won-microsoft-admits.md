---
title:   "\"OpenDocument Format Has Clearly Won\", Microsoft admits"
date:    2008-06-20
authors:
  - jaroslaw staniek
slug:    opendocument-format-has-clearly-won-microsoft-admits
---
<a href="http://www.infoworld.com/article/08/06/19/Red_Hat_Summit_panel_Who_won_OOXML_battle_1.html">Microsoft</a>: 

<tt>"ODF has clearly won. We sell software for a living. The ability to implement ODF in the middle of our ship cycle was just not possible. We couldn't do that during the release of Office 2007. We're looking forward and committed to doing more than [ODF-to-OOXML] translators."</tt>

It looks like honest words, so ... welcome to market based on competition, guys.