---
title:   "Qt for Android"
date:    2009-09-30
authors:
  - jaroslaw staniek
slug:    qt-android
---
Shocked by the title? So I am. 

Would you like to see Qt supported on this platform? Just two days ago the answer was like "But it's close to impossible".
Now with <a href="http://android-developers.blogspot.com/2009/09/now-available-android-16-ndk.html">NDK 1.6</a> the "little robot" OS opens more to C/C++ native code. I am eager to read some analysis on the topic.

Would that be too big hybrid, considering existence of Qt/S60? Someone can complain about replicating features already existing on the platform (graphics or networking?), but since version 4 Qt is more modular: splitted to libraries and still can be configured to meet certain needs.
