---
title:   "Locus OS: Failed MS Plasma"
date:    2010-02-11
authors:
  - jaroslaw staniek
slug:    locus-os-failed-ms-plasma
---
No, <a href="http://www.vimeo.com/9281370">Locus OS</a> concept design, has apparently no association with MS.

The author claims:
<i><b>"This interface was designed before iPhone 3.0, Palm Pre, Android etc, making the ideas original at the time :)"</b></i>

Is the last sentence true at all?