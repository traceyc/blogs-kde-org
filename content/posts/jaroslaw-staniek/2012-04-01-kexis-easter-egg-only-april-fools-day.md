---
title:   "Kexi's Easter Egg only on April Fools' Day"
date:    2012-04-01
authors:
  - jaroslaw staniek
slug:    kexis-easter-egg-only-april-fools-day
---
And Now for Something Completely Different.
Kexi's Easter Egg just appeared and quite magically it's only available on April Fools' Day ;)

To try, run any beta of Kexi 2.4 or 2.5 alpha. Then close the app, run again and look closer. Enjoy!

It works (I hope - untested!) thanks to the remote updates for the user feedback/news module[*] that we managed to fit within the Calligra 2.4 release plan. More on that later.


[*] BTW, you won't find any traces in the source code... 
