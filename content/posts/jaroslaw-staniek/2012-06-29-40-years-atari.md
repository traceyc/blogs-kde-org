---
title:   "40 years of Atari"
date:    2012-06-29
authors:
  - jaroslaw staniek
slug:    40-years-atari
---
This week we celebrated 40 years of Atari, once #1 computer brand.

</flame-mode>

For football and/or Atari fans - on time, some of us think that Mario Balotelli celebrated the anniversary too after scoring during the Germany vs Italy Euro 2012 semi-final match:

<img src="http://farm9.staticflickr.com/8008/7465037514_a0589a1e6b_o.jpg">

Anyway, have a good time at Akademy!