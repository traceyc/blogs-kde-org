---
title:   "Beyond Bug Tracking"
date:    2012-10-10
authors:
  - jaroslaw staniek
slug:    beyond-bug-tracking
---
During my short summer holidays I had the opportunity to perform overdue bug triage for Kexi. 
Funny enough, that alone dominated (by numbers) the Commit-Digest's Bug Killers list for <a href="http://commit-digest.org/issues/2012-08-12/">12th August</a> :) Some bug reports were really dated, 2006 or so, anyway the Calligra 2.6 series is a good occasion for such cleanup.

Now "only" 130 bugs of 426 are opened. Below I'd like to discuss some software development workflow-related topics. I think it may be most interesting for people new to the KDE project or those who so far only contribute sporadically and would like to do more.

I am curious how do you look at the bugs.kde.org KDE Bugtracking System. Is it "just" a bug tracking site for you or a project management tool? There were numerous blog entries on the topic but I will share some thoughts on the Project Management aspect.

One can argue that in projects consisted of volunteers we shall not push for project management tools or procedures since everything the project members do is out of their good will. This applies to actual work and commitments or schedules. Volunteers' time is so much limited, why to risk another overhead and boring tasks?

But I would say conversely. Basics of project management can improve quality of the communication by reducing risk of unnecessary work, duplication of efforts or even conflicts. Every project with limited resources (read: every project on this planet) where planning is involved benefits from prioritization of tasks. Similarly, relationships with the customer^w FOSS users require that we have realistic and frequent releases and we plan them in the open, inclusive environment.

<br><b>Managing a Project with bugs.kde.org</b>
So in the <a href="https://bugs.kde.org/buglist.cgi?cmdtype=runnamed&namedcmd=all%20unresolved%20kexi%20bugs&list_id=237460">Kexi bug list</a>, out of all the bugs reported, 58 are mine. This is not because I am the only user :) but because I also employ so-called bugs.kde.org's project management features (even while I really like more complex systems such as <a href="http://en.wikipedia.org/wiki/JIRA">JIRA</a>). In particular order: 

<ul>
<li>I use the <a href="https://bugs.kde.org/editcomponents.cgi">Edit Components</a> available within the <a href="https://bugs.kde.org/admin.cgi">administration page</a>. Once I created them, there's rarely a need to look here. One thing worth remembering is that users reporting bugs will have hard time using the components list for your project or even will ignore it if you have too many detailed or badly named components.
<li>I also use the <a href="https://bugs.kde.org/editversions.cgi">Edit Versions</a> from the same administration page. To keep it always updated it's a good idea to add all possible releases for given minor version in one go. Just remember that precise versions improve bug tracking. Also, notation should follow a clear scheme, instead of, say, Ubuntu-like codenames. For example Kexi series 2.5.x defines: 2.5 alpha1, 2.5 beta1, 2.5 rc1, 2.5.0, 2.5.1, 2.5.2, 2.5.3.
<li><b>Version Fixed In</b> is a custom field that our KDE admins enabled for a purpose, and I use it a lot. It gives a clear hint about first release in which the wish or bug fix has been implemented. I use <b>FIXED-IN</b> <a href="http://community.kde.org/Sysadmin/GitKdeOrgManual#Commit_hook_keywords">git commit hook keyword</a> in my commit message, e.g. <br>
<code>FIXED-IN: 2.5.3</code><br><br>

To make FIXED-IN work from a git commit, BUG:<number> line should be also included in the commit message. It's also a good habit to make sure that version matches one of the versions specified in the <i>Edit Versions</i> page as mentioned above. 
<li>Out of all fields in bugs.kde.org BUG entries <b>Target Milestone</b> is in my opinion the most related to project management. I maintain the milestones in the <a href="https://bugs.kde.org/editmilestones.cgi">Edit Milestones</a> for the project through the <a href="https://bugs.kde.org/admin.cgi">administration page</a>. I use Major.Minor notation, e.g. 2.5, 2.6, what reflects the typical approach to planning releases with feature freezes. Milestones are indication of our plans regarding the bug or feature and thus some way to keep clear communication with our fellow developers, distributors and users. It's only valuable if we treat it seriously, so declarations shall be close to reality and up to date ;)
<br/>
While we're at it, I found it handy to add the Target Milestone field (as TargetM) to the Bug List table using the <i>Edit search</b> feature of bugs.kde.org:
</ul>
<a href="http://kexi-project.org/pics/blog/2012/feature-plan.png"><img src="http://kexi-project.org/pics/blog/2012/prj-management_sm.jpg"></a>

<br><b>Feature Planning & Tasks</b>
To make things simple I did not mention every single field of the bug entry. For example I do not use <i>task</i> value in the <i>Importance</i> field, just bugs and features (the latter are marked as wishlist). The reason why I do not use task value at all is that detailed description of tasks are currently impossible to maintain in bugs.kde.org - unlike other systems known to me (such as JIRA or Mantis) there is no way to edit the report description or comments. Contacting the administrator to make such edits for me does not seem reasonable. Moreover, the content is just plain text plus linear attachments.

To address the needs of more editing freedom, a simple rather minimal approach to Feature Plans for Calligra has been introduced within the KDE Community wikis. It's admittedly, slightly redundant to function of bugs.kde.org but the goal was to have usable tool as soon as possible, providing one “bird's-eye view". The page can be found at <a href="http://community.kde.org/Calligra/Schedules/Feature_Plan “>http://community.kde.org/Calligra/Schedules/Feature_Plan</a>.

Since Calligra is a family of apps, there is one table per app. For nontrivial tasks I often create separate complex wiki pages (<a href="http://community.kde.org/Kexi/KexiDB/User_data">example</a>), and a Wishlist entry in bugs.kde.org. A link to the page can be then inserted in the Wishlist's <i>URL</i> field, what results in two-way references between a task and description page. It all makes sense for nontrivial tasks that involve communication and extra documentation, analysis or planning.

Adding table rows in the wiki is simplified thanks to extensive use of wiki templates (but let's admit, still error-prone), e.g.:<br>
<code>{{CalligraDone|KexiDB - [[Kexi/KexiDB/User_data|Add support for user data storage]] - {{Wish|305074}}|jstaniek|2.6}}</code>

If you look closer, the tables have sortable columns, so one can group tasks by <i>status</i> (TODO, In Progress, Done), <i>description, optional git commit, contact person,</i> and <i>target release</i>. Formalized short component name (e.g. KexiDB) goes first in the Description, so the sorting works nicely.

Previously there was one Features page per minor release but it has moved to a single page, with extra <i>Target release</i> column, so tasks can be freely moved between release. The table can also help to craft change logs based on key changes in a bit more convenient way than the read-only git logs. 

<a href="http://kexi-project.org/pics/blog/2012/feature-plan.png"><img src="http://kexi-project.org/pics/blog/2012/feature-plan_sm.jpg"></a>

Should you have special bits of your workflow or ideas, please share them in the comments.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.calligra.org/"><img src="http://www.calligra.org/wp-content/themes/calligra/images/calligra-logo-200.png"></a>