---
title:   "Native Qt3-only version of KexiDB, and more"
date:    2008-08-22
authors:
  - jaroslaw staniek
slug:    native-qt3-only-version-kexidb-and-more
---
I've been enjoying the time (bikes!), meetings (absolute record # of attendees) and technology (n810!) at <a href="http://akademy2008.kde.org/">Akademy</a> since 9th August. And a young student from Poland, <a href="http://www.blogger.com/profile/07988895342942636490">Michał Bartecki</a> has been invited to come, what adds one to the number of the gfx individials around and made the KDE ping-pong team stronger. Kudos for the Team for such an unique event, for their time, sweat and tears :)

As the recent <a href="http://commit-digest.org/issues/2008-08-10/">KDE Commit-Digest</a> unveiled, it's visible that I've delivered <b>Qt3-only incarnation of KexiDB</b> to <a href="http://websvn.kde.org/branches/kexi/kexidb-qt3/">branches/kexi/kexidb-qt3/</a> in the KDE Subversion. KexiDB is a high-level database connectivity and creation library with database backend drivers for SQLite, MySQL and PostgreSQL. Its development within the Kexi project started in 2003.
<!--break-->
The rationale behind porting such quite an ancient code to equally ancient Qt version is that people, including me, use it in in-house projects, what also includes commercial developments. So, if you have Qt license, KexiDB is LGPL, so also perfectly available for you.

<a href="http://www.kexi-project.org/docs/svn-api/1.6/html/namespaceKexiDB.html">API docs for this version</a>. What differs in KexiDB-Qt3, is only how plugins are loaded (QLibrary::resolve() instead of KParts::ComponentFactory::createInstanceFromService<KexiDB::Driver>(), so the the public API remains unchanged.

PS1: Note that the build of MySQL and PostgreSQL drivers is disabled in drivers/driver.pro for now in SVN.

PS2: In related news, at Akademy I've finally created a KDE Subversion <a href="http://websvn.kde.org/branches/work/predicate/">branches/work/predicate/</a> branch for <b>Predicate library</b>. It is what previously was called KexiDB v2, developed with KOffice/Kexi v 2.0, and now is becoming Qt4-only general purpose library (the buildsystem is cmake). Apart from the technology, interesting thing is that the Predicate name was proposed by Aaron in Dublin Akademy and I've been keeping such a transition plan in my secret TODO notepad...