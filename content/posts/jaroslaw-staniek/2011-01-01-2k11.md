---
title:   "2k11"
date:    2011-01-01
authors:
  - jaroslaw staniek
slug:    2k11
---
Besides all possible conventional wishes there is something I wish you for 2011: 

<i><b>"Let any good code you commit come back to you redoubled"</b></i>

A twisted variant of <a href="http://en.wikipedia.org/wiki/Boomerang_effect">the karm^w boomerang theory</a> ;)
