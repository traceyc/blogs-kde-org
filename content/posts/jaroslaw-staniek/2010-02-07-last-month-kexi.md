---
title:   "Last month in Kexi"
date:    2010-02-07
authors:
  - jaroslaw staniek
slug:    last-month-kexi
---
With 2010 we've started to employ <a href="http://identi.ca/kexi">identica</a> (then connected to <a href="http://twitter.com/kexi_project">Twitter</a> and <a href="http://www.facebook.com/pages/Kexi/218171903489">Facebook</a>) as an channel for our live changelog at the {power}user level. Here's the dump for the past ~30 days (oh I should have used an XSLT).

<ul>
<li>     We're replacing serialized QFont attrs with ODF equivalents in Kexi Reports file format; e.g. fo:font-family; it's extension of OpenRPT
</li>
<li>     Finally we're still embedding SQLite as many options are not set in distros, e.g. SECURE DELETE should be the default <a href="http://bit.ly/amZfJ3" class="external free" title="http://bit.ly/amZfJ3" rel="nofollow">http://bit.ly/amZfJ3</a>
</li>
</ul>
<!--break-->
<ul>
<li>     <strike>Kexi switched to using sqlite bundled with the operating system instead of using a fork. The newest stable version is always recommended.</strike>
</li>
<li>     Sidebars behave now as expected: when collapsed, vertical buttons appear like in Kate. It's now natural to put widgets palette as a sidebar.
</li>
<li>     An extra ko dev has taken an interest in the report backend used in kexi (koreport), working to making it usable in kplato, a win allround!
</li>
<li>     We have line style combo box in the Property Editor. Today high precision point type also added too. Good for accurate printouts (reports).
</li>
<li>     Just ported color selector for the property editor. Now we're defaulting to KDE's Oxygen palette instead of the old school VGA colors.
</li>
<li>     Exporting in action! <a href="http://www.piggz.co.uk/kexi/kexi-export-1.ogv" class="external free" title="http://www.piggz.co.uk/kexi/kexi-export-1.ogv" rel="nofollow">http://www.piggz.co.uk/kexi/kexi-export-1.ogv</a>
</li>
<li>     MS Access 2010 drops support for many formats: Paradox &lt;=7, Lotus 1-2-3, Access 1/2. But 64-bit ver not even planned. <a href="http://bit.ly/5xZZV7" class="external free" title="http://bit.ly/5xZZV7" rel="nofollow">http://bit.ly/5xZZV7</a>
</li>
<li>     Is zoomable table view reasonable idea for you? <a href="http://bit.ly/8AWp6c" class="external free" title="http://bit.ly/8AWp6c" rel="nofollow">http://bit.ly/8AWp6c</a> Added as todo just yesterday for Kexi 2.6 <a href="http://bit.ly/64C4jb" class="external free" title="http://bit.ly/64C4jb" rel="nofollow">http://bit.ly/64C4jb</a>
</li>
<li>     MS discovered SVG? <a href="http://bit.ly/76lrNr" class="external free" title="http://bit.ly/76lrNr" rel="nofollow">http://bit.ly/76lrNr</a> Kexi just switched to svg ns for XML tags in Kexi Reports format-took 2 hours! <a href="http://bit.ly/6uD7dy" class="external free" title="http://bit.ly/6uD7dy" rel="nofollow">http://bit.ly/6uD7dy</a>

</li>
<li>     All the most ugly painting glitches now fixed in Kexi Table View. So time-consuming but pays-off...
</li>
<li>     Some rendering issues in the Table View widget fixed now. Looks like the Table View will stay Qt3-based until Kexi 2.3 - porting takes time.
</li>
</ul>