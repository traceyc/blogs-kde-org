---
title:   "Kexi in June & July"
date:    2010-08-09
authors:
  - jaroslaw staniek
slug:    kexi-june-july
---
<ul>
<li>KOffice Developer Meeting (<a href="http://blogs.kde.org/node/4246">pictures</a>).</li>
<li><a href="http://akademy2010.kde.org">Akademy</a> time! <a href="http://kexi-project.org/pjs.html">One</a> Kexi dev was available for hugs^wdiscussion; chatted a bit with the ownCloud hackers about exposing Kexi databases though it, KDevelop guys about injecting Kexi database plugin in a form of KDevelop's "Database view" and mobile guys about possible options for Kexi Mobile.</li>
<li><a href="http://userbase.kde.org/Kexi">http://userbase.kde.org/Kexi</a> updated. New tutorial added: <a href="http://userbase.kde.org/Kexi/Tutorials/Reports/Kexi_Reports_for_Beginners">Kexi Reports for Beginners</a></li>
<li>Adam works on porting the remaining Q/K3 code in Kexi, starting with the Project Navigator. <a href="http://www.piggz.co.uk/kexi/ihasamodel1.png">Ported</a> now.</li>
<li>The works on Kexi 2.2 Handbook started within the KDE Userbase wiki: <a href="http://userbase.kde.org/Kexi/Handbook">http://userbase.kde.org/Kexi/Handbook</a><li>
<li>Kexi now supports multipage (large up to 64 KB) memo values when importing data from MS Access. And it's the only open source app doing it!</li>
<li>Some work on <a href="http://websvn.kde.org/branches/work/predicate/">Predicate</a>, database connectivity and creation library, a new iteration of KexiDB library developed within Kexi (Jarosław). Expect a more in August.</li>
</ul>
