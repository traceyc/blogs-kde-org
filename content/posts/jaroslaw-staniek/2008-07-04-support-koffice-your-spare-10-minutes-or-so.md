---
title:   "Support KOffice with your spare \"10 minutes\" or so"
date:    2008-07-04
authors:
  - jaroslaw staniek
slug:    support-koffice-your-spare-10-minutes-or-so
---
Hi!
We, at KOffice, are looking for volunteers to make screenshots for 'visual changelogs' of each release. It's really welcome especially alpha and beta stages. Someone would just interview the devs on time, asking about important features, make the screenshots and publish them. 

I am not asking for writing whole articles, but hey, it's welcome too.

Then we could avoid comical cases like the one in <a href="http://dot.kde.org/1215168947">the article</a> where GIMP is presented instead of Krita ;=)

That said, a habit like “every maintainer delivers (from whatever source) at least one screenshot of "his" app“ would be still nice. 

<img src="http://static.kdenews.org/jr/koffice-logo.png"  align="center"/>
<!--break-->