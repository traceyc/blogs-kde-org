---
title:   "SCI-FI: Better Qt slots and signals with Qt5?"
date:    2005-06-08
authors:
  - jaroslaw staniek
slug:    sci-fi-better-qt-slots-and-signals-qt5
---
One of interesting proposals published in <a href="http://aristeia.com/EC3E/TR1_info_frames.html">Technical Report 1</a>, a specification for new functionality being added to C++'s standard library, is the <a href="http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2002/n1402.html">Polymorphic Function Object Wrapper</a>. 

Having that, QObject can just keep a list of function objects, aka signals connected to a slot. Hmm, perhaps a new C++ feature set will be implemented in all major compilers before Qt5?