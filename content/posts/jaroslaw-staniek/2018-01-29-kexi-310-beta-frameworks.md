---
title:   "KEXI 3.1.0 Beta & Frameworks"
date:    2018-01-29
authors:
  - jaroslaw staniek
slug:    kexi-310-beta-frameworks
---
Today is the release day for KEXI 3.1.0 Beta & its frameworks: <a href="https://community.kde.org/Kexi/Releases#3.1.0_Beta_1">https://community.kde.org/Kexi/Releases#3.1.0_Beta_1</a>

Since version 3 it becomes KEXI not Kexi to suggest becoming a standalone app. It's standalone status includes being first-class app also outside of KDE Plasma. To make this real things such as useful yet simple file widget are developed or single click mode is really single click mode "even" on XFCE. Actually implementing optimal experience for Windows is quite similar to supporting XFCE.

KEXI Frameworks are now prepared for backward compatibility rules within the series >=3.1. So I would encourage to try KProperty if you need powerful property editing features in your app in place of tedious Qt list or tree views. There's KPropertyExample in the same repository. Then there's KDb if you actually expect more (something low or high-level) than QtSql, that is also need to create database or SQLite-based documents, what seems to be very popular container in our times. Then try KReport if you want escape from generating (ODF/HTML/whatever) documents "by hand", or QPainting them by hand, just to be able to print your application's data in a structured way with nice title, header, footer. Try KReportExample to see KReport in action with "a few lines of code" app.

Finally, try KEXI to create designs of reports mentioned above, design data-driven apps (lots of features are missing before you're able to desing, say, a JIRA like app but it IS coming), integrate data, perform some analysis (again, lots of these features are not shipped as stability was, again, the goal).

I trust stability makes KEXI and its frameworks pretty competitive already. The codebase was tested with Coverity, works with gcc, clang, msvc. Critical parts are autotested much more than in the 2.9 or 3.0 times. Much of the 220 improvements since 3.0.2 is stability, usability or API fixes.

We expect stable release in one month. And here's one request: if you're packager or know one, please send link to available 3.1  packages so we can eventually have a Download page. If you are able to create AppImage or Flatpak packages or work on Craft support for Windows, or test existing source or binary packages once they are published, <b><a href="https://community.kde.org/Kexi/Contact">we are looking for help</a></b>.

<a href="http://kexi-project.org/pics/3.1/3.1-collage.png"><img src="http://kexi-project.org/pics/3.1/3.1-collage-720.png"></a>