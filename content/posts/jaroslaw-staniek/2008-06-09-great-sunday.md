---
title:   "Great Sunday"
date:    2008-06-09
authors:
  - jaroslaw staniek
slug:    great-sunday
---
Great news: Robert Kubica (Poland, 23) has won <a href="http://uk.reuters.com/article/motorSportsNews/idUKL0850716720080608">his first F1 Grand Prix</a> (in Canada). Moreover, in the same time another Pole, <a href="http://en.wikipedia.org/wiki/Lukas_Podolski">Lukas Podolski</a>, <a href="http://uk.reuters.com/article/sportsNews/idUKL0967312220080609">scored two goals in Euro 2008 match</a>, the first one was assisted by <a href="http://en.wikipedia.org/wiki/Miroslav_Klose">Mirek Klose</a>.

Oh wait, both Podolski and Klose (plus <a href="http://en.wikipedia.org/wiki/Piotr_Trochowski">Trochowski</a>) represent Germany in Euro 2008... Anyone tricked? ;)
