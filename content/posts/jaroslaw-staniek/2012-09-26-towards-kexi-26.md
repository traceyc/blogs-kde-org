---
title:   "Towards Kexi 2.6"
date:    2012-09-26
authors:
  - jaroslaw staniek
slug:    towards-kexi-26
---
I hope that Kexi like all the Calligra software will benefit from having a big release every 3 or 4 months. This way more than ever users become co-authors.

<a href="http://kexi-project.org/pics/2.5/kexi-2.5-report-calculated-field.png"><img src="http://kexi-project.org/pics/2.5/kexi-2.5-report-calculated-field_sm.png" width="200"></a> <a href="http://kexi-project.org/pics/2.5/kexi-2.5-report-calculated-field-data.png"><img src="http://kexi-project.org/pics/2.5/kexi-2.5-report-calculated-field-data.png" width="190"></a>

<br><b>2.5 series</b>
First, it's worth mentioning the 2.5 series received many improvements in already released 2.5.1 and 2.5.2 versions.  Many users apparently found the forums (the link is now <a href="http://forum.kde.org/kexi">forum.kde.org/kexi</a>) more convenient than mailing lists; not only questions moved here but for actual improvement and some planning discussions. It's honour to work with people that not only devote their limited time to the project but also use Kexi for their daily tasks, yet they store and process their valuable data with it!

As always it's important to have updated software so I can only encourage to do that. If you distro is slow with updates - politely request it every time, that's their job as a part of the equation. Many distros catched up and Calligra is now pretty well supported. There is no reason to keep software in the waiting-room too long when every feature and fix counts.

<br><b>In <a href="http://www.calligra.org/news/calligra-2-5-1-released/">2.5.1</a> you can find:</b>
<ul>
<li>Workaround for visual glitch of Oxygen style for the Modern Menu widget in kexi (bug 305051)
<li>Fix crash on importing CSV files with more than 1024 columns (bug 304405) and with Mac line endings (bug 304329)
<li>Missing table kexi__parts is no longer a critical error in Kexi (this enables opening databases created e.g. by the future Words Bibliography tool)
<li>Fixed bug: New Kexi project file created in current directory instead of the selected one (bug 305163)
<li>Table View: Fixed misplaced ‘Date/Time data error’ validation popup in tableview (bug 282295)
<li>Table View: Show warning when invalid date values in tables and forms are rejected (bug 299867)
<li>Table View: Clicking validation popup editor is focus again allowing to correct the value
<li>Table View: Use new message widget for validation popups
<li>PostgreSQL support: Display cmake compile warning if libpqxx is not in version 3.x
<li>buildsystem: move checks for pgsql, mysql, tds, xbase dependencies to global area (bug 300871)
</ul>

<br><b>and in <a href="http://www.calligra.org/news/calligra-2-5-2-released/">2.5.2</a>:</b>
<ul>
<li>Fixed bug in Query Designer: removed SQL Editor’s history as it was not functional (bug 306145)
<li>Make string concatenation operator || work in Query Designer (bug 305793)
<li>Startup GUI: fetch startup UI definition from x.y.0 URL for any x.y.z version, not from x.y.z URL
<li>Buildsystem: make it possible to disable database drivers even if their dependencies are present and found (wish 305683)
</ul>

<br><b>In about two weeks you'll also get 2.5.3 with these goodies:</b>
<ul>
<li>Main Window: Fix crashes on Kexi closing (bug 299484)
<li>Main Window: for consistency, display names in tab bars, not captions (names are displayed in the Project Navigator too)
<li>Fix loading maximumListRows property of lookup field schema (noncritical because it is not yet used)
<li>Tables: Fix possible crash when saving changes to design of table used elsewhere (bug 306672)
<li>Fix missing "*" (All Columns) item in Query Designer, it was present in Kexi 2.2 and ealier (bug 306577)
<li>General: Force lower case letters for object identifiers (except for widget names); this adds consistency and fixes support for objects renamed to not-all-lowercase in earlier version of Kexi (bug 306523)
</ul>


<br><b>What's around the corner for 2.6?</b>

<img src="http://kexi-project.org/pics/2.6/kexi2.6.0-alpha.png"/><br><i>Kexi users benefit from the Modern UI that debuted in the 2.4 release.</i>


Ready:
<ul>
<li>Added support for user data storage (feature 305074), now used to remember column widths in tabular data view (feature 230994)
</ul>

There are really expected features, in progress:
<ul>
<li><a href="http://community.kde.org/Kexi/Migration#Add_support_for_appending_CSV_data_to_an_existing_table">Add option for appending imported CSV data to existing table</a> (wish 305505)
<li><a href="http://community.kde.org/Kexi/Plugins/Tables/Simple_alter_table">Add support for alter table's design without losing data</a> (wish 125253). This one <b>massive</b> addition deserves more explanation. It looks like after so many years I found a working solution (and energy to implement it). <br>To get idea what's it about, imagine case when user changed data types for some columns, changed columns' order, removed some of them and also added. Then after all it pressed the Save button. You have to convert data from the original table to the new format. Currently no single app (or even command line tool) I know is able to do that for SQLite 3 database engines (including the nice <a href="http://sqliteman.com/">Sqliteman</a> app). Moreover, even server engines such as PostgreSQL and MySQL can lead to "data loss" (not random loss but one driven by user's mistake, e.g. if you change to integer from text type, conversion isn't automatic). Since Kexi is <b>not a database admin tool</b> but more a <b>rapid database-oriented application development platform</b>, it takes care about internals and usability - much more than database admin tools aimed at professionals.<br>We're in the middle of the implementation; for now Kexi is able to alter table relationships without data loss and core of the system works. It's really convenient!
</ul>


Other feature I hope will appear:
<ul>
<li>Add support for table backups (wish 306270). The backups can be automated or triggered by users. One motivation for this feature is to have an undo option when the <i>alter table design</i> action goes wrong. Yet, we're getting a new feature that is not even available in established software like MS Access. No idea about other competitors.
</ul>

Long-term plans always accessible on <a href="http://community.kde.org/Calligra/Schedules/Feature_Plan#Kexi">community.kde.org</a>.


<br><b>Manpower</b>
This year another talented developer joined. Please welcome Oleg Kukharchuk from Belarus! 
<br><img src="http://kexi-project.org/pics/2.5/oleg.png"/>

He has contributed with many things already, except fixes and porting these are: several new form widgets and full screen mode of Kexi 2.5. The "append imported CSV data" feature is his current interest.

This shows how quickly one can be introduced to Kexi thanks to the great ecosystem and community of Calligra and KDE. So if you wanted to work on something big (150K+ lines of code, we have plans for tens of man-years!), <a href="http://community.kde.org/Kexi/Getting_Started">do not hesitate to contact us</a>.

<br><b>Windows?</b>
People ask about Windows version too. This large user base is waiting so long for new Kexi. What to say? Other than "I have limited time resources", I also switched to Linux-only development machine so it would. <b>If you have a spare but legal copy of 7 let me know, so I would run it in a virtual machine. </b>

<b>MS Office 2010 or newer license could be a good donation too</b> (a pro version which includes Access) since we're on working importing features and have to be up to date with the outside world. I think it's worth the effort too -- Kexi is already known as one of the best possible options for importing data from MS Access files, still one of the most closed and legacy formats around. If you don't care, you can still try the CSV importer - it often beats what you can achieve with spreadsheets import functions. I have heard about users that installed Kexi just for these features and then they started to regularly use the whole app.

<br><b>kexi-project.org</b>
In other news, <a href="http://www.jowenn.net">Joseph Wenninger</a>, one of the original Kexi contributors transferred the domain name kexi-project.org to the <a href="http://ev.kde.org/">KDE organization</a>. I'd like to thank Joseph for sponsoring this domain for years and for his contributions. The is first step towards integration of the web site with the KDE infrastructure. At this moment we're looking for persons interested in contributing to this web site, so if you're the one please drop us a note.