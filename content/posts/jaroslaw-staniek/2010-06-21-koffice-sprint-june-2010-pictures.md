---
title:   "KOffice Sprint June 2010 in Pictures"
date:    2010-06-21
authors:
  - jaroslaw staniek
slug:    koffice-sprint-june-2010-pictures
---
The pictures say it all. If not, see <a href="http://www.koffice.org/news/last-week-in-koffice-week-23-2/">here</a> and <a href="http://slangkamp.wordpress.com/2010/06/20/koffice-mid-2010-meeting/">here</a>.

<a href="http://picasaweb.google.com/jstaniek/KOfficeSprintMid2010?authkey=Gv1sRgCISLs4ST1fm9IQ" title="KOffice Sprint Mid 2010"><img src="http://lh3.ggpht.com/_FBlhhjjfQMQ/TB6iIUiDaRI/AAAAAAAAHrc/z77_VGl0F10/s512/koffice_sprint_mid_2010.jpg"/></a>


As always all that would not happen without organizers: Alexandra and Inge!