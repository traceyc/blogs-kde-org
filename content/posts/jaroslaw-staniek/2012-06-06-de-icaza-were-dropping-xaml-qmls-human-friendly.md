---
title:   "de Icaza: we're dropping XAML; QML's human friendly"
date:    2012-06-06
authors:
  - jaroslaw staniek
slug:    de-icaza-were-dropping-xaml-qmls-human-friendly
---
Short story: Almost 3 years ago <a href="http://en.wikipedia.org/wiki/Miguel_de_Icaza">Miguel</a> praised elegance of QML compared to <a href="http://en.wikipedia.org/wiki/XAML">XAML</a>. It took so much time to abandon Moonlight but other than technical, the hottest reason is abandoning the XAML/Silverlight and whole .NET-on-Desktops idea by Microsoft itself.
<!--break-->
<i>"<b>Q:</b> Right now, there are four different XAML-based UI technologies: WPF, Silverlight, Silverlight for Windows Embedded and Silverlight for Windows Phone. With the introduction of Windows 8, we will be seeing a fifth version? What is your opinion of this diversification?</i>

<i><b>Miguel:</b> We are currently focused on C# for Android, iOS and Mac so we do not tend to interact much with XAML based frameworks.There are interesting parts to XAML, but I was never completely bought into XML for the markup. <b><a href="https://twitter.com/migueldeicaza/statuses/2455228032">I wished for a very long time that they had adopted something simpler for humans to produce, like using Json for the markup</a></b>, or the markup that was part of JavaFX. It was just as easy to consume and maintain with tools, but it was also easier on the eye for programmers and easier to type."</i>

<a href="http://www.infoq.com/news/2012/05/Miguel-Moonlight">Full interview</a>.

For comparison how hard XML can be for even easiest task before your morning coffee, see the <a href="http://www.developer.nokia.com/Community/Wiki/Animation_Basics_Silverlight_vs._QML">Nokia's comparison</a>. As usual, MS reduced deficiencies at language level (lack of consistency or "finishing" and recently, fragmentation) with pretty good Expression Blend tools that "cover" the rough edges. 

Yes, I know it's not QML that's the new focus but MSHTML5. You read it right: not HTML5 <a href="http://www.techradar.com/news/internet/web/mozilla-chief-blasts-windows-8-for-return-to-dark-ages-1079848">for this reason</a>. Never stop extending; <a href="http://www.joelonsoftware.com/articles/fog0000000339.html">Fire and Motion</a>, you know... Good that ideas like <a href="http://www.elpauer.org/?p=660">QML for XAML, a kind of legal/technical sudoku</a> have never materialized.

Bonus: <a href="http://blog.mardy.it/2012/05/from-g-to-q.html">confessions of a convert to Qt from GTK+</a> (thanks to Stephen Kelly for this link).

<a href="http://akademy.kde.org/"><img src="http://community.kde.org/images.community/0/03/Ak2012_imgoing2.png"></a>