---
title:   "KSemanticBrowserWidget"
date:    2005-10-04
authors:
  - jaroslaw staniek
slug:    ksemanticbrowserwidget
---
I hope someone could be interested in implementing semantic browser widget for KDE4 while playing with Qt4 drawing features... For example, similar to <a href="http://beta.news.com.com/html/plasma/newsplasma6.swf">news.com's</a> News Plasma.

Quite suitable complement for <a href="http://appeal.kde.org/wiki/Tenor">KDE4's</a> search <a href="http://kat.mandriva.com/">engines</a>.

<!--break-->