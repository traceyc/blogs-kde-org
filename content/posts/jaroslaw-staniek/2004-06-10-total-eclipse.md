---
title:   "Total Eclipse"
date:    2004-06-10
authors:
  - jaroslaw staniek
slug:    total-eclipse
---
Yeah, it looks for me like deja vu: one day Sun introduced OpenOffice.org, more recently IBM presented <a href="http://www.eclipse.org">Eclipse</a> (development environment). Why I am talking about this here, you may ask?

<br><br>
I am not too big enthusiast of Java and Java-related tools/technologies, even if in ancient times I've tried IBM's Visual Age, for Java and Smalltalk, IIRC. However my work is somewhat related to 1) openoffice.org 2) Kexi (and also more-and-more reusing/sharing bits of KDevelop). So, I've subjective feelings about oo.org and Eclipse:

<!--break-->

<ul>
<li> Both projects were developed/acquired in the past by a big players and let's be honest, both were a bit outside of the development maintstream<sup>TM</sup>, at least in these companies' meaning.

<li> Eventually, both companies had a choice to a) drop the project after many months of its dying b) opensource it. Both tried the latter way, and with relatively success. Both companies used their formerly-proprietary product as a part of their advertisement strategy (Sun even *pretends* that it's StarOffice is something very deferent than oo.org, while S.O. is often obsolete compared to current oo.org, and commercial offerings e.g. like OpenOfficePL).

<li> SO what's wrong with this? Both projects were developed (not from the very beginning) to be portable across operating systems. Thus, both projects become more and more bloated, used less-and-even-less OS-native components (something what was good for MS DOS-like OSes :), but not wanted anymore after 80's). 

</ul>

Now interesting part: what's related to KDE:

<ul>
<li> On the FOSS 'market' oo.org and Eclipse at least potentially competes with KOffice and KDevelop; both sides have its advantages (bloating vs inmaturity, smaller feature set or KDE-dependence); both have areas that really cannot shine: 1) KDE's Office cannot play well with, say, MS OLE, without breaking KParts design by using wrappers-to-wrappers-to-wrappers-to-wrappers (what oo.org does) :) 2) oo.org can integrate with underlying desktop environment only with some GUI bits and minor newbie-visible components (as Ximian shown), but it's API (7 tiers, or so) is just black magic, incompatible with any desktop environment's native API without ugly wrapping (I was surprised with <a href="http://artax.karlin.mff.cuni.cz/~kendy/cuckooo/">CuckOOo</a>, for example).

<li> Both oo.o and Eclipse, as software packages, are internally built as set of components somewhat interrelated each other via plugins. This is good, although these plugin frameworks are specific for the given project (e.g. <a href="http://api.openoffice.org/docs/DevelopersGuide/ProfUNO/ProfUNO.htm">UNO</a> for oo.o -- while advertised as general purpose framework, I am not aware of  well-known external projects developed using UNO). On the other hand, If I would be average user, I can at least discover that KDevelop uses the same source code editor and terminal emulator as is integrated with many other KDE apps. For typical Joe User, this is what integration means. Of course KDE Developers can found useful many more examples in KDE Libs' interfaces.

<li> More on plugins. See <a href="http://www.eclipse.org/eclipse/presentation/eclipse-slides.pdf">Eclipse Project Slide Presentation</a>, page 8 and so on. Kexi has similar architecture "almost everything is a plugin" like Eclipse, except I cannot see Eclipse plugins integrated somewhere else within desktop environments (because these environments already have their frameworks, eg. KIO / GnomeFS).

<li> Oo.o and Eclipse have it's own settings/configuration cache/framework built in (ah, Mozilla/XUL has it too, btw) -- again: all this is <b>custom</b>, not reused, external developers are not familiar with these frameworks. Probably these projects could use more stuff from freedesktop.org (ok, however there are still many specs to add). For now, Eclipse reinvents what KSyCoCa / KConfig, (or Gnome's cache) already does, oo.o has its own set of xml files and its own conf. strategy as well. IMHO this is mostly because oo.o and Eclipse are children of projects developed in really other situation in the IT universe.

<li> On page 14 of the presentation: Eclipse has plugins management and cross-plugins dependencies. Good. That's way like in we're designing Kexi. KDevelop has managed plugins per-language, as well. Right, Eclipse is here and is working, but it's valuable to show we're starting to have even more mostly-incompatible applications on Linux/Unix platform. And looks like none of them will dissapear because each has other objectives and 'market niche'. Just similar as on win32 or MAC OS X... At least, good that there are OASIS specs (hmm, do we have something like this for KDevelop/Eclipse?).

<li> Finally - there are many other areas where both oo.o and Eclipse increase redundancy in our Desktop OS: i18n, widgets (and even low-level pixmap painting!), language engines (Eclipse is shipped with IBM's Java, AFAIK at least by default; oo.o has Python bindings but requires internal compiled-in python interpreter). Shortly, these designs are monolythical.

</ul>


I am here not to beat great Eclipse and oo.o projects. Design decisions in these projects were also biased by compatibility requirements (eg. oo.o's scripting module is real functional ripp-off of MS Visual Basic for Applications, while oo.o API is highly incompatible with MS Office API, so more advanced scripts are not portable betweeen VBA and StartBasic engines, and in slowly incoming (in oo.o 2.0) MS Access replacement will be complete MS Access' ripp-off as well). IMHO, this one and reasons  I've mentioned above are also our gothas to remember, within KDE subprojects.

<br><br>
Funny, but there is a threat that one day Kexi will have to add some redundancy to the desktop OS (if it's to be workable not only on KDE but as Qt-only app, e.g under Gnome) to provide functionality like better portable binary plugins, making binaries out of projects, etc. However even then, I hope that redundancy can be greatly reduced.

