---
title:   "MS Access Visual Basic support in Kexi"
date:    2011-05-06
authors:
  - jaroslaw staniek
slug:    ms-access-visual-basic-support-kexi
---
MS Access Visual Basic support in Kexi. Short story: there's no support.

The reasoning, from the economical to the technical aspect is provided on the <a href="http://forum.kde.org/viewtopic.php?f=203&t=94748#p195438">KDE Forum</a>.

Or maybe there is some tiny support.

If you look closer, those that in the ancient days called themselves VB/VBA developers can find quite a few similar concepts in Kexi, regarding the object model and the way how the scripting language works. So switching to a new tool does not mean dropping all the knowledge and proper habits.

<img src="http://farm6.static.flickr.com/5295/5395750782_0e68895e72.jpg"><br><small><a href="http://www.flickr.com/photos/shetlandarts/5395750782/">Shetland Arts, by nd 2.0</a></small>
