---
title:   "Creating Photos Database in 10 minutes with Kexi 1.1"
date:    2006-07-05
authors:
  - jaroslaw staniek
slug:    creating-photos-database-10-minutes-kexi-11
---
<a href="http://kexi-project.org/pics/1.1/kexi_image_box/kexi_image_box_object_datatype.png"><img src="http://kexi-project.org/pics/1.1/kexi_image_box/kexi_image_box_object_datatype_sm.png" align="right" border="0"></a>
In the last post I blogged about <a href="http://blogs.kde.org/node/2075">database table schema (design) altering</a> features of the forthcoming Kexi 1.1 (to be released soon in alpha version). I promised to extend that story by presenting how all of this plays with GUI. In the meantime I've decided to take a break from this laborious task and finish the long standing TODO: implement the <i>Object</i> data type allowing to deal with binary data and force the <i>Image Box</i> widget to accept such sort of data. 

<!--break-->

<a href="http://kexi-project.org/pics/1.1/kexi_image_box/" target="_blank">For impatient, here's the screencast</a> (Flash, ~14 MB)

<br><b>Data Formats</b>

For now we have silently assumed that the binary data represents an image data. The binaries are silently mapped into <a href="http://en.wikipedia.org/wiki/BLOB">BLOB</a> data type of the database backend (SQLite3, MySQL, PostgreSQL, you name it). Kexi 2.x (powered by KDE4) will hopefully allow to store data of any type and most of them also possible to display (probably via some sort of the KParts and/or KoParts interface). To get the idea, imagine a widget box allowing you to insert, view, edit, and store an KWord or OpenOffice.org Writer file, track the changes and share it with others, without any effort once  the database is designed.

The image types currently supported are any of those supported by the KDE Image IO API. The conversion to QPixmap is performed only when displaying is requested (or saving to the clipboard or file) - in any other case we deal with QByteArrays.

<br><b>Image Box</b>

<a href="http://kexi-project.org/pics/1.1/kexi_image_box/kexi_image_box_insert_from_file.png"><img src="http://kexi-project.org/pics/1.1/kexi_image_box/kexi_image_box_insert_from_file_sm.png" align="right" border="0"></a>Kexi's Image box widget is already pretty well extended comparing to the ordinary QLabel. In particular it properly supports frames, backgrounds, scaling and keeping aspect ratio of the image. Moreover it contains an optional drop-down button on the right hand making it usable even if a Joe User cannot discover his RMB context menu.

The widget also supports images loading and saving back to the filesystem as well as clipboard operations. Exactly the same set of operations is available for <i>static</i> version (behaviour) of the widget. <i>Static</i> means just that when the widget has no data source assigned, it acts like a way for putting static images onto the form at the design time. Reusability: +1.

<br><b>Images in Table View</b>

On the other hand, Kexi's Table View widget is one of the larger beasts in KDE already. Being so feature-laden, it supports its own cell-level factory allowing to provide plugins for cell editors or just data presenters. One of them (built-in for now) is the one for presenting image data. The image is presented as a thumbnail. There will be added a drop-down button and menu exactly the same as the one in form's image box widget. Reusability: +1.

<br><b>Looking Forward</b>

<a href="http://kexi-project.org/pics/1.1/kexi_image_box/kexi_image_box_thumbnails.png"><img src="http://kexi-project.org/pics/1.1/kexi_image_box/kexi_image_box_thumbnails_sm.png" align="right" border="0"></a>Future extensions of the functionality include:
<ul>
<li>a possibility to assign a table field of type Object to use <i>external</i> files as its data, what avoids pushing the large binary stream into the (sometimes busy) database server; thus the only data stored in the database is the URL string</li>
<li>delayed loading of the data, probably associated with displaying some sort of a progress bar; in fact such optimization is planned for most operations in Kexi 2.x, making it <i>largely non-blocking</i> app</li>
<li>options allowing to <i>internal</i> storing some thumbnails in the database, so Kexi can load them much faster (depends on the aforementioned delayed loading)</li>
<li>plugin API for adding filters that you can apply to the images, say, from Krita</li>
<li>images collection (and objects collection in general) defined globally per-database, allowing to refer the images (objects) by name from any part of the database application (also from scripts and macros), and thus decreasing the overall redundancy - think about this as about referring to KDE Icons by name</li>
</ul>

Some of the TODOs have slightly lower priority though.

<br><b>Bonus</b>

At the end of the screencast I have added a small bonus showing how even small features (data validation) are reused in table view and form view. All this without a single line of code at your side.

The next post will be about some new results related to database table schema (design) altering and then about another much awaited feature: data-aware combo box widget.
