---
title:   "Downloading KOffice"
date:    2010-06-29
authors:
  - jaroslaw staniek
slug:    downloading-koffice
---
KOffice is a huge chunk of a code and a whole family of subprojects. Multiply this by (growing!) number of supported operating systems. Then you get the idea: yes, <b>we need your help with maintining the newly created <a href="http://userbase.kde.org/KOffice/Download">UserBase page on where to find KOffice software</a>.

You can share your opinions in person - like many other KOfficers:
<a title="Akademy 2010" rel="http://akademy.kde.org" href="http://akademy.kde.org" target="_blank"><img class="aligncenter size-full wp-image-136" title="Akademy 2010" src="http://kders.files.wordpress.com/2010/06/akademy2010.png?w=380&h=200" alt="Akademy 2010" width="380" height="200" /></a>
<!--break-->
