---
title:   "pim on win"
date:    2008-02-02
authors:
  - jaroslaw staniek
slug:    pim-win
---
Good evening from Osnabrück; Tom <a href="http://www.omat.nl/drupal/kde-pim-meeting-day-1">shares</a> some facts from the day 1 with you, so all I have now (before putting my hand on svn commit) is some  graphics. Recently (except for updating opensuse) I have switched from winnt5 to winnt6 on my notebook (thanks Adriaan!).

I need to admit I am regular user of Akregator under Vista now (which is -- repeat-after-me -- fully functional -- aKregator I mean, not Vista ;)). I am also slowly moving to KMail as well on the OS. The latter KDE app runs with POP3/SMTP/IMAP/SASL support offered out of the box. 

Read on for the gfx...
<!--break-->
<a href="http://kexi-project.org/pics/akademy/2008/osnabruck_kdepim/kmail_akregator_winv.png"><img src="http://kexi-project.org/pics/akademy/2008/osnabruck_kdepim/kmail_akregator_winv_.jpg"/></a><br/>Akregator and KMail for Vista (click to enlarge)

As we went crossplatform, the KMail's User Agent string is now more accurate, letting us to perform some statistics and provide better technical support:
<img src="http://kexi-project.org/pics/akademy/2008/osnabruck_kdepim/kmail_user_agent.png"/>


The default Qt widget style already gives pretty good and _native_ look&feel -- you should definitely expect more soon.

<br/>
<br/>
<img src="http://kexi-project.org/pics/akademy/2008/osnabruck_kdepim/walk.jpg"/>