---
title:   "New icon theme for KDE: Awesome"
date:    2012-11-19
authors:
  - jaroslaw staniek
slug:    new-icon-theme-kde-awesome
---
<a href="http://fortawesome.github.com/Font-Awesome">Font Awesome</a> is an iconic font designed for web UIs. It's a full open source suite of pictographic icons available with examples and documentation.

During the <a href="https://qtconference.kdab.com/node/23#Serving_QML">QML network</a> lecture at <a href="https://qtconference.kdab.com">QtDD</a>, Jeremy Lainé has noted that icons based on Fonts Awesome are useful for optimizing network usage, especially ones with high latency. While I have never used them I knew the technique which seems to be quite popular among web developers. 

So I came to related idea. <b>How about adaptation of the Font Awesome icons for KDE?</b> Obviously number of icons expected by KDE themes is much larger, but there's a way to start. It would be awesome (!) if someone implements that to show power of <a href="http://qt-project.org/doc/qt-4.8/qiconenginev2.html">QIconEngine</a>...

Of course the adaptation would only be <i>semi-automatic</i> because mapping is needed between Font Awesome naming and <a href="http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html">freedesktop.org naming</a>.

<a href="http://fortawesome.github.com/Font-Awesome"><img src="http://kexi-project.org/pics/blog/2012/font-awesome.jpg"></a>


Further ideas if you dare for more:
<ul>
<li>Because Font Awesome icons are monochrome, perhaps they could be also useful as a replacement for currently quite dated high-contrast KDE icons. It's trivial to add contrasting outlines.</li>
<li>Support for Qt-only apps would be possible since the icon engine API is Qt's QIconEngine.</li>
<li>Similarly, more organized support for Qt Quick is also doable through properly set up theme API implemented in QML or via a C++ plugin. Having a large icon theme would be good for start or at least for prototyping. Currently, unless predefined (desktop or mobile) components are used, to develop a Qt Quick app graphics files have to be supplied.</li>
</ul>

At visual level there is a bit to think about too - putting taste aside, by popular opinion monochrome (or at least visually simplified) icons give apps fresh and "cleaner" look, allow to put focus on content. Moreover, the web uses monochrome icons a lot these years, the world is tired of <a href="http://en.wikipedia.org/wiki/Skeuomorphism">skeuomorphism</a>, Vista glossy style is dead...