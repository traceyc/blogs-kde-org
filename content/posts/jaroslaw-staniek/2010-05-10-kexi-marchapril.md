---
title:   "Kexi in March+April"
date:    2010-05-10
authors:
  - jaroslaw staniek
slug:    kexi-marchapril
---
No, we do not skip March, for Kexi it was just too silent month to blog about it alone ;) Here we go (based on <a href="http://identi.ca/kexi">identi.ca notes</a>):

<ul>
<li>Adam now understands the <a href="http://websvn.kde.org/trunk/koffice/libs/odf/">koodf</a> library, exporting a report from Kexi into a spreadsheet can be done 'the right way' :)</li>
<li>New address of the Kexi Fan Club on Facebook: <a href="http://www.facebook.com/kexi.project">http://www.facebook.com/kexi.project</a></li>
</ul>
<!--break-->
<ul>
<li>Follow-up on the SQLite the <a href="http://blogs.kde.org/node/4156">secure delete</a> thing: Oh boy, the guys rock - they have <a href="http://www.sqlite.org/pragma.html#pragma_secure_delete">implemented all my requests</a> for <a href="http://www.sqlite.org/releaselog/3_6_23.html">3.6.23</a>. We're going to recommend this version at least.</li>
<li><a href="http://wiki.koffice.org/index.php?title=Kexi/Releases/Kexi_2.2#Missing_or_discontinued_features_in_2.2">Autofield widget and layouts will be disabled in Kexi 2.2</a></li>
<li>Fixed visibility of form widget properties in the designer; that was really obscure bug ;)</li>
<li>Kexi 2.2 beta 2 arrives in 2 days with KOffice 2.2 beta 2, please test! Yes because of quantum leap there's no Kexi 2.0 nor 2.1 in the wild.</li>
<li>Adam just begun work on reports for Kexi 2.3 even before 2.2 is out the door! Supporting plugins and a cleaned up core, so hopefully less bugs (April 4).</li>
<li>Adam just added image loading from the database for reports. Closing a bug with 1 line of code.<br/>&nbsp;<br/><a href="http://kexi-project.org/pics/2.2/rc1/kexi-2.2-rc1-report-images.png"><img src="http://kexi-project.org/pics/2.2/rc1/kexi-2.2-rc1-report-images_sm.png"/></a><br/></li>
<li><a href="http://events.linkedin.com/KOffice-Sprint-2010/pub/296141">KOffice Developers Sprint 2010 is coming</a></li>
<li>Kexi is in bug fixing mode in preparation for 2.2. Please try out the betas and let us know what you think on bugs.kde.org or #kexi on freenode (April 19)</li>
<li>We've just started development of Kexi Version 2.3 Alpha 1 (a part of KOffice 2.3 Alpha 1)! This is in parallel to perparing 2.2 series.</li>
<li><a href="http://www.ohloh.net/p/predicate">Predicate</a> rather won't be used in Kexi 2.3; doing that is too complex so we keep KexiDB until 2.4 release</li>
<li>Group boxes and tab widgets fixed in Kexi forms. Last fixes for 2.2.</li>
<li>Optimized display of scaled image entries in table view. <a href="https://bugs.kde.org/show_bug.cgi?id=234347">Scrolling is smooth</a> for large images too and scaling is smooth.<br/>&nbsp;<br/><a href="http://kexi-project.org/pics/2.2/rc1/kexi-2.2-rc1-tableview-optimized-images.png"><img src="http://kexi-project.org/pics/2.2/rc1/kexi-2.2-rc1-tableview-optimized-images_sm.png"/></a><br/></li>
<li>And last but not least: much of the work is possible because of quality bug reporting, thanks for that and we're asking for more :) These weeks special thanks go to George Goldberg!</li>
</ul>

(brought to you by <a href="http://www.piggz.co.uk/index.php?page=topnews">Adam</a> and myself)

<br/>
And of course:<br/>

<a href="http://akademy.kde.org"><img src="http://dot.kde.org/sites/dot.kde.org/files/igta2010.png"></a>