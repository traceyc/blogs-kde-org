---
title:   "To docbook or not to docbook?"
date:    2007-09-19
authors:
  - jaroslaw staniek
slug:    docbook-or-not-docbook
---
Recently I work on updating polish translation of <a href="http://www.ribbonsoft.com/qcad.html">QCad</a> manual before its release in OpenOffice Polska. Andrew Mustun, QCad author, has switched to <a href="http://xparrot.thebird.nl/parrotstable/xparrot/portal/doc.xprt/htmlview_light.html">xparrot</a> documenting tool</a>, i.e. an online and offline toolkit, containing markup which is simplified compared to docbook. When using xparrot I sometimes think how faster my work is when I just use simple set of tags (that's why having this in KDE would make me motivated to update docs more frequently, and possibly - online). Yet I have not seen to many templates that take advantage of such semantics used in Docbook (which is great -but for machines)...
<--!break-->

<i>"Projects using EU-xpert (an xParrot clone for EU projects) have compiled 200-900 page reports with the help of 20-60 contributing authors - who could work concurrently and had real time information on the state of the document.
<br/>
<br/>
xParrot is an open source collaborative authoring system through the Web using an XSLT engine. The goal of the xParrot project is to provide a replacement for Word when writing documents with large groups of people. Other software tools, like Wiki's, are used for this purpose, but we found they have certain short comings. In particular with regard to editing (problematic markup for non-technical people) and navigation (the structure of a Wiki document is hard to maintain)."</i>

xparrot uses <a href="http://xmlgraphics.apache.org/fop/">Apache FOP</a> (XSLT) for generating PDFs, so it's similar way how it's done in KDE now.

BTW, This is 3000th entry in kdedevelopers!
