---
title:   "Fruits of CSS2: Eating Our Own Dog Food"
date:    2011-11-28
authors:
  - jaroslaw staniek
slug:    fruits-css2-eating-our-own-dog-food
---
It may seem obvious but <a href="http://en.wikipedia.org/wiki/Eating_your_own_dog_food">eating our own dog food</a> can definitely be good for your project's condition. Especially when it is maturing over time. How many testers can we have and how many personas for your <a href="http://en.wikipedia.org/wiki/User-centered_design">user-centered design</a>  can we define and maintain? Usually just a few with the actual resources. Is only fulfilling needs of more or less fictional actors focusing on few use cases a good direction? Or does it put our vigilance to sleep?

Well, I am convinced you and your co-workers can be good actors if you actually <b>start using your software more</b>.

<a href="http://kexi-project.org/pics/akademy/2011/helsinki_sprint/calligra_sprint_2011-11-eating-our-own-dog-food.pdf"><img src="http://kexi-project.org/pics/akademy/2011/helsinki_sprint/calligra_sprint_2011-11-eating-our-own-dog-food.png"></a><br/><a href="http://kexi-project.org/pics/akademy/2011/helsinki_sprint/calligra_sprint_2011-11-eating-our-own-dog-food.pdf">Click to open (PDF)</a>

In the CSS2 presentation I published above there are only few but probably telling examples of what features of Calligra we can start using more in the project and how. There also a 'why'. Never force using apps if there are better (or good enough) tools already in place. Own dog food consumption can also be a risky business if the usage feels superficial and would not finally translate to any real-world use cases.

For example I do not intend to employ Kexi database creator for ad-hoc spreadsheet calculations on small, loosely structured data. Kexi and databases in general were not designed with this use case in mind. So as always it is better look closer.

The idea already brings its fruits. <a href="http://kexi-project.org/ppiggz.html">Adam</a> has already set up a PostgreSQL server for internal project-related services, in addition to various tests we plan to host there.

Can you quickly identify your products' use cases that can be introduced by your team eating your own dog food? Please leave them in the comments section below.

PS: Next time in the CSS2 series on shared Calligra themes.

<center><a href="http://www.calligra.org"><img src="http://kexi-project.org/pics/2.4/ads/calligra-2.4-beta-500.png"></a></center>