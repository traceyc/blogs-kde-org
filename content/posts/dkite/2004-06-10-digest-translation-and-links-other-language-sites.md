---
title:   "Digest translation and links from other language sites"
date:    2004-06-10
authors:
  - dkite
slug:    digest-translation-and-links-other-language-sites
---
Perusing the apache logs I noticed some hits coming from
 
<a href="http://www.kdehispano.org/modules/news/article.php?storyid=338">kdehispano.org.</a>

Pedro Jurado aka melenas has translated some things from the digest.

My question is how to assist those who would like to translate? I haven't given it much thought, but would be willing to set things up so others could read the digest.

Derek

