---
title:   "State of HEAD"
date:    2003-08-26
authors:
  - dkite
slug:    state-head
---
I am thinking of adding a feature each week describing the state of CVS HEAD. I will report which modules build, which don't, and any glaring bugs or neat stuff. My usage pattern is pretty typical, ie. email, browsing, addressbook, etc.

I have hesitated for two reasons; a lack of time. Also a stable bug-free development branch is a non sequitur.

But with the feature freeze the situation is changed. The state of the body of code is of interest.

Any objections? The purpose isn't to berate or complain, simply to report.

Derek