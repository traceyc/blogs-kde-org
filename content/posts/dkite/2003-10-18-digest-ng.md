---
title:   "Digest NG"
date:    2003-10-18
authors:
  - dkite
slug:    digest-ng
---
Time to redo the digest. I'm getting bored with what is produced, and frustrated with the limitations of static pages. Most complaints I receive are from developers whose work isn't highlighted.

So, my goals are:

 - Continue with highlighting selected commits, giving a view of the development process and progress.
 - Continue with news from developers about their projects
 - Continue to restrain myself from rants. Mostly successful.

I think those things have worked reasonably well. Now for the new stuff:

 - Simplify the generation of content. This would allow more time for adding or commenting on the content as opposed to simply editing. 
 - Allow collaboration. It is getting impossible to meaningfully cover all the applications. Others could help cover applications they are familiar with. Also in the event of Derek Kite being hit by a bus, or more likely being hit by a big hockey player going into the corner for the puck, there could be some continuity.
 - Allow readers to drill down for more content. For example, when features are added, usually the developer progressively commits his work. Sometimes the commit logs are cryptic, or describe a series of small changes that make up the feature. These situations are difficult to highlight, and are most often the cause for complaints from developers of being ignored. It would be useful to be able to view all the commits that relate to a file, or module, or a developer, or a development branch. Imagine being able to browse all the work done in the kwin_iii branch since the beginning of the year. The commits would be shown as atomic.
 - Expanded statistics. Possibly running totals over the week, statistics by week, month, year, or since a release tag. There is an interesting ebb and flow to the project. Graphs would be interesting.
 - Allow viewing of images and icons that are committed.
 - Improve the links to the source code. CVS is limited by it's file by file view of the world. It would be helpful to be able to view the whole of the commit, in all the files. I haven't even begun thinking this one through.

And for the wishes that may or may not come true:
 - Allow readers to configure what they see.
 - some type of indexing.

I've started writing in PHP, and in a surprisingly short period of time am almost at the point of generating what we have now. Once the code is working as expected, I will move everything to a server to permit further testing. There are a few technical issues to work out. The cvs logs can be very large, making dynamic generation of content too slow. It would be nice to work from a cvs mirror, but I haven't even started coding for that. I will start small and simple, get it working.

If anybody has suggestions on a domain name, let me know. I have no imagination for such things.

The code, as it is, full of errors and debugging statements, is in www/areas/cvs-digest/enzyme. It changes continually as I work on it.

Derek