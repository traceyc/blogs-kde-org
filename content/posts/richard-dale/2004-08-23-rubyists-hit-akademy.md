---
title:   "Rubyists hit the aKademy"
date:    2004-08-23
authors:
  - richard dale
slug:    rubyists-hit-akademy
---
I decided to come to aKademy about a week and a half ago, and am so pleased I did.

But travelling there was a disaster; I tried to save 10 UKP by flying with KLM  
via Schipol, Amsterdam, rather than direct from Heathrow. Big mistake! <!--break--> The
flight to Amsterdam was two hours late, and by the time it finally got there,
my connecting flight to Stuttgart had already gone. Then I found out that it 
was the last one of the day, and I would have to wait for a 9:20 am flight the 
following morning, getting me into Ludwigsburg about lunchtime. 

No compensation was offered, so I thought I'd make the best of a bad situation 
and at least have an evening out in Amsterdam.
When I arrived there I found all the bugdet hotels were fully booked, it being 
Friday night at the height of the holiday season. Hence, the only reasonable option was
to find a good bar, get blind drunk, return to Schipol and pass out on an airport chair. 
After exploring an excellent selection of Belgian beers in a friendly bar, I followed on with
a few more 'sharpeners' in the more 'exotic' part of Amsterdam to round the evening 
off before returning. Then a few hours of sleepless boredom sitting on uncomfortable 
aiport seats or wandering round aimlessly.

Anyhow, after all that, I felt like death warmed up the following day when I finally arrived 
in Ludwigsburg. So thanks, but no thanks to KLM for that experience..

The first talk I saw was Harri Porten, discussing his experiments with adapting the java
Prevalayer code to work with C++. Prevalayer is a persistance layer; instances of 
business objects are cached in memory, and it has a transaction oriented api with full
ACID properties with journalling and rollback. As I'm not much of a C++ fan, it
seemed to be a lot of work to try and make up for the lack of dynamism and reflection
in the C++ runtime.

Next was Matthias Ettrich talking about 'Designing Qt-style APIs'. He gave an excellent 
insight into the thinking involved in designing an application framework such as Qt 4.
How to make it easy to do simple things, while still having enough flexibility to handle
more complex requirements. Avoiding too many classes, giving methods consistent and 
meaningful names. It was the sort of thing that sounds obvious - but I suppose it really 
comes down to excellent taste and judgement combined with peer review to refine the results. 

Ian Geiser gave a run through of how to wrap C++ classes for kjsembed, although I 
couldn't quite follow the code from the slides - I need some better glasses.

I bumped into Alex 'lypie' Kellett before Daniel Stone's talk about freedesktop.org.
He was hacking away on ruvi (a vim clone written in ruby), but I managed to distract
him enough to give a demo of the latest improvments in the Korundum ruby bindings. He only 
has windows and no Linux on his machine at the moment which is unfortunate. I showed off the pykde
example apps that I'd translated to ruby, and the neat DCOP support.

Here's my particular DCOP favourite - reading a file from disc and writing to klipper with one
line of code. Note 'clipboardContents' looks like an attribute or property, where it can be
assigned to:

klipper = DCOPRef.new("klipper", "klipper")
klipper.clipboardContents = IO.readlines("myfile").to_s

Top that in other KDE languages!

Daniel discussed D-BUS from freedesktop.org, and it sounds as though that will be even
more powerful than DCOP, although I hope they manage to keep the api as easy to use (see
above).

The evening rounded off with some speeches from the local people who had organised the
conference - it really seems as though Free/Open Source software is taking off in
Germany. The was a buffet and loads of free beer and wine, which allowed to me to finally
shake off the hangover from the previous night. I spoke with loads of interesting guys
including Jim from Xandros about how they are adapting KDE with commercial add ons like
a Konqueror substitue, and 
a forthcoming server oriented desktop management framework. I think it will greatly help
KDE if they can show that it is possible to use it as a basis for commercial development 
and make money. The unstoppable Brazilian Helio Castro, was trying to convince anyone who
listen that next year's conference should be held on a remote Brazilian beach. Hmm, 
difficult to argue with that. I found Eric Laffoon much easier to understand in
person than by reading some of his more 'avant garde' kde dot news posts. I talked
with another unstoppable Brazilian, Avi Alkalay about advice on giving presentations. 
He said he'd given several introductions to Linux in portugese, but this was the first
time he'd attempted a technical one in English.

That's all for now, I'll try and write some more about day 2 at aKademy later..




