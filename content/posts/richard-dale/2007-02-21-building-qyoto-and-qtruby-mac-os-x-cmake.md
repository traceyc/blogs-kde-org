---
title:   "Building Qyoto and QtRuby on Mac OS X with cmake"
date:    2007-02-21
authors:
  - richard dale
slug:    building-qyoto-and-qtruby-mac-os-x-cmake
---
<p>I read this recent article about <a href="http://www.regdeveloper.co.uk/2007/02/16/uncle_mac_mono_/">Mono on the Mac</a> and thought why don't I try building Qyoto on Mac OS X and see if it works. 
</p>
<p>The article talks about GTK# using X11, although a native port of it, along with System.Windows.Forms being done. Cocoa# seems a bit incomplete. I personally like Objective-C, having been an Objective-C programmer for 10 years, and I don't think the Cocoa libs translate too well into C#. The api looked a bit ugly to me, with weirdness like every C# method being annotated with the objc type signature so you get to write everything twice. Using Attributes to annotate methods and classes is fine in the bindings classes themselves, and the Qyoto code does exactly the same thing with the C++ type signatures, and classnames of the methods being wrapped. But I'm not sure if you want to do that in application code. For example, here is a snippet of code from the ViewSample.cs example:
<pre>
[Register ("SimpleView")]
public class SimpleView : View {

    public SimpleView (IntPtr raw) : base(raw) { }

    [Export ("initWithFrame:")]
    public SimpleView (Rect aRect) : base (aRect) {}

    [Export ("drawRect:")]
    public void Draw (Rect aRect) {
        BezierPath.FillRect (this.Bounds);
        Graphics g = Graphics.FromHwnd (this.NativeObject);
        Font f = new Font ("Times New Roman", (int)(this.Bounds.Size.Height/15));
        Brush b = new SolidBrush (System.Drawing.Color.White);
        g.DrawString ("This is System.Drawing Text\non a NSBezierPath background!\nTry Resizing the Window!", f, b, 10, 10);
    }
}
</pre>
</p>
<p>I downloaded mono 1.2.3_1 and it had a dead straightforward .dmg package install, so that bit was easy. 
</p>
<p>To build the smoke library, I needed to fix up the CMakeLists.txt files to change some KDE specific bits, like some macros for the install directories where I needed to change '${INCLUDE_INSTALL_DIR}' to '${INSTALL_DIR}/include', and adding a few more INCLUDE_DIRECTORY() lines. On Linux perl scripts are executed as 'perl generate.pl', but on Mac OS X I needed to make them executable, and I don't know how to do that yet in a more portable way.
</p>
<p>Qyoto built pretty much first time with just a couple of unsatisfied link errors that were easy to fix. The qyoto lib needed to be built as 'libqyoto.so', and not 'libqyoto.dyld'.</p>
<pre>
# To build libqyoto.so on Mac OS X:
ADD_LIBRARY (qyoto MODULE ${SRC_CPP})

# The builds a libqyoto.dyld which doesn't work:
ADD_LIBRARY (qyoto SHARED ${SRC_CPP})
</pre> 
<p>Then I tried running the cannon game tutorial t14, and it worked fine. The only thing I would like is dbus support, and I don't know how to get the QtDBus module built for Mac OS X.
</p>
<p>I got QtRuby built ok, but it didn't work because Ruby extensions should be bundles such as 'qtruby4.bundle' and not 'qtruby4.so' or 'qtruby4.dyld'. There is no BUNDLE option in the cmake ADD_LIBRARY() command, and I haven't worked out how to get round that yet. The compiler needs to be given a '-bundle' option, and so I think it will need a Mac specific ADD_CUSTOM_TARGET(). So apart from some minor problems I'm pretty pleased with how cmake works cross platform, and the next thing I want to do is to try it with Windows.
</p>