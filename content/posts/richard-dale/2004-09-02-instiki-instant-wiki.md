---
title:   "Instiki instant wiki"
date:    2004-09-02
authors:
  - richard dale
slug:    instiki-instant-wiki
---
After reading this blog http://www.loudthinking.com/arc/000292.html
I thought I'd try out Instiki - it's a simple to set up wiki which can also export to an html website. Recently I've been using KJots to handle 'random notes' and todo lists, but what I really like to have is an outliner like 'Acta' that I used to use on my Mac a few years ago. Acta didn't do anything fancy - it wasn't trying to be a presentation package or a word processor that could also do outlining. But it was dead simple to use, and you could link in MacDraw figures if you needed any sophisticated diagrams.

I downloaded the instiki package and started it up by typing 'ruby instiki.rb' - that's all there is to it! Then the instiki daemon listens on port 2500 in the background, so you just have to type a url of 'http://localhost:2500/' in a browser to connect to it. You get a home page with setup instructions on how to get started, and a summary about the easy to use 'textile' formatting language ( see documentation here: http://www.hobix.com/textile/ ). 

Instiki doesn't do outlining as well as Acta, but it does it a lot better than KJots. You start a line with an asterisk for bullet points or a hash for numbered points. You can nest points using a line staring with two or three asterisks or hashes and so on.

I can connect to the wiki from every machine in my home intranet and update the pages. It handles versioning - you can see every change, and even roll them back. You can easily search the entire site (kjots would only search the current page which I found a big limitation). It has all sorts of formatting options: footnotes, sidebars, tables, variable size headers, and they are all really easy to use.

This is simple to use software, that is also powerful, and it would scale easily to serve multiple users where my beloved Acta wouldn't. Why do people still use word processors, rather than wikis, when they rarely print anything out on paper? I don't know..
