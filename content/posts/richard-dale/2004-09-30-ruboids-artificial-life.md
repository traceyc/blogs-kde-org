---
title:   "Ruboids Artificial Life"
date:    2004-09-30
authors:
  - richard dale
slug:    ruboids-artificial-life
---
<p>
On today's ruby-talk there was a post by Michal 'hramrach' Suchanek entitled "ruboids (on mac os) - singleton instance() returns nil?". I thought he was refering to the rubyists as 'ruboids', which seemed slightly rude to me. 
<!--break-->
</p>
<br>
<br>

But when I looked at what he was asking about, I found the he was trying to port a Ruby/Qt application to QtRuby. There is absolutely no code in common between the two bindings, just that they both wrap the Qt library for ruby programming. His problems with ruby singletons turned out to be a result of the half finished port. But I downloaded the code and finished converting the app to QtRuby not really knowing what it was, other than it used OpenGL and the Qt QGLWidgets. I thought from the name it was a game like asteroids.
</p>
<br>
<br>
<p>
But in fact, RuBoids is a <a HREF="http://www.red3d.com/cwr/boids/"> Boids</a> Artificial Life simulation written in Ruby and using OpenGL and Qt. It has these flocks of birds flapping away, and clouds with shadows over a grassy plain. There is a camera with slider controls that you can adjust to rotate the scene or zoom in and so on. I was really amazed. Each bird has very simple 'flocking behaviour', but they interact with each other and look, well, really quite bird-like. There are three simple rules:
</p>
<br>
<br>

<li>Separation: steer to avoid crowding local flockmates</li>
<li>Alignment: steer towards the average heading of local flockmates</li>
<li>Cohesion: steer to move toward the average position of local flockmates</li>
<br>

<p>
I've been wanting an OpenGL example app for a while, and so I've checked it into the QtRuby examples directory. I hadn't really thought of ruby as a language for writing 3D apps, but it works very well. I suppose OpenGL is doing most of the work, and the language just has to orchestrate things without needing to be especially fast itself, and hand everything off to the hardware acceleration.
</p>