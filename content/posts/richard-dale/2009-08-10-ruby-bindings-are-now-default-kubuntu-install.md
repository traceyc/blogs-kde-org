---
title:   "Ruby Bindings are now in the default Kubuntu install"
date:    2009-08-10
authors:
  - richard dale
slug:    ruby-bindings-are-now-default-kubuntu-install
---
<p>Using the Kubuntu distribution, I've been a bit envious of the Python bindings guys, because the bindings are installed by default and frequently get updated. So it made my day when I recently did an 'apt-get dist-upgrade' to get KDE 4.3 in my copy of Jaunty and saw that the KDE Korundum Ruby bindings were installed. This is great stuff Kubuntu community!</p>

<p>It isn't just a small subset only sufficient to run Plasma applets, you get the entire development environment with complete coverage of the Qt, Plasma and KDE apis, the rbuic4 tool to use with Qt Designer, and Ruby 1.8 of course. I think this is quite significant because Kubuntu is now a very nice end user programming environment for Python and Ruby right out of the box.</p>

<p>A few weeks ago Derek Kite wrote the he was trying to do some <a href="http://digested.blogspot.com/2009/05/rapid-application-development.html">Rapid Application Development</a>, but that he had problems with the QtRuby bindings:</p>

<p><i>Qtruby isn't packaged for the distro that I use (Arch). The kdebindings package includes only kdepython, which is required for kde. I run trunk, and tried building, with no success. At this point I was thinking that anyone else who wanted to use what I was writing wasn't going to jump through these hoops, so I looked elsewhere. Also I needed to get something working.</i></p>

<p>For me this is a great shame because nearly all the distributions package QtRuby and Korundum, and I have been working on the bindings for over six years now, and so it isn't something which is bleeding edge and very recent. It can be quite hard to build both the Python and Ruby bindings from the KDE svn, and so we really depend on distributors packaging them. I don't know what went wrong with Arch - maybe they have a no Ruby policy, or maybe nobody volunteered to do the work. If they are having build problems, I would be happy to help try and fix them.</p>

<p>With Kubuntu, absolutely everyone who uses it will now have an awesome Ruby environment installed, and it will mean that people like Derek will be able to write their apps knowing that everyone will be able to run them without having to install anything extra.</p>

<p>In the 1980s there were lots of computer magazines that used to publish programming articles with BASIC code, that everyone could input and run on their own computers. However, in the 1990s such large scale end user computer programming pretty much died out - tweaking the odd web page isn't quite the same thing. One of the assumptions that the Free Software movement makes is that every user is also a programmer of some sort, who is able to tweak the software on their computers. I hope we can get back to that spirit, and change the way that people think about KDE programming, because at the moment there is a tendency to think it is hard and that only the 'C++ gods' like David Faure or Thiago Macieira can do it. In fact it is pretty easy to write small Python and Ruby apps and plasmoids, or to write a little script to message an app over DBus. We just need to get communities of like minded people together who write tutorials on TechBase, create blog entries with code (like the 1980s BASIC articles), and help beginners get started. These ubiquitous end user programming environments in Kubuntu (and other distributions I hope) will make it possible to do that.</p>
