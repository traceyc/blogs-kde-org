---
title:   "English Spelling a Design Disaster"
date:    2006-12-07
authors:
  - richard dale
slug:    english-spelling-design-disaster
---
On the #kde IRC channel tonight there was some discussion about how bad english spelling rules were. I found out via google that there is a 'Simplified Spelling Society'. From the site, here is a description of how English spelling was invented:

<b>From 1476 printers took charge of things.</b>
<i> The early printers were nearly all foreign. Caxton, who set up the first printing press in London, was English but had lived mainly in Belgium and had written mostly in Latin. His assistants all came from the Continent. English spelling rules were therefore devised almost entirely by non-native speakers of English. Printers often also added letters to the last word of a line to make the whole text look neater. They were paid by line and habitually inserted letters into words to earn more money. Many of their whims and tricks eventually became rules of English spelling.</i>

http://www.spellingsociety.org/aboutsss/leaflets/whyeng.php

That made me 'laugh' (pronounced 'larf'), or as we say in the 21st century LOL..
