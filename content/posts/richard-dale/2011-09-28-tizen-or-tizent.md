---
title:   "Tizen or Tizen't?"
date:    2011-09-28
authors:
  - richard dale
slug:    tizen-or-tizent
---
<p>I'm not sure really. We have been discussing this question on the Codethink irc channel today, and I think probably Tizen't.</p>

 <p>I think the main point of HTML5 is that it runs everywhere and is platform independent. Once you create a platform that uses HTML5 plus its own extensions it really isn't HTML5 anymore. Giving up on Qt and going for some as yet unspecified native toolkit doesn't seem to be a good way to please and developers who have commited to MeeGo.</p>

<p>When it comes down to it, one of the only really innovative community based post PC projects is Plasma Active. It is still there after the sudden 'MeeGo axed' announcement. It isn't subject to the whims of large corporations like Intel. It isn't subject to patent attacks (yet). It is a perfect platform for ubiquitous computing - it runs on every device from large desktop flat screens, to tablets, and down to smart phone scale and below. It is activity and not application based with a foundation of the Nepomuk semantic desktop. Instead of being 'a bucketful of apps' as an interaction designer at the Berlin Desktop Summit described  Android, iOS, WP7 and the others, it knows about what you are doing and why. It moves beyond the simple 'game console/app store' style environment of the competion to provide a very different take on what a 21st century computing environment should look like.</p>

<p>Not only is Plasma Active/Contour technically advanced, but it is very advanced socially in that it is an integral part of KDE. The KDE community have shown themselves to be an exemplary Free Software project. We have many countries involved, we have all types of people involved, young and old, men and women, gay and straight, Christian, Muslim and Aethiest - they are all there. We have careful governance provided by the KDE eV organization who are in it for the long term. If you use KDE technology you know you can rely on it to have a consistent long term roadmap.</p>

<p>Aaron Seigo and the other Plasma Active and Contour participents have been careful to make sure that the project has good business connections right from the start. Companies like Basyskom are keen to create a common eco-system where 'a rising tide floats all boats'.</p>

<p>So what's not to like about Plasma Active. Have we taken a hit by the sudden cancelation of the MeeGo project. I don't think so, Tizen't likely.</p>