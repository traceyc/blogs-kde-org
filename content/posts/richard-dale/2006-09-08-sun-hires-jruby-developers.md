---
title:   "Sun hires the JRuby developers"
date:    2006-09-08
authors:
  - richard dale
slug:    sun-hires-jruby-developers
---
<p>Charles Nutter writes in <a href="http://headius.blogspot.com/2006/09/jruby-steps-into-sun.html">his blog</a> 
<i>The two core JRuby developers, myself and Thomas Enebo, will become employees at Sun Microsystems this month. Our charge? You guessed it...we're being hired to work on JRuby full-time.</i>
</p>
<!--break-->
<p>
He continues:
<i>The potential for Ruby on the JVM has not escaped notice at Sun, and so we'll be focusing on making JRuby as complete, performant, and solid as possible. We'll then proceed on to help build out broader tool support for Ruby, answering calls by many in the industry for a "better" or "smarter" Ruby development experience</i>
</p>
<p>
Woo hoo! This is great news for getting Ruby into the mainstream. It looks like we might get Ruby as a first class citizen in the Netbeans IDE too, which has had pretty non-existant non-java language support to date. And MicroSoft are sponsoring the development of Ruby on the CLR, so lets hope that can brought up to be on a par with Iron Python. In the main Ruby development tree we have greater speed on the way with the YARV VM for Ruby 2.0.. What can possibly stop the rise and rise of Ruby now?
</p>