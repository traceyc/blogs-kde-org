---
title:   "Running TiddlyWiki on the N810"
date:    2008-10-06
authors:
  - richard dale
slug:    running-tiddlywiki-n810
---
<p>One of the nicest applications I've found for the Nokia N810 is a single html page! <a href="http://www.tiddlywiki.com/">TiddlyWiki</a> consists of a page called 'empty.html' that you download and copy when you want to create a new Wiki. It works just like a traditional Wiki but is single user, and doesn't need a web server. The code for the Wiki is embedded within the page as JavaScript and CSS.</p>

<p>To use TiddlyWiki on the N810 I needed to fix a problem with JavaScript warnings when saving anything on the N810. I found some instructions <a href="http://www.internettablettalk.com/forums/showthread.php?t=21620">here</a> and <a href="http://groups.google.com/group/TiddlyWiki/browse_thread/thread/d00c441fd67b39f0/99e8868f62086fe3">here</a>. You need to enter 'about:config' as a URL and the browser allows you to add and edit preferences. Add these two key/value pairs to the preferences to allow local file:// URLs to be edited:</p>
<pre>
capability.principal.codebase.p0.granted
UniversalXPConnect  UniversalBrowserRead

capability.principal.codebase.p0.id 
file://
</pre>

<p>It made me think how easily web applications adapt to small devices like the Nokia. I wonder if it would be a good idea to have a custom web server always running, that could handle the UI for the main menus instead of the current menu system, and replace simple apps like the Bookmarks and Notes ones.</p>