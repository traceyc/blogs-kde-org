---
title:   "Rubyists hit the aKademy - day 2"
date:    2004-08-25
authors:
  - richard dale
slug:    rubyists-hit-akademy-day-2
---
Sunday started off early at 9:00 with Avi Alkaday's talk on
the Linux Registry project - he had some very nicely done slides and graphics,
and explained it all pretty clearly. <!--break--> IBM had paid for his travel expenses from
Brazil, and also lent him a nice lap top so he could hack on it while waiting
for airplanes. And thanks to Big Blue for picking up the tab on today's lunch
too.. 

The Linux Registry is like the windows registry, but done properly - ie not an
accident waiting to happen. It seemed well thought out to me - its avoids over
design. For instance, it doesn't need a daemon server process which could be a
single point for failure. It sounded very similar to NeXT/Apple's Netinfo, with
a heirarchy of things with name/property pairs associated with them. I wondered
why it lacked an rpc mechanism, but the argument was that would have added bloat
and maybe would lead to general overkill. It could end up gradually  escalating
into something like the 'Common Information Model' that Matthias Holzer-Klupfel
described next.

The CIM sounded useful, but very complicated. It handles a similar problem to
the Linux Registry, but for an entire enterprise rather than single machines.
It provided a good contrast between the big business 'design by committee'
approach (cf CORBA or EJB) where you throw in everyone's pet idea including the
kitchen sink, compared with the 'design by small numbers of tasteful
individuals' (cf DCOP or Linux Registry).

Lubos Lunak talked about the issues involved in intefacing KDE with the
X-Window system. Apparently he is the entire KWIN team at the moment, and was
hoping someone in the audience would be inspired to help out.

At lunchtime I talked to Brad Hards, who'd come all the way from Australia. He
told me about how Canberra has a Free Software presence much larget than you'd expect,
with several well known linux kernel developers coming out of the same
university department.

In another discussion with the trolls Eric and Matthias, Ian G and
others, Ian suggested packaging a complete Free software toolchain with
gcc/KDevelop and possibly WINE which would allow you to cross compile to three
different binaries (Linux, Windows and Mac OS X) in the one dev environment.
Eric said this would be really good as the 'Microsoft dev tools tax' double the
price of Qt windows development. 

Matthias said "I don't really get this dynamic typing stuff. It seems like
removing all the mirrors from your car..". I replied "Well, we give you a bouncy
car. You can bump into things and it doesn't matter!". Then more ruby advocacy
followed from Alex and myself.

Lars Knoll's talk was on 'Rendering and Laying Out Unicode Text', and he gave a
good summary of all the complicated problems involved in laying out all the
different types of writing system. I was encouraged when he said the he was
cooperating with the pango and OpenType guys to have more common code and push
it down into the base OpenType library.

I met Alex again and I managed to tear him away from his ruvi hacking; we
discussed the ruby bindings, the Smoke library and PerlQt. Alex suggesting a way of
breaking up the Smoke library into modules, which would speed up loading, and
also allow us to easily wrap and integrate other libraries such as the KMail
classes. We agreed we'd have a really good try at persuading the PerlQt guys,
Ashley and Germain to at least get a copy of PerlQt in the kdebindings cvs. It
would add to the credibility of Smoke, and make it easier to enhance the library
to keep PerlQt up to date with it, and we could port the korundum code back to
PerlKDE.

Brad's talk was on writing KDE plugins, which seems a 'black art' to me. I
think it would be good to have a day of 'entry level' education outside the
main dev conference, which could include scripting environments like kjsembed,
pykde and korundum. I think it's quite hard to show C++ code on a projector and
have people make sense of it - I had some trouble with working out what was
going on in Ian's kjsembed bindings implementation discussion.

Marius Bugge showed off the MVC based design in Qt 4 for QListView and
QTable. Scott Wheeler said he'd tried the initial alpha version, and it seemed
to lack 'convenience classes'. Marius said these weren't ready for the initial
version, but they were ready now, and should allow the classes to be used in
a straightforward way, while the new design would give a lot more flexibility
for advanced uses.

Finally, David Faure and Kalle showed off some of the most interesting hacks
described in Kalle's new book - I tried to take notes but forgot the exact
details fairly straightaway, so I'm looking forward to getting hold of a copy.

I ended the day drinking beer and eating pizza with the multimedia guys,
a good finish, although I'm personally more analogue - those low bit rates just
give me a headache - too old, insufficiently deaf for mp3's..
