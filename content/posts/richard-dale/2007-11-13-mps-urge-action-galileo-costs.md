---
title:   "MPs urge action on Galileo costs"
date:    2007-11-13
authors:
  - richard dale
slug:    mps-urge-action-galileo-costs
---
<p>I was just completely amazed to read that British MPs think that the Galileo project is unimportant. To me the combination of accurate and cheap global positioning systems, combined with the infrastructure to determine the relative position between one thing and another, and a semantic web that allows that GPS meta data to be annotated ubiquitously to all information on the web, is so important that every 21st economy will depend on it.</p>

<p>A BBC article <a href="http://news.bbc.co.uk/2/hi/science/nature/7087941.stm">MPs urge action on Galileo costs</a> suggests that:</p>

<p><i>"What taxpayers in the United Kingdom and other European countries really need and want is better railways and roads, not giant signature projects in the sky.."</i> said committee chairwoman Gwyneth Dunwoody.</p>

<p>I just don't know where to start explaining why this is so stupid. It shouldn't be a problem because politicians should just be enablers, and they would speak to people who actually know what needs to be done to make the future happen. But that just seems to have failed with these guys in the UK - they only seem to want act and behave like readers of the several dumb/moronic UK right wing populist newspapers.</p>
