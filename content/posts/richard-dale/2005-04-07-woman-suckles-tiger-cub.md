---
title:   "Woman suckles tiger cub"
date:    2005-04-07
authors:
  - richard dale
slug:    woman-suckles-tiger-cub
---
I read <a href="http://www.guardian.co.uk/life/thisweek/story/0,12977,1453305,00.html"> this article</a> in today's Guardian.
<!--break-->
<i>Call it a touching act of altruism or a curious assertion of maternal capability, but the bottom line is 40-year-old Hla Htay is breastfeeding a Bengal tiger cub.</i>
...
<i>As to why Ms Htay has offered her services, when bottled milk could do just as well? "I don't even want to go there," says Strike.</i>
<p>
She would like to stop when the tiger cub grows teeth. But why would a tiger cub eat the breast that feeds her, even when it had grown teeth? Would a grown up tiger still eat humans if she had been breastfed?
 
