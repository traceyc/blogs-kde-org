---
title:   "Qt4 QtRuby Windows port working"
date:    2005-12-16
authors:
  - richard dale
slug:    qt4-qtruby-windows-port-working
---
Congratulations to Ryan Hinton for getting a Windows port of the Smoke library and Qt4 QtRuby working. We just need to do a bit more to sync his version with the kde svn, and get it packaged. Then we're ready to do a first release that will run on Linux, BSD*, Mac OS X and Windows. Here's a couple of examples of how the ruby api improves on the original C++ one.
<!--break-->
<p>
You can pass blocks to a constructor, which allows you to have your own 'custom constructor'. This is very nice for painter paths:
</p>
<code>rectPath = Qt::PainterPath.new do |r|
    r.moveTo(20.0, 30.0)
    r.lineTo(80.0, 30.0)
    r.lineTo(80.0, 70.0)
    r.lineTo(20.0, 70.0)
    r.closeSubpath()
end
</code>
<p>
Or setting up widget layouts:
</p>
<code>buttonLayout = Qt::HBoxLayout.new do |b|
    b.addStretch(1)
    b.addWidget(@okButton)
    b.addWidget(@cancelButton)
end
</code>
<p>
Any 'setFoobar(thing)' method can be written as 'foobar = thing', which makes the code more readable, especially when combined with constructor blocks like this:
</p>
<code>slider = Qt::Slider.new(Qt::Vertical) do |s|
    s.setRange(0, 360 * 16)
    s.singleStep = 16
    s.pageStep = 15 * 16
    s.tickInterval = 15 * 16
    s.tickPosition = Qt::Slider::TicksRight
end
</code>
<p>
Of course you don't have to compile your code, and don't need the moc preprocessor. You don't appreciate how much of an advantage this is until you go back to C++, and it seems very old fashioned..
</p>