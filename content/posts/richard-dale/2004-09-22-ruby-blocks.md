---
title:   "Ruby blocks"
date:    2004-09-22
authors:
  - richard dale
slug:    ruby-blocks
---
Michael Pyne is <a HREF=http://grammarian.homelinux.net/~mpyne/weblog/personal/learning-ruby.html> learning ruby</a>. 

<i>Call me stubborn, but I am so used to { and } for forming blocks of code that I don't even want to go back to anything else. Python neatly sidesteps the problem by not having keywords for this sort of thing. Although I'm not fond of using indentation for creating blocks, that at least works with my programming style.</i>

Hey guess what! You can use curly brackets for ruby blocks if you prefer. A convention that quite a lot of people use is for curly brackets when the block is all on one line, but they use begin and end for multi-line blocks.