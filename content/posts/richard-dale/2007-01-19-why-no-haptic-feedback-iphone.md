---
title:   "Why no haptic feedback in the iPhone?"
date:    2007-01-19
authors:
  - richard dale
slug:    why-no-haptic-feedback-iphone
---
The highlight of our Foton company Christmas party was playing with a Wii for the first time. We got one rigged up via a projector onto the wall, so there was a nice large image, and then put the sound through a little stereo so it had a bit of punch. The game we tried first was Wii tennis, and it was amazing to watch people moving around and using the controllers just like bats in a physical real world game.

When my turn came I thought it would seem strange using the controller as a bat, but actually the strange thing was that it just felt really familiar. Playing Wii tennis isn't quite like being out in the open on a grass tennis court on a sunny day, but instead it 'feels' just like playing a dreamy sort of ping pong. Not like a computer game at all. 

But the stand out moment for me was when I moved the pointer on the screen onto a button, and the little controller in my hand twitched and made me feel there was a kinesthetic connection between the pointer, the controller and my head. In fact I didn't actually want to go on a play tennis, I just want to mess around with the pointer and buttons. It was one of those 'wow' experiences just like when I saw an Apple Macintosh in the flesh for the first time, and played with the mouse and MacPaint.

When I read about the iPhone announcement last week, my expectations of what my next generation of mobile phone would be like, were already coloured by my experience with the Wii controller. When you get a call from a mobile phone it vibrates. When you make a call on a mobile phone you start gesticulating unconsciously, even though the person you are talking to can't possible see you. So why can't the phone vibrate slightly when you hit a button, and why can't you wave the phone around in the air like the Wii controller and do things like hold it upside down and shake it to get rid of old messages. Then feel the messages going 'clink' as they drop out of the phone. Or jiggle it around a bit while upright to rearrange the order of things in the inbox? Certainly a touch screen with gesture recognition, such as being able to pinch your fingers together to make something smaller is nice and needed as well.

My dream phone doesn't exist yet. It will be user programmable and Free Software like the Trolltech Greenphone (and it won't cost 700 euros). It will have the slick looks and touch screen gesture recognition of the iPhone. And it will have 'Wii'ness built in, because that is what people will come to expect. After using a Wii controller you are changed, and a lot of other device controllers suddenly seem dumb and flat..

