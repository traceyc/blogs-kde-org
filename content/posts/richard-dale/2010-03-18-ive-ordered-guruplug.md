---
title:   "I've ordered a GuruPlug"
date:    2010-03-18
authors:
  - richard dale
slug:    ive-ordered-guruplug
---
<p>I read an interesting blog this morning <a href="http://www.h-online.com/open/features/Interview-Eben-Moglen-Freedom-vs-the-Cloud-Log-955421.html">Freedom vs. The Cloud Log</a> where Glyn Moody interviewed Eben Moglen. Eben Moglen was General Council of the FSF for 13 years and helped draft various versions of the GPL. He talks about the implications for software freedom caused by the rise of services in the 'cloud' where your data is owned by the service provider, and the fact that they don't usually release the code of their applications that run on the servers.</p>

<p>With the recent ownCloud initiative, the KDE community is doing something about the problem, by allowing you to have your own data stored in a variety of types of places of your choice, so that it is independent of a particular machine. It gives you a option of putting it on a host providers machine, or on you own network - somewhere where you personally are in control of it.</p>

<p>I had been thinking of getting some sort of home server, that was always connected to the internet, to use as a music server. The Apple Mac Mini running either Mac OS X or Kubuntu (like I run on my small laptop) seemed to be the best bet. Most servers are still really big and ugly with noisy fans, and not really suitable for running in your living room. The Mac Mini is one of the few attractive and quiet options, but it is quite expensive.</p>

<p>When Eben Moglen talked about how we could construct a distributed infrastructure peer to peer style, instead of client/server as used by Facebook and the like, he mentioned a small ARM based server called the 'SheevaPlug'. That got me thinking, and I went off googling for everything I could find about this tiny server the size of a wall wort power supply. It turned out that there is a new model called a <a href="http://www.globalscaletechnologies.com/c-4-guruplugs.aspx">GuruPlug</a> which has additional features like WiFi, eSATA interface for cheap fast hard disks, and an SD slot. The more I thought about it, the more fun the idea sounded. There are so many things you can do with one of these little servers.</p>

<p>So I just went ahead and ordered one. The basic server was 91.47 euros, a JTag board for debugging was another 26.82 euros, and shipping by FedEx was a bit pricey at 49.76 euros. The total cost was 168.05 euros, which is actually quite a lot relative to how much a cheap netbook costs these days. I'm sure in a years time you will be able to walk into a computer shop and get them for more like 50 euros or so. I think these things will be subject to Metcalfe's Law where their usefulness will rise according to the square of the numbers of users. If Eben Moglen is right we should be able to undermine the efforts of authoritarian governments, such as China, the UK, Australia, France and so on, by going completely peer to peer with strong encryption and cut off the government snoopers from invading our privacy and stealing our civil liberties. So for reasons like that, I think my 170 euros, plus a bit of my time will be a good investment.</p>

