---
title:   "UK under-graduate syllabsus's"
date:    2004-10-08
authors:
  - richard dale
slug:    uk-under-graduate-syllabsuss
---
Chris Howells posted a synopsis of his CS syllabus, and it sounded really dull to me. If someone has been making useful contributions to KDE like Chris, what is it that they need to learn from their teachers? ie what about the teacher who asked the question "who has used a computer before?".