---
title:   "UK Makes Science easier"
date:    2007-08-29
authors:
  - richard dale
slug:    uk-makes-science-easier
---
<p>I love the practical way the neo-liberal UK government make things happen to create a more 'business friendly' environment. The latest idea is to simplify science exams to allow more people to pass GCSE physics - what could be wrong with that? More scientific people and we'll surely have a more scientific country.</p>

<p>I had a look at the example physics paper <a href="http://extras.timesonline.co.uk/pdfs/exampaper.pdf">here</a> and tried myself out - I scored 38/40 'correct' questions, although sometimes I answered with what the examiner might have in mind as correct, as oppposed to my own viewpoint.</p>

<p>So which questions did I 'fail'?</p>

<code>They see many stars like our Sun.
The colours are different depending on the age of the star.
Which is the colour of the star between the stage between yellow and white?
</code>

<p>I'm not convinced stars are ever yellow - isn't that something to do with the Earth's atmosphere? I answered 'red' instead of 'blue'</p>

<code>Chei and Jaz are told that stars will eventually cool so much that they no longer glow. These cooled stars could still be detected by taking photographs using:

a. Gamma rays
b. X-rays
c. ultraviolet
d. infrared
</code>

<p>I answered 'gamma rays' without thinking about whether or not you can take pictures of gamma ray emissions. I'm not sure if a star is no longer emitting visible light, it will still be emitting infrared either, and the correct answer of 'd' doesn't seem that correct to me.</p>

