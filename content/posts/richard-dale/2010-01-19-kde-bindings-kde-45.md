---
title:   "KDE Bindings in KDE 4.5"
date:    2010-01-19
authors:
  - richard dale
slug:    kde-bindings-kde-45
---
<p>Aaron wrote an interesting blog about <a href="http://aseigo.blogspot.com/2010/01/key-quest-scripting-and-dynamic.html">scripting and dynamic language support</a>, and I thought I'd like to add some comments of my own about where we're heading with non-C++ languages in the KDE 4.5 SC release.</p>

<p>Language bindings are infrastructure, and they are a kind of compiler technology. We take the C++ headers and compile them into a form that allows the C++ methods to be called in other languages.</p>

<p>Infrastructure technologies only really succeed when they become 'boring and invisible'. For instance, there are many products which are based on the Linux kernel, but Linux is so 'boring and invisible' that for the most part, nobody even knows whether their television, GPS system or router contain Linux, and the products 'just work'. Similarly, the gcc compiler just works and we don't worry whether or not our latest build will seg fault, because the project has got to the stage where we can depend on gcc and only need to think about building our code, without be distracted by worries about whether or not it will work.</p>

<p>I've been working on various language bindings and technologies for the Qt and KDE apis for about 10 years now. At first, my work was 'exciting and visible', and involved much heroics in getting something like a Java binding to work at all for the whole Qt/KDE api, let alone being complete, reliable and easily maintained. I'm excited to think that we are finally getting to the point where people will think language bindings are 'boring' and they expect them to just work.</p> 

<p>The major bindings innovation for KDE 4.4 has been Arno Rehn's new C++ 'Smoke' library generator that he wrote for his Google Summer of Code project. We now have an industrial strength tool that can be used by normal people (as opposed to language bindings gurus) to produce language independent bindings libraries to wrap any KDE library. It is just a matter of creating a fairly simple XML file that consists mainly of the names of that classes you want to include, and a pretty standard simple cmake file to go with it, and then you're done. With a bit of documentation of TechBase about how to do this, and maybe some helper scripts we can make this really easy. This means that we can make a 'cultural shift' where people expect to create a bindings library for every KDE project as a matter of course, and that code will be part of their project instead of being inside the kdebindings svn module.</p>

<p>Mark Kretschmann of the Amarok project made some interesting comments on Aaron's blog about QtScript is the best language application scripting and extension. This is because on Windows, and the Mac to a lesser extent, even very common languages like Python or Ruby can lead to quite serious 'dependency hell' problems. I know how much Mark and some of the Amarok team like Ruby, and so I know that they wouldn't have switched to a QtScript-only policy for Amarok without pretty good reason.</p>

<p>Another Amarok guy, Ian Monroe started a QtScript binding based on the Smoke libraries a few months ago, and recently I've been working hard on the <a href="http://gitorious.org/qtscript-smoke/qtscript-smoke">JSmoke bindings</a>. They are now looking pretty complete with 32 QtScript plugins covering the Qt and KDE libraries, and I hope to move them from gitorious to the kdebindings svn module once the dust has settled from the KDE 4.4 SC release. I'm really pleased with how the code has turned out - there isn't much of it in the core runtime just 3300 LOC including white space and comments, it is easy to read and does a lot. What more could I ask for? I think I can pay it my highest complement and say the code is 'boring'. It has been a 10 year odyssey for me, to get to the point where I can produce something so dull, so unheroic that it will just disappear into the background. People will just take for granted that they can use their pick of over 2000 classes and their accompanying methods to throw together JavaScript complete applications, and to extend existing C++ KDE applications or write plugins.</p>

<p>So to summarize, I think we need to have a social change within the KDE project where languages bindings for non-C++ languages and scripting, are no longer 'fringe activities', but are part of the mainstream. We have made enough technical progress to be able make that change.</p>

<p>I have been talking about the Smoke based bindings, but I'm pretty sure Simon Edwards and others have got the PyKDE bindings to a similar stage of maturity, and I've seen some signs of non language bindings guys start to produce Python bindings recently which is really good. I hope more Python bindings will be created as part of a KDE app or library and be maintained in the same svn or git module, rather than in kdebindings.</p>

<p>A final exciting piece of news - today Chris Burrel announced this on the kdebindings mailing list:</p>

<p><i>"I'm interested in committing the work I've done on PerlQt4 to the
kdebindings svn tree, but I'm not really sure how to proceed.  The
project is currently maintained at http://code.google.com/p/perlqt4/.
I'm applying for an svn account, so I won't be able to do anything
until that gets approved, but I'm wondering where I could commit the
code.  I'm also interested in maintaining the history of the files,
but I'm not sure if that's possible.  Are you guys cool with me
committing this stuff?"</i></p>

<p>Woo hoo! I can't wait for KDE 4.5..</p>