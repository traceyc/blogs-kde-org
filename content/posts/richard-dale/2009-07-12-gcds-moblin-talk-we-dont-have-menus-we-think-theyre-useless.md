---
title:   "GCDS Moblin talk: \"We don't have menus, we think they're useless..\""
date:    2009-07-12
authors:
  - richard dale
slug:    gcds-moblin-talk-we-dont-have-menus-we-think-theyre-useless
---
<p>At conferences like the GCDS there is so much going on, and you get bombarded with information from all the talks one after that other, and that means that sometimes it takes a while for the meaning of it all to sink in. For me the two biggest themes of the conference were firstly the emerging Semantic Desktop, and secondly the increasing importance of visual design. We actually are in the process of inventing new ways of visual communication, and it was very exciting to be right there in the middle of it happening.</p>

<p>One presentation which didn't have a subtle, delayed effect on me at all, was Nick Richards' Moblin talk. The Moblin graphic design is really very good - nice clean icons and tasteful use of 3D for stuff like 'zones' (virtual desktops). The main screen reminded me a lot of the presentation of Plamsa for netbooks on the Akademy Beauty track.</p>

<p>He included a parody of how Microsoft would redesign and re-market the Apple iPod, which was actually made by some guys at Microsoft. It was really funny the way the 'iPod Human Ear Professional Edition' ended up with a complicated box plastered in text. Microsoft just don't have 'beauty and aesthetics' in their DNA for some reason, and if we want to compete with them we have to be in the Apple league, and move on from trying to imitate their ugly badly designed UIs.</p>

<p>The phrase in Nick's talk that really hit me immediately, was the one in the title of this blog: <i>"We don't have menus, we think they're useless.."</i>. I certainly don't like the look of KDE's or Gnome's menus much - they look like copies of Windows or Mac OS 9 respectively, and seem very dated. My pet hate is the underlines in the menu item names to denote keyboard accelerators which I personally never ever use. It has got to be one of the most ugly graphic design 'crimes' that Microsoft has ever commited, and I think it is sad that both desktops have copied it. Even though Gnome is trying to look like an old Mac, they still have those ghastly underlines.</p>

<p>If we can use Plasma for a beautiful customized Netbook workspace, how do we customize the actual KDE applications to look more 2010 than 1990? Most KDE applications like Kate/KWrite or Konqueror are built of KParts, and so it should be possible to wrap these KParts in modern QGraphicsView based shells with animation and a higher level of design (the shells could be written in Python or Ruby to make it easier to experiment). Also KDE applications have their menus and toolbars defined as XML files, and so it might also be possible to produced a different set of KActions that are more 'Plasma-like'. Either way I really hope we can produce something as impressive as Moblin over the next year.</p>