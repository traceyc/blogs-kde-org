---
title:   "QtRuby D-BUS Hello World"
date:    2006-06-06
authors:
  - richard dale
slug:    qtruby-d-bus-hello-world
---
<p>
I've just got my first Ruby D-BUS app pretty much working, it starts up, outputs convincing looking messages, and then crashes kdbus. I only started yesterday and so it hasn't taken long to get the D-BUS api pretty much wrapped. I still need to do some work on the QDBusAbstractInterface::call() method, as it uses C++ templates and doesn't translate easily into Ruby.
<!--break-->
</p>
<p>
I needed to add a Ruby equivalent of Q_CLASSINFO() to create suitable info in the Qt::MetaObject so that a class could be labelled as a D-BUS interface. Then I added to QtDBus headers to the ones used to generate the Smoke library, along with the D-BUS libs and that's all there was to it. Here is what the 'hello world' app looks like:
</p>
<code>
require 'Qt'
class MyWidget < Qt::PushButton
    q_classinfo("D-Bus Interface", "org.kde.MyWidget")

    slots 'mySlot(QString)'
                
    def initialize(parent, name)
        super
        Qt::DBus.sessionBus().registerObject("/", self, Qt::DBusConnection::ExportSlots)
    end
    
    def mySlot(greeting)
        puts "greeting: #{greeting}"
    end
end

a = Qt::Application.new(ARGV)
slottest = MyWidget.new('Hello dbus', nil)
slottest.show
a.exec
</code>
<p>
Or with xml introspection data:
</p>
<code>
require 'Qt'

INTROSPECTION_XML = <<EOS
<interface name="org.kde.MyWidget">
  <method name="mySlot">
    <arg direction="in" type="s" name="greeting" />
  </method>
</interface>
EOS

class MyWidget < Qt::PushButton
    q_classinfo("D-Bus Interface", "org.kde.MyWidget")
    q_classinfo("D-Bus Introspection", INTROSPECTION_XML)

    slots 'mySlot(QString)'
                
    def initialize(parent, name)
        super
        Qt::DBus.sessionBus().registerObject("/", self, Qt::DBusConnection::ExportSlots)
    end
    
    def mySlot(greeting)
        puts "greeting: #{greeting}"
    end
end

a = Qt::Application.new(ARGV)
slottest = MyWidget.new('Hello dbus', nil)
slottest.show
a.exec
</code>
<p>
(Edited 7/6/2006, the code now works with kdbus, and the slot can be invoked). And that's it! You just add a q_classinfo('D-Bus Interface', 'an interface name') to your class, define any slots and signals you need, and they are automatically turned into D-BUS slots by the Qt::DBus.sessionBus.registerObject() call.
</p>
<p>
I need to find more about how kdbus works, and whether the one I apt-get installed from Dapper works with the dbus-1 libs I linked QtRuby against. I understand that I can just add signals to the class above, and they will be forwarded to the bus, although I haven't found out how you connect to a D-BUS signal yet..
</p>