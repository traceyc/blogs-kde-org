---
title:   "JSmoke bindings KDE hello world working"
date:    2009-12-08
authors:
  - richard dale
slug:    jsmoke-bindings-kde-hello-world-working
---
<p>The Smoke based QtScript bindings are progressing well, and are now called 'JSmoke' in the style of 'JQuery' the JavaScript library or 'JScript' the .NET JavaScript implementation. In KDE promo-like words, I hope this will 'raise the brand recognition' of the state of the art KDE Smoke dynamic language bindings technology.</p>

<p>The project now has 24 QtScript plugins for the following libraries:</p>

<pre>
qtcore
qtdbus
qtgui
qtmultimedia
qtnetwork
qtopengl
qtsql
qtsvg
qtuitools
qtwebkit
qtxml
qtxmlpatterns
kdecore
kdeui
kfile
khtml
kio
knewstuff2
knewstuff3
kparts
ktexteditor
kutils
plasma
solid
</pre>

<p>There is a common 'libjsmokeruntime' lib that the plugins share. For running scripts there are two executables 'jsmokeapp' and 'jsmokecoreapp' for GUI and command line apps respectively. 'jsmokeapp' loads the qtcore and qtgui plugins and starts a QApplication. There still needs to be a 'jsmokekapp' which will start a KApplication, load the base KDE plugins, and run a KDE based script. Today I got a half Qt/half KDE app working. Here is the code:</p>

<pre><code>
qs.script.importExtension("jsmoke.kdecore");
qs.script.importExtension("jsmoke.kdeui");
qs.script.importExtension("jsmoke.kio");

function MainWindow(name) {
    KMainWindow.call(this, name);

    this.setObjectName(name);
    this.setCaption("KDE Tutorial - p3");

    var filemenu = new QMenu("&File", this);
 
    var openAction = new QAction("&Open", this);
    openAction.triggered.connect(this, this.fileOpen);
    filemenu.addAction(openAction);

    var saveAction = new QAction("&Save", this);
    saveAction.triggered.connect(this, this.fileSave);
    filemenu.addAction(saveAction);

    var quitAction = new QAction("&Quit", this);
    quitAction.triggered.connect(QCoreApplication.instance(), QCoreApplication.instance().quit);
    filemenu.addAction(quitAction);

    var about = "p3 1.0\n\n" +
                 "(C) 1999-2002 Antonio Larrosa Jimenez\n" +
                 "larrosa@kde.org\t\tantlarr@supercable.es\n" +
                 "Malaga (Spain)\n\n" +
                 "Simple KDE Tutorial\n" +
                 "This tutorial comes with ABSOLUTELY NO WARRANTY\n" +
                 "This is free software, and you are welcome to redistribute it\n" +
                 "under certain conditions\n";
    helpmenu = this.helpMenu(about);

    var menu = this.menuBar();
    menu.addMenu(filemenu);
    menu.addSeparator();
    menu.addMenu(helpmenu);

    var hello = new QTextEdit(
            "<H2>Hello World !</H2><BR>This is a simple" +
            " window with <I><font size=5><B>R<font color=red" +
            " size=5>ich </font><font color=blue size=5>Text" +
            "</font></B></I> capabilities<BR>Try to resize" +
            " this window, all this is automatic !", this );
    this.setCentralWidget(hello);
}

MainWindow.prototype = new KMainWindow();

MainWindow.prototype.fileOpen = function() {
    var filename = KFileDialog.getOpenUrl(new KUrl(), "*", this);
    var msg = ("Now this app should open the url " + filename);
    KMessageBox.information(null, msg, "Information", "fileOpenInformationDialog");
}

MainWindow.prototype.fileSave = function() {
    var filename = KFileDialog.getSaveUrl(new KUrl(), "*", this);
}

window = new MainWindow("Tutorial - p3");
window.resize(400, 300);
window.show();

QCoreApplication.instance().exec();
</code></pre>

<p>Today I also got the code working with Qt 4.6 pretty much. There is still a problem with setting QObject properties, although reading QProperties works fine. This is unfortunate because most of the examples set QProperties, and don't work with Qt 4.6. You need to build all the smoke libs in the trunk kdebindings, before you will be able get all the plugins to link. To build with Qt 4.5, you will need to disable the KDE plugins and QtMultimedia.</p>

<p>What is there still to be done? Well, loads of things. But I feel the bindings are starting to be useful, as they now have pretty complete coverage of Qt 4.6 and the KDE 4.4 kdelibs classes. The names and general structure of the project are in place, and it is now a proper KDE project - you are very welcome to try it out provide feedback, and help finish it off..</p>