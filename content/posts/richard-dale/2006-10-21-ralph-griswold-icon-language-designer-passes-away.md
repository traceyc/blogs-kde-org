---
title:   "Ralph Griswold Icon Language designer passes away"
date:    2006-10-21
authors:
  - richard dale
slug:    ralph-griswold-icon-language-designer-passes-away
---
<p>
I used to use my trusty original Macintosh in the 80s to learn new programming languages. Every year or two I'd get some a Mac version of something like Lightship Scheme, Allegro Object Logo or AlphaPop Pop-11 and a few books about them, and then work my way through learning stuff. One of my favourites was a nice GUI version of Ralph Griswold's Icon programming language. I was sad to read on LtU that he has <a href="http://lambda-the-ultimate.org/node/1780#comment">recently died.</a>
</p>
<p>
Icon was interesting because it featured 'goal directed execution', and I discovered it originally when I read about it in a book about implementations of Prolog. Functions didn't return 'true' or 'false', they succeeded or they failed. A function could cooperate with another function, returning multiple results and suspending itself after returning each result, and when the cooperating function returned it would restart in the same place. See the <a href="http://en.wikipedia.org/wiki/Icon_programming_language">wikipedia entry about Icon.</a>
</p>
<p>
Well guess which language has a very similar feature today? Why Ruby of course! In Icon the keyword 'suspend' allows you to transfer control to another function like a cooroutine, and in Ruby it's the 'yield' keyword. I don't think Ruby was influenced directly by Icon, and it got 'yield' from the CLU language, which I've personally never tried.
</p>
<p>
Here is an example from the wikipedia entry, it takes a pattern which matches numbers, and then returns the numbers that are odd, suspending itself after returning each odd number.
</p>
<pre>
procedure findOnlyOdd(pattern, theString)
  every i := find(pattern, theString) do
    if i % 2 = 1 then suspend i
end
</pre>
<p>
And here is an exactly equivalent method in Ruby, the only real difference is that you have to explicitly convert a String to an Integer before you can do arithmetic on it. Ruby is very fussy about types, and just because it's dynamically typed it doesn't mean that it isn't strongly typed.
<pre>
def findOnlyOdd(pattern, theString)
    theString.gsub(pattern) do |i|
        if i.to_i % 2 == 1
            yield i
        end
    end
end

irb(main):015:0> findOnlyOdd(/[0-9]/, "123456789") {|n| puts n}
1
3
5
7
9
</pre>
<p>
Well RIP Ralph Griswold, but how nice it is that we still have a main stream language that uses generators pervasively.
</p>
