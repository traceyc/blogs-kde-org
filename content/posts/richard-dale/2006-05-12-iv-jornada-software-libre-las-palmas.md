---
title:   "IV Jornada Software Libre, Las Palmas"
date:    2006-05-12
authors:
  - richard dale
slug:    iv-jornada-software-libre-las-palmas
---
<p>
Yesterday, I went to the IV Jornada Software Libre conference at the Universidad de Las Palmas sponsored by the Canary Islands government. There were some interesting talks, and much discussion in between. I gave one called 'Software Libre is Inevitable' in the morning.
</p>
<!--break-->
<p>
Someone asked about what I thought about how Microsoft's Vista compares with KDE, and I had trouble in remembering exactly what Vista is going to have that isn't already in Windows XP. I said I thought they added transparency to the windows at the expense of making it hard to tell which was the active window. 
</p>
<p>
There was a similar question about what I thought about the differences between Gnome and KDE, and I answered that based on my experiences with Ubuntu and Kubuntu they look much the same except that Gnome is 'brownish' while KDE is more 'bluish'. I added that unless you were a programmer familiar with the toolkit api's they were very similar, although of course I personally preferred KDE, and it had some nice features like IO slaves that Gnome didn't. I talked about the efforts that freedesktop.org is making to evolve common standards, such a mime types for drag and drop, and the Portland project.
</p>
<p>
After lunch, Alberto Barrionuevo of FFII Espana gave a talk on open formats, and how standards such as the Open Document ISO standard are an important complement to Software Libre. Borja Prieto gave a talk which appeared to be about how Cambrian era life forms evolved into present day bloggers - I really wish my Spanish was better so I could follow the subtle bits of the talks.
<p>
The last speaker was Enrique Dans who gave a really good presentation about how the combination of blogs, and open content with creative commons style types of licenses was creating a revolution in business methods, and the old ways just wouldn't work anymore. He's written a short piece about the conference on <a href="http://www.enriquedans.com/">his blog</a>.
</p>
<p>
We ended up in a restaurant stuffing ourselves with Canarian food, such as 'Papas arugadas con mojo' (wrinkley potatoes with a red garlic/chilly sauce) and I had plently of Cerveza Tropicals of course. Perfecto!
</p>

'T luego
Senor Dale Cerveza