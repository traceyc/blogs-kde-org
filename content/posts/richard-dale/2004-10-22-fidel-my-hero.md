---
title:   "Fidel is my Hero"
date:    2004-10-22
authors:
  - richard dale
slug:    fidel-my-hero
---
I've just posted this as a 'Story', but I think it's more of a blog. So please take down the story, or I launch my assault.. :)
<p>
I've read recent news that Fidel Castro is getting very old, and has <a href="http://newswww.bbc.net.uk/2/hi/americas/3761748.stm"> broken his arm</a>, that's sad because he is one of my great heroes. Up there with Hank Williams or Alan Kay..
<!--break-->
I went on a 'Marxism Today' study trip to Cuba in 1988 and came away awe inspired by what those revolutionaries had achieved in the 1950's. Although I'm not personally a Marxist, just a 'free thinker', and I had no personal axe to grind on whether the revolutionaries and their cause was right or wrong. But if you study the history of Cuba you will find many parallels with more modern liberation movements such as the Vietcong or the emerging resistance in Iraq. The defining moment of the Cuban revolution was the assault on the <a href="http://www.grossmont.net/mmckenzie/moncada.htm"> Moncada barracks</a> on July 26th 1953. Fidel and a small number of fellow revolutionaries holed up on <a href="http://www.cubaroutes.com/show.aspx?type=AT&cod=305&lng=2"> Siboney Farm</a> outside Santiago de Cuba to plan the assault. Shortly before the operation they gathered about a 100 young people there who didn't really know what they were in for. They just 'wanted to do something'. The actual event was pretty much a failure, just a bunch of students without much military trainging shooting holes in the walls. Afterwards a large number of them were tortured and killed. But their moral position was so strong that the government couldn't execute Fidel, and they put him in jail where he wrote 'History Will Absolve Me' - a brilliant pre-communist revolutionary tract.
</p>
<p>
It's very sad that Fidel has stayed on long past his sell by date, and I believe people in Havana today refer to him as 'the Old Horse'. All you see on television are old movies about the revolution and exhortations never to forget it, which bores everyone stiff. But then is access to MacDonalds hamburgers more important than a reasonable degree of personal liberty since the Revolution, compared with the 'yoke of American Imperialism' (as they would say). It's no use having free speech in the US if you can't afford healthcare, and are badly educated. In Cuba they have a higher literacy rate and a better universal health care system than in America for about a tenth the cost. Yet still Americans from the 'Land of the Free' are banned from visiting Cuba as tourists, in case they might get 'ideas'.
</p>
<p>
What is the relevance of this to software development? I think time has come when we need to choose whether to be revolutionaries, or whether to let US style corporate corruption take over. Should we be planning the assault on Moncada or just let big business roll over us?
</p>
<p>
Do we really want to let SCO or Kodak own obvious common property such as Operating Systems or OO IPC mechanisms? And charging huge fees to anyone interested in developing software of any sort, thus banning small developers from writing and distributing software. If anyone had asked me five years ago about Richard Stallman's ideas I would have said that he was a 'looney'. I wouldn't have understood his distinction between 'Open Source' and 'Free Software'. But the more I think about it, the more Free Speech and Free Software seem interconnected. Can you separate politics from software development - I think not. 
</p>
<p>
I originally started developing Free Software as a tax protest in early 2000. There was a tax regulation called IR35 which sought to ban anyone employed as a software consultant from investing in their own company, although in the UK you can't be an IT Consultant without actually forming your own company. The State decided that it was too easy for Freelance Consultants to become skillful relative to permanent employess, and so they had a crackdown on skill. Brilliant! Perfect! Sites like this <a href="http://www.shout99.com/contractors/"> Shout99</a> still reflect people's anger to this day. 
</p>
<p>
Should 'managerial types' dictate whether proprietary languages such as Java or C# become popular instead of say python or ruby? Why shouldn't I be able to invest money earned doing some boring consultancy in trying to invent the future? Does a typical permanent employee want to invent the future anyway - I think not.
</p>
<p>
Well anyhow, that's enough 'revolutionary talk' for now, back to improving KDevelop ruby support, which is coming on nicely..
</p>
<p>
-- Richard