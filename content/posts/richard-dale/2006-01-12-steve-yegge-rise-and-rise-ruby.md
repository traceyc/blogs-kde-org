---
title:   "Steve Yegge on the rise and rise of Ruby"
date:    2006-01-12
authors:
  - richard dale
slug:    steve-yegge-rise-and-rise-ruby
---
I've just read a couple of Steve Yegge's blogs about why he thinks certain languages have succeeded while other technically superior languages have failed. He has spent most of his time in the last year or two programming python, but recently has got into ruby. He writes really well about how the 'culture' around a language can affect its success or failure, and how the ruby culture is more open and friendly than python's.
<!--break-->
<p>
In the first blog <a href="http://opal.cabochon.com/~stevey/blog-rants/anti-anti-hype.html">A little anti-anti-hype</a>, he gives his reasons for why he thinks perl became more popular than python in spite of having arguably more warts, and why ruby might end up being more popular than both of them. This annoyed so many people he took it down before Christmas, and only put it back in the new year after he'd added some commentary and apologies for any unintentional python bashing. To me that suggests that the truth of what he was saying hit a raw nerve
</p>
<p>
Then in the second article <a href="http://www.oreillynet.com/ruby/blog/2006/01/bambi_meets_godzilla.html">Bambi Meets Godzilla</a> he gives some background for those who weren't around when Java killed Smalltalk, about what it feels like to see a language you love die on you as a result of marketing and good timing rather than any particular technical merit. 
</p>
<p>
That struck a chord with me because I was an OpenStep/Objective-C programmer in 1995 when NeXT was going strong, and Sun were about to release a version of OpenStep for Solaris. It seemed then that Objective-C was heading for world domination. A company called Taligent were trying to produce an OpenStep killer based on C++, but failing because of the limitations of that language (don't underestimate what an amazing technical achievement Trolltech have pulled off in producing the first worthwhile GUI toolkit based on C++). So what could possibly go wrong? Well the answer is a lot. Only a year or two later, Sun had abandoned their Objective-C/OpenStep implementation in favour of a pretty limited new language called 'Java', which had a pathetic GUI toolkit by comparison with OpenStep called the 'AWT'. A few mainly ex-NeXT guys started trying to do something to get a better toolkit than AWT, and wrote the Netscape Foundation Classes. That project was taken over by Sun, and made less NeXTStep-like and more static by IBM I believe. It ended up becoming the awful mess called Swing that we are still saddled with today.
</p>
<p>
Because of that experience I've never really been able to take a liking to java, not because I particular dislike it, but more for how it nearly killed Objective-C. So I can understand what Steve Legge is saying when he describes the depth of feeling felt by Smalltalkers.
</p>
<p>
But the story has a happy ending because Ruby, like Objective-C, is based on a lot of Smalltalk ideas, and it may well equal Java in popularity and supplant it over a 5-10 year time frame as the Java community become more bureaucratic, less hip and lose momentum. So a Happy New Year to all you rubyists out there - I think we have good reasons to look forward to a very successful and exciting 2006.
</p>