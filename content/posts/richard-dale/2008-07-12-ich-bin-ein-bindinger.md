---
title:   "Ich bin ein Bindinger"
date:    2008-07-12
authors:
  - richard dale
slug:    ich-bin-ein-bindinger
---
I've been in Berlin since Thursday, where we're having a meeting and hacking session about language bindings and Kross scripting. I like Berlin - it's a bit like Amsterdam - plenty of hippies on bicyles although without the canals, the Dutch or the narrow buildings.

On Thursday evening we went out for a meal and an Asian fusion restaurant, with a menu that was written in a mixture of German, English and Far Eastern food names, combined with very dim lighting for an extra challenge. I got the German guys to verify option 90 was vegetarian, and then on the spur of the moment ordered a 91 instead as it looked much the same, but with a few more words in the description. That was a mistake anyway, as I discovered that not all German words for meat end in -fleish, and I now know I need to avoid anything beginning with "Hen.." (ie chicken stuff). Other things to avoid in Berlin are lemons in the beer. I quickly realized that you need to order beers with a phrase something like 'Noch ein bier mit kein citrone bitteschon'.

We spent most of Friday describing our projects to each other when not hacking.

Mauro Iazzi is doing some interesting stuff with Lua which doesn't even have classes or inheritance, and he explained how it was actually sufficiently flexible to be able to emulate those features with closures and hash tables. He was using one of Roberto Raggi's C++ parsers from QtJambi to generate XML, and then generating C from that. We tend to measure progress in Qt bindings in terms of how far you have got in translating the cannon game tutorials. Mauro has done t1 to t6, and is working on t7 at the moment. T7 is the big one where you have to be able to define slots and signals in the bindings language, and so he is doing really well to get that far.

Cyrille and Sebastian described Kross and we discussed how we might get it to play better with PyQt and QtRuby. I made some changes to QtRuby and Kross today to allow them to share Ruby instances of QObjects and QWidgets without needing to create a new Ruby value anymore. Another idea might be to have a cut down version of QtRuby with a minimal set of widgets for doing simple things like dialogs, and it might be possible to cut the memory footprint down by quite a bit. Aleix Pol is using Kross with KDevelop and he talked about how it would make it easier to link in code like Bazaar that is written in Python, into a plugin that could be written in Python too.

Simon Edwards arrived a bit late on Friday afternoon and he discussed recent progress in the PyKDE4 bindings, which sound really very complete.

I talked about how the Smoke library works by comparing what it does with the features of the Qt moc utility, and how it worked in QtRuby. Then Arno Rehn talked about how we were using Smoke with the C# Qyoto and Kimono bindings.

Thomas Moenicke of KDAB has done a great job organizing the meeting - many thanks to KDAB and the KDE Ev for sponsoring the event. Thomas talked about the new PHP Qt bindings and the exciting possibility of implementing a web browser in PHP (!) using the QtWebKit widgets. I think a Plasma and a PHP binding could be a great combination for lowering the barrier to entry to KDE programming, and so I think we'd all like to get going on implementing that as soon as possible.

In theory, we were going to spend Saturday talking about how to get some documentation going on the TechBase wiki, but instead the day has turned into a bit of a hacking frenzy instead. For instance, we've got both the Ruby and C# Plasma scripting engine bindings working very well now (I'll do a blog about how all that works in a bit). Arno is fixing Ruby and C# DBus bugs and improving the List and Hash marshallers. Simon is adopting the way we've done plugins for C# and Ruby without needing any extra C++ code, and doing a Python very which will be very nice.

So "Auf Wiedersehen" from Berlin for now..