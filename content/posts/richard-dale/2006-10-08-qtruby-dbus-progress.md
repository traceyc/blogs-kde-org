---
title:   "QtRuby DBus progress"
date:    2006-10-08
authors:
  - richard dale
slug:    qtruby-dbus-progress
---
<p>
This week I've been converting the QtDBus examples from Qt 4.2 to Ruby, and getting the various Qt::DBus* classes working. Now QDBus support is pretty much complete and it will be fun hacking up some interesting apps like bridges to web services like I tried with DCOP. I'll have to get kde4 kdelibs and whatever else builds so I've got some sample apps with DBus support to experiment with.
</p>
<p>
I just need to find out what QDBusVariants are about and how custom type marshalling/de-marshalling should work in Ruby. Also I haven't done Ruby equivalents to qdbusxml2cpp and qdbuscpp2xml yet as I'm not sure if they're needed. I don't see how it is easier to write code in XML than Ruby. Qt::DBusAbstractAdaptor works fine in QtRuby, but again I'm not sure if it makes sense to use it. Really this stuff needs trying out to see what is useful and what is not. I think it's important for Ruby DBus programming to be as simple as possible.
</p>
<p>
Here's an example of what the code looks like - this is from the qdbu/listnames example. First the app checks whether it can connect to the session bus:
</p>
<pre>
if !Qt::DBusConnection.sessionBus.connected?
    $stderr.puts("Cannot connect to the D-BUS session bus.\n" \
                 "To start it, run:\n" \
                 "\teval `dbus-launch --auto-syntax`\n")
    exit 1
end
</pre>
<p>
Next it calls the ListNames method to get a list of dbus services from the Qt::DBusMessage instance that is returned:
</p>
<pre>
    bus = Qt::DBusConnection.sessionBus
    dbus_iface = Qt::DBusInterface.new("org.freedesktop.DBus", "/org/freedesktop/DBus", 
                                       "org.freedesktop.DBus", bus)
        
    qDebug(dbus_iface.call("ListNames").arguments[0].to_ruby.inspect)
</pre>
<p>
In QtRuby there is a simpler way to invoke a remote method, you can just do this:
</p>
<pre>
qDebug(dbus_iface.ListNames(arguments.inspect)
</pre>
<p>
You just call the method as if it was local and it will the list of names as a return value. Obviously you can't do error checking without the Qt::DBusMessage, but it is easier and looks prettier.
</p>
<p>
So QtRuby for Qt4 is looking really nice. But like Alexander says in <a href="http://adymo.blogspot.com/2006/10/white-vs-black-good-vs-evil-or.html">White vs Black, Good vs Evil</a> I'm a bit annoyed by attempts to ban Ruby apps from the kde modules on the basis of some survey where Ruby scripting got 250 votes, and Python got 300 votes. And I really don't want to have to defend Ruby by bashing Python as that's really lame - there is no reason for it to be an 'either/or' thing.
</p>
<p>
What advantage will be gained by only having one dynamic language for writing complete apps? Note that the question asked in this survey (with hidden agenda) was 'Preferred Scripting Language' which I would take to mean 'which scripting language to you prefer to use for scripting c++ applications', and I wouldn't take it to mean 'which dynamic language/RAD environment do you prefer to use for writing complete applications'. We are working on KDevelop support and Ruby bindings for KDE4 on the expectation that the killer RAD environment we hope to create can be used to write Ruby kde apps as first class citizens and suitable for inclusion in the kdeedu and other modules. Sorry to sound annoyed, but we don't expect to be banished to only extragear, thankyou very much.
</p>