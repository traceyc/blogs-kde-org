---
title:   "Java reflection vs Ruby respond_to?"
date:    2005-10-08
authors:
  - richard dale
slug:    java-reflection-vs-ruby-respondto
---
I've recently been making some rubbish attempts at fixing a bug in the QtJava bindings. The problem is here in <a href="http://bugs.kde.org/show_bug.cgi?id=112409">bug #112409</a>, it meant that an event handler method in QtJava could only be a direct subclass of a QtJava widget, like QWidget or whatever and not a sub class of a sub class of QWidget and so on. To fix it involved using java reflection to look for any overriden event methods in the java superclasses by writing a loop to go up the class heirarchy. Please laugh at my comments as I continually screw up on the commit :).

Here is how you do it in Ruby:

<code>instance.respond_to? :mouseReleaseEvent</code>

Um, not much to be said really..

And here is my effort in Java after 4 tries getting it wrong:

<code>
Class targetClass = onThis.getClass();
		
do {
    try {
        method = targetClass.getDeclaredMethod(methodName, parameterType);
        method.setAccessible(true);
        break;
        } catch (NoSuchMethodException e1) {
        }
			
        targetClass = targetClass.getSuperclass();
} while (targetClass != null);

if (targetClass == null) {
    return false;
}
</code>
<p>
This disadvantage of the Ruby approach is that it works first time without you having to think about it. Whereas the Java one allows you to display skills such as writing twisty loops with exception handling as in the above code, and looking like a loser on kde-commits :)