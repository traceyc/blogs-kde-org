---
title:   "Qt4 QtRuby windows port progress"
date:    2006-05-02
authors:
  - richard dale
slug:    qt4-qtruby-windows-port-progress
---
<p>
I've recently been working 'over on the darkside', and have been getting QtRuby working with the GPL'd Qt 4.1.2 and mingw on Windows 2000. Several months ago Ryan Hinton got QtRuby working be generating the code for the Smoke library on Linux, and then hand hacking it to get it to build on Windows. So ever since I've been very keen to get it working - that is until the prospect of devoting an entire weekend or more to Windows programming, and not surprisingly I just kept putting it off. Fortunately, Caleb Tennis's company sponsored nearly two weeks of solid QtRuby development, and I was able to get an awful lot done, including a Windows port.
</p>
<!--break-->
<p>
I've very little experience with Windows, only a couple of months 10 years ago with VB3, and Windows 95 which I found really awful, and was a major factor in causing me to switch jobs and end up as a NeXTSTEP/Objective-C contractor for the next few years. 
</p>
<p>
A big problem for me is that Windows isn't Unix - I can hardly blame Windows for that, but as I've only used Unix and Unix-like systems since 1987, I just expect my usual familiar environment to be on every machine I use. So it was interesting to try the Cygwin and Mingw shells, along with the DOS shell the gets installed by Qt.
</p>
<p>
I built Qt first, and found that the mingw compiler kept crashing when compiling the debug versions of the libs, and I had to disable debug in the end. Also the build of the jpeg stuff wouldn't work, and I had to give up and just use 'make -k' with that. I found that Qt doesn't actually have the headers under $QTDIR/include, until you do a 'make install' even though I was using it in the same place as I'd build it under c:/qt/4.1.2. That meant that kalyptus wasn't finding the header code, and it took me a while to puzzle out why that was.
<p>
I initially tried to get the KDE autoconf stuff working, and found that it was expecting X11, and the X11 headers and libs to be found. I hacked it to use the Mac OS X option instead to avoid that check. But then I kept hitting problem after problem, such as autoconf not finding zlib, even though I had both the lib and the headers installed. We really need a cross-platform build system that actually works cross-platform. My experience with autoconf on Mac OS X, and now Windows has been that it just isn't good enough - I hope cmake works better.
</p>
<p>
So having given up on autoconf, I just edited the perl scripts by hand that drive the kalyptus code generation of the Smoke libraries. I was pleasantly surprised quite how well perl works on Windows - I didn't have to change a thing. There were some compile issues with the generated to be fixed, like where Windows has a different type for a process id and the code didn't compile. There were about three relatively minor things like that, and then the Smoke lib built and linked.
</p>
<p>
Next was getting qtruby working using Ruby extconf.rb script for the extension. That worked as long as I did it from the mingw shell, and not from the others. I found that to build Qt and QtRuby I had to use all three shells at different times. So after a bit of handing hacking the generated Makefile, I found the qtruby code compiled without much problems and only a couple of minor things needing fixing such as a call to vasprintf that mingw didn't have.
</p>
<p>
There are still some issues to be sorted out with the port, such as a garbage collection bug which causes half the Qt examples to crash unless they're run with 'GC.disable', but apart from that the Windows version works great. 
</p>
<p>
I heard today that Qt4 QtRuby also runs on Intel Mac OS X, which would be a very nice combination.
</p>
<p>
So now you can run QtRuby on all three major platforms without needing any source code changes, and you can use Qt Designer to visually design UIs, will we have a contender for a VB replacement, as being discussed on various KDE blogs at the moment? I think so, and people are mistaken when they think Ruby isn't suitable for beginners - on the contrary it's a great first programming language, even though it has enough 'interesting features' such as continuations and lambdas to keep even the most expert programmer happy.
</p>