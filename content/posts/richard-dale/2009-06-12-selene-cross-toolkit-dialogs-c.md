---
title:   "Selene - Cross-Toolkit Dialogs in C#"
date:    2009-06-12
authors:
  - richard dale
slug:    selene-cross-toolkit-dialogs-c
---
<p>When you develop a language binding you never know what sort of thing people will develop with them, and it's really fun when people turn up with something. Yesterday I was chatting with Tobias Kappe on irc and he mentioned his <a href="http://selene.ntlabs.org">Selene project</a> that allows you to create dialogs in C# that are toolkit independent.</p>

<p>Tobias said that he was learning the Qt api, and the project  uses the GTK as an alternative toolkit. Here is an example of how you can combine both in a single source file:</p>

<pre><code>
using System; 
using Selene.Backend; 
using Gtk; 
using Qyoto; 
 
#if QYOTO 
using Selene.Qyoto.Frontend; 
#endif 
#if GTK 
using Selene.Gtk.Frontend; 
#endif 
 
namespace Selene.Testing 
{   
    public class Demo 
    { 
                private delegate void Show(); 
                 
                public static void Main(string[] Args) 
                {         
                        Show Execute = Grouping.Show; 
#if QYOTO 
                        new QApplication(Args); 
                        Execute(); 
                        QApplication.Exec(); 
#endif         
#if GTK 
                        Application.Init(); 
                        Execute(); 
                        Application.Run(); 
#endif 
                } 
    }  
}  
</code></pre>

<p>I've always liked learning new stuff such as toolkits and languages, and so this project is right up my street. You can learn C# programming in both Qyoto Qt and GTK# and see how they compare. Then maybe try a third variant with KDE widgets using Kimono. Once you are using the KDE classes, you can have a look at KConfig that does something similar, and try it out and see if you can borrow some ideas.</p>

<p>By the time you've done all that you've covered a lot of ground, although each individual step is easy and fun. That's what lowering the barrier to entry to Qt/KDE programming is all about, and a good demonstration of how language bindings can help. Some people may be able to learn C++, pick up the Qt and KDE apis and write a large app straightaway. But for a lot of people that is just too much to achieve in one go, and if you wanted an easier way to get going, I think helping out Tobias with the Selene project could be ideal.</p>