---
title:   "Should Qt and KDE apps written in C# be considered Free Software?"
date:    2009-06-29
authors:
  - richard dale
slug:    should-qt-and-kde-apps-written-c-be-considered-free-software
---
<p>Richard Stallman is giving a keynote talk about Free Software at the Gran Canaria Desktop Summit and I am very much looking forward to hearing what he has to say. However, I just read this short post <a href="http://www.fsf.org/news/dont-depend-on-mono">Why free software shouldn't depend on Mono or C#</a>, and to me what it is saying seems incoherent.</p>

<p>On the one hand he says that:</p>

<p><i>This is not to say that implementing C# is a bad thing. Free C# implementations permit users to run their C# programs on free platforms, which is good.</i></p>

<p>So Mono is a Free Software implementation of C# and that seems to be good. But then he says that is isn't a good idea:</p>

<p><i>The problem is not in the C# implementations, but rather in Tomboy and other applications written in C#. If we lose the use of C#, we will lose them too. That doesn't make them unethical, but it means that writing them and using them is taking a gratuitous risk</i></p>

<p>Umm, so we are to consider Mono as Free Software as long as we don't actually write anything in C#. Well call me baffled, but what I really like about the four software freedoms outlined in the Free Software definition is how unambiguous and clear they are. Yet here is an annex to that definition which is saying that we are to consider software as Free Software except when:</p>

<p><i>The problem is not unique to Mono; any free implementation of C# would raise the same issue. The danger is that Microsoft is probably planning to force all free C# implementations underground some day using software patents. (See http://swpat.org and http://progfree.org.) This is a serious danger, and only fools would ignore it until the day it actually happens. We need to take precautions now to protect ourselves from this future danger.</i></p>

<p>I am used to this sort of FUD argument from discussions on OS News etc, but to get it from Richard Stallman himself is something else again.</p>

<p>Are we supposed to lie down supine in the face of utterly stupid patent laws like those in the United States, or are we supposed to actually try to attempt to fight for our freedoms? If Free Software isn't about fighting for freedom, then I really can't see the point, and we might as well just write business friendly 'Open Source Software' (whatever that means).</p>

<p>Anyhow I'm looking forward to a full a frank discussion about this issue at the conference, as I think it is a very important issue.</p>

