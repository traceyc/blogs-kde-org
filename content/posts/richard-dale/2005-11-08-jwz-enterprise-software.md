---
title:   "JWZ on 'Enterprise Software'"
date:    2005-11-08
authors:
  - richard dale
slug:    jwz-enterprise-software
---
I've been quite taken aback by all the recent fuss about some default install option for KDE changing on a single distribution. I thought it might be a good time to post this quote about how <a href="http://www.livejournal.com/users/jwz/444651.html?replyto=5752811">Enterprise Software is boring</a>.
<!--break-->
<i>
True enterprise software is about as unsexy as it gets, and will remain so forever, I think. This is because it's not made for people. It's made for hive minds. It represents the borg mentality jacked up to 11. And really, that's not a criticism of enterprise application design: enterprise software is basically designed to suit the world of suits that uses it, and in some cases it does this quite well. For that audience, it's all about the bottom line rather than the booty call. 

It's just too bad that the needs of the suits have to get mixed up with those of average joe noncorp individuals. But too many people design for them who hold the cash and are willing to spend it lavishly.
</i>
<p>
I haven't personally put a pile of volunteer work into KDE bindings/RAD tools in order to please a bunch of suits. I really hope this frees us up to just get on with being innovative, and not have to compromise about frightening the bean counters. Why not just try and create a great community, cooperate with Gnome and others via freedesktop.org, while still trying to push the boundaries with plasma and so on for KDE 4?
</p>