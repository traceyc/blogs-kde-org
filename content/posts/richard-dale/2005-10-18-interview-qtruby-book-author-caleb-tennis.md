---
title:   "Interview with QtRuby book author Caleb Tennis"
date:    2005-10-18
authors:
  - richard dale
slug:    interview-qtruby-book-author-caleb-tennis
---
/\ndy Hunt one of the pragmatic programmers <a href="http://www.toolshed.com/blog/articles/2005/10/07/interview-with-qtruby-author-caleb-tennis">interviews Caleb Tennis</a>, author of the new book <a href="http://www.pragmaticprogrammer.com/titles/ctrubyqt/">Rapid GUI Development with QtRuby</a> in this podcast. 
<!--break-->
Caleb has just done a long overdue new release of Qt3 QtRuby and Korundum on the <a href="http://rubyforge.org/projects/korundum/">RubyForge site</a>, although we haven't announced it yet. I hope to do a first relealse of Qt4 QtRuby in the next couple of weeks, and put it up on RubyForge too.