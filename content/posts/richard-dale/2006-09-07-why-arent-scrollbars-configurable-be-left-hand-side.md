---
title:   "Why aren't scrollbars configurable to be on the left hand side?"
date:    2006-09-07
authors:
  - richard dale
slug:    why-arent-scrollbars-configurable-be-left-hand-side
---
<p>
A left handed person asks on Slashdot about the difficulty of using a touch screen when the <a href="http://ask.slashdot.org/article.pl?sid=06/09/06/2129200">scrollbars are on the right hand side</a>. When Alan Kay and others developed the original WIMP interface at Xerox PARC in the 1970s their systems always had the scrollbar on the left. For some reason Apple chose to move the scrollbar to the right, and everyone else, except NeXT just copied them.
</p>
<!--break-->
<p>
This has always baffled me because on the NeXT the scrollbars were much more usable on the left hand side compared with, say the Apple Macintosh. This isn't because I'm left handed, as unless you're using a touch pad that doesn't make any difference in itself. It's because if your text is ragged right and only some lines are very long you can position the right hand side of a window off the right edge of the screen without losing the scrollbar, and this allows you to make much better use of screen space. Even it you don't buy that argument, then if one side is correct for left to right languages it follows that it will be wrong for right to left languages. So how nice it would be if KDE could be made configurable so you could specify which side the scrollbar goes. The only exception at the moment is the excellent konsole where they can be configured. Does anyone know why it was ok to add for konsole, but not for all the other KDE apps?
</p>