---
title:   "Screen Locking in Fedora Gnome 3"
date:    2011-08-04
authors:
  - richard dale
slug:    screen-locking-fedora-gnome-3
---
<p>I wanted to try out Fedora 15 with Gnome 3 running under VirtualBox on my iMac before I went to the Berlin Summit. I've already tried using Unity-2d on Ubuntu, and I thought I if I had some real experience with Gnome 3 as well, I could have a bit more of an informed discussion with our Gnome friends and others at the Summit.</p>

<p>Sadly it didn't go all that well. Installing the basic distro went fine, but I couldn't manage to install VirtualBox Guest tools so that 3D graphics acceleration would work. The tools built fine, but the 'vboxadd' kernel module was never installed and there was no clue why in the build log. Then while I made a first attempt at writing this blog, Virtual Box crashed my machine and I lost everything. So it looks like I'll stick with VMWare for a bit yet even though it doesn't have 3D acceralation for Linux.</p>

<p>I discovered  that Gnome 3 locks the screen, when it goes dim, by default just like I found Kubuntu and Mandriva did recently. I had a look at where that option is defined and it was under 'Screen'. So Screen locking was under 'Screen' and I managed to guess where it was first time. Score some points for Gnome usuability vs KDE there! Even so I still don't think it is a 'Screen' thing it is a 'Security' thing. Interestingly Ubuntu doesn't lock the screen by default. Does that mean Fedora and KDE are aimed at banks, while Ubuntu is more aimed at the rest of us?</p>

<p>In contrast, I had spent a lot of time going round the KDE options and failing to find it how to turn off screen locking. Thanks to dipesh's comments on my recent blog <a href="http://blogs.kde.org/node/4458">about virtual machines and multi booting USB sticks</a> he pointed out that it was under 'Power Saving', and I managed to turn it off on my Mandriva install. There were also options under power saving to disable the various notifications that had annoyed me so much like the power cable being removed. Excess notifications are a real pain and it is very important to be disiplined about when to output them in my opinion. It feels like some programmer has mastered the art of sending notifications, and they want to show that skill off to the world.</p>

<p>Another app that outputs heroic numbers of notifications is Quassel when it starts up. I get a bazillion notifications about every channel it has managed to join, that I really, really don't care about. I think developers need to ask the question 'if the user was given notification XXX how would they behave differently, compared to how they would have behaved if they never received it in the first place?'. For instance, I can't imagine what I would do differently if I am told the power cord is disconnected, when it was me who just pulled it out. Maybe it would be useful if you had a computer where the power cord kept randomly falling out of its socket. Or with Quassel, do I sit watching the notfications for the twenty different IRC channels that I join waiting for '#kde-devel' so I can go in immediately. In fact I can't do anything with my computer because it is jammed up with showing me notfications.</p>

<p>Unlike Kubuntu, Mandriva was able to suspend my laptop when the lid was shut even when the power cord was connected.</p>

<p>The default behaviour on both Kubuntu and Mandriva with my HP 2133 netbook when I opened the lid, was to wake up with lots of notifications that I wasn't interested in, force me to enter my password in a screen lock dialog that I didn't want, and then immediately go back to sleep. This was actually the last straw I had with Kubuntu, and I was really surprised that Mandriva 2011 was exactly the same. </p>

<p>I had a look at my Mac System Preferences and couldn't find any way to lock the screen. The closest equivalent was in the 'Security' group that allowed you to system to log you out after x minutes of inactivity. That option certainly isn't on by default. Macs go to sleep when you close the lid, and wake up when you open the lid without a lot of fuss or bother.</p>

<p>Anyhow I look forward to seeing everyone in Berlin..</p>

<p> <a href="https://www.desktopsummit.org/"><img src="https://www.desktopsummit.org/sites/www.desktopsummit.org/files/DS2011banner.png" /></a></p>
