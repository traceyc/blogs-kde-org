---
title:   "Dynamic DCOP Server"
date:    2005-05-03
authors:
  - richard dale
slug:    dynamic-dcop-server
---
I've been messing with with idea of dynamically adding DCOP slots to a running DCOP server program. I've got a little server with a slot called 'eval()' which takes a string of ruby code and evaluates it as a sort of 'compile command'. So you send the new code for a dcop slot to the server, it evaluates it, and you can then invoke the new slot.
<!--break-->
<br>
<br>
<p>
Here is the code:
<code>require 'Korundum'
include KDE

class DynamicDCOP < DCOPObject
    k_dcop 'void eval(QString)'

    def initialize
        super "DynamicDCOPServer"
    end

    def eval(string)
        DynamicDCOP.module_eval(string)
        KDE.createDCOPObject(self)
    end
end

about = AboutData.new("dynamicDCOP", "Dynamic DCOP", "0.1" )
CmdLineArgs.init(ARGV, about)
a = Application.new
dcop = DynamicDCOP.new
a.exec
</code>
Then use the kdcop app to send this string to the eval slot:
<pre>k_dcop 'QString hello()' ; def hello() 'hello world' end</pre>
Refresh the kdcop view, and you will see the new slot 'hello()', which will reply 'hello world' when called.
<br>
<br>
You wouldn't normally want to actually send code to a running DCOP server like this, but what it does mean is that SOAP services could be discovered on the fly, and the code for suitable dcop slots could be generated to invoke the SOAP service as a bridge.