---
title:   "Sun go Through the Looking Glass"
date:    2006-10-14
authors:
  - richard dale
slug:    sun-go-through-looking-glass
---
<p>I was interested to read this article on how <a href="http://www.internetnews.com/dev-news/article.php/3637411">Sun have set up a pavilion in Second Life</a>, and are using it for virtual meetings:
<p>
<i>Tuesday, Sun became the first Fortune 500 company to hold an 'in-world' press conference to show off its new pavilion in Second Life, the popular 3D online world. Sun said it plans to invest in the Sun Pavilion as a place for developers to try out code, share ideas and receive training.</i>
</p>
<p> 
<i>"Our problem is that every year our largest developer conference (JavaOne) attracts about 22,000 people and we get to meet with them face-to-face for a week," said Sun's chief researcher John Gage during the virtual event. He said Sun hopes to reach millions of Java developers in Second Life with training and other support features.</i>
</p>
<p>
On the #kde-ruby IRC channel we've begun discussing how we might write a whiteboard or shared text editing app. Kelko has set up a wiki page and <a href="http://amarok.kde.org/wiki/KlipChart">summarized his thoughts so far</a>. I think collaborative P2P software is a really interesting area to think about. Do we need something simple like Gnome's Gobby, or the KDE mateedit app. Or a more elaborate app that would allow you to use a shared whiteboard over a protocol like jabber? Or should we aim to go straight to attempting to integrate legacy desktop environments like KDE with shared 3D worlds like Second Life or Croquet?
</p>
<p>
I've been looking through the Croquet site trying to find a definition of the 'wire protocol' for Tea Time/Tea Parties, but can't really find anything precise enough to do some sort of implementation from. The closest I got is this presentation <a href="http://www.opencroquet.org/Site PDFs/2005 Hedgehog Architecture.pdf">Hedgehog Architecture</a>. It talks about a 'Router' instance for each 'Tea Party' (a shared space for avatars to meet), and the router takes each event in the Tea Party and gives them a message sequence so that each message can be ordered in time. So for a simpler collaborative editor do we need all the messages for edits to the shared text to go through a central server? That's what Gobby does, and it works on a LAN, but would it introduce too much lag in editing text if typing every character involved a round trip to a server on another machine?
</p>
<p>
Another architectural issue is how to implement and use a protocol like jabber. One very interesting project is <a href="http://telepathy.freedesktop.org/wiki/FrontPage">Telepathy</a>, it is based on a DBus server that clients interact with that understands various P2P protocols. As I've recently added QtDBus support to QtRuby this would be a great way to experiment with them, and so I've begun getting code genertion for the Smoke library with the KDE4 kdelibs headers so we can start a KDE4 DBus based telepathy client.
</p>
<p>
Finally, how about this <a href="http://croquet-bento.blogspot.com/2006/06/better-avatars.html">better avatars</a> - software that allows you to transfer your Second Life avatar to Croquet. Imagine getting your avatar crafted by the 21st equivalent of a Saville Row tailor - they would measure you body in Real Life and suggest a body and clothes that looked enough like you for people to recognise, but had those 'special touches' that only an experienced avatar tailor could make. 
</p>
<p>
<i>"Would you like to look serious sir, or maybe you would prefer something more cartoonish and fashionable? We have some nice anime-look avatars on special offer this week. If you're a nature lover who would  prefer an animal with human augmented facial musculature - we do a popular line in rabbits.."</i>
</p>
