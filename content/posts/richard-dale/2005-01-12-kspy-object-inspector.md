---
title:   "KSpy Object Inspector"
date:    2005-01-12
authors:
  - richard dale
slug:    kspy-object-inspector
---
The other week I wrote about the <a href="http://blogs.kde.org/node/view/788">'Object Inspector'</a> features of the KDevelop Ruby Debugger that make use of the various sort of Qt runtime metadata and show it in the debugger's Variable Tree. I thought it would be nice to have something similar for debgging C++ programs.

I knew about a tool called Qt Object Inspector, but it isn't part of the Qt Free Edition so I haven't been able to try it out. I think it connects to a C++ program via a socket which must be built with a special debugging version of the Qt lib.

But this week I discovered Richard Moore's KSpy tool in kdesdk (under kdesdk/kspy), and it really is great fun to play with - another gem in the kde cvs that not many people might not know about. To use it all you have to do is include 'kspy.h' in your code, and then call KSpy::invoke() at runtime, and it uses KLibLoader to dynamically load the lib. A Window appears which allows you to browse the complete QObject heirarchy with a pane on the left, and look at the details of individual instance in four tabs in a pane on the right.

I've added various features from the ruby inspector code, such as a 'Receivers' tab which allows you to see if a QObject has any signals connected and which slots or signals they're connected too. Here is a screenshot of the <a href="http://blogs.kde.org/node/view/797">Receivers tab</a>.

You can examine the properties of a QObject with the <a href="http://blogs.kde.org/node/view/798">Properties tab</a>.

One thing it would be great to do would be to start KSpy from the KDevelop gdb debugger with suitable debugger commands at runtime. I've been reading the gdb docs, and it sounds as though it ought to be possible, but I haven't been able to work it out..