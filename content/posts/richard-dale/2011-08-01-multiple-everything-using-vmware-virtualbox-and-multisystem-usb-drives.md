---
title:   "Multiple everything - using VMWare, VirtualBox and Multisystem usb drives"
date:    2011-08-01
authors:
  - richard dale
slug:    multiple-everything-using-vmware-virtualbox-and-multisystem-usb-drives
---
<p>Recently there was an post on Hacker News about <a href="http://en.wikipedia.org/wiki/List_of_collective_nouns_for_birds">collective nouns for birds</a> in English. I run loads of virtual machines on my computer and I wonder what they should be called - 'a herd of virtual machines'? I have the mediocre Windows 7 Home Premium, and I wonder if that should be called a 'A badling of windows' after the phrase 'A badling of ducks'.</p>

<p>The big change for me in my computing evironment recently has been using virtual machines all the time instead of setting up my computers with multiple boot options. For work I use a 27 inch Macintosh with 8 GB of memory, running Windows 7 Home Premium and Kubuntu 11.04 under VMWare as guests under Mac OS X as the host. We have moved from scarcity in computer resources for programmers to a world where even the most under powered netbook can handle most of the things I need to do.</p>

<p>I spent most of last weekend preparing my HP 2133 netbook for use at the forthcoming Desktop Summit. I wanted to get it working well in advance as I had a lot of trouble with my netbook at the recent Qt Contributors Summit.  At that conference I couldn't even get WiFi working for the first day or two, and it was only because I happened to bump into the awesome Paul Sladen from Canonical that I managed to get it working to a reliable standard at all.</p>

<p>Talking to Paul he didn't think he was any kind of power user (he works for Canonical as a UI designer), and that his suggestions to me about how to sort out my machine should be obvious. I'm not sure about what exactly I'm good at, but I think it is only programming and I am not a very good systems administrator (or a UI designer for that matter). But if I have a lot of trouble doing obvious things with my Linux portable I think you can assume anyone at all normal with be having even more trouble.</p>

<p>I found out about the <a href="http://liveusb.info/dotclear/">Multisystem project</a>, and managed to create a USB stick with Kubuntu 11.04, Ubuntu 11.04, OpenSUSE 11.4, Mandriva 2010, Mandriva 2011 RC2, Fedora 15, Debian Squeeze 6.0.1. Then I tried running each of these distributions in turn and trying to install them onto my HP 2133.</p>

<p>My Great White Hope was SUSE because that was the distribution that my HP 2133 originally came with. I never got it running when I first tried to boot my new HP 2133 because I got a 'grub error 18' or similar, after i got it home and tried to boot it - that would have put off 99% of potential Linux users straight away. The install of  OpenSUSE 11.4 started and then after a while it died. Oh dear.</p>

<p>Next up was Fedora 15, and I got as far as the initial screen after being warning that my machine wasn't powerful enough to run Gnome 3. I started the 'install to hard disk' tool, and it died after a minute or two. Not enough memory, some other problem? I've no idea.</p>

<p>I was beginning to run out of ideas and then I thought of Mandriva and tried to install Mandriva 2010. That went well until I tried to boot and the Mandriva grub 1.0 install clashed with the grub 2.0 install of Kubuntu that I had on the second partition in my netbook. The naming scheme for partitions has changed between grub 1.0 and grub 2.0 and it isn't a good idea to combine them.</p>

<p>Large parts of my weekend were beginning to disappear even though it should be simple for an expert user to set up a netbook. Then I found out that Mandriva 2011 uses grub 2.0 and installing that worked great.</p>

<p>Back to bird collective nouns - I clearly had a 'flight of Mandrivas' here. I like the changes that Mandriva have made to the KDE UI. They have their own custom Plasma panel. It is black and doesn't look horrible like the default grey Plasma panel does. It doesn't have multiple virtual desktops by default. I thought I should just try the default UI at the Desktop Summit and see how I got on with it.</p>

<p>I wonder what has gone wrong with KDE Usuability considering we have a usuability expert on the KDE eV board. I find trivial usuabiltiy problems with KDE really annoying. For instance, if I set up KDE Wallet why do I have to give it a different password once I have logged in? Why doesn't it trust me?</p>

<p>If I try and make my laptop suspend while it is still connected to the mains power by shutting the lid, why doesn't it just suspend? I have to disconnect the power cable and then shut the lid again. Then plug the mains cable back in to ensure the laptop doesn't run out of power. Why do I get a completely useless notification about 'your laptop has had its power removed'? Of course I know that because I just removed the power cord to get round the problem of the laptop not suspending properly.</p>

<p>When my laptop wakes up it asks me for a password before it unlocks the screen. Why does it lock my screen by default? I don't know. I used to work on real time trading systems at a bank, and there was certainly a policy there of using screen lockers. But banks must be about 1% of KDE's users and so I have no idea why locking the screen is something a non-expert user should be confronted with. I haven't worked out how to disable screen locking even though it is a complete pain in the arse.</p>

<p>I have no idea why the KDE menus still have underlines in them to allow you to navigate without a mouse. That made sense when mice were rare things in the early eighties and power users without mice could navigate through Windows 1.0, but now about 1% of people use that option why are the other 99% being exposed to such an ugly UI.</p>

<p>I'm looking forward to the Berlin Conference and I hope we can make it an opportuntity to sort out KDE's 1980s and 1990's UI problems like I've just described. Clearly Plasma Active is in advance of everything else in my opinion, but the normal widget based normal KDE UI isn't. I still think we can do better than Mac OS X Lion and I look forward to hearing the opinions of KDE UI experts at the conference.</p.