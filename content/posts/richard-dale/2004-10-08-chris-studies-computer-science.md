---
title:   "Chris Studies Computer Science"
date:    2004-10-08
authors:
  - richard dale
slug:    chris-studies-computer-science
---
Chris Howells talks about how the computer science tutor on his new course made an <a HREF="http://www.chrishowells.co.uk/showcomment.php?blog=61">unpromising start</a> by asking <i>"Has everybody used a computer before?"</i>. Finding someone aged under 25 in the UK who hasn't used a computer before must take some doing. 
<!--break-->
<p>
I attempted to make on comment on Chris's blog page, but got this error:
<pre>
MySQL error: Column count doesn't match value count at row 1
</pre>

<p>
So it looks like a PHP/SQL debugging 101 might be a good start :)

<p>
<br>
<br>
<p>

I'm interested in what is being taught these days as Computer Science. Have you got a summary of the syllabus you could post Chris? Which languages for instance, are they using - Java or C++, and maybe python for a more dynamic language? Is it more practical than theoretical - ie is it how to interview users to establish their business requirements, or more like investigating the relative speed of quicksort vs. a bubble sort?

