---
title:   "My 30th Anniversary as a Programmer"
date:    2008-08-07
authors:
  - richard dale
slug:    my-30th-anniversary-programmer
---
I started my first programming job on August 7th 1978 as a graduate trainee in the 'Advanced Systems Sector' of a company called Dataskil, and today is the 30th anniversary.

In those days you didn't have a computer or even a terminal on your desk. Instead you had lots of listings, punch cards and coding sheets. In fact you were actually expected to spend most of your day imitating a computer because you were cheaper than the real thing. You would stare at a listing doing 'desk checking' because with only about 1 or 2 compilations or runs per day you couldn't really afford to make many mistakes. You carefully wrote a new program on coding sheets with a pencil, making sure that characters like Zs of Os were different from the 2s and 0s, and then sent if off to be 'punched'. To make edits you could send in a card pack with line editing commands, or book some time on a teletype and do it interactively. A fleet of vans would regularly collect your 'jobs', and take them to the computer centre, and return the printed output later.

Semiconductor memory was just coming in, but there were plenty of machines with ferrite core memory still. In the data center an early seventies ICL 1904S with 1 megabyte of memory was about 10 x 2 x 1.5 meters in volume for the core store.

I specialized in writing 'transaction processing' applications in a language whose syntax consisted mainly of commas. Once a program was ready to test you had to hire a small mainframe for about 100 pounds an hour, or we would go and borrow time on the local council's machine in the evening to save money.

If all of this sounds a bit grim, you're quite right it was. Mainframe batch style computing had no redeeming qualities whatsoever. In the previous two years I had been learning to program, as part of my philosophy degree, using a language called POP-11, which was a sort of lisp with a friendlier more pascal-like syntax (ie the Ruby of its day). The POP-11 programming environment was completely integrated with Unix tools such as ed and mail, and you could interact with it just like you can interact with the Ruby irb utility. irb would work just fine on a teletype, although it isn't integrated with an editor, and so in that respect it is in fact slightly less advanced than the environment I was using 32 years ago.

So I started this new job, in a strange world of utterly primitive batch computing, after getting used to thinking of programming as an interactive thing. Today most programming is still done in that primitive batch style; create the code with a text editor, prepare a build environment with tools like cmake and make, wait for it to compile and link and finally run it. OK thankfully punch cards have gone away and we actually have our own computers, but I find it sad that we have made so little progress in creating better programming environments. IDE's like Eclipse certainly make heroic efforts to cope with the problem of languages like Java being so static, but in the end it is just 'punched cards with better visuals' as far as I'm concerned.

While I was interacting with a teletype learning POP-11 in the 1970s, the researchers at Xerox PARC were busy inventing Smalltalk and an awesome programming environment that allowed you to interact graphically with your program. A finished Smalltalk program is an 'image', which is a snapshot of your current session, rather than text files. The trouble is that nearly all of our current tools are expecting a world of text files, and so if you switch to Smalltalk you have to ditch all of them and start again.

Next week, at Akademy it will be my birthday, and I hope we can celebrate by drinking lots of Belgian beer, and coming up with ideas for turning KDevelop4 into a Smalltalk-like environment for Ruby programming. I've got the smoke bindings for the KDevPlatform building, along with the accompanying Ruby extension ready to try out being able to write KDevelop Ruby support in Ruby. The amazing *Doctor* Dymo will be there I hope. Alex wants to use the Rubinius parser for parsing the source, both before and during a program run. We need to think about how we can send text to the irb enviroment from the Kate editing part, and how to show the current state of irb in a graphical manner. If we don't want to have an image, how should we store the extra data over and above the program's sources? Should the Ruby debugger be integrated with irb too - or should it remain more of a stand-alone tool?

Well anyhow, stuff the past - looking forward to meeting everyone, and perhaps inventing a bit of the future next week..

-- Richard
