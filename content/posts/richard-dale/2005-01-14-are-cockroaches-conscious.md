---
title:   "Are Cockroaches Conscious?"
date:    2005-01-14
authors:
  - richard dale
slug:    are-cockroaches-conscious
---
I enjoyed reading the answers to the question "What do you believe is true even though you cannot prove it?" <a href="http://www.edge.org/">here</a>. My favourite was by Alun Anderson, Editor-in-Chief New Scientist. His topic was how he thought insects were conscious..

<i>Strangely, I believe that cockroaches are conscious. That is probably an unappealing thought to anyone who switches on a kitchen light in the middle of the night and finds a family of roaches running for cover. But it's really shorthand for saying that I believe that many quite simple animals are conscious, including more attractive beasts like bees and butterflies.</i>

...

<i>To make sense of this ever changing behaviour, with its shifting focus of attention, I always found it simplest to figure out what was happening by imagining the sensory world of the bee, with its eye extraordinarily sensitive to flicker and colours we can't see, as a "visual screen" in the same way I can sit back and "see" my own visual screen of everything happening around me, with sights and sounds coming in and out of prominence. The objects in the bees world have significances or "meaning" quite different from our own, which is why its attention is drawn to things we would barely perceive.</i>

...

<i>That's what I mean by consciousness—the feeling of "seeing" the world and its associations. For the bee, it is the feeling of being a bee. I don't mean that a bee is self-conscious or spends time thinking about itself. But of course the problem of why the bee has its own "feeling" is the same incomprehensible "hard problem" of why the activity of our nervous system gives rise to our own "feelings".</i>

Or he describe how a spider 'hears' - it is covered in tiny hairs:

<i>Spiders that hunt at night live in a world dominated by the detection of faint vibration and of the tiniest flows of air that allow them to see fly passing by in pitch darkness. Sensory hairs that cover their body give them a sensitivity to touch far more finely grained than we can possibly feel through our own skin.</i>

Then our own ears have tiny hairs in them - the mechanism isn't that different, only where the hairs are. The spider really has an ear on the outside of its entire body, where we have an 'inner ear' to pick up the vibrations. Perhaps the subjective hearing experience of 'hearing a fly coming' for a spider might not be so different in kind from what we experience. And maybe the beating wings of prey would come in through the 'smell channel' too, and a juicy fly might smell just like sizzling steak would to us.

And on his favourite cockroaches:

<i>And as for the cockroaches, they are a little more human than the spiders. Like the owners of the New York apartments who detest them, they suffer from stress and can die from it, even without injury. They are also hierarchical and know their little territories well. When they are running for it, think twice before crushing out another world.</i>

This article <a href="http://www.vivisectioninfo.org/cockroach052200.html">Cockroach Capable of Feeling Pain, Says Study</a>

There's an article here about <a href="http://www.geocities.com/skews_me/implants.html"> 'Cockroach hacking'</a> - connecting their brains directly to physical devices.

What do I personally believe, but can't prove? That disease in the form of viruses, bacteria, and parasites have played a more important role evolution than sexual reproduction. Cockroaches have stayed virtually unchanged for hundreds of millions of years, and they don't suffer from disease much as far as I know.

