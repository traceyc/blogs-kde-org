---
title:   "New QtRuby/Korundum book"
date:    2005-10-06
authors:
  - richard dale
slug:    new-qtrubykorundum-book
---
Dave Thomas, the Pragmatic Programmer, made a <a href="http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-talk/157648">recent announcement</a> on ruby-talk about a new book by Caleb Tennis called 'Rapid GUI Development with QtRuby'. It is about 90 pages long and costs only $8.50 for a personalised pdf version. The idea of the 'Friday' small book series is something to try out on Friday afternoons. Congratulations to Caleb for getting the book out! Why not try out some QtRuby programming one Friday? The book will help you get going, and anything that helps lower the barrier to entry for Qt/KDE programming is great news.

Check out <a href="http://www.tarkblog.org/mind">Caleb's blog</a>, he is doing a series of 'handy things to do with QtRuby' articles. 

The Qt4 version of the QtRuby bindings are going well, and they aren't far off being ready for a release. I converted some OpenGL examples to ruby the other day, and that is a really good combination. You just need the 'opengl' package plus QtRuby to write 3D apps. They perform really well, and I think OpenGL/QtRuby would be a great for writing 3D games.

-- Richard