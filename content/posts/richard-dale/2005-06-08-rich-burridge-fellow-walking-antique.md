---
title:   "Rich Burridge is a fellow walking antique.."
date:    2005-06-08
authors:
  - richard dale
slug:    rich-burridge-fellow-walking-antique
---
Rich Burridge <a href="http://blogs.sun.com/roller/page/richb/20050608#30_years_celebration"> talks about</a> how he first started working in the computer industry 30 years ago for ICL computers. Hey! That rings a bell, I started my first programming job in 1978 as a graduate trainee in the Advanced Systems Sector of Dataskil, which was a software house subsiduary of ICL in Reading.

I remember punch cards, paper tape, magnetic core memory, GEORGE 3. it seems only yesterday..