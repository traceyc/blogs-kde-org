---
title:   "Two Tribes"
date:    2010-07-30
authors:
  - richard dale
slug:    two-tribes
---
<p>It's official the combined KDE Akademy and Gnome GAUDEC conferences will be held in Berlin in 2011, next year and this is great news! I played a small part in organising the joint conference in Gran Canaria in 2009, and really enjoyed working with the Gnome guys most of whom I hadn't meet before, as well as the familiar KDE people. It was great fun to see how it turned out. I don't think anyone really knew in advance - we didn't know if too much collaboration would spoil the 'community bonding' aspects of the conference and their individual identities. Or maybe too little collaboration would increase the distance between the two communities. In the end I thought the collaboration aspects could have been better, indeed like the WiFi could have been better - there is always something you can improve at these conferences, but what the heck, by and large it was mostly pretty good.</p>

<p>I enjoyed being able to go to both presentations, and some of my favourite ones, like the MeeGo^h^h^hMoblin user experience talk in the mobile track, came from the Gnome side. Both projects are facing the problem of how to you merge your desktop technology with the upcoming Smartphone/Small devices that run platforms such as Android or Meego.</p>

A consequence of stresses like upstream vs downstream or mobile vs desktop, is that Dave Neary's <a href="http://www.neary-consulting.com/index.php/2010/07/29/gnome-census-report-now-available-as-free-download/">recent survey of Gnome contributors</a> has given rise to much discussion and flaming about Canonical's contribution. One from Mark Shuttleworth called <a href="http://www.markshuttleworth.com/archives/439">Tribalism is the enemy within</a>, which is worth a read. That addresses tribalism within the Gnome community, and that is clearly a bad thing.</p> 

<p>Another good post from Jono Bacon <a href="http://www.jonobacon.org/2010/07/30/red-hat-canonical-and-gnome-contributions/">RED HAT, CANONICAL AND GNOME CONTRIBUTIONS</a> included this from Dylan Macall, which made a good point:</p>

<p><i>"..This whole thing is really putting forwards an issue Gnome has right now: they can’t, as a community, decide whether they like the idea of external projects building new environments on the Gnome platform. (Case in point: Meego for netbooks).
I think there’s one camp that thinks Gnome should be a user-facing product, with its own special branding and its own distinctive look that everything ships in pristine condition. (I’ll inject my opinion in brackets here: I think that entirely defeats the purpose of having multiple distributions).</i></p>

<p><i>Then there’s another camp that sees Gnome as a starting point with lots of handy tools (and common modules) for distributions to build operating systems. For example, Unity, Meego, Jolicloud, UNR…
That first camp sees Gnome as a monolithic project; only internal work is worthy. The latter camp sees Gnome as something akin to Gnu.."</i></p>

<p>I think you could have exactly the same discussion about the KDE project and what it's relationship with MeeGo should be. Does KDE consist of a lot of useful tools and libraries that we should encourage as many other projects as possible to use? Or should we concentrate mainly on a cohesive desktop experience? Are the two aims incompatible? I don't know..</p>

<p>I hope we can minimise Gnome vs KDE tribalism too, and I'm looking forward to another combined conference, where we can integrate the communities better (and in the process have great parties involving much beer drinking of course). I was slightly disappointed that Gnome 3.0 has been delayed, and I really wish them good luck in getting it out of the door. It is not true at all, that if Gnome does worse, then KDE will do better. If both projects succeed it is more like '1 + 1 = 3' or if one project fails it is '1 + 1 = 0.5'. We now have much infrastructure in common and need each other to succeed so let's "Relax"..</p>