---
title:   "Lypanov Announces Rubydium"
date:    2004-09-28
authors:
  - richard dale
slug:    lypanov-announces-rubydium
---
It was a pleasure to work with Alex on QtRuby/Korundum. But I haven't heard much about what he was doing since aKademy. There he talked about how he'd made a start with speeding up the ruby runtime with JIT techniques. I just loved <a HREF="http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-talk/113779">this announcement</a> he made about his latest project on ruby-talk. What a stylish entrance! Go Lypanov, Go!