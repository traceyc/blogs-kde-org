---
title:   "FOSDEM, day 1"
date:    2008-02-24
authors:
  - pinotree
slug:    fosdem-day-1
---
So, as I said previously, I'm at <a href="http://www.fosdem.org/2008">FOSDEM</a> right now, sitting in the KDE boot, with some Amarok guys, Joos demo'ing all around, Marijn working on KDE and Adriaan hacking on the KDE4-branded SOLARIS thin client.

<!--break-->

Personal impressions on some talks of day 1:
<ul type="disc">
<li>Linux in Hollywood: interesting how all the modern graphics for movies is all based on Linux, even if just for the rendering farms. A long-ish list of proprietary computer graphics applications for Linux is a nice way to tell the world that Linux can actually be used even for serious stuff</li>
<li>Interesting update on the current status of FreeBSD an its community, and how they still use CVS (sigh), but using Preforce to work more reliably with branches and such</li>
<li>Status on software patents: another good occasion to stress on the topic, recaling that the real issue is the patents _on software_</li>
<li>Linkat: an OpenSuSE-based distribution, targetting catalan schools (its primary language is catalan), and providing many services around it. It is interesting how they provide catalan documentation for the distro and the applications; I'm taking contact with them so they can hopefully share their material about KDE-Edu applications with us (KDE-Edu)</li>
<li>Sadly lost Enrico Zini's talk about Xapian, the room was damn full, not even the space for throwing the head in the room...</li>
<li>Followed MadCoder's talk about using GIT to fully maintain a Debian package, for all original sources, debian directory and patches to it</li>
</ul>