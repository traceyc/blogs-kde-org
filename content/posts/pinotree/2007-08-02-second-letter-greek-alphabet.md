---
title:   "Second letter in the Greek alphabet"
date:    2007-08-02
authors:
  - pinotree
slug:    second-letter-greek-alphabet
---
Here it is, <a href="http://www.kde.org/announcements/announce-4.0-beta1.php">KDE 4.0 beta 1</a>. Finally. I won't describe all the new features this beta comes with, as there are better people (<a href="http://aseigo.blogspot.com">random example</a>) able to speak at least 10 times than what i would be able to do :-P

So, going straight to the topic, the version of <a href="http://www.okular.org">okular</a> you find in the beta1 will have some (few, but interesting) improvements (other than the usual bugfixes), taking the alpha 2 as base version.
<!--break-->
The annotation capabilities of okular gains the possibility to move the annotations you placed on the pages. This is possible by holding Control and then dragging an annotation with the left mouse button. We are investigating with our usability expert Florian if this combination is optimal.
Speaking about dragging, thanks to a patch kindly provided by Diego R. Brogna, you can now drag the shaded zones in the thumbnails to change the visible parts of a document, even for the overview mode.
[image:2916 align=right hspace=10 vspace=5 border=0]
One of the most hated problems in today's applications are the blocking operations, that makes the applications freeze and the users unhappy. In KPDF and okular there are some operations that are still blocking. One of them is the opening of the propertoes dialog for PDFs, especially large ones. That happens because we loads the font information from the document before showing the dialog, and that operation can takes some time for large PDFs. Now, okular reads these information without blocking the interface. The result is a font view that progressively updates with new fonts (screenshot on the right).
We are also working on removing one of the blocking operations left, but the fact <a href="http://tsdgeos.blogspot.com">Albert</a> has <a href="http://tsdgeos.blogspot.com/2007/07/no-internet.html">no Internet</a> <a href="http://tsdgeos.blogspot.com/2007/08/avoid-yacom.html">these days</a> at home does not help :-(
On the usability front, we changed a bit the layout of the toolbar, removing some reduntant zoom buttons, and adding the text selection tool. The usability work is going on, even after the beta 1.

One thing it did not make into the beta 1 is the hopefully fixed support for compressed files, able to open nicely even remove compressed files now. But hey!, beta 1 is just a step before the final KDE 4.0 :)

Anyway, enjoy this new okular version, and remember feedback <a href="http://okular.kde.org/contact.php">is always welcome</a>!