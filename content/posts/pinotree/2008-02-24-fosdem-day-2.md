---
title:   "FOSDEM, day 2"
date:    2008-02-24
authors:
  - pinotree
slug:    fosdem-day-2
---
Another day, another set of interesting talks out of the over 200 here at FOSDEM.

First, I had a look at what the KDE guys at OpenSuSE are doing for KDE 4: plans to ship KDE 4.0.1 (or better, .2 as soon as it is released), whith some concerns about eg KDE PIM. Was also nice to know that they are working on porting the KDE 3 applications developed by their developers to KDE 4: KNetworkManager (using NetworkManager 0.7), Kerry (using a Xesam interface, thus more generic), KPowerSave, etc.

<!--break-->

After that, I went to the talk on KDE 4 done by Jos Poortvliet and Sebastian Kügler: lots of (interesting) bla bla ( :-P ) about KDE 4 concepts, ideas, etc, with Jos - instead of demo'ing kdeedu, kdegames and kdegraphics - tooking more than 10 minutes just to show the universe (with KStars, of course ;) ) to the people :-D
Then I decided to "relax" a bit, and placed myself into the KDE booth to catch up with emails and news from the world, until lunch.

After lunch, other three talks followed:
<ul type="disc">
<li>Knut Yrvyn's talk about Skolelinux and its importance for education («Spreadsheets away from 8 y.o. kids!», could not agree more)</li>
<li>Alexander Neoudorf presented CMake and some of the stuff the new 2.6 version will bring, especially for testing and simple packaging of applications</li>
<li>after a small break, Jonathan Riddell's "Debian/Ubuntu packaging for dummies". Note unrelated (not that much, to be honest) to the talk: wondering _how_ 'GNU hello' reached version 2.3...</li>
</ul>

Then at 5pm, almost the whole crowd of the FOSDEM started packing all their stuff, and KDE people as well.
I was one of the last four KDE guys to leave the building, together with Bart Coppens, Peter Rockai and Holger Freyther (that managed to lose himself around the city two days out of two :-D ).
I slowly reached the train station via bus and metro, and took the train (when I wrote this blog entry) that brought me back in The Netherlands (where the usual Dutch rain was there waiting for me, duh).

Many thanks go to the KDE people, to the Debian Qt-KDE guys (and gal, hi Ana!), and to my room mate, Marijn Kruisselbrink: all of you made me enjoy the two days at my first FOSDEM :)
