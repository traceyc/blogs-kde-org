---
title:   "Communities... for humans?"
date:    2008-04-06
authors:
  - pinotree
slug:    communities-humans
---
The fact that day by day, new people try to use free software or free operating systems (like GNU/Linux) is a good thing, and we all know that.
Something is supposed to help in the process is also the set of the various communities: GNU/linux distributions, applications, etc. Though, this is also something really delicate, where everybody invloved should (IMHO) be careful in what is shown towards the new comers.

Nowadays, you can basically do most (if not all) of the basic operations without even caring about what a terminal is. That is something really nice! KDE, GNOME, whatever graphical interface does not matter, as long it's there and works.
What I cannot really, really, really stand is when you read on user forums, blogs, and other help support dedicated to users, instructions like (emphasys by myself):
<pre>
Today the application Foobar v2.1 was released.
You can update your previous Foobar version by <b>opening a terminal</b> and doing the following:

$ wget <magic url>
$ cd /opt/whatever
$ tar xvzf foobaris-x.y.z.tar.gz
$ chown $USER:$USER -R foobar..
$ chmod -R ugo+rwx foobar...
$ cd foobar...
$ ./foobar
</pre>
What the hell is that, for a normal user???

Leaving aside the fact that distros (especially the most "user-oriented" ones) try to have all the updated versions available (even the same day of the release!), why the hell do we have GUIs for unpacking archives, file managers for managing files and their permissions, package managers to install packages even out of the distro official update channels, etc???
I feel all the work for doing these interfaces seems "lost", if the user channels just propose this attitude.

Communities, <b>STOP IT</b>!! The only effect is loosing and discourage new users!

Important note: this does NOT mean this is something that all the communities or all the people in each communitiy do, but still it's something sadly not so unusal to see. Sigh...

One of the mottos of Ubuntu is "Linux for humans". And sometimes the "humans" could even not bother about the terminal, especially for easy operations.

<!--break-->