---
title:   "Dutch cold fog"
date:    2008-02-14
authors:
  - pinotree
slug:    dutch-cold-fog
---
This is what I was able to see this morning, out of the window in my new Dutch apartment.
As ERASMUS student, two weeks ago I moved to Leiden for my studies, and here I am, facing myself with the Dutch environment. And its weather as well, that today kindly remember that we are still in winter. (Although last weekend was really nice, sunny and warm.)

<!--break-->

Some random notes I collected so far:
<ul>
<li>There is a better life style (or at least, it does actually give this feeling), especially when compared to my place</li>
<li>Transportations work, yay!</li>
<li>Bikes do everything, bring you everywhere, and people love them. Just wondering whether Dutch people have a sort of "shield" (or don't know what), as they go happily biking even with rain and strong wind. Have to investigate.</li>
<li>Some (most?) of offices and shops close at 5pm, something to consider when needing to go to more than one in the afternoon.</li>
</ul>

<a href="http://www.fosdem.org"><img src="http://www.fosdem.org/promo/going-to" alt="I’m going to FOSDEM, the Free and Open Source Software Developers’ European Meeting" align="right" /></a>
Oh, and a positive aspect of being in The Netherlands is that I will be hopefully able to attend (without paying much for travelling) to the next <a href="http://akademy2008.kde.org">aKademy</a>, and in nine days to the annual <a href="http://www.fosdem.org/2008">FOSDEM</a>.
So see you there ;)