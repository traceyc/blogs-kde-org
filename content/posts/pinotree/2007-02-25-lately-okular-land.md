---
title:   "Lately on the okular land..."
date:    2007-02-25
authors:
  - pinotree
slug:    lately-okular-land
---
... many things happens, uhm... the start of support of PDF forms.
But let's start explaining piece by piece. :)

Thanks to the work of Bradh Hards and Jiri Klement, the [w:XML_Paper_Specification|XPS] backend of okular has been improved a lot. It can now render pages in a way that starts to match to the expected behaviour, and get some information from the documents.

Second, okular got a simple audio player. You can wonder what's the purpouse of an audio player in a document viewer, right? The PDF format supports the usage of sounds - both external and embedded - in the documents. Thanks to the audio player, we can now play at least the external sounds. This thanks also to the great work of Matthias Kretz with <a href="http://phonon.kde.org">Phonon</a> and with the Xine backend for Phonon he's developing.

*drums*
[image:2694 align=right]
(<a href="http://tsdgeos.blogspot.com/2007/02/good-riddance-xpdfrc.html">thanks TSDgeos for the presentation</a> ;) )
Now, thanks to the efforts of Julien Rebetez - that developed the support in the core of <a href="http://poppler.freedesktop.org">Poppler</a> - I added the support for text and choice fields of PDF forms in the Qt4 frontend of Poppler, and in okular itself.
This means we can now show text and choice fields of PDF documents, even if in a really basic way.
But this also means you can not save the edited contents, you can not send the form, etc.
The most important thing, however, is that now we can get our hands really dirty with them, and implement the proper support they deserve :)
