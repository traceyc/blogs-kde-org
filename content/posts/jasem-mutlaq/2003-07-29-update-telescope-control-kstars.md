---
title:   "An update on telescope control in KStars"
date:    2003-07-29
authors:
  - jasem mutlaq
slug:    update-telescope-control-kstars
---
KStars should have a decent telescope control by 3.2. The current client/server architecture in INDI is scalable and would be very handy in observatories or when controlling multiple telescopes. I haven't committed anything in quite a while as I have been moving from one location to another within town and will finally settle soon!

There were some architectural problems with a previous version of INDI which required an update of the protocol. After upgrading to the updated protocol, controlling telescopes under Linux would be as easy as click n slew! KStars take care of many of the details and provides a powerful interface to control many of the telescope capabilities.

Okay, enough evangelizing and back to work!
