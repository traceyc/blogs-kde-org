---
title: PowerDevil in Plasma 6.0 and beyond
authors:
  - jpetso
date: 2024-04-23
SPDX-License-Identifier: CC-BY-SA-4.0
---

Hi, [I'm Jakob](https://invent.kde.org/jpetso) and this is my new KDE blog. Let's see how this goes
as I haven't blogged for literally decades.

I started working on Plasma code sometime last year and hope to play a tiny part in
~~setting the stage for world domination~~ improving user experiences like
[so many other awesome and dedicated contributors do every week](https://pointieststick.com/).
My Plasma contributions started out with a few rounds of build improvements and bug fixes
initially. Before long though, I decided to realize my lifelong dream of a friendly and usable
Energy Saving settings page. Thus started a journey into the code of Plasma's power management
service, internally known as PowerDevil.

Quick recap: As a service, PowerDevil is mainly responsible for two rough areas.

* It will suspend Plasma sessions when the time is right, and also,
* It adjusts backlight brightness levels of your display and monitors, including the ability to
  turn them off altogether.

In terms of user interfaces, the Energy Saving module in System Settings allows you to set
different suspend/shutdown behaviors and brightness adjustments for different power states:
on AC power, on battery, and on *low* battery. Ad-hoc adjustments to the same settings can also
be made with two applets in your system tray: Power and Battery, as well as Brightness and Color.

Let's have a look at some of the work that went into Plasma 6.0 in this area. I'm about two months
late for a 6.0 news blog, but [Fedora 40 KDE Spin](https://fedoraproject.org/spins/kde/) is just
out as the first major Linux distro on a semi-annual release cycle to feature Plasma 6. So maybe
not terrible timing. We'll also have a look at some of what's been brewing for 6.1 so far.
You may have seen some of this across a number of different "This Week in KDE" blog posts,
but then again maybe you haven't!


## System Settings & Config Data

![Energy Saving settings, before and after the redesign. It'll be called Power Management starting in Plasma 6.1.](/images/2024-04-23.powerdevil-kcm-comparison.png)

I'm quite happy with the way the new settings worked out. It also took a while to get there.

A year earlier, I posted [an outlandish proposal](https://invent.kde.org/plasma/powerdevil/-/issues/10)
to radically change the Energy Saving page. It's too long and boring and unworkable to cover here.
However, it made me think long and hard about all the different ways that the settings page could
look like, what its controls must be able to represent. It also made me aware that
[another UI rework](https://invent.kde.org/plasma/powerdevil/-/merge_requests/66#note_342005)
had already been in the works previously, which didn't get merged.

Nothing is ever as easy as one hopes it to be. A substantial redesign of the settings page would
require rewriting the UI with modern Qt Quick / QML code, as opposed to the rather unwieldy
Qt Widgets that the old UI was using at the time. As I learned, a settings module based on
Qt Quick also wants more knowledge about its underlying configuration data. Things such as:
"what's the default value for this setting", or "if you click this checkbox a second time
can we disable the Reset button again".

PowerDevil had lots of config data and was using old infrastructure in non-standard ways to access
it. The service would generate a new config file on first run, with all of its default values
written to `~/.config/powermanagementprofilesrc`. After creating this config file, it had no clue
which of the values were defaults and which were your own customized settings. In Plasma 5.x,
pressing the "Defaults" button would open a dialog to warn you that all of your changes will be
thrown away immediately and can't be recovered if you proceed. Compare that to other modules,
which let you press "Defaults" and "Reset" as much as you want until you decide finally "Apply".

Before even attempting to improve the UI itself, a series of changes was necessary in the code
that reads and interprets config files. I got there by extracting and improving one small
bite after another from the extensive but ultimately unmerged earlier attempt. A steady stream of
small, easy-to-review patches will win the race eventually.

The result is this. Power management settings are now only written if you change away from the
default values. If you checked the "Highlight Changed Settings" option, System Settings will be
able to show which of your Energy Saving settings have been modified compared to the defaults.
You can transfer your SSD from a laptop to a PC and the suspend/shutdown defaults will now reflect
the new system's capabilities. Extra config files for the various power states and activities have
been migrated to `~/.config/powerdevilrc`.

Building on these changes, the UI rework got merged just in time for Plasma 6.0. I think it's
significantly cleaner than it used to be, while at the same time there are still things I'd like
to change about it. Eventually I'd like to configure my "laptop lid closed" action only once,
instead of applying the same change across all three power states. The "custom script" controls
make more sense for laptops with different power states than for desktop PCs, where the current
design can be a little confusing. Screen and keyboard brightness should ideally move to the
Display Configuration and Keyboard settings modules. We'll get there eventually.

Special thanks to [Nicolas Fella](https://nicolasfella.de/) and [Nate Graham](https://pointieststick.com/)
for their repeated, kind & thoughtful code reviews. Throughout this project and really everywhere
else in KDE. Getting (or giving!) that kind of feedback makes a huge difference.


## Moved & Removed Functionality

The UI rework would have been harder if Nicolas hadn't also simplified PowerDevil in other areas.
One is that WiFi, WWAN and Bluetooth are gone from Energy Saving, they were more confusing there
than they were helpful. Switching wireless devices on or off can still be done through the system
tray applets, or the WiFi/Bluetooth modules in System Settings.

Plasma 5.27 and earlier allowed you to set a full power management settings profile if a given
Activity is currently active. It's still possible in Plasma 6.0 to block automatic sleep or
screen turn-off. Those settings have moved to the Activities settings module, where they can be
configured as part of each individual Activity. Entire pages of power management settings cannot be
controlled through Activities anymore in 6.0 though. If these capabilities ever end up returning,
it will be with a different code architecture and a different UI.

![Inhibit sleep or screen turn-off in the settings for Activities](/images/2024-04-23.activities-kcm-edit.png)

Natalie Clarius has invested lots of time into system tray applets. Instead of a single
"Battery and Brightness" applet and a separate "Night Light" applet, the brightness settings in
Plasma 6.0 are now colocated with Night Light as "Brightness and Color" applet. This leaves the
"Power and Battery" applet by itself, which displays battery status (including peripheral battery,
if your mouse or keyboard supports this) and can configure power profiles on the fly
(if power-profile-daemon is installed).


## New & Improved Functionality

In Plasma 5.x, you were able to configure how long the system should be idle before the display
gets turned off. For Plasma 6.0, Jonathan Haney added a setting to configure a shorter turn-off
duration on the lock screen. You won't be reading long web pages and PDFs on the lock screen so
you might as well avoid distractions and save some power. This setting can be reduced all the way
to zero seconds so that the screen turns off immediately when locking the session.
I spent some time to make this work reliably on both Wayland and X11.

![Turn off screen when locked in Energy Saving settings](/images/2024-04-23.powerdevil-profiles-dpms-when-locked.png)

We still need to streamline the settings UI a bit, in line with the "simple by default, powerful
when needed" mantra.

Brightness can be increased or decreased in 5% steps by pressing brightness up/down keys, which are
usually found on laptop keyboards. In Plasma 6.0, brightness steps can also be controlled in
1% steps by pressing Shift in addition to the brightness key.

We merged a number of bug fixes to improve reliability of automatic brightness adjustments.
Restoring keyboard backlights works better now after waking up from system sleep, dimming after a
period of inactivity is less fragile, fading to black before the screen is turned off won't flicker
when a mouse move interrupts the fade at the wrong time. If (lib)ddcutil or PowerDevil's use of it
makes trouble with controlling external monitor brightness via DDC/CI, it can be disabled by
setting a `POWERDEVIL_NO_DDCUTIL=1` environment variable for the PowerDevil service on startup.

![On-screen display for switching power profiles](/images/2024-04-23.power-profiles-osd.png)

There is now an OSD switcher for power profiles (i.e. Power Save, Balanced, Performance) that will
pop up when the battery function key on some laptop keyboards is pressed, or Meta+B.

In Plasma 5.27, you could configure a custom script to run when entering or exiting a power state
(AC, battery, low battery) or after a period of inactivity in this current power state.
In Plasma 6.0, you can configure a custom script for each of these "run conditions" in each
power state. So you can run a script to activate something when you plug in the laptop, and disable
it again when removing the power plug.

![New sleep mode drop-down in Energy Saving settings](/images/2024-04-23.powerdevil-profiles-sleep-mode.png)

Auto-suspend after inactivity, laptop lid closure and power button presses now each allow you to
select "Sleep" vs. "Hibernate" as their respective suspend actions. The "Hybrid sleep" option and
the confusingly placed "Sleep, then hibernate after a period of inactivity" checkbox are now part
of a separate drop-down that lets you choose alternative sleep modes. Less confusing than before,
although there's more work needed now to make this updated categorization consistent across the
entire desktop, like on the logout screen.


## Functionality that just keeps working!

For years, the UPower daemon had marked its laptop lid detection functionality as deprecated, to be
removed in a future release. Plasma's power management service relies on UPower, and Plasma would have
soon lost the ability to react to lid closure events. UPower had already removed this code from
their unreleased development codebase, it was only a matter of time until the next official version
would be released to break lid handling everywhere. So I started working on a replacement,
but getting full coverage across different platforms (X11 vs. Wayland, systemd vs. not-systemd)
is quite a bit of effort and there were lots more kinks to figure out.

So I am very happy that UPower has indeed
[released a new version](https://gitlab.freedesktop.org/upower/upower/-/releases/v1.90.4),
one that puts the lid detection code back in for the benefit of upstream users such as Plasma,
GNOME and various window managers that have also relied on their lid detection functionality.
Better yet, it's meant to stay this time around. Things will just keep working as they do now!
It's great to have this kind of system-level functionality available in common infrastructure
such as UPower so we don't each have to implement substandard knock-offs individually.

On the monitor brightness front, recent versions of libddcutil have introduced a feature that
prevents two independent programs from talking to the same monitor simultaneously. Due to the way
that our background service has traditionally been using libddcutil, Plasma 6.0 sessions have been
hogging libddcutil's monitor handles. Users who like to perform other operations with DDC/CI,
for example through the `ddcutil` command-line program, would find it non-functional. Starting with
Plasma 6.0.4, our DDC/CI code was restructured so that a monitor is only hogged for the short time
it takes to change the brightness. `ddcutil` works again as intended in a Plasma session.


## Beyond 6.0

The System Settings module will be renamed from "Energy Saving" to "Power Management". This used to
be the name of the entire category containing the "Energy Saving", "Advanced Power Settings" and
"Activity Power Settings" modules. After the redesign, there is only one module left and it can be
called by the arguably more appropriate name.

After the Plasma 6.0 feature freeze went into effect, Bogdan Onofriichuk significantly improved
the code for external monitor brightness handling via DDC/CI (using libddcutil).

Starting with 6.1, the power management service will recognize at runtime when monitors are
connected or disconnected. Previously, only a single monitor detection was done on startup and
if you changed your monitor configuration afterwards, brightness controls in Plasma would have
no effect on newly plugged-in monitors.

Brightness via DDC/CI will not be animated anymore in order to minimize the risk of shortened
monitor lifespans. The chance of this happening is hard to verify for someone not in the
monitor industry, but we'll follow the example of monitor manufacturers' own helper applets and
apply newly adjusted monitor brightness values only after a half-second delay.

Brightness animations of internal laptop backlights, on the other hand, are going to get more
responsive when dragging the brightness slider in the applet or holding down the brightness key.

We've done more work on cleaning up the display code, with the goal of allowing per-monitor
brightness controls as opposed to the single brightness slider of current Plasma releases.
After raising the minimum brightness value to "the lowest value that's higher than 0%" to stop
screens from inadvertently turning off, we'll lower it back down to an exact 0 where it's safe to
do so. We also want to move the core of screen brightness control into KWin, which will be able to
do more awesome things such as adjust brightness on OLED screens that don't even have a backlight
to control. No guarantees when any of this will be ready to ship, but it's a lot closer now than
just a few months ago.


## How You Can Help

There are a lot of edge cases and system configurations that could still be handled in a better way.

Device interactions can be hairy, they will often differ from one system to another. What might
work well on my system can break on someone else's. You can use the power of having a
different setup to do things that a number of KDE developers cannot. If you're running into issues
with suspend, brightness and other things that PowerDevil may have a hand in, check out
[the new README file](https://invent.kde.org/plasma/powerdevil/-/blob/master/README.md).

Consider helping with bug triage! PowerDevil on KDE's bug tracker has a [service component](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO&component=general&list_id=2670705&order=changeddate%20DESC%2Cbug_status%2Cpriority%2Cassigned_to%2Cbug_id&product=Powerdevil&query_format=advanced)
with lots of reported bugs, as well as a [component for the System Settings module](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO&component=kcm_powerdevil&list_id=2670714&order=changeddate%20DESC%2Cbug_status%2Cpriority%2Cassigned_to%2Cbug_id&product=systemsettings&query_format=advanced)
with fewer bugs and quite a few wishlist items. The service component includes some very old bug
reports which may or may not fully apply to Plasma 6.0 anymore.
[Read this](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging) if you're interested in
helping us determine the amount of developer love still required for different bug reports.
Also if you rock at figuring out how to reproduce issues reliably, when the original reporter
wasn't able to find a conclusive pattern by themselves.

And of course, if you're a developer, feel free to fix a problem and submit a merge request.
Some issues aren't straightforward to resolve. We'll work through them eventually.
Probably. I'm a volunteer just like most of you, so no promises whatsoever.

You can also [donate to KDE](https://kde.org/community/donations/) so they can continue to do
helpful things like paying Natalie to work on hardware integration, or (if the approval gods
are kind) hopefully subsizing my travel costs to [Akademy 2024](https://akademy.kde.org/2024/).

Discuss this post [on KDE Discuss](https://discuss.kde.org/t/powerdevil-in-plasma-6-0-and-beyond/14548).
