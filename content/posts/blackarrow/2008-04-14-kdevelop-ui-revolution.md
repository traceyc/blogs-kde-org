---
title:   "KDevelop - UI revolution"
date:    2008-04-14
authors:
  - blackarrow
slug:    kdevelop-ui-revolution
---
I'm currently at the KDevelop sprint at the Trolltech offices in Munich, Germany.  We had a productive weekend of talks to bring everyone up to speed with the current state of KDevelop, and while what's going on under the hood is very exciting, it currently suffers from a kluged together, buggy and incomplete UI.  For the first day of the programming several of us are working on improving the user interface, and it's making progress slowly but surely.  Stay tuned for a screenshot / screencast when we have it looking just right :)