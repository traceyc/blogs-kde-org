---
title:   "Hard Knock Outs"
date:    2005-08-19
authors:
  - daniel molkentin
slug:    hard-knock-outs
---
Don't you love it when all your hardware goes on strike almost at the same time?

On Wednesday, my workstations secondary harddisk went on strike, eating (among other things that I had mostly backed up) a report I was working on for quite some days. All lost, sucks. I suspect bad sectors might be the cause. I will get a new harddisk after aKademy, for now I am living with only one.

Just today, 3 workdays before I am leaving for akademy, one my laptops hinges broke. I checked warranty, and the HP website told me that it expired two days ago. Then I called the hotline, faxed the Invoice, and now my warranty is extended until end of this month (the proper expiration date: one year after I actually bought it), and they promised me to send the stuff until monday, so I can fix my laptop before akademy. At least some good news since aKademy without laptop seems a bit pointless...<!--break-->