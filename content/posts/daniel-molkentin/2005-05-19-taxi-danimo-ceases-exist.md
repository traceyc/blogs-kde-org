---
title:   "Taxi Danimo Ceases to Exist"
date:    2005-05-19
authors:
  - daniel molkentin
slug:    taxi-danimo-ceases-exist
---
Last Friday (the 13th) was literally a black Friday for me. My car broke in the middle of my ride home and is broken beyond repair. Right on the freeway, involving a police car to protect me, my car and the ongoing traffic - the details would fill up two or three blog entries, but I'll spare you that stuff. So I'll get myself a BahnCard 50 now and try to be mobile by train. *cough*

Anyway, that means there will be no Taxi Danimo for the time of being. It was quite convinient for fairs and other KDE Events, but that's not possible anymore for the time of being. And for those who feared my driving skills: It's over now, you can get out from whereever you were covering ;)