---
title:   "Basic Desktop API"
date:    2005-05-19
authors:
  - daniel molkentin
slug:    basic-desktop-api
---
Kurt has <a href="https://blogs.kde.org/blog/418">an interesting point</a> on a common api for basic desktop usage, hidden under some stuff I basically agree with but don't think it's easy to solve: A minimal desktop API for applications that do not use DE libs directly. In fact, Matthias, Scott and me discussed a solution to a similar problem back in Nove Hrady. We were wondering how to enable the use of KDE widgets in Qt applications.

For that we came up with a very thin C API (which is probably still in Scotts drawer) which can be reimplemented by several providers. Handling e.g. file dialogs is trivial, and even printing is sort of easy: the preprocessed printing (read: postscript) data would be pushed into a unix socket provided by the API. Of course it gets trickier when looking at details, but that's a second iteration :)

That would give us native dialogs for the most common dialogs:

<ul>
  <li>Generic Yes/No/Cancel Dialogs</li>
  <li>Save (as)...</li>
  <li>Open...</li>
  <li>Print...</li>
</ul>

Of course more are possible. On the technical side, it's an interesting question on wether those providers should be DCOP/DBUS services with the API being an interface definition or if we should go for a plain C interface, which could be wrapped by other languages. The advantage of the latter is flexibility, the problem with the former is that every application would need do use whatever IPC we use. I don't think we should enforce the use of IPC, but I am open for a discussion here.
<!--break-->