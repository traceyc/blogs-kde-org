---
title:   "Delayed initialization"
date:    2004-07-22
authors:
  - daniel molkentin
slug:    delayed-initialization
---
"Wow, Kontact is starting up much faster now" many of you might say after testing a recent CVS snapshot. While we of course are steadily improving speed, this perception of huge speedup is due to the delayed initialization trick: Kontact starts - the mainwindow almost instantly appears, showing the component icons. The splash screen remains, showing a progress bar, until everything is finished.

Funny enough, Kontact in fact starts amazingly fast, even if you stop trusting your perception and take your stop watch:

First startup: about 3 secs
Other startups: about 1 sec

(On an AMD Athlon 2600+)

Please note that during that time, Kontact preloads the kmail part and loads the summary part! Now lets just get rid of the remaining bugs, and make Kontact kick ass. :)