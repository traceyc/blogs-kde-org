---
title:   "All I want for Christmas..."
date:    2005-12-11
authors:
  - daniel molkentin
slug:    all-i-want-christmas
---
... is love, peace and <a href="http://eng.iaudio.com/product/product_X5_feature.php">this little bugger</a>. The iAudio X5 has all I really want: 20 or 30 GB of harddisc, a color display, and plays mp3, wma, ogg vorbis and even flac (and more, e.g. mpeg4 video, but I'm not sure I really can make use of this feature, given the tiny display). It has a high quality microphone build-in and can record to mp3. So that'd be a good alternative to an iPod, which I don't want since it's not capable of playing oggs. It provides USB host support and thus allows for plugging in cameras in order to serve as intermediate storage for camera data. The X5 costs 280 Euro cheapest for the 20 GB version or 330 Euro cheapest for the 30 GB version.

PS: I should add that the X5 has an FM module included and it can record from FM to mp3.<!--break-->