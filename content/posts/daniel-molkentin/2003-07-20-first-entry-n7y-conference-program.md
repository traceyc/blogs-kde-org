---
title:   "First Entry, N7y Conference Program"
date:    2003-07-20
authors:
  - daniel molkentin
slug:    first-entry-n7y-conference-program
---
So this is my first blog entry. I will try to add something every now and then if it makes sense. Let's see how useful it becomes. I think it has great potentials. Number two on my wishlist would be a wiki on developers.kde.org. Anyone? :)
<br><br>
I finally <a href="http://events.kde.org/info/kastle/conference_program.phtml">got the program out</a> and everyone seems to be happy about it. To all that are wondering: "reserved" really means "reserved" on the slots that are marked as such They will be filled until the conference starts. 
<br><br>
I really hope everyone will enjoy the conference. Hope to see you all in nove hrady!
<!--break-->