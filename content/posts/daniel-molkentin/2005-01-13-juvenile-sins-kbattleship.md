---
title:   "Juvenile sins: KBattleship"
date:    2005-01-13
authors:
  - daniel molkentin
slug:    juvenile-sins-kbattleship
---
As some might know, KBattleship was Niko and my first application in KDE CVS. Today, I saw some commits to it again. It's unbelieveable that people kept working on it, even added new features like the DNS-SD support that was recently checked in. I was looking at the code and felt totally embaressed - ill-named classes, heavy pointer abuse, unused layouts, lack of appropriate data structures, just to name a few of our sins. So I want and fired up vim.

Some quick class renaming ended up in actual refactoring efforts with half a dozend commits, even though I didn't dare to touch the really broken things (class releations, class concepts, changing datastructures) yet. This might happen in the future, but it's not a priority. After all it works and it has worked since 2001.

It was fun to work on it again, and it's a nice game conceptually. That said, it could use some artistic TLC. If an artist is reading this: Mail me!