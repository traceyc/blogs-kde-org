---
title:   "LWE Frankfurt"
date:    2005-11-18
authors:
  - daniel molkentin
slug:    lwe-frankfurt
---
Due to obligations at Uni, I was unable to attend all days of Linux World Expo and Conference in Frankfurt this year. However I spent most parts of Tuesday and all of Thursday at the the KDE booth.

Due to the more business-focused nature of the fair, most of visitors were consultants, administrators and developers. Many people asked about our position on KDE for use in the public sector and companies, our plans for the future and technical questions.

When Kurt demoed Klik on SUSE 10.0 or even spontaniously created packages, quite some jaws dropped on the floor. Other presentations included KDE on Kubuntu as well as a designated demo point that showed KDE in the office environment with Kontact and KOffice.

Bernhard Reiter received the Linux New Media Award for the "Best Groupware Solution" on behalf of the Kolab team and Martin Konold and me conducted a successfull Kolab tutorial.

The Trolls showed Qtopia and Qt 4.1 at their booth. I had the pleasure to finally meet Scott Collins and some of the other Trolls I hadn't met before. Scott is a weirdo, but he's really funny.

On Thurday evening after packing, the Trolls joined us for dinner in an italian restaurant in Frankfurt close to the fairgrounds. At around 11:00 I took the train home to Bonn. If you ever get the chance to ride on a new ICE 3 train going 300 km/h at night next to an autobahn, seize it! The view from the panorama window is amazing. I need a decent camera...
<!--break-->