---
title:   "DCOP ported to Qt4; Arthur eyecandy"
date:    2005-05-11
authors:
  - daniel molkentin
slug:    dcop-ported-qt4-arthur-eyecandy
---
<a href="http://developer.kde.org/~danimo/screenies/arthur_purty.png"><img border="0" class="showonplanet" align="right" src="http://developer.kde.org/~danimo/screenies/arthur_purty_small.png" /></a>
As already pointed out by others, we started porting kdelibs to Qt4. Thanks to the restless work of <a href="http://www.cs.cornell.edu/~maksim/blog/">Maksim</a>, DCOP now compiles and works, at least the client part of it. He managed to restore compatibility with the protocol used in KDE3, so we now have some means of posting stuff between toolkit versions. 

The other module that's ported is kdefx and thiago's new network classes, quit a lot of stuff in kdecore already compiles, too. kdeui and parts of kdecore are going to be very tricky. While this is mostly a pure porting effort, we already burried some methods and classes deprecated since KDE2 and earlier, such as the kde:KExtendedSocket and friends. For those who are curious or want to help: you'll need a recent Qt 4 snapshot, a patched unsermake and the ported kdelibs. All of this can be found at http://websvn.kde.org/branches/work/kde4/ (<a href="http://anonsvn.kde.org/branches/work/kde4/">anonsvn link</a>). 

Note that you need to use this version of unsermake atm, neither automake+am_edit nor oder versions of unsermake are supported.You will also need a set of environment variables set, e.g. LD_LIBRARY_PATH in order to have Qt4 find its libs. Once that works for you and you compiled Qt4 and successfully ran configure on kdelibs, you can start porting. To spare you some frustration I recommend to sync up with us on IRC.

In other news, last nights Qt snapshot showed some new fancy demos for <a href="http://doc.trolltech.com/4.0/qt4-arthur.html">Arthur</a>, including a very own widget style that make use of the alpha channel and anti-aliasing (See screenshot). To use a word I recently learned from sarah03 on <tt>#kde-devel</tt>: purty! Way to go Trolltech! 
<!--break-->'