---
title:   "21c3 roundup; Kontact hacking; Qt4; New Year"
date:    2004-12-31
authors:
  - daniel molkentin
slug:    21c3-roundup-kontact-hacking-qt4-new-year
---
I spent the last days in Berlin, joining the folks at the <a href="http://21c3.ccc.de/wiki/">21. Chaos Communication Congress</a> (dubbbed "21C3" for that matter). Besides attending some highly interesting talks this was a very welcome way to not only meet the "usual suspects" from the KDE and GNOME camp, but also to make new friends, share new ideas and discuss things. The athmosphere was awesome, albeit unfortunately only as in feeling, not as in breathing (unless you like lung cancer).

The unfortunate part was when I was leaving early on thursday to catch my flight. I messed up with the trains (took the S-Bahn instead of the express train): When I arrived at the airport, flight boarding was already closed. So I had to take the train and I was flying with a cheap airline, I had no means of returning the ticket or even get it partially refunded. Those 50 Euros could have served a better purpose...

The good thing that I got a lot of work done during the six hours that I spend in the train Yay for power plugs in long-distance trains! I am still in progress of cleaning up some of the patches for Kontact I wrote during the ride. I felt a bit guilty since all KDE-related work I did at the congress was filing Qt4 issues with qt-bugs (and Beineri is still ahead by one I think...).

That said, Qt4 is just awesome. I played around with it quite a lot now and I like it more and more, even though the implementation still contains some rather critical bugs. But as I kind of expected this, there is nothing to rant about. I chose to play the early bird after all :)

With Qt3 on the other hand, I ran into problems when I wanted to use a QComboBox with an abstract item representation. QComboBox in Qt3 just can't do that, so I went with storing the data in an extra array. Qt4's QComboBox implemementation on the other hand uses Interview, and thus a slightly modified MVC approach, which would have worked a lot better in my case.

What else? Uhm, oh yeah, it's close to new years eve again. Man, time flies... or is that realword thingy leaking resources again? Yeah I know it's one buggy piece of crap, but someone please valgrind it? Someone? No? Really not?

Anyway, I want to use this opportunity to thank everyone contributing to KDE and of course especially the KDE PIM team. You all did an awesome job. Keep up the excellent work, no matter how unthankful your job may be. It's important and very much appreciated.

So, what will 2005 bring? A successfull year for everyone of us I hope, and thus a success for the project.

<b>A happy and successful Year 2005!</b> May your dreams and wishes come true.