---
title:   "Wikipedia for everyone; More Internet for me"
date:    2005-04-20
authors:
  - daniel molkentin
slug:    wikipedia-everyone-more-internet-me
---
<img align="right" class="showonplanet"  src="http://developer.kde.org/~danimo/screenies/knowledge_mini.png" />
So i have toyed around with Qt4, which led to a small application called "Knowledge" (Screenshots: <a href="http://developer.kde.org/~danimo/screenies/knowledge_preview.png">1</a>, <a href="http://developer.kde.org/~danimo/screenies/knowledge_preview2.png">2</a>, <a href="http://developer.kde.org/~danimo/screenies/knowledge_preview3.png">3</a>). Once it's grown up, Knowledge is supposed to become a <a href="http://wikipedia.org">Wikipedia</a> offline reader. Right now I need to find a good indexer and an easy way to generate HTML from Wiki markup (which is not trivial, since this also requires a TeX parser and a parser for special metadata, i.e. for drawing timelines). I'll probably hack up MediaWiki to work as a preprocessor (which I already started with, but it's a all a big mess right now)
<p>
The second limitation is QTextBrowser: It's nice for enrichted text, but it's still fairly buggy and even once this is fixed, I really need a proper browser at the end of the day  with whistles and bells. Maybe this will motivate me to help porting KHTML once we're getting there.
</p><p>
The indexer part is harder. There is CLucene, but it refuses to link properly and I haven't found enough time to tackle this. Even the examples fail to link. Doesn't anyone use that stuff?
</p><p>
In other news, I will soon enjoy the power of a 2 MBit/s ADSL line. The <a href="http://www.congster.de">new contract</a> is even significantly cheaper than the old one, which only allowed 1 MBit/s. I could now even go up to 3 MBit/s, but that's overkill most of the time, and would cost slightly more. Let's see how long it takes until T-Online regrets this offer :).</p>
<!--break-->