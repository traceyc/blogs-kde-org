---
title:   "ALSA dmix for HP nx5000"
date:    2004-11-08
authors:
  - daniel molkentin
slug:    alsa-dmix-hp-nx5000
---
<p>You got one of those nice <a href="http://h10010.www1.hp.com/wwpc/us/en/sm/WF05a/321957-64295-89315-321838-f33-395654.html">HP nx5000</a> with SUSE 9.1 preinstalled at <a href="http://conference2004.kde.org">aKademy</a>? You were annoyed because the stupid intel chipsets could not handle mixing and the sound device was permanently busy? No more!
</p>
<p>
Simply place the following code into your home directory as <tt>~/.asoundrc</tt>, restart artsd and enjoy your newly-gained software-mixing.
</p>
<code>
pcm.ossmix {
    type dmix
    ipc_key 1024          # must be unique!
    slave {
        pcm "hw:0,0"
        period_time 0
        period_size 1024  # must be power of 2
        buffer_size 8192  # dito.
    }

    bindings {
        0 0   # from 0 => to 0
        1 1   # from 1 => to 1
    }
}
pcm.!default {
    type plug
    slave.pcm "ossmix"
}
··
pcm.dsp0 {
    type plug
    slave.pcm "ossmix"
}

ctl.mixer0 {
    type hw
    card 0
}
</code>

If you need more help, you might want to take a look at <a href="http://opensrc.org/alsa/index.php?page=DmixPlugin">this very helpful page</a> which I derived my configuration from. you can now happily use <a href="http://developer.kde.org/~wheeler/juk.html">JuK</a>, <a href="http://amarok.kde.org">amaroK</a>, <a href="http://kaffeine.sf.net">Kaffeine</a> and <a href="http://www.mplayerhq.hu">mplayer</a> aside for the full monty of multimedia expirience.