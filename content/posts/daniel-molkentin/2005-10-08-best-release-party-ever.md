---
title:   "Best release party ever!"
date:    2005-10-08
authors:
  - daniel molkentin
slug:    best-release-party-ever
---
This friday I went to the meeting of SUSE beta testers to the <a href="http://www.suse.de/de/company/suse/directions/">SUSE headquarters</a> in Nuremberg. The travel went pretty smooth and so I soon checked in and arrived in a meeting room with all the other testers. We got a nice introduction to the openSUSE project, covering the past, the status quo as well as future plans. Greg from Novell as well as Adrian, Sonja and Andreas from SUSE gave me a good feeling about the project's future. 

I bet there will be updates and details about our discussions in blogs and on <a href="http://opensuse.org">opensuse.org</a>, soon. At this point, there are just ideas that will be tested for feasibility and then discussed on opensuse.org, such as ideas about a future open build server.

In the evening after a lot of fruitful discussions, we joined SUSE people on their release party. It was really nice. Lots of food and free drinks. I bet only very few people stayed sober and even met some people that I didn't know yet, such as Duncan. The brave SUSE trainees ran the bar. Thanks guys!

Around midnight, a cuban guy with a bongo drum stepped by and someone dragged him into the party location. Soon, our very own Duncan took over the instrument. He's is a natural talent playing on the bongo drum. His performance was amazing even  though he claimed he only played it once before. Being from latin America works wonders when it comes to rythm

As a nice surprise, Jan from openusability.org and his gf Jutta stepped by. It was easy for them since they were staying in Erlangen, which is basically around the corner, even if Coolo doesn't seem share this opinion :).

At around 2am in the morning, a couple of developers and beta testers decided to switch locations. We ended up in the "downtown"-bar, a really small location that is in fact located in downtown Nuremberg. We enjoyed the great atmosphere until we noticed we ended up at a "bad taste party". It was nevertheless really cool (What does that tell about us?...). The music was a rather wild but interesting mix of all styles. The bartender girl was pretty hot though and tried her best to prove that. After a lot of partying and a couple of drinks they kicked us out at around 5am since they wanted to close.

I ended up in bed at around 6am, and for that reason I was unable to attend the sightseeing tour organized by the SUSE folks. It's a real pity, but Tackat got me an express version when we went for the train station this evening. Today, we seized the rest of the day to hack on Globepedia. I finished the windows port and the Installer. Now I am sitting in the ICE back to Bonn, trying to create SUSE rpms. When you read this, that probably means I arrived at home.

One thing is for sure: SUSE guys don't only know how to make a cool distribution, they also know how to party!
<!--break-->