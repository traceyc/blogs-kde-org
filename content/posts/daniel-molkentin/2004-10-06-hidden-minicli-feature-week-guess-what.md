---
title:   "Hidden minicli feature of the week: Guess what..."
date:    2004-10-06
authors:
  - daniel molkentin
slug:    hidden-minicli-feature-week-guess-what
---
Today I was told by a friend that minicli contains a... calculator. Just press Alt+F2 and type e.g. 23-5. Then press enter. Seems like there were alterantives to kcalc that predate the current calcualtor war. Now: anyone willing to implement a solver for differential equations? ;-)
<!--break-->
