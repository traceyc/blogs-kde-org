---
title:   "Draw your conclusion"
date:    2005-01-21
authors:
  - daniel molkentin
slug:    draw-your-conclusion
---
Ok, this is just in: The town of vienna is <a href="http://www.pro-linux.de/news/2005/7734.html">migrating to Linux</a>. Contrary to Munich, they are doing a "soft migration", which means they will only migrate 4.800 workplaces to Linux out of 7.500 which will be running OpenOffice.org independant of the OS. This is a lot more cost-efficient for them. They have no migration pressure, as their support contacts with Microsoft are running until 2010, yet they do want to migrate.

This creates something called "customer demand" and makes vendors think about porting their software to the demanded platform. A hard migration is a lot more expensive than such a soft migration and there has more to come than some software projects not willing to port their software to make that happen.

If you were about to say "but they will stay with windows then", you should wonder about the reasons. Does Linux still suck too much? Well bad luck for Linux, you're not goona win an IT deciders heart with Stallman talk, when a hard migration is that much more expensive.

We need to be competative I agree, and I also agree that we should focus on Linux and other Free Software driven operating systems. Still I think actively ignoring Windows or even discourage porting isn't gonna help us -- as soon as someone's reporting for duty for a "KDE Windows Platform Maintainer" job or similar. That said, I don' think there is much left to do for applications like those in kde-pim, we even have means to make the libs much more plattform agnostic, which is a good idea anyway imho.

There is something called "<a href="http://www.pi-sync.net">PIM/PI</a>" (Plattform independant) already which, while being the least common denominator, is quite helpful in its current state. It's not exactly an Outlook replacement but it's a cool pim suite for the Zaurus and a swiss army knife on Windows. It saved my life once more today and I am really happy to have it.