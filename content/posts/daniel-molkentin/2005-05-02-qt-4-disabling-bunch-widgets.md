---
title:   "Qt 4: Disabling a Bunch of Widgets"
date:    2005-05-02
authors:
  - daniel molkentin
slug:    qt-4-disabling-bunch-widgets
---
I just had a problem with Knowledge, which as you might know, is my attempt towards a Wikipdia offline reader: I needed to disable lots of GUI stuff in my QMainWindow-derived class as long as no offline image is loaded. Some of the widgets werent even available as object members. All widgets need to be deactivated and then activated again after the book was loaded. The obvious choice would have been to add a bunch of members, one for every object in question, and then call <tt>setEnabled( true )</tt> or <tt>setEnabled( false )</tt> for each of them respectively. Result: lotsa code. Lotsa members, sucks.
<br/><br/>
So what was my solution? Obviously, all relevant classes in this case are really QWidgets (except for QActions, I'll get to them later). So what I did is having a private member <tt>QList < QWidget* > mStateWidgetList</tt>. Every time a widget shows up I would add it to the list. Tackeling a bunch of them is just as easy:

<code>mStateWidgetList << firstWidget << secondWidget << ... ;</code>

At the end of the constructor I call

<code>foreach( QWidget *w, mStateWidgetList )
 w->setEnabled( false );
</code>

And the same with "true" as argument at the place where stuff a book gets loaded. Very simple, looks clean and no need for ugly global pointers just for the purpose toggeling the enabled-state. The performance penalty due to dynamic nature of this solution shouldn't be too bad either. 
<br/><br/>
The only obvious downside of this approach of cource is that it won't work with QActions, which can also be enabled or disabled. You'd need to special-case those, qobject_cast attempts are your friend here. In this case, this should be a nice enough task for a little helper class. I just think it rocks. Now I am waiting for someone to tell me how he had the same idea back in 1998 :).
<!--break-->