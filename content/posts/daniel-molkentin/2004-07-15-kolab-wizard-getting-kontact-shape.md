---
title:   "Kolab Wizard, Getting Kontact into shape"
date:    2004-07-15
authors:
  - daniel molkentin
slug:    kolab-wizard-getting-kontact-shape
---
Just in time for the feature freeze, I managed to finish the Kontact wizard. I finally took the time to understand the concepts behind the framework that Cornelius orgininally came up with and I really start to like it. The current version contains some known bugs, but I will address that in the next days.

Another hot topic are bugs. The Kontact framework has <a href="http://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&field0-0-0=product&type0-0-0=substring&value0-0-0=kontact&field0-0-1=component&type0-0-1=substring&value0-0-1=kontact">plenty of them</a>, but quite a lot are probably invalid or outdated, need checking or are actually bugs of the components, most others should be fixable without breaking the string freeze.

Currently around 1/3 of all bugs are wishes and there are only 8 crashes, so I am confident kontact is soon in good shape. If you disagree, feel free to file more bugs or even better: send a patch!

The only tough thing will be writing a replacment for the currently used merged config dialog from kdelibs. But either we add a replacement or disable the merged dialog alltogether again for KDE 3.3. Currently it's a usability nightmare.