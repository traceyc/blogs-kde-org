---
title:   "Chemnitzer Linux-Tage 2006"
date:    2006-03-09
authors:
  - daniel molkentin
slug:    chemnitzer-linux-tage-2006
---
People have been wondering what I have been up to all the time. Now let me tell you: For one uni and a project consume most of my time at the moment. It should get better as the time of exams has passed by (which is on the beginning of april). But even then expect me to have only limited time available until may.

Now for what I've been up to on KDE things: I've been organizing the KDE presence at <a href="http://chemnitzer.linux-tage.de/2006/">Chemnitzer Linux-Tage</a> this year. We showed off Kubuntu Dapper Drake and gave away Breezy CDs. Beineri showed XGl on his SUSE 10.1 desktop (this works just fine with dapper, btw). People were amazed.The GNOME guys showed the official Novell videos but the crowd usually surrounded the joint KDE/Kubuntu/Klik booth. There's nothing like a live demo.

I also gave a presentation on KDE and the future of the Linux desktop (<a href="http://chemnitzer.linux-tage.de/2006/vortraege/detail.html?idx=424#folien">slides</a>) which was apperantly well received by about 200 people sitting in the audience and a couple of people listening to the live stream (the audio-stream will hopefully be available for download soon).

Additionally, I had the chance of giving a three hour workshop on Qt 4 (<a href="http://chemnitzer.linux-tage.de/2006/vortraege/detail.html?idx=425">slides</a>) with 23 participants. Interestingly, all attendants finished the tasks on schedule, which is pretty good given that this was my first Qt workshop and I had a fairly limited idea of my audiences C++ skills. Thanks to tackat for his workshop slides and for the review of my slides.
<!--break-->