---
title:   "Kolab Express"
date:    2004-07-15
authors:
  - daniel molkentin
slug:    kolab-express
---
While Kolab 2 promises an easy installation, I always thought <a href="http://www.kolab.org">Kolab</a> 1 was a bitch to set up. When I tried to set it up and build it from scratch, it took me more than a day and it was really bad. The fortunate thing is: It's not any longer a pain to get your Kolab running. You can now set up a Kolab Server while watching the evening news.
<br><br>
"How does this work out?", you might ask. Well, the fine guys at <a href="http://www.zfos.org/">ZfOS</a> have made Kolab installation much more convinient. They have packages available for all mainstream distributions. Even if your Distribution is not covered, their "obmtool" (<i>OpenPKG poor man's Boot, Build & Management Tool</i>) makes it easy to setup an entire Kolab in three easy steps:

<ol>
<li>Get the packages (one-by-one or in a convinient tarball)
<li>run <i>./obmtool kolab</i>
<li>run <i>/kolab/etc/kolab_bootstrap -b<i>
</ol>

Now you can already fine-tune the server the webinterface at <i>https://server/admin/</i> or attach your Clients.
<br><br>
Installation on my SUSE 9.0 box (including the bootstrapping process which does the setup) took 15 minutes. Immediately I was able to browse the admin interface.
That's a radical improvement over the full day I spent before. As a matter of fact, Kolab2 uses obmtool, too. Thanks ZfOS!