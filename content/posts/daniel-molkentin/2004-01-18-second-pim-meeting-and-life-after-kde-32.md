---
title:   "Second PIM Meeting and the life after KDE 3.2"
date:    2004-01-18
authors:
  - daniel molkentin
slug:    second-pim-meeting-and-life-after-kde-32
---
Hmm, haven't written something in a while, but now that KDE 3.2 is tagged and branched, I think I should give an update. Cornelius has already described everything that comes into my mind these days (and yes, picturing Zack without dreads scares the hell out of me as well...). 

So far we already merged pretty much all the funky stuff we did in osnabrueck from osnabrueck_branch to HEAD, which is open for feature commits again.

I am currently in the progress of organizing the <a href="http://www.tu-chemnitz.de/linux/tag/2004/workshops/meetings/kde.html">KDE PIM Groupware meeting</a> at the <a href="http://www.tu-chemnitz.de/linux/tag">Chemnitzer Linux-Tag</a>. We will meet with some folks from groupware server projects and get some work done. This will be very exciting to see how it goes. It seems quite some projects are interested in joining. I expect not all PIM developers will be able to make it, but I look forward to meet Zack, Don and the others at the KDE Hackers meeting later this year (But only if Zack does something about his hair style until then ;) )

And there is more excitement on the way: Hamish Rodda is working on improvements for IMAP. It's highly experimential, but if it works out, it will be a nice speedup. KPilot integration in Kontact also looks really nice already. Once I dig out my old Palm I might post a screenshot. Bo merged in the Free/Busy view in KOrganizer. Most applications also use KConfigXT and kcmodules now for their config files, so it will hopefully very easy now to compile a intuitive merged configuration view in Kontact.

And finally the directory cleanup is really a relief to everyone who wants to have a quick overview over kdepim: kdepim/kresources now contains the currently available resources for both addresses and calendars (currently for eGroupware, Kolab (imap) and Exchange). Previously things used to be spread in evolutionary-grown places all over the module.  Thanks to the janitors!

We already achieved a lot, given that the really focused and coordinated work on KDE PIM started only one year ago. And if we implement only 2/3 of the features of the <a href="http://developer.kde.org/development-versions/kdepim-3.3-features.html">KDE PIM 3.3 features plan</a> as mandated by the release criteria, we'll have reached an even more amazing state in only 1,5 years, because we were able to base on existing and proven components. That's the prove that refactoring and step by step improvements really work in a really impressive and exciting way.

Let's see if I can hack in my missing feature of the week: allow to send a to-do by dropping it on the kmail icon. Though I am afraid this will be mostly a matter of finding time to do it as uni currently demands quite a significant amount of time (exams get closer).

