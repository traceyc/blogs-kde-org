---
title:   "New Color in Kontact Land"
date:    2004-07-21
authors:
  - daniel molkentin
slug:    new-color-kontact-land
---
After Till kept bugging me, I finally asked David Vignoni to submit his final versions of the icons, which I then committed to CVS. As a result, Kontact has now finally decent icons. Interested people should take a look. Have a look: 

[image:527]

On related news: I was looking into zacks idea of delayed initialization (http://blogs.kde.org/node/view/509) using a [qt:QTimer]. I will try change Kontact to do delayed initialization. Let's see if that has positive pyscological effects on startup perception.

Update: I tried the hack, and it works, but it's still not impressive. Still I think this is the right direction. Additionally I wanted to get rid of Kontacts own Splashscreen Class, but someone dedicded it might be a good idea to hardcode the Splashscreen size, so I can't use this for Kontact :(