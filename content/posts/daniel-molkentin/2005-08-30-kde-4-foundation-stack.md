---
title:   "KDE 4 Foundation Stack"
date:    2005-08-30
authors:
  - daniel molkentin
slug:    kde-4-foundation-stack
---
<a href="http://developer.kde.org/~danimo/kde4foundation.png"><img src="http://developer.kde.org/~danimo/kde4foundation_small.png" border="0"  align="right" class="showonplanet"/></a>
Ok, so we just had a meeting where discussed the redesign of kdelibs and kdebase. Although we have great technology in there, with Qt4 we have a good opportunity and reason to clean up the foundation of our very own DE.

Basic outlines:

<ul>
<li>In the new structure, things are less tied into each other, leading to an cleaner overall design</li>
<li>Through the libs-components, KDE might attract more developers and ISVs that just want to use some classes. The hope is that they will eventually use even more of them, they are free to chose. In turn, we benefit from new users of our classes</li>
<li>The new components will at least be split in core and UI each, so it will be possible to exploit the possiblities that QCoreApplication offers to people seeking out for console-only development with smaller footprint.</li>
<li>KApplication as it exists in its current form will die. We are currently discussing a couple of alternatives to make sure all KDE stuff is properly initialized without increasing inconvenience for programmers.</li>
<li>The current classes will be moved to a KDE3Support library to ease porting</li>
<li>additionally, kdelibs trunk might be tagged/branched so app developers can continue with raw porting to Qt4</li>
</ul>

<p>The <a href="http://developer.kde.org/~danimo/kde4foundation.png">new design graph</a> outlines the new things pretty well.</p>

Legend:
<ul>
<li>Green: Qt</li>
<li>Blue: Platform independent</li>
<li>Red: Platform dependent</li>
</ul>

<p>Now go, talk about it and spread the word, KDE4 is gonna have an even more rocking architecture!</p>
<!--break-->