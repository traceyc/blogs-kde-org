---
title:   "aKademy here I come"
date:    2004-08-19
authors:
  - daniel molkentin
slug:    akademy-here-i-come
---
So I'll jump into the car in the next minutes. The plan is to arrive at wheels place somewhat before 10. I really hope at least Aaron will open the doors, or else... ;-) We will then arrive in LuBu in the afternoon. Fun, yay! :)

PS: No, the blog entry doesn't really contain too much contents, I just don't want aseigo to feel too lonely on <a href="http://www.planetkde.org">planetkde.org</a>.
<!--break-->