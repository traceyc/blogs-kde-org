---
title:   "\"Is your application enterprise ready?\" - KConfig::writePathEntry()"
date:    2004-11-28
authors:
  - daniel molkentin
slug:    your-application-enterprise-ready-kconfigwritepathentry
---
<p>
This is an attempt to establish awareness of KDE programmers for problems that might arise on large installation sites. I am expiriencing them at Uni from time to time, where several hunderts of clients run a FC1 based KDE installation. So I decided to start a little series that might be an eye-opener for people that usually only code with the computer as a single workstation in mind and as a reminder for those who know about the difficulties that may arise in corporate environments. Feel free to continue this series in your blog or send ideas for more "enterprise development problem" quickies my way.
</p><p>
If you write KDE applications, you probably also have to deal with saving pathes to the users home directory, usually when storing files into your applications custom directory (e.g <tt>(~/.kde/share/apps/myapp/</tt>).
</p><p>
This raises a problem with some enterprise setups which use network filesystems such as NFS. Assuming the server has an nfsexport /data/ which holds all home directories in a subdir "homes", a users home directory can appear at <tt>/nfs/homes/user/</tt> or <tt>/nfshomes/homes/user</tt>, depending on name of the mountpoint on the local machine. This happens surprisingly often even in well-administered environments.
</p><p>
Now if we store the full path to a file using <tt>writeEntry()</tt>, the user would be lost as soon as he uses a differently administered machine with the same home directory, which is of course not valid on this machine and causes all kinds of weired behavior. Now, how do you, the caring programmer, solve that?
</p><p>
Enter <tt>writePathEntry()</tt>. Of course the environment knows about $HOME, otherwise we wouldn't have come as far as starting KDE. Since KDE 3.1.3, KConfigBase and classes inheriting from it have <tt>writePathEntry()</tt>, which as a special offer for KDE programmers, is also available as <tt>writePathListEntry()</tt>. They love to take care of your problem by substituting your local home directory in pathes with <tt>$HOME</tt>, so their lovely counterparts <tt>readPathEntry()</tt> and <tt>readPathListEntry()</tt> can take resubstitute the variable with your current home directory path. Tadaaa! Problem solved.
</p>
<p>
Within KDE, Helge Deller and others have tried to ensure that every application uses writePathEntry where <tt>appropriate</tt>, but if you see one (either in your application or in one of the other applications of either kde module), please don't hesitate to fix this or send a patch.
</p>