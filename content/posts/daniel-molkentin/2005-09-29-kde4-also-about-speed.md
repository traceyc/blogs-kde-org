---
title:   "KDE4 IS also about speed"
date:    2005-09-29
authors:
  - daniel molkentin
slug:    kde4-also-about-speed
---
So Tom's story <a href="http://slashdot.org/article.pl?sid=05/09/29/0210226&">hit Slashdot</a>. A lot of responses, from simply requests to almost flame-like comments asked KDE4 to simply be fast. I feel urged to comment on that:
<ul>
<li>Qt4 gives us a major speedup in first place. The KDE3 codebase that we ported to Qt4 is already faster than "Kanzler" (this seems to be a general positive thing about Qt4, see also <a href="http://blogs.qtdeveloper.net/archives/2005/08/24/qsa-120/">here</a>).</li>
<li>As Aaron has indicated quite a few times during aKademy, Plasma will not only bring a nicer look, but also unify applications that have previously been separated, resulting in a faster-loading, tighter integrated Desktop Environment.</li>
<li>KDE 4 will have eye candy, but it will still work nice and fluent on computers without modern gfx cards. Also, every decent 2D-gfx card will soon be accelerated by Exa, resulting in improved graphics performance.</li>
<li>The granularity of libs will be a lot finer, resulting in applications that only load whatever they need. But with preloading and all the other fancy magic in modern memory management, memory whiners that base their analysis on top &amp; co. should be <a href="http://blogs.kde.org/node/1445">careful about their results</a> anyway.</li>
<li>And finally: There is no hidden agenda or ally with graphics card vendors whatsoever, so we have no interest to make KDE slow, the contrary is, of course, the case. After all, we are also using KDE, and nobody of us likes to wait, either.</li>
</ul>

Conclusion: Yes, KDE 4 will also be about speed, it will hopefully be even speedier than KDE 3, but that's something that future development will show.

<!--break-->