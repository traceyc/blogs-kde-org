---
title:   "LinuxTag is over"
date:    2005-06-26
authors:
  - daniel molkentin
slug:    linuxtag-over
---
I arrived home safely at around 11:30pm with a Thomas, a fellow of my <a href="http://bolug.uni-bonn.de">LUG</a> in Bonn and a translator for kdeextragear. He also took our booth parts in his car, which helped us a lot, thanks again man!

Looking back, LinuxTag was a very interesting event that got lots of press attention, as did our <a href="http://meta.wikipedia.org/wiki/KDE_and_Wikipedia">KDE-Wikipedia cooperation</a>, which was probably one of the most amazing things that we managed to organize. The booth also went very well, thanks to Josef for taking care, it was awesome!

People blamed the organizers for taking an entrace fee for the first time. While I can understand that, I think it's a good way to increase quality for a such an event. If you were to have a cool night in a remote town, it would probably be the same. 15 Euro a day is nothing compared to some 100 Euroish tickets for traditional UNIX-conferences. Plus you could get the tickets for free in advance. 

The social event was the best I ever had at LinuxTag, the contacts were right, and all those who felt they were missing the "community spirit", should communicate their concerns with the organizers. My personal wish: Please fix the A/C, it was way too hot.

Too bad neither SUSE nor Novell had an official booth at LinuxTag. Most comments I heard during the event said that Novell was is hard to ignore the enduser distribution market (being kind of an essential "entry drug") and tries to focuses on business, where it fails badly. I won't comment on it, but it's a pity to see such a great distribution go down the drain in the public eyes. Thumbs up for Martin Lasarsch, who bravely fought in almost-lost terretory. Anyway, <a href="http://www.heute.de/ZDFheute/inhalt/26/0,3672,2326362,00.html">ZDF.heute</a> and <a href="http://www.3sat.de/3sat.php?http://www.3sat.de/neues/sendungen/show/80449/index.html">Neues</a> reported excusively on Ubuntu and Kubuntu (yay!) for the enduser.

On the positive side of things, I met a lot of interesting people and good friends. For instance I had a nice chat with Jasmin Blanchette from Trolltech, whom I lend my Laptop for his Qt presentation.

Also some of my personal friends visited me on the booth. I was especially happy to meet some of the #s2000 Cabal (hi drosera ;)). Anyway, I think that's enough of events for the next weeks. Coming up later this year: <a href="http://www.wikimania.org">Wikimania</a> in Frankfurt, where I will present our technical progress with regard to this weeks announcement, <a href="http://conference2005.kde.org/">aKademy</a> in Malaga and <a href="http://www.linuxworld.de">LinuxWorld</a>, again in Frankfurt.
<!--break-->