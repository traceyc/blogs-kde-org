---
title:   "Linksys WRT54G: GPL'ed software for appliances"
date:    2004-11-13
authors:
  - daniel molkentin
slug:    linksys-wrt54g-gpled-software-appliances
---
<img src="http://www.linksys.com/products/image180/wrt54g v1.1.jpg" align="right" />

I got one of the <a href="http://www.linksys.com/products/product.asp?grid=33&scid=35&prid=601">Linksys WRT54G</a> Routers from aKademy, and I must say it really rocks. It can be whatever you want: Easy to use or a real geek toy. Why? Well, it runs linux :)
<br /><br />
Read on if you are looking for a new router, somthing that runs Linux or just some cool piece of hardware.

<!--break-->
<br /><br />
It is really  well designed from the hardware point of view: It supports <a href="http://en.wikipedia.org/wiki/VLAN">VLAN tagging</a> for all of its 5 ports out of the box, all antenna settings are tweakable, which makes it extremely flexible. The five external LAN ports basically can act as an autonom switching unit, but are connected to the processors ethernet interface. you can assign a vlan to every port, by default four ports are tagged as one vlan and one is in another. 
<br /><br />
Another network port from the CPU is attached to the wifi antenna. Thus, by default you have WLAN, 4x wire-bound ethernet for LAN and one WAN (PPPOE, ethernet or other kinds of broadband access), even though there are only two network devices. In theory, the hardware supports up to 5 vlans, but not all firmware vendors support that. The broadcom processor is basically a MIPSEL CPU with 200 Mhz, only RAM and ROM are limited with 16MB and 8 MB respectively.
<br /><br />
It would have been nice if it offered a USB output just like the according AUSUS router does, which uses the same chipset, and which also is supported by OpenWRT. Unfortunately, that one lacks the switch ports. It would have been a nice way to attach a small harddisk. But well...
<br /><br />
Now we move on to choose a firmware for the WRT54G. There is a variety of different firmware images: The original firmware which is still being maintained very well by Linksys, a slightly modified version, derivated from an oder firmware, the SveaSoft firmware (although their coping with the GPL questionable), all coming with more or less features easily configurable using a web interface. 
<br /><br />
And there is <a href="http://www.openwrt.org">OpenWRT</a>, a pure shell-based firmware which can be accessed by telnet or ssh. Flashing is done by sending the image via TFTP during the reboot phase (usually, you would use an upload form in the routers admin interface). Once bootstrapped, you get a tiny distribution and can start to add your pages, using the "ipkg" tool, originally developed for iPAQ and Zaurus mini distributions. It works similar to apt-get and there is really a lot of Software around already.
<br /><br />
I just switched to OpenWRT because I want it to do a VPN tunnel to various places and I want to keep the freedom to turn off my rather power-consuming workstation. Tomorrow I will configure the counterpart of the tunnel, a debian box, and test if it really works. Apart from that it runs a cron daemon to trigger remote actions on other servers. Again, my workstation is one step closer to be shut down overnight. This would never have been possbile with a propritary router appliance I believe, except for really high-cost solutions maybe.
<br /><br />
<pre>
system type             : Broadcom BCM947XX
processor               : 0
cpu model               : BCM3302 V0.7
BogoMIPS                : 199.47
wait instruction        : no
microsecond timers      : yes
tlb_entries             : 32
extra interrupt vector  : no
hardware watchpoint     : no
VCED exceptions         : not available
VCEI exceptions         : not available
dcache hits             : 4278189947
dcache misses           : 2214928385
icache hits             : 2575943508
icache misses           : 1880329084
instructions            : 0
</pre>

OpenSource rocks :)