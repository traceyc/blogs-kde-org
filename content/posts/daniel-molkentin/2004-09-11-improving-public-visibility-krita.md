---
title:   "Improving the public visibility of Krita"
date:    2004-09-11
authors:
  - daniel molkentin
slug:    improving-public-visibility-krita
---
Krita is a very promising application. Its problem is that the potential user currently has to build all of KOffice only to get krita. 

At aKademy, Ken Wimer told about that at the artists meeting. I first thought it would be the best to get Krita out of the KOffice into kdeextragear or something, but the downside is that it would take a duplication of all the cool koffice libs features, so I had a look into the issue.

I came up with a simple solution: I only checked out the basic direcories: lib, templates and mimetypes. Afterwards, one checks out krita and filters/krita, and runs make -f Makefile.cvs, then I just compiled (yes icecream even rocks on a PowerBook, if one has the apropriate cross-compiler environment) and installed the stuff and it works.

Today, tackat asked me again on how one could get Krita more visible by regulary posting it on kde-apps.org and make it painless for the user to install.

Simple: create a package from the libs (all directories listed above, suse has that already, so it might be worth taking a look into their source RPMs) and create one for krita. Then offer both for download. As KOffice libs are downward compatible, this should be a good way that even distributors could follow. Now if Krita wouldn't need bleeding edge CVS koffice libs (actually I didn't check), then it would be even more easy, as we didn't have to care about versioning of the libs at all.

So if anyone of the Krita guys is reading this: Do you agree that's a good idea? Tackats thesis is that if Krita is easier to install, its development can happen much faster, and I tend to agree, because contrary to kde pim, where you seldomly need only one application, this is not true for Krita, or maybe even other applications. Kexi is already released seperately afaik, and so could be Krita and Karbon, at last as development snapshots.

