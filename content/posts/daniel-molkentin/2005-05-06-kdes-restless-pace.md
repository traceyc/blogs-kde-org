---
title:   "KDE's restless pace"
date:    2005-05-06
authors:
  - daniel molkentin
slug:    kdes-restless-pace
---
Being online for less than 3 days, subversion already shows the rapid pace in which KDE development happens. Starting off with revision <a href="http://lists.kde.org/?l=kde-cvs&m=111519335623926&w=2">#409210</a> after conversion, Stephan Binner just made commit <a href="http://lists.kde.org/?l=kde-cvs&m=111538083622127&w=2">#410000</a>, by fixing ugly-looking disabled icons (by using setIconSet instead of setPixmap</div> ). Maybe we will soon see a run for milestones like in the <a href="http://de.wikipedia.org/wiki/Wikipedia:Meilensteine">german wikipedia</a>? Anyway, way to go KDE!
<!--break-->