---
title:   "[UPDATE] How Dapper LTS Succeeded To Spoil CUPS Printing (Part Three -- Installed on Harddisk)"
date:    2006-06-06
authors:
  - pipitas
slug:    update-how-dapper-lts-succeeded-spoil-cups-printing-part-three-installed-harddisk
---
<hr>
<p><b>Update:</b> Debian packager <a href="http://chistera.yi.org/~adeodato/blog/">Adeodato Simo</a> wrote a <A href="http://blogs.kde.org/node/2064#comment-4974">comment to my previous blog entry</A>, establishing as a fact that the instructions in the <i>README.Debian.gz</i> file in fact where not added by "upstream Debian" (as someone in #kubuntu tried to suggest to me). <a href="http://people.warp.es/~isaac/blog">Isaac Clarencia</a> confirmed to me (also in IRC, in channel #nx), that Debian does not disable the CUPS web interface. He installed a CUPS 1.2.1 from "testing" and found it fully functional. Given these facts, I personally think it very misleading conduct by the Ubuntu packagers (I don't suspect intentional dishonesty), to add 2 paragraphs into the README.Debian.gz file that is signed by Debian packager Jeff Licquia without making obvious who added the modifications. </p>

<p>A clean way to handle this kind of stuff would be to add their own README.Canonical.gz and sign that with their own names as packagers. I wonder how many more packages are affected by this kind of stuff? I'm now beginning to understand some of the bad moods I encountered when talking to hardcore Debian developers about Ubuntu.... Isn't that easily fixable?</p>
<hr>

<!--break-->