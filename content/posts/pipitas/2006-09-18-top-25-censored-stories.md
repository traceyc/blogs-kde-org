---
title:   "\"Top 25 Censored Stories\""
date:    2006-09-18
authors:
  - pipitas
slug:    top-25-censored-stories
---
<p>Have you ever heard about <a href="http://www.projectcensored.org/"><i>Project Censored</i></a>? That project is "a media research group out of Sonoma State University" in California. And it's again that time of the year when it publishes its annual <a href="http://www.projectcensored.org/censored_2007/index.htm">"Top 25 Censored Stories"</a> (attaching the number "2007" to it, for whatever reason).</p>

<p>You'll surely be in for quite a few surprises if you take the time to read them. Did you know...</p>

<ul>
<li>...that <a href="http://en.wikipedia.org/wiki/Halliburton">Halliburton</a> sold nuclear technologies to Iran? Halliburton is a Texas-based multinational company, employing 100.000+ persons and gaining 20+ billion $US revenues every year.  Oh, and they also give a 200.000 $US p.a. (deferred) salary to US Vice President <a href="http://en.wikipedia.org/wiki/Dick_Cheney#Relationship_to_Halliburton_as_Vice_President">Dick Cheney</a>. Cheney used to be Halliburton's Chairman and CEO. He still owns quite a large chunk of their stock options. [<a href="http://www.projectcensored.org/censored_2007/index.htm#2">Story #2</a>]</li>

<li>...that last year alone, Cheney’s Halliburton stock rose by over 3000 percent? Not bad. (If I'd manage to gain a 30-fold increase of my bank account balance, I just about reach a figure of 6 digits -- unfortunately, it will have to be multiplied by -1 as well in order to transform it from that large an overdraw to that large an asset). [<a href="http://www.projectcensored.org/censored_2007/index.htm#24">Story #24</a>]</li>

<li>...that a well-known physicist, professor Steven E. Jones, wrote a paper challenging the official 9-11 explanation why the 3 (!) high rise buildings at World Trade Center collapsed. He found lots of evidence for "controlled demolition" using explosives, but none that supports the U.S. government's theory which blames "widespread fires weakening the structure which cause the 'pancaking' of the floors". [<a href="http://www.projectcensored.org/censored_2007/index.htm#18">Story #18</a>]</li>
</ul>

<p>Of course, your own favorites amongst the 25 stories may differ from mine. Or I myself, a week from now, may find other topics more interesting such as... </p>

<ul>
<li>...<a href="http://www.projectcensored.org/censored_2007/index.htm#1"><i>"#1 Future of Internet Debate Ignored by Media"</i></a>, </li>
<li>...or <a href="http://www.projectcensored.org/censored_2007/index.htm#3"><i>"#3 Oceans of the World in Extreme Danger"</i></a>, </li>
<li>...or <a href="http://www.projectcensored.org/censored_2007/index.htm#4">"#4 Hunger and Homelessness Increasing in the US"</i></a>, </li>
<li>...or <a href="http://www.projectcensored.org/censored_2007/index.htm#14"><i>"#14 Homeland Security Contracts KBR to Build Detention Centers in the US"</i></a>, </li>
<li>...or <a href="http://www.projectcensored.org/censored_2007/index.htm#15"><i>"#15 Chemical Industry is EPA’s Primary Research Partner"</i></a></li>
</ul>


<p>When you don't have a private Linux notebook any more, a weekend at home can cause you to browse the 'net for hours and hours and make you find all kinds of interesting links and stories.... </p>

<!--break-->