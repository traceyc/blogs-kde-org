---
title:   "The Future Of Cybersex  (== The Present Of All Media Outlets?)"
date:    2006-09-20
authors:
  - pipitas
slug:    future-cybersex-present-all-media-outlets
---
<p>My first thoughts were: <i>"This is hilarious! Too hilarious to be true..."</i></p>

<p>Here is what I read: <i>"With the slimming feature, anyone can appear more slender -- instantly."</i> Yes, that's right: it says <i>"appear"</i>, not <i>"become"</i>. So this is not a TV ad for slimming pills or other chemistry. This is marketing for modern digital photographic technology -- on the very homepage of a well-known company. Go to <A href="http://www.hp.com/united-states/consumer/digital_photography/tours/slimming/index_f.html">hp.com</A> and read more:</p>

<dl><dt></dt>
<dd><p>"They say cameras add ten pounds, but HP digital cameras can help reverse that effect. The slimming feature, available on select HP digital camera models, is a subtle effect that can instantly trim off pounds from the subjects in your photos!"</p>
</dd> 
</dl>

<p>They even have example pics on show. And they say: <i>"The effect is subtle—subjects still look like themselves. Can be adjusted for a more dramatic effect."</i> Appreciate the feat with the newest HP Photsmart models...</p>

<p>Oh the joy. Cybersex is going to be so much more fun! Just wait until that technology is common enough to get stuffed into webcams. Maybe I should finally eBuy me one. Maybe the time has come when I should leave behind my cybersex virginity for good...  Imagine: this technology will not stop with just slimming. It will make me look like Young Schwarzenegger, or like Ronald Reagan. And if I'd like to pick up a Lesbian hottie, it could even bring about a sex change in real time for my CyberSelf... </p>

<p>My last thought is: maybe I should start a new blog. One that uses the tagline "Discoveries of an adult idiot about digital deceptions in everyday life, including the media". Or maybe "Don't trust no-one any more -- no picture, new news line, no movie, no 'factual evidence', no government statement..."</p>
<!--break-->"