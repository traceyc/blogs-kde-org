---
title:   "How to publish certain facts... and keep them out of public awareness at the same time"
date:    2006-07-25
authors:
  - pipitas
slug:    how-publish-certain-facts-and-keep-them-out-public-awareness-same-time
---
<p>Actually, I've now saved (as a web archive file) that <A href="http://www.dailymail.co.uk/pages/live/articles/news/news.html?in_article_id=397124&in_page_id=1770">Daily Mail news story</A> mentioned in my previos blog entries. Just in case it "disappears" again. Or gets modified. From the beginning, it was already so well hidden even on the publishing website, that I was unable to find it by following a link from Daily Mail's <A href="http://www.dailymail.co.uk/">portal page</A>. Unless you knew and followed a direct link you'll never be aware of its existence. The story clearly isn't pushed by anyone, but rather played down. <i>But see, we have a democracy, and whoever doubts that can easily be pointed to the fact that the incident indeed <b>has</b> been published. It was just that the electorate was <b>not interested</b> in it, and therefore we had to put other material on the front page.....</i></p>

<p>But what does wonder me even more is this:</p>

<ul>
<li>not any one of the known "investigative journalism" players have so far picked it up</li>
<li>I was unable to find any hint about it on any international general news site</li>
<li>no mention of it on www.stern.de, www.spiegel.de, www.frankfurter-rundschau.de or www.tagesschau.de either</li>
</ul>

<p>Is it that the Daily Mail (not exactly famous for their investigative journalism) did fall prey to a fake or hoax? (Hmm... if that happened, I guess their competitors would only be too happy to point that out and enjoy lots of <A href="http://en.wikipedia.org/wiki/Schadenfreude">Schadenfreude</A>, no?)</p>

<p>Oh, well.... /me is going back to <A href="http://en.wikipedia.org/wiki/Three_monkeys"><i>"Three Wise Monkeys"</i></A>-mode now...</p>

<!--break-->
