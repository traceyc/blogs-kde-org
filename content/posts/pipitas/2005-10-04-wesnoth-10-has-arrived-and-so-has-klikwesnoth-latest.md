---
title:   "Wesnoth 1.0 has arrived and so has klik://wesnoth-latest"
date:    2005-10-04
authors:
  - pipitas
slug:    wesnoth-10-has-arrived-and-so-has-klikwesnoth-latest
---
<p>Yesterday in IRC (<a href="irc://freenode.net/kde-devel">#kde-devel</a>) this happened:</p>

<pre>
  Oct 03 17:13:28 &lt;isaac&gt;     http://www.wesnoth.org/start/1.0/
  Oct 03 17:13:32 &lt;isaac&gt;     we have just got wesnoth 1.0 out :)
  Oct 03 17:13:59 &lt;Narishma&gt;  is there a klik package ?
  Oct 03 17:14:06 &lt;misty&gt;     what is wesnoth?
  Oct 03 17:14:18 &lt;Narishma&gt;  misty: a strategy game
</pre>

<p>OK -- what Misty asked could have been my own question too. Now I know it -- 'cause I even run the game in the meanwhile. The reason was: the above chatter had a continuation. And looking at the backlog, you'll see just how easy and fast it is to build klik bundles from .deb packages that are compiled for Sarge:</p>

<pre>
  Oct 03 17:44:45 &lt;pipitas_2&gt; isaac: are there .debs of Wesnoth already?
  Oct 03 17:45:21 &lt;pipitas_2&gt; isaac: if so, it will be very trivial to write a 
                              recipe and make a klik://wesnoth package availaible
  Oct 03 17:45:39 &lt;isaac&gt;     they will reach unstable in a few hours
  Oct 03 17:47:13 &lt;pipitas_2&gt; isaac: we prefer packages build for Sarge  --  because 
                              these run practically everywhere
  Oct 03 17:48:21 &lt;isaac&gt;     pipitas_2: I see, I'll backport the packages when I 
                              got some time
  Oct 03 17:49:29 &lt;pipitas_2&gt; isaac: without backport, no klik://wesnoth  --  with 
                              backport, klik://wesnoth 10 minutes later
  Oct 03 17:53:59 &lt;isaac&gt;     pipitas_2: backport for sarge is being built
  Oct 03 18:23:20 &lt;pipitas_2&gt; isaac: great! tell me once it is ready :-)
  Oct 03 18:23:51 &lt;isaac&gt;     pipitas_2: I'm uploading it to http://debian.wesnoth.org/sarge/
  Oct 03 18:24:04 &lt;pipitas_2&gt; isaac: Oki-dok!
  Oct 03 20:13:11 &lt;isaac&gt;     pipitas_2: wesnoth backport for sarge is ready
  Oct 03 21:01:29 &lt;pipitas_2&gt; isaac: your wish was our command!  So you can now try 
                              "klik://wesnoth-latest" and play your game
</pre>

<p>Contrary to what the log seems to suggest, it was not me who created the recipe, but <A href="http://klik.atekon.de/blog/">probono</A>. And contrary to what the log says, the klik fake URL is for now <A href="klik://wesnoth-latest">klik://wesnoth-latest</A>.</p>

[image:1515 align="right" hspace=4 vspace=4 border=0 width="320" class="showonplanet"]

<p>Once you have the <A href="http://klik.atekon.de/">klik</A> client installed (hey, on KDE it is as easy as hitting <tt>[alt]+[f2]</tt>, typing &nbsp;"<tt>wget klik.atekon.de/client/install -O -|sh</tt>"&nbsp; and following the instructions, just click on <A href="klik://wesnoth-latest">klik://wesnoth-latest</A> to enjoy the brandnew version 1.0 of <A href="http://www.wesnoth.org/">"The Battle for Wesnoth"</A>. Don't forget to let us know how it went. Especially if you are a Slackware or Gentoo or Mandriva or Redhat/Fedora user: give us feedback if it works for you. You can also find us in IRC, channel <A href="irc://freenode.net/klik">#klik</A> on Freenode.net.</p>

<p>klik, as should be well known by now, was designed to follow the <i>"1 application == 1 file" paradigm</i>. klik creates compressed images of a file system (similar to ISO images that are burned onto a CD) with the extension .cmg. This klik .cmg file system contains all dependencies of libs that are not expected to be present on the target system. </p>

<p>klik prefers to use as its input to the bundle creation process Debian packages built for Sarge. These usually work for other distros (like SUSE-9.3 and SUSE-10.0 too, with some minor modifications required which klik knows about and klik applies).</p>

[image:1516 align="left" hspace=4 vspace=4 border=0 width="320" class="showonplanet"]

<p>klik clients build their own bundles themselves (completely transparent to the user who has only to click on a link like <a href="klik://datakiosk-demo">klik://datakiosk-demo</a>). The klik server then just sends a "bundle building recipe" in ASCII text format to the klik clients, and the klik clients do all the hard work (download the .deb files from the URLs named in the recipe, unpack them and transform the many input files into the single .cmg file, complete with a wrapper script that is capable of running the result) themselves. </p>

<p>Because of this distributed nature of downloading the input from different repositories, and letting the bundle creation work be done on the client side, while providing the recipes from a central server, klik scales extremely well.</p>

<p>The first of the above screenshots (click to enlarge) shows the .deb packages and their sources used by the <a href="klik://wesnoth-latest">klik://wesnoth-latest</a> link to build a 42 MByte sized SUSE-9.3 wesnoth-latest.cmg. The other shot shows the intro screen of the game (heh, I had to run it over an <A href="http://www.nomachine.com/">NX</A> link because I didnt know how to take a screenshot from a fullscreen game session...). </p>

<p>I'm pretty sure that this klik-bundle (as most others too) due to its compresssion is considerably smaller (i.e. taking less space on the harddisk) than a standard system installation set of RPMs or .debs on the respective systems. So much for the objection "klik wastes my harddisk space because it includes some libraries in each individual .cmg".</p>

<!--break-->
