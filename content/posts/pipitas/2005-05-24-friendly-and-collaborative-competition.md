---
title:   "Friendly and collaborative competition"
date:    2005-05-24
authors:
  - pipitas
slug:    friendly-and-collaborative-competition
---
My last but one <a href="http://blogs.kde.org/node/view/1087">blog entry</a> prompted several people to write me personal mails. I was away and offline for most of the weekend, so upon return I was glad to see a friendly personal note from <a href="http://www.advogato.org/person/bolsh/">Dave Neary</a> who pointed me to <a href="http://www.advogato.org/person/bolsh/diary.html?start=119">his own blog</a> on the matter, and also to <a href="http://www.gnome.org/~jdub/blog/issues/desktop/1116729637">Jeff Waugh's</a>.

They both took issues with how I smilingly had commented on the recent GNOME Foundation agreement to join the <a href="http://www.opensourceconsortium.org/index.html">OpenSource Consortioum</a> (OSC) in the UK. Jeff rightly hinted at the fact that journalists in their hunt for "stories" always tend to blow up conflicts to make their content more interesting, and especially when these could fuel some infighting within the FOSS community. I do see this too, and that's why I more <b>asked</b> about their intentions rather than stating a fixed opinion of mine about it. I am glad that Dave and Jeff took it up and responded in their blogs (even if I may not agree to some of their points -- but let's not discuss those now and here; let's more explore our common ground. And that is still, and for quite some time to come: how can both of us best go forward and conquer market share from the big Redmond monopoly?).

Other mails arrived which agreed to have KDE involved in OSC alongside Gnome -- some even from people who are themselves involved in OSC!

Jeff's most important point was: <i>"We have a wonderful environment of friendly and collaborative competition, and have avoided unnecessary bitterness for a long time. Please don't mess with it lightly."</i> This is one point I <b>do</b> agree with. And having Gnome as well as KDE supporting OSC so we all jointly canvas for much more FOSS usage in UK local and national government offices will make everyone involved in the deal stronger. So here is more ground for friendly and collaborative competition!
<!--break-->