---
title:   "klik://troubleshooting"
date:    2005-11-20
authors:
  - pipitas
slug:    kliktroubleshooting
---
<p>Another two section for the <A href="http://klik.atekon.de/wiki/index.php/User's_FAQ">klik User's FAQ</A> done today. This time "Troubleshooting" and "Tipps + Tricks". Enjoy.</p>


<TABLE width="95%" cellspacing="2" border="1" cellpadding="2" align="center" bgcolor="#d3d3d3">
  <tbody>
    <tr>
      <td>

<b>Troubleshooting</b>

<p>
<b>Q: </b><i>I got a dialog saying "Error while trying to run xmule". What does this mean?</i>
<b>A:</b> This could mean there was a network outage and the server providing the input files to the klik recipe could not be reached to download the ingredient packages. </p>

<p>
<b>Q: </b><i>I get a message saying: "This package contains no application. klik can't handle it." -- What does this mean?</i>
<b>A:</b> You clicked on the name of a package that can not be used standalone, and therefor is not feasible for klik.</p>

<p>
<b>Q: </b><i>Which package names are not feasible for klik?</i>
<b>A:</b> Examples for such packages are all libraries, or packages containing fonts or artwork.</p>

<p>
<b>Q: </b><i>Why can't you remove those packages from the klik website which are not feasible for klik?</i>
<b>A:</b> We can. But it takes time. Do you want to help?</p>

<p>
<b>Q: </b><i>I get a popup dialog telling me it is downloading a Debian package. But I am on SUSE, so I cancelled it! What gives??</i>
<b>A:</b> Don't worry. The Debian package will be converted into a klik .cmg file. Most recipes are designed to use .debs as input. In general, these work just fine for SUSE.</p>

<p>
<b>Q: </b><i>I get a popup dialog telling me it is downloading multiple RPMs. But I am on Debian, so I cancelled it! What gives??</i>
<b>A:</b> Don't worry. The RPMs will be converted into a single .cmg file. Some recipes are just designed to use RPMs as input, because there was probably no equivalent .deb package available that was recent enough in its version.</p>

<p>
<b>Q: </b><i>I read that klik uses a single file with a .cmg extension. Now I'm trying klik:/xyz and it pops up a dialog warning that it downloads a mixture of .debs, .rpms and .tgzs onto my sytem. No way! I cancelled the download. How do I get the single .cmg?</i>
<b>A:</b> The listed "mixture of .debs, .rpms and .tgzs" are only the input files to create the single .cmg file from. The .cmg will be created by your own system. The .cmg will contain the "ingredients.list" file for future reference.</p>

<p>
<b>Q: </b><i>Doesn't the mixture of ".debs, .rpms and .tgzs" lead to problems when executing the .cmg?</i>
<b>A:</b> This is not ruled out for certain combinations of packages. We have however had some good experience with it. Our recipes try to avoid problems. Should problems occur, they can be fixed by modifying the recipe. The reason for potential problems is not the mixture of RPM and .deb per se -- those would rather be caused by "binary incompatibility" of the different packages. Binary incompatibility can also happen between 2 RPM packages.</p>

<p>
<b>Q: </b><i>Is there a very quick and minimum download method to test if the klik client installations works in principle?</i>
<b>A:</b> Yes. Try "klik://xvier" first (0.4 MByte download, 0.37 MByte xvier.cmg file result). If that doesn't work, try "klik://xvier@scratch" (6.3 MByte download, 6.2 MByte xvier.cmg file result). The "@scratch" addition includes many more dependencies directly into the .cmg, without assuming them to be present on the klik client OS base system.</p>

<p>
<b>Q: </b><i>Some klik applications do not terminate cleanly. I have some processes still running, and the mountpoint blocked. What can I do?</i>
<b>A:</b> First, check which mountpoint is occupied, using the "mount" command:
<pre>  mount</pre>
(this will tell you which mountpoints are occupied (f.e. "/tmp/app/1" or "/mnt/app/3").
Second, try to umount manually (as user, not as root):
<pre>  umount /tmp/app/1
  umount /mnt/app/3</pre>
(exact mountpoint depends on your local system).
Third, find out which process(es) still use this mountpoint:
<pre>  lsof /tmp/app/1</pre>
Fourth, kill that process(es):
 kill &lt;pid&gt;
Fifth, free the mountpoint:
<pre>  umount /tmp/app/1</pre>
Sixth, remove all remainders of the previous action:
<pre>  rm -rf /tmp/app/1</pre>
</p>


<b>Tipps+Tricks</b>


<p>
<b>Q: </b><i>Is there a way to discover the exact recipe which is used to construct a given application via the klik://application link?</i>
<b>A:</b> Yes. Run the klik client from the command line, with the "-x" switch. Interrupt the download once the first dialog pops up. Copy the last line (starting with "wget klik.atekon.de/client/install....") and modify it to not execute the recipe, but save it into a local file. (Replace the last dash, after "-O" with the recipe filename of your choice). More details are in Kurt Pfeifle's blog article.</p>


<p>
<b>Q: </b><i>Is there a way to re-construct the recipe that was used to construct a finished .cmg file?</i>
<b>A:</b> No. Currently the recipe itself is not stored inside the .cmg file. But maybe it is a good idea. We will think about it.</p>


<p>
<b>Q: </b><i>Is there a way to force the usage of the Kanotix recipe for kitty, instead of the SUSE-10.0 one?</i>
<b>A:</b> Yes. Try "klik://kitty@kanotix0504".</p>


<p>
<b>Q: </b><i>Is there a way to include the maximum number of dependencies into a given recipe, say the one for xvier?</i>
<b>A:</b> Yes. Try "klik://xvier@scratch".</p>


<p>
<b>Q: </b><i>Can I enforce other variations of a klik recipe, if the standard one does not work for my system?</i>
<b>A:</b> Yes. You can also try "klik://xvier@kanotix0504", "klik://xvier@multi" or "klik://xvier@breezy", or analogous for other application names instead of xvier.</p>


<p>
<b>Q: </b><i>What does "klik://xvier@multi" do?</i>
<b>A:</b> It enforces the execution of the klik recipe that was designed to work for multiple klik client versions of Linux. Be aware that not all recipes do have variations per klik client OS.</p>


<p>
<b>Q: </b><i>What does "klik://xvier@kanotix0504" do?</i>
<b>A:</b> It enforces the execution of the klik recipe that was designed for "Kanotix-2005-04", regardless what your actual klik client OS is. Be aware that not all recipes do have variations per klik client OS.</p>

</td>
    </tr>
  </tbody>
</TABLE>


<p>Next topics, maybe written tomorrow: Security, Roadmap, Howtos. Now done 80 out of 110 questions. I wonder how many more will arrive in my mailbox after I've dealt with them all.</p>

<!--break-->