---
title:   "Systems exhibition: going to see x2go and CoreBoso"
date:    2007-10-24
authors:
  - pipitas
slug:    systems-exhibition-going-see-x2go-and-coreboso
---
<p>This afternoon it looks like I'll get to go tomorrow to the Systems fair in Munich. I've got various exhibitor booths to visit and see what new things they have on offer, and also one or two meetings arranged already.</p>

<p>However, there is one thing I'm especially keen to see: <a href="http://x2go.berlios.de/index-en.html"><b>x2go</b></a>.</p>

<p>x2go is a  [image:3070 align="left" width=400 height=300 hspace=6 vspace=4 border=0 class="showonplanet"] fast remote desktop connection suite based on <a href="http://www.nomachine.com/">NoMachine's NX libraries</a>; you can scale it up to enterprise level or slim it down to personal use only for an <i>"everywhere@$HOME"</i> personal use-case for geeks and nerds.</p>

<p>To say it first: x2go is <b>*not*</b> compatible with FreeNX or NoMachine's NX servers. Nor is it with NoMachine's NX client or with QtNX. However, x2go does base its remote desktop access part of their functionality on NoMachine's NX libs -- but it implements 'the rest' differently: mainly sound/printer/fileshare tunnelling and user authentication for now (example: uses sshfs for file sharing; LDAP for user admin+auth; printing and sound do not work yet). Their x2go client is Qt4-based (Win32 version available).</p>

<p>x2go developers don't care much about accessing Windows remotely -- their main concern is Linux/Unix, and that's where they focus their development work on.</p>

<p><b><i>Their coolest (working!) feature for now: store your authentication data on a USB stick or a SmartCard. Remove stick/card from client -- your remote session is suspended; stick it in again (even on a different client) -- your session is immediately resumed.</i></b></p>

<p>What KDE users probably will appreciate very much: they have their configuration module completely integrated into KDE's Control Center. <!--break--> [And before you ask: yes, of course you can use x2go to run Gnome, and Gnome-only sessions remotely for your users, if you like. It's only the x2go admins who'll ever get to see the KDE integration, if you don't like to expose your users to a "foreign world"...] See also <a href="http://x2go.berlios.de/screenshots-en.html">their screenshots</a>. One part of that control center module can be used completely independent from x2go for LDAP and SAMBA administration. (<i>Is there an interested KDE developer who wants to pick this up and work on including it in KDE's SVN?</i> I'm sure the x2go developers will be open for discussions about how to do this without forking their project....)</p>

<p>x2go is still under heavy development, but very well usable already. Since half a year, a not-to-small school in Bavaria is using the an x2go network with 3 terminal servers to provide KDE desktops to dozens of classrooms. The school is the projects (so-far non-publicized) beta-test site, which gives them lots of real-world feedback. It was this input which led to the development of some additional tools like <i>x2gospyglass</i> and <i>x2gomail</i>.</p>


<p>One other very interesting booth is <a href="http://www.linux-ag.com/index.php?page=coreboso">Linux Information Systems AG's </a> where they exhibit the CoreBoso suite which can manage the entire IT infrastructure of a medium-sized business.</p>

<p><i>CoreBoso</i> is supposed to showcase how the cornerstones of a company's IT can be based on Free and Open Source software (while providing additional support to integrate MS Windows systems too). This includes all workstation client systems as well. They offer their own series of pre-installed, support-contracted and remotely-administered servers (geared to customers' specific needs such as printing, file serving, single sign on, router, firewall, gateway, web server, etc.) under the "CoreBiz" branding, based on Debian (and/or Ubuntu?). Their exhibition description says it will have show separate stations where you can take hands-on experience with IT management, printing, voice-over-IP, groupware, with  workstations configured for the needs of typical marketing, finance, service and sales departments as well as home offices for remote work. Should be interesting to see and try everything....</p>