---
title:   "My Application of the Day: Kochizz for Apache2 Configuration"
date:    2007-08-29
authors:
  - pipitas
slug:    my-application-day-kochizz-apache2-configuration
---
<h2>My Application of the Day: Kochizz for Apache2 Configuration</h2>

<p>My application discovery of the day (well of the <i>yester</i>day, to be more precise), is <a href="http://kochizz.sourceforge.net/">Kochizz</a>. Kochizz is a Qt4-based GUI tool to get to grips with the Apache2 configuration.</p>

[image:2959 align="center" size="preview" hspace=4 vspace=2 border=0 class="showonplanet"]

<p>As anyone who ever tried to set up an Apache2 web server may know, this can be an insanely complicated task, because the thingie can use multiple configuration files at once, which are included and nested into each other with (you guessed it?), <i>Include</i> directives.</p>

<p>Kochizz makes that job much easier now. Mind you, Kochizz is not yet released as a stable application, but it is already in <a href="http://sourceforge.net/project/showfiles.php?group_id=202287"><i>Release Candidate 1</i></a> shape. Being a Qt4 app, it means you get Linux, Windows as well as Mac OS X versions... for free. Not least because the license is GPL v2...</p>

<!--break-->
[image:2960 align="right" width="465" height="318" hspace=4 vspace=2 border=0 class="showonplanet"]

<p>(BTW: clicking on one of the screenshots displays them from their original location, where you may find additional explanatory commentary about them.)</p>

<p>Kochizz can not only handle one server configuration at a time, it can handle many -- by using a extra tab for each additional server (or main config file variation). That is very handy if you are responsible for multiple Apache installations, or if you want to create a new configuration as a variation from an existing one.</p>

<p>When Kochizz starts up, it loads the config file you pointed it at, and all the nestedly included additional files, parses them for potential syntax errors (which it reports) and then displays the overall configuration directive in a tree-like display. Then you can easily change any directive by enabling or disabling a checkbox. </p>

<p><i>Attention, here is a glitch: seeing an <b>enabled</b> checkbox (with an 'x' in it) usually means that this part of the configuration is <b>dis</b>abled.</i> (The reason is: disabling a directive from Kochizz inserts a specially formated comment in front of the respecitive line, that it can also reliably remove: that comment markup consists of the three characters <tt>#$*</tt> as you can see on one of the screenshots.</p>

[image:2961 align="left" width="465" height="318" hspace=4 vspace=2 border=0 class="showonplanet"]

<p>Anyway... the next goodie is this: you can easily switch to a different tab in the GUI editor that displays the <i>original config file as ASCII text</i>. And benefit from its nice builtin syntax highlighter. And edit that, there... And see how your edited change propels back into the GUI representation of the config.</p>

<p>What I also do like very much: unlike many other GUI tools to edit configuration files of $someothersoftware, Kochizz preserves the original comments in your configuration.</p>

<p>Whatever config file(s) Kochizz has read in, Kochizz can also <i>'Save as...'</i> under a different name. You can optionally pick to merge all the <i>Include</i>-nested config files into a single "all-in-one" file.</p>

<p>To run my little test I <a href="http://sourceforge.net/project/showfiles.php?group_id=202287&package_id=240960&release_id=534596">downloaded the Linux tarball</a> and simply extracted it. It contains a binary called (you guessed it?) <i>kochizz</i>. (Unfortunately, the download page does not tell you which exact versions of Linux distros the binary is meant to run on.) However, on an openSUSE-10.2 system this ran without a flaw. Since the authors of Kochizz say that the application has no other external dependency (but Qt4) it should run on pretty much every Linux distro that has this dependency satisfied (On $debian, just do a <tt>"sudo apt-get install libqt4-core libqt4-gui"</tt> to get this on board). I was able to start the binary directly from the extracted tarball (which was placed on a rather obscure spot in my filesystem).</p>

<p>The nice thing about the builtin documentation for each config directive is this: basically, there is none... it uses the <b>original Apache documentation</b> instead, and it is able to jump to the correct spot there if you are looking for context-sensitive help. So you know that you get your info from the mouth of the horse, if you look up something...</p>

<p>What I'm missing from this release is a "Search" function, that let's me find spots of in the config containing a certain string. (The function is there, in the menu; but it didn't work for me when I tried it -- didn't find several words I searched for which I know are in the config files multiple times). Another unfinished thing: the Kochizz manual. I mean, it's there. But it's only there in French, not (yet) in English....</p>

<p>The authors of Kochizz, <a href="http://kochizz.sourceforge.net/a-propos/">two French students</a>, would be glad to see people joining their project for further help, now that they have made their first release.</p>

<p>If you like Kochizz, you may want to visit its <A href="http://www.qt-apps.org/content/show.php/Kochizz?content=64969">Qt-apps page</A> and leave a comment and your rating there.</p>

<p>You still are puzzled about that name, "Kochizz"? Read the authors' <a href="http://kochizz.sourceforge.net/a-propos/">explanation</a>....</p>

<p><small>See also a few details about the screenshots: <A href="http://blogs.kde.org/node/2959">shot 1</A>, <A href="http://blogs.kde.org/node/2960">shot 2</A>, <A href="http://blogs.kde.org/node/2959">shot 3</A>.</small></p>