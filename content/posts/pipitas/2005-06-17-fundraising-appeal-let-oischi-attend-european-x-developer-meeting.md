---
title:   "Fundraising Appeal: Let 'Oischi' Attend European X Developer Meeting"
date:    2005-06-17
authors:
  - pipitas
slug:    fundraising-appeal-let-oischi-attend-european-x-developer-meeting
---
The <A href="http://wiki.x.org/wiki/LinuxTagMeetingSchedule">European X Developer Meeting</A> has been kept quite low profile by its organizers. Maybe because it is meant to be primarily for developers. Maybe they are just too busy with organizing things -- after all, it was announced at very short notice. Yet there is a <A href="http://wiki.x.org/wiki/LinuxTagMeetingSchedule">very attractive program of presentations</A> and discussions which should give a glimpse of the future of the modern X Window System extensions to all interested developers:<br><br>

<ul>
<li>Lars Knoll (<A href="https://blogs.kde.org/blog/632">KDE developer</A> and <a href="http://www.trolltech.com/">Trolltech</a> employee) will be talking about <A href="http://wiki.x.org/wiki/LinuxTagMeetingLars"><i>Improving and Extending Render</i></A></li>
<li>Zack Rusin (<a href="https://blogs.kde.org/blog/14">KDE</a> and Trolltech</A> too) will show his work on <A href="http://wiki.x.org/wiki/LinuxTagMeetingZack"><i>Redesign of the Acceleration Architecture</i></A>. </li>
<li>Gian Filippo Pinzari <A href="http://www.nomachine.com/">(NX/NoMachine)</A> presents (of course) <a href="http://wiki.x.org/wiki/LinuxTagMeetingGianFilippo"><i>NoMachine NX</i></a>.</li>
<li>Kai Uwe Behrmann will talk about <a href="http://wiki.x.org/wiki/LinuxTagMeetingKaiUwe"><i>X and Colour Management</i></A>.</li> 
</ul>
One guy may be missing. He is <A href="http://www.blogger.com/profile/6873305">a student</A>. He is the one who will create a very cool <A href="http://kde-apps.org/content/show.php?content=14422">kompose version</A> for KDE 4. Since his cool preview of earlier tody was very quickly pushed down into the bottom of the <A href="http://planetkde.org/">planetkde.org</A> and <A href="https://blogs.kde.org/">www.kdedevelopers.org</A> blog assemblers, here it is again:
<br><br>
><img align=center src="http://home.degnet.de/hans.oischinger/kompose2.gif" class="showonplanet">
<br><br>
Beautiful and exciting, isn't it? The <A href="http://oisch.blogspot.com/2005/06/combination-of-two.html">description</A> says, that the new powerful <A href="http://doc.trolltech.com/qq/qq09-qt4.html">Qt4 libraries</A> (not yet released as a final version) together with the barely ported KDE4 libraries made this possible with only a few lines of code. And with very little CPU load too! KDE4 is going to rock many boats...<br><br>

Oischi, the creator of this demoware, told me on IRC today that he'd <i>love</i> to go to Karlsruhe to the meeting (not to present something, just to learn and meet the people), but  a) he learned too late about it (only this evening) and b) he couldnt afford the travel expenses and the accomodation.<br><br>

Now let's see: Are there enough KDE enthusiasts reading this, who.. 
<ul>
<li>...who care enough about KDE's future that they will <A href="http://www.kde.org/support/donations.php">put together the funds</A> to make Oischi attend this meeting? </li>
<li>...who are interested in making the KDE4 GUI not just cool, but give it a <A href="http://plasma.bddf.ca/">breathtakingly beautiful</A> appearance? </li>
<li>...who want to empower people who work to combine eyecandy with a <A href="http://www.openusability.org/">usability level</A> that is good enough for your Grandma and Grandpa computer newbie to use, and that sports some very exciting <A href="http://plasma.bddf.ca/cms/1069">evolution of what we use today</A></A>? </li>
<li>...who want to see KDE becoming a <A href="http://www.google.com/url?sa=U&start=7&q=http://www.linuxplanet.com/linuxplanet/reviews/5816/5/&e=912">leader in innovative desktop</A> technologies?</li>
</ul>
OK, folks -- here is the deal: <A href="http://www.kde.org/support/support.php">go to KDE's fundrasing website</A>, and send some money in. Whatever you can afford. Whatever amount it is worth while for you. (You <A href="http://www.kde.org/support/donations.php">can even track</A> that it made the trip to KDE successfully). I will personally guarantee that Oisch will not have to pay a penny for his travel and accomodation (yes, he has to buy his own drinks!) -- if you don't raise enough money to make KDE e.V. pay Oischi's expenses, I will cover the missing part from my personal wallet.<br><br>

Deal?<br>
<!--break-->