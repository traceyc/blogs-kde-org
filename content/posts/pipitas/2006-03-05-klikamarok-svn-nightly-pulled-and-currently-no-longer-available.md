---
title:   "klik://amarok-svn-nightly is pulled and currently no longer available"
date:    2006-03-05
authors:
  - pipitas
slug:    klikamarok-svn-nightly-pulled-and-currently-no-longer-available
---
<p>amarok is a really great piece of KDE software. Its ever increasing popularity</A> also led it to be one of the favorite and <A href="http://klik.atekon.de/popular.php">most frequently klik-ed</A> packages of users. The nightly builds from SVN were gaining really <A href="http://amarok-svn-nightly.klik.atekon.de/comments/">enthusiastic user comments</A>.</p>

<p>But klik://amarok-svn-nighly is no more. Not for now. Not unless someone steps forward and reliably provides builds from SVN on a regular basis.</p>

<p>What happened?</p>

<p>For a very happy initial period, <i>klik://amarok-svn-nightly</i> worked very well for lots of users. (There were some problems with the kliks for some distros initially, but these were fixed over time). Now the packager does not have the time any more to monitor the builds. And he publically said so; and he asked in IRC, channel #amarok, if someone would like to take over maintenance. He didn't find anyone. The scripts were left running automatically on their own. But unfortunately, their results  deteriorated over time. Currently, they only output binary zeros of 1.6 MByte and name the file an "amarok_1.4.SVN_i386.deb". klik may do wonders for some -- but obviously it is beyond its power to convert binary zeros into a useful .cmg file for beta testers and enthusiastic bleeding edge users.</p>

<p>That's why I had no choice but to pull it: it doesnt work any more, because the upstream .deb build process produces garbage, since a few days; and there is currently no-one who can look after fixing them and keep an eye on their behaviour.</p>

<p>And this is a pity. A very big pity. Because the (very young) amarok-svn-nightly build clearly was the <A href="http://amarok-svn-nightly.klik.atekon.de/">main contender</A> for the crown of the most popular klik-ed application of 2006. Even in the face of such "heavyweight" longtime contenders as <A href="http://firefox.klik.atekon.de/">firefox</A>, <A href="http://skype.klik.atekon.de/">skype</A>, <A href="http://ar7l.klik.atekon.de/">acroread7</A>, <A href="http://opera.klik.atekon.de/">opera</A>  and <A href="http://realplayer10gold.klik.atekon.de/">realplayer10</A>...</p>
<!--break-->