---
title:   "KDE is leading the way, admit its competitors"
date:    2005-05-20
authors:
  - pipitas
slug:    kde-leading-way-admit-its-competitors
---
<br>Earlier today I came across <a href="http://www.pcpro.co.uk/news/73035/gnome-set-to-invade-europe.html">an article</a> that made me smile.
<dl><dt> A quote first:</dt><dd>
<i>"Gnome will join OSC's Community Advisory Board and work with the OSC to promote the open source desktop."</i> &nbsp;  &nbsp;  &nbsp; (from the article)</dd><br>
<dt>Another one:</dt><dd>
<i>"...the (Gnome) Foundation will hope to raise the profile and acceptance of its interface as a standard for open-source systems throughout Europe. Gnome's rival KDE has a significant hold on Linux desktops in Europe, so the move also advances Gnome's chances of gaining market share."</i> &nbsp;  &nbsp;  &nbsp; (from the article)</dd></dl><br>
What does it mean? Well, to me it means three things, two for sure, and another one not-so-sure.<br><br>

The first sure thing is: <b>KDE is leading the way</b> for the Linux desktop adoption, which is expressed in the growing movement away from the MS monopoly. And you can see the recognition in the quotes above, as well as in various poll results and studies.<br><br>

The thing I am unsure about is this (so let me put it as an open question): Are our friends from the camp of the other, the second biggest desktop, now <b>giving up targeting MS market share?</b> Are they now shifting focus to bite into KDE's "market share" as their main purpose of existence? Well, there is no reason to be afraid. Knowing what kind of killer applictions for the enterprise and government market are currently maturing inside the official KDE code base, there is no way anyone could easily remove KDE users' loyality and make them switch.<br><br>

The second sure thing is: KDE market share may be a worthwhile target for the other big Open Source desktop, as KDE is much bigger -- but this is not true the other way round. I do not see any need or inclination for KDE to attack Gnome, as long as there are still well over 90% desktop workstations on MS Windows. KDE's most substantial growth in the future, short and middle term lies with the companies and governments that want to switch away from Microsoft.<br><br>

Oh, and of course it si great to see associations like <a href="http://www.opensourceconsortium.org/index.html">Open Source Consortium</a> in Britain being formed. KDE should join this as well as <a href="http://www.lisog.org/Infos/gruendung/Gruendungsmitglieder">similar efforts in other countries</a> too. They are an excellent platform for reaching out to the still-MS Windows using masses of computer users.. <br><br>

Looking at the successful migration from CVS to <A href="http://websvn.kde.org/">Subversion</A> now completed, watching how diligent and even vigorous a dedicated group of KDE core developers already are on to porting kdelibs and kdebase to Qt4, and seeing some of the incubent new killer applications being worked on, I can not help but smile again.<br><br>

And, BTW, there is a new baby-sister to the well-known <A href="http://kde-apps.org/">KDE-apps.org</A> and <A href="http://kde-look.org/">KDE-look.org</A>: it is called <A href="http://kde-files.org/">KDE-files.org</A>. These websites give expression to the huge FOSS ecosystem of end user and desktop applications KDE has grown to become. You can find some really well-polished gems there already, which have not yet made it into KDE proper..<br><br>

It would be nice, if <A href="http://dot.kde.org/1075252150/">Frank</A> started blogging about what is going on with his well-run sites. An initial blog entry, leaking some website and application statistcs perhaps, Frank?<br>
<!--break-->