---
title:   "Recommendations to (K)Ubuntu Dapper users: How to restore an uncrippled CUPS [2: client side printer browsing]"
date:    2006-06-24
authors:
  - pipitas
slug:    recommendations-kubuntu-dapper-users-how-restore-uncrippled-cups-2-client-side-printer
---
<p><b>[2] Restoring the client side CUPS printer browsing ability</b></p>

<p>My last blog entry outlined how Ubuntu users can restore the CUPS web interface to full functionality. The next one will explain how they can setup network printer auto-discovery goodness into their CUPS installation, a feature that was artificially crippled by their distro's packagers. This one investigates the "printer browsing" settings presented by Ubuntu, and demonstrates how to set them back to a fully functional (and more secure) mode. [image:2120 align="right" size="preview" hspace=4 vspace=2 border=0 class="showonplanet"]</p>

<p>So let's move to the meat of the matter. </p>

<p>Printer browsing is an &uuml;ber-cool feature, which lets CUPS clients use a CUPS print service and its shared printers in the LAN...</p>

<ul>
  <li>...without needing to install any printer locally, </li>
  <li>...without needing to install any driver locally, </li>
  <li>...without needing to run any special setup. </li>
</ul>

<p><b>The benefits are obvious:</b> True "zero configuration" print setup. Simple, but excellent and reliable printing goodness for lots of users. Very comfortable. Fully functional. Including the full access to every single printer driver option that may be interesting to use with a print job. Works from the commandline, works from KDEPrint/kprinter, works from gtklp, works from xpp (two other GUI frontends to conduct printing tasks). Not sure about Gnome-Print, though...  But easy for administrators to run and overlook from one single point. Even allowing for simple and efficient fail-over, load-balancing and print-cluster setups. Waaaaay superior to what MS Windows has to offer when it comes to network printing, believe me.</p>

<p>CUPS' cool printer browsing abilities are often misunderstood. Users don't have a chance to even see and experience it, if their distro's packagers disable it. And packagers (at least some of those I talked to, often times in recent years) seem to disable it because they don't "get" its benefits. Or because they don't even understand its mechanics (and don't seem to be keen to learn more). And keep clinging to their once-assumed <i>"But it's unsafe!"</i>-presumption.  So let's put up another round in this long fight. Let's start another  <i>101 CUPS Browsing Primer</i> course...</p>

<p>Actually, there's two different aspects to CUPS printer browsing:</p>

<ul>
  <li>client-side printer browsing: this lets the CUPS clients passively discover all printers which are announced by CUPS servers in the LAN</li>
  <li>server-side printer browsing: this makes CUPS servers actively announce their printers to all CUPS clients in the LAN</li>
</ul>

<p>Both aspects are separate from each other. Clients may ignore all, ignore a part or listen to all of the CUPS servers announcing their printers.</p>

<p>Printer browsing works through UDP (not TCP!) broadcasts. Like this:</p>

<ul>
  <li>Every <i>${BrowseInterval}</i> seconds (defaults to 30), a CUPS server sends one UDP broadcast package (with a size of approx. 100 - 200 Bytes) for each shared printer to <i>${BrowseAddress}</i>.</li>
  <li>CUPS clients, if setup to use <tt>"Browsing On"</tt>, pick up this info from the LAN, assemble a list of printservers and their shared printers and readily offer them to their user whenever she wants to print.</li>
</ul>

<p>The screenshot on the top, taken on a Knoppix immediately after booting, shows which printqueues kprinter can "see" without any configuration. It's the result of client side printer browsing, that is enabled on the Knoppix Live CD out of the box. The browsing picked up 7 different printers, from 3 different CUPS servers (<i>socceroo</i>, <i>10.162.2.251</i> and <i>elephants</i>). [image:2119 align="right" size="preview" hspace=4 vspace=2 border=0 class="showonplanet"]</p>

<p>What could be a more easy than this? A user does not need to care about installing a printer. She does not need to search for the correct driver. She is not required to decide for a protocol backend. She just clicks on the "Print" icon and <i>Pooof!</i>, all available printers show up, magically brought to her desktop by a CUPS server with printer browsing enabled. Depriving users from this comfort for no good reason is a crime against the general rallying call of <i>"Better usability for the Linux desktop!"</i>. Disabling it directly goes against the commonly agreed goals to <i>"Make printing more easy!"</i> as was declared by this April's <A href="http://groups.osdl.org/workgroups/dtl/desktop_architects/desktop_printing/">OSDL desktop printing architects meeting</A>. How more stupid could we be than hiding from our users and disabling a printing technology cornerpiece that makes our platform superiour in comparison to the market leader Microsoft?</p>

<p>Client side browsing of course also satisfies the needs of commandline users (see other shot showing <i>Konsole</i>). They can query CUPS for available printers (using the <tt>"lpstat -p"</tt> command), and will see this very same list, so they can pick one printqueue to send a job to.</p>

<p>CUPS.org's upstream default configuration does <b><i>not</i></b> make for an active print server out of the box. It only provides for a maximum user-friendlyness for each CUPS client. It enables workstations to effortlessly print without any configuration and with no setup headaches, should there be an active CUPS server around. Let's be overly clear: CUPS.org's default setup has tt>"Browsing On"</tt>. But a client with only "Browsing On" enabled does <b><i>not</i></b> share his own locally installed printers with the rest of the world. "Browsing On" does not make it a print server. "Browsing On" does not make it a broadcaster! Clear now?</p>

<p>So why is it that Ubuntu deprives its users from a fully functional CUPS? Why don't they allow the web interface enabled like it is prepared by CUPS.org? Why do they suppress client-side printer browsing? When I asked, I was told: <i>"It is our (and Debian's) policy to not open any un-needed port by default."</i></p>

<p>I agree to that policy, very much so. The point is: CUPS' default configuration from upstream does <b>not</b> open any external port by default, let alone "un-needed" ones! And yet it has a functional web interface. And yet it has a working client setup. And yet it can immediately discover all printers shared by CUPS servers. And yet it is a "zero configuration" effort to make it print out of the box in any environment where a CUPS server is at work. And yet it can utilize all driver-supported job options, without any need for messing with a local driver installation.</p>

<p>Let's have a look at the Dapper configuration for CUPS as compared to the one shipped by CUPS.org:</p>


<pre><font color="blue">
         +--------------------------------+---------------------------------+
         | .......Dapper's default....... | ......CUPS.org's default....... |
         |--------------------------------+---------------------------------|
         | Listen localhost:631           | Listen localhost:631            |
         | Listen /var/run/cups/cups.sock | Listen /var/run/cups/cups.sock  |
         | <b>Browsing Off</b>                   | <b>Browsing On</b>                     |
         | BrowseOrder allow,deny         | BrowseOrder allow,deny          |
         | BrowseAllow @LOCAL             | BrowseAllow @LOCAL              |
         | <b>BrowseAddress @LOCAL</b>           | <b># no setting for BrowseAddress!</b> |
         |--------------------------------+---------------------------------|
         | ------ (extracts from the respective CUPS configuration) ------- |
         +------------------------------------------------------------------+
</font></pre>

<p>Now let's see what different results we get if we run a port scanner against each of these two configurations.</p>


<pre><font color="blue">
+--------+------------------------------------------------------------------+
|        |            <b><i>Does "Browsing On" open a port? -- No!</i></b>                |
|--------+--------------------------------+---------------------------------|
|Setting:| Browsing <b>Off</b>                   | Browsing <b>On</b>                     |
|--------+--------------------------------+---------------------------------|
|Probe:  | nmap -p 631 10.16.2.4          | nmap -p 631 10.16.2.4           |
|--------+--------------------------------+---------------------------------|
|        | Interesting ports on 10.16.2.4:| Interesting ports on 10.16.2.4: |
|Result: | PORT    STATE  SERVICE         | PORT    STATE  SERVICE          |
|        | 631/tcp <b>closed</b> ipp             | 631/tcp <b>closed</b> ipp              |
+--------+--------------------------------+---------------------------------+
</font></pre>

<p>Thus, the whole argument of Ubuntu packagers why they disable client-side printer browsing in Dapper manifests itself as bogus. <tt>"Browsing On"</tt> on its own does not open a TCP port to the outside world. Hence, their modification to set <tt>"Browsing Off"</tt> does not close a TCP port, and does not make the system more secure. But it indeed does make the system less functional, less powerful, and more user-unfriendly, more crippled... It takes away out-of-the-box client-side printing capabilities, which work with zero setup and zero driver installation. It hurts Linux competitiveness on the desktop market, especially the small business, the public sector and the enterprise market segments.</p>

<p>But they made more modifications which are bad. Look at their <tt>"BrowseAddress @LOCAL"</tt> setting. It is enabled by default; the "@LOCAL" macro is shorthand to tell CUPS to address all local network interfaces and subnets (excluding modem, ISDN card or ppp0 links) in case it broadcasts its own printers. It does not do anything as long as the accompanying <tt>"Browsing Off"</tt> is kept. Now assume, a user wants to enable <i><b>client-side</b></i> printer browsing only (without any server-side browse announcements going out). She (of course) follows the advice given to her by CUPS.org and changes the setting to "<tt>Browsing On"</tt>.  But on Ubuntu all of a sudden something will happen that 99% of users (I bet!) are unaware of. And it certainly does not blend in well with the declared security goals of the Dapperistas: the <tt>"BrowseAddress @LOCAL"</tt> setting now kicks in as well; the user's box immediately starts to <b><i>announce</i></b> its own printers, effectively turning on server-side printer browsing and active broadcasts as well.... <b>Oooops!</b>!</p>

<p>Compare this to the default CUPS config file shipped with the sources by CUPS.org: since it has no default BrowseAddress enabled, this problem does not occur. Users can turn client browsing on or off at will without turning their computer into an active broadcaster.</p>

<p>All in all, Ubuntu Dapper does not convince me that its CUPS package maintainers have created a well considered printing setup. There is a lot to fix, and a lot to improve. Next topic: SNMP network printer auto-discovery. Stay tuned!</p>

<!--break-->