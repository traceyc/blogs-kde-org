---
title:   "\"No hoods. No electric shocks. No beatings. These Iranians clearly are a very uncivilised bunch\""
date:    2007-04-01
authors:
  - pipitas
slug:    no-hoods-no-electric-shocks-no-beatings-these-iranians-clearly-are-very-uncivilised-bunch
---
<p>Not an April Fool's day article. Appeared yesterday. In the British Guardian:</p>


<dl>
<dt>&nbsp;</dt>
<dd>
<p><b>"No hoods. No electric shocks. No beatings. These Iranians clearly are a very uncivilised bunch"</b></p>

<p><i>"I share the outrage expressed in the British press over the treatment of our naval personnel accused by Iran of illegally entering their waters. It is a disgrace. We would never dream of treating captives like this - allowing them to smoke cigarettes, for example, even though it has been proven that smoking kills. And as for compelling poor servicewoman Faye Turney to wear a black headscarf, and then allowing the picture to be posted around the world - have the Iranians no concept of civilised behaviour? For God's sake, what's wrong with putting a bag over her head? That's what we do with the Muslims we capture: we put bags over their heads, so it's hard to breathe. Then it's perfectly acceptable to take photographs of them and circulate them to the press because the captives can't be recognised and humiliated in the way these unfortunate British service people are."</i></p>

<p><i>"It is also unacceptable that these British captives should be made to talk on television and say things that they may regret later. If the Iranians put duct tape over their mouths, like we do to our captives, they wouldn't be able to talk at all. Of course they'd probably find it even harder to breathe - especially with a bag over their head - but at least they wouldn't be humiliated."</i></p>

<p><i>"And what's all this about allowing the captives to write letters home saying they are all right? It's time the Iranians fell into line with the rest of the civilised world: they should allow their captives the privacy of solitary confinement. That's one of the many privileges the US grants to its captives in Guantánamo Bay."</i></p>

<p><i>"[....]"</i> </p>
</dd>
</dl>

<p>It's well worth reading <a href="http://www.guardian.co.uk/print/0,,329764373-103677,00.html">the whole piece</a>...</p>

<!--break-->