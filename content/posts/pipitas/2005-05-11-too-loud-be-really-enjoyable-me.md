---
title:   "Too loud to be really enjoyable for me"
date:    2005-05-11
authors:
  - pipitas
slug:    too-loud-be-really-enjoyable-me
---
<a href="http://c133.org/blog/random/coolest_thing_all_week.html">No, clee</a> -- it <a href="http://www.motorschirm-motorgurt.de/spanien/page1.htm">is old news</a>, very old in fact ;-)   And the guy for sure didnt "invent" it.<br>
The equipment is <a href="http://www.motorschirm-motorgurt.de/spanien/P1010015.htm">too heavy to really carry</a> on your back. It is noisy. And it stinks. I should know -- I actually tried it once. I didnt enjoy it much, though it momentary felt like a cool thing. But to each his own....I <a href="http://blogs.kde.org/node/view/980">like it more without</a> the engine. Just your muscles and the sun, baby.
<!--break-->