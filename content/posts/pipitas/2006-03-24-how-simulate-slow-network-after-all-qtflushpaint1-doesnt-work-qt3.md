---
title:   "How to simulate a slow network (after all, QT_FLUSH_PAINT=1 doesn't work with Qt3)"
date:    2006-03-24
authors:
  - pipitas
slug:    how-simulate-slow-network-after-all-qtflushpaint1-doesnt-work-qt3
---
<p>I think it is time to reveal a nifty little tool that I like to simulate a slow network connection, even without a network. It is called <i>"tc"</i> (think "traffic control") and is present on every modern Linux system. It is part of the "iproute" or "iproute2" package.</p>

<p>tc lets you simulate (amongst other things) latency. We all know how network latency of even the slightest degree kills off every amount of remote X11 usability. Even if you throw multi-Megabits of bandwidth towards it, that will never compensate -- because it is just the wrong cure for an illness that consists of many thousand roundtrips undertaken by minuscule packets.</p>

<p>Aaron <A href="http://aseigo.blogspot.com/2006/03/better-than-watching-paint-dry.html">hinted at the cool Qt4 builtin</A> feature that allows for easy diagnosing of superfluous repaint operations of certain widgets (which also induce shitloads of roundtrips on the wire if an app displays its GUI across a network link). But what about Qt3 and KDE 3.x applications? Obviously setting <tt>"QT_FLUSH_PAINT=1"</tt> will be useless here. <A href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/166-Paint;-Sound;-Triage.html">Adriaan proposed to test an application remotely</A> over a not-too-fast DSL line; "ssh -X" should run it securely, and even add a tad bit more of latency to the link.</p>

<p>That's fine; but it is not good enough. (Well, maybe for Adriaan, since he does not seem to use Linux too much -- he's more a FreeBSD type of guy. But for the *BSDs there is "trickle" to achieve similar things as "tc" can do for our purpose.) After all, you're not always on a "not-too-fast DSL line"; plus, you probably want to have full control over the network parameters. You want to emulate slowness even if you are on a *fast* DSL line. You want to see how an app behaves over a network even if all that you have is <i>lo</i> on <i>localhost</i>...</p>

<p>So, back to tc. We use tc quite regularly to test FreeNX and <A href="http://www.nomachine.com/">NoMachine NX</A> with it. The beauty of tc is that it does work even <b><i>*without*</i></b> a network connection. Because you can apply traffic control parameters even to the loopback interface. Travel Bundesbahn, fly Lufthansa, hitchhike on the backseat of a Porsche, enjoy a hacking session in the Norwegian woods... and always be able to simulate a network connection for your application to run across; no need for having an ethernet cable plugged in, or a Wifi card switched on.</p>

<hr>

<dl>
<dt><b>CAUTION!</b></dt>
<dd><p><small>Be very cautious if you run SUSE, though! I found a bug in SUSE kernels which <b>make the system to freeze completely</b> if you enable such a traffic control queueing discipline; as soon as you then actually start having traffic, you better had saved your open documents. I filed a bug back in September, but it is not yet fixed. The <A href="https://bugzilla.novell.com/show_bug.cgi?id=116309&x=21&y=4">bug number is 116309</A> in Novell's bugzilla. It affects at least SUSE-9.1, 9.2, 9.3 and 10.0 default kernels for i386 architectures.</small></p>

<p><small>This bug did so far not happen on Debian/Knoppix/Kanotix systems where I tested various tc commandline.</small></p></dd>
</dl>
<hr>

<p>OK, here is what I remember (I don't have my notes here -- I'm using a company WinXP notebook with NX Client currently to read mail remotely and browse the 'Net right now, all from a cold, loud Hotel room. Actually I'm accessing mail via KMail in an NX session running on a SUSE system; so no way in hell will I now test the command for you to verify...). The base command (for root) is s.th. like:</p>

   <pre>  {/usr}/sbin/tc qdisc add dev lo root handle 1:0 netem delay 20msec</pre>

<p>(I *hope* I remember correctly; otherwise, the syntax should be at least close to the above line). You need to be root to execute the tc command. SUSE places it in /usr/sbin/, Debian in /sbin/.</p>

<p>This command adds a queueing disciplin ("qdisc") to the loopback device ("dev lo") that enforces a network emulation ("netem") with a (fixed) delay of 20 milliseconds for each package.</p>

<p>Now to test the result. You can see the practical effect of the currently active queueing discipline on the loopback interface simply by using ping:</p>

   <pre>  ping localhost</pre>

<p>Note, that ping roundtrip times should now be around 40 milliseconds; because the given delay of 20 msec is applied to both directions. To display the currently active queuing disciplin on all devices use simply:</p>

   <pre>  tc qdisc</pre>

<p>To remove an active queueing discipline for "dev lo", use "del":</p>

   <pre>  tc qdisc del dev lo root</pre>

<p>You can change "lo" for "eth1", "wlan2", "lan0" or whatever device name may be appropriate. (I myself tested mostly with "lo" and it worked very well):</p>

   <pre>  tc qdisc add dev eth0 root handle 1:0 netem delay 55msec</pre>

<p>Also, you can add more parameters:</p> 

   <pre>  tc qdisc add dev lo root handle 1:0 netem delay 60msec 12msec 25%</pre>

<p>This will add a 25% probability that the avarage 60msec delay will have a deviation of +12/-12msec. Finally, to modify an active queuing disciplin, use "change" instead of "add" or "del":</p>

   <pre>  tc qdisc change dev lo root handle 1:0 netem delay 110msec 5msec 50%</pre>

<p>On SUSE, the "tc" command comes as part of the <i>"iproute2"</i> package. On Debian it's <i>"iproute"</i>.</p> 

<p>And tc can do even more sophisticated traffic shaping. You can insert a probability for packet losses, enforce packet duplications, simulate packet corruption, create packet re-ordering and what-not. (But I personally never played with *these* much).</p>

<p>You can probably find more info by googling for "netem" and "Stephen Hemminger". Hemminger is the author of the tool.</p> 

<p>Stephen is an OSDL employee, and also maintains the "linux-net" Wiki with articles about tc and netem. This is where I originally found the utility. Before that, I played with <A href="/gg:trickle download">"trickle"</A> which is not as powerful overall as tc (but can be used without root privileges).</p> 

<p>If I had a wish free with the KDE4 programming guideline drafting gods, I'd ask for this test to be passed as a mandatory one to each application:
<ul><li><i> $APP runs just fine remotely even over a slow line and is fully usable without too much annoyance. Tested with "tc qdisc" delays of 40-80 msecs. --> <b>Check!</b></i> </li></ul>
Cheers. Kurt</p>
<!--break-->