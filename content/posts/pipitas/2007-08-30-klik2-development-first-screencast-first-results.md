---
title:   "klik2 Development: A First Screencast with First Results"
date:    2007-08-30
authors:
  - pipitas
slug:    klik2-development-first-screencast-first-results
---
<p>+++ klik development taken up some speed, progressing rather nicely now +++ stop <br />
+++ currently working on <a href="http://klik.atekon.de/wiki/index.php/Klik2"><b>version 2 of klik</b></a> client/runtime environment +++ stop <br />
+++ moved all development activities to <a href="http://code.google.com/p/klikclient/"><b>Google-Code</b></a> +++ stop <br />
+++ klik2 will no longer use shell/bash for the client runtime code, but python +++ stop <br />
+++ loopmount from <a href="http://klik.atekon.de">klik1</a> (with all its limits and (f)ugliness) is gone -- fusemount is the new king +++ stop <br />
+++ klik1 did binary-patch away absolute paths embedded in its images -- klik2 will use completely unmodified .rpm and .deb and .tgz packages as ingredients +++ stop <br />
+++ klik1 expected to be run on a debian-etch-alike host linux system -- klik2 will run on any distro that complies to the lsb 3.(1?) specification +++ stop <br />
+++ klik1 mixed commandline with gui components (xdialog, kdialog, zenity) in one single bash script -- klik2 will sport a clean commandline interface and expose an API to write "native" gui frontends (Gtk, Qt, KDE, Tcl/Tk, PyKDE, PyQt, ncurses,... $whatever) +++ stop <br />
+++ klik1 was alpha, proof-of-concept, ugly, hack-ish... software, but worked (if the recipe maintainer found time to do his job) -- klik2 will become stable, polished, cleanly designed... and will work even better (and therefore attract more recipe maintainers, with more time too :-) &nbsp;) ++++ <br/><br/>
<hr>
++++ <a href="http://video.google.com/videoplay?docid=-1910211845236337938"><b>first screencast of current klik2 in action</b></a> (proofing how cool, easy-to-use and 'grandma-safe' klik2 will be once it is ready) in a <a href="http://video.google.com/videoplay?docid=-1910211845236337938"><b>Google klik2 Video</b></a> (58 seconds) ++++</p>
<hr>
<p><b>Go  <a href="http://video.google.com/videoplay?docid=-1910211845236337938"><b>watch</b></a> it!</b></p>
<p>Watched the video now? Disappointed because of the Gtk interface? You should be...  <b>:-)</b> </p>

<p>That's because <b>"He, who writes the code, decides."</b> </p>

<p>We just 'don't have no-one' to write Qt/KDE/PyQt/PyKDE/$whatever GUI code yet. But we have people who can write the Gtk stuff.... Do *YOU* want to be the one who makes the difference? Then come visit us in the <a href="irc://irc.freenode.net/klik">#klik IRC channel</a> on Freenode.</p>