---
title:   "FOSDEM 2008 'speaker interview' about klik2 published"
date:    2008-02-03
authors:
  - pipitas
slug:    fosdem-2008-speaker-interview-about-klik2-published
---
<p>The FOSDEM 2008 organizers now have published their <A href="http://fosdem.org/2008/interview/kurt+pfeifle+and+simon+peter">'speakers interview' with probono and myself</A>.</p>

<p>If you are interested in some background about the current <A href="http://code.google.com/p/klikclient/">klik2 development</A>, it may be serving as a good general introduction into the concepts.</p>