---
title:   "'Progress' in Afghanistan..."
date:    2007-08-13
authors:
  - pipitas
slug:    progress-afghanistan
---
<p>It seems to be an undisputed fact, that Afghanistan in 2007 no longer exports much raw opium at all.</p>

<p><i>"Good"</i>, you'll probably say. <i>"That is because the Western troops now have chased the Taliban back into the mountains. The fight to bring democracy and Western culture to this backward country finally seems to show promising results."</i></p>

<p>Wait a minute. Don't draw your conclusions preliminarily.</p>

<p>Afghanistan doesn't export raw opium any longer, true. Yet, this year it harvests the highest amount of opium the world has ever seen... Additionally, the country is doing what our Western international aid efforts urge every developing country to do: Afghanistan has gone into manufacturing and 'value-added' operations.</p>

<p>What Afghanistan <b>does</b> export now, is <b>heroin.</b> Yes, in the Afghanistan of today raw opium is processed to heroin. On a *massive* scale. More precisely: on an industrial scale, not just in some backyard kitchens.  This 'achievement' alone accounts for 40% of all of Afghanistan's GDP -- as well as for the record 66% growth of the GDP in the six years of Western occupation...</p>

<p>Opium processing, outputting heroin requires lots of chemicals. Many millions of liters in fact. Chemicals that are no longer transported into the country by lorries. But by tankers. And then loaded to lorries for further transport to the heroin factories, which use the same roads as the NATO troops use. (You may also wonder which of the Big Ten Monopolies in the world-wide chemical industry is selling them these massive supplies....)</p>

<p>You now think <i>"Maybe the troops are blind? Or too busy fighting the Taliban so they can not pay attention to this illegal industry?"</i></p>

<p>You're wrong again.</p>

<p>Go read <A href="http://www.dailymail.co.uk/pages/live/articles/news/news.html?in_article_id=469983&in_page_id=1770">this complete article</A> in the Daily Mail. It was penned by <A href="http://www.craigmurray.co.uk/">Craig Murray</A>, who used to be the Ambassador of the UK government to Usbekistan, and who is a man who surely had (and maybe still has) access to a lot of information sources that we Joe Does can't even dream of...</p>

<!--break-->