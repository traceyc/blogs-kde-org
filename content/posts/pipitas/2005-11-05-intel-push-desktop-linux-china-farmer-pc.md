---
title:   "Intel Push For Desktop Linux... In China, With 'The Farmer PC'"
date:    2005-11-05
authors:
  - pipitas
slug:    intel-push-desktop-linux-china-farmer-pc
---
<p>Guess what I regard as the most important story around The Internet today?</p>

<p>Right. It is the one that <i>raises crucial questions about the future of the Linux desktop platform in general</i>. A platform, which will be used as personal computing systems by hundreds of millions of people worldwide, within a few years.</p>

<p>It was Tom Adelstein partnering with Carla Schroder who have put up this extremely <A href="http://lxer.com/module/newswire/view/46808/index.html">interesting piece</A>, over at <A href="http://www.LXer.com/">LXer.com</A>. They extensively quote from a document that was forwarded to them, originating from an Intel intranet web server. Basically, they publish extracts from a leaked document, including many photos taken in China, showing the impact of Intel's marketing campaign there. The surprise: Intel is heavily promoting Linux in China.</p>

<p>The document details how Intel proceeds. It even suggests that Intel already has succeeded to gain a very strong foothold on the emerging Chinese PC market. Their product is running Linux! The leaked document basically has 3 main talking points:</p>
<ul><li>a new and inexpensive (<i>"sells for $350 using an inexpensive but high-quality Intel processor"</i>) machine called "The Farmer PC" is helping rural citizens in China to get online and gain access to agricultural and farming data.</li>
<li>potential demand for the farmer PC is said to be huge -- the Intel paper quotes a figure of 73 percent of rural citizens surveyed saying they'd be interested in buying one.</li>
<li>the farmer PC is one of a number of branding and marketing efforts in rural China which represents a potential market of hundreds of millions of people.</li>
</ul>

<p>The document also enumerates a series of steps that Intel has taken in China to gain that much ground. Really interesting read.</p>

<p>Tom and Carla applaud the Intel move. But they also raise some really interesting questions. Asking them alone triggers some very enlightening thought processes in one's own mind. Let me paraphrase: </p>
<ul><li>If this all can be done in China - why not in the US? </li>
<li>Why can Intel champion a cheap, but quality PCs with Linux pre-installed in China -- and why not in the land of Microsoft? </li>
<li>If Intel can sign MoUs with local government bodies (Memorandums of Understanding) to help adoption of Linux in China -- why not do the same in their own home country? </li>
<li>If Intel can run unusual marketing and branding efforts in rural China, such as painting village walls with Linux slogans and Intel logos (flying in even a Vice President of Sales and Marketing!) -- why not run the same campaign where George Bush is President? </li>
<li>If Intel can help to set up 4000 Linux training centers in the rural areas of China -- why can't they do so in the US or in Mexico? </li>
<li>Why can Intel present their Linux product in China as "a really user-friendly PC" -- while in North America and elsewhere in the world this type of decisive pro-Linux marketing seems to be nowhere close?</li>
<li>If Intel know by now their marketing approach did work in China -- what is stopping them from trying the same in the US, in Mexico, South America, Africa, the Middle East, Oceania and other regions of Asia?</li>
</ul>

<p>One key sentence of their story is: <i>"We wonder how strong an influence Microsoft has on Intel when it comes to marketing a similar product as the farmer PC to others."</i></p>

<p>Their conclusion (not at the end, but burried in the middle of the article): <i>"We believe that our government should investigate the relationship between Intel and Microsoft. We believe the government should appoint an independent special prosecutor to investigate everything Microsoft does, especially the relationships between Microsoft and hardware vendors; not only desktop systems, but peripherals like network devices, printers, expansion cards, and so forth. That's where customer choice has been completely derailed."</i></p>

<!--break-->