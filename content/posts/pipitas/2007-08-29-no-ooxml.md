---
title:   "No OOXML!"
date:    2007-08-29
authors:
  - pipitas
slug:    no-ooxml
---
Y'all are aware of the current frenzied push by Microsoft to whip their OOXML file format (used for MS Office 2007, <img  align="right" hspace="6" vspace="3" src="http://www.noooxml.org/local--files/start/isomeeting.jpg" class="showonplanet" /> described on some 8.000 printed A4 pages) through the ISO 'fast track' standardization process to make it a 'standard'.

You may not be aware of the <a href="http://www.noooxml.org/"><b>'No-OOXML'</b></a> website though, which collects information and articles around the issue that focus on why OOXML (like it is proposed right now) is *not* a good idea.

Microsoft did a lot of work in the last few weeks to influence votes, stuff the ballots and manipulate participants in the various country ISO committees to enforce favorable decisions for themselves:

<ul>
  <li><a href="http://www.groklaw.net/article.php?story=20070829070630660">"Microsoft Memo to Partners in Sweden Surfaces: Vote Yes for OOXML"</a></li>
  <li><a href="http://www.groklaw.net/article.php?story=20070827111019189">"More Irregularities in the OOXML ISO Process Surface"</a></li>
  <li><a href="http://blogs.freecode.no/isene/?p=3">"OOXML in Norway: The haywire process"</a></li>
</ul>

So the cartoon shown in this post, directly from the No-OOXML website, summarizes MS's efforts rather nicely (and may be not so funnily)...

<small><b>Disclaimer: I'm currently not aware that Novell and/or Apple representatives did indeed (as depicted in the cartoon) vote in favor of OOXML, but I may have missed that if it so happened.  </b>If you know something about this, leave a comment. In any case, the main characters of this cartoon are not represented in a simple ASCII subset anyway....</small>
<!--break-->
