---
title:   "sidux -- a new star in the Linux galaxy"
date:    2007-02-23
authors:
  - pipitas
slug:    sidux-new-star-linux-galaxy
---
<p>Two days ago the first incarnation of <a href="http://sidux.com/Article116.html">sidux</a> was released, code-named "Chaos". sidux is a desktop-oriented distribution. It comes as a Live CD based on the "unstable" branch of Debian, but is easily able to install onto harddisk from the running Live CD using a completely new graphical installer frontend. </p>

<p>On <A href="http://distrowatch.com/table.php?distribution=sidux"><strike>Distrowatch</strike></A>, sidux already climbed into the <A href="http://distrowatch.com/index.php?dataspan=1">Top 20 list</A> for the last 7 days.</p>

<p>(Some background about the naming: <strike>The unstable branch of Debian always goes by the name "Sid". The now current Sid will become the next "stable" release named "Etch". On the day of Etch's release, Sid and Etch will be identical for a moment, but then Sid will take up pace again in assimilating and testing newer package versions to prepare the next release after Etch. Sidux will always be based on Sid.</strike> Second attempt: The unstable branch of Debian always goes by the name "Sid". Sid feeds into "testing" [which changes names -- current one is "Etch"]; "testing" at a certain point in time will be frozen [this is the case right now], and then becomes the next "stable" release [which will be "Etch" this time]. Sidux will always be based on Sid, which is the permanent name for the "unstable" branch.) </p>

<p>Sidux is created and maintained by <a href="http://sidux.com/Article2.html">a group of developers</a> who split from the Kanotix project and launched their own distribution. Kanotix had not seen much development activities in the last 12 months, and Kano (its founder) last October had announced plans to re-base Kanotix itself away from Debian/Sid to a more "stable" distribution (either Debian Etch or Ubuntu). In this light, sidux is a continuation of most of the known Kanotix tradition, carried by a big and active part of previous Kanotix contributors (but seemingly with less emphasize on a "benevolent dictator" type of governance, and more focus on "team responsibilities"). </p>

<p>The group is well organized and gets things done efficiently. They managed to get the <i>sidux Foundation, Inc.</i> organized within a few weeks, as a <i>Nevada incorporated IRS 501(c)(3) not-for-profit organization</i>. They released a well tested Live CD for 2 different architectures within 3 months of development (with several preview releases). Their ISO creation process was written from scratch in a clean room environment, using no code from Kanotix.</p>

<p>Their self-proclaimed long-term ambition: to <i>"strive to do the impossible: making Debian Sid (aka "Unstable") stable. The goal is to become the best Debian Sid based live distro with special focus on clean and easy hard disk install. Strategic milestones and 3-4 annual releases timetabled will give stability and accountability to corporate and home users with a demand for bleeding edge software running on modern hardware, as well as a definable path over time."</i></p>

<p>sidux "Chaos" ships KDE 3.5.5 (en and de) [<a href="http://sidux.com/PNphpBB2-viewtopic-t-1834.html">Complete package list</a> visible when logging in]. There are <a href="http://debian.tu-bs.de/project/sidux/release/"><b>downloads</b></a> of 2 types of iso images: 64bit (for AMD64 &amp; Intel Core2 CPUs) and 32bit (for i686 CPUs), with a ~701 MByte size ("KDE fulll") and 402 MByte ("KDE lite") version each (<a href="http://linuxtracker.org/browse.php?cat=444">torrents here</a>).</p>

<p>For optimal support of bleeding-edge hardware, have a look at the <A href="http://www.sidux.com/Article116.html"><i>hints for hardware with non-free needs</i></A>.</p>

<p>My first impression: sidux is even better than Kanotix (which I experienced to be the best Live distro when it was still actively developed); and their IRC channel (<a href="irc://irc.oftc.net/sidux">#sidux on server irc.oftc.net</a>) is just as helpful and newbie-friendly as #kanotix is.</p>

<p>What's most impressive: the huge amount of <a href="http://manual.sidux.com/en/welcome-en.htm">user documentation</a> their documentation and translation teams have managed to write up in the short amount of time (the sidux user manual is currently available in English as well as Danish, Kroatian, Spanish and German).</p>

<hr>

<p><b>Update:</b> In IRC, toma had this to tell me about how little I understand Debianisms: <i>"pipitas: your debian blurb is not quite correct. etch = testing and will become next stable, sid is already accepting newer versions or new packages, they just wont flow to testing/etch anymore"</i>. Oh, well. In fact, I <i>used</i> to know this already, once upon a time. It is just <i>today</i> that I (once more) mixed up Debian "testing" with "unstable". Hence my above <strike>strikethrough editing</strike> and replacements.... -- I hope the rest of my scribble above is more accurate....</p>

<!--break-->