---
title:   "Ghostscript 8.60 is out! It's merged with CUPS' ESP Ghostscript! It's cool! We want packages!"
date:    2007-08-03
authors:
  - pipitas
slug:    ghostscript-860-out-its-merged-cups-esp-ghostscript-its-cool-we-want-packages
---
<b><i>...or: Why has no consumer distro been providing updated packages for Ghostscript for more than a year?</i></b>

<p>It's been <a href="http://blogs.kde.org/node/2100">more than a year</a> that artofcode LLC and its lead developer, Raph Levien have <a href="http://advogato.org/person/raph/diary.html?start=411">revealed</a> that bleeding egde Ghostscript will no longer be AFPL licensed, but will switch its Subversion-held development tree to the GPLv2 license.</p>

<p>This in turn paved the way for Mike Sweet, developer of CUPS and maintainer for ESP Ghostscript <a href="http://www.cups.org/espgs/articles.php?L390">to announce</a>: "everyone will be able to use the latest Ghostscript and contribute fixes back to the core", implying his willingness to overcome the fork (which at a far time back in the past was a necessity in order to make the CUPS ball rolling at all).</p>

<p>Then these things happened:</p>

<ul>
 <li>2006-05-17: GPL Ghostscript 8.54 was <a href="http://ghostscript.com/doc/current/History8.htm#Version8.54">released</a></li>
 <li>2007-03-14: GPL Ghostscript 8.56 was <a href="http://ghostscript.com/awki/GPL_Ghostscript_8.56">released</a></li>
 <li>2007-04-30: "ESP and GPL merger on its way" <a href="http://www.cups.org/espgs/articles.php?L463">Announcement</a></li>
 <li>2007-05-11: GPL Ghostscript 8.57 was <a href="http://ghostscript.com/awki/GPL_Ghostscript_8.57">released</a> (which had the first stage of the merger with ESP Ghostscript included)</li>
</ul>

<p>An awful lot of that painstaking work has been silently conducted by the ever-industrious and great <a href="http://www.linux-foundation.org/~till/">Till</a> &nbsp; <a href="http://www.linuxday.at/typo3temp/pics/29284495f7.jpg">Kamppeter</a> who worked days and weeks on end to make the merger come true.</p>

<p>And now it's done.</p>

<p><b>GPL Ghostscript 8.60 (merged with ESP Ghostscript) is released (since 2 days ago).</b></p>

<p>Strangely, all these cool developments didn't spark any amount of noticeable interest, not even in a year.  Not much mentioning in the relevant online press. No increased activity on the part of distro packagers to provide updates and/or backports. And not much demand from the general Linux user demography either....</p>

<p>Let's try and fix this now. Let's wake up the sleepie ones. Let's shout it from the rooftops (maybe provides some impetus to cause some more packager activities?): </p>

<p><b>Ghostscript 8.60 is released! It's the bleeding edge! It's no longer AFPL, it's GPL!! It's cool!!! It has new features!!!! It is merged with ESP Ghostscript (which now is end-of-life-d)! It has much better support for working with PDFs (processing and creating them)! And for spot colors! And for color profiles! And for shadings! IT IS A MUST-HAVE! IT IS A MUST-HAVE! IT IS A MUST-HAVE...</b></p>

<p><small><b>P.S.:</b> <i>and while you're at it, please make the new Ghostscript builds use its newly gained modularity, will you? No, not any longer a need for one single monolithic Ghostscript binary. Yes, drivers modular! Yes, and support for an outsourced, separate Ghostscript library that can be used by "-sDEVICE=x11". No -- no more different builds for "--with-x11" and for "--without-x11" support... So, when will you be ready?</i></small></p>
<!--break-->
