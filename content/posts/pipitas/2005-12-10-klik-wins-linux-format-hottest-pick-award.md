---
title:   "klik wins \"Linux Format Hottest Pick\" award"
date:    2005-12-10
authors:
  - pipitas
slug:    klik-wins-linux-format-hottest-pick-award
---
<p>We got notified from the <A href="http://www.linuxformat.co.uk/"><i>Linux Format</i></A> (a printed magazine sold in UK newspaper stands) that they give their Hottest Pick award to <A href="http://klik.atekon.de/">klik</A>. -- <b>Woooohoo!</b></p>

<p>The January 06 edition carries a 1-page article about klik, which is a very nice read. (bfree bought the thingie at probono's request and made a <a href="http://klik.atekon.de/klik-hotpick.jpeg">scan available</a> to us). </p>

<p>The only little thing to nitpick is that they put klik into the "package manager" category (which <A href="http://klik.atekon.de/wiki/index.php/User's_FAQ#Is_klik_a_new_alternative_package_management_system.3F">it is explicitely not</A>, and which it <A href="http://klik.atekon.de/wiki/index.php/User's_FAQ#Is_klik_destined_to_become_a_package_management_system_once_it_is_more_mature.3F">doesn't want to be</A>).</p>

<!--break-->