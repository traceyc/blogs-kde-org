---
title:   "klik recipe maintainers wanted"
date:    2005-10-24
authors:
  - pipitas
slug:    klik-recipe-maintainers-wanted
---
<p>One other improvement was introduced by probono yesterday: the beginning of a <A href="http://klik.atekon.de/maint/">recipe maintainer</A> interface on the klik web server.</p>

<p>klik started out to provide a means to install additional software packages into Linux system running from a Live CD (Knoppix, Kanotix). It is now extending its reach, to make its compelling ideas available to more users, on more distros, and also to developers.</p>

<p>The first stage of this evolution was more or less an experiment to find out how much work needs to be put into refining klik's basic concept to make it work more universally. It turned out that it was much less than expected. Initial "multi" klik packages are working *much* better on multiple distros than one could ever have dreamed of. We hear even reports from users of currently unsupported distros (Gentoo, Slackware, RH/Fedora) telling us that an astonishingly high percentage of klik packages/recipes works for them without a flaw.</p>

<p>Currently, most of the recipes offered from the klik server are created automatically. The klik server assumes a given base set of system libraries for each of its supported clients OSes. The server also owns a recent DB dump of the Debian APT repository dependencies. Once a client asks for a certain recipe, the server runs "serverside apt" to resolve dependencies for a given package, and includes them into the recipe (if required, because the base system is known to not possess them, or not possess the required version).</p>

<p>Some distros follow different naming conventions for the same library. The recipe covers this by creating some appropriate symlinks inside the .cmg. Since binaries used in the klik recipes usually are build on an "older" system (Debian Sarge, or SUSE-9.2), one of the libs that go into each .cmg is <i>libstdc++.so.5</i> to make sure the .cmg will run on more modern systems too (those based on libstdc++.so.6).</p>

<p>These, and a few more very simple measures suffice to make klik recipes and klik .cmgs work on multiple distros fairly well. In most cases where there were failures, these could be fixed very easily: changing 1-5 lines of shell code was enough to make users report "Now it works!". klik developers were themselves pretty much surprised by this outcome of that first stage.</p>

<p>However, it is now time to go towards the next stage: the plan is that all klik recipes will be subjected to some human scrutinization and maintainer testing in a more organized way. But we can't do that on our own. We need help. We need <i><b>you</b></i>. </p>

<p>You do not need to be a C++ programmer to be able to fix and maintain a klik recipe for a given application. (None of the klik developers is either, BTW). If you are an enthusiastic Linux user, and if you have started to learn a bit of Shell scripting, this is already enough of technical qualification. Add the willingness to look after at least two or three of your favourite GUI software programs in the form of klik recipes, and your new fun can start tomorrow.</p>

<p>My own, more general ideas about what should be the first visible results of this effort are the following:</p>
<ul>
 <li>we should dump one (small) part of the current recipe set altogether: it doesn't make much sense to offer a cupsys or dhcpd or bind server via klik (even if they were included in the original Debian DB dump imported to the klik realm). </li>
 <li>we should put the big part of packages and recipes (enumerating approximately 4.000) into a category as "untested", and try to find a number of active "recipe maintainers" to look after and fix those which fail.</li>
 <li>we should create a visible differentiation, also in the klik web interface, of packages that are tested and "known to work" well on a set of given distros. These packages should get labelled appropriately. Maybe the total number of packages would be not more than 100 initially, and grow from there over time, with the help of newly established recipe maintainers.</li>
</ul>

<p>I myself have <A href="http://klik.atekon.de/maint/?maintainer=pfeifle%20at%20kde.org">taken over</A> a few KDE-related applications for now. I haven't done much work on them yet. The first one I'll start with will be <A href="klik://amarok">klik://amarok</A>. (This will give you amarok-1.3.2 for now). You can see my web interface to access the amarok recipe for further manipulations <A href="http://klik.atekon.de/maint/recipe.php?package=amarok">here</A>. Notice that amarok is tagged as "pending". My initial fixes of today made it work at least on SUSE-9.3 and 10.0 ($Debian not yet tested). Before, it didn't work at all. You can test it -- but since it is "pending", you'll be asked to type in <A href="mailto:pfeifle@kde.org">my KDE email address</A> to start the download. More about my adventure in klik-ifying amarok in one of my next blog entries...</p>

<!--break-->
