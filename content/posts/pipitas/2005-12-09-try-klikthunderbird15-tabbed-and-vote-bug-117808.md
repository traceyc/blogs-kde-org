---
title:   "Try klik://thunderbird15-tabbed (and vote for bug #117808)"
date:    2005-12-09
authors:
  - pipitas
slug:    try-klikthunderbird15-tabbed-and-vote-bug-117808
---
<p>One of my long standing feature requests now seems to  come true: <i>tabbed email processing</i>. Not with <A href="http://kontact.kde.org/">Kontact</A> or <A href="http://kmail.kde.org/">KMail</A>, though. It is with <A href="http://www.mozilla.com/thunderbird/">Thunderbird</A>. But it is not yet in the <A href="http://www.mozilla.com/thunderbird/releases/1.5.html">official release</A>. It is just a <A href="https://bugzilla.mozilla.org/show_bug.cgi?id=297379">patch</A>, created by Thunderbird hacker <A href="http://www.melez.com/mykzilla/">Myk Melez</A>. Two days ago <A href="http://www.melez.com/mykzilla/2005/12/tabbed-message-browsing-in-thunderbird.html">he blogged</A> about it. </p>

<p>After having had this idea about a year ago, I talked to various KMail hackers. Their initial response was so discouraging, that I nearly myself started to believe it was a bad idea. Therefor I never submitted the feature request in bugzilla. Now of course, my interest is resparkling.</p>

<p>Reading Myk's blog, I also found his <A href="http://www.melez.com/tabmail/">link to various builds</A> where he offers patched binaries with the tabbed interface feature enabled.</p>

<p>Oh, pre-built binaries! Linux included! But only <i>.bz2</i> and <i>.tar.gz</i>? No real packages? No <i>.rpm</i>, no <i>.deb</i> ?!? -- Damnit!...</p>

<p>No, wait... there is klik!</p>

<p>klik can take any packaged binaries (.rpm, .deb, .tgz, .tar.gz, .bz2) and convert them into a <i>.cmg</i> (a compressed file system imagage, similar to an ISO image, which can be run by the klik client scripts; the .cmg follows the paradigm <i>"1 app == 1 file only"</i> and has not even a chance to mess up the system libraries). </p>

<p>It took me exactly 1 minute to create 2 initial klik recipes and run a first test. Of course it didn't work. The build used libstdc++.so.6. OK, include that package from the Debian repository into the .cmg bundle. Make klik's "serverside apt" automatically find the right URL, and also include other dependencies, if present. Hmmm, still didn't work. The .bz2, unpacked, has this non-LSB directory layout, where the binary and the thunderbird libs are all inside the subdirectory thunderbird/. OK, 10 more minutes of fiddling with injecting some symlinks into the .cmg, and everything worked (on SUSE-9.3 for a start).</p>

<p>So here is for your testdrive and tabbed email evaluation pleasure the result:</p>
<ul>
<li> <a href"klik://thunderbird15-tabbed">klik://thunderbird15-tabbed</a> (a patched 1.5 release candidate version)</li>
<li> <a href"klik://thunderbird16-tabbed">klik://thunderbird16-tabbed</a> (a patched 1.6 Alpha version) </li>
</ul>
<p>If you don't know about <A href="http://klik.atekon.de/">klik</A> yet, here is the <i>20 second installation procedure</i> for the klik client (should work on all $DEBIAN distros, as well as SUSE-9.3/10.0):
<pre> wget klik.atekon.de/client/install -O -|sh</pre>
and follow instructions on the screen.</p>

<p>If this installation instruction sounds suspicious to you (after all you're piping the <tt>install</tt> script into a local shell for direct execution), read the <A href="http://dot.kde.org/1126867980/">klik intro article</A> or visit the <A href="http://klik.atekon.de/wiki/index.php/User's_FAQ">klik FAQ</A> page.</p>

<p>The klik recipe uses as ingredients the binaries provided by Myk himself <A href="http://www.melez.com/tabmail/">http://www.melez.com/tabmail/</A>. So if you would trust this source (Myk) and install his binaries onto your system, you can as well trust the klik package. Well, you can trust the klik package <b>even more</b>: it doesn't install Myk's libs into your <i>system</i> (not even into <i>/usr/local/</i>) -- it only embeds them into its single .cmg file, and lets them not get anywhere close to Your Package Manager's Empire. </p>

<p>klik is the fastest and most easy way to testdrive new packages, across different supported distros. I hope at least 2 or 3 KDEpim hackers will now use these 2 new kliks to have a very quick look at what our friendly Thunderbird competition came up with. I think these are great ideas. (And if there was an option to also "print at once" all mails currently opened in tabs, I think it is close to perfection ;-).</p>

<p>Oh, BTW: there are more people who'd love to see tabbed email processing: George Staikos has submitted <A href="http://bugs.kde.org/show_bug.cgi?id=117808">feature request #117808</A> only 2 days ago. There are older two ones: <A href="http://bugs.kde.org/show_bug.cgi?id=76451">#76451</A> (from February 2004, has a nice mockup going with it) and  <A href="http://bugs.kde.org/show_bug.cgi?id=86327">#86327</A> (from December 2004). Both never received any responses... I closed the older ones as duplicates now [probably not formally the correct way -- but 117808 seeems to have better chances to get some luvin' attention by a KDEpim hacker ;-) ]</p>

<p>So, go and vote for <A href="http://bugs.kde.org/show_bug.cgi?id=117808">bug #117808</A> now. Look at the <A href="http://www.mojavelinux.com/cooker/incoming/tabbed-kmail-concept.png">mockup</A> made by danallen. Use <a href"klik://thunderbird16-tabbed">klik://thunderbird16-tabbed</a> to get an impression about how your email processing workflow with such a feature could be streamlined.</p>

<p><i>Of course, since this is experimental Thunderbird code; you'll surely want to backup your Mail folder first, will you?</i></p>
<!--break-->