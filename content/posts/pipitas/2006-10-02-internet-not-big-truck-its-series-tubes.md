---
title:   "\"The Internet Is Not A Big Truck -- It's A Series Of Tubes!\""
date:    2006-10-02
authors:
  - pipitas
slug:    internet-not-big-truck-its-series-tubes
---
You, the reader of this blog being an internet user: I have a question for you. Have you ever heard about the topic of "Net Neutrality"? If not, you may want to google for it...

The US Senate is blessed with one particularly enlightened member, 72 year old Ted Stevens. The gentleman recently held a speech on net neutrality. It seems that Stevens is the third in line of succession, should something happen to the sitting US President. YouTube has a small extract from his speech, where he describes <a href="http://www.youtube.com/watch?v=URYNnF5mz84"><i>how the internet works</i></a>. After you heard the original audio footing, maybe you'll also enjoy the <a href="http://www.youtube.com/watch?v=EtOoQFa5ug8"> techno remix of the speech</a>... (2 minutes of "fun" guaranteed....)
<!--break-->