---
title:   "\"Moving to PDF as a future print job spooling format\" (linux.com)"
date:    2006-04-19
authors:
  - pipitas
slug:    moving-pdf-future-print-job-spooling-format-linuxcom
---
<p><A href="http://www.linux.com/">Linux.com</A> today published my article <A href="http://applications.linux.com/applications/06/04/18/2114252.shtml?tid=13">"Moving to PDF as a future print job spooling format"</A>. It's already generating some <A href="http://applications.linux.com/comments.pl?sid=36614&threshold=0&mode=nested&commentsort=0&op=Change">feedback</A>....</p>

<p>Currently I'm writing down some more notes which may eventually crystalize into a series of articles. Each one will deal with one of the topics I outlined in my <A href="http://blogs.kde.org/node/1934">last blog entry</A>. Not sure were I'll publish all of these; probably also <A href="https://blogs.kde.org/blog/418">here</A> (I can't imagine there is a Linux-leaning news site out there that would run a series of boring printing articles over 4-6 weeks).</p>
<!--break-->
