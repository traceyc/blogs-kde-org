---
title:   "klik://basesystem"
date:    2005-11-12
authors:
  - pipitas
slug:    klikbasesystem
---
<p>Continuing with the <A href="http://klik.atekon.de/wiki/index.php/User's_FAQ">klik User's FAQ</A> today. This time: the sections dealing with "klik and the Base System", "klik and Package Management", "klik Application Wishlist" and "klik Recipe Maintainers". -- Today's local preview:</p>


<TABLE width="95%" cellspacing="2" border="1" cellpadding="2" align="center" bgcolor="#d3d3d3">
  <tbody>
    <tr>
      <td>

<b>klik and the Base System</b>

<p>
<b>Q: </b><i>Will klik ever mess up my system library installations?</i>
<b>A:</b> No. klik doesn't "install" at all (in the traditional sense). klik is designed to stay away from your system installation. klik is not interfering with your system's package manager.</p>

<p>
<b>Q: </b><i>So you guarantee that running klik will not mess up with my local configuration?</i>
<b>A:</b> No, we do not guarantee this. An application may have a hardcoded or default path to read and write its own configuration files. So the klik-ed OpenOffice.org or the klik-ed KMail applications very likely will touch your pre-existing personal configuration files for these programs (often referred to as "the dot files in your home directory").</p>

<p>
<b>Q: </b><i>What can I do to safeguard my current config files from klik's interference?</i>
<b>A:</b> First, it is not klik's interference -- it is that fact that you use a second instance of the same application ;-)  To be safe, backup or move away the current configuration files (or current Mail data files, or whatever), before starting a klik bundle for a program that you already use as a system-wide application. Or create a separate user which you use to test klik bundles.</p>


<b>klik and Package Management</b>

<p>
<b>Q: </b><i>Is klik a new alternative package management system?</i>
<b>A:</b> No. klik does neither replace, nor interfere with your system's package manager.</p>

<p>
<b>Q: </b><i>Is klik destined to become a package management system once it is more mature?</i>
<b>A:</b> No.

<p>
<b>Q: </b><i>If klik doesnt have a versioning and a package management of its own, how does it handle upgrades if there is f.e. discovered a vulnerability in Firefox?</i>
<b>A:</b> Click on  klik://firefox  again to get the latest version. (And if this still gives you and old version, contact the maintainer of the klik Firefox recipe to gently hint him at the fact that there is a new Firefox version out, so he updates the recipe).</p>

<p>
<b>Q: </b><i>What good is klik then for?</i>
<b>A:</b> klik is good for letting you test bleeding edge software releases and development versions, or use packages that are not available for your distro (or only in outdated versions). Once you start to use and like klik, you will probably find many more use cases on your own.</p>



<b>Application Wishlist</b>

<p>
<b>Q: </b><i>Can I submit a new cool application for klik, so that there will be klik://coolapp link for it?</i>
<b>A:</b> Yes you can. But you'll have to convince us into accepting it.</p>

<p>
<b>Q: </b><i>What exactly do you need for a new klik://coolapp to be accepted?</i>
<b>A:</b> First and foremost, we need a klik recipe maintainer for coolapp.</p>

<p>
<b>Q: </b><i>What does the coolapp recipe maintainer have to do? </i>
<b>A:</b> He will have to create the initial coolapp recipe (but we will help with that). This is really easy in most cases. Then he'll have to monitor coolapp for new releases, and update the klik recipe accordingly. He'll also have to monitor klik://coolapp user feedback to identify common problems so we can fix these. Last, he may need to help modify and test klik://coolapp as soon as we extend klik support to additional distros.

<p>
<b>Q: </b><i>How is the list of package names available from the klik server created?</i>
<b>A:</b> Some of the package names are created from manual entries into the list (about 140 currently). Most of the packages are automatically created using Debian's software catalog (currently nearly 2000). It would not be possible to maintain such a large catalog by hand. We are currently in the process of remove all those package names which are not feasible for klik, and at the same time categorizing them, using "debtags". Do you want to help?</p>


<b>klik Recipe Maintainers</b>

<p>
<b>Q: </b><i>Who can become a klik recipe maintainer?</i>
<b>A:</b> Best, you yourself volunteer to be that person. Or try to convince the coolapp developer himself to maintain the klik recipe.</p>

<p>
<b>Q: </b><i>What is the advantage for a developer having his application in the klik database?</i>
<b>A:</b> Having a "coolapp" in the klik list of working supported applications certainly brings more exposure and users. Having "coolapp-latest" or "coolapp-svn-nightly" can bring more and better bugreports early in your development cycle. It can also help your translators and documentation writers to get their job done. </p>


</td>
    </tr>
  </tbody>
</TABLE>


<p>Tomorrow's topic:  Troubleshooting, Tips+Tricks. I now count 60 out of about 110 questions done; my TODO for tomorrow list another 20. Submit the ones you are interested in, including the answers, please.</p>

<!--break-->
