---
title:   "NoMachine NX-1.5.0 is out (including sources of GPL-ed NX Core libs)"
date:    2005-07-23
authors:
  - pipitas
slug:    nomachine-nx-150-out-including-sources-gpl-ed-nx-core-libs
---
<p><A href="http://www.nomachine.com/">NoMachine</A> <A href="http://www.nomachine.com/developers.php">NX</A> <A href="http://www.nomachine.com/documentation.php">1.5.0</A> is finally <A href="http://www.nomachine.com/news_read.php?idnews=159">out</A>! Including <A href="http://www.nomachine.com/sources.php">sources</A> of the GPL'ed NX Core libraries. It also sports a new <A href="http://www.nomachine.com/manager_screenshots.php">NX Server Manager interface</A> (still Beta).</p>

<ul>
<li>Ever wanted to have a usable remote desktop access to your KDE workstation even when thrown back to a modem dialup link? -- <i>You should have used NX-1.3 or NX-1.4 or FreeNX already in the last 2 years!</i></li>

<li>Ever wanted to be able to detach from your remote X session, and re-attach to it later? Or just recover a session lost by a network cut-off? Or move a remote X session to a different local workstation? -- <i>You should be using NoMachine NX 1.4.0 or FreeNX already!</i></li>

<li>Ever wanted to have sound come with your remote X session? -- <i>NX-1.4 and FreeNX were here for you already since quite some time.</i></li>

<li>Want to work on your home KDE Linux workstation from the office while sitting in front of your Windows notebook? -- <i>You must have missed it: this works wonderfully already since a long time.</i></li>

<li>Ever wanted to resize your remote NX session window by just dragging the window border with the mouse? -- <i>Or toggle to fullscreen mode and back with a keyboard shortcut? NX-1.5.0 now brings this feature to your fingertips.</i></li>

<li>Need to access from Linux remote Windows Terminal Sessions, but don't like the poor 8-bit color depth of the session? -- <i>Go for NX-1.5 (or FreeNX-0.4.2 with the NX Core libs 1.5.0).</i></li>

<li>Don't like the primitive, proof-of-concept type <A href="http://websvn.kde.org/trunk/kdenonbeta/knx/">kNX</A> NX client application that is in kdenonbeta? -- <i>Go hack on it and make it better. Or wait and see if <A href="http://www.planetsoc.com/node/144">Chris Cook's effort</A> in his Google SoC project bears some new fruits in the next few weeks...</i></li>
</ul>

The FreeNX Team is also currently busy with a whole list of things: write a release announcement for the 0.4.2 bugfix release, fight the SVN troubles plaguing our <A href="http://developers.berlios.de/projects/freenx/">Berlios repository</A> getting ready for release the 0.5.0 version which will sport seamless printing powers and a nifty new <i>nxfish</i> utility (derived from KDE's <i>fish://</i> thingie).
<!--break-->