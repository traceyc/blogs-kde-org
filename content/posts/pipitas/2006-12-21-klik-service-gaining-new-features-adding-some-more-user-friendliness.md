---
title:   "klik service gaining new features (adding some more user friendliness)"
date:    2006-12-21
authors:
  - pipitas
slug:    klik-service-gaining-new-features-adding-some-more-user-friendliness
---
<p>probono [image:2591 align="left" hspace=3 vspace=6 border=3 width=150] has added a few cool hacks to the <A href="http://klik.atekon.de/">klik server</A>. One is that all package recipes which are auto-created from the Debian repositories and klik's "server side apt" do now display version numbers. So if you <a href="http://klik.atekon.de/trove.php?id=18">browse the klik recipe repository</a>, you'll now see how much net load you'll get in a minute  <b>:-)</b> </p>

<p>Another one is that he's now working on is showing automatically created dependency graphs for each recipe. Our server-side apt does automatically resolve dependencies anyway, and this kind of graphviz-created illustrations will be available to users and klik recipe developers to see which other packages and libraries an application does draw in and why.</p>

<p>However, previous attempts to do just that did "dig too deep": using the repository's pot file with the --dotty option always painted the <i>*entire*</i> dependencies of a package; very often we got enormously large trees of entire Debian criss-cross repository interdependencies. Which was of course useless for the purpose intended on the klik pages...</p>

<p>At [image:2590 align="right" hspace=3 vspace=6 border=3 width=150] the recent <A href="http://www.freestandards.org/en/LSB_face-to-face_%28December_2006%29">LSB Packaging face to face meeting</A> in Berlin, Michael Vogt, the APT maintainer had the right ideas how to limit the painting of the dependency tree to only the ones that indeed get included into the klik bundle.</p>

<p>Contrary to some urban myths, klik does <i>*not*</i> include <i>*all*</i> dependencies into each package. It only includes those dependencies, which are not satisfied by libraries expected and assumed to be present in the klik client's base system.</p>

<p>So now you want to know the exact list of packages klik expects to be present on a klik-enabled system? OK, look at these lists: <a href="http://klik.atekon.de/apt/status/multi">@multi list</a> and <a href="http://klik.atekon.de/apt/status/lsb">@lsb list</a>. 
<ul>
  <li>The @lsb list matches the current Linux Standard Base 3.1 (and is only used right now if you add it to the klik handler parameter as a suffix: <a href="klik://gaim@lsb">klik://gaim@lsb</a>).<br /> &nbsp;</li>
  <li>@multi is longer (distros tend to install more than what is defined in LSB); it is currently used for most distros (unless we auto-discover a client's distro which requires a special tweak, and we know about that tweak). It also represents an attempt for a good compromise that "averages" between various major distros (It's an estimation by klik developers based on some experience and user feedback).</li>
</ul>
<p>(<small><small>BTW, "99999" in these lists means "most recent", and "xxxxxx" means "not really required".</small></small>)</p>

<p>Using @multi (instead of @distroversion123) means that the resulting .cmgs will work and be relocateable between distros and  their versions: because if we are in doubt, a dependent package will be included to make it work f.e. on SUSE 9.2, even if it would work on Debian Sid without that inclusion. It means that the .cmg may become a bit bigger (but you know already, that klik saves 65% of harddisk space anyway by means of its cramfs compression, right?). </p>

<p>Can you see how much bigger the .cmgs become if we do strictly only assume LSB, and include into the .cmg everything beyond that definition? </p>

<p>These new graphs will get enabled in the near future for all users and all auto-generated recipes. Right now, you could look at them if you knew the address :-)&nbsp; &nbsp;&nbsp;&emdash;&nbsp; But it is still too "expensive" for server CPU, since they are re-generated every time you access the URL; probono wants to add caching of the graphic, and have it generated only on the first visitor's access, and then store it on harddisk. </p>

<!--break-->