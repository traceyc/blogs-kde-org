---
title:   "How Well Do NX And FreeNX Work For You?"
date:    2005-09-03
authors:
  - pipitas
slug:    how-well-do-nx-and-freenx-work-you
---
<p>It was only 10 minutes after my last blog entry appeared that one reader phoned me and objected: he thinks that the 280 msec latency he experiences from his currently Malaga/Spain-based notebook to his guest account on my NX server in Karlsruhe/Germany would be too much, and a data flow rate of 27 kBits/sec too sparse (he used a crappy and fairly saturated WLAN link to test this) to make users feel comfortable in using NX permanently, day-in and day-out over the network. He agreed NX was still performing very well, given the conditions he had to cope with, and that he could still be nearly as productive as he was used to from using his local machine.</p>

<p>OK -- I concede so much: I do not envisage many German government officials to mostly work from a holiday resort 2900 km apart from home. I think it is more likely for them to experience latencies of much less than 50 msec, and bandwidth of much more than 100 kBits/sec when they are in their offices.</p>

<p>I'd like to ask the readers: 

<ul>
<li>What are the figures <em>you</em> experience in real life? </li>
<li>How does <em>your</em> company's network perform? </li>
<li>What is <strong>your</strong> testimony about (Free)NX in your personal use case?</li>
</ul>
<!--break-->