---
title:   "FreeNX news from the development hotbed"
date:    2004-09-17
authors:
  - pipitas
slug:    freenx-news-development-hotbed
---
<br>
In the last two weeks Fabian has made huge progress with FreeNX:
<ul>
<li>he designed and implemented a new security model for FreeNX (with the help of some outstanding people who are now regularly joining debates in the #nx IRC channel on freenode). It doesn't use the <i>nxssh</i> binary any more (which made some Linux distro security auditors to be very suspicious), but instead uses the most recent "standard" OpenSSH package. So any newly discovered future SSH vulnerability doesnt need to be an "extra" NX concern -- fix SSH and you also fixed NX. We hope to have made the security experts (notably those from <a href="http://www.suse.de/">SUSE</a>) happy with this and that the upcoming audits of FreeNX will soon be passed without need for much re-design.

<li>he made FreeNX behave well with various types of clients (KDE's initial <i>knx</i> client for NX and FreeNX sessions, as available from KDE-CVS in the <I>kdenonbeta module</i> -- <a href="http://quality.kde.org/develop/cvsguide/buildstep.php">compile instructions are here</a>; the <a href="http://www.nomachine.com/">NoMachine</A> commercial -- but <i>free-as-in-beer</i> -- NX clients of the 1.3.x as well as the various 1.4-snapshot releases). For the NoMachine 1.3.x NX Client he even succeeded to hack an "auto-resume" feature into the FreeNX server: the user restores any suspended session automatically upon re-connection (the 1.3.x clients dont normally support that feature, they always create a new session).

<li>he made FreeNX use server-specific SSH key authentication (for the special "nx" user who initiates each connection) as well as the general NoMachine key authentication (for the "nx" user), as well as supporting a passwordless, key-based SSH connection without the need for the "nx" user), as well as PAM-based authentication schemes.
</ul>
But the coolest thing last....
<!--break-->
It was inspired by a Slashdot posting (yeah, sometimes you even find gems
there!) from someone who wished he could use an <I>"ssh -NX"</I> instead of an <I>"ssh -X"</I>
commandline. This is not exactly here yet. But what you *can* use now is one of these:
<pre>
  nxtunnel username@remote.NX.host bash
  nxtunnel username@remote.NX.host xterm
</pre>
This will start a Bash shell or an xterm (just as if you used <I>"ssh -X username@remote.NX.host xterm"</I>. But here the session is tunneled through an NX link, going through SSH. If you now type into the xterm or behind the new bash shell prompt something like "konqueror" or "kmail", you'll start these applications on the remote end (and display them locally), speed-boosted by NX compression (which is better and less CPU-intensive than generic ZLIB compression) and by NX caching (which is unprecedented in its efficiency and rate of cache hits). This is already considerably faster than "ssh -X -C" sessions. The third NX component, the X roundtrip suppression, which makes things really fly, is also enabled in nxtunnel sessions...

<br><br>
[<b>CAVEAT!</b>
<br>
Be aware that the mentioned third element of NX's superior methods to speed up remote X GUI
sessions, its round-trip suppression scheme, does not yet work very reliably for the case of single application windows. The reason is, that the "nxagent" part of the NoMachine software currently only has experimental support for "rootless windows". Single applications therefore normally bypass the "nxagent" part of the NX machinerie and still suffer from the roundtrips. (If you are interested in a flowchart of the NX architecture, <a href="http://www.pl-berichte.de/berichte/jpgs/lt2004-nxartikel/6r-nxflow-allproto-nxenabled.png">look at this one</a> While benefitting from NX compression and caching, a roundtrip-bogged NX session is still faster than a plain "ssh -X -C" session, but it is far from being regarded as as a ground-breaking thing. -- The nxtunnel script enables the <b>rootless window mode <i>with</i> roundtrip suppression</b>, but it has still some bugs. The most important one being, that if you <b>close</b> just one window (like any child dialog) using the window manager "close" button, it'll close the whole nxagent connection. But this is being worked on...  To partially avoid this, exit or kill the application from the bash or xterm window you started it from.]

<br><br>
nxtunnel can of course also start a complete KDE session:
<pre>
  nxtunnel username@remote.NX.host -r startkde
</pre>
The <i>"-r"</i> parameter tells the skript to set up a rooted window for a window manager. 

<br><br>
nxtunnel was <a href="https://mail.kde.org/pipermail/freenx-knx/2004-September/000046.html">announced</a> yesterday on the recently created <a href="https://mail.kde.org/mailman/listinfo/freenx-knx">FreeNX-kNX mailing list</a>. nxtunnel is a Bash shell script and also available from the current <a
href="http://debian.tu-bs.de/knoppix/nx/">FreeNX software repository</a>.

<br><br>
nxtunnel development started from my own humble beginnings from a few months ago (which was based on the material I found in the NoMachine <a href="http://www.nomachine.com/download/nxsources/nxscripts/nxscripts-1.3.2-2.tar.gz">nxscripts.tar.gz package</a>), was improved by <a href"http://www.kalyxo.org/">Peter "mornfall" Rockai</a>, picked up there and rewritten by <a href="http://web.walfield.org/">Neal H. Walfield</a>, saw some heavy hacking from Fabian last, and may become a permanent part of the FreeNX package soon....

<br><br>
We hope that we can move the complete FreeNX and nxtunnel stuff rather soonish to a decent SVN
repository on <a href="http://www.freedesktop.org/">Freedesktop.org</a> to make it easier to
participate (or just check out the software and use it). I hope that there will be no silly excuses heard any more for more delays to occur. Or do we need to look for another hosting place?

<hr>

<b>P.S.:</b>
<br>
And before I again get my mailbox filled with even more FAQs: 
<li>Yes, you *need* to install the NoMachine NX/GPL packages in order to make FreeNX, kNX or nxtunnel work. 
<li>No, can't forgo without their NoMachine foundational work if you want to use FreeNX or nxtunnel. 
<li>Yes, FreeNX and nxtunnel also have some additional software package dependencies: on OpenSSH, on expect, on netcat and on netpipes.
<li>No, you don't get commercial support contracts for FreeNX from us. 
<li>Yes, for quotes about commercial NX support contracts you have to please contact <a href="http://www.nomachine.com/support.php">NoMachine</a>.
<li>No, we don't intend to offer paid support for FreeNX any time soon. 
<li>Yes, there have been pre-decessors to NX (such as <i>DXCP</i> and <i>mlview</i>), or other attempts to make remote X faster (such as <i>LBX</i>). 
<li>No, these other attempts have never ever gone so far and never became so mature, stable and efficient as NoMachine's NX has been developed to. 
<li>Yes, NX and FreeNX can also proxy connections to Windows RDP servers or  '((any-platform-you-like)'-VNC servers. 
<li>No, printing and sound is not yet implemented in FreeNX (but it may be next week). 
<li>Yes, FreeNX strifes to stay compatible with commercial NoMachine NX.
<li>No, there is not yet a comprehensive FreeNX documentation available. 
<li>Yes, NoMachine have put all of their core technology developed in-house under the terms of the GPL. 
<li>No, NX and FreeNX are not limited to low bandwidth links.
<li>Yes, NX and FreeNX work perfectly well also across fast and high bandwidth connections.
<li>No, NX and FreeNX don't limit themselves to remote X sessions only.
<li>Yes, NX and FreeNX can also link you up to TightVNC (all platoforms) and RDP (Windows) servers.
<li>No, NX-1.4 is not yet released, it is still in development. As is FreeNX and nxtunnel. -- "snapshot" releases are available for all of them.
<li>Yes, we expect to have a stable release very soon, possibly even this month.