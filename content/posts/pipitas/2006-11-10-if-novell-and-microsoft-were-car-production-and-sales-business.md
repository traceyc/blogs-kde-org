---
title:   "If Novell and Microsoft were in the car production and sales business...."
date:    2006-11-10
authors:
  - pipitas
slug:    if-novell-and-microsoft-were-car-production-and-sales-business
---
Look at it this way for a moment:

<ul>
<li> If I, as an end-user, bought a car from Ford, that does indeed contain technology infringing some patent owned by DaimlerChrysler -- would there by any likelyhood that Ford would sue me, the end-user?</li>
<li> However, if I, as an end-user, buy a SUSE Linux Enterprise Server, that *may* contain (well, it doesn't, and never will, as Novell strongly reassures me) software patented technology owned by Microsoft -- would there be any likelyhood that Microsoft would sue me, the end-user?</li>
</ul>

Let's do a little thought experiment.

Assume, somehow 90% of the world wide car sales are DaimlerChrysler, 3% are Ford, and 7% are RedCar. Now,  in recent years an increasing number of households are looking at Ford and RedCar, pondering to change their preferred car brand away from DaimlerChrysler (or to buy at least one of their household's five cars from Ford or RedCar).

However, prospective Ford customers tell their sales reps that they are full of fear that DaimlerChrysler would sue *them* for patent violations that may have occurred when Ford designed and built their cars.

Ford turns round, negotiates with DaimlerChrysler, and indeed gets them to sign a "patent pledge", valid for 5 years, to not sue any Ford end-user customer if they buy from Ford (or do already own a Ford), so that Ford customers and prospectives do enjoy "a peace of mind" if they indeed get illoyal to DaimlerChrysler.

Furthermore, DaimlerChrysler even commits itself to buy 75.000 cars from Ford every year, so that they could offer these to any of their own customers who wants to opt for a mixed-brand family car pool. (They also start a joint car component development lab.... you get the idea).

DaimlerChrysler pays 300+ million $US as a first lump sum to Ford. Both companies commit themselves to not sue their counterpart's customers for patent violations, and both agree that they pay each other an "insurance fee" to cover the cost of that pledge ("No, we do not crosslicense any specific patents -- we do cross-insurance each other's end-users, just in case one of us does violate the other's patents") -- but DaimlerChrysler has to pay more to Ford (since DaimlerChrysler sales are higher? therefore their "anti-patent lawsuite insurance fee" is higher?) than the other way round....

Isn't that whole purported setup of the deal a very strange construction? Especially if it is meant to justify a net flow of money from Microsoft to Novell?

Something smells funny here. I just am unable to take the story as it is told by the acting personae at its face value, sorry. There must be a different reason for the money flow, and what they tell us is just a smokescreen to hide the real reason. Whatever it is, for now we can only speculate. 

<ul>
<li> Does Novell have some secret ace to threaten Microsoft with? (Novell is one of the few companies that repeatedly succeeded to get big $$$ out of Microsoft by means of lawsuits.)</li>
<li> Does, for example, Novell have proof of Microsoft to have violated an Novell patent? </li>
<li> Does Novell Mono violate some MS .NET-related patents, but Microsoft wants to see Mono alive (at least for a certain time) to proof to the world how viable C#/.NET are as a platform-independent development framework? (Giving money would ensure a degree of control that could stir Mono into any desired direction).</li>
<li> Or is it just that Microsoft needs to create a good and more pleasant image of themselves, so that they do not get too hard a punch from the EU Commission's investigations into their anti-competitive business practices? </li>
<li> Is it that Microsoft simply can not afford to be seen as a 100% monopolist (bad for PR, makes it vulnerable for anti-monopole lawsuits), so it is better to have a limited "controlled competition" in place? (They did the same with Apple in the past, and they obviously couldn't [and didn't want to] do it with RedHat). With Bush [whose first election let the DoJ lawsuite against MS go poof] now being turned into a "lame duck" president, that idea is no longer too crazy either...</li>
</ul>

We don't know (yet). But do not take everything at face value what the participants in this story do tell us.

I do understand and respect that Novell employees are concerned about their job security, their salaries, their families' well-being; they have every right to be loyal to their employer and defend and justify their management's actions. (Paid, fulltime Novell employees do of course also care about the community, the GPL and the future of FOSS, no doubt -- but they are also, in addition, to a certain degree bound by their employment loyalities). 

But please do also understand, when people who can be concerned *only* about the community (because they do not work fulltime for a Linux vendor) and about its future raise questions and discuss their thoughts about the story....


