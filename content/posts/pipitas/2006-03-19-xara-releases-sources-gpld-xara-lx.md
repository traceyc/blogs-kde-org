---
title:   "Xara releases sources for GPL'd Xara LX"
date:    2006-03-19
authors:
  - pipitas
slug:    xara-releases-sources-gpld-xara-lx
---
<p>The vector graphics package <a href="http://www.xara.com/"><i>Xara Xtreme</i></a> so far was only available for Windows. Back in October, the Xara company <A href="http://www.xaraxtrem.org/news/11-10-05.html">announced</A> the porting of its flagship product to Linux and Mac OS X. Not only that -- the complete source code should become available, and subject to the GPL license. But at the time they consoled hopes for an immediate release to a later date.</p>

<p>Now the day has come.</p>

<p>Right on time for the opening of the <a href="http://www.libregraphicsmeeting.org/"><i>Libre Graphics Meeting</i></a> in Lyon, France, Xara has published a <a href="http://www.xaraxtreme.org/developers/sourcecode.html">website</a> with details how to access the sources. (It even looks like they'll grant <b>write</b> access (!) to their subversion repository for some external developers.</p>

<p>Our <a href="http://klik.atekon.de/">klik webservice</a> offers recipes for <a href="klik://xara">klik://xara</a> <i>(build rev. 676)</i> and <a href="klik://xara-latest">klik://xara-latest</a> <i>(build rev. 689)</i> to those...</p>
<ul> 
 <li> ...who do not want to compile Xara LX on their own, or...</li>
 <li> ...who do not want to install Xara LX binaries into the heart of their system, or...</li>
 <li> ...who run a distro where no Xara packages are available.</li>
</ul>
<p>(As most of you know, klik does not at all "install" in the traditional sense -- klik builds self-contained *.cmg bundles, which run in user space, even directly from a CD-RW medium; if a klik bundle does not work, or if you are fed up with it, just delete the one .cmg file and your system is back to like it was before).</p>

<p>The current 0.3 beta series for the first time allows creation of new *.xar files. (Previous versions only supported opening and playing with the bundled demo files). To get a first impression about Xara [and compare it to <a href="klik://inkscape-latest">klik://inkscape-latest</a> or to <a href="klik://karbon-1.5-beta">klik://karbon-1.5-beta</a> from the KOffice-1.5 beta1 &nbsp; :-) &nbsp; &nbsp; ],&nbsp; just open the included demo files. These are to be found at <i>/tmp/app/[number]/usr/bin/Design/</i> (while xara runs via klik).</p>

<!--break-->