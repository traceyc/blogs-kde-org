---
title:   "\"Steal Back Your Vote\" (Reading Recommendation #2)"
date:    2006-11-07
authors:
  - pipitas
slug:    steal-back-your-vote-reading-recommendation-2
---
<dl>
<dt><p>Today's reading recommendation #2. Extracts:</p></dt>

   <dd><p><br><a href="http://www.gregpalast.com/steal-back-your-vote"><b>Steal Back Your Vote</b></a><br><small>Published by Greg Palast November 6th, 2006 in his blog</small><br><br>A lot of advice we're getting from our progressive friends is to take photos of your ballot and silly stuff like that.  Well, that's all about how to complain after they steal it. I have a better idea:  Win, don't whine.<br><br>The regime's sneak attack via vote suppression [see, <a href="http://www.gregpalast.com/how-they-stole-the-mid-term-election">"How They Stole the Mid-Term Election"</a>] will only net them about 4.5 million votes.  You should be able to beat that blindfolded.   As that will cost about 5% of the vote.  That means you can't win with 51% of the vote anymore.  So just get over it.  If you can't get the 55% you need for regime change, then you're just a bunch of crybaby pussycats who don't deserve to take charge.<br><br><i>#1:  Vote Early, Vote Often</i><br><br>Vote today — at early voting stations — so you can spend tomorrow bringing out others to vote.  Also, if you're challenged, you've got another day to bring in more ID or scream bloody murder to your county elections board about your missing registration.<br><br><i>#2:  Gang Vote</i><br><br>Arrive with five!   Never go bowling, make love or vote alone.  And volunteer at get-out-the-vote operations.  It's worth it just for the stale donuts, cold coffee and hot democracy.<br><br><i>#3: Tell Them to Take Their Provisional Ballot and...</i><br><br>If they try to hand you a "provisional ballot," scream bloody murder.  If there's a problem with your ID or registration, demand adjudication from a poll monitor, come back with proper ID, or demand appeal to the county supervisor of elections.<br><br>But don't just walk away.  If it's provisional or nothing, take it — then return for the count to defend it.<br>[....]<br>   &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; (<a href="http://www.gregpalast.com/steal-back-your-vote"><b> ----> more</b></a>)</p></dd>
</dl>   

<!--break-->