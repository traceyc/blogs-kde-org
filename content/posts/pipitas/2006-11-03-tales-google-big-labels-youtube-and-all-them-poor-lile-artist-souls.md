---
title:   "Tales from Google, The Big Labels, YouTube and all them poor, li''le artist souls...."
date:    2006-11-03
authors:
  - pipitas
slug:    tales-google-big-labels-youtube-and-all-them-poor-lile-artist-souls
---
<p>Some <a href="http://www.blogmaverick.com/2006/10/30/some-intimate-details-on-the-google-youtube-deal/">interesting details from the Google/YouTube deal</a>. I had already suspected some maneuverings along these lines, but preferred to not say anything. Now that others have gone public, lemme chime in.....</p>

<p>Once more: it is entirely unclear if this "story" has any substance -- but, hey, doesn't it at least read very nice? Doesn't it soooo look and feel like real life?  :-)   </p>

<p>In short: Remember, <a href="http://www.google.com/">Big G</a> bought <a href="http://www.youtube.com/">YouTube</a> for a breathtaking price tag of 1,650 million $US. But YouTube has had no serious income whatsoever, no business model, no longterm financial future on its own... What it possessed instead, other than plenty of brand recognition: dizzying download figures (and bandwidth traffic bills) -- and lots of potential Copyright infringement lawsuits dangling over its head... </p>

<p>Google, as part of the YT package deal, entered agreements with major labels and media companies re. Copyright breaches by YouTube.(Google guided YouTube's hands in negotiations with media companies for some time before it openly appeared on the stage.) And now the "tit for tat" goes like this: </p>

<ul><li> Them Few Big Labels get compensation and some non-neglectigable $$$ from Google. But they'll make it look like "investment profit", not "licensing income". Clever, ya know? Because "licensing income" for the labels... they'd have to share it with their artists. And we all know how These Fine Labels like the general idea of sharing, no? </li>

<li> The return service of Major Labels for Google? No libels, no court suits, no legal actions against YouTube for half a year. They'll look into the other direction for 6 months while Google will continue to offer pirated copies like mad. Moreover, the Labels will sue into oblivion all (smaller) YouTube competitors instead in that period, so Google's growth can go on sky rocketing for now...</li></ul>

<p>Of course, The Big Labels had a timely foreknowledge that someone would buyout YouTube:</p>

<ul><li>  Previously, YouTube had tried to buy their peace with offerings of "straight revenue share" schemes to the media labels. These got easily dismissed as lame, because there <b>was</b> nothing to share, and YouTube statements remained openly fuzzy -- a poor cover for their cluelessness about how they'd generate future revenue. </li>

<li> More recently, YouTube abondoned all "revenue participation" offerings. Instead, they suddenly waved with checkbooks and deals laden with hard cash. Of course, at this point the Big Label Guys smelled there was a sugar daddy around (even if still incognito), willing to buy the young, ugly YouTube bride (though they may not have thought of Google, and that it encompassed a $US 1.65 billion mega-deal...). Better strike it now, before that stupid Old Fart looses interest, no? After all, a windfall of 50 extra million in cash for their 3rd quarter of financial year <b>does</b> look good in the books, yes?</li></ul>

<p>Now, cynics like us always knew to look beyond The Big Labels' flubdub and claptrap regarding "justice" and "compensation" for these poor artists they pretended to defend and protect.... hogwash! But <b>these</b> background bargainings (is there <i>any</i> reason to doubt this rumor???) behind the YouTube transfer makes my nose tip go white in shame for witnessing such a breathtaking impudence in action that bends over all label-0wned artists + skroo-ed 'em in This Big Deal...</p>

<!--break-->

