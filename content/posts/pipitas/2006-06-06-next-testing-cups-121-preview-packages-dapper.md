---
title:   "NEXT: Testing CUPS 1.2.1 Preview Packages for Dapper"
date:    2006-06-06
authors:
  - pipitas
slug:    next-testing-cups-121-preview-packages-dapper
---
<p>Since I also got pinged on IRC yesterday by one of the Ubuntu print software packagers; I'll continue tomorrow with investigating a newer package version. He told me he had built new CUPS 1.2.1 packages for testing, and I agreed to look closely at them and report back. I'll download those and continue my testing (and ranting) based on <a href="http://www.grad.hr/~ivoks/ubuntu/cups/">these packages</a>.</p>

<p>Finally, let me wrap up for today: I like the rest of what little I've seen from Kubuntu very much. Good job! I installed and played a bit with <A href="http://www.kde-apps.org/content/pre2/36832-2.png">Kerry</A> (KDE frontend to Beagle search), <A href="http://web.mornfall.net/adept_installer.html">Adept</A> (KDE frontend to apt-get/dpkg software package management and installation) and <A href="http://wiki.thekatapult.org.uk/Basic_Usage">Katapult</A> (Quickstarter for selected applications, documents and bookmarks). All of them are very nice. Adept however realls shines (though its sorting is a bit slow -- but <a href="http://web.mornfall.net/blog/profiling_adept_filters.html">mornfall seems already out</a> fixing this.)</p>

<!--break-->