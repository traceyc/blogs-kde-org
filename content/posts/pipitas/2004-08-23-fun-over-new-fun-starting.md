---
title:   "Fun over -- new fun starting..."
date:    2004-08-23
authors:
  - pipitas
slug:    fun-over-new-fun-starting
---
<p align="justify">Unfortunately I couldnt attend any of the talks at <A href="http://conference2004.kde.org/sched-devconf.php">DevConf</A> other than the fine <A href="http://conference2004.kde.org/cfp-devconf/eirik.chamb-eng-keynote.php">opening keynote</A> by Eirik Chamb-Eng (<A href="http://www.trolltech.com/">Trolltech</A>). Too much work to do, too much of a cold having occupied the deep end of my throat. The cold makes my voice sound like I could have a lot of guys drifting towards me -- if I were a female. It also makes me sweat the hell out of my pores at night, preventing me from sleep most of the time, and getting me to arrive late at aKademy. The work was mainly related to prepare the <A href="http://conference2004.kde.org/tutorials.php">tutorial rooms</A>, get the HP notebooks in shape, and hand them out to developers in need. A lot of the work around the notebook installation was done by our local helper team, consisting mostly of the local CCC -- thanks and kudos to them! The notebooks are a loan from HP Germany for the duration of aKademy. Thanks HP -- it is great to see your support for this KDE conference having grown so immensely in the last 2 weeks.</p>

<p align="justify">Lots of work -- but still it was a fun weekend. I saw a lot of <A href="http://ktown.kde.org/~binner/aKademy2004/day3/">happy and thrilled KDE developers</A>. I assume, there are 2 main factors contributing to this: 
<ul>
<li><A href="http://developer.kde.org/~binner/aKademy2004/day1/dscf0039.jpg">Free lunch</A></li> 
<li>We are having the biggest KDE event ever.</li>
</ul></p> 

<p align="justify">Speaking of "the biggest meeting ever": Still late registrations are coming in. Today I saw registration ID 647 submitted. (Actually, that number is somewhat exaggerbated -- we have about 150 - 200 "bogus" registrations, resulting from setting up the registration form and database and testing it.)</p>

<p align="justify">Tomorrow starts the <A href="http://conference2002.kde.org/marathon.php">CodingMarathon</A> week. This will see a continuation of the <A href="http://accessibility.kde.org/forum/">Unix Accessibility Forum</A>, plus the many groups of developers starting to hack on their various projects. One of these is <A href="http://conference2004.kde.org/cfp-devconf/kurt.pfeifle-nxaccelerateskdedev.php">FreeNX</A> -- more about that later.</p>

<p align="justify">I am looking forward to this week.</p>
