---
title:   "New Migration Opportunities For KDE Offered by Fast Single Application Mode of NX/FreeNX"
date:    2005-09-03
authors:
  - pipitas
slug:    new-migration-opportunities-kde-offered-fast-single-application-mode-nxfreenx
---
<p>Some of you know already: since NoMachine <A href="http://www.nomachine.com/news_read.php?idnews=159">released</A> the 1.5.0 of their GPL'd Core NX Libraries, KDE applications run blazingly fast over remote internet links in "single window mode". Even modem connections work great. Previous versions hadn't yet built in X roundtrip suppression for single application windows, and you could enjoy the full speed of applications only when running them inside a complete KDE desktop environment.</p>

<p>Now think about a typical <A href="http://www.muenchen.de/vip8/prod1/mde/_de/rubriken/Rathaus/40_dir/limux/publikationen/20050622_LinuxTag2005_Behoerdenkongress_lu_en.pdf">"from-Windows-to-OpenSource"</Ahref="http://www.muenchen.de/vip8/prod1/mde/_de/rubriken/Rathaus/40_dir/limux/publikationen/20050622_LinuxTag2005_Behoerdenkongress_lu_en.pdf"> migration scenario</A>. We all know that in most cases this can not be achieved in one single big jump. Companies, local governments and organizations are only too often dependent on some specialized application that is not yet ported to the Linux platform. This frequently manacles the complete IT environment to the old-fashioned, virus-ridden and worm-infected OS prison. IT managers are having a hard time to find small loopholes that provide a way out.</p>

<p>NX's (and therefor FreeNX's) new fast single application mode now cuts major breaches into the walls that form the prison keeping large user communities entangled to the proprietory software camp. Has anyone else noticed this already?</p>

<p>Take as one example <A href="http://kontact.kde.org/">Kontact</A>, the KDE suite of <A href="http://en.wikipedia.org/wiki/Kontact">personal information management</A> programs. <A href="http://kmail.kde.org/">KMail</A> is its most prominent component program. Others are <A href="http://pim.kde.org/components/kaddressbook.php">KAddressbook</A>, and <A href="http://korganizer.kde.org/">KOrganizer</A>. Each of these applications can still work in a standalone mode at any time -- but only if they are embedded into the Kontact groupware shell in a seamlessly integrating manner they are able to challenge proprietary Outlook's dominance on the market.</p>

<p>And hey, Kontact has some major advantages to offer: not only is it in the same league of <A href="http://www.openusability.org/projects/kdepim">usability</A> and <A href="http://kontact.kde.org/components.php#summary">feature completeness</A> as its proprietary components. It also is far from being constantly threatened by virus infections, worm invasions and annoying advertisement popups.</p>

<p>But still, a "classical" approach to make a user move over to an open source mail and PIM client would require to first migrate him to a complete Linux desktop environment before he could start using a secure mail client. (Another approach is to port Kontact to directly run on the Win32 platform which also seems <A href="http://conference2005.kde.org/cfp/develconf/jaroslaw.staniek-kdelibs.for.win32.php">well on its way</A>).</p>

<p>NX and FreeNX now can offer an innnovative way to switch over hundreds of thousands of clerks and civil servants in local, regional and national governments to a Linux and KDE mail program, while they can continue to use their legacy Windows workstations for the time being: just leave their Outlook icon sitting on their desktops -- but replace the program it starts by a remote "Kontact" or "KMail" single application session. Instances of Kontact or KMail would run on one central server, displaying their user interface and dialogs to each user's respective screen over a simple TCP/IP connection.</p>

<p>This approach offers some tremendous cost as well as organizational benefits:
<ul>
<li>All workers of a certain department or company or administrative body could run their mail clients on one central server. </li>
<li>All users' mails and file attachments could be scanned for malware at one place. </li>
<li>All mail backup measures could be concentrated at one place.</li>
<li>A standby, mirrored KMail application server could provide more reliability of service where there is the need for this. </li>
<li>The training for the users to use KMail instead of Outlook should be minimal. </li>
<li>The work load for the system administrators looking after the wronggoings, organizing software updates and maintaining virus protection for hundreds or thousands of workstations is considerably reduced.</li>
</ul>
</p>

<p>German government money has been used in the past to <A href="http://www.gnupg.org/aegypten2/">bring industry standard encryption</A> to KMail and a few other mail programs. (Google for the "Projekt Aegypten" if you are interested in more details). But how many users can enjoy the safety of communication at this time? How many have actually been migrated to a Linux and Open Source desktop so far? Is the figure already in the 5 digit range?</p>

<p>Couldn't a NX- or FreeNX-based mail client setup running in single application mode transfer a 7 digit number of users onto a safe and secure communication platform within a matter of months?</p>
<!--break-->

