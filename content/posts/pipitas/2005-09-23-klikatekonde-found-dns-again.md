---
title:   "klik.atekon.de is found by DNS again"
date:    2005-09-23
authors:
  - pipitas
slug:    klikatekonde-found-dns-again
---
<p>Huzzah! (would friend <A href="http://aseigo.blogspot.com/">Aaron</A> say),</p>

<p>klik.atekon.de is again found by DNS. </p>

<code>
  kliktestbox:~> nslookup klik.atekon.de 
  Server:         195.20.224.234 
  Address:        195.20.224.234#53 
  
  Non-authoritative answer: 
  Name:   klik.atekon.de 
  Address: 134.169.172.48 
</code>

<p>That's since about 30 minutes ago. Hopefully it stays like that. After all, there wasn't any problem since the server went online, more than a year ago.</p>

<p>It is really amazing to watch the <A href="http://134.169.172.48/comments.php">feedback</A> coming in again, almost by the minute. I haven't mentioned that feedback system before, did I? It is nice to see one's own feedback (that you are asked for if you use f.e. <A href="klik://scribus-latest">"klik://scribus-latest"</A> to make the latest Scribus klik-run on your system). It is also nice for the klik developers, to get an overview of which packages worked on which OS versions. (We know not *all* klik recipes work everywhere -- but most can be fixed easily, if we have knowledge what exactly breaks, and on which systems. But can you imagine that looking after 4.000+ packages is too much for 4 maintainers? So, if you want to volunteer, to help fixing klik packages that work on only some systems --> you can find us in Freenode IRC, channel #klik).</p>
<!--break-->