---
title:   "Hell Freezing Over?"
date:    2006-11-05
authors:
  - pipitas
slug:    hell-freezing-over
---
<p>Yes, on first look it may appear so.</p>

<p>No, not because Microsoft now pays lots of $$$ to Novell for 70,000 SLES support vouchers each year (making it the single biggest distributor of SUSE products). 
But because the <a href="http://en.wikipedia.org/wiki/Neocons">NeoCon</a> cabal seems to <a href="http://www.vanityfair.com/politics/features/2006/12/neocons200612?printable=true&currentPage=all">leave the sinking ship</a> of the Bush Administration.</p>

<p>And don't you love these really mean and sinister looking faces on <a href="http://www.vanityfair.com/politics/features/2006/12/neocons200612?printable=true&currentPage=all">those photos</a>? (No idea if the pics have been photoshopped -- nowadays you just don't trust any publication's pristine-ness, even if you like its content.)</p>

<p><i>"I spend the better part of two weeks in conversations with some of the most respected voices among the neoconservative elite. What I discover is that none of them is optimistic. All of them have regrets, not only about what has happened but also, in many cases, about the roles they played."</i></p>

<p>Of course, former <a href="http://en.wikipedia.org/wiki/Project_for_the_New_American_Century">PNAC</a> masterminds like <a href="http://en.wikipedia.org/wiki/Francis_Fukuyama">Francis Fukuyama</a> and <a href="http://en.wikipedia.org/wiki/William_Kristol">William Kristol</a> had enough brains to understand the imminent perdition and were the first ones to flee for their lifes (the latter even chose The Daily Show to announce his defection). Guys like <a href="http://en.wikipedia.org/wiki/Richard_Perle">Richard Perle</a>, <a href="http://en.wikipedia.org/wiki/Kenneth_Adelman">Kenneth Adelman</a>, <a href="http://en.wikipedia.org/wiki/Frank_Gaffney">Frank Gaffney</a>, <a href="http://en.wikipedia.org/wiki/Michael_Rubin">Michael Rubin</a>, <a href="http://en.wikipedia.org/wiki/David_Frum">David Frum, <a href="http://en.wikipedia.org/wiki/Michael_Ledeen">Michael Ledeen</a> and <a href="http://en.wikipedia.org/wiki/Eliot_Cohen">Eliot Cohen</a> are following suit. While the "thinkers" are gone, left over now remain their hardcore "performers" who entertain a bit less of an own "intellectual" brain (<a href="http://en.wikipedia.org/wiki/Dick_Cheney">Dick Cheney</a>, <a href="http://en.wikipedia.org/wiki/Donald_Rumsfeld">Donald Rumsfeld</a>... oh, and <a href="http://en.wikipedia.org/wiki/G.W._Bush">G.W. Bush</a> himself). And these now have at their disposal a set of newly passed laws and bills which combine themselves into something that is paramount to Hitler's "enabling act"...  preconditions that make their potential military actions more dangerous and less predictable to the rest of the world.</p>

<p>Remember, it's now 2 days before the mid-term U.S. elections. Unless there comes a staged "early November surprise" that gives them unfavorable headlines, the Democrats (looking like a slightly differently colorized version of Republicans to my European-politics trained eyes) are about to take a landslide victory. </p>

<p>Also, tomorrow 4 different newspapers geared to be consumed by the US military and their families will <a href="http://www.msnbc.msn.com/id/15552388/">publish an urge for Bush to kick Rumsfeld</a> out of office (R. is the defense minister). Remember, while the papers in question are formally speaking "independent", they can only be distributed around U.S. military bases of the world with the tacid support of the command. Hence, they usually don't cross the "party line", and are 100% loyal to the cause(s) of U.S. military. So this type of "bashing the top military leader of the nation"-editorial surely does need the silent nods of all top military commanders in Army, Navy and Airforce before it ever has a chance to get in print, let alone leak its content days in advance.</p>

<p>And all this exactly in the weeks when <a href="http://debka.com/article.php?aid=1223">the largest U.S. sea strike force</a> is gathering in front of Iran, sent there to prepare... what?</p>

<p>Hell's not yet freezing, but it surely changed temperature by a degree or two....</p>
