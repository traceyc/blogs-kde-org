---
title:   "A first summary of topics discussed at OSDL Desktop Linux Printing Summit"
date:    2006-04-15
authors:
  - pipitas
slug:    first-summary-topics-discussed-osdl-desktop-linux-printing-summit
---
<p align="justify">Exhausted, but happy about the work we've done I'm now back in Stuttgart. I have attended the 3 intensive days of discussions and work that were the <i>Desktop Linux Printing Summit</i>, jointly organized by <A href="http://www.OSDL.org/">OSDL</A> (John Cherry) and <A href="http://www.Linuxprinting.org/">Linuxprinting.org</A> (Till Kamppeter). It was held in Atlanta, hosted by <A href="http://www.Lanier.com/">Lanier</A> at their Education Center. The hosting was made possible by Uli Wehner. Uli is one of Lanier's senior support and testing engineers (responsible for Lanier's ever-growing business of non-Windows system printing); he is also quite active on the <A href="http://www.linuxprinting.org/forums.cgi">Linuxprinting.org user support forums</A>. </p>

<p align="justify">Altogether we had nearly 40 people there. They represented a broad range of backgrounds. See yourself:</p>

<ul>
<li><i>printer hardware vendors:</i> Lanier, Ricoh, Sharp, HP, IBM, Xerox, Lexmark, Epson</li>
<li><i>Linux distributions:</i> Novell, Debian, Mandriva</li>
<li><i>other operating system vendors:</i>Apple, IBM, Sun</li>
<li><i>desktop environment projects:</i> Gnome, KDE</li>
<li><i>printer driver development projects:</i> Gutenprint, HPLIP</li>
<li><i>application developers and Independent Software Vendors:</i> Mozilla Corporation, Easy Software Products, Scribus</li>
<li><i>printing consultants:</i> Tykodi Consulting, Danka</li>
<li><i>standards defining organisations:</i> Linux Standard Base, Printer Working Group, Free Standards/Open Printing Group</li>
<li><i>usability professionals:</i> OpenUsability.org, Relevantive AG</li>
<li><i>developers of core Linux printing software:</i> CUPS, Ghostscript</li>
<li><i>other organizations:</i> OSDL DTL, Beijing Software Testing &amp; QA Center</li>
</ul>

<p align="justify">For links to the respective websites, have a look at the <A href="http://groups.osdl.org/workgroups/dtl/desktop_architects/desktop_printing/">OSDL printing summit</A> website.
A huge amount of presentation materials created by participants is listed in the <A href="http://groups.osdl.org/workgroups/dtl/desktop_architects/desktop_printing/">preparation material</A> section.</p>

<p align="justify">Waldo already <A href="http://blogs.kde.org/node/1930">leaked</A> it: we had a very cooperative atmosphere, and a lot of common ground was found. We identified 7 major areas which need attention for the future development and efforts by everyone involved with making Linux printing better, some of which must be addressed from inside KDE (and from inside Gnome or other applications) by the respective developers:</p>

<dl>
<dt><b>Printer and Driver Installation:</b></dt>
     <dd><p align="justify">Setting up printing is still much too difficult, even for power users and administrators, let alone the home user who is no Unix expert. Very often there is no automatic device discovery, nor the correct display of available drivers. Users who have a working printer, and are familiar with the setup tool used by their distro, find themselves completely hanging in mid-air as soon as they are tasked to install the same model on a different distro using a different setup tool. Some distros don't even ship PPDs, thus rendering the CUPS web interface (which usually works flawlessly) completely useless for installation tasks. </p></dd>
<dt><b>Error Messages and General Feedback:</b></dt>
     <dd><p align="justify">Error messages frequently are very in-comprehensive (mostly using the standard, yet cryptic official IPP error codes); job progress feedback is not good enough or simply non-existent. Finding help is not obvious. Some distros have patched and completely crippled the CUPS web interface without documenting their changes at an easy to find place and telling users how to setup the modified system.</p></dd>
<dt><b>Usability Experience: </b></dt>
     <dd><p align="justify">Across different desktop environments and applications users experience too much inconsistency; each of KDE, Gnome, Mozilla/Firefox, OpenOffice.org, Scribus, Acroread and more use their own self-rolled print dialogs; what's worse even: none of these dialogs is the obvious best one from a usability point of view either.</p></dd>
<dt><b>Print Dialog Extensibility:</b></dt>
     <dd><p align="justify">Hardware vendors were asking for an easy (and standard) way to plug in their own extensions into the print dialogs provided by the desktops. This can be done on Windows and Mac OS X quite easily (on Windows, driver developers can add their own DLLs to the Microsoft provided ones, and even completely skip these). Linux printing lacks a mechanism like this. High end production printers from vendors like Xerox are not able to map their device options into the (limited) standard syntax of PPDs -- they use a different, XML-based method to make their driver GUIs display options to their users. </p></dd>
<dt><b>Printer Driver Development:</b></dt>
     <dd><p align="justify">As can be seen from looking at some of the efforts made by hardware vendors who provide Linux printer drivers for their boxes, the knowledge about a preferred and recommended architecture of printer drivers has not yet trickled down into their ranks. One can see very wild and non-standard ways to design printer drivers as well as installing these into the various Linux systems. </p></dd>
<dt><b>Print Job Data Transfer Format:</b></dt>
     <dd><p align="justify">PostScript is no longer seen as the core format for print jobs to be spooled and transferred from print client to print server to printer device, or from customer to professional print shop. However, currently Linux printing is still centered around this legacy format, and all non-PostScript devices are only supported by converting PS to the printer's native raster or PCL language. </p></dd>
<dt><b>Buying a Printer:</b></dt>
     <dd><p align="justify">When buying a printer, choice is difficult: many models do not work perfectly with Linux; OTOH, many work fine, but they don't say so on the box; some do carry a Tux logo on their wrapping, but very often the manual doesn't say how to install the driver; some come with proprietary drivers which do not blend in well with CUPS, or which do not install on all distributions. Searching <A href="http://www.linuxprinting.org/printer_list.cgi">Linuxprinting.org database</A> is a too complicated step for many to succeed in. </p></dd>
</dl>
<p align="justify">It will not be easy to fix each and every one of these; and it will not be done within a few months. But the meeting heard a few very good proposals for all these points, and we will be working on them in the future. Also, the upcoming CUPS 1.2 series (the 3rd release candidate for 1.2.0 will be out in the next couple of days, and the final release likely 2 weeks after that) has a few improvements on board which will greatly help with some of the problems listed above.</p>

<p align="justify">I wonder if we could try and transform one or more of the above problem fields into a tangible set of tasks, that we could announce as a <A href="http://code.google.com/summerofcode.html">Google Summer of Code</A> challenge, and this way find one or two new coders for Linux printing stuff?</p>

<p align="justify">Over the next few weeks I will write up a more detailed summary for each of the above identified points, including some of the proposed solutions that were discussed at the summit.</p>

<p align="justify">If you are interested to keep an uptodate view of the ongoing work or better: if you want to contribute in either one of the named problem fields, please join the <A href="https://lists.osdl.org/mailman/listinfo/desktop_printing">OSDL desktop printing mailing list</A>. Over the next few months a lot of the ideas for solutions we discussed in Atlanta will be thrashed out and refined there; also, the next summit which will take place in October is going to be prepared via the discussions taking place on this list.</p>

<hr>
<small><b>Amendment:</b> Initially I forgot to list IBM amongst the list of entities who had representatives at the summit; in fact, IBM is not only the vendor of AIX, but also a vendor of digital high end commercial and production printing equipment. Summit participant Claudia Alimpich did not only represent the FSG/Open Printing Group, but also her employer, IBM. Sorry for not having had that right in the first round, and my deep apologies to Claudia and IBM.</small>
<hr>
<!--break-->