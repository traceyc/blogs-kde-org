---
title:   "\"Don't install -- just klik'n'run!\" -- a proposal for the benefit of KDE app snapshot testing"
date:    2005-09-16
authors:
  - pipitas
slug:    dont-install-just-kliknrun-proposal-benefit-kde-app-snapshot-testing
---
<p>So my <A href="http://www.Dot.org/">Dot</A> article about <A href="http://klik.atekon.de/">klik</A> is <A href="http://dot.kde.org/1126867980/">now online</A>. It has had already 40 comments within 2 hours, mostly positive. It contains a concrete, workable proposal how to accelerate KDE development: accelerating it by bringing our non-technical/non-coding contributors more closely to the "bleeding edge" code. We can do this by handing out bleeding edge binaries of KDE application development snapshots to our non-techies...</p> 

<p>
<ul>
 <li>...so they can run them without a complicated installation procedure </li>
 <li>...so they do not de-stabilize their work environment with installing some unstable system components into /usr/lib/</li>
 <li>...so they can easily delete it again and revert the system to its previous state when they do not want it any more (or if there is a new update for the app)</li>
</ul>

<p>It can be made as easy as <i>"klik and run"</i> -- no, it <b><i>is</i></b> as easy as that already. -- (Hey, readers of the <A href="http://planetsuse.org/">Planet SUSE</A>: and it also contains some exciting news regarding some cool add-ons that can be brought to any <A href="http://www.opensuse.org/">SUSE 10.0 desktop</A> in the near future.)</p> 

<p>Why?</p>

<p>We all know that these indispensible groups (<A href="http://i18n.kde.org/">translators</A>, <A href="http://docs.kde.org/">documentation writers</A>, <A href="http://kde-artists.org/">artwork designers</A>, <A href="http://bugs.kde.org/">bug reporters</A>, <A href="http://www.openusability.org/">usability engineers</A>, <A href="http://promo.kde.org/">PR people</A>,...) do not typically compile their own working environment from sources. The (slightly simplified) picture is this: The non-coders often lag 6 or more months behind with what their desktop looks like, compared to what the coding part of our community has in front of them for their daily KDE-ing. This means f.e. that they report bugs against "old" released versions, packaged by their favorite distro -- while the coders are the only ones who see the applications running and can find bugs prior to releases. And the reports from bugzilla relating to released versions requires the developers to switch away from their bleeding edge environments only too often. If this takes too much time, it makes them more hesitant to do so -- and the bugfix is delayed.</p>

<p>Distribution of bleeding edge .cmg files can relieve that problem. If we had a "know good" repository of pre-build development releases of some key applications (updated nightly, weekly, monthly...whatever), this would bring a big boost to development. If translators could be provided with the actual interface of the new app/dialog they are currently working on instead of a boring string table view, this would probably bring an influx of new translators, and additional translation teams (KDE is already the project with by far the most translations -- 70, many more than Microsoft! -- but it does not yet cover the world completely). If our artists could work with applications while they are still in development, they may get a different inspirations as compared to just look at screenshots provided by the coder. If the usability engineers could start giving their input and suggestions right from the beginning of the development cycle (instead of after the first release), ...well, you get the idea.</p>

<ul>
<li>The distribution of a klik-able AppDir file (.cmg extension) is very easy: just copy it to your system.</li>
<li>The execution of a klik-able AppDir file is very easy: just run the helper script with the .cmg as parameter: ./.zAppDir my-test-app.cmg</li>
<li>The removal of a klik-able AppDir file is very easy: just delete it.</li>
<li>The creation if a klik-able AppDir file is very easy: probono is currently in IRC (#klik on <A href="http://www.Freenode.net/">Freenode.net</A>) stepping <A href="https://blogs.kde.org/blog/105">manyoso</A> through the process to create a <A href="http://extragear.kde.org/apps/datakiosk/">datakiosk</A>.cmg</li>
<li>The internal structure of a klik-able AppDir file is easily understood: it is made of a compressed cramfs or zisofs filesystem, that can be mounted in userspace and executed by a simple helper shell script (".zAppRun").</li>
<li>The pre-requisits for successfull execution of a klik-able AppDir are easy: it is assumed that *some* set and versions of base system libraries is present on the hosting system -- everything else (libs, dependencies) is contained in the compressed .cmg image.</li>
<li>The danger posed by different versions of an applications and its libraries for the stability of a given base system is non-existent: everything the .cmg brings to your system is self-contained in the .cmg, and goes with the deletion of the .cmg, reverting your PC to the previous state (granted, it may have written into your $HOME/.dot-files or similar, but that is minor, and you can easily take care of that).</li>
<li>The opportunity to utilize KDE's cool <a href="http://kstuff.org/hotstuff/">Get Hot New Stuff</a> technology to offer binary development snapshots of certain applications is a no-brainer: it takes just one small group of persons to do it. </li>
<li>The suggestion to include a new "make cmg" Makefile target is a great one: it would certainly simplify the creation of klik-ified applications even more.</li>
<li>The danger posed by wild criss-cross offerings of klik-able .cmg files around the internet opening up a way for "malicious-by-design" klik-ified programs is certainly there: this needs to be addressed. (But I do not think it is so much greater than for any other software/package/format a user does install from untrusted sources.)</li>
</ul>

<p>What do you think?</p>

<!--break-->