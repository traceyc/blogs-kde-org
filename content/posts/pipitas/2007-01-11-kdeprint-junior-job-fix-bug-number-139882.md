---
title:   "KDEPrint 'Junior Job': fix bug number 139882"
date:    2007-01-11
authors:
  - pipitas
slug:    kdeprint-junior-job-fix-bug-number-139882
---
<p>The last few days I ploughed through the <A href="http://bugs.kde.org/component-report.cgi?product=kdeprint">KDEPrint bugs</A>.</p>

<p>This should serve the same purpose as outlined in <A href="http://blogs.kde.org/node/2610">my last blog</A> entry: to make it more easy for the real coders to see the valid and important bugs (we're short of people who've enough time to work on KDEPrint code for KDE4). </p>

<p>Also, I wanted to get an overview about what were the main problems reported during the last year (I was unable to participate much in KDE, or even use it myself most of the time), so that these things stand a higher chance to get attention for KDE4.</p>

<p>Last, were there any wishlist items and suggestions for improvement that we had not yet noticed?</p>

<p>[image:2612 align="right" hspace=12 vspace=12 border=1 size=thumbnail] [image:2611 align="right" hspace=12 vspace=12 border=1 size=thumbnail] I found lots of useful tidbits (and closed quite some bug reports that were invalid because the problem was solved long ago without getting a bugzilla update).</p>

<p>I even created a <A href="http://bugs.kde.org/show_bug.cgi?id=139882">new bug report</A> of my own. It is annoying that the lower pane on the "Driver Settings" tab is fixed in height, and a very low height at that. Because it is not resize-able, it's difficult to get an overview about all supported media sizes, for example. See the two screenshots to understand what I mean (click thumbnails for bigger images).  </p>


<dl>
  <dt><b>Here is a challenge for you:</b></dt>
    <dd><i>Fix that bug</i> (<A href="http://bugs.kde.org/show_bug.cgi?id=139882">#139882</A>). I guess it can be done in less than 5 lines of code. The files to look at: <A     href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdeprint/kpdriverpage.cpp?rev=611414&view=markup">kpdriverpage.cpp</A> and  <A href="http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdeprint/driverview.cpp?rev=611414&view=markup">driverview.cpp</A>.
   </dd>

  <dt><b>Price for current KDE developers:</b></dt>
    <dd>(a) You'll feel good.<br>(b) Pizza and a coke or beer next time we meet.
    </dd>

  <dt><b>Price for anyone who's not (yet) a KDE developer:</b></dt>
    <dd>(a) You'll gain some appetite to fix more little things in KDE.<br>(b) You'll have completed the huge first step on your way to become a new KDE developer: your first patch.<br>   (c) You may get one of these cool <i>@kde.org</i> email addresses for you soon.   </dd>
</dl>

<p>Any takers?</p>

<!--break-->