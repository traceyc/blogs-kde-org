---
title:   "First FreeNX Wallpaper"
date:    2005-09-03
authors:
  - pipitas
slug:    first-freenx-wallpaper
---
<p>Another feedback re. my recent blog entry: there is now <A href="http://www.kde-look.org/content/files/28658-freenx-wallpaper02-1600.png">The First FreeNX Wallpaper</A> available from <A href="http://kde-look.org/">KDE-Look</A>. It is <A href="http://www.kde-look.org/content/show.php?content=28658">installable</A> via the <A href="http://dot.kde.org/1110652641/">"Get Hot New Stuff"</A>. Looks pretty cool too (some non-Europeans say it is too Old-World-centric -- well, let them create stuff that suits their tastes better. The important point is that FreeNX can connect us all, no?)</p>