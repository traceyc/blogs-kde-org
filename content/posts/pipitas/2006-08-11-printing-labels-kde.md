---
title:   "Printing labels with KDE (!)"
date:    2006-08-11
authors:
  - pipitas
slug:    printing-labels-kde
---
Today <a href="http://blogs.kde.org/node/2239">Cristian blogged</a> about a (good) <a href="http://www.linux.com/article.pl?sid=06/08/04/1651234">article on Linux.com</a> dealing with label printing on Linux.

<dl>
<dt>But despite all the good info contained in that piece, there is an extremely disappointing aspect to it:</dt>

<dd>that article misses to even mention in passing the <b>leading</b> application for printing labels on Linux. It is a KDE application. Its name is <a href="http://www.kbarcode.net/">kbarcode</a>. Their homepage has some enlightinging <a href="http://www.kbarcode.net/Screenshots.29.0.html">screenshots</a>. It comes with a <a href="http://kent.dl.sourceforge.net/sourceforge/kbarcode/kbarcode-2.0.0.pdf">good documentation</a>.</dd>
</dl>

Why, oh, why is it that some of the most shining gems that were grown in the KDE ecosystem do not enjoy any good visibility in the wider software world? And what's even worse: why do some not even have any visibility in the somewhat "inbreed" Open Source / Free Software szene??

Does our general PR really suck so badly? Can we fix that with KDE4? What does need to be done? What needs to be done <i><b>now</i></b>?

Or is it just that in this specific case the application developers picked a bad, misleading name with "KBarcode"?

In any case: I don't blame the author for not mentioning KBarcode, or his lack of familiarity with it. (He seems to have a preference of commandline tools anyway...). I blame ourselves for being rather bad in getting the word out about some of our excellent applications. Is it because these are not too useful for the typical Linux or KDE Geek? (They all use email, web browsers, news readers, music players, instant messengers, code editors, blog software, HTML editors on a daily basis. But printing software? <i>Label</i> printing software?? What's a <b><i>label</i></b> again, Dude?!

OK, let's at least get the word out here, on this website. Let at least the 200 readers of KDE developer blogs know the following points:


<ul>
<li>KBarcode is an excellent, world-class barcode and label printing application for KDE. (I do not even know of a thingie that comes anywhere close to it on the Windows platform. DISCLAIMER: I am not familiar with proprietary software that costs more than 1000.- EUR per license; so there <i>may</i> in fact something available for lots of $$$).</li>

<li>KBarcode can be used to print a lot more things that are *not* barcodes, and do not include anything like a barcode!  Everything from simple business cards, adress labels, price labels, postcards, up to very complex labels with several barcodes (e.g. article descriptions).</li>

<li>KBarcode ships with an easy to use WYSIWYG label designer. It includes a setup wizard. It supports batch import of data for batch printing labels (directly from the delivery note). It bundles *thousands* of predefined labels. It sports database management tools. It is translated into many languages. </li>

<li>KBarcode can even print more than 10.000 labels in one go with no problem at all. </li>

<li>KBarcode can import data for printing can be imported from several different data sources, including SQL databases, CSV files and the KDE address book. </li>

<li>Last, but not least, KBarcode includes a (simple) barcode generator (similar to the old xbarcode some of you may know). *All* major types of barcodes like EAN, UPC, CODE39 and ISBN are supported! Even complex 2D barcodes are handled well (this one is using third party tools). The generated barcodes can be directly printed or you can export them to images for use in another application.</li>
</ul>

Yes, and KBarcode is Free Software licensed under the terms of the GNU GPL.

<!--break-->
