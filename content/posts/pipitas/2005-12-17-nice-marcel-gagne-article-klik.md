---
title:   "Nice Marcel Gagne article on klik"
date:    2005-12-17
authors:
  - pipitas
slug:    nice-marcel-gagne-article-klik
---
Marcel Gagne published a very nice, and well written <A href="http://www.informit.com/articles/article.asp?p=432808">article about klik</A>. This piles onto the stack of recent klik publicity (canllaith has an extended piece <A href="http://www.tuxmagazine.com/">in the current TUX magazine</A>, which even made it into the major headline on the frontpage). I found only very few things I could nitpick about. Go read it, if you are not yet familiar with <A href="http://klik.atekon.de/">klik</A>.
<!--break-->