---
title:   "klik server: delivery of \"recipes\", not of pre-build .cmgs"
date:    2005-10-23
authors:
  - pipitas
slug:    klik-server-delivery-recipes-not-pre-build-cmgs
---
<p>This weekend saw some more progress in <A href="http://klik.atekon.de/">klik development</A>. Before I write about the details involved, let me first re-iterate some very simple facts about how klik works. I think it is required, given some of the emails I received, and some of the questions asked in IRC.</p>

<p>Most klik-using people know by now, that after successfully "following" a link like <A href="klik://flock">klik://flock</A> or <A href="klik://kitty">klik://kitty</A> the klik client will somehow make a flock.cmg or a kitty.cmg appear on your desktop. The knowledge about what .cmg files are (compressed file system images, similar to an ISO image which is burned to CD), how the contained application is executed (loopmounting the .cmg and running a wrapper script that knows about the additional PATHs and LD_LIBRARY_PATHs to use) and what the benefits are ("1 file == 1 application", easily relocatable, doesn't interfer with your system libraries or your package manager, easily reverted if you don't want it any more) seems to have become fairly common already.</p>

<p>The knowledge about what happens in the background if you click a klik link is still very scarce. Contrary to what most people tend to think, the klik server does *not* offer ready-made .cmg files for download.</p>

<p>So, to summarize, I'll use the example of <A href="klik://kopete">klik://kopete</A>:</p>
<ul>
 <li>the klik server receives the request "I want to get a klik bundle called 'kopete' from you, and I am OS version Linux ABC.xyz"</li>
 <li>the klik server does *not* send the klik-bundle "kopete.cmg" back to the client</li>
 <li>instead, the klik server sends a "recipe" to make the klik client build the kopete.cmg for himself</li>
 <li>the recipe enumerates a set of <b><i>binary</i></b> files (mostly official .deb packages), together with a few dependencies for the target system Linux    ABC.xyz</li>
 <li>the klik client fetches the .debs (could also be .rpms or .tgz, depending on the recipe) and unpacks them in /tmp/klik/kopete/</li>
 <li>the klik client re-bundles the input .debs (.rpms, .tgzs,..) into one single, compressed .cmg file system image together with the "wrapper" script</li>
 <li>the klik client last moves the newly-made kopete.cmg into the $HOME/Desktop directory (to make the file location most obvious to newbie users), and deletes the input .deb, .rpm or .tgz files (currently it lets the /tmp/klik/kopete/ directory stay there, though)</li>
</ul>

<p>You could look at klik as being a web service, that enables users to build their own .cmg files from binaries that are offered by known Linux distributors, or simply the upstream developers of a program (including proprietary software vendors such as Opera, Skype, Real Networks or Adobe).</p>

<p>This concept makes klik scale very well, even for very large propagations. At the same time it is very flexible: change the recipe at one place and a bug is fixed or an improvement is in place for all future users at once.</p>

<p>It is beyond the scope of klik to compile binaries. klik is *not* a replacement or competition to <A href="http://autopackage.org/">autopackage</A> (rather, it is the case that autopackage-d builds work best for klik and are most usable for cross-distro .cmg bundles).</p>

<p>Having said that, you obviously are aware of the slight modification that the klik recipe concept can easily stand: instead of pointing the client to some official .deb/.rpm/.tgz packages to use for fritting its own .cmg, you as the developer of a cool KDE application could of course also elect to build a ready-made .cmg from your own compiles (it is pretty easy) and just tell us a valid URL to fetch the nightly or weekly updated KCoolApp.cmg from. We'll then provide a 10 line recipe on the central klik server, that will download, save and run your cool application upon clicking <A href="klik://KCoolApp-testing">klik://KCoolApp-testing</A> regardless of where it is published..... The benefits for you are obvious: an easy way to make your latest development binaries available to your beta-testers, translators, documentation-writers, usability-advisors, power-users. Plus, an extremely quick "time to market" after each of your releases -- users can already enjoy features of a new version even before their distro has picked it up and provided "official" packages. </p>

<!--break-->