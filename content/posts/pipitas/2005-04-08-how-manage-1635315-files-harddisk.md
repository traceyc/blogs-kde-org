---
title:   "How to manage 1,635,315 files on a harddisk?"
date:    2005-04-08
authors:
  - pipitas
slug:    how-manage-1635315-files-harddisk
---
Today I counted: I have <b>lots</b> of files on my systems.

<ul>
<li> 1,635,315 files on my main workstation (SUSE-9.1); 80 GBytes on one disk filled up by 50%.</li>
<li> 1,493,166 files on my other workstation (SUSE-8.0); 20 GBytes on 4 disks filled up by 93%.</li>
<li> 418,769 files on my company notebook (Win XP Prof); 40 GBytes on one disk filled up by 88%.</li>
</ul>

OK, there probably is no better way for now than organize these files in a structure of directories, subdirectories, files and accessing them by "paths".
<br><br>But is it really the best way to present these data to me in the same hierarchical view as the file system stores them?

<br><br>Or isn't there a better way of presention? A better way of navigating the disk's content than following the <i>d:\directory\subdirectory\subsubdirectory\filename</i> "paths" for finding stuff? At least one that complements our traditional "file managers"?

<br><br>Windows' Explorer already sucks very much with 400,000 files. Konqueror, compared to its competitor does much better, servicing 1,600.000 files. But there is a feeling of uneasiness. How much longer can a traditional file manager keep up with the growth rate?

<br><br>The typical number of files we have to handle on our computers isn't going to decrease. (Who remembers the days of MS-DOS 3.3? Did it have more than 1.000 files to handle? And did MS-DOS 6.22 ever reach beyond 10,000?)

<br><br>So one thing is sure: traditional file managers and their interfaces will have more and more problems to remain a useful tool for handling our data. 

<br><br>What will be once we have 100 million files to cope with inside one workstation? (Don't say that won't happen. Remember how the IBM chairman in the 50s predicted that "the world market for computers will never go beyond the number of 5"? And how Bill Gates said that no computer user would ever need more than 1 MByte of RAM?).
<!--break-->
