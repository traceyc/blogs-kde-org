---
title:   "klik2 development: Milestone 2 reached"
date:    2008-01-27
authors:
  - pipitas
slug:    klik2-development-milestone-2-reached
---
<p>This weekend it's time to announce it. Finally: <A href="http://code.google.com/p/klikclient/">klik2</A> development has reached our internal "<A href="http://code.google.com/p/klikclient/wiki/MileStone2">Milestone 2</A>". </p>

<p>Remember klik? That project that aims to make Linux end-user software installation and usage more easy than on any other platform? "Grandma-proof", if you like? By making to 'install' an application as easy as copying a single file to a USB thumbdrive or to a different computer? By implementing application-level virtualization, encapsulating each end-user program into a single file, following the <i>1 application == 1 file</i> principle? </p>

<p>For Milestone 2 we originally had set very modest goals:</p>

<ol>
 <li>Number of tested applications: 5 (xvier and 4 Gnome apps only: xchat, gobby, glade and hardinfo)</li>
 <li>Number of tested distros: 3 (Ubuntu Gutsy, Fedora Core 8, Mandriva 2008)</li>
 <li>Number of tested klik CLI sub-commands: 2 (klik get, klik run)</li>
 <li>Number of tested desktop environments: 2 (Gnome, KDE)</li>
 <li>Number of Screencasts: 3 (gobby, glade, xchat)</li>
</ol>
 
<p>Well, if we took it literally, we failed. Because we could not find someone to thoroughly test on Fedora (it also turns out we have most problems currently to get klik2 to work on Fedora), and we could not find <i>anybody</i> for Mandriva. We also don't have the screencasts yet... </p>

<p>But we don't take it literally. Since we found people to test it on openSUSE and on <A href="http://distrowatch.com/sidux">Debian/Sidux</A>, we're happy enough nevertheless and declare Milestone 2 reached. And as for the screencasts... why don't <b>you</b> step forward and create some for us??</p>

<p>This Milestone 2 achievement means that we have most of the "pillars of klik2" in place now to make big progress in a very short time. With some more finetuning, we hope to have a few hundred tested applications working and also <a href="http://www.fosdem.org/2008/schedule/events/260">showcase them pretty soon...</a> However, for Milestone 3 we still do 'only' aim for 15 -- but!, we'll add Qt- as well as KDE-based applications, plus, we want 2 more distros tested...</p>

<p>What is even more exciting: our old friend Niall "bfree" Walsh is currently putting lots of efforts into two totally unexpected additions which we would very much like to see ready for addition to the current Milestone 3 goals: </p>
[image:3229 align="right" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"] 

<ul>
  <li>transforming the klik2 client source code into a properly done .deb package</li>
  <li>getting klik2 run on the tiny EEE PC</li>
</ul>

<p>If you want a glimpse at bfree's work look at the screenshot on the right (click thumbnail to see full size).</p>

<p>Next stop: Milestone 3! If you want to help, here is what you can do:</p>

<ul>
<li><A href="http://code.google.com/p/klikclient/wiki/HowToTestGUI"><b>Install and test</b></A> the klik2 client from our SVN repository (and ask questions in <A href="irc://freenode.net/klik">#klik</A> IRC channel on Freenode)</li>
<li>If you happen to run Mandriva, test the Milestone 3 application set for that distro (and <A href="http://code.google.com/p/klikclient/wiki/MileStone3">fill in</A> the gaps)</li>
<li>Test the <A href="http://code.google.com/p/klikclient/wiki/MileStone3">Milestone 3</A> application set for your distro (feel free to already start testing <A href="http://code.google.com/p/klikclient/wiki/MileStone5">some more too</A>)</li>
[image:3230 align="right" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"] 
<li>Use the <A href="https://build.opensuse.org/">openSUSE Build Service</A> to create proper RPM packages (also for Fedora, CentOS, Redhat, Mandriva,...) for the klik client (and <A href="https://mail.kde.org/mailman/listinfo/klik-devel">tell us</A> about it)</li>
<li>Help us create some nice screencasts about klik2</li>
</ul>
<p>Make sure you send us back the automatically generated feedback (see screenshot on the right -- fill in some additional comments too), when that dialog pops up after running <i>"klik get someapplication"</i>...</p>

<!--break-->
