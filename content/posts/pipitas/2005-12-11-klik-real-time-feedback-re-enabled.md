---
title:   "klik: real-time feedback is re-enabled"
date:    2005-12-11
authors:
  - pipitas
slug:    klik-real-time-feedback-re-enabled
---
[image:1671 align="right" hspace=4 vspace=4 border=0 width="407" class="showonplanet"]
<p>The weekend has seen some more work on klik. probono has re-enabled the nice "give-us-some-feedback-after-first-run" feature.</p>
<p>It means that a kdialog (or an Xdialog or zenity equivalent) will pop up after you run a klik recipe for the first time, asking you for some feedback. After you filled it in and clicked "OK", you'll immediately see it has gone live on the klik server if you visit the user <A href="http://klik.atekon.de/comments.php">feedback page</A>.</p>
<!--break-->