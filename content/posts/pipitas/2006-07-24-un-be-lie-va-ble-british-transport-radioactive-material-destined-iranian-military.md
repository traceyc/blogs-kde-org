---
title:   "Un-be-lie-va-ble. British transport of radioactive material destined for Iranian military confiscated in Bulgaria."
date:    2006-07-24
authors:
  - pipitas
slug:    un-be-lie-va-ble-british-transport-radioactive-material-destined-iranian-military
---
<p>A British lorry transporting radioactive material has been <A href="http://www.dailymail.co.uk/pages/live/articles/news/news.html?in_article_id=397124&in_page_id=1770">confiscated in Bulgaria</A> at the border to Rumania on Saturday. The radiating load contained lots of Caesium 137 and Americium-Beryllium which can be used to build a <A href="http://en.wikipedia.org/wiki/Dirty_bomb">"dirty bomb"</A>. It arouse the suspicion of border guards only because its radiation level was 2000 times above the normal.  The shipment was destined for and addressed to the Iranian Ministery of Defense (!). It seemed to have had an official export approval by British authorities ("Department of Trade and Industry"). Un-be-lie-va-ble.</p>

<!--break-->