---
title:   "Recommendations to (K)Ubuntu Dapper users: How to restore an uncrippled CUPS  [0]"
date:    2006-06-18
authors:
  - pipitas
slug:    recommendations-kubuntu-dapper-users-how-restore-uncrippled-cups-0
---
<p>In recent months I wrote various blog entries (no, they were rather "rants", and even tagged as such; [<A href="http://blogs.kde.org/node/1899">1</A>], [<A href="http://blogs.kde.org/node/2064">2</A>], [<A href="http://blogs.kde.org/node/2072">3</A>], [<A href="http://blogs.kde.org/node/2076">4</A>], [<A href="http://blogs.kde.org/node/2077">5</A>]) dealing with Ubuntu's crippled CUPS 1.2 configuration. (I'm not complaining about the feedback -- but I was rather surprised how many emails on that topic I received; also how high a number of abusing messages by anonymous writers sought to annoy me. Seems (K)Ubuntu followers have a higher ratio of "fanboy" type users than most other distros... :-) ). </p>

<p>Previously, I had <A href="http://blogs.kde.org/node/2078">committed myself</A> to test and report about experimental 1.2.1 packages prepared by an Ubuntu maintainer -- but then could not in fact follow up due to the unexpected removal of those packages. Anyway, my take on the whole topic is this, and I'm not giving up on it: </p>

<ul>
  <li><i><b>CUPS 1.2.0 has a series of new and cool features that make it easier than ever to configure printing, not only for home users, but also for enterprise environments.</b></i></li>
  <li><i><b>Disabling these features (for the sake of supposedly better security) does not help in making the Linux desktop more competitive against MS Windows (current as well as future versions).</b></i></li>
</ul>

<p>I'm not a regular (K)Ubuntu user (yet) -- my past blog writings were rather prompted by a number of private emails and phonecalls I received asking for help and advice. When investigating the problems, I found various... hmm, let's call them "sub-optimal configuration decisions", made by the Ubuntu maintainers in charge of CUPS packaging. These led to a CUPS setup that is wildly different <b><i>in functionality</i></b> from what <A href="http://www.cups.org/">CUPS developers</A> ship as default, and to a behaviour of CUPS that is very different to the one that is officially documented. More important: it lets users suffer from missing functionality, broken features and a CUPS 1.2 handling that has become more difficult to cope with instead of more easy. This policy also goes completely against the <A href="http://blogs.kde.org/node/1934">goals set</A> by this year's <A href="http://groups.osdl.org/workgroups/dtl/desktop_architects/desktop_printing/">OSDL desktop printing architects meeting</A> aiming to get printer installation done automatically and to make their setup more of a <i>"It Just Works (TM)"</i>-type of operation.</p>

<p>Here is a short summary of my grieves with Ubuntu's (and in part also Debian's) packaging and modifying of CUPS:</p>

<ul>
 <li>The CUPS web interface is artificially crippled to disable printer and server administration functions (which usually make for easy handling by users).</li>
 <li>The CUPS client browsing is disabled and thus deprives users from effortlessy utilizing printers shared by functional CUPS print servers.</li>
 <li>The new CUPS <i>snmp</i> backend is disabled and hence disallows auto-detection and semi-automatic installation of network-attached printers.</li>
 <li>The supposedly increased security introduced by the Ubuntu packager changes to the default CUPS setup is a bogus achievement.</li>
 <li>The documentation accompanying Ubuntu's modifications are inappropriate, incompleted and even misleading.</li>
</ul>

<p>In the next few days, I'll deal with each one of these items, provide suggestions to the Ubuntu packagers of CUPS, and explain to interested users how to get back the most important items of the lost functionality. Stay tuned...</p>

<!--break-->