---
title:   "iPod marketing (how someone else could have done it)"
date:    2006-03-15
authors:
  - pipitas
slug:    ipod-marketing-how-someone-else-could-have-done-it
---
<a href="http://video.google.com/videoplay?docid=36099539665548298&q=microsoft+ipod">This</a> is a hilariously good movie -- apparently from inside Microsoft. It outlines how MS would have marketed the iPod "the MS way".  It is meant to be self-joking about the company's own marketeers; and indeed it is.:-)
<!-- hidden content 1 -->
But still, the thing has a serious lesson behind it.
<!--break-->
<!-- hidden content 2 -->