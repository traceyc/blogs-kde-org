---
title:   "klik://wesnoth-latest <-- now updated to 1.1.1"
date:    2006-02-08
authors:
  - pipitas
slug:    klikwesnoth-latest
---
hmmm... not sure if I should really leak it. 

Because it is totally untested. I've currently only a remote connection from a Windows/PuTTy box to a SUSE Linux server with no FreeNX or NX server installed. So I couldn't test it. But I used isaac's Debian packages (not knowing if they are for Sarge or for Sid -- I'm assuming Sid) to update the <a href="klik://wesnoth-latest"><b>klik://wesnoth-latest</b></a> recipe. Previously it built a Wesnoth 1.0 klik image -- meaning it wasn't really "*-latest". Now it builds a 1.1.1 image -- it is <b>really</b> latest!  :-)

Since I couldn't test it, I had solely to rely on <i>"ldd ./usr/lib/*.so* ./usr/bin/*"</i> to discover any dependency libs that needed inclusion into the image. I hope I succeeded... In any case <a href="http://wesnoth-latest.klik.atekon.de/comments/">users' feedback</a> will tell me tomorrow evening what is missing in the package.... I expect anything between "total failure -- klik suxxx" and "wesnoth works, but misses features X, Y and Z".
<!--break-->