---
title:   "A shameful sellout of Linux to Microsoft by Novell? I tend to say \"Yes\""
date:    2006-11-04
authors:
  - pipitas
slug:    shameful-sellout-linux-microsoft-novell-i-tend-say-yes
---
<p>Meanwhile I read a bit more about the Microsoft <--> Novell cooperation deal. Hell, what an utterly shameful sell-out!</p>

<p>In essence, Novell (and the guys leading it, Ron Hovespian & Co.) have defacto acknowledged that Linux violates Microsoft patents. They bought themselves (as a company) some exclusive "peaceful co-existence" (limited to 5 years from now) with the Evil Empire of Global Software Monopoly.</p>

<p><a href="http://www.novell.com/linux/microsoft/faq.html">Novell's FAQ</a> says, they worked out the details "with the principles and obligations of the GPL in mind". Right...., riiiiiight! Yes, with the "GPL principles in mind" -- but not in order to <b>advance</b> these. Rather in order to <b>work around</b> them. Get this, readers!</p>

<p>Microsoft in turn <i>now</i> is free to go after all the rest of Linux vendors (Novell's competitors in the Linux market). They'll just deal with Novell <i>last</i>. In 5 years... Meanwhile, MS even has some "legal credibility" (more so than SCO) -- it can show to judges and courts that Novell (the holder of the Unix Copyright, yes?) in fact has acknowledged in writing its basic claims about its software patents.</p>

<p>Part of the deal is that Microsoft and Novell will cross-promote and support one another's products to their respective customers. Hello?! Microsoft now selling SUSE Linux Enterprise Servers?! Hell freezing over?? White flags flying over their Redmond campus???</p>

<p>Surely not. Never forget what sort of company Microsoft is to its adversaries on the market: rich, powerful, adamant, extremely persistent, without mercy. And it combines these assets with enormous cleverness in persuing its (natural) goals.</p>

<p>Or has Microsoft suddenly given up its strive for bigger sales, more stranglehold on the markets, stomping down all competitors, holding on to its monopolies? Has it turned into a "we-are-nice-guys-now" charity that wants to help Linux and Free Software? Bet your arse they've not.</p>

<p>Microsoft's little proxy war on Linux, fought via SCO, is reaching endgame phase. And they're not exactly looking like winning this one. A new strategy is needed. And quick they are to execute it now (and smart they were to prepare it since more than a year). </p>

<p>Novell CEO Ron Hovespian (and his other guys) have nicely fallen into a well-prepared trap. (Or.... was this CEO even "planted" into Novell to lead exactly to that end??)</p>

<p>In all of its history (ever since they were a very, very tiny software company when they out-smarted and a*se-f+++ked even almighty IBM at the time) Microsoft never, ever made such an interoperability deal to "help" its new "partner", but always to defeat them. This one is not different. (It will just <b>look</b> a bit different for one or two years.). You've been warned.</p>
<hr>
<p><small>(<b>Update:</b> I've received 2 anonymous emails concerning my last blog entry. They were drooling from blindful hate -- I assume, penned by Novell employees?, who else would have a motive? -- accusing me of various things. One said, I was "jumping the gun" with my (rather harmless, IMHO) last blog, and asked me to "shut the f*ck up with [your] posturing as a KDE spokesperson on all matters SUSE". *LOL*...  Dudes -- I am not shutting up, for once. Should I ever be silent on a matter, I just am too lazy about it. Or too busy. Or have no opinion at all on it. But sure, what I publish in my blog (or in any signed article I choose to write), is my own personal opinion, and of course in no way a statement for any group or project I may have contributed to. Capito?!? --  <i>And really, just so you know: What does bother me much more that this sad Novell "Yes, MS patents are in Linux"-affair right now, is the continuous buildup of a huge naval force of US battle ships in proximity of the Iranian coast... just 4 days away from the US mid-term elections. I'm more worried what false aces Bush's macchiavellian chief strategist Karl Rove will conjuringly produce from inside his pants.... Really!)</small></p>

<!--break-->