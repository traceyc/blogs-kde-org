---
title:   "\"How many Microsofties does it take to implement the OFF menu?\""
date:    2006-11-26
authors:
  - pipitas
slug:    how-many-microsofties-does-it-take-implement-menu
---
<p>An interesting read: Joel Spolsky argues that too many choices lead to user unhappiness and <a href="http://www.joelonsoftware.com/items/2006/11/21.html">looks at the Windows Vista <i>"OFF"</i> feature</a> as an example. In a response on his own blog, former Microsoft programmer (now Google employee) Moishe Lettvin who worked on exactly that part of Vista for a year <a href="http://www.drizzle.com/~lettvin/2006/11/windows-shutdown-crapfest.html">describes how the development process inside Microsoft worked for his group</a>. </p>

<p>After reading this, you can be pretty sure that our KDE's development process as we know it would still compare as very lean and highly efficient -- even if we had 5 <a href="http://lists.kde.org/?t=116336735900002&r=1&w=2">threads like this</a> every week...</p>
<!--break-->