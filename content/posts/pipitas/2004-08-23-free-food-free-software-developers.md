---
title:   "Free Food for Free Software developers"
date:    2004-08-23
authors:
  - pipitas
slug:    free-food-free-software-developers
---
It was great to have the <A href="http://conference2004.kde.org/sched-devconf.php">DevConf</A> being fed by IBM sponsoring a "Free Lunch for Free Software developers". Actually, what the food voucher said was "Big Blue and Blue Angel happily invite you for a free lunch". If you dont know what "Blue Angel" is -- it is the translation of the name of the Bistro associated to Filmakademie...

<IMG src="http://developer.kde.org/~binner/aKademy2004/day1/dscf0039.jpg" alt="IBM free lunch voucher at KDE DevConf" width="600" height="450"  border="0">
