---
title:   "klik://ooo2 (Fastest testdrive of OpenOffice.org2 RC 1, ever)"
date:    2005-09-29
authors:
  - pipitas
slug:    klikooo2-fastest-testdrive-openofficeorg2-rc-1-ever
---
<p>OpenOffice.org 2 has been announced as available in Release Candidate 1 shape. The SUSE-RPM is 126 MByte to download (if you include German language support). Happy installing...</p>

<p>Oh, wait. If you install these, not only will your download time have to be accounted for, the action will also overwrite your current installation of OOo2. (OK, that might be a pretty safe bet. After all, it will replace a previous <i>Alpha</i> or <i>Beta</i> build of OOo, and likely will not do any harm.) But upon installation, it will expand to over 300+ Mbyte and 3000+ files in 300+ directories on your harddisk. And you will not be able to just copy the installed application from your office workstation to your notebook to your home computer to your Knoppix CD....</p>

<p>Why don't you just try to <A href="http://klik.atekon.de/">klik-install</A> OOo2 RC1? probono has prepared a package in record time for you.</p>

<p>After having the <A href="http://klik.berlios.de/">klik client</A> on your system, klik-installing OOo2 RC1 is as easy as clicking this link: </p>

<ul><li><A href="klik://ooo2">klik://ooo2</A>.</li> </ul>

<p>I've successfully run it on Kanotix (2005-03) as well as SUSE (9.3 + 10.0). It should work on many more distros equally well.</p>

<p>The klik://ooo2 initiated data transfer will be 127 MByte; the download will be all in one file (called "ooo2rc1.cmg"), which will launch OpenOffice immediately (to proof everything works); you can later re-run the application directly by simply clicking on it from Konqueror; the ooo2rc1.cmg will also *stay* all in one file -- it will not expand into 300 or more MByte upon installation! -- and you can move this file from your Desktop to a USB stick (or burn it onto a CD), and run it from there: no separate installation needed on your office workstation, notebook, computer, Knoppix CD. In all cases, your old OOo installation will remain unaffected (even your personal configuration files go into a save, separate directory: $HOME/.ooo-2.0-pre). Should you want to get rid of it again, just delete the ooo2rc1.cmg file. Your system will be reverted to its previous state...</p>

<!--break-->