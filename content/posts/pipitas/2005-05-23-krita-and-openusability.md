---
title:   "Krita and OpenUsability"
date:    2005-05-23
authors:
  - pipitas
slug:    krita-and-openusability
---
I was thrilled to read what <a href="http://www.valdyas.org/fading/index.cgi/hacking/krita/rc.html">Boudewijn has to say</a> about new features in the upcoming <a href="http://koffice.kde.org/krita/screenshots.php">Krita</a> (released as part of the <a href="http://koffice.kde.org/">KOffice suite</a>, 1.4.0). 

One thing he said is even better: <i>"I’ve also toyed with the idea of asking the OpenUsability people for a review of Krita’s UI — the problem here is that we have some very clear ideas on what we want to change almost immediately after the release and that may impact the usefulness of a review."</i>

Yes, Boudewijn, it may impact the usefulness of a review for your <i>current</i> release. But to get in touch with the <a href="http://www.openusability.org/">OpenUsability project</a> as early as possible in the development process is better (IMNSHO) than trying to get their retrospective comments and criticism. If you have already "very clear ideas" on what you want to do next, this is the right time to start discussing with them. Just to make sure that your clear idea is not "clearly wrong" in their eyes...  ;-)

It's good for applicaitons to have <i>retrospective</i> usability reviews done by people who are experts on that. It is bad to have no input at all. It is better to have their help during the development cycle. But probably it is the best to have them on board <i>from the very first moment</i> you have an idea you want to implement. (But I dunno for sure -- no-one in the KDE project ever involved them from this early stage. We are just about learning how to handle that issue, aren't we?)

----------------------------------
<small>(BTW. Boudewijn, I am only writing this in *my* blog, 'cause I couldn't figure how to repsond directly at *yours* ;-)  )</small>
<br>
<!--break-->