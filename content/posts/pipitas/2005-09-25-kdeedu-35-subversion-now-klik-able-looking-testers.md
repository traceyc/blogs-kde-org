---
title:   "KDEedu 3.5 from Subversion now klik-able... looking for testers"
date:    2005-09-25
authors:
  - pipitas
slug:    kdeedu-35-subversion-now-klik-able-looking-testers
---
<p>I've now prepared 2 klik-able <A href="http://edu.kde.org/">kdeedu</A>  packages. I compiled with "--enable-debug=full". One of the packages is stripped to save space and bandwidth. However, I recommend you to run the one with debug symbols included. This way a meaningful backtrace or konsole output is possible:</p>

<ul>
  <li>80 MByte:  kdeedu-latest-debug.cmg</li>
  <li>40 MByte:  kdeedu-latest-stripped.cmg</li>
</ul>

<p>The packages' main purpose for now is to serve as a proof-of-concept.  I wanted to see if I could do it, and I wanted to explore the amount of work I have to put in to make it happen.[image:1479 align=left hspace=6 vspace=3 border=0 width=365 height=338 nolink=0]</p>

<p>They are built from a self-compiled KDE source code check out of today. I built on a SUSE-9.3 box inside the KDE-3.4.2 environment (packages from SUSE), and compiled not against qt-copy, but against the system's Qt (3.3.4). To make the result more portable, I should probably have used a SUSE-9.1 environment with KDE-3.3 installed. But I do not have such a box with enough harddisk space for now.</p>

<p>The result I've tested on a SUSE-9.1 and a SUSE-10.0 system, as well as a Kanotix-03.2005 DVD. It works. (Well, at least all programs started up without a crash; didnt have time to do more).</p> 

<p>So far, I made it all manually. The next step is to write a script that automates the creation of kdeedu klik bundles. Well, script not necessarily made by me: I've no time to do it. (Best would be to have a "make cmg" Makefile target hacked into the KDE build system. But who is up to such a task?)</p>

<p>To run this package, you need to have the klik client installed.  To install the klik client, open a <i>"Mini CLI"</i> (keyboard shortcut is <i>"[alt]+[f2]"</i>, K Menu entry is <i>"Run Command..."</i>). Then type 
  <code>
    wget klik.atekon.de/client/install -O - | sh  </code> 
and hit <i>"Enter"</i>. Follow the instructions. This will download and run the <i>"install"</i> script from the URL above. The result will be two files in your home directory, <i>".klik"</i> and <i>".zAppRun"</i>. You will be using ".zAppRun" to run this KDEEdu package.</p>

<p>The package includes a goodie: the <A href="http://www.tuxipuxi.org/?Code:KEduLaunch"><i>"kedulaunch"</i></A> by <A href="http://tuxipuxi.de/blog/">Michael "tuxipuxi" Goettsche</A>. It is not yet part of the kdeedu module, and it is still in its early stages.</p>

<p>I found kedulaunch incredibly useful to make this klik package a rounded up one. Therefore kedulaunch is started by default when klik-ing on the kdeedu-latest-*.cmg file.</p>

<p>There are more tricks to this package. You can start the package from the commandline in various ways: [image:1480 align=right hspace=6 vspace=3 border=0 width=184 height=464 nolink=0]</p>


<dl>
<dt><tt>${HOME}/.zAppRun kdeedu-latest-debug.cmg</tt></dt>
   <dd>without any additional arguments, this will run kedulaunch.  kedulaunch then lets you choose from the real KDEEdu applications. You can run multiple apps at the same time.</dd>

<dt><tt>${HOME}/.zAppRun kdeedu-latest-debug.cmg ktouch</tt></dt>
   <dd>with one of the KDEEdu app names given as an argument, it will run the respective app (without kedulaunch making its appearance).</dd>

<dt><tt>${HOME}/.zAppRun kdeedu-latest-debug.cmg foobar</tt></dt>
   <dd>with an arbitrary argument given, the package will pop up a kdialog that offers to choose one of the contained applications.</dd>
</dl>

<p>Of course, you can add one or more of these commands into the K Menu. I assume you know how to do this.</p>

<p>If you start this package, its programs will write into a <tt>KDEHOME=$HOME/.kdeklik</tt> -- this I separated and named differently from the standard <tt>".kde"</tt> to avoid that it messes with your current settings should you have other kdeedu applications installed. If you want it to use your standard KDEHOME (very often in .kde or .kde3), create a symlink:

  <code>
    ln -s $HOME/.kde $HOME/.kdeklik  </code> 
Remember, klik will only work if your Kernel supports <i>cramfs</i> and loopmounting. Most modern distros in fact do have Kernels which support this by default.</p>

<p>And now you want to know where you can download this package?  You can't. The reason is: I do not have a webspace for this.  And it will only go to the klik server if I know it works for a few more systems than just mine.</p>

<p>If you want to help finding that out, mail me or grab me in IRC, channel #klik. If you have webspace to offer, great. Tell me so.</p>

<p>My next endeavour will be to build a klik bundle of the current <A href="http://ww.KOffice.org/">KOffice</A>, hopefully in time for the <A href="http://developer.kde.org/development-versions/koffice-features.html">upcoming 1.4.2 release</A> (end of this month).</p>

<p>Have fun with <A href="http://klik.atekon.de/">klik</A> and these KDEEdu programs. After you asked me where to get them from...</p>

<!--break-->
