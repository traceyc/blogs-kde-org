---
title:   "Wanted: volunteer to create VMWare images for klik development"
date:    2007-08-09
authors:
  - pipitas
slug:    wanted-volunteer-create-vmware-images-klik-development
---
<p>klik developers are looking for one or more volunteer(s) to create (and possibly maintain) VMWare images that can be used with VMWare player (and possibly other 'virtualized OS' players) for klik bundle development and testing.</p>

<p>Specifically, we have had requests from application developers who want to offer bleeding-edge development versions of their code in the shape of a ready-made klik bundle. This means they'll have to compile their application (whenever they want to release a new klik) on a "Debian stable" platform (because one usually doesn't find his own bleeding edge code readily built as an 'Etch' .deb). The VMWare image is meant to help with that. It should contain exactly the "base system" klik bundles expect to meet on the host platform they're started up on. A list of packages making up that base system is here:</p>

<ul>
   <li>current klik ('klik1') base system (still based on Debian Sarge): <A href="http://klik.atekon.de/apt/status/multi">http://klik.atekon.de/apt/status/multi</A> (and the <A href="http://klik.atekon.de/apt/sources.list/multi">sources.list</A>) </li>
   <li>future klik ('klik2') base system (LSB-compliant): <A href="http://klik.atekon.de/apt/status/lsb">http://klik.atekon.de/apt/status/lsb</A>  </li>
</ul>

<p>These packages are expected to be present as the minimal runtime OS platform where all klik1/2 bundles should be made to work. Now, if a developer wants to compile his app for that target platform, of course GCC and other build tools need to be added to this list.</p>

<p>Of course, there are already a *lot* of ready-made <A href="http://www.vmware.com/appliances/">VMWare images available</A> for download. So one of the first things to do is check them out and see if <a href="http://www.vmware.com/vmtn/appliances/directory/920">one of them</a> already is close to provide what we need. </p>

The next thing to check out is what LSB has on offer: there is <A href="http://www.linux-foundation.org/docs/lsbbook/si.html">LSB-si</A>. LSB-si is a 'Sample Implementation' for LSB. 'Sample' (in contrast to 'Reference') means: it is meant to demonstrate one way to comply with the written specification (while 'Reference Implementation' serves as the baseline for the validation of LSB conformance. LSB-si comes with a <A href="http://www.linux-foundation.org/docs/lsbbook/si-setup.html">chroot LSB-si, an UML LSB-si and a Knoppix LSB-si</A> (according to 'si-setup.html' document). 

<p>So, if...</p>

<ul>
 <li>...you are interested to support klik</li>
 <li>...know how to build a Debian base system</li>
 <li>...know how to configure a software build and compile environment on Debian</li>
 <li>...know a little bit about creating VMWare images</li>
</ul>

<p>you may be the person that will make the difference. Come visit us at the <a href="irc://irc.freenode.net/klik/">#klik IRC channel</a> if you are interested. Or <a href="mailto:pfeifle.at.kde.dot.org">mail me</a> and/or <a href="mailto:probono.at.myrealbox.dot.com">probono</a>.</p>

<!--break-->