---
title:   "How To Extend Open Source on more Desktops?"
date:    2004-12-10
authors:
  - pipitas
slug:    how-extend-open-source-more-desktops
---
<br>So, Aaron, you got <A href="http://aseigo.blogspot.com/2004/12/how-to-kill-open-source-on-desktop.html">very vocal</A> about it.... You think the Open Source Desktop efforts are killed by porting these very same deskops' applications stack over to Windows, making a complete sweitch-over completely un-attractive. 
<hr>
<br><i>"We are at a cross roads...."</i>

<br><br>That's a lot of drama you put in here.

<hr>
<br><i>"that we may not get a second chance to visit as KDE, Mozilla and OOo make determined pushes on win32."</i>

<br><br>Well, Mozilla and OOo are. KDE? No, not yet. For KDE there are only humble beginnings, by a few (too few) individual developers.
<ul>
<li>KDiff3</li>
<li>Kexi</li>
<li>KDE on Cygwin</li>
</ul>
I can not (yet) value these efforts already as "a determined push on Win32". And certainly not one by the whole of "KDE". But of course, I om one of those who would like to see it, and certainly not oppose it. -- In any case, to debate the topic is very healthy.

<hr>
<br><i>"It's All About the Applications"</i>

<br><br>I agree. Mostly. Without applications, a Linux (or FreeBSD, or Hurd, or whatever) platform would be doomed.

<hr>
<br><i>"...the vast majority of users select which operating system to run based on the applications available for it."</i>

<br><br>No, Aaron.

<br><br>The vast majority of users don't choose their operating system. It is being chosen <i>for</i> them. They are not even being asked. Their wishes are not being listened to. (Not that I think users actually <b>had</b> strong wishes, or even silently prefered Linux. That is not the case.)

<br><br>Most first time computer users (up to the present day) make acquiantance with such a box in their workplace. Or at school. And everywhere there, someone else made the choice. Not "the users". Be it the ones in the driver's seat of IT Department. Or be it teachers, professors, high ranking government clerks....

<br><br>I never had a job where my boss on the first day welcomed me saying "Nice you join the team now. We'll give you a notebook straight away. Which operating system do you want?"

<br><br>It <i>*may*</i> be a bit different for private users. But not much. Most 1-st time computer users deciding to buy their first private PC, chose to stick to the same applications as they are used to from work. Once they enter the shop, they choooose..... wait, there is not much choice. There is only Microsoft.

<br><br>So do we now have established the fact that <i>the vast majority of users do not themselves select the operating system they run</i>, Aaron? Of course. Hoever, your argument about "applications available for it" isn't loosing much of its credit by this concession. Because even if it is not "the users" who decide about the OS to install, then the IT department, the teachers, the government officials, or the CEO selling pre-configured PCs thru his business all see some value behind "the applications" behind their OS choice. Or should so in most sane cases. (We leave out the occurances of insanity here, or the notorious cases where Microsoft blackmailed hardware resellers into accepting the one and only "choice"....)

<br><br>Read on...
<!--break-->
<hr>
<br><i>"....applications are what enable people to get things done, be that making a spreadsheet or playing a game"</i> 

<br><br>True. But it is likely that "games" are not valued very highly in businesses' choice of their OS. While for private PCs they of course are a big part of the picture.

<hr>
<br><i>"If the applications people want are available on Windows, they will tend to stick with Windows." </i>

<br><br>You have a point here.

<br><br>Even more: you have more than a point here. You hold valid piece of the puzzle in your hand. But you dont even see the colors of the piece. At best, your view of it is reduced to shades of gray. And there are missing lots of pieces to make up for the complete picture in your essay.

<br><br>Since a port of some key apps to Windows will not give the complete experience of a complete Linux stack, they will still be missing lots of features and benefits when they stay on the other side. But we would be teasing them. We whould have injected the thought "Why not go the whole way now?" into their minds. We had made it more easy for IT departments to go ahead and leave the users (again) no choice but to comply.

<hr>
<br><i>"Conversely, if the applications they want are to be found only on Linux/BSD, they will eventually end up using Linux/BSD."</i>

<br><br>Possibly. 

<br><br>But what if only a very small part of the same applications are to be found on Linux/BSD? And the other needs could be covered, but be covered by completely different applications?  They would have to
<ol>
<li> learn the ins and out of the new OS, plus </li>
<li> learn the ins and outs of the new applications.</li>
</ol>
Would they choose to migrate?

<br><br>That scenario is not fictious. That is the situation facing them now.

<br><br>The miracle is: even at the present stage there is a considerable share of IT desicion makers in enterprises and government bodies who seriously evaluate options and costs of a "switch over". For most, it now looks like "all or nothing", and a big jump. A too big one in many cases. So they refrain. So they sign another 5 year contract with MS....

<br><br>To chop the task into smaller pieces, to take the direction, but only a few steps for now, to smooth the transition out over a period of time is very difficult. And it costs. Not only do you have to train the users. You also need to re-train the IT teams. So Microsoft is of course playing on the card of Total Cost of Ownership (TOC), with a liiiiiittle bit of (every marketeer's) exageration, but with a tiny bit of valid argument too. They keep winning, albeit often by a small margin. And they even start loosing some rounds, lateley.

<br><br>I was a close witness in some of these battles for the market in Germany. In none of the cases where Microsoft lost, they would have won if there had been more OSS applications ported from Linux to Windows. And in many of the cases where Microsoft still "won" and held on their old fortresses, they would have lost at least some ground had there been more OSS assets ready on Windows.

<hr>
<br><i> "By porting Free Software to Windows one increases the number of valuable applications on that platform, and since application availability is a key factor in operating system usage, we can do the math pretty easily...."</i> 

<br><br>But it is not just math, Aaron. 

<br><br>The idea is to "lower the fence", or "make the gap more narrow" between the MS Windows/Proprietary Software and the Linux/GNU/FOSS worlds. Because if it is too high, or too wide, people will never attempt to climb or jump. What you argue for, is a world where the fence remains really high (but possibly transparent, Windows users can watch what happens on our side), and we build kick-ass applications that makes them want to climb across the fence. Hmmm.... if they are locked onto their system, do they really care to look through the fence? Do they ever see what they are missing? Do you think your "strategy" can really convince people to "change camps" while fences are high, gaps wide and migration costs not low enough?

<br><br>But since you do like math, here is a different angle to the problem.

<br><br>Currently the fence or gap to cross when leaving Windows (in terms of training efforts and costs) is still  pretty high. Do you agree, Aaron? That makes it pretty difficult and very unlikely that a big number of fresh converts will show up next month on #irc.kde.org asking for advice.

<br><br>Assume the fence or gap to cross in becoming a Linux user (in terms of training efforts and costs) is really low. Why should such a fictively low barrier prevent a complete migration more likely than the currently high barrier? Your reasoning fails here. The decision to move is not based on a one-dimensional consideration vector of "just-look-at-the-apps".

<br><br>Assume you can have, on top of Firefox, Thunderbird, OpenOffice.org, a complete KDE (plus Gimp, Inkscape, Scribus and whatever)  working very well on Windows. Are you saying that Linux and the rest of its feature stack (all the OpenSource and Free Software idioms of faith, all its technical advantages as in multi-user capabilities, higher security etc.) are then suddenly turning to become useless in and of themselves?

<hr>
<br><i> "....if Windows has Microsoft applications plus the stable of Free Software apps while desktop Linux/BSD/etc has only the Free Software apps..."</i>

<br><br>This assumption is not valid. There are already (some only, true) proprietary applications (like it or not) available on Linux natively. And more will no doubt come. A lot of Windows apps already work in WINE. Most even work in CrossOver. (WINE and CrossOver may pose technical problems, license costs etc. -- but that was not your point here. Your point was a comparison of the available application stacks and a statement with the wrong assumption that "Linux/BSD have only the Free apps".)

<hr>
<br><i> ".... why would anyone in their right mind switch to Linux/BSD (and incur the training and data migration costs)"</i>

<br><br>Because (of the same reason why) some of them do it even now. Because they may not have the funds to finance a <b>*complete*</b> transition <b>*on the spot*</b>, but they may have enough to do <b>*some*</b> of it <b>*now*</b>.

<br><br>Do you consider that 1, 3, 5 years from now, many of today's computers used in enterprises will be broken and out-dated? So they need to purchase a new set. The question of the OS comes up (again). Next time they'd say: "OK -- we had already well working, but 'foreign' apps like Koffice, Kontact, Konqueror on our old Windows PCs. Our users like it and they need no more training on it. But MS is the same pain for us as the last time we needed to make a decision. Malware, Sypware, Virii are still abounding. If we go for Linux completely, we'll be able to manage, maintain and pay for that this time. And we have left behind the 'Empire' for good, once and for all."

<hr>
<br><i>"... when they already have all the software they need and want right in front of them? They have no reason to. None. Ergo, they won't."</i>

<br><br>Aaron !?

<br><br>Why are you only thinking about private users, which *may* make their own decisions? 

<br><br>Why dont you also consider enterprise environments?

<br><br>Why dont you count in the advantages of such wonderful things like Linux' multiuser capabilities, KDE's Kiosk Mode and Network transparency, the platform's clustering features, NX application servers and other stuff? These surely won't be ported easily, and they would still be attractive enough to get users and decision makers to cross the (now lower) fence.

<hr>
<br><i>"To see how this plays out, let's look at an example ripped from today's headlines: FireFox.
.... FireFox: Enabling Windows Users.. To Use Windows"</i>

<br><br>True.

<br><br>But also true: get Windows users amazed about the power of the Free Software movement. Sow doubt into their minds questioning the superiority of Microsoft's technology. Make them feel at home when using Free Software. Inject more doses of FOSS into enterprise software distribution mechanisms. Give us the advantage to argue "If you switch to Linux completely, you have even more benefits. Try it, it is not more difficult than your switch to Firefox was." 

<br><br>Remember, it once started all with the lone Apache/Samba intraweb server that "just worked", many years ago, when that one "rebel" administrator installed it (possibly behind the back of the boss) to quickly solve a problem?

<br><br>And you surely don't believe that most current Windows/Firefox users would run Linux by now, had Firofox not solved their Internet Explorer problems? You probably dont even think 10% of them would, do you?

<hr>
<br><i> "I think we can all agree that FireFox gives Windows users a way out from the security nightmare and feature desert that is Microsoft's Internet Explorer."</i>

<br><br>Yes, we can.

<br><br>So what?

<br><br>It also cracks a huge glaring breach into the Microsoft giant's stronghold. It exposes their false presumption, that they had won "The Browser Wars" (forever). To users and IT bosses it shows that there are good solid functional apps out there which don't originate in Redmont -- which don't even originate in a commercial software development environment.

<hr>
<br><i>"... Meanwhile, Microsoft is not porting any applications to Linux/BSD, nor will they start to do so anytime soon." </i>

<br><br>I tend to agree. So what? 

<hr>
<br><i>"And so the application imbalance begins..."</i>

<br><br>There are 2 sides to this, don't you see? Microsoft can't rely on that asset Internet Explorer any more. Complete companies (mostly small ones at that) will start to write their HTML and web applications "Optimized for Web Standards" now, not just "Optimized for IE". The rest of them will start at least to lurk over their shoulders to not miss their last chance to switch. Users and IT Specialists and Windows Network Administrators and CIOs have had (or will be having) their "eye-opener moment". Many will have a more open mind now when it comes to "using Free Software".

<br><br>I tell you a little secret. A recent desicion within my employer company was very favorably influenced by the whole of the Firefox public hoopla. That decision was to go for Apache/Subversion for a newly introduced software version control system, instead of Visual Source Safe. You don't believe? I can't help then. But I was present at all the debates, which were very short. One year ago, the outcome would have been different.

<hr>
<br><i> "Survey people using FireFox on Windows. How many of them are saying,  'I'm so impressed I'm going to switch to a Free Software desktop.' Virtually zero."</i>

<br><br>True.

<br><br>But Firefox is only a few drops into the bucket. And Firefox is not a complete Free Software desktop either. Most still have not yet seen such a deskotp. What I say is, "Lets add more drops." We are only at the beginning now. And no process shows the same results at its beginning as it shows at its conclusion.

<hr>
<br><i>"Too many of us in the Open Source community naively expect people to draw conclusions that today's technology consumers are not motivated, let alone empowered, to make."</i>

<br><br>True.

<br><br>But this also happens outside of this debate.
<br><br>
Your own drawing of conclusions here is not sound either. It is too one-sided, too inflexible and too much based on mere assumptions. It is to a big part circular, too. Is makes as a first assumption "Users decide by themselves which OS to use based on the available application stack, so we should keep it low", goes on to assume that porting OSS from Linux to Windows would give them an even more complete applications stack and goes on to conclude.... the assumption. "Users decide to stay on Windows because some evil- or wrong-minded developers want to port their apps to Windows." Hmm... 

<hr>
<br><i> "The more software we port to Windows the more we reinforce this application availability imbalance and strengthen the user's inertia to stay on Windows."</i>

<br><br>May be valid, in parts.

<br><br>But <b>also</b> valid is this: The more software we port to Windows... 
<ol>
<li>....the more we proof to the user and his IT boss that there are no astronomic costs for re-training involved if they switch oompletely.</li>
<li>....the more we make him familiar with the world outside of the Windows he normally stares at.</li>
</ol>
<hr>
<br><i> If users had to make a choice between Windows or Linux (or BSD) when it came to getting access to better applications they would find they had a motivation to switch. And switch they would."</i>

<br><br>Hmmm, hmmm. Just exactly how do you want to make them aware of the "better applications", if there remains a Chinese Wall, plus an Iron Curtain between the platforms?

<br><br>"Switch" a lot of them would <b>like</b> already. But they can't the gap is too big still. It is the foreign apps plus the foreign OS which add up.


<hr>
<br><i> "Consider: why do you think Apple invests so heavily in their iApp series of programs? and why was Apple so hesitant to port iTunes to Windows? "</i>

<br><br>I am not sure if I get the point you want to make here. (And I also dont know the 'iApp' programs nor have I bothered to google for them....)

<br><br>But I have seen the reports which say that a not-to-be neglected part of their recent iPod sales and iTunes software downloads went to "never-before-Apple"/currently-on-Windows users, many of whom now even consider to buy a Mac as their next computer. Have you seen them too?

<br><br>Of course it is somewhat different, in that Apple is a commercial company (using even money invested by MS; hehe...). But it proofs (if true; do you doubt it?) that "teasers" for a different OS platform can be successfully deployed to increase the likelyhood for users to switch to one's own.

<br><br>Or look at Novell. They are a company which is now doing an internal migration of their own employes. Their workforce were previously on Windows, and are now directed to "eat their own dogfood".

<br><br>It would be interesting to see some first-hand accounts about Novell's internal migration progress.

<br><br>But I dont think they do it in one go. I think they are very glad that they can do some of the moves in different stages. And I strongly have the conviction (without any actual evidence), that one of their first item actions was to install onto all Windows PCs Firefox and OpenOffice.org.

<hr>
<br><i> "This implies that there's a downside for Open Source when users stick with Windows. What is that downside? .... As long as Microsoft can keep people on Windows they will have the necessary time to improve their applications and more importantly the supporting software stack. That stack includes the kernel, system libraries and application components that enables their own software to integrate and perform better than the competition's. They used this before to root out competitors like Lotus 1-2-3 and DR-DOS and they'll do it again. It's too easy and too obvious not to."</i>

<br><br>Technically -- yes.

<br><br>But... a big "but": What would the outcome before the various courts of the world be this time?

<br><br>How would users and corporate decision-makers react this time? -- Do you assume history merely repeates itself?

<hr>
<br><i> "As FireFox takes market share from IE, Microsoft will fight back not just with improvements to Internet Explorer but with investments in operating system and 'desktop environment' development that will give Internet Explorer unanswerable advantages over FireFox."</i>

<br><br>But they (MS) did it already. 'Unanswerable' advantages they had already. For a few years. They thought the fight was over. But now it returns on them again.

<hr>
<br><i> "People will once again switch back to IE and FireFox will be Netscape all over again. Worst, the Mozilla project will not be able to muster a meaningful response.... "</i>

<br><br>Last time Netscape was not only a browser. It was a commercial company. And MS was able to ruin that company with their sheer financial superiority and power. This time they struggle the OpenSource community. Have you forgotten that, Aaron? This time they need to think of something different. They do not yet have a response to this new challenge.

<hr>
<br><i>... because they do not have access to the playing field (the underlying software stack) that Microsoft will have beat them on. You can not compete on a playing field to which you do not have access."</i>

<br><br>OK, granted.

<br><br>But the argument counts both ways. Does Microsoft have access to 'our' playing field? Do they have a proven strategy to fight the rise of the Open Source and Free Software movement? (OK -- they are currently trying different things. Let's debate that at another occasion.)

<hr>
<br><i> "This is why FireFox needs Linux/BSD to survive in the long term: we aren't trying to kill FireFox, but most importantly we don't have a way to kill FireFox even if we wanted to. "</i>

<br><br>Agreed.

<hr>
<br><i> "On Windows it's a very different story. As long as Microsoft controls the Windows technology stack..."</i> 

<br><br>Didnt they loose part of that control just now by people overturning the default IE web browser and setting it to be Firefox? 
 
<hr>
<br><i> "....(and they will never give that control up) they will have the ultimate ability to out-compete any 3rd party software on Windows that they wish to. Because the pain of millions of Windows users is eased by FireFox, Microsoft has been given the space they need to improve their software without people leaving the Windows platform while they perform the needed improvements."</i>

<br><br>OK -- people dont *need* to leave the Windows platform yet. And they didnt "need" before Firefox' release either, because it is still a trickle of converts, not a big drove. But chances are, it becomes easier in many people's minds in the future once they started to Digest a switch to OOo and Firefox/Thunderbird.

<hr>
<br><i> "By porting software to Windows,..."</i>

<br><br>Just *how* much work would that be, exactly?

<br><br>I am not talking about a complete port of the OSS stack. I am mainly thinking about going for some "low hanging fruits" first. Is it true that gaining a fully working Kontact/KMail suite is only dependent on one releatively small library whose authors didnt agree to a license change? And a "write from scratch" would involve "only" 6 months payment for 2 fulltime developers?

<hr>
<br><i>"... we eliminate the majority of the competitive advantage of Free Software desktops in the eyes of the overwhelming majority of consumers while Microsoft has all the rope they need to shut the door once again on us."</i>

<br><br>They never did it on "us". Last time it was Netscape. That was a commercial company. There was no OpenSource/Free Software movement on the scale there is now. So far, Microsoft hasnt inflicted any decisive defeat on OpenSource/Free Software yet. The Big Battle is still ahead.

<hr>
<br><i> "We have left our flank exposed while expending our efforts improving the position of the unfriendly force."</i>

<br><br>I can't see "our flank exposed". I can see this rather to be a big thorn in the unfriendly forces' side. 

<br><br>And I cant see "improving" the opponents' position by porting OSS software to Windows. Especially if there are people already doing it for their own fun. And especially not if it is all "low hanging fruit". (But of the last point I am not sure of -- someone with more intimate knowlegde of the code should tell us.)

<hr>
<br><i> "Meanwhile We Starve Ourselves...</i>

<br><br>Why? What? Starve ourselves of what? Starving ourselves of active developers working on Linux, maybe?

<br><br>Do you think someone can command anyone to "go work on Windows ports"? You know it can't happen. Only these with the drive and motivation about it will do the work. It is obvious that you yourself and many others wont be part of thses &nbsp; &nbsp; &nbsp; ;-)

<br><br>What you  miss here is a bit of dynamic and creative thinking. You seem to assume that there is a fixed number of developers which do native KDE (or OSS) work. Having some of these go to do "porting" work decreases your way of doing the "head count". What if the persons working on porting wouldnt do any native KDE work at all? What if the "porting" is the only way to gain or retiain their creativity? And what if this work triggers an influx of a whole new layer of developers who come from the Windows platform into our camp? 

<hr>
<br><i> "Making the situation even worse, by keeping people on Windows we decrease the odds of them getting involved and contributing back to the community. This is because the tools necessary to do so are relatively rare on Windows. How many Windows users have debuggers or compilers or even receive awareness marketing on the part of their primary software vendor (Microsoft) to 'Get Involved and Give Back'? Moreover, resources that could be spent making the Open Source desktop environments more compelling are instead being spent on making Windows more compelling in the form of superior applications."</i>

<br><br>You assume that anyone working on a Windows port would automatically be available to work on Linux, if he wouldnt do that Windows port. But what if they are volunteering and inspired <i>only</i> by the Windows port? What if their work recruits other ex-Windows developers to the Linux camp entirely?

<br><br>See the world is not as static, mathemetical and geometric as you seem to assume.....  ;-)

<hr>
<br><i> "Microsoft owes us a big 'thank you' when you think about it: we are giving them the opportunity to react on the playing field they are most effective on.... "</i>

<br><br>Yes we would be heavily disturbing their circles....

<hr>
<br><i>"....while we are limiting our own resources. "</i>

<br><br>No, we wouldnt limit our own resources. Because you cant command any developers to do that work. And I see no-one attempting to issue such a command.

<hr>
<br><i> "This 'strategy' ensures Free Software desktops remain a 5% fringe in the market. "</i>

<br><br>No, it would bring a few key Free Software applications (and not more!) to 100% of the market.

<hr>
<br><i> "This translates to ISV interest in desktop Linux/BSD being kept to a barely noticeable minimum."</i>

<br><br>No, this translates into increased interest for ISVs into the tools, the platform, the brainshare, the manpower and creativity of the OSS community.

<hr>
<br><i> "In turn this means fewer software packages, which in turn means even fewer reasons for people to use Free Software operating systems. Can you hear the dominoes falling as they approach? "</i>

<br><br>Oh yes!  ;-)

<br><br>But which is the last domino the rolling tide is going to ditch ?

<br><br>This is a gigantic domino field. It is not easy to make out where the falling chain heads to...  ;-)

<hr>
<br><i> "Free Software desktop applications on Windows represent a no-win situation for Open Source,... "</i>

<br><br>Yes, if you dont count mindshare. If you dont count stretched out migration scenarios. If you dont count that it lowers the cost for many migration scenarios, If you dont count that some/many migrations will only be possible in the future if the migration-willing companies now can avoid to sign long-term contracts with MS. If you dont count that now MS needs again to invest large sums into IE development (will they do it?) -- a product they had deemed "finished and polished" since 3 years. If you dont count the big publicity this brings to OSS in general.

<br><br>In short: if you prefer to turn a blind eye.

<hr>
<br><i> "...but Open Source desktops on Free Software operating systems do."</i>

<br><br>We are in agreement here.

<hr>
<br><i> "It's now up to us to pick our directions and to pick them carefully."</i>

<br><br>We dont have a Napoleon who sends us into the Russion Winter.

<br><br>We only debate. If a group of deves reaches an agreement by debating, its members may for a period act on that agreement.

<br><br>If others dont support that direction, they dont go along. Thats how it worked so far. That's how the Free Software organism of contributors will continue to work. Sociologists or Biologists may be calling that Self Organization of Organisms.

<hr>
<br><i> "Today, I look at <A href="http://webcvs.kde.org/kdelibs/win/">kdelibs/win/</A> and grow concerned."</i>

<br><br>I see the same and feel delighted....   ;-)

<hr>
<hr>

    
    <b>The Philosophical Summary:</b>
    
    <br><br>We are not seeing the complete piece of a puzzle by staring at one point of it only. We are not seeing the complete picture the puzzle represents, if we are only using one piece to play with. We are not dealing with a static puzzle here -- we have a dynamic motion picture.  It is not black and white, it is not even 256 shades of gray -- it's full-blown color. It is not flat, it is 3-dimensional. It changes over time. It is the real life.
    
<br><br>The Bible said (my own translation from German): "Your Words Shall Be 'Yes, yes or No, no.' Every Other Syllable is Evil". That one sentence can't be applied to all evaluations you make.

<br><br>To me, Aaron's reasoning is way too much contaminated with the limits of formal, strict school of logic, where this school misses the dialectics of a problem and all its facettes.
<hr>
<hr>
    <b>The Simple Summary:</b>
    
    <br><br>It is more difficult to migrate a complete enterprise or organisation to both,
      <ol>
	<li>a completely new stack of applications</li>
	<li>a completely new operating system platform</li>
      </ol>
      at once.
    
    <br><br>It is more easy to migrate a complete enterprise or organisation to one at a time,
      <ol>
	<li>first a few new key applications (we'll never be able to provide the complete stack anyway!)</li>
	<li>and later to a new operating system platform (Linux) which runs the same applications.</li>
      </ol>
<hr>
<hr>
<b>The Executive Summary:</b>

<br><br>*If* there is any sense to put coordinated organized work of KDE developers behind "porting applications to Windows" (or even porting frameworks or the complete DE), it is most important to choose the strategic ones that make a lot of business sense to enterprises (not home users, though some of them would appreciate that too): Kontact/KMail, KOffice, Konqueror, Quanta,.... One can probably rule out the complete KDE application stack.
<hr>
<hr>
<b>The Pragmatic Summary:</b>

<br><br>It all depends just how much exactly the effort amounts to in porting KDE apps or the complete DE to Windows, and how much fun the respective developers will have with it during the endeavour.

<br><br>It all depends on wether there will be a few energetic individuals tackling this. If there are, we'll see what happens. If there aren't, the idea is dead. And life carries on the way Aaron would prefer. &nbsp; &nbsp;  &nbsp; &nbsp; ;-)
<hr>
<hr>
<b>The Wishful Summary:</b>

<br><br>Maybe, maybe, it depends on a 'rich' sponsor stepping forward and paying the 6 months or so fulltime development for 2 persons which would accomplish the "little rest" (one well-defined library) that needs to be re-written for Windows under an LPGL-like license to make "Native Konatact for Win32" happen.
<hr>
<hr>

