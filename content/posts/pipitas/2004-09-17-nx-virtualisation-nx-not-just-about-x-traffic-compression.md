---
title:   "NX virtualisation  (NX is not just about X traffic compression)"
date:    2004-09-17
authors:
  - pipitas
slug:    nx-virtualisation-nx-not-just-about-x-traffic-compression
---
Brad is <a href="http://blogs.kde.org/node/view/620">commenting</a> about <a href="http://info.pittsburgh.intel-research.net/project/isr/"> a research project</A> that does <I>"Internet Suspend / Resume"</I> of user sessions:
<br>
<I>"...interesting idea - they basically envisage a thick-client model, running on a virtual machine (VMWare in their tests) which can suspend. The suspend state then gets shipped over a network connection to another machine where it can be “resumed