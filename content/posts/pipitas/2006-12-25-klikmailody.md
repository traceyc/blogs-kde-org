---
title:   "klik://mailody"
date:    2006-12-25
authors:
  - pipitas
slug:    klikmailody
---
<p>I've created a (quick'n'dirty) "klik recipe" for <a href="http://www.mailody.net/">mailody 0.3</a> (using the <a href="http://ftp.gwdg.de/pub/linux/misc/suser-guru/rpm/packages/Network/mailody/mailody-0.3.0-1.guru.suse93.i686.rpm">SUSE 9.3 RPM</a> made by Guru, to ensure maximum portability).

<p>Mailody embedded into one single file, <i>mailody.cmg</i>, with 430 kByte size, running on most current Linux systems; you can start the klik-ed mailody even from CD, or run it from a USB stick....

<p>I tested the new bundle on a SUSE-10.0 and on a Debian Sid system, and it works for both. So chances are, that it works also on most other systems out there (Of course, they need the kdelibs and Qt installed)!

<p>So if you wanna run a quick'n'clean test of mailody, just visit the <a href="http://mailody.klik.atekon.de/">klik page for mailody</a> and click on that blue round button with the <a href="klik://mailody">klik://mailody</a> -link....

<p>Remember, you need the <i>klik client</i> installed to run klik .cmg files. This is a 30 second affair: just run <tt>"wget klik.atekon.de/client/install -O -|sh"</tt> and follow instructions.

<p>See also the <a href="http://klik.atekon.de/wiki/index.php/User%27s_FAQ">klik User's FAQ</a> for any questions you may have about klik.

<!--break-->
