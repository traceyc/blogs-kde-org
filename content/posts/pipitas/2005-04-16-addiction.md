---
title:   "Addiction"
date:    2005-04-16
authors:
  - pipitas
slug:    addiction
---
Canllaith seems to <a href="http://blogs.kde.org/node/view/979">start liking to fly gliders</a>.<br><br>

<img  align="right" hspace="6" vspace="3" src="http://www.dhv.de/typo/uploads/pics/flugmarmolada_03.jpg" class="showonplanet" />

Hey, I can certainly understand that! This experience is sooo just incredible.... feel how the air is able to carry the weight of your wooden (or plastic) bird... hear the wind's always present noises change with the flight state, mixing with the excited beep-beep-beep of the vario, once the pilot has successfully centered his circles in the thermal... get surprised by the centrifugal forces pressing you into your seat in tight curves... Hmmm, and feel your stomach and guts all at once jumping close to your throat, just because the pilot decided to be a little mean to his only passenger and do a sudden nose-dive.<br><br>

But watch out, it certainly is addictive, once you survive your first or second flight as a passenger having liked it!<br><br>

I did it too, eleven years ago. I even started to take a course to become a glider pilot. After about 10 flights during the winter with me in the front seat, and the instructor in the back, a collegue convinced my to accompany him to his first "trial day" of <A href="http://en.wikipedia.org/wiki/Paragliding">paragliding</A>. <br><br>

Well, that was even more addictive! What I liked much more with paragliding, was that there was very little "overhead" to get me up into the air. I am fascinated by the perspective of only needing to shoulder my knapsack with all the equipment (including the "boneless aircraft"), scramble up a suitable mountain slope for 1 or 2 hours (should I not want to take the chair lift), get ready to take off within 5 minutes, and go.

<img  align="left" hspace="6" vspace="3" src="http://www.dhv.de/typo/uploads/pics/dynamikselbstwettkampf.jpg" class="showonplanet" /> If conditions are fine, the flight can easily take 2 or more hours. You may have only climbed the slope half-way or less -- but now you can let the wind carry you up, soaring back and forth on the mountain's slope until you even leave the pinnacle underneath you. I will always remember the day in the early summer of 2000 when I had left the "High Dachstein" peak (Austria. 2995 meters or 9826 feet above sea level) nearly 500 meters underneath me.<br><br>

Compared to that my "10 hours of work duty in exchange for 1 hour of flying" arrangement with the local glider club suddenly wasnt appealing any more to me. Also, to really take off, I had to have at least 5 other people helping me to prepare the bird, work as the motor winch operator, function as the flight commissioner and do other things to get me up.<br><br>

OK -- the trip to the mountains also takes time, and there are often conditions where gliders could still cope with stronger winds and we paragliders have to watch them enviously from the ground.... But I still give the independence feeling the preference. With a paraglider canopy, if conditions are well enough, I *can* get up very high without any external human help, just using my own muscle to get to a good starting slope, and the sun-driven thermal. For a glider, I always need friends and colleagues who help me to gain some initial height.<br><br>

Gliding and paragliding certainly give you a completely different perspective in the face of all earthly or personal things and problems. After your first flight where you have handled everything on your own (in gliding, even with the instructor still in the backseat), you will have become a different being.<br><br>

For all those of you, who want to try gliding at least from a "Flight Simulator" perspective, have a look at <a href="http://www.silentwings.no/">Silent Wings</a>. Silent Wings, a gliding and soaring simulator software, was released only 4 weeks ago. Its sceneries are gorgeous, and provides very detailed and a high resolution with real-time rendering as you fly, and its simulations even include aerotowing exercises. Its weather, aircraft and aerodynamics simulations are very realistic. <A href="http://www.silentwings.no/article/articleview/31">Various movies</A> (AVI) for demo available. Yes, it does run on Linux -- and no, it is not a late port from Windows. Yes, its primary development platform is Linux. No, it is not Free, but commercial/proprietary software, costing you 69 EURO -- but you can have a fully functional 2 week demo for free. (Check out the <A href="http://www.silentwings.no/article/articleview/14/1/2/">hardware requirements</A> first!). For someone like Canllaith pondering to take a glider pilot's training, Silent Wings certainly is a worth while investment.<br><br><img  align="center" hspace="6" vspace="3" src="http://www.silentwings.no/ezimagecatalogue/catalogue/variations/97-700x9999.jpg" class="showonplanet" />

<hr>
<small><b>Note:</b> the glider pic is from the Silent Wings website and shows the simulation quality, the paraglider pics are real life ones from http://www.dhv.de/</small>

<!--break-->