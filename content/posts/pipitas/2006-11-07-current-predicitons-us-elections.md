---
title:   "Current predicitons for U.S. elections"
date:    2006-11-07
authors:
  - pipitas
slug:    current-predicitons-us-elections
---
Current predictions for new U.S. Senate and Congress after tomorrow's (Tuesday) elections, based on some polls:

<a href="http://www.electoral-vote.com/"><img src="http://www.electoral-vote.com/evp2006/Icons/ev.png" alt="Click for www.electoral-vote.com" border="0" height="144" width="144"></a><a href="http://www.electoral-vote.com/"><img src="http://www.electoral-vote.com/evp2006/Icons/ev-house.png" alt="Click for www.electoral-vote.com" border="0" height="144" width="144"></a></p><br clear="all">

Only 33 out of 100 Senate seats are up for election, but all 435 Congress ("House") seats are. (Click graphic for more details.)
<!--break-->
