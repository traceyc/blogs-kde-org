---
title:   "Ubuntu's \"No Open Ports!\" policy questioned by Avahi developer"
date:    2006-07-27
authors:
  - pipitas
slug:    ubuntus-no-open-ports-policy-questioned-avahi-developer
---
<p>Thanks, Lennart!</p>

<p>Very <A href="http://0pointer.de/blog/projects/zeroconf-ubuntu">well written pleading</A>.</p>

<p><A href="http://avahi.org/">Avahi</A> is not the only victim. Ubuntu's "no ports open!"-policy has (along with some other, similar decisions) <A href="http://blogs.kde.org/node/2117">also</A> <A href="http://blogs.kde.org/node/1899">badly</A> <A href="http://blogs.kde.org/node/2064">hurt</A> <A href="http://blogs.kde.org/node/2076">CUPS</A>, <A href="http://blogs.kde.org/node/2106">and</A> <A href="http://blogs.kde.org/node/2121">considerably</A> <A href="http://blogs.kde.org/node/2138">reduced</A> out-of-the-box usability and comfort for users. </p>

<p>CUPS servers use UDP broadcasts to announce available and shared printers to their potential CUPS clients on the same LAN. CUPS clients use a setting of <tt>"Browsing On"</tt> in their cupsd.conf to make them notice these broadcast UDP announcements. Note, that this setting on its own does *not* make a cupsd announce his own local printers (that would require to additionally specify <tt>"BrowseAddress"</tt>), and does *not* turn it into a CUPS server!. </p>

<p>CUPS.org ships default config settings of "Browsing On" and BrowseAddress commented out. This makes perfect CUPS clients, which work out of the box, and enabling them to print with "zero configuration" (no printer installation required, no client installation necessary either) should they discover a CUPS server near them.</p>

<p>(Some GUI apps however stupidly and wrongly rely on a valid "client.conf" to tell them which CUPS server to use, and will still not see the printers. But an <tt>"lpstat -p"</tt> would show a list of available printers regardless. KDEPrint and kprinter will work out of the box as well. The "client.conf" file is meant for spoolerless printing, or for people who understand what they do; it is not meant by the CUPS developers to be the means for all clients to use.) </p>

<p>The justification of "no open ports!" was used to disable zero configuration printing for Ubuntu CUPS clients, and likewise, I'd say that reasoning was thought out by someone who didn't have much experience with CUPS networking at the time it was originally imposed. </p>

<p>And if this policy is kept up for much longer (now that the responsible people know better how CUPS really works), then they'll do it because they value a real life ease of use for their users less than they value some theoretical level of security. (Yes, it is a weighing+balancing between these two goals. Currently, the practical result is more like "you can't use your system for printing, but at least it is super-secure". Not something that will make world domination more easy...)</p>

<p>Cheers,<br>Kurt  <br>[ now expecting to be accused of calling other people "ignorant"... ]</p>

<!--break-->