---
title:   "More klik E17 screenshots"
date:    2005-09-27
authors:
  - pipitas
slug:    more-klik-e17-screenshots
---
<p>The IT news site <A href="http://www.Golem.de/">Golem.de</A> has <A href="http://www.golem.de/0509/40655.html">picked up</A> the topic of klik now. It is very obvious that they actively tested klik themselves (although they do not write so), not just paraphrased other people's writings, and that they were mainly intrigued by the <A href="klik://enlightenment">klik://enlightenment</A> option. Proof: a series of <A href="http://scr3.golem.de/?d=0509/e17&a=40655">8 very nicely done screenshots</A>.</p>

<p>&nbsp;&nbsp;<a href="http://scr.golem.de/?d=0509/e17&a=40655&s=1"><img src="http://scr3.golem.de/screenshots/0509/e17/sc01k.jpg" border="1" width="120" height="85" alt="Screenshot-1" />&nbsp;&nbsp;&nbsp;&nbsp;</a><a href="http://scr.golem.de/?d=0509/e17&a=40655&s=2"><img src="http://scr3.golem.de/screenshots/0509/e17/sc02k.jpg" border="1" width="120" height="85" alt="Screenshot-2" />&nbsp;&nbsp;&nbsp;&nbsp;</a><a href="http://scr.golem.de/?d=0509/e17&a=40655&s=3"><img src="http://scr3.golem.de/screenshots/0509/e17/sc03k.jpg" border="1" width="120" height="85" alt="Screenshot-3" />&nbsp;&nbsp;&nbsp;&nbsp;</a><a href="http://scr.golem.de/?d=0509/e17&a=40655&s=4"><img src="http://scr3.golem.de/screenshots/0509/e17/sc04k.jpg" border="1" width="120" height="85" alt="Screenshot-4" />&nbsp;&nbsp;</a>
</p>

<p>&nbsp;&nbsp;<a href="http://scr.golem.de/?d=0509/e17&a=40655&s=5"><img src="http://scr3.golem.de/screenshots/0509/e17/sc05k.jpg" border="1" width="120" height="85" alt="Screenshot-5" />&nbsp;&nbsp;&nbsp;&nbsp;</a><a href="http://scr.golem.de/?d=0509/e17&a=40655&s=6"><img src="http://scr3.golem.de/screenshots/0509/e17/sc06k.jpg" border="1" width="120" height="85" alt="Screenshot-6" />&nbsp;&nbsp;&nbsp;&nbsp;</a><a href="http://scr.golem.de/?d=0509/e17&a=40655&s=7"><img src="http://scr3.golem.de/screenshots/0509/e17/sc07k.jpg" border="1" width="120" height="85" alt="Screenshot-7" />&nbsp;&nbsp;&nbsp;&nbsp;</a><a href="http://scr.golem.de/?d=0509/e17&a=40655&s=8"><img src="http://scr3.golem.de/screenshots/0509/e17/sc08k.jpg" border="1" width="120" height="90" alt="Screenshot-8" />&nbsp;&nbsp;
</a>
</p>

<p>The Golem folks seem to like KDE. Their <A href="http://scr3.golem.de/?d=0509/e17&a=40655&s=1">first shot</A> shows an enlightenment window only (seems to run on top of some other system which is not shown), that has two browser windows open: one Firefox, one Konqueror. <A href="http://scr3.golem.de/?d=0509/e17&a=40655&s=4">One other pic</A> shows, they actually run it on (K)ubuntu -- good to know that our klik package works well there too ;-) Their <A href="http://scr3.golem.de/?d=0509/e17&a=40655&s=8">last illustration</A> shows a resized E17 window on top of a full blown KDE desktop.</p>

<p>Now if this E17 klik package proofed so easy to handle and install, even for the press! -- maybe I should seriously think about preparing a "<A href="klik://kde35">klik://kde35</A>" package ?</p>

<p>Would there be any interest at all?</p>


<!--break-->