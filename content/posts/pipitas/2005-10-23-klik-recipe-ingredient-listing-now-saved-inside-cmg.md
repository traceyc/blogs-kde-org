---
title:   "klik recipe: ingredient listing now saved inside the .cmg"
date:    2005-10-23
authors:
  - pipitas
slug:    klik-recipe-ingredient-listing-now-saved-inside-cmg
---
<p>One of the improvements to klik that has taken substance over the weekend is this:</p>
<ul>
<li> each future .cmg created on the client side by executing the klik recipe for the bundle now includes <i>"file.list"</i>. This enumerates the input files and their URL source that where used to cook up the final .cmg dish (hrmm..., maybe we should have named it <i>"ingredients.list"</i> to stay in tune with the recipe theme).</li>
</ul>

<p>This is in the interest of making it easier, even months after using a .cmg for the first time, to track where the files came from. It will also enable current and future klik recipe maintainers to get their job done more easily.</p>

<p>To see the file, you'll have to mount the .cmg ("<tt>cd; mkdir mountpoint; mount -o loop Desktop/SomeNew.cmg mountpoint/</tt>") and then just look at it ("<tt>cat mountpoint/files.list</tt>"). Or, just access the file.list while one or more of your klik-ed apps are running: "<tt>ls -l /tmp/app/*/*.list</tt>").</p>
<!--break-->"