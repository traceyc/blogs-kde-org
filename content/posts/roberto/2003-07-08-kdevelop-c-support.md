---
title:   "KDevelop C++ support"
date:    2003-07-08
authors:
  - roberto
slug:    kdevelop-c-support
---
Finally some news about KDevelop C++ support and Code Completion.<br>
Yesterday i've made many changes to cppsupport, now we have a new and more simple to maintain <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdevelop/parts/cppsupport/cppcodecompletion.h">code completion</a> engine :)  <br>
of course the code is not finished yet. for instance, the 
support for namespaces still sucks, but i hope to finish it soon.<br>
<br>
Now i'm experimenting with QDataStream and Tags(kdevelop/lib/catalog), i'm thinking to
replace our persistant class store with a QDataStream based one. the point is 
berkeley DB isn't so cool as i've expected! the file size of the PCS db is too big and it's incredible
slow when you have many items stored.