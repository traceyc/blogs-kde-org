---
title:   "Automake and unsermake"
date:    2003-07-09
authors:
  - roberto
slug:    automake-and-unsermake
---
Great! <br>
now KDevelop can be compiled with both Automake and <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/unsermake">Unsermake</a>. That's a great news, because Unsermake is a very nice replacement of Automake, and i'm sure it will be another killer feature of KDE.<br>