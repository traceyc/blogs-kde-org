---
title:   "Hardware blues"
date:    2005-06-21
authors:
  - simon edwards
slug:    hardware-blues
---
Setting up my Athlon64 mobile I realised why I should stick to software: hardware is frustrating, and I'm just not particularily good at it. Here is the short list of problems I've come up against getting all this to work.

1) Had to run out and buy a new gfx card. ("What do you mean my 1998 AGP gfxcard won't work in a motherboard from the year 2005?")

2) The board didn't want to give me a display. It just wanted to beep at me. After pulling the board out, reconnecting everything, and testing with a  known good gfx card, it still wasn't working. Then I stumbling on the error code info in the manual, which directed me to the memory module. Apparently I 
wasn't using enough extreme violence to fully insert the DIMM.

3) Lot of trouble getting back on the net. The cable modem will only talk to one particular ethernet address/NIC. I knew this but couldn't get the bloody NIC to work in the new motherboard. I had to switch to plan B and by one o'clock in the morning I had cobbled together some kind of internet access involving the old computer where the NIC did work. The GF wasn't too happy about losing internet connectivity either. Turns out she is more interested in having five nines than 24 carets. The real problem was a driver and motherboard incompatibility/bug issue.

So after those headaches things are kind of working OK again.

The good news is that I now have a gfx card with dual outputs. Which means I now have a direct interest (and the hardware) to work on dualhead support in <a href="http://www.simonzone.com/software/guidance/">Guidance</a>.

The bad news is that I'll probably be without internet access at home in a couple weeks time when I move house and the cable (unsurprisingly) screw up my moving my network connection.

had to get that off my chest.