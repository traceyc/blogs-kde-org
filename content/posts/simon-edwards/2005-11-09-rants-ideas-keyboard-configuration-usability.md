---
title:   "Rants & Ideas: Keyboard configuration usability"
date:    2005-11-09
authors:
  - simon edwards
slug:    rants-ideas-keyboard-configuration-usability
---
<p>
I let on somewhere that I'll like to see keyboard configuration in KDE be greatly improved, and since then a few people have been trying to catch me, hold me down and get me to elaborate. So I'm going to shotgun this one out into blogsphere(?) and see what happens.
</p>
<p>
My first problem with how things are now is simply that everytime I have to configure my keyboard setup I have to guess as to what model my keyboard actually is. It's pretty generic so I go for "Generic x", then I usually do a quick count of the keys to work out what 'x' should be. (Does the caps lock key count? and those funny windows keys??) The next problem is do I have, or do I want, dead keys? I know that if I choose something wrong here then some of my modifier keys start "acting up" and doing things which I don't want.
</p>
<p>
Problem number two. Which layout do I have? It doesn't actually say anywhere on the keyboard what the layout is. But it was made for Honeywell Australia in Mexico. So I compromise and go for US English. (But Australia uses British english spelling, does that mean we also use the British layout???)
</p>
<p>
Assuming that went OK. The next problem is how can I type accents, umlauts and all that other stuff that you need in europe. It wasn't until one of the KDE.nl'ers threw a link to a HOWTO on the IRC one day that I found out that you could even have a cool compose key in KDE. 
</p>
<p>
Now, granted a lot of people are thinking "Simon, you're an idiot. You're not even trying", my real point is how the hell are normal people (not computer geeks!) supposed to correctly configure their keyboards? How are people supposed to work efficiently on configurations that barely match the language(s) they use? The situation isn't much better on that other operating system. I've seen people at work who have memorised those weird ass Alt+123 codes for all of the accented etc characters that they need. It ain't pretty folks.
</p>
<p>
What is the solution? May I dream out loud?
</p>
<p>
Firstly I want to be able to choose my keyboard model from a bunch of pictures/diagrams of keyboard models. I should be able to see what is written on each key in the diagram. The diagrams should also highlight which parts of the keyboard I should be paying attention to in order to correctly match my model, and which parts can vary. (for example, the position of the pipe sign often varies on US english keyboards).
</p>
<p>
Once I've workout which model I've got in front of me, the next task is to choose the layout. And yes, I want more diagrams here too. I want to _see_ what I am choosing. I want to see every key and what it does, laid out in front of me.
</p>
<p>
Next, the current Keyboard layout module in kcontrol does a poor job of explaining that you can have multiple layouts which you can then switch between. This needs to be better explained, and I'm not talking about a user guide. I mean in the UI itself.
</p>
<p>
The whole "Xkb" tab needs to be broken down into understandable chunks that also explain what they do. I know there are a lot of people out there who would love to have a working compose key, if only they knew that the functionality even existed and how to turn it on.
</p>
<p>
What I'm discussing here is of course a hell of a lot of work. The best part is that creating this ultimate keyboard configuration tool wouldn't need much programming. It would need a lot of work collecting info about all of the different layouts and models, and preparing all of the diagrams and pictures needed. Not to mention a good looking at by a someone with time and usability skills. But it is the perfect task for any non-coding developers out there who want to get involved. Any takers?
</p>
<p>
(For bonus points to anyone still reading. How the $@%# do you type the euro sign?)
</p>