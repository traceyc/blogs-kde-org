---
title:   "Guidance, desktop/web usability evolution"
date:    2005-12-15
authors:
  - simon edwards
slug:    guidance-desktopweb-usability-evolution
---
<p>
<b>GUI updates in Guidance</b><br />
I was going through the usability report about Guidance a month ago and saw this comment at the end of the report:
</p>
<p>
<i>4.3 Checkbox feedback<br />
When the user checks or unchecks the checkbox "Start during boot", the corresponding entry in the list on the left side is updated (e.g. from "yes" to "no"). This is hard to notice, since the mental focus is on the checkbox, not on the list. Therefore, a stronger visual indication about the change is helpful.</i>
</p>
<p>
(Report is here BTW, http://www.openusability.org/reports/?group_id=166 )
</p>
<p>
Jan Muehlig also suggests that the text in the listview change font/colour, or flash or something to indicate that it has changed. At the time I thought "yeah true, it is hard to notice, mind you every other desktop application ever does this the same way" (e.g. updates parts of the screen without explicitly drawing extra attention to it). After that I put the issue on my mental "nice to have" feature list and didn't think too much of it. As far as I know KDE and Qt don't really directly support a blink/flash change feature.
</p>
<p>
<b>The Yellow Fade Technique</b><br />

Now, I'm a web application developer during the day and earlier this week I saw a 3 hour presentation in Antwerpen, Belgium about AJAX. AJAX, for those who don't know, is shorthand (and buzzword) for doing spiffy things inside the browser. Things that most people thought were confined to desktop applications. (Think Google Maps, Gmail and Google Suggest.) During this presentation they mentioned the "Yellow Fade Technique". As it turns out this is one usability solution to same problem that Jan mentioned above in Guidance. How do you let the user know that part of the screen has been changed. The "Yellow Fade Technique" simply draws a yellow background behind the part of the screen/page that has been updated. The yellow background then quickly fades back to the normal background colour.
(This article here talks about it http://www.lukew.com/resources/articles/ajax_design.asp, see "Communicating Change" half way down the page)
</p>
<p>
The question is now, does this or similar functionality exist in KDE/Qt? and if not, why not?
</p>
<p>
<b>Desktop and web usability evolution</b><br />
Despite the more constricted programming environment on the web, it appears that usability techniques may be evolving faster there than on the desktop. The lack of strict usability guidelines and techniques on the web can be a curse, but it might also be a blessing. A lot of different things are being tried out on the web. Much of which is mediocre or worse, but still some new and good techniques are being invented. Another good example would be form validation. Most desktop applications throw up a popup box if you click on "OK" and the dialog's field are not correctly filled in. On the web we are seeing things like compulsory fields being clearly marked with red stars etc, and invalid fields being visually highlight to show their invalid state. I don't think I've seen much of this in desktop applications, and again I ask why?
</p>
<p>
In my opinion, what is helping drive this is that companies can now quickly collect feedback about what works and what doesn't work. An example given at this presentation in Antwerpen is of an online shop (forgot the name) that had a way for users to score products out of ten. The user had to click on a link and go to another page of something. By changing the way this feature worked so that people could give a product a score in just one second and without leaving the current page, they were able to increase its use by an order of magnitude (10x). That is important data for a business to collect. Being able to quantify the effect of a usability improvement helps separate the good stuff from the bad.
</p>
<p>
Has anyone else seen any techniques or tricks out there on the web which might also belong on the desktop?
</p>