---
title:   "GCDS and Python in KDE 4.3"
date:    2009-07-04
authors:
  - simon edwards
slug:    gcds-and-python-kde-43
---
A small status report about the Python bindings and support in the almost arrived, KDE 4.3. All of the APIs have been updated of course and I've added support for polkit-qt. This makes it possible to write applications and configuration tools which feature the much needed (and working) "Administrator" button.

Yes, I'm down here in Gran Canaria with the rest of the geeks. It's shorts and T-shirt weather 24/7, even when it is cloudy and "bad" like it is now. (Actually this is better. The full on sun is a bit too much.) There are a lot of old familiar faces around and a lot of new ones. Speaking of which if you are at GCDS and are working with Python on KDE then please come and find me and introduce yourself. (I'm the geeky one with glasses with the Kubuntu T-shirt on.) I'm curious to know what you are developing, what is working well in Python bindings, what is not and what you would like to see in the future. Help me help you.
<!--break-->
