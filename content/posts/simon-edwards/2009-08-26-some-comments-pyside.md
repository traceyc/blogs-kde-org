---
title:   "Some Comments on PySide"
date:    2009-08-26
authors:
  - simon edwards
slug:    some-comments-pyside
---
To be honest I'm not all that happy with the current situation. Riverbank Computing, basically Phil Thompson, has done an excellent job developing SIP and PyQt over the last 10 years and providing a Free Software (GPL) version which PyKDE is built on. Phil has also done an excellent job of providing answers to my queries, basically as a free service to KDE and the FOSS community. Having two competing Python bindings for Qt is a waste of resources and is generally disruptive to the community at large. Seeing the future of Riverbank and the good working relationship between it and KDE jeopardised is not something that appeals to me. I am disappointed that no kind of cooperative agreement could be reached between Nokia and Riverbank Computing.

On the technical side, PySide is just starting out and has a long way to go before it is as mature as PyQt and is a viable replacement for it. This is only natural. Bindings development is not easy -- just ask Richard Dale, he's been at it for years. (As a point of interest Nokia could have saved themselves a lot of effort by building on top of Richard's Smoke libraries/tools.) It will be interesting to see how PySide progresses and what happens in Python / Qt community. In the short term at least, PyKDE will continue to be developed on the mature SIP/PyQt.
<!--break-->
