---
title:   "First laptop: Acer Aspire 5612"
date:    2007-02-20
authors:
  - simon edwards
slug:    first-laptop-acer-aspire-5612
---
So some money come my way and I finally caved in and bought myself a laptop, after having talked on and off about the idea to Deb for the last couple of years. I bought an Acer Aspire 5612, which is to say 15.4" widescreen, 1Gb ram, 120Gb HDD, Intel Duo Core with the matching set of Intel chips and Intel 945 graphics. All for about 850 euro. The price was right, the specs were right, and best of all is that the drivers for everything are good *and* FOSS. Being able to buy it off the shelf, literally, in a real shop, is also handy, especially if you are impatient. I've got Kubuntu Edgy running on it plus that other OS that came with it, (for Deb who wants it for her work).

<b>The rant starts now</b>

As far as setting up the laptop for general use, the most annoy stuff has been dealing with XP:

* No installation media for when XP needs to be reinstalled. Not only do you not own a copy of XP, but a license, apparently the customer isn't even deserving of a .50 euro CDROM.

* Partitions formatted as FAT32. /me shakes head.

* Stooopid Acer recovery program that wants to use half of your driver just as a backup.

* Stooopid Acer recovery daemon which hangs, burning CPU once when the backup partition is removed and then used for Kubuntu.

* 10 minutes of putting up with all the annoying popups and warnings from the preinstalled trial version of Norton antivirus was too long. Crapware.

* XP likes to tell me that it found a new IR device everytime it boots up. Pointing it to the just downloaded drivers didn't seem to help. (It has stopped doing this now. I don't know why exactly).

* XP likes to install updates, reboot, and then 10 minutes later find more updates as if to say "Hey! I found some more back here. They must have fallen behind the couch!".

I'm really quite surprised by the amount of crap the average XP user must have to put up with. A lot of the problem seems to be the "value added" software that Acer put in. I guess I'm just not used to applications which are busy jostling amongst each other for the user's attention, either by cluttering your taskbar, showing random popups, splash screens at boot time, throwing legalesse in your face, etc. All that distraction. It's like being in a casino.


<b>The Good Stuff</b>

Kubuntu went on pretty easily, Acer had the drive partitioned in half so I could just use the 2nd partition without having to mess with XP. I used JFS since reiserfs was not available on Kubuntu's standard install CD.

What works, what doesn't:

* Shutdown - works most of the time, but sometimes seems to hang. I don't know why. Forcing it off with the powerbutton works and doesn't seem to harm it.

* Suspend - seems to work fine.

* Hibernate - seems to work fine, but has the same problem as shutdown.

* Bluetooth, flashcard reader, inbuilt in webcam are untested. I remember reading somewhere that someone had written drivers for the webcam.

* Wifi - Untested, but it is an Intel chipset and I expect it to work fine and have full support with OSS drivers.

* Graphics and screen - This was one of the few things that I had to tweak a bit to get it working. This laptop requires the 915resolution hack/package to be installed to use the native 1280x800 resolution. After that I set the resolution in system settings. I don't know why 915resolution isn't installed by default. Another comment about the screen. I'm happy with it, but it has limitations, namely a fairly limited viewing angle by modern standards, and to me the default gamma was just way brighter and washed out looking than what I was used to. In system settings I set the gamma correction to 0.8.

* Battery reporting and CPU frequency scaling works out of the box. I can finally use Sebas' little powermanager utility. :-D Battery life for office type usage is about 2.5 hours.

So far so good. I wasn't looking for a top of the line machine to replace my desktop. But a solid machine that will run Linux well for anything from hacking on the run to showing digital holiday photos to grandma. In summary: I'm happy with it and can recommend it.
