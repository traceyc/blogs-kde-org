---
title:   "Python and Qt programming with Roberto Alsina"
date:    2009-04-01
authors:
  - simon edwards
slug:    python-and-qt-programming-roberto-alsina
---
Roberto Alsina recently posted a series of tutorials about Python and PyQt on his blog. I don't think they appeared on Planet KDE, so I'm forwarding them on. ;-) It covers typical Qt GUI programming using Qt Designer.

Most of the material is directly relevant if you want to develop KDE applications using Python, PyQt and PyKDE. Some differences of note are:
<ul>
<li>KDE widgets - KDE has many widgets that replace standard Qt widgets and extend them with better integration with the KDE desktop and more functionality. Assuming you've got the right packages installed, you should have extra KDE widgets available inside Qt Designer.</li>

<li>pykdeuic4 - The pyuic4 tool doesn't understand or support KDE's widgets. Use pykdeuic4 instead. It's basically pyuic4 with added KDE support.</li>

<li>XMLGUI for menus and toolbars - KDE has support for defining menus, menu items and toolbars in an external XML file which will automatically be used if your application uses the KXmlGuiWindow class for its main window. This makes it easy for people to customise your application and saves you from having to write a lot of tedious and boring set up code.</li>

<li>Build system - Python takes a pretty low ceremony approach to most things including build systems. Usually you don't need one, except perhaps during installation and/or packaging. In PyKDE in the kdebindings module, there should be an example Python project and directory called tools/cmake_project (exact location depends on your distribution). It is a basic project template which uses the CMake build system to correctly install your application and its data files, and doing things like generating .pyc files, and compiling .ui files to .py. It is a good base which can be expanded and customised. Some prior knowledge of CMake is required though.</li>

<li>Resources - We, or at least I, don't use Qt style resources for handling images and data files etc. In a KDE application you'll be installing your files, possibly using CMake and using the methods on the KStandardDirs class (via KGlobal.dirs()) to locate your data files.</li>
</ul>

Here are the links. Thanks Roberto!

<a href="http://lateral.netmanagers.com.ar/stories/BBS47.html">Session 1</a>
<a href="http://lateral.netmanagers.com.ar/stories/BBS48.html">Session 2</a>
<a href="http://lateral.netmanagers.com.ar/stories/BBS49.html">Session 3</a>
<a href="http://lateral.netmanagers.com.ar/stories/BBS50.html">Session 4</a>
<a href="http://lateral.netmanagers.com.ar/stories/BBS51.html">Session 5</a>
<!--break-->
