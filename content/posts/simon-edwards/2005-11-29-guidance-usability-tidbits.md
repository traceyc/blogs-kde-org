---
title:   "Guidance usability tidbits"
date:    2005-11-29
authors:
  - simon edwards
slug:    guidance-usability-tidbits
---
<p>
Jan Muhelig from OpenUsability.org did a usability review of Guidance with Sebas back at Malaga. The <a href="http://www.openusability.org/reports/?group_id=166">report</a> is now up at OpenUsability for those who are interested. I like to think that I know something about usability. I've been studying/reading about it since the mid-90s and applying it to the software that I design and construct. I thought Jan had some very good comments that we'll be soon putting into a future version of Guidance. After working with the code for so long you get blind to its usability faults. :-) It is really good to have someone who is still "fresh" go in and have a good look at what can be made better.
</p>
<p>
While on the topic of Guidance, I just put <a href="http://www.simonzone.com/software/guidance/">version 0.5.0</a> up on my site. I've been working pretty much flat out for the last few weeks on displayconfig. Displayconfig is our utility for configuring screen and X server settings. It should now work for single head configurations, although it has only been tested on my two machines here. The goal is to work Jan's changes in, get single head support stable and tested and also going on to dual head; the Holy Grail!
</p>
