---
title:   "Back online (yay!)"
date:    2005-07-31
authors:
  - simon edwards
slug:    back-online-yay
---
So finally after 4 weeks without having any kind of internet connection at home, I'm back. Sure, I could browse the web and download my mail at work, but it wasn't really possible to change what was happening "out there" on the net. Kind of like TV. You could watch, but not interact. Rather irritating.

Concerning a certain Dutch cable company who couldn't move my connection to my new address, and couldn't even tell me if there was cable in the street let alone give me a date when everything would be worked out. I hex thee! I wanted to say something else, in fact a lot else, but I thought I better keep it clean. Coincidentally, I was googling on the word 'hex' just to make sure that it meant what I thought it meant, and at the same time discovered the "Speak text" tool in Konqueror. Flashbacks to AmigaDOS 1.3. I'll report back in a few days if KTTS managed to unleash hell by reading out the page of hexes. (KTTS KDE4 feature: hex casting!)

Seriously, I've got ADSL from KPN now and it seems to work well. The helpdesk is even helpful and actually seemed to give a damn that they had sent me a dud modem. (causing ~1 week delay) The connection also works as advertised. I've been updating my Kubuntu installation here and I'm getting ~160Kb/s downloads, which sounds about right. The only problem is that the modem is acting as as NAT firewall, and my attempts to get everything working with the modem working in "bridge mode" have failed. This is problem because makes half of the software projects that I'm working on irrelevant. I can't test Guarddog or Guidedog in this configuration... So, has anyone had success with a Speedtouch 546 ADSL modem in bridge mode?

