---
title:   "I get Git (finally!)"
date:    2009-05-20
authors:
  - simon edwards
slug:    i-get-git-finally
---
I'm about 18 months behind the curve here compared to you trend-setters in KDE-land, but I think I now actually "get" git. Meaning that I now have a mental model of git which makes sense and I can use to make sense of the numerous "Git $X seconds" blog posts hanging around on the web (where the number $X is always smaller than the last blogger). I remember sebas having a go at explaining it to me and me not understanding what the big deal was or what it really was about.

The thing that did it was Tom Preston-Werner's <a href="http://tom.preston-werner.com/2009/05/19/the-git-parable.html">"The Git Parable"</a>. Just a 10 minute read and boom! there it was in one go in my head: clarity. I seem to understand complex things better when I understand the forces that shaped them into their present form. I like "why"s with my "how"s.

If you're familiar with CVS / Subversion style revision control systems, then you are doomed to failure if you try to learn git in terms of SVN. I've tried that and failed. It is best to clear your mind of that model and read Tom's parable. Even the name of the git command "rebase" actually makes sense after reading Tom's piece and <a href="http://jbowes.wordpress.com/2007/01/26/git-rebase-keeping-your-branches-current/">this post by James Bowes</a>.

I'm now browsing through the <a href="http://book.git-scm.com/">Git Community Book</a>, it's pretty straight forward. If I'm feeling really confident I might carefully try out git-svn on part of KDE's SVN. 8-)
<!--break-->
