---
title:   "PyKDE plans for KDE 4.4"
date:    2009-08-26
authors:
  - simon edwards
slug:    pykde-plans-kde-44
---
KDE 4.3 is out, I'm vacationed, and now is a good time to explain share same of the things I would like to get done in PyKDE for KDE 4.4.

The main feature I want in KDE 4.4 is PyKDE support for Python 3. Snapshots of PyQt have supported Python 3 for a while now and from the reports I've heard the transition isn't too difficult and it is fairly straight forward to support both Python 2 and Python 3 using the same bindings (.sip) files. PyKDE support for Python 2 would remain of course until interest waned enough that I could drop it without upsetting too many people. One welcome change with moving to a PyQt/PyKDE on Python 3 is that we can use the simpler and more "Pythonic" PyQt API which eliminates QString and QStringList and QVariant. Instead of dealing with  QStrings and Python strings inside the same program, you'll just have Python strings and things will be converted automatically.

The biggest problem which I foresee with supporting Python 3 is how to handle things like Python based plugins in system-settings and our desktop shell Plasma. The problem is that it is not practical to embed a Python 2 interpreter inside an application (e.g. "plasma-desktop") and a Python 3 interpreter at the same time. What this means is that you can't run Python 2 plasmoids at the same time with Python 3  plasmoids. One solution is to just say that only Python 2 is supported for plasmoids. This might work for a while, but in the long term Python 2 is going away. The other solution is to... ummm aaahh... I guess... say that version 4.X of KDE will only support Python 3 plasmoids, "Port or Die" would be the motto. That is not an option I'm really looking forward to either. If any plasmoid programmers, users or distro people have thoughts about this, then please speak up. I could really use some input on this issue.

As for wider binding coverage, I've had a request to make some bindings for most of the kggz* libraries in KDE's games module. So I'll make that happen (probably in playground and for KDE 4.3 initially). And I remember someone a long time ago on IRC requesting the ability for make Okular plugins with Python. If this is still (strongly) desired then get in touch with me (read: nag me and/or grovel as necessary).

I also want to spend some time on tooling to help with updating all of the bindings. The tool "twine" in kdesupport is used for bulk updating all of the .sip files which describe the KDE APIs for SIP which actually generates the bindings code. It was made by my PyKDE hacking predecessor and works quite well but it could use some refactoring and changes to help better match the workflow that I now use for development.
<!--break-->
