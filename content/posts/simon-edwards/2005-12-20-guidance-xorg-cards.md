---
title:   "Guidance & Xorg: Cards"
date:    2005-12-20
authors:
  - simon edwards
slug:    guidance-xorg-cards
---
Work was having a clean up and I got my hands on a bunch of old videocards: 2x nVidia TNT2 (one with TV out, with is handy), 2x S3 (old school AGP, so I'll have to reassemble my old Pentium 2), 1x Diamond something (AGP 1.5v & 3.3v) and a Rendition chipset based Diamond card (PCI!). This expands the number of chipsets and configurations that I can use to test the displayconfig part of Guidance.  I whacked the PCI card into my other computer along side the AGP card that was already in there and it quickly exposed a problem in displayconfig. (Only one card appeared on the hardware tab). Another interesting thing was that the BIOS chose the PCI card to use during boot. While the existing xorg.conf file still worked fine and used the AGP card. Maybe displayconfig also needs a way of choosing between two cards, even for singlehead configurations.

I also started some work on adding support for clone configurations to displayconfig (i.e. displaying a copy of the primary monitor on the secondary monitor).
<!--break-->