---
title:   "Which languages are people writing Plasmoids in?"
date:    2011-08-15
authors:
  - simon edwards
slug:    which-languages-are-people-writing-plasmoids
---
It was good to make it to the Desktop Summit in Berlin this year and to see some familiar KDE faces again. I feel that I've been able to catch up a bit on news about many of the interesting projects that are happening in our KDE community and also in the greater FOSS desktop community.

I was talking to some people about Python in KDE and where it is being used, and I pointed out that quite a few plasmoids are written in Python. Being the curious type, and also bearing in mind the saying "if something doesn't have a number written next to it then it is just opinion", I went to kde-apps.org today and did a quick survey of the plasmoids section to see which languages are being used. Here are my results of surveying the latest 50 plasmoids, (methodology is described below.):

Python 46%
C++ 44%
QML 6%
Javascript 4%

So there you go. A lot of Python being used, more than I expected really. C++ is still getting a lot of use despite how difficult it is to distribute to end users when compared to more script oriented approaches. But the big surprise is how little Javascript appeared in my sample. If anyone can explain what is going on here or what I'm doing wrong, I'd appreciate it. Ruby didn't appear in my sample, sorry Richard.

Also a small pro-tip to plasmoid developers. When you release a plasmoid make sure that no temp, back up files, or complete .git directories are included in the file plasmoid archive(!).

Methodology used. What I did was go to the plasmoids section on kde-apps.org, and looked at the "latest" tab, and then starting with "Play Control 1.0" I worked back in time recording the implementation language used for the latest 50 plasmoids. If the plasmoid was "plasmoid script", then I also downloaded code and looked inside to see what was being used.
<!--break-->
