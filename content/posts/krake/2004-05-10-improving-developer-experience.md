---
title:   "Improving the developer experience"
date:    2004-05-10
authors:
  - krake
slug:    improving-developer-experience
---
Collection of ideas on how to encourage more developers to develop with KDE.
<!--break-->
I have been doing developer support on mailinglists, newsgroups and web forums for about three years now.
Support means helping developers solving development problems, especially for developers who are new
to Linux, Qt or KDE.

Helping developers new to KDE is quite difficult, because a lot of our advanced stuff is not so easy to
understand if you haven't already used it.

I put together some of my thoughts on how to improve the experience when developing KDE, any
kind of feedback appreciated.

<b>I. Enhance lib API documentation</b>

- Linking related classes
  Provide links in the API docs between classes the usually work together, for example
  between kde:KLibFactory and kde:KTrader. (btw. I can't find KTrader in the API docs for 3.2 or CVS,
  for 3.1 it is under kio)

- Add information about which libs are needed (giving build system variables when appropriate)

- Add more source snippets
  I think this could be a job for people starting with KDE related development, as they have to
  make themselves comfortable with the KDE API anyway.
  Important would be a recipe how to sumbit such snippets, so that they get reviewed by
  developers who know those parts of the libraries very well.

  For example it might be a working solution to load the standard icon for "File Open" and
  put it in a kde:KAction, while the correct solution would be to use KStdAction:: open().

  A secondary thought on namespaces, I think there should be a link or at least a hint on
  the API entrance page, like there is one for classes in parenthese after the library name.

- Add links to examples (like in Qt docs)
  Providing buildable examples for review by experienced developers might also be a nice
  job for beginners interested in contributing.
  Some areas definitely need help from experienced developers, stuff like IO jobs and
  components.

- Improve visibility of generic solutions (Trader, Interfaces)
  KDE's strength is great modularisation (KParts, plugins, Interfaces) but IIRC most
  documentation discusses general KPart handling, no example that shows how easy it is
  to embed a media playerusing KMediaPlayer:: Player

<b>II. Add application API docs</b>

- Add docs for plugin APIs, especially KHTML, Konqueror, Kicker

- Generate DCOP docs from headers like API docs, so there is a place to
  look for explainations if you just found out there is are soe notify methods
  in KNotify's default interface, but you have no idea what the parameter mean.

- See power users as high level developers
  Creation of service menus, scripting with DCOP, scripting with commandline tools like kfmclient, kdialog).
  Current documentation is mostly targeted at application developers using C++ (with some nice exceptions like Aaron's service menu tutorial)

<b>III. Internal documentation</b>

   Make internal docs (Job metadata, XML GUI, ...) more visible
   (linking from API docs, create a section on developer.kde.org for them)

   You can find quite some threads on kde-devel where some developer has a question
   about KDE specific feature like KIO jobs or XML GUI stuff, where medium level
   developers like myself can't reach a point when they can't help any further, but the
   next code developer that turns up just has to point to kdelibs/.../DESIGN which
   explains everything relevant.

<b>IV. Improve reachability of core developers</b>

   Developers with in-depth knowledge of KDE are sometimes hard to reach, usually because
   they are not subscribed to the help areas, due to the high traffic and bad signal/noise ratio.
   Maybe we can find a way to keep trivial questions away from them while bringing
   difficult problems to their attention.

   One idea is taken from KDE quality (or at least I understood some of the proposals this way):
   provide a mediator layer, kind of human filters, which process the incoming traffic and
   forward advanced problems to a restricted list, properly categorised, e.g. using a
   subject prefix indicating the problems nature.

<b>V. Improve KDevelop knowlegde</b>

- Beginners and switchers from other platforms tend to use IDEs, sometimes they can't even
  separate KDE and KDevelop ("how can xyz be done with KDevelop" while actually meaning
  "how can xyz be done with Qt/KDE").

- Long term, experienced KDE developers, including myself, often don't use KDevelop (e.g. I use Kate) and thus can't
  give instructions on how to apply solution in KDevelop, especially in build system
  (how does one do the suggested Makefile.am modifaction using the automake manager),
  installation (how do I tell KDevelop where to install data files/images)
  or testing related problems (why do I have to "make install" just for testing and is
  there another possibility)

<b>VI. Integrate integrators or "KDE as a platform"</b>

- Gather feedback from developers who try to integrate their applications with KDE
  (scribus, OpenOffice.org integrator, Gambas developer, binding developers).
  What do they find difficult, what would they like to have to make their live easier.
  For example how do they detect location of KDE's headers, kde-config only can tell
  the install path for libs.

- Provide runtime access to KDE: detect if KDE is running, which version, etc.

  Maybe provide commandline client like kfmclient to access KDE configuration, for
  general information like current language, double click policy, etc.

- Acknowledge integrators' efforts by displaying their work in the kde.org website
  family. Maybe provide some official logo to tell visitors to their websites that
  a KDE enhanced version is available.

- Advertise KDE as a platform
  IMHO current developer mindshare sees KDE as some kind of Qt extension.
  My pet example are frameworks that have platform dependend implementations like
  wxWidgets or SWT.
  IMHO there is no point in creating wxQt ot SWT/Qt as they already have implementations
  on Qt's other platforms, but wxKDE or SWT/KDE would be nice additions.

- Especially encourage Qt developers to provide KDE enhancements, for example having a list
  of necessary changes which would be needed to access certain KDE features
  ("When does a program need a KApplication instance, when is a KInstance instance
  sufficient, etc).

  Maybe offer some public domain code snippets with proper #ifdef'ed code
  for example for creation and initialisation of the (Q/K)Application instance.

- Provide help for switchers (developers with experience on other platforms
  e.g. Win32/MFC), possibly using first hand experience of KDE developers with
  similar background.

<b>VII. Tools</b>

   Encourage creation of tool programs, for example creating XML GUI file from .ui file
   of a designer generated QMainWindow form.
   It is like the IDE thing, beginners and switchers like tools, even for things we
   consider trivial.
   Creation of helper tools might actually be nice jobs for those beginners.