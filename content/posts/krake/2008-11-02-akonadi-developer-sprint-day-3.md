---
title:   "Akonadi Developer Sprint - Day 3"
date:    2008-11-02
authors:
  - krake
slug:    akonadi-developer-sprint-day-3
---
I'd say we had an extraordinary good last day since nobody had to leave early and the majority didn't have to leave until late afternoon.

After plenty of breakfast we started with a bit of blog reading, email checking and warm-up hacking.

We then proceeded with one of the agenda points we had left from yesterday, Igor's presentation of the test framework he created as his Google Summer of Code project. One of the deliverables of the project is the so called Akonadi Testrunner, an application which will creates a fully self-contained AKonadi setup, i.e. test specific redirections for XDG base directories, KDEHOME, etc., a D-Bus session bus separated from the user's main session bus, an Akonadi server with its own database so it won't interfer with the developer's actual Akonadi cache.

The obligatory group photo is still in Will's camera at the time of writing, but is probably already available in his blog once I get the opportunity to post this.

Apropos group photo: Ingo from the Hotel staff had a visitor from California and it turned out that he is a    interface designer. So we invited them to have lunch with us where Will grabbed the opportunity to hold a "recruitment speach" :)
<!--break-->
