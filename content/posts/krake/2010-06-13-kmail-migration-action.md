---
title:   "KMail migration in action"
date:    2010-06-13
authors:
  - krake
slug:    kmail-migration-action
---
After blogging about our progress on KMail's data and config migration to Akonadi for a couple of times, I felt that it was time for a screencast showing the migrator in action.

The KMail test setup migrated here has most of the common account types: POP3, IMAP, Disconnected IMAP, local MBox file and, of course, KMail's local folders.

After starting up, the migration tool checks whether it will be dealing with one or more Disconnected IMAP accounts. If there is at least on unmigrated account of that type, just like in this test scenario, it asks the user how to handle the old cache after import.

Bascially, a Disconnected IMAP account in KMail has local copies of the mails on the IMAP server, so it can access them even when offline (hence "Disconnected IMAP").
The migrator imports these copies into Akonadi's cache so you don't have to download them again after the switch to KMail2.

<em>Note:</em>normally you don't need the old copies anymore, but for example if you want to test migration several times, you will want to opt for keep the copies for the next round.

In the screencast I am option to delete all successfully imported copies (the migrator will always keep those which it failed to import for whatever reason) in order not to duplicate disk space usage.

The migrator then continues to work through the configured accounts, starting with a POP3 one.
Retrieving the authentication data for the server requires access to the user's KDE wallet, which prompts for the password protecting it.

Next account is an IMAP server without local message copies (normal IMAP).
While this does not require import of already downloaded messages, it might still have only locally available information that is worth migrating, e.g. tags not supported by IMAP.

After finishing this rather fast check for metadata, the migrator continues with the test setup's local MBox account.

At this stage we get to the interesting part: the Disconnected IMAP account.
A couple of its folders have locally cached messages, two of them quite a lot (5000 each).

Finally, after having processed all configured accounts, the migrator's last task is to make KMail's local folders available to the Akonadi setup.
It therefore creates a special Akonadi resource which can deal with the mixture of Maildir and MBox folders and then checks whether any of these folders should be used for special roles such as "inbox", "outbox", "sent mail" and so on.

In the scenario of the screencast it can even make them the default for their respective role, just like they were in a traditional KMail setup.

<embed src="http://blip.tv/play/AYHmgFQA" type="application/x-shockwave-flash" width="320" height="270" allowscriptaccess="always" allowfullscreen="true"></embed>

Video available in OGG Theora and Flash on <a href="http://blip.tv/file/3749220">Blip.tv</a>
<!--break-->
