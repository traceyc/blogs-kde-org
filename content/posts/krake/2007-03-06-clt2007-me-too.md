---
title:   "CLT2007 - me too"
date:    2007-03-06
authors:
  - krake
slug:    clt2007-me-too
---
<a href="http://daniel.molkentin.de/blog/index.php?/archives/73-CLT2007,-LiMux,-Where-Documentation-Really-Belongs.html">Danimo</a> and <a href="http://frinring.wordpress.com/2007/03/05/been-to-the-clt-2007/">frinring</a> already blogged about Chemnitzer Linux-Tage 2007 and I can't resist doing it as well.

I have been quite lucky at picking which talks to go to, especially the choice to attend <a href="http://blog.alphascorpii.net/english/debian/events/clt07-2.html">Meike Reichle's</a> talk about how to promote Free Software. She is an awesome speaker, totally in control of her audience's attention.

I also enjoyed the update on the progress of Munich's transition to Linux, since Florian Schießl doesn't get too much into technical details but rather concentrates on telling about organisational problems and their solutions.

As Friedrich already mentioned, we incidentally met at Christian Böttger's talk about Groupware solutions and I was quite astonished that he alsmost praised Kolab as being a best-of-breed thing when one looks at non-proprietory solutions, bascially leaving just the missing web frontend as its single shortcoming.

However, I think the two most interesting talks I chose were Ralf Gesellensetter's talk about Free Software usage in schools and Karsten Hilbert's talks about a Free Software package for doctors (med). Not necessarily because the topics are my own areas of interest, but rather because they are domain experts (Ralf being a teacher and Karsten being a doctor) who really know the requirements of their respective domain and are a lot better in triggering their peers' interest than any of us software developers could ever do.

It is difficult for Free Software projects to compete on the mass-market playing field (e.g. office suites) since there is such a wide range of requirements, but special interest groups with well defined common requirements for at least part of the software stack are likely within our reach. Being based on voluntary cooperation gets us an andvantage over proprietary software vendors, because it is a lot more difficult to get experise regarding a targeted domain, while we could get this experise basically "for free" (i.e. in exchange for our expertise in software engineering).

As a side note, Karsten said that they are looking for someone interested in doing <a href="http://pim.kde.org/components/korganizer/">KOrganizer</a> - <a href="http://www.gnumed.org/">GNUmed</a> integration and I told him that it might a good idea to enter this (or something built upon this) as an idea for a <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Ideas">KDE related Google Summer of Code</a> project.

<!--break-->