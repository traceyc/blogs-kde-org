---
title:   "Akonadi Screencast"
date:    2008-09-25
authors:
  - krake
slug:    akonadi-screencast
---
I originally planned to do that in time for the KDE PIM special feature which has been published as part of one of the recent commit digests, but I didn't find enough time to it then and almost forgot about it later.

But today I sat down and recorded by first screencast ever and several takes due to tons of stupid mistakes, I present to you the screencast about <a href="http://blogs.kde.org/node/3615">Akonadi KResource bridges</a>.

Video available as <a href="http://blip.tv/file/get/Krake-AkonadiKDECompatibility954.ogg">OGG Theora</a> and <a href="http://blip.tv/file/get/Krake-AkonadiKDECompatibility954.flv">Flash</a> on <a href="http://krake.blip.tv/posts?view=archive&nsfw=dc">blip.tv</a>

Since the audio volume is quite low and am not such a good speaker, I'll explain the most important scenes.

I started with launching KAddressBook and added a new entry to its default file based resource.

After that I began the process of adding the first half of the Akonadi bridge, the application side plugin, just like I would add any other traditional address book plugin.

The configuration dialog for this special plugin also allows to perform Akonadi management tasks, such as adding new Akonadi resources (in the screencast and the UI I called that "Address Book Sources" to avoid the resource ambiguity).

Using the second half of the bridge, the Akonadi resource operating on KResource plugins, I gained access to the very same address book file the default resource is pointing to.

After removing this duplication by removing the default plugin, I launched a second Akonadi enabled application called akonadiconsole.
This isn't a nice userfriendly one because it is meant as a developer tool, but unfortunately I didn't have any other option available at this point due to not many applications using Akonadi yet.

The demonstration continues with showing Akonadi's change notification handling, e.g. that adding a second resource in akonadiconsole has it automatically available in KAddressBook as well and that changing data (removing the email address) is also automatically relayed.

If anyone is interested in trying this as part of the upcoming <a href="http://www.confuego.org/archives/6-KdePimDay-and-Groupware-details.html">KDE PIM bug squashing</a> I would really appreciate any feedback and will try to be available on IRC during most of the day (my nick is krake there as well).
<!--break-->