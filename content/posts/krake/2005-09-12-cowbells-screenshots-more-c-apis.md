---
title:   "Cowbells, Screenshots, more on C APIs"
date:    2005-09-12
authors:
  - krake
slug:    cowbells-screenshots-more-c-apis
---
Ok, that was a lie, no cowbells ;)

Getting tired of the primitive example application I had for <a href="http://www.sbox.tugraz.at/home/v/voyager/qds/index.html">QDS</a> I deciced to create a better one,
including a GUI one can take 
<a href="http://www.sbox.tugraz.at/home/v/voyager/qds/screenshots.html">screenshots</a> of :)

One for the curious:
<a href="https://blogs.kde.org/system/files?file=images//dialog_auth_0.preview.png"><img alt="KWallet asking for password for authentification" src="https://blogs.kde.org/system/files?file=images//temp/dialog_auth.thumbnail.png" class="showonplanet" /></a>
Yes, that is an almost pure Qt application getting authentification credentials from KWallet for accessing
a webdavs URI

After my <a href="http://blogs.kde.org/node/1369">rant on C APIs</a> I had another take on one of them: the
<a href="http://developer.gnome.org/doc/API/2.0/gnome-vfs-2.0/index.html">GNOME-VFS API</a>

While one sometimes reads complains about the state of their API docs, sometime even from GNOME core developers,
I found them pretty good, especially when compared to the Carbon docs.

The simulated OOP makes it still a little peculiar to track object life cicles but nevertheless I managed to
implement a GNOME-VFS based Launcher service for QDS within a few hours, including fighting a broken GNOME installation (turned out I accidentally installed GNOME 1 devel files instead of the 2.x ones *doh*)

Only weird thing is that I haven't found out yet how to correctly launch URL that do not end in files, for example
http://www.kde.org/ (Yes, I also tried http://www.gnome.org/ just to be sure ;))

<!--break-->
