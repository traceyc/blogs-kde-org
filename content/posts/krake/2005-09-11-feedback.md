---
title:   "Feedback"
date:    2005-09-11
authors:
  - krake
slug:    feedback
---
Free software development is all about feedback.
Feedback from other developers, non-coder contributors to the project,
your packagers and your users.

Sometimes you get very few feedback, which could either mean you are not
reaching anyone or everyone is so satisfied with the projects current state.

For my on sanity I assume the latter :)

One of my current projects, the KDE addressbook commandline client
<a href="http://kde-apps.org/content/show.php?content=25480">kabcclient</a> (also in <a href="http://websvn.kde.org/trunk/kdereview/kabcclient/">SVN</a>)
is generating very little feedback: only a handful comments on kde-apps.org,
no comments on its freshmeat page and only two people so far by email.

Speaking of the two guys who contacted me by email: I am pretty exited by that
because they are developers themselves and use kabcclient as part of their
applications.

For example as part of a <a href="http://kaddressamba.blogspot.com/">SuperKaramba applet</a>

The other project is not released yet, so I'll refrain from blogging about that until it is.

Now for some feedback from me:
KDE's translators rock. kabcclient was imported into SVN three days ago and the "pt" (Portugal if I'm not mistaken) translators have finished their
translation two days ago.
Busy like bees they are! :)
<!--break-->

