---
title:   "Akonadi for application developers"
date:    2009-02-01
authors:
  - krake
slug:    akonadi-application-developers
---
This week I have been working on an <a href="http://techbase.kde.org/Development/Tutorials/Akonadi/Application">Akonadi tutorial</a> targetted at application developers.

Compare to the <a href="http://techbase.kde.org/Development/Tutorials/Akonadi/Resources">Akonadi Resource Tutorial</a> this was rather difficult. Resources have a well defined task and a tutorial can being with a basic implementation and move on to more complex scenarios later on.

An application built for a specific purpose so a tutorial needs to choose a specific task which is neither to complex nor to abstract and still show as much topic related API usage as possible.

So after quite some thinking I decided to base the application of this tutorial on the task of stripping away attachments from e-mails, i.e. allowing the user to save an attachment to disk and have it being removed from the e-mail itself.

As always, any feedback is appreciated!
<!--break-->
