---
title:   "Desktop Summit 2011"
date:    2011-08-12
authors:
  - krake
slug:    desktop-summit-2011
---
A slight delay of my flight from Düsseldorf to Graz gives me time to recap the awesome time at the Desktop Summit 2011 in Berlin.

Having been part of the programme committee I was looking forward to see at least a small subset of the talks live, though I hope I will have the opportunity to see many more once the videos have been processed and are available online.

Selecting talks hadn't been easy, both because of the huge amount of proposals we've received and because deciding on suitable topics for such a wide range of attendees is no piece of cake either.

I think the session I liked most was the lightning talk style collection of presentations of our communities' GSOC students and GNOME's women outreach program interns. Not only did they often take on difficult challenges, they all showed amazingly sophisticated solutions.

I also found it extremely encouraging that these programs seem to strengthen and even expand the diversity of our communities. I mean, sure, GNOME, KDE and other Free Software projects are international by design, with contributors from many countries. But due to various reasons the bulk of people are usual from North America or Europe.
So seeing so many students/interns from Asian, Latin American and Eastern European countries was one of my personal highlights of these summit. I hope that the incredibly high percentage of women in this group of presenters indicates that we are on the right path to improving our rather embarrassing track record in making our female peers feel welcome in our midst.
<!--break-->
