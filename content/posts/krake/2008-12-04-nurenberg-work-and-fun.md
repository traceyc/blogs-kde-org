---
title:   "Nurenberg work and fun"
date:    2008-12-04
authors:
  - krake
slug:    nurenberg-work-and-fun
---
When your boss tells you that you have to take over some work from a collegue, work that involves travelling several hours in each direction, work on a project you haven't been involved before and only get a minimum briefing, your most common reaction will likely be "this sucks!".

Lets not be kidding, work itself and the circumstances still sucked, but being a KDE developer has certain advantages, e.g. knowing people all over the world, especially in KDE stronghold like Nurenberg.

So on Tuesday evening I went for dinner with The SUSE People(tm), Flavio, Stephan, Cornelius, Dirk and Will, to a really nice place called "Herr Lenz" with an interesting style of cuisine and very good beer :)
Thanks a lot guys!

P.S.: unless I am really lucky I am going to have to cunduct the actual site acceptance test procedure next week as well, so you might get a call from my again.
<!--break-->
