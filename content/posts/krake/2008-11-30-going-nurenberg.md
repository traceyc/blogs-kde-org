---
title:   "Going to Nurenberg"
date:    2008-11-30
authors:
  - krake
slug:    going-nurenberg
---
The next few days I will be in Nurenberg, working on-site at our customer's facilities at the airport.
I am arriving Monday afternoon and departe on Thursday morning, so I basically have three evenings to spend on going out for dinner and probably a pub or two :)

So if anyone living in that area doesn't want to pass up on a verifyable excuse for some fun, let me know.
<!--break-->
