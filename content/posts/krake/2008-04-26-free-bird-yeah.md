---
title:   "Free Bird yeah!"
date:    2008-04-26
authors:
  - krake
slug:    free-bird-yeah
---
<a href="http://en.wikipedia.org/wiki/Lynyrd_Skynyrd">Lynyrd Skynyrd</a> rocks, but you already knew that.
<a href="https://blogs.kde.org/blog/77">Will Stephenson</a> rocks as well and you quite likely also already knew that.

<a href="http://en.wikipedia.org/wiki/Freebird">Free Bird</a> is one the reasons why Lynyrd Skynyrd rocks and one of the reasons why Will Stephenson rocks is that he took over the gargantuan task of moving the Akonadi server to is <a href="http://websvn.kde.org/trunk/kdesupport/akonadi/">KDE independent location in kdesupport</a>.

<i>"Gargantuan". You know, I've always liked that word "gargantuan", I so rarely have the opportunity to use it in a sentence.</i> -- Elle Driver (Daryl Hannah) in "Kill Bill 2".

Usually moving code in a SVN repository isn't that hard, but although we were very thorough about not getting KDE dependencies into Akonadi server, a lot of its build system related things were.
So making Akonadi server build again in its new location and making Akonadi dependent code in <a href="http://websvn.kde.org/trunk/KDE/kdepimlibs/akonadi/">kdepimlibs</a> and <a href="http://websvn.kde.org/trunk/KDE/kdepim/akonadi/">kdepim</a> build again was truely a gargantuan task (twice in one blog entry, not bad).

Additonally, several other developers, including Mr. Buildsystem <a href="https://blogs.kde.org/blog/531">Alex Neundorf</a>, have done some cleanups and thus further improve Akonadi's cross-desktop characteristics.

We still need to clean up some KDE name references, such as D-Bus names, etc. and setup project infrastructure on freedesktop.org, but I am quite confident that this won't take a lot of time. Until then, meet us Akonadi developers on IRC channel <a href="irc://irc.freenode.net/akonadi">#akonadi</a>, freenode network.
<!--break-->
