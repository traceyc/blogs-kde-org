---
title:   "Changes in communication channels"
date:    2009-02-16
authors:
  - krake
slug:    changes-communication-channels
---
When people used to say that blogs are the new usenet, they meant that discussions and flamewars which used to happen on usenet newsgroups, now happen in blogs and their comment sections.

Some bloggers even use their new publishing channel for making inquiries, i.e. the famous and infamous "dear lazyweb" postings.

See? I wrote unconsciously even used "postings", not something like "personal log entry"!

Anyway.

I've come to the conclusion that there is another shift of media going on, this time a move away from mailing lists. I have to say that I haven't yet been able to determine where the usage shifted to, so in case you have ideas, please let me know.

It is probably also quite dependent on target audience because at least our developer mailing lists seem still to be used for questions, announcements and so on.
Since, as a free software community contributor, I am primarily a developer nowadays, I wasn't aware of this change in other areas of contributions.

For example about a month ago I sent an invitation to the two main Free Software Desktop projects, cunningly addressing their "external relationship" mailing lists (or what I percieved to be their most likely mailing list for promotional activities): GNOME's <a href="http://mail.gnome.org/archives/marketing-list/2009-January/msg00017.html">marketing-list</a> and KDE's <a href="http://lists.kde.org/?l=kde-promo&m=123240203116654&w=2">promo list</a> (same posting on both links, called cross posting in the good old times).

Unfortunately, I have missed the memo about switching to a different communication channel, see above.

Or neither project has any interest in getting in touch with their Austrian users or just trying to attract Austrian contributors.

I know, I know! Even thinking about this possibility is absurd, it must be the change of communication channels.

So, dear lazy web (see? can do this as well, I am quick study), if you have any hint on what this new channels are, the Austrian Free Software enthusiasts would be very greatful to know, in particular the organizers of <a href="http://www.linuxtage.at">Grazer Linuxtage</a>.
<!--break-->