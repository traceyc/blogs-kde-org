---
title:   "How services change the application landscape"
date:    2009-03-01
authors:
  - krake
slug:    how-services-change-application-landscape
---
While working through my backlog of articles on <a href="http://planet.gnome.org">Planet GNOME</a> on my way to Nuremberg (yes, again), I came across a <a href="http://pvanhoof.be/blog/index.php/2009/02/20/e-mail-metadata-e-mail-as-a-desktop-service">blog entry</a> of Philip Van Hoof.

In it he asserts that soon the era of email clients will be over, which I think some people misinterpreted as application for reading and managing email messages becoming obsolete.

However, what I think he intended to say and what he tried to explain in the following paragraphs, is that monolithic, end-to-end functionality, clients will be succeeded by applications specialized in displaying, creating and editing PIM data such as emails, but that other tasks like fetching emails from servers, filtering, etc. will be offloaded to services specialized in those domains.

Until now applicaton developers had to consider distracting isuses like how to get emails from servers, different protocols and capabilities of these servers, how to store the downloaded messages locally, how to search through potentially huge amounts of headers and message bodies, etc.

Since Philip's blog entry focuses on the search and filtering part, so I'll add the missing info about the data access and transferring bits.

From now on application developers can fully concentrate on how to present data to the users, how to let the users interact with it, etc.
The part of getting the data from server, protocol differences, local storage and caching is taken care of by the <a href="http://www.akonadi-project.org">Akonadi framework</a>.

Spezialized Akonadi clients, called resources, will take care of transferring data from servers and local storage.
The Akonadi service will take care of caching and uniquely identifying data items and their core properties such as MIME type.
Another set of services, called Akonadi agents, will perfom data manipulation, extraction and analysis tasks, such as the feeding semantic storages like Nepomuk or, as we can read in Philip's post, Tracker.

End user applications which only want to work on a subset of the vast amount of a user's PIM data can then ask the search service to tell them which data items they'll have to look at and get only those items from Akonadi.

Akonadi will then make sure to get the item data from whereever it is currently stored or from the cache if it is still available there.

Perfect cooperation across projects through a service based approach!
<!--break-->
