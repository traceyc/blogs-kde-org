---
title:   "Hacking at aKademy"
date:    2006-08-10
authors:
  - krake
slug:    hacking-akademy
---
While I have been a KDE developer for ages, this will be my first KDE conference and I am even one of the few lucky ones that had their talk proposal excepted :)

Btw, talking about luck: has anyone else not yet received confirmation about their hostel reservation/payment?

Anyway, since I am currently not directly working on any KDE core parts, I am pondering what I will be hacking on during the coding marathon.

I am thinking about bringing the D-Bus Qt3 Bindings Backport up-to-date, or maybe porting kabcclient to KDE4 and working on Akonadi in the process.
The D-Bus bindings would be useful for implementing the DAPI aka Portland 2 API interfaces on KDE3, but I guess the KDE PIM developers wouldn't mind getting some helping hand either.

What do other "free lancers" plan to work on?
<!--break-->