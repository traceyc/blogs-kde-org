---
title:   "Commandline scripting KDE"
date:    2005-05-21
authors:
  - krake
slug:    commandline-scripting-kde
---
Last week we had our local Linux event of the year called <a href="/wwww.linuxtage.at">Grazer LinuxTage</a> which is part of an Austrian wide series of events called <a href="/www.linuxwochen.at">Linuxwochen</a>.

This year I decided to do a talk about KDE and not about Qt programming as the <a href="http://glt04.linuxtage.at/?id=40">years</a> <a href="http://glt03.linuxtage.at/lab4_1300.php">before</a>.

Being asked to do a beginner level talk, I settled with an introduction on commandline scripting KDE, i.e. using KDE features through commandline applications.

The official webpage of the talk, including the slides, can be found <a href="http://www.linuxtage.at/?id=6">here</a>

I did a quick English translation: <a href="http://www.sbox.tugraz.at/home/v/voyager/scriptingkde_en.kpr">KPresenter file</a>/<a href="http://www.sbox.tugraz.at/home/v/voyager/scriptingkde/">HTML version</a>

From the feedback I got I'd say that the audience was quite impressed by the size fo the feature set accessible through commandline clients :)

kstart, kdialog and dcop were the two most well known among the advanced users in the auditorium, however kde-config or kreadconfig/kwriteconfig were almost unknown.
Which is a pity since those three are quite useful for sysadmin tasks.

While I am on the topic of sysadmin tasks: after the talk I was approached by a visitor who had questions regarding large scale user setups.

For example he asked what the best way would be to copy new icons to every user's desktops.
I suggested to use kde--config to determine the user's desktop path in a script sourced from KDEPREFIX/env/ during KDE startup (or should I say kde-config --path exe | sed s#/bin#/env#g :))

Anyone know a more elegant solution, maybe some Kiosk trick?

