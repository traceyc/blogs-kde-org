---
title:   "Akonadi, the GLib client library and the Evolution Data Server"
date:    2008-03-28
authors:
  - krake
slug:    akonadi-glib-client-library-and-evolution-data-server
---
Based on reactions to my <a href="http://blogs.kde.org/node/3352">previous blog entry</a> I'd like to add a bit of context regarding the GSoC idea of implementing a GLib/GObject based client library for Akonadi.

The whole architecture of Akonadi is based on the idea not to depend on any specific library but to make the data storage access protocol, and D-Bus for out-of-band notifications, the only requirements for clients.
Learning from the non-adoption of DCOP and KIO, both also out-of-process service infrastructure projects, due to the lack of visible alternatives to the respective KDE based client libraries, the developers of Akonadi acknowledge the need to at least a second, independent, client library implementation.

This could of course be done in Java or Python, but since the GLib software stack is one of the two main stacks for Free Software desktop applications, it sounds a lot more reasonable to use it for the first of hopefully many means to access data in Akonadi.

Some people understand this to attempt replacing the Evolution Data Server.
Of course a GLib/GObject based client library for Akonadi would make it an second option for application developers on that software stack, but I am pretty sure that the EDS developers have enough confidence in their technology to not be afraid of the partial overlap between the two projects.

But just in case they want to strengthen their position and are looking into hosting a GSoC project for developing a Qt/KDE based client library for EDS, I'll be available as a co-mentor on Qt/KDE specific issues.
Btw, for this is would be great if the EDS maintainers could put the D-Bus based communication back on their roadmap.
<!--break-->
