---
title:   "Myth Busting"
date:    2009-07-30
authors:
  - krake
slug:    myth-busting
---
Once in a while we come across rumors, urban ledgends and myths about all kinds of things.

For example you might have read, heard or otherwise encountered wild claims about Akonadi's dependencies, maybe even as ridiculous as "depends on KDE".

Don't get me wrong, <b>I</b> don't have the slightest problem with stuff depending on KDE, but one of Akonadi's goals is to be a viable option for all PIM applications and a dependency on KDE would make that quite hard.

So I spent a couple of days updating my Java skills by writing a proof-of-concept Akonadi client in <a href="http://people.freedesktop.org/~krake/akonadi/akonadi-java.tar.bz2">pure Java</a> (re-using the Unix socket adapter written for the <a href="http://dbus.freedesktop.org/doc/dbus-java/">Java D-Bus bindings</a>).

Granted, it is not an extremely impressive client. Its functionality is to "walk" through the Akonadi collection tree, print collection properties to its console output and list each collection's items.

The output for each collection is formatted like this:
<pre>
Name
    identifier (remote identifier)
    list of possible content MIME types
      item identifier (item remote identifier) item MIME type
</pre>

On my test system the Akonadi folder structure looks like this
<img src="https://blogs.kde.org/files/images/akonadifolders_1.png" alt="Folders in Akonadi Console" title="Folders in Akonadi Console"  class="image image-preview " width="427" height="406" />

In the output of the demo app it looks like this:
<pre>
Creating Akonadi data connection at /home/kevin/.akonadi-test/.local/share/akonadi/akonadiserver.socket
Birthdays & Anniversaries
    5 (akonadi_birthdays_resource)
    application/x-vnd.akonadi.calendar.event
      72 (b19) application/x-vnd.akonadi.calendar.event
      73 (b29) application/x-vnd.akonadi.calendar.event
      74 (b52) application/x-vnd.akonadi.calendar.event
      75 (b55) application/x-vnd.akonadi.calendar.event
      76 (b64) application/x-vnd.akonadi.calendar.event
      77 (b65) application/x-vnd.akonadi.calendar.event

Search
    1 ()

akonadi_googledata_resource_0
    6 (google-contacts)
    text/directory
      78 (http://www.google.com/m8/feeds/contacts/abcd%40gmail.com/full/0) text/directory
      79 (http://www.google.com/m8/feeds/contacts/abcd%40gmail.com/full/32b8b2c80dd7cb2d) text/directory

std.ics
    3 (file:///home/kevin/std.ics)
    text/calendar
    application/x-vnd.akonadi.calendar.event
    application/x-vnd.akonadi.calendar.todo
    application/x-vnd.akonadi.calendar.journal
    application/x-vnd.akonadi.calendar.freebusy
      2 (KOrganizer-874394642.152) application/x-vnd.akonadi.calendar.event
      3 (KonsoleKalendar-1123156469.102) application/x-vnd.akonadi.calendar.event
      4 (KOrganizer-313723522.982) application/x-vnd.akonadi.calendar.event
      5 (KOrganizer-1709699137.995) application/x-vnd.akonadi.calendar.event
      6 (KOrganizer-756482139.219) application/x-vnd.akonadi.calendar.event
      7 (libkcal-598331247.452) application/x-vnd.akonadi.calendar.todo

Kolab
    15 (akonadi_kolabproxy_resource)
    inode/directory

    demo.kolab.org/kevin.krammer@demo.kolab.org
        16 (7)
        inode/directory

        My Data
            17 ( 8 )
            inode/directory

            Calendar
                20 (10)
                inode/directory
                application/x-vnd.akonadi.calendar.event

            Contacts
                19 (11)
                inode/directory
                text/directory
                application/x-vnd.kde.contactgroup
                  88 (81) text/directory
                  89 (82) text/directory
                  90 (83) text/directory
                  91 (84) application/x-vnd.kde.contactgroup
                  92 (85) text/directory
                  93 (86) application/x-vnd.kde.contactgroup
                  94 (87) text/directory

            Journal
                18 (12)
                inode/directory
                application/x-vnd.akonadi.calendar.journal

            Notes
                21 (13)
                inode/directory
                application/x-vnd.akonadi.calendar.journal

            Tasks
                22 (14)
                inode/directory
                application/x-vnd.akonadi.calendar.todo

std.vcf
    4 (file:///home/kevin/std.vcf)
    text/directory
      8 (2bpus4eE8c) text/directory
      9 (2efjr21aA) text/directory
      10 (2uun8V066z) text/directory
      11 (3abf3YIm3w) text/directory
      12 (4w20Vzaqu) text/directory
      13 (566ynkrpNY) text/directory
      14 (5JBqjwr9D4) text/directory

demo.kolab.org/kevin.krammer@demo.kolab.org
    7 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/)
    inode/directory

    Inbox
        8 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX)
        message/rfc822
        inode/directory
          80 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX-+-1) message/rfc822

        Calendar
            10 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Calendar)
            message/rfc822
            inode/directory

        Contacts
            11 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Contacts)
            message/rfc822
            inode/directory
              81 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Contacts-+-1639) message/rfc822
              82 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Contacts-+-1640) message/rfc822
              83 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Contacts-+-1641) message/rfc822
              84 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Contacts-+-1643) message/rfc822
              85 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Contacts-+-1649) message/rfc822
              86 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Contacts-+-1651) message/rfc822
              87 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Contacts-+-1652) message/rfc822

        Journal
            12 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Journal)
            message/rfc822
            inode/directory

        Notes
            13 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Notes)
            message/rfc822
            inode/directory

        Tasks
            14 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/INBOX/Tasks)
            message/rfc822
            inode/directory

    shared.common playground
        9 (imap://kevin.krammer@demo.kolab.org@demo.kolab.org/shared.common playground)
        message/rfc822
        inode/directory
</pre>
<!--break-->
