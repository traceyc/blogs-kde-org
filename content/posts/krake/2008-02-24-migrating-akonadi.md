---
title:   "Migrating to Akonadi"
date:    2008-02-24
authors:
  - krake
slug:    migrating-akonadi
---
As <a href="http://blogs.kde.org/node/3286">promised</a> I am going to try to present the work I have been doing over the last couple of weeks in a less developer centric way.

Bascially the idea is to have an intermediate step in moving from the traditional facilities for addressbook and calendar to the future ones based on Akonadi.
This intermediate step should allow developers to migrate both applications and data acesss methods (e.g. groupware server access) one by one, so that new applications can already make use of all the possibilities of Akonadi while at the same time allow existing applications to adapt at their own pace.

Lets discuss the different approaches based on an common use case:

assume you are running KMail and at some time during the day you'd like to change some contact information in your addressbook so you start KAddressBook as well, modify the contact and save the modification.
Of course you also expect KMail to also know about this modification, e.g. if you changed somebody's email address, you want to have this new address when getting autocompletion suggestions.

The following image shows the three setups side by side:
[image:3294 size=preview]
(click <a href="http://blogs.kde.org/node/3294?size=_original">here</a> for full size)

<ul>
  <li><b>Traditional setup</b></li>

The traditional setup is based on plugins we developers refer to as KResource framework.
For KDE's addressbook the default plugin is called "file" which means the contact data is stored in a file called "std.vcf" on your local harddisk.
The applications, in this case KMail and KAddressBook, actually don't have to care about which plugin is used, from their point of view they all look the same.

In the use case described above, both applications read the file and parse the contact data stored in it into objects we developer refer to as "Addressees".
At the time in the use case where KAddressBook saves the modifications, it overwrites the current file with the contact data it currently has stored in its addressee objects, i.e. also "replacing" the unchanged ones.

The "file" plugin inside KMail actually watches this file for changes, so it detects that someone has altered it and basically repeats its loading procedure, i.e. loading the file and parsing the contact data <b>again</b>.

  <li><b>Migration setup</b></li>

The migration setup is quite similar on the upper part of the diagram, i.e. KMail and KAddressBook still use plugins to access the data from a shared source.
However, this shared source is now a process by itself, the Akonadi server. The actual data reading and writing has been delegated to a specialized handler called the VCard resource.

In the use case described above, only this very resource handler has access to the storage file. It loads and parses the contact data similar to how the "file" plugin of the tradition setup did, so not a big difference yet.
Without going further into boring developer details the data is then transferred to the Akonadi server.

As I wrote above the two applications basically don't care which plugin they told to use, so from their point of view there is no difference in used a plugin called "akonadi" and they have no idea that this plugin is getting the data from the Akonadi server.

The main difference to the traditional setup is they way how the modification is handled.
When KAddressBook tells the "akonadi" plugin to save the addressbook it uses an Akonadi facility called ItemSync to only upload the modified addressees to Akonadi. The Akonadi server in turn notifies all interested parties, in this case KMail, KAddressBook and the VCard resource about the change.
In contrast to the traditional setup, the "akonadi" plugin inside KMail now only needs to fetch the modified addressee and only needs to get it from the Akonadi server and not from a storage device like a file.

To summarise the traditional applications can, without any code modifcations, already reap some of the advantages of Akonadi, e.g. fast distribution of local changes (no access to the storage device necessary) and single points of access to storage devices.

  <li><b>Future setup</b></li>

The future setup, i.e. native Akonadi support, looks almost like the migration setup when being abstracted like in the diagram.

The differences are now mainly how the applications can handle the contact data internally, e.g. they are no longer limited to keeping copies of all addressee objects at all time, KAddressBook no longer needs to check which addressee objects to upload to the Akonadi server since it can now upload them individually right after modification.
</ul>

That's all for now.
I am going to write a second part about migration options for data access facilities. Until then stay tuned through Danny Allen's awesome <a href="http://commit-digest.org/">Commit Digest</a>
<!--break-->
