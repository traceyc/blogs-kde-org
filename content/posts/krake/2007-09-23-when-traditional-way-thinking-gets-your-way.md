---
title:   "When the traditional way of thinking gets into your way"
date:    2007-09-23
authors:
  - krake
slug:    when-traditional-way-thinking-gets-your-way
---
Sometimes, usually on weekends, I award myself the luxury of checking websites that <b>might</b> have interesting stuff to read.

Yesterday one of these sites has been Dell's IdeaStorm portal which probably all of you know because of the Ubuntu/Dell stories.

A couple of months after starting to sell Ubuntu pre-installed on selected modells, Dell started a similar campaign in three European countries: German, France and the UK.

One of the IdeaStorm topics that caught my eye was "Europe is not just German, France and UK"

Right, good point!

However, this is where the traditional way of European thinking gets into the way of one's own interest.

Traditionally we Europeans think in countries, borders, differences. We have for centuries.
The couple of years or at most decades we have been building something bigger haven't changed that yet, or not changed enough.

I am not talking about not being patriotic or not feeling pride when "we beat the others" in sports, no I am talking about assuming borders which are no longer present.

True, Dell has chosen three countries where to open a Linux divison, but if for one moment one doesn't look at the countries themselves but rather their primary language, they have opened a Linux division much bigger audience.

If we look at it this way, we got a German, a French and an English store.
While primarily indending to serve customers from the countries they are located in, there is at least one other country with pretty much the same native language, e.g. Austria for the German store, Ireland for the English store and Belgium for the French store (and several offical minorites all over the continent)

Sure, it might be more convenient to deal with a "local" store, e.g. telephone calls being cheaper, but it's not a deal breaker, isn't it?

So I'd like to ask people with said native languages, even if they are not located in one of the three countries, to think a bit about those in our community that are not as lucky, e.g. the Italians or Spanish, and instead of requesting more convenience maybe let Dell focus on actually establishing the service for those who currently do not have access to it.
<!--break-->
