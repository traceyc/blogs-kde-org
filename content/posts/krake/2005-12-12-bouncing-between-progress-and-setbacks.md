---
title:   "Bouncing between progress and setbacks"
date:    2005-12-12
authors:
  - krake
slug:    bouncing-between-progress-and-setbacks
---
I started to have a deeper look into <a href="http://www.freedesktop.org/Software/dbus">D-BUS</a> as I will require an IPC facility if I want to provide a Qt4 API of <a href="http://qds.berlios.de/">QDS</a> and still access KDE services.

So after I installed the dbus-1.0 packages, it played a bit the included tools, starting a session bus and watching output with dbus-monitor.

Then I tried to build a small test application using the Qt3 bindings, but unfortunately the headers didn't include any API documentation (I later found out the docs are in the source files, go figure)

As the version that is currently in Debian SID is quite old, I fetched a checkout from <a href="http://cvs.freedesktop.org/dbus/dbus/">freedesktop.org's CVS</a> but it only contained the same outdated version I had already installed.

<a href="/websvn.kde.org/">Browsing through SVN</a> led me to a work branch directory created by Will Stephenson where he seemed to have started to backport the Qt4 bindings.

Seeing that most files still contained the Qt3 includes I guessed that he has currently no time for that endeavor but I looked like a good idea.

So I quickly copied the contents of my dbus checkout's qt directory over to qt3/ and ...
<!--break-->
*sigh*

.. after fighting with the build system for a couple of hours (until I incidentally discovered it was looking for a different automake version than the one I had installed) successfully managed to get the files to compile after putting most of the Qt4 related code into comment blocks and changing includes to their Qt3 equivalents.

All functionality necessary to do basic sendAndReply() calls was recovered quite easily, but after some initial progress I ultimately failed to directly invoke slots based on the parameters delivered in messages for incoming methods calls.

Well, considering that this requires including private Qt headers it might be a good thing I didn't succeed :)

Limiting the approach to well defined signatures for slots expected for signal and async reply handlers seems to work a lot better. At least for signals, haven't tested async replies yet.

For method calls I am currently test driving an interface (abstract class) based model.

So, I could be at the "progress end" of the bounce before I get my well deserved sleep in a couple of minutes, but no, I had to watch the CPU meter during the last test: my test application takes all available CPU time :(

It seems the socket notifier used to handle the "writing" watch for D-BUS fires continuously, even after all calls and their replies have been processed.

Have yet to figure out why. Hopefully that will be one of the progresses tomorrow.