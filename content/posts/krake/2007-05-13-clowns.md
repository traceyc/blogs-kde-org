---
title:   "Clowns"
date:    2007-05-13
authors:
  - krake
slug:    clowns
---
People like clowns, they are funny!

A clown figure is the image of a very clumsy person, sometimes even stupid. They wear oversized clothes, huge shoes and have ridiculous makeup.
They do things way too stupid for normal people to even consider, their jobs is to be whereever there is a need to from someone to laugh about.

Clumsiness of clowns is so funny, because it is intended, naturally clumsy people are not, because their clumsiness happens accidentally and often creates awkward situations rather than funny ones.

A person who plays a clown is a highly skilled performer, athletic and creative. They can do tricks in costumes normal people couldn't even walk in.

So why all this clown business on an IT blog site?
<!--break-->

Today I came across <a href="http://www.bangkokpost.com/090507_Database/09May2007_data05.php">this article</a> in which a person titled "platform strategy director" makes funny statements about things like "Free software", "Open Source Software", "commerical firms" and relations between them.

Now any reader with a bit of background in IT, especially those with background in software, will immediatelly see that most of the statements and relations are funny, way too stupid for normal people to even consider.

Now, one could assume that this "platform strategy director" figure is not knowing what he is talking about, since as we all know there are quite a lot of such people around.

However, I believe that Bill Hilf is actually knowing about the topics he it talking about. Therefore I conclude he is a clown. A good one! Made my day.
