---
title:   "Akonadi Developer Sprint - Day 1"
date:    2008-11-01
authors:
  - krake
slug:    akonadi-developer-sprint-day-1
---
Day one of the second Akonadi developer sprint for this year started quite early for me, since I had to get up at 5:00 to pack and catch my plane from Graz to Düsseldorf.

I arrived around 11:00 at the venue Tom Albers has organized for us as both a place to stay and work: the extra ordinary fine <a href="http://www.linuxhotel.de">Linux Hotel</a> in Essen, Germany.

The people running this place are exceptionally welcoming and really know how to make Free Software makes feel at home. The place has a quite seminar room equiped with power strips, network switches and plenty of ethernet cables on a sufficiently fast Internet connection, high quality hotel rooms and, no kidding, free coffee, soft drinks and beer (yes, as in free beer).

Volker arrived shortly after me so we had a head start at hacking preparations, e.g. getting ice-cream running.
Over the next few hours Ingo, Thomas, Stephen and Will arrived and after a short introduction too hotel guidelines we went out to get some Pizza.
A short while afterwards, Tom, Bertjan and Igor arrived.

Tom, or rather his vicious cookies managed to pull one of Will's tooth fillings, so he had to visit a local dentist and therefore couldn't join us for dinner in a really nice (but also quit expensive) restaurant.

While the others are still working feverishly, I decided to call it a day, but in the light of free beer (no pun intended, really) opted to resume my laptop under the excuse of providing the hotel with e.V.'s billing address and writing this blog entry.

See you tomorrow!
<!--break-->
