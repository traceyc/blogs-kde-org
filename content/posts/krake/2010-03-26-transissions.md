---
title:   "Transissions"
date:    2010-03-26
authors:
  - krake
slug:    transissions
---
Today was my last day at <a href="www.avibit.com">AviBit Gmbh</a> where I have been working for almost nine years now.
Starting with development of internal tools, becoming maintainer of some of the companies main components and finally designing and supervising implementation of a versatile client framework for our surveillance products.
What a ride :)

Nine years of Qt development for multiple platforms (mainly Linux though), on a KDE powered desktop using my personal favorite editor Kate.

Nine years seeing Linux grow on both servers and workstations in an industry (air traffic control solutions) which has been using proprietary Unix on server and Windows on workstations (though some competitors also have workstation offerings Unix like systems).

It has always been interesting to see how using Qt can give a new player like AviBit a significant advantage in public tenders by reducing time to implement features (lots of code sharing between clients and servers) and by being able to deliver on the customer's platform of choice (or by being able to deliver on the previous suppliers platform of choice).

While I am really looking forward to what I will be working on next, I today felt a bit like after graduating from school, i.e. knowing that people I used to see almost every day and grown friendships with will now become people I occasionally see on purpose or incidentally run into in town.

Since my collegues at AviBit would greatly appreciate somebody taking over parts of the now increased workload, I'll ask anyone who is interested to <a href="mailto:kevin.krammer@gmx.at>contact me</a> for further information.
It is a great company to work for as a graduated software engineer, but also offers students of respective subjects a good opportunity to improve your skills alongside University work.
And the problem domain has obviously ample offerings for all kinds of thesis, including hardware related stuff.

Well then, next stop weekend, three days of vacation and a short first week at my new company overlords: <a href="www.kdab.com">KDAB</a>
<!--break-->
