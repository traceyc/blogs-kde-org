---
title:   "Double trouble..."
date:    2008-05-10
authors:
  - krake
slug:    double-trouble
---
...that's what my friends all call me.

Actually they don't but since I am a huge fan of Lynyrd Skynyrd and this blog entry is closely related to the <a href="http://blogs.kde.org/node/3439">previous one</a> I am (ab-)using yet another of their song titles.

Earlier this week I got rid of the "org.kde" namespace in Akonadi's D-Bus names, e.g. org.kde.Akonadi.Resource is now called org.freedesktop.Akonadi.Resource, to emphasize that we are a cross-desktop project and that the real interaction point for interested parties are the interfaces (and the data transfer protocol), not some specific implementation like libakonad-kde.

Speaking of renaming: I am <a href="http://lists.kde.org/?l=kde-pim&m=121043227310159&w=2">not yet quite satisfied</a> with one of our D-Bus names, so if you have any suggestions I'd like to hear them.

To further facilitate contributions outside the KDE sphere we <a href="https://bugs.freedesktop.org/show_bug.cgi?id=15711">have requested</a> project infrastructure on freedesktop.org
Since this will take a while, let all any interested contributors know that <a href="http://techbase.kde.org/index.php?title=Contribute/Get_a_SVN_Account">getting commit access</a> to KDE's SVN is not an arcane procedure and anyone with a proven track record on another free software project will most likely get approved quickly (I am talking about the order of minutes here).
<!--break-->