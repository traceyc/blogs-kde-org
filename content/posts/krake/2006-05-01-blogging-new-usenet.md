---
title:   "Since blogging is the new usenet"
date:    2006-05-01
authors:
  - krake
slug:    blogging-new-usenet
---
... and Aaron <a href="http://aseigo.blogspot.com/2006/05/stupid-people.html">blogs</a> about an <a href="http://www.murrayc.com/blog/permalink/2006/05/01/munichs-limux-to-use-kde/">entry</a> in Murray Cumming's blog, I feel entitled to reply using my own :)
<!--break-->

Murray fears that from his point of view suboptimal choice will negatively impact the whole free desktop market and while his wording could be intentionally unpleasant about KDE, I guess that he actually believes it.

Being a KDE developer I have only very little GNOME experience, mostly from seeing it running on someones laptop or on a Linux show. However this is obviously only a fraction of GNOME's possible appearances and reflects those people's preference more than the GNOME defaults or flexibility.

Assuming that the sitation is vice versa, Murray might very likely just know shiny geek KDE desktops, not those corporations like credativ or the LiMux contractor deliver to their customers.
Even if he has, for testing or comparison, used KDE himself, he will very likely have used a "default" KDE installation as delivered by most Linux distributions, i.e. all options available, and not one using a corporate style KIOSK policy.

Having seen the LiMux base client in action in one of Florian Schießl's presentations, I can ensure Murray that their contractor knows how to use the flexibility of the framework to reduce the complexity for their target users.

One funny thing I remember is that they used Konqueror's "webbrowser icon" for launching its file manager profile from the desktop :)

