---
title:   "Return of the king^Wdeveloper"
date:    2008-12-31
authors:
  - krake
slug:    return-kingwdeveloper
---
2008 has been a great year for me Free Software wise.

Not only did I get the opportunity to attend three awesome KDE developer meetings/sprints, I also got invited and attended the Linux Collaboration Summit.

However, personally the most important aspect of 2008 is me returning to serious KDE development. Hmm, probably not returning since my contributions in the early years more or less where heling other developers, doing a couple of fixes and enhancements, mainly in the KDE Games and KDE Edu areas.

Due to time being available to me only in small chunks of a couple of minutes but all over the day, I moved on to doing support. Both end user support as well as developer mentoring, the latter mostly on web forums such as <a href="http://www.mrunix.de">mrunix</a> (German), qt-forum and I am actually one of the initial guys behind <a href="http://www.qtcentre.org">QtCentre</a>.

That was a great time as well as I find helping and receiving many people's thanks extremely gratifying more so when I discovered that people within the KDE community where actually aware of my work there, something I discovered after I did a talk on user support at Akademy 2006.
Special thanks to people like <a href="http://lydgate.org/blogs/">Anne"</a> to whom I can trust our help seeking followers.

Anyway, back to topic. Doing only proprietary software development grew boring pretty fast, so I really longed for doing real team work again, working on a vision rather than stupidetly implementing dull specifications.

Some years back I had started to work on a command line client for KDE's address book so I used it as a starting point for searching for new challenges quickly found all I could have dreamed of in <a href="http://www.akonadi-project.org">Akonadi</a>.

I thought that if I were a third party developer using KDE's address book API in my application, I would quite likely be angry if that API would become useless from one day to the other. So I investigated whether it would be possible to make this API work together or on top of Akonadi. The rest is history :)

Hopefully the coming year will allow me to continue working on that and probably other bridges, e.g. for libebook/libecal or Mozilla based products.
<!--break-->
