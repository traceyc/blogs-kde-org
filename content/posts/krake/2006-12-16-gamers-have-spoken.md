---
title:   "The gamers have spoken"
date:    2006-12-16
authors:
  - krake
slug:    gamers-have-spoken
---
As I <a href="http://blogs.kde.org/node/2563">predicted</a>, the offer of game porting specialist Runesoft to do a Linux port of "Ankh" if at least 200 pre-orders could be achieved, has been met with sufficient demand on the side of Linux gamers.

Runesoft has <a href="http://www.rune-soft.com/article.php?action=view&article_id=30">announced</a> that they will be delivering as promised, additional sources hint that the release date might at the year's end or very early next year.

Additionally to the original <a href="http://www.ixsoft.co.uk/cgi-bin/web_store.cgi?ref=Products/de/RSANKH01DV.html">pre-order site</a> at German reseller ixsoft, one can now also order the game at international reseller <a href="http://www.tuxgames.com/details.cgi?&gameref=142">tuxgames'</a> site.
<!--break-->
