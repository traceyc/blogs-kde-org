---
title:   "News from the sideline"
date:    2006-10-27
authors:
  - krake
slug:    news-sideline
---
I have been quite sucessfull with my secondary development projects this week.

The problem of the KDE packages in Debian has been fixed, thanks to the <a href="http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdebase/debian/changelog?op=diff&rev=4774&sc=0">swift response</a> of Fathi Boudra.

Just in case you didn't know yet: Debian's KDE packages are top notch, packages for the enthusiasts!

I managed to tackle the problem regarding espacing UTF-8 sequences in Portland's xdg-email all by myself :)

Supported by valuable feedback from KDE guru David Faure regarding the URI decoding problem, I came up with a hack^Wsolution for the special case in xdg-email, when the session locale is different from UTF-8 but the URI is constructed using UTF-8.
This hasn't been comitted yet as I have been waiting for some feedback, but since it improves the situation on my system (locale encoding being set to ISO-8859-15) I'll commit it tomorrow unless any objection is raised.

Finally, also Portland related, a patch of mine for adding xdg-utils support to one of Wine's tools <a href="http://www.winehq.org/pipermail/wine-cvs/2006-October/027331.html">got committed</a> by Wine's maintainer Alexandre Julliard.

So, off to spreading more xdg-utils mayhem^Wgoodness over the world ;)
<!--break-->
