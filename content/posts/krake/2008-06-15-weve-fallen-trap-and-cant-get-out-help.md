---
title:   "We've fallen into a trap and can't get out, help!"
date:    2008-06-15
authors:
  - krake
slug:    weve-fallen-trap-and-cant-get-out-help
---
So some guy from Nokia named Ari Jaaksi was speaking at some conference and said something along the lines of open source developers needing to be "educated" about consumer restriction technologies like DRM.

While I agree with <a href="http://thunk.org/tytso/blog/2008/06/13/learning-how-to-communicate/">Ted Tso</a> that these words weren't chosen wisely, I believe that the actual message got "lost in translation".

As reported at some point Ari Jaaksi said "...but we are <b>not yet ready</b> to play by the rules..." (emphasis mine), cleary indicating a wish to move beyond the the messed up situation they are currently in, caught by traps like DRM.

To get out they need help from someone who is not trapped and, as Ari Jaaksi indicates, this basically means the Free and Open Source developers on a technical side and the Free Culture communities overall, since anyone in the industry is caught as well.

To help somebody out of a trap one needs to understand the trap and how those in it got caught (or why they willingly submitted into), so I am going to discuss one of the: DRM, Digital Restriciton Management, e.g. technology and techniques to deny consumers the execution of part of their rights.

DRM is a very nasty trap, not only for those effected by it from outside, but also for those caught in it, because it is a cover-up of a lie.
As everybody knows, if one lies, one needs to make sure the truth is not uncovered, because whatever one used the lie for, getting exposed as a liar has usually worse consequences as one's original wrong-doing.

It started when those who get the work of the gifted to distribute it to the consumers, also known as publishers, failed to do their job properly and their profits declined.
Instead of taking the responsibility for losing sight of the customers' demands and letting product quality decrease, they blamed it on the "pirates".

To make sure their lie would not be noticied, they were willing to spend money on whoever was willing to help hold up the fake truth and when money is to be made, there is always somebody willing to take it.

So DRM was born and everybody directly involved knows that it does not work as advertised to those who pay it.
The developers in the companies creating DRM technology know it, their management knows it, the management of their customers (the publishers) knows it, their developers integrating it into the final product know it, however, those who pay, the consumers and publisher sharewholders do not.

There will be different strategies how we, the untrapped, can help them out, assist them in getting out, disarming the traps, etc. and each strategy will have different implications for both sides.
Therefore it is important to learn about the messed up situation these technology providers are in, to understand which strategies might look good from our point of view, but will force them into countermeasures.
They are looking for a way out that lets them leave broken concepts like DRM behind without "losing their face".

I think we should continue to do what we do best: delivering cool, innovative, useful and, most important none-restricting, techology experiences, so customers learn to understand that they do not have to cast away their rights in order to participate in modern life style.
<!--break-->