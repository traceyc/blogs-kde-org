---
title:   "Misconceptions about the Portland Project"
date:    2006-08-26
authors:
  - krake
slug:    misconceptions-about-portland-project
---
There is an <a href="http://applications.linux.com/article.pl?sid=06/08/23/1929240&tid=13">article</a> over at linux.com which predicts that the <a href="http://portland.freedesktop.org/">Portland initiative</a> will fail to reach its goal of "unifying the Linux desktop".

Unfortunately the author somehow missed that "unifying the Linux desktop" is <b>not the goal</b> of Portland.

Moreover, aside from failing to understand what solutions we are aiming for, the article also claims that other projects like <a href="http://www.freestandards.org/en/LSB">LSB</a> or <a href="http://www.freedesktop.org/">freedesktop.org</a> would also be failing to reach it.

I guess I don't have to point out that neither of the latter two project has "unifying the Linux desktop" as its goal either.

Now the question is, why did the author think that Portland aims at this unreachable goal?

Is it some bad wording on the website? Some ambiguity in a press release?

Obviously any project where contributors of different free desktop teams work together will result in some kind of common good, be it a specification or a shared implementation.
However there is quite some difference between doing common stuff in a compatible way and removing all differences. i.e. "unifying the Linux desktop"
<!--break-->