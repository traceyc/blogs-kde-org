---
title:   "KDE PIM at Akademy 2008"
date:    2008-08-16
authors:
  - krake
slug:    kde-pim-akademy-2008
---
Since, at the time of writing, I have another six hours of train journey before me on my way back from Akademy 2008 I decided to spend my remaining battery power on writing a blog about the event.

As an executive summary let it be said that the conference and all activitied during the week were really well organized and I think that every attendee will agree that it was lots of fun.

Both conference days saw Akonadi related sessions. On Saturday Till and Volker did a <a href="http://akademy.kde.org/conference/slides/Akonadi-Akademy-2008.pdf">talk</a> on the current state of Akonadi as released with KDE 4.1.
On Sunday we had the <a href="http://akademy.kde.org/conference/slides/Akonadi-Rumble-Akademy-2008.pdf">Akonadi Rumble</a>, bascially a series of lightning talks about various aspects of Akonadi integration like aggregating PIM contacts and IM (instant messaging) contacts, showing PIM data in Plasmoids, etc.
As far as I know both sessions will be available as video download once the material has been properly processed.

On Wednesday we, the KDE PIM people and distinguished guests, had a BoF about the KDE 4.2 road map for KDE PIM and Akonadi.

Since all developers currently involved will be having less time than they would like to have for working on KDE, we needed to find a suitable procedure which would allow us to work on migration to Akonadi but still enable us just enhance the Akonadi eco system and keep the exiting applications at the traditional data access frameworks.

After discussing a couple of options we came to then conclusion that the "emergency stop" requirement basically dictates that application can still access the KResource based data handling framework, i.e. doing what they do right now in KDE 4.1

So the current plan is to concentrate on the data migration first, i.e. making at least the user's contact and calendar data accessible through Akonadi.

This requires a tool which takes the user's KResource configuration and creates respective Akonadi resources and then modifies the KResource configuration to point to Akonadi so that applications using the traditional framework will continue to work as intended.

When I last talked to Volker he was close to finish coding it.
Since the resulting changes have quite some impact, a considerable amount of testing is still needed to ensure it works with kinds of user setups. 

My responsibility over the next few months will be to enhance and subsequently stabilize the Akonadi<->KResouce bridges I wrote about in a couple of previous blogs entries (or links see end of blog).

This will allow our application developers to concentrate on refactoring their respective applications to ease an Akonadi port later on or help on porting KResource based data sources to Akonadi.

While speaking about resource porting: improving existing Akonadi resources and, to a somewhat lesser extent, porting KResource plugins to Akonadi are almost ideal entry points into KDE PIM because resources are relatively small programs (often only a handfull of files/classes) and independent from each other.
See our <a href="http://techbase.kde.org/Development/Tutorials/Akonadi/Resources">Akonadi Resource Tutorial</a> on an implementation "walk-through".

If you are interested, find us on IRC channel <a href="irc://irc.freenode.net/akonadi">#akonadi</a>, on the <a href="mailto:kde-pim@kde.org">kde-pim mailing list</a> (freedesktop.org project creation still pending) or send me a <a href="mailto:kevin@akonadi-project.org">private mail</a>

Older blog entries related to migration facilities
<ul>
<li>developer oriented:
  <ul>
    <li><a href="http://blogs.kde.org/node/3270">Akonadi KResource plugins</a> (application side plugins)
    <li><a href="http://blogs.kde.org/node/3286">KResource Akonadi resources</a> (backend side plugins)
  </ul>
<li>same two topics, less technical details but with diagrams :)
  <ul>
    <li><a href="http://blogs.kde.org/node/3295">Migrating to Akonadi</a>
    <li><a href="http://blogs.kde.org/node/3303">Migrating to Akonadi, Part 2</a>
  </ul>
</ul>
<!--break-->
