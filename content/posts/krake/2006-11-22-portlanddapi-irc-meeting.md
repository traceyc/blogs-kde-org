---
title:   "Portland/DAPI IRC meeting"
date:    2006-11-22
authors:
  - krake
slug:    portlanddapi-irc-meeting
---
We had an IRC meeting about Portland's DAPI today and while most people seemed to have joined to discuss API related things, it more or less turned into a session to clarify positioning in relation with other projects cleaning up some misconceptions.

Lets quote from the log
<pre>
&lt;thiago> I'm going to be bold and just say my opinion.
&lt;thiago> the way I see it, DAPI has a three-fold purpose:
&lt;thiago> 1) define D-Bus interfaces -- that is, XML files with documentation, pure and simple
&lt;thiago> 2) implement a library that wraps around libdbus-1 and makes those calls in an easy fashion
&lt;thiago> 3) implement a daemon that handles the receiving side of the calls for desktops that don't provide such an interface by themselves
&lt;thiago> s/interface/service/ on the last line
&lt;thiago> I suppose that almost no one will agree with me here. So, what are your opinions?
&lt;krake> No, I agree
</pre>

As I wrote on IRC, I fully agree!

Points #1 and #3 required some additional discussions later on:

<b>Interfaces</b>

Currently there is only one <a href="http://webcvs.freedesktop.org/portland/portland/dapi-dbus/data/dapi-command.xml?view=markup">single file</a>, covering all available methods of DAPI at this point.

However, this is merely a result of converting the <a href="http://webcvs.freedesktop.org/portland/portland/dapi/doc/API.txt?view=markup">already existing</a> API spec to D-Bus, for showing that D-Bus is a viable option for our IPC needs.

The goal is of course to have separated files, each covering an aspect of the DAPI tasks, i.e. one for launching (opening files/URLs in the user's preferred application), one for screensaver handling, and so on.

<b>Daemon</b>

There is no requirement for an additional DAPI daemon if the desktop's infrastructure already implements all DAPI interfaces.

There were a couple of people worried that DAPI would require a "man in the middle" even when all functionality would be available through the environments already running processes.

After some discussion we arrived at the conclusion that a DAPI daemon would only complement desktop provided functionality, for example when the runtime infrastructure cannot provide D-Bus access.

<b>Project's position towards others (e.g. freedesktop.org)</b>

A couple of people were afraid that Portland would be competing with the general freedesktop.org work.

As written above, part of the DAPI work is specifying D-Bus interfaces and, since D-Bus is now also available for KDE, freedesktop.org work has also included such kind of specifications, i.e. agreeing on a set of D-Bus methods to do certain things.

While this looks like competing efforts, it is more an overlapping.

Any relevant freedesktop.org specification, which can be suitably implemented with the already released functionality (e.g. KDE3.5), won't require a separate or duplicated DAPI specification.

As an example, controlling the screensaver looks like a good candidate of an official freedesktop.org specification which can be used verbatim by DAPI.

Only if a freedesktop.org specification is to hard or impossible to implement on older desktops, then there will be a need for Portland to create its own specification.

Additionally, if there is no suitable freedesktop.org specification yet, a specification suiting DAPI's needs might be useful as a test bed for a shared specification for not yet released desktop software, i.e. a future freedesktop.org specification.
<!--break-->
