---
title:   "Akonadi Workspace Integration"
date:    2010-07-17
authors:
  - krake
slug:    akonadi-workspace-integration
---
With Akonadi most operations are running behind the scenes, carried out by background helper processes called Akonadi Agents.

While we do have respective progress monitoring in KMail2, users will eventually take advantage of fact that they are no longer tied to specific applications. At which point they might want to be able to check on the status of these background processes without launching some front end applications.

<a href="http://blogs.kde.org/node/4218">Back in April</a>, during one of our development sprints, I've created a Plasma DataEngine which provides information about running Akonadi agents.

I was kind of hoping that somebody with actual widget skills would be curious enough to try some Plasma widgets on top of it, alas this didn't happen.

Therefore I sat down today and wrote one myself, using the opportuntiy to also have a first real attempt in doing some KDE<->JavaScript coding.

The code for it more or less looks like this:
<pre>
layout = new LinearLayout( plasmoid );
layout.orientation = QtVertical;

engine = dataEngine( "akonadiagents" );
agents = engine.sources;

resources = new Array;

addAgent = function( name ) {
  label = new IconWidget();
  label.orientation = QtHorizontal;
  layout.addItem( label );
  resources[ name ] = label;

  engine.connectSource( name, plasmoid );
}

plasmoid.dataUpdated = function( name, data ) {
  label = resources[ name ];

  label.text = data.name;
  label.icon = data.typeIcon;

  label.infoText = data.statusMessage;
  if ( !data.online ) {
    label.enabled = false;
  } else {
    label.enabled = true;
    if ( data.status == 1 ) { // running
      label.infoText = data.statusMessage + " (" + data.progress + "%)";
    }
  }
}

while ( agents.length > 0 ) {
  agent = agents.pop();

  // ideally this would be using the agent type's capabilities, but the DataEngine::Data as returned by
  // DataEngine::query() is not accessible from within JavaScript (or at least nobody on #plasma knew)
  if ( agent.indexOf( "resource" ) != -1 ) {
    addAgent( agent );
  }
}
</pre>

I am sure this is not very pretty from the point of view of skilled JavaScript users, you are welcome to beat me on that :D

Anyway, <a href="http://blip.tv/file/3894382">screen cast</a> of the Plasmoid in action, as usual on <a href="http://blip.tv">blip.tv</a>

It shows the widget running in plasmoidviewer side by side with Akonadiconsole to demonstrate that the data engine really exposes the same data.
I start with toggling a local VCard file resource between Offline and Online state, which the widget visualizes by disabling/enabling the respective item.

Finally I synchronize all collections of the IMAP resource, showing status and progres reporting.
<!--break-->
