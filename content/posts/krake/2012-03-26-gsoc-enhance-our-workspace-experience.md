---
title:   "GSOC: Enhance our workspace experience"
date:    2012-03-26
authors:
  - krake
slug:    gsoc-enhance-our-workspace-experience
---
Since so many students have already shown interest in working for KDE during this years Google Summer of Code, with some already having started to send in first drafts of their proposals, I thought I'd a bit of advertising for a <a href="http://community.kde.org/GSoC/2012/Ideas#Project:_Plasma_Workspace_Integration_for_Akonadi">specific idea</a>. Well "my" idea :)

It has been mostly ignored so far, i.e. not getting a lot of obvious attention, probably because it is "hidden" in the KDE PIM section and not listed in one of the section that guarantee eternal fame.

So here I am attempting to correct this misconception: this idea is mostly about creating workspace components, e.g. Plasma applets, for information made available by KDE's PIM infrastructure.

Currently almost all display, input and managment of PIM data happens through their respective applications, e.g. receiving, reading, writing and sending mail through Kontact Mail, applications which are of course designed for that tasks but not always strictly necessary.

Lets take for example a person with a mobile computing device of some sort, e.g. a traditional laptop or a tablet running KDE Plasma Active. Such a user might like to have a quick way to prevent all or certain email accounts from being checked for new mail (current Internet connection could have traffic limits) or specifically want to synchronize with a particular server-based calendar right now.

Launching the respective application would work but might not be the quick and easy way the user would like to have.

All information and applicable actions are available outside those applications, what we need is a way for users of our workspace product to interact with them.

So I sat down and, with the enormous help of Viranch Mehta (who wrote the pure QML based UI), created a prototype for such an interaction point

<a href="http://blogs.kde.org/node/4548">
<img title="Status icon hidden by default" src="http://blogs.kde.org/sites/blogs.kde.org/files/images/agentapplet-systray_0.png"></a>

<a href="http://blogs.kde.org/node/4549">
<img title="Has access to the same status data as developer tools" src="http://blogs.kde.org/sites/blogs.kde.org/files/images/agentapplet-overview.preview.png"></a>

<a href="http://blogs.kde.org/node/4550">
<img title="Can show activity and progress" src="http://blogs.kde.org/sites/blogs.kde.org/files/images/agentapplet-progress.png"></a>

<a href="http://blogs.kde.org/node/4551">
<img title="Can trigger actions" src="http://blogs.kde.org/sites/blogs.kde.org/files/images/agentapplet-actions.png"></a>

The suggested GSOC work would be to take that prototype and existing workspace integration solutions such as <a href="http://vizzzion.org/blog/2010/09/getting-email-done-the-stack-and-the-heap-of-lion-mail/">Lion Mail</a> and build an experience that makes contacts, emails, events, todos and notes first class items on our workspaces and provides us as users with means to check and control what all these little busy helpers do when we need to know or need to make decisions for them.

Hint for interested students: think first about what status information people are usually interested in (more specific hint: e.g. new/unread mails, calendar reminders) and only then do research into displaying data.

Additional suggestion: have primarily a look at Plasma related technologies, this is where most of your tasks will come from.

<!--break-->