---
title:   "Akonadi Developer Sprint - Day 2"
date:    2008-11-02
authors:
  - krake
slug:    akonadi-developer-sprint-day-2
---
As usual the middle day of a developer sprint is the most productive one since all people are here the whole day.
We managed a couple of our agenda items in the sense of discussing the current state, roadmap and delegating jobs to those who still have time to do something beside their own important TODOs :)

Since Tom asked us to send him an overview of what we worked on today for <a href="http://www.omat.nl/drupal/content/Akonadi-meeting-day-2">his blog</a> I'll use mine to give a bit more detailed insight into the topic of KResource migration.

One of the remaining issues in feature coverage is (email-) distribution lists. Tobias already wrote most of the formatting and parsing code which can be use to both read/write to/from files and as the transport format between Akonadi clients and the Akonadi server.
So I mainly worked on integrating that and trying to make the address book plugin map between its native distribution list structures and these new ones.

Volker and Thomas spent a lot of time testing and improving the migration itself, i.e. the automated task of transforming a traditional setup of address book and calendar resources into an Akonadi based one.
During the tests they also discovered, pinpointed and fixed bugs which were unfortunately still in the Akonadi compatibility bridges.

So all in all a very successful day and I am quite confident we can continue this tomorrow.
<!--break-->
