---
title:   "100% mimelib free"
date:    2009-11-29
authors:
  - krake
slug:    100-mimelib-free
---
If you have no idea what this means, don't worry, neither do I.

What I do know, however, is that a lot of people around KMail and are extremely happy about this :)

Basically the folks working hard on porting KDE PIM apps to Akonadi have reached one of their bonus mission goals: they've got rid of a very old, very obscure, tedious to maintain, mindboggling to work with (you get the picture, right?) legacy part of the mail handling framework.

Well, I guess bonus goal might give the wrong impression here. It is not less important than any of the primary mission objectives (to stay in game jargon). It is a bonus because it makes other things easier, boost your enthusiasm, lets you look forward for the challenges to come.
Think in terms of finding the double barrel shotgun in Doom, getting the Holy Grenade in Worms, looting an epic item in WoW, getting super cow powers in Aptitude.

So, congratulations to the folks at KDAB. You rock!
<!--break-->
