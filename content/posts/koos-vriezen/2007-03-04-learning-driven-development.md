---
title:   "Learning driven development"
date:    2007-03-04
authors:
  - koos vriezen
slug:    learning-driven-development
---
At Fosem when asked about motivation developing OSS, it's learning in my case.
I started the XML engine in KMPlayer when in my previous job, between projects,
I was reading some docu's about JavaEE, and getting bored reading I started
thinking about an implementation. Now there where some bug reports about not
supporting certain links, that turned out to be crappy ASX/broken XML files, I
started writing a failure proof XML parser, replacing pre parsing hack to make
QDom happy.
It grew a bit beyond former expectations to also support more XML playlist like
XSPF, podcast and SMIL. Now SMIL is big, so that will keep me busy some time, but
I just fix/implement real live links. Now SMIL is a bit old fashion and I believe
to be super seeded with HTML+CSS+JavaScript or flash. Nevertheless, moving KMPlayer
in other directions than the other KDE mm-players, I kept on working on it.
Latest edition got the cairo painting implementation. Painting was always crappy
till then and I was delaying the real fixing to later, eg Qt4 has this nice
graphicsview.. Anyhow, my current colleague Frerich Raabe introduced me with the
visitor pattern that, at first looked a bit inefficient, has this nice benefit to
separate a particular functionality in a single class. Together with the porting
of KMPlayer to the Internet tablet and a thread on the mailing list about cairo
compared to other painting engines, this was a nice way to learn about the behaviour
of visitors.
Two visitor classes are now used, one for painting and one for mouse events. And
yes, it really is a cleanup.

There are more things on my todo list, I only need a trigger to start implemented it.
Now what's there more to learn ...