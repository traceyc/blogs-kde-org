---
title:   "Porting kmplayer to N770"
date:    2005-12-18
authors:
  - koos vriezen
slug:    porting-kmplayer-n770
---
Being in the N770 developments program and their great SDK/emulator, I really couldn't resist to try some programming for myself. Obvious choice is of course a port of kmplayer. The SDK is great and has both an i386 and arm target. The arm target uses qemu and is not capable of running GUI clients, but does run the console stuff, like configure scripts!!, this way.
Having a 220MHz arm + 220MHz dsp dual core CPU and 64Mb of memory makes this device something that was standard about 8 years ago. The dsp chip adds additional video/audio decoding. Nokia added herefor a few closed source gstreamer modules.
I haven't been able to pipeline those properly so that they would play video. But I guess that should be possible, also because I've managed to get mp3 playing with it.
Given a pda with wifi/bluetooth 800x480 px sreen and 64Mb RAM/128Mb flash plus rs-mmc extention slot, I makes sense to go for a full blown x-server. They've chosen for gnome, though stripped down versions. Actually I don't know what's gnome about this because it mainly gtk/d-bus/gstreamer/gnome-vfs/.. stuff plus a so called hildon application framework (but what do I know on this subject). I don't think there is an alternative given this hardware other then using framebuffer instead of X.

So this is a nice challenge for me. The internal playlist handling in kmplayer is pretty gui independent. So my first try was to make a split view with a tree showing some XML file.
Now that was quite different with how it went with KDE. Using a tree is quite complicated in GTK. They have this model/view, that I happen to now from java swing and quite like it too. However, this is really difficult for adding your own model. Even the docu's recommend using the predefined treestore, which eventually I did.
So I need a tree with icons/text items. With Qt that's just simply found in the QListViewItem class, but how should this all work in GTK? After some reading I finally found this case, half way their tutorial.
This is really a downside of using GTK. Though the API are documented, the cohesion code examples are often hard to find. Of course there is always the source. Nevetheless, Qt is quite a time saver.
The playlist code using typical Qt/kde objects like QString/KURL/.., so I had to write GTK versions of these classes. I used template wrappers and removed Q/K from names, so in principle it can be used for other toolkits or even Qt itself.
Next challenge is to make the smil animations work. This means diving into gnome-vfs/gdk/pixbuf/pango/glib timeouts. This went pretty smooth actually.
I have some limited video support by using nokia's own closed-source  osso-media-server. That serves both nokia's video- and audioplayer. Although the use of this is undocumented, with some dbus monitoring and stracing, I got enough clues to make it more or less work. Already noticed when playing with the gst modules, playing with video can really crash the system.

So what does it gain for me? This device doesn't really need another video player. What it does need, and work is done for it, is things like SIP client, GPS/route, ie. network tools.
I guess that trying out another toolkit can't hurt. Maybe this is a start for plugins for GTK-based browsers. But most of all, I've got my own application always in my pocket. And who says that everything has to be useful :-). Because kmplayer looks and reacts different on N770 that I got the idea to make the playlist editable in raw XML. Also because it's not so fast as my 2.4GHz P4, kmplayer really needs a new way to setup timings in smil.