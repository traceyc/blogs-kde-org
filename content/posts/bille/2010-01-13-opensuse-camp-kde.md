---
title:   "openSUSE at Camp KDE!"
date:    2010-01-13
authors:
  - bille
slug:    opensuse-camp-kde
---
At the last minute I'm getting away from the snow and ice to visit Camp KDE in San Diego this weekend.  I'll be there waving the openSUSE flag, giving a talk about using the Build Service to package and distribute your KDE applications for many Linux distributions, generally enthusing people about openSUSE and thinking about ways for KDE to be better as distributed.  So if you see a guy with an SUSE t-shirt on staggering under a huge pile of DVDs, say hi!  I hear San Diego has a very good zoo, perhaps they'll lend me a chameleon for even better recognisability...

<img src="http://blog.cryos.net/uploads/campkde2010_logo.png"/>
<!--break-->
