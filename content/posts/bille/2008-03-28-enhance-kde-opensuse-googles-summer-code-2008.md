---
title:   "Enhance KDE on openSUSE for Google's Summer of Code 2008"
date:    2008-03-28
authors:
  - bille
slug:    enhance-kde-opensuse-googles-summer-code-2008
---
Student?  Love KDE and/or openSUSE?  Want to get 0x1194 bucks for improving them?  Then check out the <a href="http://en.opensuse.org/Summer_of_Code_2008">openSUSE Google Summer of Code ideas page</a> or suggest your own project.  There are a  number of projects listed already which would improve KDE on openSUSE and upstream.  As well as getting paid, it's an opportunity to work on a real world project, and learn from the experience of some leading KDE and openSUSE developers.

But don't delay, the <a href="http://code.google.com/opensource/gsoc/2008/faqs.html#0.1_timeline">deadline is on Monday March 31 midnight UTC</a>.
<!--break-->