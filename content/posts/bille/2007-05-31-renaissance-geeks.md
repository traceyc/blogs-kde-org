---
title:   "Renaissance Geeks"
date:    2007-05-31
authors:
  - bille
slug:    renaissance-geeks
---
Something I read on <a href="http://aseigo.blogspot.com/2007/05/building-brand.html">aaron's blog</a> just now struck a chord with me, and cut through my morning head haze nicely.  We are the renaissance geeks.  Some of the stuff we do is indescribably technical and abstract, but it all has the end of increasing general utility, that is, by letting people use their computers in ways that grow their happiness and productivity.  

So programmers, coders, software engineers and hackers through the ages have all written good software, but the renaissance is that the stuff that KDE and the rest of the free software crowd produce is different - it's Free.  By putting your efforts into its production you are creating something permanent, that can grow and be the feedstock for others' contributions long after you have put it aside.

That to me is the most powerful aspect of what we do.  It's something we personally know already, but don't forget it and let it get buried under piles of bugs and complaints - it's something we can all be proud of and tell the world about.
<!--break-->