---
title:   "System Settings gets Interviewed"
date:    2007-12-23
authors:
  - bille
slug:    system-settings-gets-interviewed
---
I decided a couple of weeks ago to sort System Settings out and here is the first result:  I reworked the views using Qt4's InterView framework, reusing the KCategorizedView from Dolphin.  This gives us a better quality view than the hard coded view used until now - item layout should work at large font sizes or high res displays.  It will also make it much easier to improve the UI with custom delegates and category drawers in future - just by reusing code.  I was able to chuck out a couple of classes entirely which will make System Settings easier to maintain.  

There are a number of things still to do, at the moment KCategorizedView always draws a rubberband, and the number of search hits are not displayed, but it's usable already and these will soon be fixed.

Here's the screenshot:

[image:3168 id align=center hspace=20 vspace=20 border=0 width=437 height=413 node=3168]
<!--break-->