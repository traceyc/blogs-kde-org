---
title:   "Saturday thoughts"
date:    2009-02-14
authors:
  - bille
slug:    saturday-thoughts
---
<a href="http://files.opensuse.org/opensuse/en/7/70/OpenSUSE-KDE-FOSDEM.pdf"><img src="https://blogs.kde.org/files/images/kde4transition.png"/></a>

I'm drinking my Saturday morning tea and looking back on a pretty crappy week.  I was burning the candle at both ends to get various enterprise stuff finished in time, and didn't have enough energy to really enjoy FOSDEM.  Here are the <a href="http://en.opensuse.org/FOSDEM2009">openSUSE devroom slides from FOSDEM 2009</a>.

Looking forward, I've been tidying up my computers, installing openSUSE Factory (the alpha0 edition before anyone knows when 11.2 will really be done, and before everyone starts breaking things in earnest), deleting dozens of Build Service checkouts that were finished or forgotten about, and purging my unorganised piles into nice clean GTD lists.  That's giving me some peace of mind to think about what to do for KDE on openSUSE 11.2.  We'll be having an IRC meeting next Wednesday (1700UTC) to coordinate the team's efforts, but I'm starting to think about things I could do myself.  Things like a return for KPersonalizer, a KControl-like treeview for System Settings, or helping tame the Plasma Activities/Zooming UI system into something usable.  That and of course completing Network Management (oh, did I let slip the name we chose?).  If anyone is already working in those areas, please let me know.

EDIT: Oh and I should point out that openFATE is of course open for business and waiting to receive your ideas.  Find out <a href="https://features.opensuse.org/documentation">how to use openFATE here</a>.
<!--break-->
