---
title:   "Update - GSoC application period extended, another project idea"
date:    2008-04-01
authors:
  - bille
slug:    update-gsoc-application-period-extended-another-project-idea
---
Google <a href="http://code.google.com/soc/2008/">have extended the Summer of Code 2008 student application deadline</a> until April 7 so if you were busy last week or concerned your application wasn't good enough, now's your chance to get it in.

And I took the opportunity this morning to add an idea that we could really use on the Free desktop - <a href="http://en.opensuse.org/Summer_of_Code_2008#Synchronizing_Wallets.2FKeyrings_between_computers">a way to sync wallet secrets between different computers</a>, and maybe between KDE and other secure stores like GNOME keyring and the Mozilla password manager.   If you think you're hard enough, please apply.  It's a non-GUI job, and involves getting your hands dirty with various APIs and solving some tricky problems - just the kind of skills needed to make yourself useful as a professional Linux hacker.
<!--break-->