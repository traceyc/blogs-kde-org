---
title:   "Sculpting Solid"
date:    2006-09-29
authors:
  - bille
slug:    sculpting-solid
---
Most of my development effort at Akademy this year has been on the network management part of Solid.  I gave a <a href="http://conference2006.kde.org/conference/talks/16.php">talk on the present and future of KDE network management</a> and this went pretty well.  This year I learned not to even bring my laptop near a projector and used Aaron's instead.  Before giving my talk I had been worried that the content was a bit obvious but I wanted to go through how to monitor and respond to changes in connectivity in KDE 3 and 4 so that app developers can give their users a better time when the network goes a way, so i just presented everything step by step and it was pretty well received.

After the talk I just had to convert words to deeds, and that is about to become visible when <a href="http://ervin.ipsquad.net/index.php">Kévin Ottens</a> merges <a href="http://solid.kde.org">Solid</a> into kdelibs today.  One more bit of KDE 4 coalescing and dropping into place.  Together we've got the API documented, sanity checked and improved in several places, and we have a fake backend ready that can be used to develop Solid based network management user interfaces without dealing with the whims of a real networking subsystem.  

Next up, a DBUS+NetworkManager backend...

