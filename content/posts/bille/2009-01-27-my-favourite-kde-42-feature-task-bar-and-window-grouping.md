---
title:   "My favourite KDE 4.2 feature: Task Bar And Window Grouping"
date:    2009-01-27
authors:
  - bille
slug:    my-favourite-kde-42-feature-task-bar-and-window-grouping
---
I was going to make it "Konsole Tabs Are Session Managed Again" but it's a restored KDE 3 feature and all the kids know that commandline hacking is not cool. 

So instead here's one that I found about just today while proofreading the Visual Guide to 4.2 and one that made my heart leap.  Are you someone who likes to control your desktop?  Have everything organised just the way you like it?  I am, that's why I've been using KDE for *gulp* 10 years this year.  I used to think I was intrinsically chaotic, but after a while I found I could be a lot more productive if I keep related apps together.  At first I did this with many virtual desktops.   But now KDE has found a way to give me EVEN MORE CONTROL on a single desktop, and without making anything else more complicated.

Here's a screenshot tour of this cool new feature.
<!--break-->
The default taskbar grouping setting is 'By Program name, when taskbar full'.  This does the job 90% of the time.  However, sometimes tasks relate to one another that are from different applications.

<a href="http://blogs.kde.org/node/3854?size=_original"><img src="https://blogs.kde.org/files/images/snapshot1_0.preview.png"></a>

Here's a screenshot of my X60 hard at work producing a highly complex KDE application using Qt Designer.  I also have a text file open in KWrite where I've noted my plans and Vim running in a Konsole to actually write the code (had to get a Konsole in somewhere!).  But what about all these windows?  Do I actually want to be presented with all Designer's SDI window tabs when I'm cutting the code?  No!  Is Assistant's icon too similar to Designer's, so I click the wrong thing?  Yes!

Fortunately the restored task grouping feature of Plasma's Task Manager applet lets me do anything I could with KDE 3, and even more.  First, we switch the Grouping setting to 'Manual'.  To get to the Task Manager Settings, you click on an empty area of the taskbar.  If the taskbar happens to already be full of windows, tough!  Finding an empty area is hard.  This needs improvement.  There is an empty pixel between each task, see if you can find it.  

<a href="http://blogs.kde.org/node/3863?size=_original"><img src="https://blogs.kde.org/files/images/snapshot0.preview.png"/></a>

Now we can group our tasks together.  For wrangling the UI, I want the Designer windows AND the DESIGN KWrite window with my master plan.  For actual coding, I want Konsole, Qt Assistant, and Konqi for the KDE API docs (I really should get round to generating them in Assistant format). 

To group a task, alt-drag and drop it onto another task.  The task label changes into 'GroupN' with an arrow indicating it can be expanded and a tiny number indicating how many tasks are in the group.  Drag, drag, drag, done.

<a href="http://blogs.kde.org/node/3855?size=_original"><img src="https://blogs.kde.org/files/images/snapshot2.preview.png"/></a>

As you can see, a left click expands the group.

<a href="http://blogs.kde.org/node/3856?size=_original"><img src="https://blogs.kde.org/files/images/snapshot3.preview.png"/></a>

<a href="http://blogs.kde.org/node/3857?size=_original"><img src="https://blogs.kde.org/files/images/snapshot4.preview.png"/></a>


GroupN doesn't tell me anything though, let's change that.  Right-click the group and select 'Edit Group' - someone forgot the '...' there that indicates this opens a dialog.

<a href="http://blogs.kde.org/node/3858?size=_original"><img src="https://blogs.kde.org/files/images/snapshot5.preview.png"/></a>

<a href="http://blogs.kde.org/node/3859?size=_original"><img src="https://blogs.kde.org/files/images/snapshot6.preview.png"/></a>

So that's 2 groups, 'User Interface' and 'Code'.  Want to see what's inside one?  You could right-click as I showed you already, or you can middle-click a group and it pops open.  

<a href="http://blogs.kde.org/node/3860?size=_original"><img src="https://blogs.kde.org/files/images/snapshot7.preview.png"/></a>

Another middle-click and it springs shut again.  Don't get your mouse pointer caught!

<a href="http://blogs.kde.org/node/3861?size=_original"><img src="https://blogs.kde.org/files/images/snapshot8.preview.png"/></a>

Now let's start using these groups to tidy up my hacking session.  Right-click the Code group, click Minimise or press 'N' (on an X60 your fingers are never far from the keyboard) and all the User Interface windows are gone.  Get the UI finished, switch to our Code group, job done.
 
<a href="http://blogs.kde.org/node/3862?size=_original"><img src="https://blogs.kde.org/files/images/snapshot9.preview.png"/></a> 

What else does the humble task bar hold in store for us?  I don't know, I didn't work on it, but here are my wishes:

* Left-click a group should react like left clicking a single task - minimise or restore all tasks.
* Double-click to do an inline rename on the group item.  Probably that should be consistent with the configured setting for double-click on a window title bar instead, so Alt-double-click would continue the 'Alt means groups' theme.
* Session managed groups.  Yes, since suspend to ram became reliable I hardly logout any more, but a monthly Leave, Logout to get the latest KDE point release is conceivable.  And I want my session back.  I'm beginning to feel like a spoiled brat here.
* The same groupings in the Alt-tab switcher.
* Semantic activity goodness.  I want to be able to define my activities desktop wide.  And then assign everything that matters to them, task groups, panel layouts, notification intrusiveness.  You don't want to interrupt me with an IM while I'm packaging kdepim4 for openSUSE, I might slip and exclude all the Kontact plugins from the RPM.

I think that's enough.  Enjoy KDE 4.2.