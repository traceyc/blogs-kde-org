---
title:   "openSUSE 11.0 kdepim4 update strategy"
date:    2008-06-25
authors:
  - bille
slug:    opensuse-110-kdepim4-update-strategy
---
<p>openSUSE 11.0 is out and with it are a set of KDE PIM (Personal Information Management) packages from KDE 4.1.  This is part of the KDE 4 desktop choice and they have been extensively patched to work with the KDE 4.0.4 libraries on 11.0, and contain backports from KDE 4.1 trunk.  They have been tested thanks to our devoted opensuse-kde community, but fixes are still happening in trunk so here's the skinny on what will happen via online update as we approach KDE 4.1:
<p><ul>
<li>openSUSE 11.0 released with kdepim 4.1beta1 <b>DONE</b></li>
<li>post-gold-master online update containing selected critical bugfixes <b>DONE</b>.  This fixes some ugly bugs (kde#153740, #156319, #156990, #158354, #162673, #162707, #162711, #162897, #163159, #163268, and #163408, and the infamous bnc#398807 Kontact plugins missing beastie - we like to take care of our users</li>
<li>pre-4.1 update when we feel 4.1beta2 has settled down to be an improvement over the above.</li>
<li>Update to 4.1 final upon release</li>
</ul></p>
<p>Anyone using the KDE:KDE4:Factory:* repositories in the buildservice can ignore this, as they are updated roughly weekly.