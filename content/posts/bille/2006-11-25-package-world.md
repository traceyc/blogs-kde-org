---
title:   "Package The World"
date:    2006-11-25
authors:
  - bille
slug:    package-world
---
I was feeling a bit poorly today so I decided to spend the day doing something fairly easy but productive and beneficial for openSUSE: re-familiarising myself with the <a href="http://build.opensuse.org">Build Service</a> by packaging some software.  

Since I'd not packaged anything since some Kopete 0.12 prereleases, I was feeling a little guilty by not using this resource.  Those feelings were assuaged by updating the packages for <a href="http://www.frozen-bubble.org">Frozen Bubble</a> to 2.1.0 - it's the craze sweeping the KDE team at SUSE this week - and <a href="http://ktorrent.org/">KTorrent</a> to 2.1beta1.  

<a href="http://en.opensuse.org/Build_Service/CLI">Osc - the openSUSE commander</a> has improved a lot since I last tried and it was very easy (with its SVN-like interface) to check out packages, do the usual specfile and diff fettling needed to get a local build (osc build) to compile and then upload the updated package into the Build Service.  Next up, packaging <a href="http://www.tehbisnatch.org/">Michael Larouche</a>'s <a href="http://kamefu.pwsp.net">gamefu</a> emulator frontend - or will I hack MSX support into it first?  <!--break-->