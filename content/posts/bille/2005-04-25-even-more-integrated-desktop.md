---
title:   "The even more integrated desktop"
date:    2005-04-25
authors:
  - bille
slug:    even-more-integrated-desktop
---
<p>Since <a href="http://www.licq.org">Licq</a> got <a href="http://blogs.kde.org/node/view/987">integrated</a> with the rest of the KDE desktop using KIMIface (nice work Kevin), it's inspired me to make a couple more changes to improve our integration.  Now it's easier to link IM contacts with people in your address book, and you can use your calendar to switch your IM presence.</p>
<p><img src="http://www.stevello.free-online.co.uk/kopete/pimpresence.png" class="showonplanet"/></p>
<!--break-->
<p>Firstly I improved the Export Contacts feature in Kopete so that it now exports email addresses, phone numbers and contact photos when it creates addressbook entries for selected contacts in the list.</p>
<p>Additionally, I hacked together a new Kopete plugin at the weekend, that can automatically set you Away when you have an appointment in your addressbook:</p>
<p>I've just committed it in kdeplayground-network for the moment, as it's still very alpha - the config UI doesn't work yet, and no-time-associated events are ignored.</p>
<p><img src="http://www.stevello.free-online.co.uk/kopete/pimprefs.png"/></p>
<p>I might yet see if changing status can be made a KIMIface function while preserving BIC, that way we would retain the loose coupling and Kontact could switch all KIMIfaces together (no Kopete plugin needed) but that would require it or Korganizer to be running.  Hmm, unless I get my hands on Korgac...</p>