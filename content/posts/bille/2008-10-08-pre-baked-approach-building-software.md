---
title:   "The pre-baked approach to building software"
date:    2008-10-08
authors:
  - bille
slug:    pre-baked-approach-building-software
---
Mostly convenience foods are fun but loaded with sugar and salt.  But building software needn't be inconvenient or make your waistline or disk requirements start to bulge.  I've been pushing the <a href="http://build.opensuse.org">openSUSE Build Service</a> to anyone in the KDE community who'll listen, since making tiny tweaks to existing codebases with minimum effort is exactly what it's designed to do.  

So here's a worked example to go from a new Build Service account to a built, tweaked RPM in 5 minutes*. 

<a href="http://www.silicon.com/research/specialreports/agenda-setters-2008/aaron-seigo-39295155.htm">Aaron</a> just <a href="http://aseigo.blogspot.com/2008/10/dear-kde3-kdesktop-users.html">asked for testing for a patch to the KDE 3 codebase</a>, since he doesn't have a build tree for KDE 3 any more.  We'll take the existing openSUSE 11.0 kdebase3 package, and make a customised version including the patch, and finally have a repository online that Aaron can point his comma-as-decimal-point using users at to test.

Go to your home project
Add subproject 'KDE3'
Add repository: Click Advanced, find KDE:KDE3/openSUSE 11.0 and add it
Link package kdebase3 from KDE:KDE3

Get the sources:
$ osc checkout home:wstephenson:KDE3 kdebase3
Add the patch:
$ mv aseigo_kdesktop_comma.diff home:wstephenson:KDE3/kdebase3
$ cd home:wstephenson:KDE3/kdebase3
$ vi kdebase3.spec
Tell the build service about the new patch
$ osc addremove
Check it in
$ osc ci -m "Add aseigo comma kdesktop patch for testing"

Shake n'bake!  The package is scheduled for immediate build including the new patch.

You can do the command line bits using the web frontend, and vice versa, but this is my way of working.  Now all I want for Christmas is a Plasmoid to monitor my Build Service projects.

*Build time not included.  I expect the finished package will pop out <a href=" http://download.opensuse.org/repositories/home:/wstephenson:/KDE3">here</a>, accompanied with the mouthwatering smell of fresh baking.

<b>Update</b>: x86_64 was done at 1300 UTC - I've tested it and 100,3 * 4 does indeed equal <strike>101,2</strike> 401,2.<
<!--break-->