---
title:   "openSUSE KDE 4.2 respin and important repository changes"
date:    2009-04-25
authors:
  - bille
slug:    opensuse-kde-42-respin-and-important-repository-changes
---
Martin Schlander <a href="http://mschlander.wordpress.com/2009/04/25/important-news-for-opensuse-kde4-users/">already said the most important things</a> but repetition never hurt a good message:

<ul>
<li><b>KDE 4.3 is coming to KDE:KDE4:Factory:Desktop</b><. If you do nothing and use this repo you will get KDE 4.3beta1 installed soon!
<li>The stable KDE 4.2 packages will continue to be available in the new <a href="http://download.opensuse.org/repositories/KDE:/42/openSUSE_11.1/">KDE:42 repo</a>.</li>
<li>The Extra-Apps repo is gone, its packages merged into Desktop, Community or Playground according to their level of support and release readiness.</li>
<li>App packages which have both KDE 3 and KDE 4 versions are being renamed to show that KDE 4 is the default.  Eg for digikam, we have kde3-digikam and digikam-kde4.  This will cause a package upgrade to the new stable version.  If you want to keep the KDE 3 version, install the kde3-<appname> package instead of the new KDE 4 based <appname> package.
<li>The Geeko is a quiet and stealthy animal.  It doesn't make a lot of noise.  But it produces solid, well engineered Linux distros year after year.  Stephan 'Unstoppable Force Beineri' Binner has produced a respin installation CD of openSUSE 11.1 containing the latest and greatest KDE 4.2.2, and all the online updates since 11.1 came out in December.  Is the longer openSUSE release cycle making you twitchy for a hit of something new?  Do you want a rock solid openSUSE with the best KDE has to offer and none of this repo-fiddling nonsense?  Get the respin from: <a href="http://download.opensuse.org/repositories/KDE:/Medias/images/iso/">http://download.opensuse.org/repositories/KDE:/Medias/images/iso/</a>.</li>
</ul>
<!--break-->
