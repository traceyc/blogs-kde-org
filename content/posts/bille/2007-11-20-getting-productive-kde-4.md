---
title:   "Getting productive with KDE 4"
date:    2007-11-20
authors:
  - bille
slug:    getting-productive-kde-4
---
So I'm doing a bit of hacking on Kopete and System Settings tonight and thought I would compile kdemultimedia for the first time in ages and see what KDE4 sounds like.  And I'm pleasantly surprised.  Sitting here listening to Crystal Castles and some random chiptunes from 8bitpeoples is a suitably retro techy soundtrack for the future of the desktop.

The screenshot is really boring ;).  I've been doing some UI rework on Kopete lately, I should find time to blog about it soon...

[image:3105 align=center size=label width=204 height=150 class=name style=style-data node=3105]