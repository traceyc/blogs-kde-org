---
title:   "New KDE Four Live Images"
date:    2010-03-03
authors:
  - bille
slug:    new-kde-four-live-images
---
New <a href="http://home.kde.org/~kdelive">KDE Four Live CDs</a> with <a href="http://www.kde.org/announcements/announce-4.4.1.php">KDE 4.4.1</a>, and <a href="http://home.kde.org/~kdelive/KDE-Four-Live.i686-4.4.1.list">much more</a> are up.

<a href="http://home.kde.org/~kdelive"><img align="center" src="http://home.kde.org/~kdelive/KDE-Four-Live.i686-4.4.1-preview.png"/></a>

They were built with <a href="http://build.opensuse.org/">openSUSE Build Service's</a> KDE:Medias project and <a href="http://susestudio.com">SUSE Studio</a> and consist of openSUSE 11.2 plus all updates, KDE 4.4.1, upstream branding, Nepomuk enabled and Strigi disabled (because it's a Live CD).

They can be used as Live USB sticks too, see <a href="http://en.opensuse.org/Live_USB_stick">these instructions</a> if you don't know how to dd a file to a device.

You can also install to disk and use it as a normal distribution using the installer on the desktop.    Once installed, the first update will pull in all the packages that are normally on an openSUSE KDE install that do not fit on a single CD.
<!--break-->
