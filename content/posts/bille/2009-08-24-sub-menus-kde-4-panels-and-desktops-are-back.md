---
title:   "Sub-menus in KDE 4 panels and desktops are back"
date:    2009-08-24
authors:
  - bille
slug:    sub-menus-kde-4-panels-and-desktops-are-back
---
The main openSUSE users' mailing list are a demanding bunch who know what they want.  Over the last few months the KDE group have been asking them what they still miss from KDE 3 in KDE 4, and one of those things has been the ability to add a submenu of the main app launcher, whether Kickoff or traditional, <a href="http://bugs.kde.org/show_bug.cgi?id=189583">to the panel as a button in its own right</a>.  

I thought <a href="http://chani.wordpress.com/2009/08/08/gsoc-week-11-two-new-plugins/">Chani's recent blog</a> about a generic app menu dataengine for KDE 4 could be useful in reimplementing this feature, so I sat down for a quick Friday afternoon hack.  However, after a while I realised that I could do better than just hacking up another menu applet implementation.  I realised that we already have all the code needed for building the menu tree, drawing the menu, handling the clicks, and configuring the menu in the existing launcher menus ('start buttons').  Looking at the code, it just came down to changing the point in the menu used for the menu's root.  So I added a way to enable the context menu for submenus, identify the path of the submenu, then tell the panel or the desktop to add a new 'simplelauncher' applet using the original submenu as the menu root.  The changes were only a few lines of code to the existing applets so we have a nice minimal yet fully-featured solution.

That didn't take too long so I even had time to make a movie:

<a href="http://ktown.kde.org/~wstephenson/submenus-in-panel-are-back.ogv">OGG Theora version</a>

<a href="http://www.youtube.com/watch?v=prxSP7xnqR8"><img align="center" src="https://blogs.kde.org/files/images/adding_submenu.png" alt="movie of adding a submenu"/></a>

<a href="http://www.youtube.com/watch?v=prxSP7xnqR8">Flash version</a>
<!--break-->
