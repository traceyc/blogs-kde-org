---
title:   "flying the openSUSE and KDE flags at Guademy"
date:    2008-04-26
authors:
  - bille
slug:    flying-opensuse-and-kde-flags-guademy
---
I'm at II Guademy 2008, the Spanish conference combining aspects of GUADEC and Akademy this weekend.  I arrived yesterday, had lunch with my Novell colleagues Rodrigo Moya and Vincent Untz, then got straight into our presentations, which are a combined call for more cooperation and communication between the two Free desktop environments communities, leading to more effective sharing of data and infrastructure.  

I wasn't chased out of the place or pelted with fruit, but I think it will be a long road.  In the long term it should bring benefits for both desktops in the face of expanding ambitions and growing maintenance requirements.  At breakfast Aleix Pol, Richard Hughes and I had an interesting discussion over breakfast, echoing a thread on the XDG list, about the possibility of standardising KDE's KNotify and reworking GNOME's libnotify to use it, so there is some real interest in the idea outside the Novell desktop group.

I've been pimping the openSUSE build service to anyone who'll listen.  It's a bit surprising, when you jump out of the openSUSE pond, how few people have heard of all the features like packaging for distributions besides openSUSE.  More effort needed there...

Today I'm going to present Akonadi, the PIM data server.  Last week we moved it out of KDE main modules and into kdesupport, divesting it of the last bits of KDE dependencies, so I'm able to sincerely present it as a desktop-independent way to store and access your PIM data, emails, contacts, calendars and everything else.  So I better stop blogging and make sure it is ready to demo...

One last thing.  Dinner last night was 'interesting'.  I can't get into specifics, but a visit to the Los Bestias restaurant should be mandatory for inter-desktop geek conferences.  They really shake things up.
<!--break-->