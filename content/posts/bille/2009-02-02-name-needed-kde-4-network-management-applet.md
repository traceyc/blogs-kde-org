---
title:   "Name needed: KDE 4 Network Management Applet"
date:    2009-02-02
authors:
  - bille
slug:    name-needed-kde-4-network-management-applet
---
I'm nearly ready to move <a href="http://blogs.kde.org/node/3713">NetworkManager-kde4</a> to kdereview now, after a crazy week of rehashing the connection layer (the bit that writes your configured connections to KConfig (and optionally KWallet) into something that I actually want to support for a few years.  

Before I move it and start telling people about it, I want to decide on a final name.  This is important as it's not just what appears in the UI, but also determines the names of files like config files for connections, KNotify settings, translation catalogs, none of which you want to mess about with after a release. So I'm looking for suggestions for and opinions about a good name.

<!--break-->

The name will appear in the UI:
* In the Add Widgets plasma list
* In Plasma's tooltip for the widget
* In the right-click menu on the widget ($NAME Settings..., Remove this $NAME)
* In System Settings 
** Module list
** Window titles 

And for the OCD award, this is where you'll see it on the filesystem
* $KDEHOME/share/config/$namerc
* $KDEHOME/share/config/$name.notifyrc
* $KDEHOME/share/apps/$name/
* $KDEDIR/share/kde4/services/kcm_$name.desktop
* $KDEDIR/share/kde4/services/plasma-applet-$name.desktop
* $KDEDIR/share/kde4/services/kded/$name.desktop
* .po files

Plese help me come up with good values for $NAME and $name.  Currently I'm using an inconsistent mix of "Networks", "Network Settings", "knetworkmanager" and "networkmanager".

Here are some I thought of so far.
* Networks - nice and to the point but looks wrong in the widget right click menu, which shows "Remove this Networks" - bad name for hardcoded grammar
* NetworkManager - relates a bit closely to the implementation.  We may make the 'business end' a Plasma::Service and <a href="http://dot.kde.org/2009/01/31/interview-developer-dario-freddi">Dario Freddi</a> wants to work on a Wicd implementation of it.
* KNetworkManager - shows its heritage (since I worked on the KDE 3 KNetworkManagers) but there is very little common code, and really I'm not sure that it's a relationship to be proud of.  Plus it would cause conflicts on distros where KDE3 and KDE4 share a KDEHOME.
* Networking
* Connection Manager - *Manager is so overused and it will coincide with Intel's ConnMan when we have a backend for that.
* Connections
* Connection Control Panel - works with "Remove this" but is clumsy when referring to the System Settings Module - eg "Connection Control Panel Settings"
