---
title:   "Qt 4.6 preview packages available for openSUSE"
date:    2009-10-10
authors:
  - bille
slug:    qt-46-preview-packages-available-opensuse
---
Since today is the big day when KDE trunk starts to depend on Qt 4.6, Raymond Wooninck (tittiatcoke), community packaging hero, has worked to provide packages of the unreleased Qt 4.6 in the openSUSE Build Service.  

If you want to develop KDE trunk without the hassle of compiling Qt, you can use <a href="http://download.opensuse.org/repositories/KDE:/Qt46/openSUSE_11.1/">these packages (openSUSE 11.1)</a>.  We'll be updating them every week to stay current as Qt 4.6 nears release.  Please keep in mind that we're not responsible if you install them and your system KDE packages and anything else that depends on Qt stops working (although we will help you put things right, and the trolls will probably want to know about binary-incompatibility problmems) and when 4.6 is released, it will move into the KDE:Qt repo.
<!--break-->
