---
title:   "File Transfers in KDE 4"
date:    2010-04-12
authors:
  - bille
slug:    file-transfers-kde-4
---
<p>Did you know every app built with KDE 4 can save files as easily to a FTP server or a remote computer using SSH as easily as it accesses your local hard disk?  You should!  This is a feature that I take for granted since it was introduced in the days of KDE 2.0, but it's easy to forget that the majority of KDE users only started using it since then.</p>
<p/>
<p>A few of our community people got together and wrote this <a href="http://userbase.kde.org/File_transfers">thorough overview of network transparent file management in KDE</a> at the weekend.  Cookies to them for writing it and even if you think you are an old KDE hand, give it a read - I didn't know about the handy protocol selector in Dolphin, and that let me discover the settings:/ protocol - now I can access my Settings directly in Dolphin.<img src="http://ktown.kde.org/~wstephenson/settings-in-dolphin.png"/></p>
<!--break-->
