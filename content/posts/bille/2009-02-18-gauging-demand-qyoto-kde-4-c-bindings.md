---
title:   "Gauging demand for Qyoto (KDE 4 C# Bindings)"
date:    2009-02-18
authors:
  - bille
slug:    gauging-demand-qyoto-kde-4-c-bindings
---
A helpful chap just popped into #opensuse-kde and asked about our c# bindings.  It turns out that our packages exclude them in the most violent way possible.  This made me curious as to why, as AFAIK they are a high quality binding that exposes the Joy Of KDE to an untapped pool of developer talent.  The reason is lack of obvious demand and anyone to test them.  So I'd like to know, if we enable building the (stable) Qyoto bindings for C#, would you use them?

(No, they won't be on the default install, nor will installing openSUSE's KDE pull in mono unless you choose to.  Don't throw your peanuts at me.)
