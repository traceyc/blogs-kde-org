---
title:   "KDE 4: like a dream on 256Mb/1Ghz/Intel!"
date:    2007-12-10
authors:
  - bille
slug:    kde-4-dream-256mb1ghzintel
---
So someone just asked in #kde4-devel whether it was worth trying KDE 4 on a 2500Mhz/256Mb computer and I was characteristically careful and guessed "It will work, but won't be good.".  Then I decided to put my money where my mouth is and booted my Thinkpad X60 with "mem=256M maxcpus=1", logged into KDE 4 and set the power saving policy to "Powersave", which throttles the CPU to 1Ghz and locks it there.  And then I used KDE 4 some, started Konqueror, browsed about a bit, configured a few things with System Settings, started Kopete and chatted a little.  And I was pleasantly surprised with how well it all works.  With a "debugfull" build from SVN.  With kwin_composite 3d eye candy, on Intel.  With KDE 3 libraries loaded too (for KPowerSave, since it's not ported yet). As they like to say here in Franconia, it's like "a'Traum".

[image:3136 align=left hspace=20 vspace=n border=n size=label width=300 node=3136] Kopete startup is slow (we _have_ to optimise that), but definitely usable.  I'd love to get some more detailed figures but that will have to wait. This shows the benefits of KDE's integrated approach, and means one thing: KDE 4 is going to be a serious contender for a high-performance, good looking interface on low resource machines like the Eee PC, and I can't wait to try it out on one.<!--break-->
