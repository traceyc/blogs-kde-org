---
title:   "KDE at Novell's BrainShare event"
date:    2008-03-20
authors:
  - bille
slug:    kde-novells-brainshare-event
---
Over in Salt Lake City, Utah, Novell's <a href="http://www.novell.com/brainshare/">BrainShare 2008</a> event is taking place.  This is where the faithful come to see what's new and good in the big red N world every year, and what would be better to liven up a wintry landscape than a colourful talk about KDE 4?  The KDE Team here at Novell have worked our KPats off all over KDE 4 to make it great and the Novell customer base deserve to know about it.  So I put together a presentation to communicate the advantages of the brand new version of the <a href="http://www.novell.com/documentation/sled10/pdfdoc/kde_quick/kde_quick.pdf">other desktop on SUSE Linux Enterprise Desktop</a> and since 1839kg of CO2 is not to be sniffed at, got my colleagues  <a href="http://en.opensuse.org/User:AdrianSuSE">Adrian Schroeter</a> and <a href="http://zonker.opensuse.org/">Zonker</a> who are big KDE fans and were already attending to present it.  So the <a href="http://www.novellbrainshare.com/slc2008/published//sessions/IO140/IO140.pdf">interested but not-a-techy introduction to KDE 4</a> can be found <a href="https://www.novellbrainshare.com/slc2008/scheduler/controller/catalog">here (Novell login eg Build Service, forums or bugzilla required)</a> along with a lot of other interesting stuff about what Novell does with Linux.  
<!--break-->