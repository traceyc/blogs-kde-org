---
title:   "KDE 6 Roadmap: The Desktop Is Dead"
date:    2011-04-12
authors:
  - bille
slug:    kde-6-roadmap-desktop-dead
---
Did that get your attention?  Good, it was supposed to.  Now get back to making KDE 4 rock in whatever way you are able and resist the temptation to put 'KDE 5' in your blog title to get some clicks.  KDE 4 is not going anywhere in the foreseeable future because GNOME just increased their major release number.