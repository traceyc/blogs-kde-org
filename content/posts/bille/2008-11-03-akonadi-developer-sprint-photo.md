---
title:   "Akonadi Developer Sprint - The Photo"
date:    2008-11-03
authors:
  - bille
slug:    akonadi-developer-sprint-photo
---
<p>Here we are on the steps at the <a href="http://www.linuxhotel.de">Linuxhotel in Essen, Germany</a>.  From left to right:  
Igor Trindade Oliveira, Bertjan Broeksema, Stephen Kelly, Ingo Klöcker, Kevin Krammer, Thomas McGuire, Volker Krause, Will Stephenson and Tom Albers.  
<p>We had a very intense and productive couple of days filled with presentations, knowledge sharing and of course dawn till dusk hacking.  As usual Volker, as the Akonadi visionary and lead, was mobbed by the rest of us with questions, which he answered with his trademark infinite patience and good explanations.</p>
<p>I intended to hack on <a href="http://www.novell.com/products/groupwise">Novell GroupWise</a>  and <a href="http://kablink.org/">Kablink</a> resources for Akonadi, since we use those in-house at work, but I am fairly unashamed to say that I worked on the NetworkManager-kde4 applet instead of Akonadi itself.  What brings the most benefit to the most people? PIM without a network connection is only of interest to hermits and compulsive solipsists.  More about the applet later though.  I did do some API review, and started looking at the migration to Akonadi from a user experience perspective.  KDE 4.2 will start using Akonadi via the existing KResources framework, and I want to make sure this is as transparent as possible to the average user, and that the configuration works if you do need to tweak it.</p>
<p><img alt="Akonadi developers at the November 2008 sprint in Essen" width="800" height="600" src="https://blogs.kde.org/files/images/p1010286.jpg"/></p>
<p>Our meeting was kindly supported by the Linuxhotel, who provide top-quality hacking and meeting space in beautiful surroundings to open-source projects for a token fee.  These meetings are short and intense and to get the most out of a couple of days when you know you can turn up and have the connectivity, the environment and the snacks to fuel hackers from the word go.  By providing these Linuxhotel enable great Free Software to be produced.  And they really know Linux so if you need to expand your head, check out their courses.</p>
<p>Speaking of sprints, I think I need to start doing some sprinting of the physical variety again.  It's a good job I have 2 weeks' holiday now ;).</p>
<!--break-->
