---
title:   "Thoughts about Kubuntu's Status, Canonical, and your distribution's sponsors"
date:    2012-02-08
authors:
  - bille
slug:    thoughts-about-kubuntus-status-canonical-and-your-distributions-sponsors
---
Yesterday I woke up to the news that Canonical are no longer going to fund Riddell to work on Kubuntu.  I've trying to figure out what that means for KDE and for community Linux generally. 

Disclaimer: I work in the same role as Jonathan at SUSE, a competing Linux company that sponsors the openSUSE project.  This is my personal opinion, not that of the openSUSE Board or SUSE Linux GmbH.

I'm sad for Jonathan personally. He has put a lot of his lifeblood into Kubuntu over the years, at no little cost to himself, and to be pulled off one's favourite project hurts. The same thing could happen to me if the powers that be decide, so I can easily empathise with him.

In the bigger picture, I have to say that this doesn't surprise me at all.  For Canonical, Kubuntu fulfilled its purpose a few years ago already.  Kubuntu, and the other official Ubuntu derivatives, have always been a spoiler move to tie up community contributors who believed in the early community-centric image of Ubuntu, but who didn't agree with the main Ubuntu's direction.  Otherwise, there was the risk that Ubuntu design decisions would polarize the Linux community and send people towards Ubuntu's competitors.  With the derivatives, they are safely occupied under the big tent of the Ubuntu brand.

If we look back at the Ubuntu game plan as history neatly lays it out for us, we have

1) Establish the Ubuntu brand amongst early adopters (check, by about 2005)
2) Expand it to the wider Linux user base (check, by about 2007)
3) Make Ubuntu the default Linux for non-technical users (2009)
4) Tie up a paying market. Initial targets have been enterprise desktop Linux (maybe next year ;)) or consumers in the massmarket netbook segment (but that was squashed by tablets and Microsoft rounding up the manufacturer back to the XP prison), and now they are aiming at embedding into consumer electronics (TVs) and will probably snare a tablet OEM as a cloud OS (hell, if KDE can do it...) or a bookseller or someone who wants a platform to digitally sell something else off of.
5) Profit
6) Buy more spaceflight (Probably. For some, 5) is enough)

Somewhere after 1), the massive demand for KDE on Ubuntu in KDE's main territories (Germany, via the ubuntu.de forums, which IIRC threatened an unofficial fork) caused Canonical to realise that it was better to control a large dissenting minority with some token gestures than to have them really doing their own thing. So Jonathan, at that point a KDE packager at Debian, was hired, and Mark Shuttleworth did his salesman job at a couple of KDE events making some insubstantial promises (If I had a dollar for every KDE eV board member at the time who told me "But Mark has promised to install and use Kubuntu on his workstation" multiplied by every Ubuntu developer overheard chuckling that "But they don't know that Mark *never* uses his workstation, he's always on a notebook"...), a few community people got flights to events, and Kubuntu was born, and legitimised by the then-leaders of the KDE community.

Once 2) was consolidated, Kubuntu was redundant to Canonical, but on the average professional Linux hacker's salary, Jonathan was an affordable luxury.  Now, I suspect that with the trend at Canonical to develop more and more in-house to chase 4) rather than just distribute what the FLOSS community provides, putting paid man-hours on a mature product is no longer a good way to spend engineering budget.

By cheaply tying up competitors' resources, Kubuntu has hindered KDE's overall growth via other distributions and balkanized the KDE community.  It can be argued that Kubuntu has brought users and contributors to KDE as part of the rapid initial growth of Ubuntu, and Kubuntu has been a success in focussing their developers on improving KDE, but this came at the price of cementing KDE in the role of a second class environment in the eyes of everyone who came to Linux via Ubuntu.  I suspect that the GNOME community, which previously surfed the wave of Ubuntu's growth, will feel the pinch of necessity as Canonical moves towards its endgame, and having already been displaced as the default desktop for an inhouse development, will move further towards just being an anonymous organ donor to Unity and subsequent productisable UIs.

Why am I writing this? I don't want to be so crass as to just say 'come to my project instead'.  I'd like to take this opportunity to suggest that you should have no illusions about what your community Linux distribution means to the businesses that sponsor it.

For openSUSE, it's some engineering contribution to and testing of SUSE enterprise products' codebase, and supporting the enterprise brand via a halo effect from the community brand.  In setting up the openSUSE project, SUSE has been militant in giving the community complete control of the project and the distribution that comes out of it.  Call it an insurance policy or a lifeboat, but by opening and freeing all the tools that create openSUSE (as well as the source code), we assure that the results of 20 years of work are indefinitely available.  SUSE is secure enough in its business and believes strongly enough in free software to do this with the rootstock of its enterprise products, because the modular, federated Open Build Service allows SUSE to derive enterprise products from openSUSE without having to steer it.
<!--break-->
