---
title:   "Plasma 5.8: More efficient Pager and Activity Pager widgets"
date:    2016-09-06
authors:
  - eike hein
slug:    plasma-58-more-efficient-pager-and-activity-pager-widgets
---
On the heels of the Plasma 5.7's new <a href="https://blogs.kde.org/2016/05/31/new-plasma-task-manager-backend-faster-better-wayland">Task Manager backend</a>, the <a href="https://community.kde.org/Schedules/Plasma_5">upcoming</a> Plasma 5.8 LTS release will sport substantially rewritten Pager and Activity Pager widgets, aimed at improving efficiency and conserving resources in a typical Plasma Desktop setup.

<center><img src="https://www.eikehein.com/kde/blog/plasma58_pager/pager.png" alt="Plasma Pager widget" /><br /><small><i>Enemy located<i></small><br /><br /></center>

<b>Two birds with one stone</b>

Both the Task Manager and Pager widgets report on the windowing system. (If you didn't know: The <i>Pager</i> is the little game-like minimap to the left of the Task Manager, showing the windows on each virtual desktop.) To do so they need to know about the windows that exist and various things about them, such as their location (screen, desktop) and state (active, minimized). Up until now, the two would collect all of that data independently, sharing no code, and doing much of the same processing in response to changes. Plasma 5.8's Pager now shares a backend with the Task Manager (no matter how many instances of each applet you have), consolidating all the window information processing in the Plasma shell process in just one spot. This saves a whole bunch of CPU and memory, along with being less code to maintain!

<b>Doing less is doing fast</b>

The old Pager backend would toss out everything it knew about the current state of the windowing system every time any window changed in any way, causing everything the widget was showing to be torn apart and recreated from scratch - even when most of it didn't need to be touched at all. That's of course terribly inefficient, and the Task Manager's backend is fortunately much smarter. No more needless busywork.

<b>Dropping weight</b>

The way the Pager is actually drawn was cleaned up along the way, now using less CPU and memory, especially with the bare-bones default settings. Dragging a window on the Pager is now also much more efficient internally.

<b>(Re-)joining forces</b>

All of the above also applies to the Activity Pager widget - which used to be a lightly modified copy of the Pager's code, and now instead shares all of its code with its sibling. The two pagers remain separate entries in the Widget Explorer, but all further maintenance and development can now happen on a unified codebase.