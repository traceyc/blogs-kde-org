---
title:   "Community goal: Modern and Global Text Input For Every User"
date:    2017-10-24
authors:
  - eike hein
slug:    community-goal-modern-and-global-text-input-every-user
---
A few months ago, I had the opportunity to give a talk on <a href="https://conf.kde.org/en/akademy2017/public/events/359">Input Methods in Plasma 5</a> at Akademy 2017 in lovely Almería in Spain. If you were interest in my talk but were unable to attend, there's now <a href="http://origin.files.kde.org/akademy/2017/359-input-methods-in-plasma-5.mp4">video</a> (<a href="https://conf.kde.org/system/attachments/99/original/Input_Methods_in_Plasma_5.pdf">complementary slides</a>) available courtesy of the Akademy conference team. Yay!

A big part of my talk was a long laundry list of issues we need to tackle in Plasma, other KDE software and the wider free desktop ecosystem. It's now time to take the next step and get started.

I've submitted the project under <a href="https://phabricator.kde.org/T6838"><i>Modern and Global Text Input For Every User</i></a> as part of the KDE community's new <a href="http://blog.lydiapintscher.de/2017/08/20/evolving-kde-lets-set-some-goals/">community goals initiative</a>, a new thing we're trying exactly for challenges like this - goals that need community-wide, cross-project collaboration over a longer time period to achieve.

If you're interested in this work, make sure to <a href="https://phabricator.kde.org/T6838">read the proposal</a> and add yourself at the bottom!