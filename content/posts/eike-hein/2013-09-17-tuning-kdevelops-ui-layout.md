---
title:   "Tuning KDevelop's UI layout"
date:    2013-09-17
authors:
  - eike hein
slug:    tuning-kdevelops-ui-layout
---
With some idle time on my hands due to waiting on an especially long build, I decided to put some time into cleaning up my KDevelop's UI layout today:

<center><a href="http://www.eikehein.com/kde/blog/kdevui/kdevelop.png"><img src="http://www.eikehein.com/kde/blog/kdevui/kdevelop.png" width="640" height="345" alt="Customized KDevelop interface layout" /></a><br /><small><i>Click for original resolution.</i></small><br /><br /></center>

I removed all but the elements I actually ever click on in the main window -- all the rest I usually access via keyboard shortcuts or the menu (which I'm now going to try using via the window deco button). Since working on an IRC client makes you feel at home with text fields at the bottom of windows, I moved those toolbars down below the bottom toolview button and moved the remaining toolbar actions into the left toolview button column (I already use label-less toolbars on the left in some other apps, e.g. Gwenview). All together, this saves a lot of vertical space, which is precious on my widescreen laptop.

I think I'm happy, though I'll have to actually use it for a while to see whether I'm not missing anything. How does your KDevelop look?