---
title:   "Basic code completion for Rust in KDE's Kate (and later KDevelop)"
date:    2015-05-18
authors:
  - eike hein
slug:    basic-code-completion-rust-kdes-kate-and-later-kdevelop
---
A few days ago the Rust community <a href="http://blog.rust-lang.org/2015/05/15/Rust-1.0.html">announced</a> v1.0 of their new systems programming language, <a href="http://www.rust-lang.org/">Rust</a>. Having followed the project for some time and finally having used the language for a number of small projects this year, I've come to feel that using Rust is interesting, fun and productive. I'd like to highly encourage everyone to give it a look now that it's finally considered ready for prime time.

To aid the effort I've put some Sunday hacking time into a basic Rust code completion plugin for Kate today. It's built around Phil Dawes' very nifty <a href="https://github.com/phildawes/racer">Racer</a>, freeing it up to concern itself only with exposing some configuration and getting data provided by Racer into Kate. Not difficult at all.

This is what it looks like in action:

<center><img src="http://www.eikehein.com/kde/blog/kterustcompletion/kterustcompletion.png" alt="The Rust code completion plugin for Kate" /><br /><small><i>Completin'</i></small><br /><br /></center>

<h1><b>Caveats</b></h1>

The plugin is very basic at the time of writing (which is minutes after getting things working, this Sunday is running out!). The mapping of completion match metadata to Kate's format (from fundamentals like the match type, to more complex features like smart grouping) can likely be improved still. Ditto for auto-completion behavior (i.e. defining the circumstances in which the completion popup will kick in automatically, as opposed to waiting for manual invocation) and simply performance.

The plugin also doesn't work in KDevelop 5 yet, although that one's on KDevelop -- it doesn't support the KTextEditor plugin type present in Frameworks 5 yet. I'm told the KDevelop team has addressing this on their agenda.

<h1><b>Syntax highlighting</b></h1>

Both KDE and Rust implement the <code>AwesomeCommunity</code> <a href="http://doc.rust-lang.org/stable/book/traits.html">trait</a>. The Rust community in particular maintains an official Kate syntax highlighting file <a href="https://github.com/rust-lang/kate-config">here</a>. <strike>The repository for the Kate plugin includes this as a submodule and installs it by default; if you'd like it not to do that, there's a build system toggle to disable it.</strike> <b>Update:</b> The syntax highlighting file has moved to kde.org, read more <a href="https://blogs.kde.org/2015/05/22/updates-kates-rust-plugin-syntax-highlighting-and-rust-source-mime-type">here</a>.

<h1><b>A MIME type for Rust source code</b></h1>

While the plugin will run for all *.rs files, Kate/KDevelop plugins and syntax highlighting files preferably identify documents by their MIME type. It turns out there isn't a MIME type for Rust source code in <a href="http://freedesktop.org/wiki/Software/shared-mime-info/">shared-mime-info</a> yet, so I've started the process of <a href="https://bugs.freedesktop.org/show_bug.cgi?id=90487">getting one in</a>. (<b>Update:</b> <a href="http://cgit.freedesktop.org/xdg/shared-mime-info/commit/?id=f75cbe0d37c990580dbb6f0694b53bc0c914d933">Merged</a>, along with <a href="https://github.com/rust-lang/kate-config/pull/6">this</a> and <a href="https://github.com/rust-lang/gedit-config/pull/1">this</a>!)

<h1><b>Getting the plugin</b></h1>

<strike>The plugin is currently still hosted in a personal scratch repo of mine, on git.kde.org. You <a href="http://quickgit.kde.org/?p=scratch%2Fhein%2Fkterustcompletion.git">can browse the source </a>, or skip straight to this clone URL:</strike>

<code><strike>git://anongit.kde.org/scratch/hein/kterustcompletion.git</strike></code>

<strike>This location might change if/when the plugin is promoted to proper project status; I'll update the post should that happen. I've included some <a href="http://quickgit.kde.org/?p=scratch%2Fhein%2Fkterustcompletion.git&a=blob&f=README.md&o=plain">install instructions</a> that go over requirements, options and configuration. Note that you need a Rust source tree to get this working. Check them out!</strike>

<b>Update:</b> As of May 20th 2015, the plugin is now bundled with Kate in its repository (<a href="http://quickgit.kde.org/?p=kate.git&a=tree&&f=addons%2Frustcompletion">browse</a>), to be included in the next Kate release. You can get it by building Kate from git as whole, or you can do <code>make install</code> in just the plugin's subdir of your build dir; at this time it works fine with Kate's last stable release. Given all this, I'll only quickly recap some additional info for getting it working here: Once you've enabled the <code>Rust code completion</code> item in the plugin page, a new page will appear in your Kate config dialog. Within are config knobs for the command to run <a href="https://github.com/phildawes/racer">Racer</a> (which you'll therefore need an install of) and the path to a Rust source tree (which you can of course grab <a href="https://github.com/rust-lang/rust">here</a>), which Racer needs to do its job. Make sure both are set correctly, and then start editing!

<h1><b>Getting in touch</b></h1>

If you have any comments or requests and the blog won't do (say, you have a cool patch for me), <strike>you can find my mail address inside the <code>AUTHORS</code> file included in the repository. Signing up for KDE.org's proper bug tracking and patch submission pathways is still pending :-).</strike> <b>Update:</b> After the move to Kate's repository (see above), you can now file bugs and wishes against the plugin via the <code>plugin-rustcompletion</code> component of the <code>kate</code> product on <a href="https://bugs.kde.org/">our bugtracker</a> (<a href="https://bugs.kde.org/enter_bug.cgi?product=kate&component=plugin-rustcompletion">handy pre-filled form link</a>). You can also submit patches via <a href="http://git.reviewboard.kde.org/">ReviewBoard</a> now.