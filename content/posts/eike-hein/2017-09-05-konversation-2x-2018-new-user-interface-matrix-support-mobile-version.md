---
title:   "Konversation 2.x in 2018: New user interface, Matrix support, mobile version"
date:    2017-09-05
authors:
  - eike hein
slug:    konversation-2x-2018-new-user-interface-matrix-support-mobile-version
---
It's time to talk about exciting new things in store for the Konversation project!

Konversation is KDE's chat application for communities. No matter whether someone is a newcomer seeking community, a seasoned participant in one, or a community administrator: our mission is to bring groups of people together, allow them to delight in each other's company, and support their pursuit of shared interests and goals.

One of the communities we monitor for changes to your needs is our own: KDE. Few things make a Konversation hacker happier than journeying to an event like <a href="https://dot.kde.org/2017/07/22/akademy-2017-day-1">Akademy in Almería, Spain</a> and seeing our app run on many screens all around.

The KDE community has recently <a href="https://sessellift.wordpress.com/2017/09/05/results-of-the-requirements-survey-for-a-kde-wide-chat-solution/">made progress</a> defining what it wants out of a chat solution in the near future. To us, those initial results align very strongly with Konversation's mission and display a lot of overlap with the things it does well. However, they also highlight trends where the current generation of Konversation falls short, e.g. support for persistence across network jumps, mobile device support and better media/file handling.

This evolution in KDE's needs matches what we're seeing in other communities we cater to. Recently we've started a new development effort to try and answer those needs.

<b>Enter Konversation 2.x</b>

<center><a href="http://i.imgur.com/7G0rBOL.png" width="644" height="422"><img src="http://i.imgur.com/MGaaoOG.png" alt="Konversation 2.x R&D mockup screenshot" /></a><br /><small><i>Obligatory tantilizing sneak preview (click to enlarge)</i></small><br /><br /></center>

Konversation 2.x will be deserving of the version bump, revamping the user interface and bringing the application to new platforms. Here's a rundown of our goals:

<ul>
<li>A more modern, cleaner user interface, built using Qt Quick and KDE's Kirigami technology</li>
<ul>
<li>Adopting a responsive window layout, supporting more varied desktop use cases and putting us on a path towards becoming a desktop/mobile convergent application</li>
<li>Scaling to more groups with an improved tab switcher featuring better-integrated notifications and mentions</li>
<li>Redesigned and thoroughly cleaned-up settings, including often-requested per-tab settings</li>
<li>Richer theming, including a night mode and a small selection of popular chat text layouts for different needs</li>
</ul>
</li>
<li>Improved media/file handling, including image sharing, a per-tab media gallery, and link previews</li>
<li>A reduced resource footprint, using less memory and battery power</li>
<li>Support for the Matrix protocol</li>
<li>Supporting a KDE-wide <a href="https://phabricator.kde.org/T6838"><i>Global and Modern Text Input</i></a> initiative, in particular for emoji input</li>
<li>Versions for <a href="https://plasma-mobile.org/>Plasma Mobile</a> and Android</li>
<li>Updating Konversation's web presence</li>
</ul>

Let's briefly expand on a few of those:

<b>Kirigami</b>

KDE's Kirigami user interface technology helps developers make applications that run well on both desktop and mobile form factors. While still a young project, too, it's already being put to good use in projects such as <a href="https://peruse.kde.org/">Peruse</a>, Calligra Gemini, Gwenview, and others. When we tried it out Kirigami quickly proved useful to us as well. We've been enjoying a great working relationship with the Kirigami team, with code flowing both ways. <a href="https://techbase.kde.org/Kirigami">Check it out!</a>

<b>Design process</b>

To craft the new user interface, we're collaborating with KDE's <a href="https://vdesign.kde.org/">Visual Design Group</a>. Within the KDE community, the VDG itself is a driver of new requirements for chat applications (as their collaboration workflows differ substantially from coding contributors). We've been combining our experience listening to many years of user feedback with their design chops, and this has lead to an array of design mockups we've been working from so far. This is just the beginning, with many, many details left to hammer out together - we're really grateful for the help! :)

<b>Matrix</b>

Currently we're focused on bringing more of the new UI online, proving it on top of our robust IRC backend. However, Matrix support will come next. While we have no plans to drop support for IRC, we feel the <a href="https://matrix.org/">Matrix</a> protocol has emerged as a credible alternative that retains many of IRC's best qualities while better supporting modern needs (and bridging to IRC). We're excited about what it will let us do and want to become your Matrix client of choice next year!

<b>Work done so far</b>

The screenshot shown above is sort of a functional R&D mockup of where we're headed with the new interface. It runs, it chats - more on how to try it out in a moment - but it's quite incomplete, wonky, and in a state of flux. Here's a few more demonstrations and explorations of what it can do:

<center><a href="https://www.youtube.com/watch?v=1cr7hirDzws"><img src="http://i.imgur.com/CyBwW0O.gif" alt="Repsonsive window layout" /></a><br /><small><i>Responsive window layout: Front-and-center vs. small-and-in-a-corner (click for smoother HD/YouTube)</i></small><br /><br /></center>

<center><a href="https://www.youtube.com/watch?v=b88oqdlNyuUh"><img src="http://i.imgur.com/U4Lblh7.gif" alt="Toggling settings mode" /></a><br /><small><i>Friction-free switching to and from settings mode (click for smoother HD/YouTube</i></small><br /><br /></center>

<center><a href="http://i.imgur.com/XCBQpeH.png width="644" height="451"><img src="http://i.imgur.com/PIzeC85.png" alt="Overlay context sidebar" /></a><br /><small><i>Overlay context sidebar: Tab settings and media gallery will go here  (click to enlarge)</i></small><br /><br /></center>

See a <a href="http://imgur.com/a/bT7XE">gallery with an additional screenshot of the settings mode</a>.

<b>Trying it out</b>

The work is being carried out on the <code>wip/qtquick</code> branch of <a href="https://cgit.kde.org/konversation.git/"><code>konversation.git</code></a>. It needs Qt 5.9 and the master branch of <a href="https://cgit.kde.org/kirigami.git/"><code>kirigami.git</code></a> to build and run, respectively. We also have a <a href="http://flatpak.org/">Flatpak</a> <a href="https://community.kde.org/Guidelines_and_HOWTOs/Flatpak">nightly package</a> soon on the way, pending sorting out some dependency issues.

<u>Be sure to check out</u> <a href="https://userbase.kde.org/Konversation/Konvi2x">this wiki page</a> with build and testing instructions. You'll learn how to retrieve either the sources or the Flatpak, as well as a number of command line arguments that are key when test-driving.

Sneak preview of great neat-ness: It's possible to toggle between the old and new Konversation UIs at any time using the F10 key. This makes dogfooding at this early stage much more palatable!

<b>Joining the fun</b>

We're just starting out to use <a href="https://phabricator.kde.org/project/board/12/">this workboard</a> on KDE's Phabricator instance to track and coordinate tasks. Subscribe and participate! Phabricator is also the platform of choice to submit code contributions.

As noted above, Konversation relies on Kirigami and the VDG. Both projects welcome new contributors. Helping them out helps Konversation!

To chat with us, you can stop by the <a href="irc://chat.freenode.net/konversation">#konversation</a> and <a href="irc://chat.freenode.net/kde-vdg">#kde-vdg</a> channels on <a href="http://freenode.net/">freenode</a> (using IRC or the <a href="https://matrix.org/blog/2015/06/22/the-matrix-org-irc-bridge-now-bridges-all-of-freenode/">Matrix bridge</a>). Hop on and introduce yourself!

<b>Side note:</b> The Kirigami team plans to <a href="https://dot.kde.org/2017/09/02/randa-roundup-part-i">show up in force at the KDE Randa meeting</a> this fall to hack on things the Konversation team is very much interested in, including expanding support for keyboard navigation in Kirigami UI. Check out the <a href="https://www.kde.org/fundraisers/randameetings2017/">Randa fundraising</a> campaign which e.g. enables KDE to bring more devs along, it's really appreciated!