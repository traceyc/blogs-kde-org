---
title:   "Simple Menu launcher on KDE Store"
date:    2017-01-30
authors:
  - eike hein
slug:    simple-menu-launcher-kde-store
---
<center><img src="https://www.eikehein.com/kde/blog/simplemenu10/simplemenu-1.0.png" alt="Example screenshot for Simple Menu v1.0" /><br /><small><i>Simple Menu v1.0</i></small><br /><br /></center>

Quite a while ago already I wrote a launcher menu widget named <i>Simple Menu</i>. It's using the same backend I wrote for our bundled launchers, and it's a little bit like <i>Application Dashboard</i> scaled down into a small floating window, plus nifty horizontal pagination. It's also really simple and fast.

While some distributions packaged it (e.g. Netrunner Linux), it's never been released properly and released - until now! Starting today, you can find Simple Menu <a href="https://store.kde.org/p/1169537/">on the KDE Store</a> and install it via <i>Add Widgets... -> Get new widgets</i> in your Plasma.

Please note that Simple Menu requires Plasma v5.9 (to be released tomorrow). Actual v5.9, not the v5.9 Beta - it relies on fixes made after the Beta release.