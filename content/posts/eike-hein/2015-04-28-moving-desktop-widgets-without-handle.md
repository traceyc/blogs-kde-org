---
title:   "Moving desktop widgets without the handle"
date:    2015-04-28
authors:
  - eike hein
slug:    moving-desktop-widgets-without-handle
---
<center><img src="http://www.eikehein.com/kde/blog/plasma53_adnd/plasma53_adnd01.png" alt="Moving a widget in Plasma Desktop 5.3 in press-and-hold mode" /><br /><small><i>Getting a move on (notice the mouse pointer)</i></small><br /><br /></center>

In Plasma Desktop 5.3 (<a href="https://www.kde.org/announcements/plasma-5.3.0.php">released today</a>), the desktop configuration dialog offers a new experimental tweak: A mode in which widgets can be dragged around by pressing and holding anywhere on the widget. When enabled, the widget handle is also no longer shown just on hover, but only after a press and hold.

The goal of this mode is to make widget handling feel more natural and spatial, and faster. Instead of having to aim for a widget to reveal the handle, and then go after the handle in a second step, you can just grab on to it anywhere. Easy.

The immediate challenge with this setup is discoverability. To address this problem, unlocking widgets in this mode displays a notification bubble introducing the interaction pattern (which can be easily dismissed forever with the action button):

<center><img src="http://www.eikehein.com/kde/blog/plasma53_adnd/plasma53_adnd02.png" alt="Press-and-hold notification bubble on widget unlock" /><br /><small><i>"It looks like you're moving a widget"</i></small><br /><br /></center>

Press-and-hold widget handling is not yet the default in 5.3 (you can enable it on the Tweaks config page of both the Desktop and Folder View containments). <a href="https://www.reddit.com/r/kde/comments/2zxuu5/looking_for_a_little_feedback_moving_desktop/">Beta feedback has been positive so far</a>, but changes to the interaction vocab of our shell components shan't be done lightly—using press-and-hold for widget handling means withholding the verb from widgets themselves, which is something we have to negotiate with widget authors. We think the joy felt when using this pattern and its widespread adoption in other home screen UIs bode well for it, however. Let us know how you feel!