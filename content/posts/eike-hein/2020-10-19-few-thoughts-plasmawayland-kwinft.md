---
title:   "A few thoughts on Plasma/Wayland, KWinFT"
date:    2020-10-19
authors:
  - eike hein
slug:    few-thoughts-plasmawayland-kwinft
categories:
  - Plasma
  - KWin
---
There's a lot of intense, opinionated debate on the current state of Plasma's Wayland session these days. This seems to be fueled by mainly two events, Fedora's announcement to flip to Wayland by default for version 34 of their KDE variant, and a a recent fork of KWin and a few other components of Plasma, KWinFT.

On the first, I think it's a great move. Concerns of a repeat of the "shipped before it's ready" situation of early KDE 4 releases aside (for now; more on this in a moment), it certainly feels like it makes sense for Fedora in particular - it's a bold, technology-focused early adopter move, something I think the Fedora user audience generally appreciates the distro for. Plus other Fedora variants default to Wayland already, so there's an appreciable desire to make the various offerings more consistent.

Distros should understand the user audiences they're trying to cater to, and if a distro believes there's a market for a particular flavor of desktop, it's certainly fine to challenge upstreams to provide the needed software. I think as far as Plasma on Wayland is concerned, the challenge is thoughtfully timed - it's coming after the KDE community <a href="https://community.kde.org/Goals/Wayland">voted to declare good Wayland support a community-wide goal</a>, after all. I think there's every reason to believe this decision will lead to good things if the two (and overlapping) communities collaborate to make a good showing. Nice.

<b>It's 2020, for crying out loud! Why is this taking so long?</b>

A lot of us have some things in common. For example, if you're reading this, chances are you are the sort of person who is not indifferent to technological progress, even easily excited by it. Many of us also are also drawn to competition.

So people really care about how far down the road to Wayland adoption every of the competing projects is, and theories abound on what the comparison says about them. This is set against a backdrop of Wayland also still maturing as an upstream technology, driven forward by the same competing projects working together.

One particular claim that's been popping into the conversation lately is that Plasma not having the Wayland conversion safely in it's rear-view mirror yet is evidence of a project that's somehow fundamentally flawed, and unable to focus on what matters and make good long-term plans and roadmaps. If you didn't encounter this in one of the heated debates on social media yet it probably sounds a bit breathless to you, or maybe not really worth acknowledging - but it's actually my main reason to write this blog post, because it's an interesting excuse to talk about recent Plasma history!

Plasma 5.0 was the second large-scale rewrite of KDE's desktop environment offering in a row, meaning both 4.0 and 5.0 had debuted with a lot of new and young lines of code inside them. Both rewrites were motivated by lifting the desktop shell to a newer set of its base technologies - for example major versions of Qt, OpenGL-based UI rendering and so on. Younger code tends to have higher defect rates. At the time, the standard criticisms in online discussions about Plasma went something like this:

<i>"It looks great, but it's really buggy"</i>
<i>"I like a lot of the features, but it's missing those last 5% to fit my needs fully"</i>
<i>"It's resource-heavy and doesn't perform smoothly enough on my machine"</i>

And a less concrete one, but nearly as often repeated:

<i>"It's really coming together now, but I'm already worried about the next major version being another rewrite and making it shaky"</i>

The Plasma project paid attention, and made a number of very strategic decisions to set its focus on a set of goals:

<ul><li>Improve quality across the board by prioritizing bugs and bringing defect rates down</li>
<li>Go the distance with features, drive them to completion and make them easier and more consistent to use</li>
<li>Lower the resource needs and scale down to much lower-powered hardware, including ARM SBCs</li>
<li>If we do additional rewrites of fundamental code (and we ended up replacing major pieces in 5.x point releases, e.g. the entire taskbar, menu and notification systems - each of those quietly addressed dozens of bugs, but went mostly noticed for a few new features), there needs to be a noticeably positive impact on the above metrics</li></ul>

This remains the spirit of the Plasma project today, with many activities spanning multiple quarters of a year of work at the service of those goals. Recent examples include the <a href="https://phabricator.kde.org/T10891">Breeze visual refresh</a> undertaken by the Visual Design Group, the <a href="http://blog.davidedmundson.co.uk/blog/plasma-and-the-systemd-startup/">revamp of session init</a> with optional use of systemd, and the new <a href="https://conf.kde.org/en/akademy2020/public/events/219">cgroups-loving way of running apps</a>.

For Plasma on Wayland, this meant: While we strongly believing in the tech, we would take our time to get it right, and not declare the offering ready before it isn't up to par. It also wasn't always the highest-priority development activity going on, with many other steps being simultaneously taken to reach our goals. Reasonable people can (and do) have diverging opinions on the relative prioritization of work, of course, but the path we walked was most definitely a consciously strategic one - one that has lead to the gradual disappearance of those earlier quotes from user feedback we receive, speaking to its execution. Work continues!

<b>KwinFT</b>

One source of these concerns is the <a href="https://gitlab.com/kwinft/kwinft">KWinFT</a> project, a recent fork of the compositor service in a Plasma system along with a few choice pieces of support code (e.g. the screen config subsystem).

KWinFT is a project by <a href="https://subdiff.org/">Roman Gilg</a>, a contributor to KWin and KDE in general. Roman will not be shocked by reading this, as we had this discussion a couple of months before he announced the project, as well as recently, but I don't think the fork was necessary, or is a very good idea overall. I think his technical ideas for KWin are interesting, and think there were better options to deliver their value to users and have a bigger impact in the end, e.g. by making more aggressive use of feature/staging branches to prove out subsystem rewrites before merging them up. This is a suggestion I made at the time, too, and I haven't seen anything to change my mind since. We certainly keep talking.

This is a very personal opinion and speaks to my own style to open source involvement more than anything, which is shaped by the vibe of the KDE community and the KDE mentors and role models I received my education from. I believe in prioritizing user value, and in leadership by aligning diverse talents towards common goals, by being inviting, communicative and persuasive. I really don't believe these options were fully exhausted in this matter, and while I understand that starting a new project can make it easier to try out e.g. some different organizational ideas (and my impression of the fork is that it's really more about those than any actual tech disagreements) without having to do the work of getting others on board, I don't think this ultimately succeeds in bringing improvements to the max number of people who might benefit from them.

On the tech side, though, it's important to keep the focus of the conversation on Plasma on Wayland as a whole. KWin is an important component of that system, but owing to its <a href="https://blogs.kde.org/2018/08/02/engineering-plasma-extensions-and-stability-%E2%80%94-present-and-future">multi-process architecture</a>, it's only one of a fairly large and varied set of codebases that need to complete their porting efforts. Many use cases additionally require coordinated activity across several components (for example: the taskbar, System Settings, ...), and it's also still not too difficult to identify <a href="https://blogs.kde.org/2020/10/11/linux-desktop-shell-ipc-wayland-vs-d-bus-and-lack-agreement-when-use-them">needed upstream improvements in Wayland</a> to get over the finish line, some of which the Plasma team is currently <a href="https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/50">driving forward</a>.

And no, this isn't stacking features on shaky foundations - most of this work is technically orthogonal (easy to see for the ones that aren't in the same codebase or even project!), and in any case, the more pleasant Plasma's Wayland session becomes to consume, the higher the set of potential contributors exposed to the remaining tasks. Those attention dynamics of open source are fairly reliable :-). In any case, this is perhaps actually my main beef with the technical criticisms coming from the debates surrounding KWinFT - they are myopically focused on KWin and derive from this grander projections, ignoring the full picture of the system and a lot of other heavy-hitting technical activity all throughout it (including some of the projects linked throughout this post). The resulting signal-to-noise ratio is not very engaging overall. <i>KWin != Plasma</i> is an important constraint to add to them.