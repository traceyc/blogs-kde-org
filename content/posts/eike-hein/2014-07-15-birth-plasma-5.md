---
title:   "The Birth of Plasma 5"
date:    2014-07-15
authors:
  - eike hein
slug:    birth-plasma-5
---
I'll keep things brief, since I'm inbetween KDevelop windows right now: It's out today, and in my mind it took just about nine months to make it. Nine months, now that's a timescale with some cachet.

Back in September, we had this, as best as memory serves:

<ul><li>A rough initial ramp-up of the freshly-refactored low-level Plasma libraries on top of Qt 5 / Qt Quick 2.</li>
<li>And on top of a similarly rough-and-in-motion <a href="https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers">Frameworks 5</a>.</li>
<li>A Plasma shell application that could render a basic wallpaper (with no config yet).</li>
<li>A few "test this library API" widgets.</li>
<li>Very broken and very empty panels.</li>
<li>And we were still running this inside the old Plasma Desktop 4, since we didn't have kwin yet, or an actual session.</li></ul>

Since then, we:

<ul><li>Rewrote about 80% of the core Plasma Desktop components from scratch, with significant improvements in some areas (e.g. the tray and notifications).</li>
<li>Ported the rest over from 4.x, often with significant cleanup and non-trivial changes.</li>
<li>Created hundreds of new visual assets from scratch.</li>
<li>Ported many, many support and system integration bits, such as network management, while continuing to make significant improvements to them.</li>
<li>Kept contributing fixes and features to Qt 5 in a major way.</li>
<li>Continued porting and modularizing KDE's libraries, and finally shipped them as Frameworks 5.0.</li>
<li>Not insignificantly, kept up supporting and releasing KDE 4  in parallel (with a major overhaul of the semantic desktop implementation in 4.13).</li></ul>

Kick-ass. It's been an amazingly productive time in the KDE community lately (not just in Plasma and Frameworks - KF5 porting of apps is going on as well!), and we'll be back in just three months with yet more.

Back to KDevelop.