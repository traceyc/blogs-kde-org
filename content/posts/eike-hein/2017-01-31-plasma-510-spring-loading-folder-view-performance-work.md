---
title:   "Plasma 5.10: Spring-loading in Folder View; performance work"
date:    2017-01-31
authors:
  - eike hein
slug:    plasma-510-spring-loading-folder-view-performance-work
---
I was sorely remiss not to blog more during the Plasma 5.9 dev cycle. While 5.9 packs a fair amount of nice new features (e.g. here's the <a href="https://www.youtube.com/watch?v=ajIzfU0eJtI">widget gallery in <i>Application Dashboard</i></a> at some point during development), there was not a peep of them on this blog. Let me do better and start early this time! (With 5.9 out today ...)

<b>Folder View: Spring-loading</b>

<center><a href="https://www.youtube.com/watch?v=QIRGoOzbp88"><img src="https://www.eikehein.com/kde/blog/fvspringload/spring_loading.gif" alt="Spring-loading in Folder View" /></a><br /><small><i>Spring-loading functionality in Plasma 5.10's Folder View (click for <a href="https://www.youtube.com/watch?v=QIRGoOzbp88>YouTube</a>)</i></small><br /><br /></center>

<i>Folder View</i> in Plasma 5.10 will allow you to navigate folders by hovering above them during drag and drop. This is supported in all three modes (desktop layout, desktop widget, panel widget), and pretty damn convenient. It's a well-known feature from Dolphin, of course, and now also supported in Plasma's other major file browsing interface.

Folder View packs a lot of functionality - at some point I should write a tips & tricks blog on some of the lesser known features and how they can improve your workflow.

<b>Performance work in Folder View ... and elsewhere!</b>

But that's not all! I've also been busy performance-auditing the Folder View codebase lately, and was able to extract many savings. Expect massively faster performance scrolling big folders in big Folder View widgets, lower latencies when navigating folders, and greatly improved Plasma startup time when using Folder View widgets on the desktop. In the case of big folder + big widget, a 5.10 Folder View will also use quite a bit less memory.

I've done similar analysis of other applets, e.g. the <i>Task Manager</i> and the <i>Pager</i>, and done both smaller improvements or looked into more fundamental Qt-level issues that need addressing to speed up our UIs further.

Others on the Plasma team have been up to similar work, with many performance improvements - from small to quite large - on their way into our libraries. They improve startup time as well as various latencies when putting bits of UI on the screen.

While it's still very early in the 5.10 cycle,  and it won't be shy on features by the end, performance optimization is already emerging as a major theme for that upcoming release. That's likely a sign of Plasma 5's continuing maturation - we're now starting to get around to thoroughly tuning the things we've built and rely on.