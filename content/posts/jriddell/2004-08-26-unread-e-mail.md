---
title:   "Unread e-mail"
date:    2004-08-26
authors:
  - jriddell
slug:    unread-e-mail
---
It's taken me two days solid but I'm down to 1 unread e-mail.  It feels so satisfying.

Today I went to the impressive <a href="http://wiki.kde.org/tiki-index.php?page=KDE+PIM+Platform+Independent+Demonstration">KOrganiser on Windows demonstration</a>.  KDE programs on Windows would be a great way of introducing more people to Free Software and KDE.

The KDE PIM project has a library called microKDE.  There is also the impressive looking <a href="http://www.iidea.pl/~js/qkw/">Qt KDE Wrapper</a> which looks like doing much the same thing.  There may be also be a port of the GPL'd Qt X11 to win32 but I havn't found that and nobody seemed quite sure if it worked or not.  It would be great if some dedicated Windows hacker could port more KDE applications to Windows, we frequently get requests for Umbrello on Windows.
<!--break-->

