---
title:   "Ubuntu and VNC on EC2"
date:    2012-04-03
authors:
  - jriddell
slug:    ubuntu-and-vnc-ec2
---
Lots of KDE SC 4.8.2 packaging and testing today.  But some folks can't test it easily because they have small bandwidth or they don't want to install experimental packages on their local computer.  I've been using EC2 machines for a while, which you hire by the hour from Amazon Web Services.  Whenever someone in Kubuntu needs a machine to compile or test and install on I run my script to get one going and give them access.  They're also handy for showing people how to do e.g. packaging, you can log into the same machine and share a screen session to see what the other person is doing.  

Today I finally got round to working out how VNC works and it works really well.
-set up a Security Group with port <i>5900 - 5950</i> open (and <i>22</i> for ssh)
-run an ec2 machine using that Security Group with an Ubuntu image - e.g. you can copy and paste the command from here if you have an account set up http://uec-images.ubuntu.com/releases/precise/beta-2/
-<i>sudo apt-get install kubuntu-desktop</i> (or whatever you want to test)
-<i>sudo apt-get install tightvncserver</i>
-run  <i>vncserver</i>  and set a password
-on your local machine run krdc and connect to the EC2 machine and the port reported when running vncserver e.g. <i>vnc://ec2-50-17-135-186.compute-1.amazonaws.com:5901</i>
-from your ssh login on the EC2 machine <i>export DISPLAY=:1</i> and <i>startkde</i>

You can share the VNC details around if you want to work on something collaboratively such as running a tutorial, awesomeness.

<a href="http://people.canonical.com/~jriddell/vnc.png"><img src="http://people.canonical.com/~jriddell/vnc-wee.png" width="300" height="179" /></a>
Testing Plasma Desktop 4.8.2 due for release any moment now
<!--break-->
