---
title:   "KDE Frameworks 5 Alpha 2 is Green"
date:    2014-03-11
authors:
  - jriddell
slug:    kde-frameworks-5-alpha-2-green
---
Scarlett has been working hard on packaging KDE Frameworks 5 Alpha 2 and <a href="http://qa.kubuntu.co.uk/kf5-status/build_status_4.97.0_trusty.html">the build status page</a> shows a sea of green (the only yellow is when a framework is asking for a package which doesn't exist yet).  Just in time for Plasma Next to get its Alpha release this week coming :)  Grab the KF5 packages from the <a href="https://launchpad.net/~kubuntu-ppa/+archive/experimental">experimental PPA</a> for Kubuntu Trusty.
