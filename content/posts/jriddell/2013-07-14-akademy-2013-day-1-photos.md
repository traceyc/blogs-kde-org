---
title:   "Akademy 2013 Day 1 in Photos"
date:    2013-07-14
authors:
  - jriddell
slug:    akademy-2013-day-1-photos
---
<a href="http://www.flickr.com/photos/jriddell/9277768085/" title="13070001 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7410/9277768085_5742b027ff.jpg" width="500" height="282" alt="13070001"></a>
Eva Galperin from the EFF talks on surveillance "Help us Free Software you are our only hope"

<a href="http://www.flickr.com/photos/jriddell/9277767843/" title="13070003 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2807/9277767843_baaa1f8cd1.jpg" width="500" height="282" alt="13070003"></a>
Kubuntu sponsor's poster on stage with good-to-see new sponsor Blackberry

<a href="http://www.flickr.com/photos/jriddell/9280553048/" title="13070004 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2892/9280553048_56ebcd59c9.jpg" width="500" height="282" alt="13070004"></a>
Did I mention the white wine on the rooftop terrace infront of the Guggenhein?

<a href="http://www.flickr.com/photos/jriddell/9277767443/" title="13070007 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7419/9277767443_68930d6615.jpg" width="500" height="282" alt="13070007"></a>
Talk Audience 

<a href="http://www.flickr.com/photos/jriddell/9280552382/" title="13070012 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2886/9280552560_7269ecaee7.jpg" width="500" height="282" alt="13070012"></a>
As if this Akademy wasn't classy enough already we had a party in a music venue with these Cuban ladies Las Dos D singing in Basque.  Sorry Aaron your karaoke skills are no longer required.

<a href="http://www.flickr.com/photos/jriddell/9280552382/" title="13070018 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3695/9280552382_5847980473.jpg" width="500" height="282" alt="13070018"></a>
Basque Celtic Fusion.  Exactly like a Celtic Connections concert back at home.  Most classy Akademy ever.
<a href="http://www.flickr.com/photos/jriddell/9280552280/in/photostream/">See the Basque Celtic Fusion video from Adarrots band</a>
