---
title:   "Photo Retrospective: Ubuntu"
date:    2013-12-27
authors:
  - jriddell
slug:    photo-retrospective-ubuntu
---
I'm done triaging old photos, here's some of my favourite Ubuntu themed ones from 2005 to 2009.

<a href="http://www.flickr.com/photos/jriddell/11502554675/" title="2005-04-18-sydney-geeks-asleep-oliver-andreas by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3792/11502554675_40156a0c1c.jpg" width="500" height="375" alt="2005-04-18-sydney-geeks-asleep-oliver-andreas"></a>
The first Ubuntu conference I went to in Sydney featuring Andreas, one of the Kubuntu originators

<a href="http://www.flickr.com/photos/jriddell/11502698064/" title="img 0279 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2893/11502698064_f1528107d7.jpg" width="500" height="375" alt="img 0279"></a>
The excitable Jeff Waugh who provided a lot of the character behind Gnome and early Ubuntu.  Last seen writing a lament about <a href="http://bethesignal.org/blog/2011/03/12/relationship-between-canonical-gnome/">the Canonical’s worsening relationship with GNOME</a> which I must admit to being too lazy to read.

<a href="http://www.flickr.com/photos/jriddell/11502721506/" title="2009-05-29-kubuntu-groupphoto-uds-barcelona by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5492/11502721506_7d83672f6a.jpg" width="500" height="284" alt="2009-05-29-kubuntu-groupphoto-uds-barcelona"></a>
A Kubuntu group photo in Barcelona from 2009, little did I know Barcelona would become a spiritual and practical home for Kubuntu.

<a href="http://www.flickr.com/photos/jriddell/11502673724/" title="2008-12-06-uds-hot-tub by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2882/11502673724_9ae5666d88.jpg" width="500" height="375" alt="2008-12-06-uds-hot-tub"></a>
It's important to build community in open source, Kubuntu has always used hot tub parties for this, you won't get that with any other distro.

<a href="http://www.flickr.com/photos/jriddell/11502630374/" title="2005-10-26-montreal-jonathan-mvo-seb-canonical-1 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7362/11502630374_03c6679773.jpg" width="500" height="375" alt="2005-10-26-montreal-jonathan-mvo-seb-canonical-1"></a>
Canonical One flight to Montreal, these days I prefer to take the train, more environmentally friendly.

<a href="http://www.flickr.com/photos/jriddell/11502685376/" title="2006-03-10-uisprint-canonical by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3790/11502685376_58bbbfbcdf.jpg" width="500" height="333" alt="2006-03-10-uisprint-canonical"></a>
The first LTS release was preceeded by a polishing sprint in London where we worked on the first version of Ubiquity, the live CD installer.

<a href="http://www.flickr.com/photos/jriddell/11502639444/" title="2006-01-09-kubuntu-breezy-cds by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3742/11502639444_d3f8ffb4b2.jpg" width="375" height="500" alt="2006-01-09-kubuntu-breezy-cds"></a>
Back in the day I had to post out every CD by myself.  (I still get requests for CDs but we stopped having any physical media some time ago).

<a href="http://www.flickr.com/photos/jriddell/11502676243/" title="2005-07-12-paul-kubuntu by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3822/11502676243_b30e17b9c5.jpg" width="500" height="374" alt="2005-07-12-paul-kubuntu"></a>
Paul taking his Ubuntu evangelising a little too seriously.

<a href="http://www.flickr.com/photos/jriddell/11502665293/" title="2005-04-29-sydney-jonathan-pool2 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3778/11502665293_0b5a1267ca.jpg" width="500" height="375" alt="2005-04-29-sydney-jonathan-pool2"></a>
Ubuntu summits were often in fancy hotels which always surprised me by nobody using the swimming pool, I feel if life gives you a roof top swimming pool it's just ungrateful not to use it.

<a href="http://www.flickr.com/photos/jriddell/11502617746/" title="2005-04-02-sydney-jonathan-hacking-river by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2844/11502617746_144f91eeed.jpg" width="500" height="375" alt="2005-04-02-sydney-jonathan-hacking-river"></a>
I like to go for a canoe if possible when I travel the world.

Have a fun holiday all, come and join us at Kubuntu if you want to be part of more world adventures.
