---
title:   "Kubuntu Developers Summit, Paris"
date:    2006-06-20
authors:
  - jriddell
slug:    kubuntu-developers-summit-paris
---
The Ubuntu Developers Summit is now happening by Charles De Gaulle airport in Paris. We have a bunch of KDE developers here to help with the specs for Kubuntu Edgy in four months time.  We've made our own Kubuntu schedule because the automatic BoFicator didn't schedule anything for us.  If you're near Paris do drop in and say hi.  The sessions are supposed to be broadcast on teamspeak so anyone can join in but in practice this doesn't seem to be working too well.

The beer here costs a shocking €9 so last night we went out and found the one local pub and found beer for €2 which is much more acceptable.  Aaron came back at 04:30 and although he was up at 09:00 he hasn't been seen since.

<img src="http://static.flickr.com/78/170627390_31f13582cd.jpg?v=0" class="showonplanet" />

The Summit

<img src="http://static.flickr.com/45/170627323_929c1adcdb_m.jpg" class="showonplanet" />

Aaron's birthday.

<img src="http://static.flickr.com/44/170627257_d7114d8e6d_m.jpg" class="showonplanet" />

Me and ervin at the funfair on the dodgems!
<!--break-->