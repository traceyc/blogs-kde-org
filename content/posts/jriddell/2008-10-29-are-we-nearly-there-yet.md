---
title:   "Are we nearly there yet?"
date:    2008-10-29
authors:
  - jriddell
slug:    are-we-nearly-there-yet
---
Almost!

<img src="http://people.ubuntu.com/~jriddell/medium.en.png" border="0"/>

More testers wanted though, join us in #kubuntu-devel for the final sprint.
<!--break-->
