---
title:   "4.4 Release Party in Washington DC and Glasgow"
date:    2010-02-18
authors:
  - jriddell
slug:    44-release-party-washington-dc-and-glasgow
---
Last week I went to Washington DC where I was meant to be giving a talk at CALUG.  However the whole city was buried in a metre of snow so it got cancelled.  In fact the whole government shut down.  Good thing the US wasn't doing anything important last week or people would have noticed they had no government and anarchy would have broken loose, it would be like Belgium.  The snow was mostly ploughed out the way by the end of the week in time for the KDE 4.4 release party organised by Celeste.  The release party started off in true US style in a fast food burger restaurant where I ate Ostrich. Later it moved to a bar with a fine selection of beers of many interesting flavours, gosh it was like Belgium!

On Saturday we'll be hosting the first ever <a href="http://community.kde.org/Promo/ReleaseParties/4.4#Glasgow">release party in Glasgow</a>.  Do come along if you're in the country.

<img src="http://people.canonical.com/~jriddell/4353735021_8dbe969806.jpg" />
Washington DC KDE SC 4.4 release party, eagerly awaiting beers.

<img src="http://people.canonical.com/~jriddell/4354483350_5a295c81b0_b.jpg" />
Kubuntu developers visit the White House.  Obama was snowed in and couldn't make our meeting so we had to take tourist photos instead.
<!--break-->
