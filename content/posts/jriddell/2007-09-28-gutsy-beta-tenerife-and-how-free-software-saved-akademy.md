---
title:   "Gutsy Beta, Tenerife and How Free Software Saved Akademy"
date:    2007-09-28
authors:
  - jriddell
slug:    gutsy-beta-tenerife-and-how-free-software-saved-akademy
---
<a href="https://wiki.kubuntu.org/GutsyGibbon/Beta/Kubuntu">Kubuntu Gutsy Beta</a> is out.  The upgrade from Feisty is under-tested so if you are running Feisty please <a href="https://help.ubuntu.com/community/GutsyUpgrades#head-3cb12417f0af7f24d4a34f2ae4040bf791c42f52">follow the upgrade instructions</a> and let me know how you get on.

The other part which needs testing is Strigi, it has a nasty habit of using up lots of CPU and of course disk space, do try it out and let us know if it should be kept on by default.

<hr />

I came back from Tenerife after a fun week in the company of the university, local Kubuntu and Linux community.  I gave a successful tutorial about how to package for Kubuntu, using bzr and Launchpad and PyKDE.  After 4 hours of talking I was quite horse, but it was fun.  I also gave a talk at the conference proper on Kubuntu and what's new in Gutsy.  Aaron Seigo was also there and gave an inspirational talk on KDE 4.  Me and Aaron went to see one of the schools which have been installed with Kubuntu, all great stuff.

Alan Runyan of CMS Plone also gave a talk.  He told us the origin of the term "Sprint" as having come from Zope's Tres Seaver and how they use Sprints to develop Plone.  The Plone Foundation requires everyone to sign over their copyright which they consider important and only a barrier to a few potential contributors.  They were also the first customer of the Free Software Legal Centre which was painful but useful.  Computer Associates gave them a hundred thousand dollars which must have been nice.  He had harsh words for MySQL and the lack of freely available sources for their enterprise version, he thinks MySQL is a dead end for users of Free software because of this.

<hr />

<a href="http://akademy2008.kde.org/">Akademy 2008</a> has <a href="http://dot.kde.org/1191001763/">been announced</a> and it's going to be in Belgium.  Expect lots of waffles, good beer and chips.

Last year's Akademy has wrapped up quite nicely, I still have some accounting to do but the organisation generally went without major problems.  

Being part of organising Akademy offered me a couple of times when Free Software really showed how far it has come.  One of our sponsors has a practise of sending out PDF files which need to be signed and sent back (as PDF).  Being the geek type I did a PGP signature and send it back explaining what PGP was and if it needed a  physical signature how should I do that, only reply was that I had "given no signature".  There was no fax number or postal address to send it back with a physical signature so this large company seems to need people to add a physical signature to a PDF .  Making an image of my signature is easy to do in Krita with my mouse, not entirely sure it would stand up in court but adding it to the PDF I was stumped until I remembered a Qt programme called pdfedit which worked wonderfully.  Goodness knows how they expect anyone else to receive a payment.

Then there were the banners that needed printing with the sponsor's logos on it.  Scribus did great for making them up (it also did great for the booklet), but the proofs came back as a JPEG in CMYK colours which doesn't work in Konqueror.  We were a bit stumped on how to check the colours were valid until I remembered Krita was installed which worked perfectly, something Gimp can't do.  I'm using Krita for pretty much all my image editing these days (as seen on the <a href="http://akademy2008.kde.org">Akademy 2008 website</a>).  So that's how Krita and pdfedit saved Akademy.

<img src="http://static.kdenews.org/jr/akademy-2008-launch.png" width="396" height="262" />

Funky launch logo by Srikrishna Das (krish).

<a href="http://muse.19inch.net/~jr/meduxa_demostration_02.jpg"><img src="http://muse.19inch.net/~jr/meduxa_demostration_02-wee.jpg" width="350" height="263" /></a>

Me and Aaron with the people putting Kubuntu into al lthe Canary Island schools.

<!--break-->