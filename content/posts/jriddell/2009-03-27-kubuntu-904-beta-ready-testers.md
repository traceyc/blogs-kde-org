---
title:   "Kubuntu 9.04 Beta Ready for Testers"
date:    2009-03-27
authors:
  - jriddell
slug:    kubuntu-904-beta-ready-testers
---
<img style="margin: 1ex" src="http://www.kubuntu.org/~jriddell/kubuntu-bru.jpg" width="188" height="380" align="right" />
The beta for our forthcoming 9.04 is available for download and testing.  It features improved graphics and more slick applications.  To see what's new and how to download go to the <a href="https://wiki.kubuntu.org/JauntyJackalope/Beta/Kubuntu">beta information page</a>.  You can <a href="https://help.ubuntu.com/community/JauntyUpgrades/Kubuntu">upgrade from 8.10</a> or <a href="https://help.ubuntu.com/community/JauntyUpgrades/Kubuntu/8.04">from 8.04</a>.  Let us know what you think on the <a href="https://wiki.kubuntu.org/JauntyJackalope/Beta/Kubuntu/Feedback">feedback page</a> and report bugs to <a href="http://launchpad.net/ubuntu">Launchpad</a>.
<br clear="all" />
<!--break-->
