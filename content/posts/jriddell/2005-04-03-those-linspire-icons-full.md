---
title:   "Those Linspire Icons In Full"
date:    2005-04-03
authors:
  - jriddell
slug:    those-linspire-icons-full
---
With Linspire 5 out and the sources tracked down we can now get to see the new Crystal icon set commitioned by Linspire.  Very shiny.  They're under a proprietry Linspire licence at the moment so the question of whether KDE will use them hasn't come up yet and just maybe the artists are working on their own super sekret plans in the icons area.  Also there's no SVGs yet, sigh, anyone caught adding icons to KDE without SVGs will be thrown to the gimp.  Also alert viewers might spot the folder_{voliet,yellow,orange}.png icons in the lower pane of the screenshot below, they were made by me and licenced under the LGPL so quite how they end up in a .deb which does not include any mention of the LGPL is a mystery.

<p><b>Update:</b> don't take the licence issue above as a reason to flame Linspire, it's just sloppy packaging and nothing malitious, we'll send them a polite e-mail and end up best of friends again.  Their sponsorship of kde-look and kde-apps is invaluable.


<a href="http://muse.19inch.net/~jr/linspire-icons.png"><img src="http://muse.19inch.net/~jr/linspire-icons-wee.png" width="600" height="450" class="showonplanet" /></a>
<!--break-->

