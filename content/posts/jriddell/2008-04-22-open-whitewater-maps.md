---
title:   "Open Whitewater Maps"
date:    2008-04-22
authors:
  - jriddell
slug:    open-whitewater-maps
---
<a href="http://www.openstreetmap.org/">OpenStreetMap</a> can get addictive once you discover how easy and powerful the flash editor is.  But what, my girlfriend asked, is the point when you can get maps at no cost from Google et al.  The justification is that unrestricted by copyright you can do interesting things that you would be otherwise unable to do.  <a href="http://www.gravitystorm.co.uk/osm/">OpenStreetMap Cycle Map</a> shows all the best routes for a bike.  <a href="http://openpistemap.org/">Open Piste Map</a> makes maps for skiing</a>.  So this evening I made <a href="http://krissy.homelinux.com/">Open Whitewater Map</a> to create maps for canoeing and other river users showing places to get in and out, rapids and their grades and hazards on the river too.  The <a href="http://wiki.openstreetmap.org/index.php/WikiProject_Whitewater_Maps">WikiProject Whitewater Maps page</a> has the info.  Only a few river routes done so far, any canoeists out there want to add some more?

It's currently on my ADSL line, so may disappear, offers of hosting welcome.

<img src="http://wiki.openstreetmap.org/images/thumb/c/c9/Openwhitewatermap.png/800px-Openwhitewatermap.png" width="800" height="414" />
<!--break-->
