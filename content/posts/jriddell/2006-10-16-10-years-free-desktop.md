---
title:   "10 Years of the Free Desktop"
date:    2006-10-16
authors:
  - jriddell
slug:    10-years-free-desktop
---
This weekend sees the anniversary of 10 years of the Free desktop.  Well done to Tackat for organising what <a href="http://www.unix-ag.uni-kl.de/~fischer/10yearsKDE/">looks like a great stream of talks and party</a>.  

We've come a long way from that <a href="http://www.kde.org/announcements/announcement.php">first announcement</a> in 1996, but interestingly the task of "...createing a GUI for an ENDUSER, somebody who wants to browse the web with Linux, write some letters and play some nice games" was achieved many years ago, it's all been polishing since then.  

The other funky news is KDE e.V.'s new supporting members scheme for those who want to make a finantial contribution to KDE, and the first dude to sign up is <a href="http://dot.kde.org/1160932072/">Mark Shuttleworth</a>, who is now a Patron of KDE.  Thanks Mark.

If you want to win a copy of danimo's new Qt 4 book <a href="http://www.jonobacon.org/?p=783">send Jono a good excuse</a>.

<img src="http://static.kdenews.org/jr/10yearskde.jpg" width="273" height="184" class="showonplanet" />
<!--break-->