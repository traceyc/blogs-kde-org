---
title:   "#savetheday party 19:50UTC in #kubuntu-devel"
date:    2013-11-23
authors:
  - jriddell
slug:    savetheday-party-1950utc-kubuntu-devel
---
<a href="http://www.youtube.com/watch?v=X80Xq-sHwIw">#savetheday</a> party 19:50UTC in Munich and #kubuntu-devel from a project which has always taken inspiration from the Timelord.