---
title:   "Ubuntu Open Week"
date:    2008-11-05
authors:
  - jriddell
slug:    ubuntu-open-week
---
<a href="https://wiki.kubuntu.org/UbuntuOpenWeek">Ubuntu Open Week</a> talks are happening all week on IRC.  Today includes Kubuntu man Jonathan Thomas on "reinventing QA the resource-limited way" at 18:00UTC.
