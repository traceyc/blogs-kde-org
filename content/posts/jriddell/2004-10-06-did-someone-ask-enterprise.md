---
title:   "Did someone ask for Enterprise?"
date:    2004-10-06
authors:
  - jriddell
slug:    did-someone-ask-enterprise
---
I'm not sure what the definition of enterprise is, it sounds a lot like a meaningless buzzword to me, but I suspect that what I saw last Friday comes into the category of enterprise.  I was invited to visit the finishing touches being put to the nice new building below and more importantly their new computer system which uses KDE on the desktop.  There will be about 250 users moving in this week and so far they love it.  There has been only one person complaining, he can't move the icons on the desktop.  That would be Kiosk.

They want to keep it quiet for now, they expect a lot of interest but don't want visitors coming to see the setup until they iron out all the flaws.  They also don't want MS to hear of it for various reasons.

Don't get too many romantic ideas of KDE taking over the world just yet, this setup is designed to look like MS Windows to minimise new user adjustments - the kmenu sports a Windows logo, the web browser has an IE icon and the widget and icon themes are Windows.  Cross platform programmes are used, OpenOffice and Mozilla among others, the result is that the desktop is a commodity.  Because of its flexible and controlable framework KDE just happens to be the best one to choose.

<img class="rapemewithachainsawthanks" src="http://jriddell.org/photos/2004-10-04-new-building.jpg" />
<!--break-->