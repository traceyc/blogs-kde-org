---
title:   "Kubuntu Release Candidate"
date:    2005-03-31
authors:
  - jriddell
slug:    kubuntu-release-candidate
---
The <a href="http://www.kubuntu.org/hoary-release-candidate.php">Kubuntu Release Candidate</a> is out.  This is your last change to test and complain before our first release next week.  Feedback so far has been good and #kubuntu has been increasing in numbers enough that it's hard to keep up.  Both <a href="http://shots.osdir.com/slideshows/slideshow.php?release=286&slide=1">OSNews</a> and  <a href="http://scr3.golem.de/?d=0503/kubuntu504&a=37029">golum.de</a> carried screenshots.  

<img src="http://www.kubuntu.org/konqi2.png" class="showonplanet" />
<!--break-->
