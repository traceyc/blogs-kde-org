---
title:   "Ageing Gracefully"
date:    2011-12-24
authors:
  - jriddell
slug:    ageing-gracefully
---
Earlier this year <a href="http://www.jonobacon.org/2011/09/16/getting-older/">Jono mused about getting older</a>.  Ah bless, I remember him when he was just a whippersnapper promoting KDE at a warehouse in Birmingham.

Today is my turn to reach my 30s and I had it all planned out.  I was going to be living on a tropical island with loads of awesome beaches, reliable and warm surf to learn how to use a waveski (sit on top canoe kayak for surfing) while speaking fluent French and drinking ti'punch (rhum agricole, lime, and cane syrup).  I spent a lovely month there meeting new people, trying new things, working in the sunshine with a swimming pool a meter away for when something is compiling.  

But I did not count on one problem, the French drive on the wrong side of the road.  I don't remember the accident but I remember a couple of days in hospital being like a dream drifting in and out of consciousness, speaking (what I thought was) surprisingly good French to the doctors.  After three days they sent me home in a taxi with my MRI scan results, which being concussed I left in the taxi.  Fortunately I have an excellent family and my dad flew out there to sort out the status with the polis (who had got a story from the guy who crashed into me that sounded like he had learnt his physics from car chase films) and the car rental company (that car won't be on the roads again) and the canoe club (nice kit rescued from the smashed up car) and took me on a flight back home.  

The lovely NHS (the independent Scottish one, not the about-to-be-privatised English one) had GPs and MRI scanners and eye specialists look at me.  My eyes are very squint, muscle damage in one means the image is at a different angle in each of the eyes and I can only look out of each eye one at a time.  The doctors expect this to get better over the next weeks to months.  My brain feels a bit woozy, like I've been sipping low quality beer.  I'm feeling more lethargic then usual and because everyone tells me to rest I don't want to set my alarm clock early so have no way to get back on European time, a quick prescription for metacinin should fix that (surprisingly hard to get hold of, in most countries it's not even a prescription thing).

So Kubuntu has been a bit slow to get started and we missed the first alpha (testing ISOs can be boring but this time I really was comatose) for the precise release but all the packages got merged and we have bits like a KDE SC and Calligra betas packaged.  I've a long todo list which should keep me busy next year.  

And I very much love my life, excellent family, top friends, satisfying physical hobby (nearly done building the east wing to the canoe club) and excellent awesome intellectual one which I'm fortunate enough is also my job (lets see if Kubuntu can help Plasma Active do that world domination thing in 2012).

I'm looking forward to my 30s.  Happy Riddellmas and have a great hogmanay to start 2012.
<!--break-->
