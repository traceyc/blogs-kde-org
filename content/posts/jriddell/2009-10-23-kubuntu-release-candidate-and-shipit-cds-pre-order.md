---
title:   "Kubuntu Release Candidate and Shipit CDs on Pre-order"
date:    2009-10-23
authors:
  - jriddell
slug:    kubuntu-release-candidate-and-shipit-cds-pre-order
---
Kubuntu 9.10 CDs are now available for pre-order on the <a href="https://shipit.kubuntu.org/">Kubuntu Shipit site</a>.

Can't wait a month for the CD to be delivered?  Download the <a href="https://wiki.kubuntu.org/KarmicKoala/RC/Kubuntu">release candidate</a> now for one last round of testing.  Thanks to everyone who helped test the RC releases, see you again next week for the real thing!

<img src="http://people.canonical.com/~jriddell/karmic-countdown-banner/6.png" width="849" height="168" />
<!--break-->
