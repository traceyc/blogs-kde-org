---
title:   "Barcelona Plasma and KDE Frameworks 5.0 Release party"
date:    2014-07-17
authors:
  - jriddell
slug:    barcelona-plasma-and-kde-frameworks-50-release-party
---
Barcelona Free Software Users & Hackers are having a <a href="http://www.meetup.com/Barcelona-Free-Software-Users-Hackers/events/195489682/?fromJoin=195489682">Barcelona Free Software Users & Hackers</a> mañana, see you there!

<img src="http://people.ubuntu.com/~jr/konqi-catalan.png" width="800" height="580" />
