---
title:   "Linux Desktop Ready for the Users"
date:    2009-11-11
authors:
  - jriddell
slug:    linux-desktop-ready-users
---
I often say that my hypothetical user for Kubuntu is my non-technical girlfriend.  Unfortunately I'm between girlfriends at the moment but the intent is still there. Someone who uses the computer for everyday tasks of web browsing, chatting, watching videos, listening to music, writing some documents, storing photos.  But also someone who doesn't care about computers any more than I care about my car, it should do the job but I don't want to have to fiddle with it to get it to work.  Unfortunately I rarely get the chance to try out Kubuntu on such users, my family have all been using computers far longer than I have and are just as stuck in their ways as I am in mine.  

But yesterday I did get a chance to install Kubuntu on a dying WIndows machine.  The worst part of the process was booting up Windows to see how much disk space there was, it took 20 minutes to boot up loading all the half broken vendor apps and anti-virus software.  Then it took 20 minutes just to shut down, every 30 seconds interrupted by a "missing .dll" or similar dialogue.

So up boots Kubuntu from the CD in half the time than Windows boots from hard disk.  All hardware working perfectly.  The disk partition resizing is a breeze, installation works without problems.  

We came across two small problems.  Knetworkmanager doesn't give much feedback if you use the wrong wifi encryption or password method, and the Firefox installer claims it has nothing to do if you don't have an apt cache first.  Both known issues which I expect will be fixed before long.

So far one very happy customer, off to browsing the web in 1 minute beats Windows by a factor of 20.  But that is just the install.  I'll find out in the coming days how someone who doesn't care about computers manages with the everyday tasks that we all expect to work easily.
<!--break-->
