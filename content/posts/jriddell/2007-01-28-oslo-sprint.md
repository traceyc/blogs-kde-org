---
title:   "Oslo Sprint"
date:    2007-01-28
authors:
  - jriddell
slug:    oslo-sprint
---
Canonical sent its distro team to Oslo last week, to spend the time locked in a hotel room making sure all the Feisty feature goals got done.  I worked on the dist-upgrade tool and the software-properties apt sources.list editor, as well as finishing and uploading KDE 3.5.6 and KDevelop 3.4.  The tricky part with the dist-upgrade tool is patching konsole to be able to attach to an existing pty (from apt), fortunately mvo was a here there and dived into the meaty code of kpty.  The result is that upgrading from edgy to feisty should be much smoother than from dapper to edgy.

We did get let out of the hotel occationally into the dark and snowy streets of Oslo where my Quaker Friend Asbjørn showed me the sights including a bar called The Scotsman that doesn't sell a single Scottish drink.  Me and Ken (who is now working on the artwork for both Kubuntu and Ubuntu), met up with a bunch of Trolls from Trolltech including a KDE coder I'd not met before, Frans Englich.  On the last day we all went bowling where everyone was beaten horribly by Ben "the curve" Collins with his triple strike on the 10th frame.

<img src="http://kubuntu.org/~jriddell/tmp/dist-upgrader.png" width="430" height="375" class="showonplanet" />
<!--break-->
