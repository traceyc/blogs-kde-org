---
title:   "Bits"
date:    2005-02-03
authors:
  - jriddell
slug:    bits
---
Today was string freeze day for KDE, over 500 commits were made and lots of people were desperatly trying to make the deadline for i18n().  I was struck down with a cold and couldn't manage much but did add some bits to Kontact's new about screen.

In other news <a href="http://opensource.org/pressreleases/expansion.php">another step</a> in the plan for Quaker domination of FOSS took place.  

And this is how KControl should look.  KDE has a major problem with anyone being able to make usability improvements, the sane defaults thread on Konqueror seems to have lead to nowhere, sigh.  But this is a definate candidate for KDE 4 (or do I hear wishes for a 3.5?).

webcvs:kdenonbeta/systempreferences/

<img class="showonplanet"  src="http://www.icefox.net/gallery/cache/2005/System%20Preferences/640_system_preferences.png"  />
<!--break-->
