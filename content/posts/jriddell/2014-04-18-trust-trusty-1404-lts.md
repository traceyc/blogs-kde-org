---
title:   "Trust in Trusty 14.04 LTS"
date:    2014-04-18
authors:
  - jriddell
slug:    trust-trusty-1404-lts
---
<img src="http://www.cornel1801.com/disney/Jungle-Book-Trust-Me/videosong.jpg" width="640" height="480" />
Trust in Me

You've been waiting for it, we've been working hard on it.. it's the new Long Term Support release of Kubuntu!

This means we've been working hard on removing bugs, polishing features and not adding new ones.  This will probably be the last release before KDE Frameworks 5 and Plasma Next gets introduced so for those who like to live life on the cautious side you'll be pleased to know the Long Term Support label means we'll have important bug fixes and security fixes for the next 5 years.  It'll also get backports of important KDE software for the next couple of years.

<img src="https://wiki.ubuntu.com/TrustyTahr/Final/Kubuntu?action=AttachFile&do=get&target=gwenview-promo.png" width="360" height="319" />
But that doesn't mean there's nothing new.  Take a look at the <a href="http://www.kubuntu.org/news/kubuntu-14.04">release announcement</a> for a long list.  For one thing we're the first distro to ship with <a href="http://dot.kde.org/2014/04/16/kde-releases-applications-and-development-platform-413">KDE SC 4.13 fresh out today</a>.  It brings a much nicer desktop search capability that makes search fly.

Muon is slicker, all new Driver Manager means hardware works better, Gwenview plugins mean it's easier to upload to Facebook, KDE Connect makes your phone talk to your laptop.

All wrapped up with the safety of <a href="http://kubuntu.emerge-open.com/buy">commercial support</a> if you need it and plenty of community support if you need that.

I'd like to thank Harald who put in a lot of effort in this release, even writing up release notes which I've never found anyone to help with before.  Rohan did crutial last minute bugfixes including at the last minute and nifty new features like the Driver Manager.  Aurelien took care of Ubiquity to get your installs looking nice.  We've <a href="http://docs.kubuntu.org/">all new documentation</a> thanks to Aaron and Valerie and others.  Scott kept the policy ticking over.  Phillip got things packaged, debfx had bug fixes when it was needed most, Michal keeping an eye on the important packages, Scarlett being the Queen of packaging for KF5 and others.  Really what a wonderful team effort.

And next?  We'll be looking at making KDE Frameworks usable, Plasma 2014.6 may be the next desktop and who knows we may even get something working with Wayland.  it's exiting! Come and join us, chat in #kubuntu-devel and join the kubuntu-devel mailing list.




