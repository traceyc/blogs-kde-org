---
title:   "LinuxWorld Expo"
date:    2004-10-06
authors:
  - jriddell
slug:    linuxworld-expo
---
Everything is set up for the expo tomorrow.

Matthias Ettrich will be there, which will be groovy.  There are piles of leaflets to give away.  We will have news of a 250 (going up to 1000) machine deployment of KDE.  We have lots of shiny computers on which to demo KDE.  I even helped to contrust the dreaded Debian shelves of doom.

<img class="rapemewithachainsawthanks" src="http://jriddell.org/photos/2004-10-05-expo-desktop-stall.jpg" />
<!--break-->