---
title:   "Kubuntu HWDB"
date:    2006-08-16
authors:
  - jriddell
slug:    kubuntu-hwdb
---
I've made a Qt 4 client to <a href="http://hwdb.ubuntu.com">Ubuntu's Hardware Database</a> which lets you anonymously submit details of your machine.  You can then attach your given ID to any bug reports and the data is also collected into interesting statistics for the Ubuntu distributions to find out about our user's hardware.

Grab the <a href="http://kubuntu.org/~jriddell/hwdb/">packages</a> and leave me feedback on <a href="https://wiki.kubuntu.org/KubuntuHWDBFeedback">the feedback wiki page</a>, they need Edgy since it needs the new python-qt4.

More KDE-fitted artwork coming once kwwi gets back from America.

As far as I know this is the first Free Software application to make use of PyQt4, Qt 4 certainly does make it easy to do some funky graphic effects.

<img src="http://kubuntu.org/~jriddell/hwdb.png" width="507" height="445" class="showonplanet" />
<!--break-->
