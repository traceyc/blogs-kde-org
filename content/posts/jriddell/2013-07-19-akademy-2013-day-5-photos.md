---
title:   "Akademy 2013 Day 5 in Photos"
date:    2013-07-19
authors:
  - jriddell
slug:    akademy-2013-day-5-photos
---
<a href="http://www.flickr.com/photos/jriddell/9315136645/" title="DSCF8114 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5349/9315136645_dc41479d9d.jpg" width="500" height="375" alt="DSCF8114"></a>
K people discussing KPeople

<a href="http://www.flickr.com/photos/jriddell/9315138117/" title="DSCF8115 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7351/9315138117_d7516f7af6.jpg" width="500" height="375" alt="DSCF8115"></a>
The organising team busy organising

<a href="http://www.flickr.com/photos/jriddell/9315139133/" title="DSCF8116 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3720/9315139133_7f97eba759.jpg" width="500" height="375" alt="DSCF8116"></a>
Solid developers don't liquidise in the heat

<a href="http://www.flickr.com/photos/jriddell/9317933546/" title="DSCF8127 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3731/9317933546_6c33d14132.jpg" width="500" height="375" alt="DSCF8127"></a>
An evening booze cruise 

<a href="http://www.flickr.com/photos/jriddell/9315143887/" title="DSCF8124 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5443/9315143887_9eac291c32.jpg" width="500" height="375" alt="DSCF8124"></a>
Fun in the sun
