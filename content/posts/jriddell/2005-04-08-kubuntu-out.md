---
title:   "Kubuntu is out!"
date:    2005-04-08
authors:
  - jriddell
slug:    kubuntu-out
---
The <a href="http://kubuntu.org/hoary-release.php">Kubuntu announcement</a> and preparations kept me up until about 5 o'clock last night just in time for a couple of hours kip before the announcement this morning.  The feedback has been good and there's been a few <a href="http://img87.exs.cx/my.php?loc=img87&image=10021237tu.jpg">sightings of Kubuntu in the wild</a>.  Many thanks go to the helpful people at Ubuntu who put up with us being somewhat newer to this game than them.

I have quite a few narferous plans for breezy, there's some interesting KDE developements out there which could be really useful.  Updates for KDE point releases and CVS .debs to satisfy both the gentle and bleeding edge user would be nice.  Ubuntu also has various utilities which we lack, update-notifier and simplified package manager for example.  Any programmers who want to help with that side would be more than welcome.  Kubuntu has reached position 11 in distrowatch for the last month, not bad for a beginner.  Find us in #kubuntu if you have ideas to make Kubuntu Breezy the best operating system there is.

<img src="http://www.kubuntu.org/konqi-book2.png" width="284" height="261" class="showonplanet" />
<!--break-->
