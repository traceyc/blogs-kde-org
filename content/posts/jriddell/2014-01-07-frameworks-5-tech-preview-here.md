---
title:   "Frameworks 5 Tech Preview is here!"
date:    2014-01-07
authors:
  - jriddell
slug:    frameworks-5-tech-preview-here
---
<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" />

<a href="http://dot.kde.org/2014/01/07/frameworks-5-tech-preview">Frameworks 5 Tech Preview</a> has arrived.

KDE Frameworks it a port of kdelibs to Qt 5 and turned into modules so you can install only the bits you need.  People are often reluctant to add kdelibs to their applications because it brings in too many dependencies.  With KDE Frameworks it has been modularised so much is simply extra Qt libraries.  This will bring KDE software to a much wider audience.  Many parts of kdelibs have just been moved into Qt itself thanks to the open qt-project.

<a href="http://community.kde.org/Frameworks/Binary_Packages">Binary packages</a> are available in Kubuntu as part of Neon 5 and I'm starting to package them in our <a href="https://launchpad.net/~kubuntu-ppa/+archive/experimental/+packages">experimental PPA</a>.
