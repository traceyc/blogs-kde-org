---
title:   "PDF programmes"
date:    2005-01-07
authors:
  - jriddell
slug:    pdf-programmes
---
KPDF being merged into HEAD has been the talk of the IRC channels for a while, everyone is impressed.  Here's the snazzy presenter mode:

<a href="http://muse.19inch.net/~jr/tmp/kpdf-present3.png"><img class="showonplanet" src="http://muse.19inch.net/~jr/tmp/kpdf-present3-wee.png" /></a>

Someone must have been watching closely because today has popped up copies of an Acrobat Reader 7 beta for Unix, available to those hand picked by Adobe (or who know where to get it).  It has the rendering speed advantage over kpdf but doesn't have the looks: to the surprise of most it uses GTK 2 and measures in at a whopping 110MB (it's not statically linked either and it is stripped).  

<a href="http://muse.19inch.net/~jr/tmp/acrobat7.png"><img class="showonplanet" src="http://muse.19inch.net/~jr/tmp/acrobat7-wee.png" /></a>

<!--break-->