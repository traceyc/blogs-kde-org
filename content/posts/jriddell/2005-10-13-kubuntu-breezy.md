---
title:   "Kubuntu Breezy"
date:    2005-10-13
authors:
  - jriddell
slug:    kubuntu-breezy
---
Kubuntu Breezy is out.  Grab it while it's fresh (and make sure you use a mirror that has synced).

http://kubuntu.org/announcements/breezy-release.php

There's some fun stuff in this releases, best of all is Adept the package manager.  At long last .deb distributions have a decent package manager.  It needs some extra features, usability review and a cut down version along the lines gnome-app-install but we have the ability to do all that now quite easily.  

Also in there is Guidance, a set of system configuration tools.  Guidance is one of the first non-trivial KDE programs not use C++, it's written in Python.  That's caused a lot of bother from a packagers point of view but it's exciting to see KDE move in that direction.

We also replace KControl with System Settings which makes it a lot easier to find the things you want.  It also means not all the modules fit on the screen, alt+drag is your friend.

Breezy is also the first place to get KDE 3.4.3 which umm, should have been released yesterday.  The last minute addition has caused some problems like KMail not working with GPG and media:/ not using HAL, I'll get fixes for those in -updates as soon as possible.  We didn't get KOffice 1.4.2 or Amarok 1.3.3 in unfortunately, those were too close (I'm really beginning to see the advantages of gnome's 6 predictable month releases).

The Kubuntu Live CDs come with some Free Software for Windows and 5.04 adds the best KDE Windows Free Software: Kexi, KAddressBook and KOrganiser.  

The best bit of a new release is the hidden extra features.  Not only can you now go ubw:Kubuntu in Konqueror and end up at the Kubuntu wiki page but the Google search box comes with Google suggest which is hours of fun.  Katapult is the new item launcher and much better than minicli, alt+space is your new friend.  Finally locate:/ works in Konqueror to quickly find files on your hard disk.

There was one other last minute addition, YaKuake was uploaded to Universe.  It's a console that you launch by pressing F12 and it slides down from the top of your screen.  It's modelled after the console in Quake.  Now that's style.

<img src="http://www.kubuntu.org/images/kubuntu-gurl-wee.jpg" width="300" height="281" class="showonplanet" />
<!--break-->