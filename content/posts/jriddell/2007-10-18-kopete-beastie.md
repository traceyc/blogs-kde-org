---
title:   "Kopete Beastie"
date:    2007-10-18
authors:
  - jriddell
slug:    kopete-beastie
---
Preparations for Gutsy's release continue apace.  #ubuntu-release-party is going a bit nuts.  Many thanks to those who have helped test CD candidates, davmor2 gets the prize for most testing this round I think.

Many people have noticed that Kopete has picked up a crash when connecting to MSN.  It's actually a problem in kdelibs SSL code.  The relevant bug is <a href="https://bugs.edge.launchpad.net/ubuntu/+source/kdenetwork/+bug/153500">153500</a> and you can grab a <a href="http://kubuntu.org/~jriddell/kdelibs4c2a_3.5.8-0ubuntu3_i386.deb">fixed .deb</a> to install with Gutsy's new .deb installer, GDebi.
<!--break-->
