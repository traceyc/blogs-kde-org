---
title:   "Launchpad.net is Free Software"
date:    2009-07-31
authors:
  - jriddell
slug:    launchpadnet-free-software
---
Incase you missed it, the whole of top project hosting site <a href="http://blog.launchpad.net/general/launchpad-is-now-open-source">Launchpad.net is now available</a> and <a href="http://blog.launchpad.net/general/writing-code-for-launchpad">accepting contributions</a>.  Complaints about it not having source code available always confused me, I never heard them about the once-open-now-closed Sourceforge.net and KDE seems perfectly happy to make use of proprietary websites like Google or kde-look.org.  But now there is no such excuse, Launchpad is the easiest way to host a project or even a quick script you hacked up that might be useful to someone else because you can set it up with a 1 page form then just bzr push.

<!--break-->
