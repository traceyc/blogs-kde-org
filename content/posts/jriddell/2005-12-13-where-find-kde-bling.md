---
title:   "Where To Find KDE Bling"
date:    2005-12-13
authors:
  - jriddell
slug:    where-find-kde-bling
---
Ubuntu Docs <a href="http://www.advogato.org/person/Burgundavia/diary.html?start=45">Corey</a> was trying to find KDE 3.5 and 4 screenshots on planet KDE.  Of course KDE screenshots aren't news to most KDE developers so they don't get blogged about but the <a href="http://www.kde.org/announcements/visualguide-3.5.php">KDE 3.5 visual guide</a> lists some of the new features.  KDE 4 can be found at <a href="http://www.canllaith.org/svn-features/kde4.html">KDE4 - understanding the buzz</a>, not much to see here I'm afraid most of the work has been happening on the libraries, that should be accelerated now that KDE 3.5 is out the way but don't expect fancy screenshots for a wee while yet.

Meanwhile the OSDL desktop-architects list got its first flame, which disappointed a lot of people since all the desktop developers agree we won't get anywhere if we're flaming each other.  Expect a more positive statement soon.

<a href="http://jriddell.org/photos/2005-12-01-osdl-gnome-kde-love.jpg"><img src="http://jriddell.org/photos/2005-12-01-osdl-gnome-kde-love.jpg" width="360" height="240" /></a>

Desktop Architects Love: Chris Blizzard (Red Hat), Nat Friedman (Novell/Gnome), Cornelius Schumacher (Novell/OpenSuSE/KDE), Aaron Seigo (KDE/Trolltech), Waldo Bastian (Intel/KDE/Freedesktop), Jeff Waugh (Ubuntu/Gnome), Daniel Stone (X.org/Freedesktop/formerly KDE), Jonathan Riddell (Kubuntu/KDE), Lars Knoll (Trolltech/KDE), Martin Konold (KDE), George Staikos (KDE), Jonathan Blandford (Gnome/Red Hat), Tim Ney (Gnome), Alex Graveley (Gnome)

See also the <a href="http://jriddell.org/photos/2005-12-01-osdl-group.jpg">full group photo</a> and have fun filling in the names.

<!--break-->