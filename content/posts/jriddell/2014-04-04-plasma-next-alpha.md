---
title:   "Plasma Next Alpha"
date:    2014-04-04
authors:
  - jriddell
slug:    plasma-next-alpha
---
<a href="http://dot.kde.org/sites/dot.kde.org/files/calendar.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/calendar_wee.png" /></a>

This week, as well as being a <a href="https://www.flickr.com/photos/jriddell/13578295503/">centrefold model in a tabloid rag</a>, another of my life ambitions came true when I had the glory of being the release dude.  <a href="http://techbase.kde.org/Schedules/Plasma/2014.6_Release_Schedule">Plasma 2014.6</a> is the first version of Plasma using KDE Frameworks 5 and the developers are hard at work coding on it.  The release schedule required an Alpha so I was tasked with working out how to <a href="http://download.kde.org/unstable/plasma/4.95.0/">release some tars</a>.

This is a very exciting release because it's the start of the next evolution of KDE Software. No major feature overhauls just a solid codebase to work from using nice technologies like QtQuick.  

This is also a very boring release because it's made up of kde-workspace and kde-runtime both of which are about to disappear as the archive gets modularised.  kde-runtime also overlaps with much of the kde-runtime from KDE SC 4 land so you can't install it alongside your normal KDE Software install.  We'll fix that.

I also included a release of Oxygen Fonts which is the new feature font for Plasma.  The developer of this has renamed it due to trademark issues to <a href="https://github.com/vernnobile/commeFont">Comme Font</a> but there's some alignment issues in Comme Font, plus it needs a copy of <a href="http://fontforge.github.io/en-US/downloads/gnulinux/">Font Forge from git</a> to generate the .ttf files which nobody seems to be able to compile.  Please tell me how if you can.

Packages are in the <a href="https://launchpad.net/~kubuntu-ppa/+archive/experimental/">Kubuntu Experimental PPA</a> for anyone who wants to try but we're still working out all the dependencies etc.  And it'll remove your existing KDE install, so you take your chances :)

It's the start of something amazing...
