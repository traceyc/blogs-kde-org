---
title:   "SVG Wallpaper Contest"
date:    2005-01-23
authors:
  - jriddell
slug:    svg-wallpaper-contest
---
The SVG wallpaper contest is over.  My favourite didn't win, I really liked <a href="http://www.kde-look.org/content/show.php?content=19418">KDE Celtic</a>.

<img class="showonplanet" src="http://muse.19inch.net/~jr/celtic-background.png" />

Some people have queried whether KDE's SVG icon engine is ready for such use, and actually it probably isn't.  It's too slow especially on intricate but beautiful designs such as Gear Flower.

<img class="showonplanet" src="http://static.kdenews.org/binner/svg-wallpaper-contest/3640954_1a948f4809_m.jpg" />

It also doesn't work with a lot of SVG files.  However KDE 4 should have a new SVG engine based on the little known libagg.  Together with KSVG2 that should make SVGs usable.

As for the icons we never got SVGs for CrystalSVG.  However the new monochrome theme is pure SVG and proves that it's perfectly useable.  Hopefully we can get SVG files for the new Crystal theme coming in the next few months.

<!--break-->
