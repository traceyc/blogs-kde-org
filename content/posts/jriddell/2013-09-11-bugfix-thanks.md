---
title:   "Bugfix thanks"
date:    2013-09-11
authors:
  - jriddell
slug:    bugfix-thanks
---
Application icons had stopped showing sometimes in the KDE SC 4.11.1 update but our lovely upstreams found a fix and when I was notified by a user I was happy to add it to the Kubuntu packages.  What a lovely response..

<pre>
From: Jens
To: Jonathan Riddell
Subject: Re: Sry new to this 
 
OMG! You are awesome! I could kiss you, you complete stranger! :D
</pre>
