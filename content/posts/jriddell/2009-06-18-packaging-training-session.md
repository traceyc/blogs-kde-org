---
title:   "Packaging Training Session"
date:    2009-06-18
authors:
  - jriddell
slug:    packaging-training-session
---
<a href="https://lists.ubuntu.com/archives/ubuntu-motu/2009-June/005861.html">Packaging Training Session</a> at 18:00UTC today in the #ubuntu-classroom IRC channel.

<i>"Our next Packaging Training Session is going to show Kubuntu a little
love. This Thursday (June 18) at 18:00 UTC, MOTU Jonathan Thomas
(JontheEchidna) will be leading a session on packaging KDE4 Apps and
Plasmoids. Find out what it takes to pitch in and join the Kubuntu
community. Learn the ways of the Kubuntu Ninjas!"</i>
<!--break-->
