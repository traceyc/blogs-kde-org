---
title:   "Night at the Museum with Linuxcon"
date:    2013-10-25
authors:
  - jriddell
slug:    night-museum-linuxcon
---
<a href="http://www.flickr.com/photos/jriddell/10472562616/" title="photo by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3758/10472562616_80df59e6c3.jpg" width="500" height="375" alt="photo"></a>
Linuxcon ended in spectacular style with a party at the National Museum of Scotland, shown here with the interesting addition of a full marching band.  Dolly the sheep was supposed to be on display but ran off at the sound of the bagpipes.

<a href="http://www.flickr.com/photos/jriddell/10472742553/" title="1 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7369/10472742553_df4c3e6d46.jpg" width="375" height="500" alt="1"></a>
Nim enjoyed her "nerd watching".
