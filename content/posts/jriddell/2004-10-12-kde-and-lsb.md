---
title:   "KDE and LSB"
date:    2004-10-12
authors:
  - jriddell
slug:    kde-and-lsb
---
Last night I watched a <a href="http://www.suug.ch/sucon/04/video.html">video of a talk from the Swiss Unix Users Conference</a> on the Linux Standards Base.

Their aim is to create a standard which binary programmes will just run on.  Instead of creating a dozen packages for each distribution and each version of those distributions you can create one package (a subset of RPM so it can be converted with alien to .deb) which will run on any LSB 1.3 certified distribution.  The way they are doing this is by specifying the filesystem hierarchy and the libraries.  This is a middle ground between the API only standard that is POSIX and the One True Operating System standard which Bruce Perens first tried to make LSB into (SuSE and Red Hat didn't want to be told to just use Debian, he's still trying with UserLinux) and which there was a faltering attempt made by the proprietary Unix vendors called OSF1.

That libraries bit includes the names of the libraries and the ABI, application binary interface.  They have had a lot of problems with, for example, the libstdc++ ABI which has changed with every release.  LSB 2.0 is ready to be released but the C++ ABI is going to be changed again.  This is a shame because it means there is no chance of being able to create standard packages for KDE programmes yet.

LSB does not include KDE or GNOME libraries because, says Theodore Tso, they are too unstable.  glib apparently was not long ago changing major version number every month or two.  As far as I know KDE libraries have been ABI compatible for over the two years he says would be required for it to be included in LSB.  

Which is interesting because... LSB 2.0 is going to be made modular.  Currently the only GUI libraries in the standard are the X libraries (not the X server though).  Since not everyone wants to include xlib and friends those will be put off into a module so embedded distributions can say they are LSB Core compliant without having to include libraries they have no use for.  

So now they're hoping to start having people turn up on their virtual door with library ABI standards.  They want experts in their field such as the KDE library developers to create an LSB standard for KDE.  Then independent software vendors can create KDE programs and not have to worry about which version of KGX to support.  Which would be nice.

As a post script, I don't know if the LSB requires licences to allow proprietary software development but it may be that Qt's GPL or pay us status (which, being a Free Software zealot, I entirely support) means KDE can never be an LSB standard.
<!--break-->