---
title:   "Come to the Kubuntu Bug Squashing Party in Munich!"
date:    2013-09-06
authors:
  - jriddell
slug:    come-kubuntu-bug-squashing-party-munich
---
There is a <a href="http://www.it-muenchen-blog.de/2013/09/debian-kubuntu-bug-squashing-party-2013/">Debian+Kubuntu Bug Squashing Party</a> in Munich and you're invited!  If you're wanting to help out with Kubuntu or Debian or KDE do come along and start squishing beasties.  It's hosted by our friends at LiMux who do the Munich city rollout of Kubuntu.  See the <a href="https://wiki.debian.org/BSP/2013/11/de/Munich">wiki page</a> for more details.  I'm going and hope to see you there.  Kubuntu Council should be able to sponsor travel for any Kubuntu friendly people who need travel and accomodation.

<img src="http://documentfoundation.files.wordpress.com/2012/12/munich-hackfest-2012.jpeg?w=400" width="400" height="248" />
Last year's meeting at LiMux with Libreoffice
