---
title:   "Kubuntu 6.10 - Taking You To The Edge"
date:    2006-10-26
authors:
  - jriddell
slug:    kubuntu-610-taking-you-edge
---
<a href="http://kubuntu.org/announcements/6.10-release.php">Kubuntu Edgy has been released</a>, download it now!  Thanks to nixternal for the pretty release announcement.

With this release our community continues to grow, with great power management work from Sebas and Lure.  Sime shuffled around System Settings for the usability review done by el.  

We also included Sime's patches for media which give /media much the same functionality as media:/.  These are still a bit experimental but that's what Edgy is all about.

We include accessibility profiles on the live CD, press F5 at start and it boots up with high contrast or sticky keys.  

There's a client for the <a href="http://hwdb.ubuntu.com">Ubuntu Hardware Database</a> to send back feedback on your hardware, although we're still waiting for the server side to be set up to get maximum use there.  It's also the first app in the archive to use PyQt 4.

Abattoir showed the Google Summer of Code can lead to greater things by finishing his port of the OEM-installer, so you can install Kubuntu, duplicate the image onto thousands of computers which you then sell and on first boot the users get a handy wizard setting up their account.  Abattoir also gets a star for validating a bunch of CDs during the final testing round.  As does Tonio who has done a top job refining the kubuntu-default-settings package and finding plenty other improvements to throw in, including an easy flash installer for Konqueror.  

As always the cream of the MOTU (Mez, Hobbsee, freeflying, fdoving...) have been working on packaging and triaging all the packages out there, but this time we've had more people with main upload rights too (imbrandon) and people working with the debian-kde-extras team (toma, allee, fabo).  And raphink has been busy on his own Kubuntu variant, Ichthux which is just one of many Kubuntu based distributions to have appeared.

The artwork by kwwii is universally considered lovely and bling.  He was the Artist in Chief for this release, which is a rotating position so we'll be looking out for great KDE artists to be the next Artist in Chief for Feisty.

And what else shall we do in Feisty?  Well currently upgrading from Dapper to Edgy is nasty, apt's dist-upgrade just doesn't work in too many cases.  So we need to port the Ubuntu upgrader tool to Kubuntu, which should be possible now that pyKDE is said to work with the embedded Konsole.  In a similar line we can port gdebi which installs individual .deb packages.  And we have the difficult issue of how to handle KDE 4, since it can't be installed in the same place as KDE 3, we will need to look at having it installed to /opt or similar, usually highly frowned upon by Debian distros.  And we'll have to start porting some apps to Qt 4 to prepare for KDE 4.  Fortunately we have a great team of KDE and Kubuntu developers going to the Ubuntu conference in California so it should be great fun for Feisty.

<a href="http://kubuntu.org/announcements/6.10-release.php"><img src="http://kubuntu.org/images/edgy-release.png" width="557" height="120" border="0" /></a>
<!--break-->