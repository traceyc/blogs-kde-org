---
title:   "Plasma Scripting IRC Session"
date:    2010-04-27
authors:
  - jriddell
slug:    plasma-scripting-irc-session
---
Aaron Seigo will be hosting a session on Plasma Desktop Scripting in #plasma on Freenode IRC network at 16:00UTC on Tuesday (today).  It's intended for distro packagers but all welcome.
