---
title:   "Thoughts on Umbrello 1.3"
date:    2004-01-20
authors:
  - jriddell
slug:    thoughts-umbrello-13
---
Dearest Umbrello and KDE Developers,

KDE has been branched for 3.2 and Umbrello UML Modeller 1.2 will soon be released with it.  My impression of Umbrello 1.2 is that it is a good release with features that were defiantly lacking in the previous version.  Developing as part of KDE has been a good thing, our release cycle has matched well and we get quite a bit of development, translation and packaging support as well as publicity with it although I hope it doesn't put off non-KDE users from using Umbrello.  Two commercial UML tool have bought Google Adwords for 'Umbrello' which affirms in my mind that we compete with the commercial competition (and in many ways surpass it).  We have gained a couple of top class developers in the last year and a few equally important beastie reporters.  My most proud achievement is getting Americans to use the word 'beastie' :)


Unfortunately 1.2 has far too many bugs in it, many of them crashes.  Cleaning up the unconfirmed beasties today it crashed several times.  Worse, they're hard to repeat and harder to track down type crashes (which is why I didn't report them).  Changes to associations and the code generators seem to be the main culprit.  I gather that the rules for KDE 3.2 branch are the same as the recent deep freeze: fixes to major beasties only and no i18n() changes.  So any fixes to major problems should be done in HEAD and then merged back into 3.2 carefully (cvs -j is rumoured to be good for this, ask if you have any problems).

Onto Umbrello 1.3.  Bartko pointed out the choices to me:

a) improvement of Umbrello 1.2
b) rewrite of Umbrello 1.2
c) merge with Umbrello2
d) something else?

I strongly suggest the first choice.  Umbrello 1.2 is good and maintainable so we should maintain it and improve it.  There's plenty to be improved and added.

Firstly if you have not tried Umbrello before please do so briefly when you install KDE 3.2 and comment on what you think is unintuitive.

One of the most significant non-crash problems is that the direction of new compositions changes.  This is probably a candidate for backporting into 3.2 http://bugs.kde.org/show_bug.cgi?id=72615

I hesitate to mention associations because I'm not honestly sure what the current status is with them (same with packages).  I suspect more work may be needed to clean them up, probably showing the associations of a class/whatever in the list view.  You should be able to see a list of all the associations when you look at the class properties, currently you can only see the associations on the current diagram.

We may also have to turn off fancy code generation by default, I haven't tried Umbrello with large diagrams recently but http://bugs.kde.org/show_bug.cgi?id=72644 [Large (Huge?) memory leak] indicates that it may be taking up more memory than is fair.

When you create an item on the canvas the dialogue should give you the option of using an existing class/interface/whatever and list the packages it could go in.

Code import for more languages features in bugs.kde.org but it may be best to tidy up the code generators we do have.  For example why does the C++ code generator not add a variable for a unidirectional association?

The refactoring agent probably needs some work to make it more user friendly.

Adding What's This help would be good.

I like the icons we have in the toolbar, they're clean, simple and useable.  The ones with English text in them should be changed though.  Icons for diagrams can't be too hard (significant widget for that diagram type, make them coloured, add a border to indicate canvas).

I lost track of the printing which never got the work we hoped for.  If we could get SVG export of course that would be excellent.

Class diagrams should allow Objects in them (apparently, says the UML spec).  This allows for object diagrams (which are just class diagrams without the classes).

Automatic tests as suggested in http://bugs.kde.org/show_bug.cgi?id=70256 would be useful.

Better error messages on a failed paste and file open are something I've wanted for a while.

The oft-requested KDevelop integration is probably a long way off.  It would be nice if done properly but is probably too much work.

QCanvas works well for us as a canvas but anti-aliasing and SVG export would be nice.  I suspect it's still the best going at the moment.

Standards compliance with XMI is happening now which is great.  Eventually we may need to look at UML 2 but probably not for this release.

Those are the issues on my mind today but there's plenty of other entries marked 'Umbrello' in bugs.kde.org and Seb reported that valgrind gave 30,000 errors when Umbrello was run over it so if anyone is looking for a way to begin a KDE programming career this is your opportunity!

Finally if anyone feels they would do a better job than me with the title of project admin do say so.  I've not been the most active developer in the last few months (having left university) but I do make an effort to ensure beastie reporters are thanked, all e-mail is replied to and the website is up to date.  Others do that as well though so maybe I should drop the title.

Thanks and guid nicht.
