---
title:   "last.fm"
date:    2004-11-16
authors:
  - jriddell
slug:    lastfm
---
<a href="http://www.last.fm">last.fm</a> offers personal streaming music stations based on your preferences and those of your neighbours.  It doesn't cost anything to join, although they request a donation.  You can add Friends etc making it similar to orkut, but good. I've set up a <a href="http://www.last.fm:80/group/KDE">KDE group</a> so join up and if we get 10 people we'll have our own KDE radio station.
