---
title:   "Linspire go Kubuntu; Talk at Akademy; Adept and KDE Debconf"
date:    2007-02-08
authors:
  - jriddell
slug:    linspire-go-kubuntu-talk-akademy-adept-and-kde-debconf
---
Linspire have announced that <a href="http://www.linspire.com/lindows_news_pressreleases_archives.php?id=213">future versions will be based on Ubuntu</a>.In their <a href="http://www.linspire.com/linspire_letter_archives.php?id=40">Canonical partnership FAQ</a> they say that they're sticking with their preferred desktop, KDE, which adds one more to the list of <a href="https://wiki.kubuntu.org/KubuntuDerivedDistros">Kubuntu Derived Distros</a>.

KDE World Summit <a href="http://akademy2007.kde.org/conference/cfp.php">Akademy 2007 put out a call for talks</a>.  If you're working on something interesting in KDE or cross desktop then we want to hear from you.  Don't think you're not good enough to do a talk, or that your project isn't interesting, it is.  Submit your talk online with a brief summary.

Adept had a long standing bug where it couldn't launch the graphical debconf bindings for KDE.  The errors it gave us led me to stare long and hard at kdesu's handling of xauthority permissions, but all seemed well until Tonio discovered that there wasn't a problem in adept_batch.  Eventually I spotted that while adept_batch ran as normal, Adept proper forks and backgrounds itself.  This means kdesu deletes the xauthority permissions file and Adept can't start any new X applications.  So we changed Adept from a KUniqueApplication to just a KApplication and now you can install Java packages without the user interface hanging.  Yay.

<a href="http://kubuntu.org/~jriddell/adept-debconf.png"><img src="http://kubuntu.org/~jriddell/adept-debconf-wee.png" width="300" height="212" /></a>
<!--break-->
