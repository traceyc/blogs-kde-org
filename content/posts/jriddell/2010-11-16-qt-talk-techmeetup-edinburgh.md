---
title:   "Qt Talk at Techmeetup Edinburgh"
date:    2010-11-16
authors:
  - jriddell
slug:    qt-talk-techmeetup-edinburgh
---
I was asked to give a talk at <a href="http://techmeetup.co.uk/">TechMeetup Edinburgh</a> about Qt.  Techmeetup is an occational event with free beer and pizza and talks.  I told them why they want to use Qt, how to write a basic programme in it then introduced the exiting world of Qt Quick.  <a href="http://techmeetup.co.uk/videos/">Video online now</a>.
