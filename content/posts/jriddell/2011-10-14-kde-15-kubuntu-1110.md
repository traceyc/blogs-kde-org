---
title:   "KDE is 15, Kubuntu is 11.10"
date:    2011-10-14
authors:
  - jriddell
slug:    kde-15-kubuntu-1110
---
<a href="http://www.kubuntu.org/news/11.10-release"><img src="http://www.kubuntu.org/files/11.10-release-banner2_0.png" width="605" height="169" /></a>

<a href="http://www.kubuntu.org/news/11.10-release">Kubuntu 11.10</a> is out and ready for download.  This is the first Kubuntu release were I haven't had much involvement in putting it together and I'm immensly proud of the team who put in so much work.

A word of warning to users, the upgrade to KMail 2 is not smooth, see our <a href="https://wiki.kubuntu.org/OneiricOcelot/Final/Kubuntu/Kmail2">KMail 2 upgrade instructions</a> and decide if you want to upgrade.

<img src="http://dot.kde.org/sites/dot.kde.org/files/kde-15-years7200.png" width="200" height="270" />

And today KDE turns 15.  An immense achievement for a community creating fun, useful software for consumers.  We're bigger and better than ever now encompasing more than desktops with a reach into mobile, tablets and cloud.

I'll give a word of warning here too.  I hear quite often at KDE confrences that Ettrich's original goal of a complete consistent desktop was achieved long ago.  This isn't the case, we still need a world class web browser, office suite and well see the warning about e-mail above.  So plenty more work to be done before we get world domination.

Time for me to wind down Bazaar and move back to my first love, KDE and Kubuntu.
<!--break-->
