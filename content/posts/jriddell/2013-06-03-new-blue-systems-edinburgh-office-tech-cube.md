---
title:   "New Blue Systems Edinburgh Office at Tech Cube"
date:    2013-06-03
authors:
  - jriddell
slug:    new-blue-systems-edinburgh-office-tech-cube
---
After many years of working away from my bedroom desk I was invited to a party at the opening of a new hotdesking space.  <a href="http://www.techcu.be/">Tech Cube</a> is an attempt to create an entrepreneurial office space in Edinburgh.  As I heard it the old Royal Dick (no sniggering at the back) vet school got sold to a nice guy with deep pockets who is turning it into an arts complex but the ugly wee tower block stuck on the side was not very artistic so get turned into this little project.

I went to the opening party last Friday and enjoyed the beer from the on site brewery ("some offices have a beer fridge, Tech Cube has its own brewery") then signed up this morning.  I'm one of the first so it's quite an empty room for now but it'll be interesting to see how often I want to commute (across the medows) and work from a real desk in a comfy chair.
 
<a href="http://www.flickr.com/photos/jriddell/8935835764/" title="DSCF7787 by Jonathan Riddell, on Flickr"><img src="https://farm3.staticflickr.com/2848/8935835764_190cc4aab0.jpg" width="500" height="375" alt="DSCF7787"></a>
Opening Party

<a href="http://www.flickr.com/photos/jriddell/8935836936/" title="DSCF7792 by Jonathan Riddell, on Flickr"><img src="https://farm4.staticflickr.com/3698/8935836936_9e7aea0977.jpg" width="500" height="375" alt="DSCF7792"></a>
Seems I'm one of the first to take it up
