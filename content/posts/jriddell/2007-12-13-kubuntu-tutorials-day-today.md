---
title:   "Kubuntu Tutorials Day Today"
date:    2007-12-13
authors:
  - jriddell
slug:    kubuntu-tutorials-day-today
---
Good Morning Friends, do join us in a few hours for these fine IRC tutorials.

<a href="https://wiki.kubuntu.org/KubuntuTutorialsDay"><img src="http://kubuntu.org/~jriddell/kubuntu-tutorials-day-2.png" width="557" height="150" alt="kubuntu-tutorials-day-2" border="0" /></a>

And congratulations to <a href="http://dot.kde.org/1197535003/">Trolltech for including parts of Qt in KDE's svn</a> for the first time.  Even closer cooperation with our favourite framework authors is worth getting up early in the morning to publish the story for.