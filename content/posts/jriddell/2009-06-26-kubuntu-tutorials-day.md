---
title:   "Kubuntu Tutorials Day"
date:    2009-06-26
authors:
  - jriddell
slug:    kubuntu-tutorials-day
---
<img src="http://people.ubuntu.com/~jriddell/kubuntu-tutorials-day-2009.png" width="472" height="148" />

Time for another <a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">Kubuntu Tutorials Day</a>.  Learn about KDE and Kubuntu development in a helpful atmosphere next Monday from 19:00UTC in the #kubuntu-devel IRC freenode channel.  We have a lineup of exciting speakers on a range of topics.  The timetable looks like this:

19:00UTC <b>The next six months with Kubuntu</b> with Roderick Greening 
20:00UTC <b>Getting into Ruby</b> with Harald Sitter 
21:00UTC <b>Packaging and Merging with the Ninjas</b> with Jonathan Riddell 
22:00UTC <b>Artwork The composition of an icon</b> with Ken Wimer
23:00UTC <b>Amarok scripting</b> with Sven Krohlas
end of talks onwards, <b>Kubuntu Q & A Ask us anything you want to know</b>
<!--break-->
