---
title:   "Billie Piper Endorses Kubuntu"
date:    2006-07-25
authors:
  - jriddell
slug:    billie-piper-endorses-kubuntu
---
LUGRadio live happened at the weekend and we had a good number of KDE and Kubuntu people there.  Full report coming later but the highlight of the show was when Billy Piper from Dr Who stopped by the stand to say how much she liked her free Kubuntu CD.

<img src="http://static.flickr.com/58/197548353_11694bc4d0.jpg?v=0" width="375" height="500" class="showonplanet" />

<!--break-->
