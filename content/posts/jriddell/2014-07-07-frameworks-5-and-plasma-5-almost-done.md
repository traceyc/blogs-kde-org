---
title:   "Frameworks 5 and Plasma 5 almost done!"
date:    2014-07-07
authors:
  - jriddell
slug:    frameworks-5-and-plasma-5-almost-done
---
KDE Frameworks 5 is due out today, the most exciting clean-up of libraries KDE has seen in years.  Use KDE classes without brining in the rest of kdelibs.  <a href="http://qa.kubuntu.co.uk/kf5-status/build_status_5.0.0_utopic.html">Packaging for Kubuntu</a> is almost all green and Rohan should be uploading it to Utopic this week.

<a href="http://qa.kubuntu.co.uk/plasma-status/build_status_4.98.0_utopic.html">Plasma 5 packages</a> are being made now.  We're always looking for people to help out with packaging, if you want to be part of making your distro do join us in #kubuntu-devel
