---
title:   "Kubuntu Photo at Munich BSP"
date:    2013-11-23
authors:
  - jriddell
slug:    kubuntu-photo-munich-bsp
---
We're having a great meeting here in Munich, last night we collected a range of topics to chat about and today we've been moving post-it notes about as we discuss topics and fix problems.  I'm enjoying it a lot.

<a href="http://www.flickr.com/photos/jriddell/11013868524/" title="DSCF8747 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7351/11013868524_53569bdbfd.jpg" width="500" height="375" alt="DSCF8747"></a>
A 3D trello board.

<a href="http://www.flickr.com/photos/jriddell/11013799004/" title="DSCF8748 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5480/11013799004_1de41e223d.jpg" width="500" height="375" alt="DSCF8748"></a>
Elite Kubuntu developers and friends.  Martin, Manuel, Jussi, Elodi, Sari, Philip, Ovidiu, Felix, Harald\n Jonathan, Achim, Tux, Rohan.

Now we're ready for #savetheday, will the war doctor beat the baddies?
