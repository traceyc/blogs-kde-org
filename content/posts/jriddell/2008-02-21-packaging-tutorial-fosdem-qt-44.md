---
title:   "Packaging Tutorial at FOSDEM.  Qt 4.4."
date:    2008-02-21
authors:
  - jriddell
slug:    packaging-tutorial-fosdem-qt-44
---
<img src="http://kubuntu.org/~jriddell/packaging-jam.png" width="361" height="65" alt="Packaging Jam!" />

It's FOSDEM this weekend, a huge gathering of free software enthusiasts all in one place with dozens of talk tracks.  

I'm giving a packaging tutorial on the Sunday at 16:00 in the <a href="http://www.fosdem.org/2008/schedule/devroom/crossdesktop">cross desktop devroom</a> (note different time than advertised).

If you make software and want to get it into Ubuntu (or Debian) come along and learn how to make .debs and get them out for the world.  Bring your laptop to follow the tutorial yourself.

<hr />

I've put packages of Qt 4.4 in <a href="http://launchpad.net/~jr/+archive">my PPA</a> for those working with KDE 4.1.  They have a binary incompatible change compared to current Qt 4.3 so it'll break existing plasma and other bits.
<!--break-->



