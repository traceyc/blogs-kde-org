---
title:   "Next Generation Edges Closer"
date:    2014-05-14
authors:
  - jriddell
slug:    next-generation-edges-closer
---
Today I released the <a href="http://kde.org/announcements/announce-plasma-next-beta1.php">Plasma Next Beta</a>.

<a href="http://kde.org/announcements/plasma-next-beta/ss-wallies.png"><img src="http://kde.org/announcements/plasma-next-beta/ss-wallies-wee.png" width="400" height="250" /></a>

It's the first major user of KDE Frameworks 5 and tidies up the internal and the externals of the Plasma desktop.

At the Kubuntu meeting yesterday we decided not to ship Plasma Next by default in October but to make a secondary ISO for those who want to test it out while putting the Plasma 1 version into maintenance mode - updating the version and fixing the major bugs and not much more.

It's going to be an exciting release.  But not so exciting it'll implode :)

To get a sneak preview grab the <a href="http://files.kde.org/snapshots/">Neon5 ISO</a> which Rohan updated yesterday especially for this beta.
