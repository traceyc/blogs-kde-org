---
title:   "Slick New Artwork and call for testers"
date:    2013-04-18
authors:
  - jriddell
slug:    slick-new-artwork-and-call-testers
---
Some slick new artwork has arrived in Kubuntu 13.04. 13.04 is due for release a week today so all hands on deck for testing, join us in #kubuntu-devel to help out.

<a href="https://launchpadlibrarian.net/136714642/plymouth-logo.png">
<img src="http://people.ubuntu.com/~jr/plymouth-logo.png" width="350" height="263" />
</a>
Plymouth boot screen

<a href="http://people.ubuntu.com/~jr/ubiquity.png">
<img src="http://people.ubuntu.com/~jr/ubiquity-wee.png" width="300" height="266" />
</a>
Ubiquity installer

Hugs to agateau, sheytan and apachelogger for the artwork polishing.
