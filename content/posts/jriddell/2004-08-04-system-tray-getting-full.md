---
title:   "System Tray Getting Full"
date:    2004-08-04
authors:
  - jriddell
slug:    system-tray-getting-full
---
A quick rant: I don't like the system tray getting full for no good reason.  My system tray is getting almost as bad as the ones on a Windaes desktop where device drivers put their company's logos just because they can.  Why do an increasing number of KDE apps insist on not quitting when I those their windows and instead stay around in the system tray.  When I close the main window of an application I expect it to quit, this isn't MacOS.  But Noatun, KGet, Akregator, KSCD, Kaffeine and no doubt others just hang around for no good reason.  I'll let KMix off because it can actually be useful.  JuK shouldn't be needed either since everyone should use the mediacontrol applet which is wonderful.  

That was a rare rant from me, hope you enjoyed it.
<!--break-->