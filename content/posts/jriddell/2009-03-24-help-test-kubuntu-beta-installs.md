---
title:   "Help Test Kubuntu Beta Installs"
date:    2009-03-24
authors:
  - jriddell
slug:    help-test-kubuntu-beta-installs
---
Kubuntu Jaunty Beta is in two days time and we need as many people as possible to help test installs.

You can get the candidate CD and DVD images from <a href="http://cdimage.ubuntu.com/kubuntu/">the cdimage server</a> .  We need tests of live and alternate installer and Wubi the Windows installer.

We also need tests for upgrades <a href="https://help.ubuntu.com/community/JauntyUpgrades/Kubuntu">from 8.10</a> and <a href="https://help.ubuntu.com/community/JauntyUpgrades/Kubuntu/8.04">from 8.04</a>.

Join us on #kubuntu-devel to chat and report on <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">the iso testing site</a>.
<!--break-->
