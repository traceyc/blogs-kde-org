---
title:   "libxml, FOSDEM"
date:    2010-01-03
authors:
  - jriddell
slug:    libxml-fosdem
---
libxml2 is compiled without thread support in Ubuntu 9.10 due to an oversight from upstream.  This causes issues with Strigi which recently added a check for it.  You can get <a href="https://edge.launchpad.net/~jr/+archive/ppa">packages from my PPA</a> with thread support.  It should go into -proposed and -updates soon too.

FOSDEM is happening next month.  I can't go :( but you should, it's the best cross project free software gathering there is.  If you're going put yourself <a href="http://community.kde.org/Promo/Events/FOSDEM/2010">on the wiki page</a> and then think about doing a talk, if you are involved in KDE we want to hear from you.  Add any talk ideas you have to that wiki page.
<!--break-->
