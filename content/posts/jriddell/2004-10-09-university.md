---
title:   "University"
date:    2004-10-09
authors:
  - jriddell
slug:    university
---
I went to University and found the experience really useful even if most of what I learned was outside the lectures.  The first semester was HTML, and out of date HTML at that.  But after that we started with Java programming which gave me the theoretical knowledge to be able to pick up a hefty C++ book and learn that quickly enough.  University might not teach you the direct skills you want but instead should give you the theory to be able to learn whichever branch of computing you find interesting, as well as the resources (mmm, big libraries and fast internet) to be able to do your own learning.  It does mean however that you have to be prepared to work self motivated, most people on my course weren't which is why out of a year of 50 there's only 1 I can think of who is a better programmer than me.  (And I'm not a great programmer, out in the real world there are suddenly many much better programmers, I'm always in awe at most of the KDE contributers.)

Free Software and universities go well together.  Fixing or writing your own Free Software project is the best way to learn how to program.  Doing a Free Software project for your dissertation will almost guarantee you an A and you end up with some software that people will really use long after the dissertations from your peers have disappeared.  I came runner-up in an award for Umbrello which would have made my dissertation 3rd best in the country.

However someone like Chris already knows how to program so it's debateable whether he needs to spend 4 years being told theory he already knows.  Lets consider it as 4 years he can spend working on KDE then.
<!--break-->
