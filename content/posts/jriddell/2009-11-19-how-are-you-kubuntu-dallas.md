---
title:   "How are you?  Kubuntu in Dallas"
date:    2009-11-19
authors:
  - jriddell
slug:    how-are-you-kubuntu-dallas
---
The Ubuntu Developer Summit is happening this week in Dallas.  The theme of the discussions is LTS and what it will take to have a release in six months which can be supported for three years hence.  We've been having sessions on packaging, development, bugs policy, translations and more.  You can find the schedule and how to take part in sessions on <a href="http://summit.ubuntu.com/uds-l">the summit website</a>, there are icecast streams for all the rooms.  The Kubuntu specs are <a href="https://wiki.kubuntu.org/KubuntuLucidSpecs">on this wiki page</a> still works in progress of course.  It's going to be great to have a KDE 4 release suitable for LTS, just six months to do it!

<img src="http://people.canonical.com/~jriddell/kubuntu-on-ice.jpg" width="400" height="300" />
Some of the Kubuntu Team take to the ice rink
<!--break-->
