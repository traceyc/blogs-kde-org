---
title:   "Kubuntu RC Candidates Need Testing"
date:    2010-04-19
authors:
  - jriddell
slug:    kubuntu-rc-candidates-need-testing
---
Kubuntu 10.04 Release is nearly upon us.  This Thursday sees the Release Candidate with final release due a week on Thursday.  

So we need ISO testing (and upgrade testing) pretty solidly between now and then.

The <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">ISO testing Kubuntu page</a> lists what needs testing.  Join us in #kubuntu-devel if you can help.

<hr />

And testers will get a sneak preview of our all new logo, made after much pondering and brainstorming and re-learning how to use Inkscape.
<img src="https://wiki.kubuntu.org/KubuntuArtwork?action=AttachFile&do=get&target=kubuntu-logo-lucid.png" width="400" height="78" />
<!--break-->
