---
title:   "Life in the Gran Canaria Penthouse"
date:    2009-07-05
authors:
  - jriddell
slug:    life-gran-canaria-penthouse
---
KDE's conference opened today after the Canonical sponsored party last night.  The location is superb, the auditorium is massive and right next to the beach.  The people as always are the highlight with more than twice as many as usual.  The hotel is nice too with our rooftop penthouse suite.

Today started with a phone call from Claudia asking me to wake Kenny up.  Neither of the Kennys would wake up so I had to run to the auditorium and work out how to get video cameras recording.  Some running back and forth and I got it mostly working in time for RMS to moan about C#.  Maemo announced the move to Qt, nice choice Nokia.  <a href="http://www.open-pc.com">Open PC</a> wins the prize for most ambitious lightning talk topic.  

<img src="http://farm3.static.flickr.com/2476/3687746691_95b8f8b202.jpg?v=0" width="500" height="375" />
The KDE penthouse, catering to vegans and celiacs alike.

<img src="http://farm3.static.flickr.com/2660/3688194152_6eb629d3df.jpg?v=0" width="500" height="375" />
I wasn't allowed to put this in the Dot article.  Note the neat coordination of Qt towel green, blue and white.
<!--break-->
