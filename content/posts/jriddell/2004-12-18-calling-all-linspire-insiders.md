---
title:   "Calling all Linspire insiders"
date:    2004-12-18
authors:
  - jriddell
slug:    calling-all-linspire-insiders
---
Linspire 5 will come with a new version of the Crystal icon set.  So far nobody knows what this looks like but the icons are part of the Linspire 5 beta which is available to Linspire Insiders (CNR Warehouse subscribers).  Naturally we'd like to see what it looks like so if you are a Linspire insider please take a screenshot (preferably of Konqueror pointing to /usr/share/icons/crystalsvg/64x64/apps or similar), put the screenshot on a webserver and add the URL in a comment to this blog.  Or you can send it to me http://jriddell.org/contact.html



