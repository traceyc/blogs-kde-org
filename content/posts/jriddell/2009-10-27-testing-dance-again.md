---
title:   "Testing Dance Again"
date:    2009-10-27
authors:
  - jriddell
slug:    testing-dance-again
---
Once again we need testers, this time for the candidate images for Kubuntu 9.10.  CDs, DVDs, netboot and upgrades all need going through their paces in triplicate.  Join us in #kubuntu-devel to coordinate and log your results on the <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">ISO testing site</a>.
<img src="http://people.canonical.com/~jriddell/karmic-countdown-banner/2.png" width="849" height="168" />
<!--break-->
