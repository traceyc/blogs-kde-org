---
title:   "Why Kubuntu is Good for KDE"
date:    2008-02-03
authors:
  - jriddell
slug:    why-kubuntu-good-kde
---
Tsk to marketing types who bring negatives tones to otherwise peaceful Planets.  I like to walk cheerfully over the world so here's some top reasons why Kubuntu is good for KDE.

<ul>
<li>We'll be the first (out of the release schedules I can find) to ship KDE 4 on a distro by default.  Of course 4.0 is only for a select audience, so there will be another CD with KDE 3.</li>
<li>We are a pure KDE distro, it's the original and best free desktop and we want to promote it.  With some other distros when you install KDE you find yourself using, say, a non-KDE browser.  That has advantages and disadvantages, but ultimately I think we can create the best user experience by sticking to one desktop.  Ark Linux is the only other distro I know to do this for KDE.  Ubuntu Desktop takes the same attitude towards Gnome for the same reasons, it's why they didn't have displayconfig for years after we did and why they don't ship those nifty HP printing tools.</li>
<li>We use configuration tools from within KDE.  Some other distros make configuration tools such that they can only work with that one distro.  I think KDE wins if we use tools from within KDE, which is why we use Guidance.  Yet other distros use GTK configuration tools and seem to be none too enthusiastic for cooperation when I port them to Qt, oh well at least I can maintain it within KDE.</li>
<li>Sometimes we get criticised for not passing patches upstream, I'm not sure where this comes from, I'd really like to see examples of where that hasn't happened but should have.  Usually when I notice something needs a fix in KDE, I fix it directly because I am a KDE developer.  Some other distros have to be poked into actually interacting with KDE (as a Dot editor I've done some of this poking).</li>
<li>Kubuntu sponsors Akademy.  It takes tens of thousands of pounds to put on KDE's most important summer get-together.  We sponsored Akademy when even Trolltech didn't want to help make that happen, without that sponsorship it would be a lesser event.</li>
<li>We are marketed as our own product.  It's not just an extra spin for those who like that sort of thing.</li>
<li>We develop for upstream.  I got System Settings to replace KControl (a UI I've hated since KDE 1), worked with Amarok to get the first automatic codec installer working  (why do people think Ubuntu Desktop got that first?).  I'd like to see desktop-effects going into KDE SVN for those who want a compiz setup tool.  Oh and I spent a long time updating the licence policy when nobody seemed to care (even some non-KDE Debian packagers!) that KDE could become illegal to distribute (big shout out to toma and dirk on the related issue of relicensing GPL 2 only code).</li>
<li>We invite KDE developers to our conferences to plan out how we can work better getting KDE to the masses.  Want an all expenses paid trip next May to help us do this?  Let me know.</li>
<li>We use PyKDE.  The world's computer developer pool is expanding but that includes the VB programmers of this world who will never learn C++.  We are the only distro to actively promote this easy and powerful way to get into KDE development.</li>
<li>We work to being in contributors.  We were a more open project when other distros made it hard or impossible to contribute packages (still are more open in some respects, Kubuntu is one of the few large projects I know that doesn't have any hidden elitist IRC channels, yes Linus I'm talking to you baby).  Now we <a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">run introductory sessions</a> to help people get involved, watch out for more of these with Dev Day and at FOSDEM.</li>
<li>Finally and most importantly we get KDE used.  In France (Assembly), Canary Islands (schools, universities), Venezuela (selling machines), Phillipines (schools again) and Georgia (every single school in the country baby!) we get your software into the hands of non-geeks.  Then there's our derived distributions, Linspire, MEPIS (who went back to my other favourite distro Debian for no good reason anyone can see but guess who's KDE 4 packages get used without supplying the source code?) and selling embedded multimedia machines with Linux MCE (this project so rocks) all based on Kubuntu.  <a href="https://wiki.kubuntu.org/KubuntuDerivedDistros">Many more too</a>.</li>
</ul>

There are other good things coming out of Canonical which can benefit KDE.  Top of the list being commercial support for Kubuntu (and if you want there to be more paid Kubuntu developers, go and convince your company/uni/government etc to buy some of this).  Bzr is the best distributed revision control system and I hope ever so much they can convince KDE to look at using it, because if we ever switch to Git we'll find ourselves in trouble when half our account holders can't work out how to use it.  Oh and the 6 monthly release schedule we've switching to, great stuff but why are people embarrassed about where that idea came from?

Walk cheerfully Friends.
<!--break-->
