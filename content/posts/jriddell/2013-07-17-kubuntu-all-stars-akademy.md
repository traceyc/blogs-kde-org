---
title:   "Kubuntu All Stars @ Akademy"
date:    2013-07-17
authors:
  - jriddell
slug:    kubuntu-all-stars-akademy
---
A quiet day for me at Akademy catching up on e-mail and learning how to make an apt archive so here's some more photos from the rocking party last night. 

<a href="https://plus.google.com/101026761070865237619/posts">More photos from Martin "Ross Halfin" Klapetek</a>

<a href="http://www.flickr.com/photos/jriddell/9304304730/" title="IMG 7017 v1 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2894/9304304730_3f67451659.jpg" width="500" height="333" alt="IMG 7017 v1"></a>
The Kubuntu All Stars rocks Akademy

<a href="http://www.flickr.com/photos/jriddell/9304305114/" title="IMG 7020 v1 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3738/9304305114_a308ee8a3b.jpg" width="333" height="500" alt="IMG 7020 v1"></a>
Rohan "Jagger" Garg

<a href="http://www.flickr.com/photos/jriddell/9301523917/" title="IMG 7022 v1 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3685/9301523917_0609418b8f.jpg" width="500" height="333" alt="IMG 7022 v1"></a>
Valorie "You can be my Yoko-Ono" Zimmerman

<a href="http://www.flickr.com/photos/jriddell/9301524177/" title="IMG 7023 v1 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7330/9301524177_116df2f66c.jpg" width="500" height="333" alt="IMG 7023 v1"></a>
Philip "Ludovico Einaudi" Muskovac

<a href="http://www.flickr.com/photos/jriddell/9304305994/" title="IMG 7025 v1 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3720/9304305994_b6aa903305.jpg" width="500" height="333" alt="IMG 7025 v1"></a>
Jonathan "Ringo" Riddell Star
