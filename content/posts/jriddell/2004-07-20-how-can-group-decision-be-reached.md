---
title:   "How can a group decision be reached?"
date:    2004-07-20
authors:
  - jriddell
slug:    how-can-group-decision-be-reached
---
<p>KDE has had the same splash screen for the last two releases so I think it is in need of a new one.  It doesn't matter if the new one is better or worse than the KDE 3.1/3.2 splash (it's all artistic taste), this is new for the sake of new otherwise it looks to users like nothing has changed.</p>
<p>
There are a few possibles:</p>
<ul>
<li>http://www.os11.com/splash.asp was recently submitted and looks nice but has been accused of using Apple style imagary.</li>

<li>Everaldo has said he likes this one http://muse.19inch.net/~jr/tmp/kde33/kde33-3.JPG </li>

<li>Quite a few people like this one http://muse.19inch.net/~jr/tmp/kde33/sparkling-kde33-2.png</li>
</ul>

<p>And quite a few people say that none of them are good enough (the criteria is that they have to fit in with KDE's default widget, icon and desktop themes).</p>

<p>
My question is not which of these is best but rather how can a group decision be made when most people have an opinion but essentially it comes down to artistic preference?
</p>