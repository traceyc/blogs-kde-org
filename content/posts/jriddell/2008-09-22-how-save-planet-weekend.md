---
title:   "How to save the planet in a weekend"
date:    2008-09-22
authors:
  - jriddell
slug:    how-save-planet-weekend
---
This morning KDE is waking up to <a href="http://planet.kde.org">a shiny new Planet</a> blog aggregator.  I'm not sure what caused this, but there are reports the old host didn't want the CPU load on their server any more, many thanks to them for having put up with it for many years.  Fortunately I'd been working on a new Planet started by Chris Lee using <a href="http://offog.org/code/rawdog.html">rawdog</a> and my own <a href="https://launchpad.net/~jr/rawdog-rss/trunk">Rawdog RSS plugin</a>.  The design comes from the exceedingly cool Oxygen dudes, Nuno and Ruphy.  

Interestingly the first comments from a lot of people are about it using Launchpad as a tracker for feed requests.  I don't know why people have issues with Launchpad when most KDE developers have used Sourceforge, Google code or for that matter the no-code-published old Planet KDE without moaning. If someone can tell me how to use bugs.kde.org without going through a long wizard asking for which compiler is used I'll happily change but currently Launchpad is the simplest and best tool for the job.  You can also just poke me on IRC still, or <a href="http://websvn.kde.org/trunk/www/sites/planet/">the code is in SVN</a> so you can send patches (especially welcome if you know how to get the feeds list sorted).

Happy new Planet.

<b>Updates</b> Aurelien showed me how to skip the bugs wizard so I've changed to bugs.kde.org for feed requests now.  

I fixed a few things including the alphabetical order of the feeds, thanks to pinotree for the push there.

Best of all super-sysadmin Dirk opened the access control so now anyone can edit <a href="http://websvn.kde.org/trunk/www/sites/planet/">the setup in SVN</a>.  We are shockingly short of hackergotchis, time to add yours!
<!--break-->
