---
title:   "Icons, Logo, Amarok"
date:    2005-05-20
authors:
  - jriddell
slug:    icons-logo-amarok
---
This week I've moved all the KDE application icons from crystal to hicolour.  Hicolour is no longer the KDE 2 icon theme, that was renamed to kdeclassic.  These days hicolour is the fallback icon theme used by all desktops that follow the freedesktop.org standard.  KDE knows to always look in crystal because it's the default theme for KDE but over at Gnome they have a different default theme and couldn't find the KDE icons needed for the application menu.  So remember always put your application's icon in hicolour if you want non-KDE users to be able to see it.  (Actually applications outside KDE's CVS should probably put all their icons in hicolour incase KDE moves to a different default icon theme.)

KDE has a new logo, the blue square one.  That means you should update your websites to use it and the favicon.  This means you kde.nl and you kdevelopers.org.  

This week I have also been trying Amarok.  What I want in a music application is a long list of my tunes which is easily searchable.  I don't need playlists or other fancy features.  Juk gives me that but Amarok when it starts up and you get past the wizard doesn't.  How do I find my music?  There's nothing there damnit.  The trick is to click on the Playlists icon at the side then double click on All Collection from the Smart Playlists.  If Amarok did this by default I could get rid of Juk.  It's also missing (or at least I havn't found it) a way to jump to see the currently playing tune in the playlist.  Also having Windows keys defined as the shortcuts is annoying for those of us without Windows keys.  
<!--break-->
