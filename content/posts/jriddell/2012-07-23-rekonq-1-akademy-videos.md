---
title:   "Rekonq 1!  Akademy Videos"
date:    2012-07-23
authors:
  - jriddell
slug:    rekonq-1-akademy-videos
---
<img src="http://starsky.19inch.net/~jr/rekonq.png" width="128" height="128" />
Rekonq, the web browser shipped with Kubuntu, has <a href="http://adjamblog.wordpress.com/2012/07/20/rekonq-1-0/">reached 1.0</a> congratulations Adjam and friends.  It's pleasingly stable and thanks to WebKit it supports almost all the modern fancy stuff in websites.  Just a pain websites like BBC iPlayer deliberately fail on it, bad BBC.
Rekonq 1.0 is available in Quantal for latest Ubuntu users and precise-backports for those wanting to try it out.

<hr />
<img src="http://dot.kde.org/sites/dot.kde.org/files/AkademyLogo225px.png" width="225" height="170" />
<a href="http://akademy2012.kde.org/program">Akademy talk videos</a> are up.  Even if you're not into domain specific debugging tools for Qt it's well worth looking at the Keynotes of <a href="http://dot.kde.org/2012/04/22/akademy-keynote-dr-mathias-klang-freedom-expression">Mathias Klang, Freedom of Expression</a> and <a href="http://dot.kde.org/2012/04/11/akademy-keynote-dr-will-schroeder-kitware-ceo">Will Schroeder: It has a heart, a brain and it's dangerous</a>.  Be selfish and share!
<!--break-->
