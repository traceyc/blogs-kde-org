---
title:   "Do you want Catalonia to be a State? If so, do you want Catalonia to be an independent State?"
date:    2014-11-03
authors:
  - jriddell
slug:    do-you-want-catalonia-be-state-if-so-do-you-want-catalonia-be-independent-state
---
<img src="http://img02.lavanguardia.com/2014/09/11/Imagenes-aereas-de-la-gigantes_54414928868_53699622600_601_341.jpg" width="601" height="341" />

Today there are only six days left until I vote in the independence referendum.   I usually use my <a href="http://jriddell.org/diary/">personal blog</a> for non technical bits but I thought some readers of my KDE Blog might be interested in this as it does affect the geopolitics of pretty much the whole world.

<b>What's going on?</b>

Three hundred years ago Catalunya was an independent country trading its way through the world under the monarchs of Aragon with the capital in Barcelona.  Then they ran out of kings and picked the Austrians to supply the next monarch.  But they didn't even have time to check the map and realise Austria was nowhere near before the Spanish invaded and took over.  Since then monarchs were disposed, dictatorships came and went, the Catalan language was suppressed (sometimes more sometimes less), the Catalan government was kicked out, Catalunya grew an impressive amount of industry and the rest of Spain based much of their economy on tourism and constructing blocks of flats for the tourists. Then 2008 happened and Spain suffered The Crisis when the construction based economy stopped overnight and the country had to be bailed out by the Germans.

<b>Hang on, I've read this somewhere before</b>

Regular readers may recognise some similarities to <a href="https://blogs.kde.org/2014/09/02/should-scotland-be-independent-country">a blog post I wrote a couple of months ago</a>.  There's a lot which is the same between Scotland and Catalunya but also a lot which is different.

<b>What happened in Scotland?</b>

In Scotland the polls looked like it could well go for Yes.  The London politicians panicked and made a front page article with The Vow on it which said they'd give Scotland lots of new political powers, keep the funding formula for Scotland which is generally beneficial and please please don't leave.  A daily stream of scare stories came out: the oil was about to run out, the supermarkets would raise prices, the banks would leave and you're too small and too wee.  The day after the referendum with 45% voting for yes and 55% for no and relieved David Cameron mopped the sweat from his brow and announced he'd be taking away the power of Scottish MPs to vote on English matters, not something anyone had mentioned before and something which damages Scotland because all the English votes affect the Scottish budget, plus it just makes the UK government setup even more weird because you can end up with a government who don't have a majority on many issues.

There are UK elections next year and there is a real danger that the current right wing Conservative party will get back in and call a referendum on EU membership causing Scotland to leave the EU against its will and the economy to be largely destroyed.

<b>So what's going on in Catalunya</b>

Catalunya (English: Catalonia, Spanish: Cataluña) is in the north east of Spain and depending on who you ask is either a nation or a region.  It has its own language, Catalan, which sounds a bit like a faster Spanish with some French thrown in.  After the 2008 financial crisis in Spain there is now 25% unemployment (50% for the under 30s) and life is hard.  There has grown a very strong and rapid growth in the movement for political independence for Catalunya since then.  Unlike in Scotland this isn't led by the politicians but by hundreds of thousands of people on the streets, see the photo above with the people in Barcelona making a Catalan flag all through the middle of Barcelona.  The Catalan politicians have caught up and have called a referendum on independence with two questions "Do you want Catalonia to be a State? If so, do you want Catalonia to be an independent State?".

Except the Madrid government took the Catalan government to court and had it declared illegal because it might threaten the existence of the Spanish realm.  So the Catalan government changed it into a non-binding consultation.  Which Madrid then had declared illegal too.  The Madrid controlled riot police started to gather outside Barcelona.  So now the Catalan government is having a voluntary poll using no civil servants but instead a large team of volunteers to help out.  This may also be illegal but it seems too late to stop it and who knows what all those riot police are doing there, it may get interesting.

<b>Why would you vote Si?</b>

The way the debate ended up in Scotland the most important reason to vote yes was for a government which reflected the opinions of people in Scotland.  There are different political wishes in Scotland on topics like university tuition fees, private running of the health service, nuclear weapons and invading countries at random.  The second most important reason was the money, there were endless debates about whether Scotland was richer or poorer than the rest of the UK and how much of a difference the oil made with not much agreement by the end.  The least important reason was national identity, it's nice to wear a kilt and drink irn bru but it's not necessarily a good reason to define your political borders.

In Catalunya the reasons seem to be reversed in priority.  

The most important reason when I ask people is always national identity.  They want to speak their own language, make large towers of people and not have any bull fights.  They genuinely have suffered repression under years of dictatorship with Franco when they were forbidden from speaking Catalan.  This only ended in the mid-70s, since then there has been a pact of forgetting.  While Spanish investigators have helped various South American countries find the graves of their disappeared and prosecute people who took part in war crimes as part of a dictatorship here in Spain they are only just beginning to dig up graves of those killed by Francoistas and even then with massive resistance from the right wing political parties.  Nobody has ever been prosecuted for crimes under the dictatorship and records in Madrid are kept firmly locked down.  A TV documentary looking into mass graves was shown on Catalan TV but banned from Spanish TV.  There is still a giant mausoleum near Madrid where Franco's body lies and each year hundreds of fascist supporters go to salute it.  Who's want to be part of a country like that?  But on the other hand the Catalan government has plenty of freedom now to speak whatever language they like and everyone here is bi-lingual so it's unclear what problem gets solved.

There's a lot more certainty that Catalan is the richest part of Spain and they contribute a lot more to Madrid than they get back in taxes.  Like in Scotland the Catalan government has minimal powers to raise taxes, everything goes to Madrid who then decide what to give back.  This level of money leaving Catalunya through taxes does grate especially since the crisis and the people here feel they are subsidising all the Andaluthians who only work three months a year and claim benefits for the rest.  All the infrastructure like motorways and high-speed railways are built to benefit Madrid and to take resources away from Barcelona.  Given independence, I've been told, the Catalans can use the extra money saved to build schools and hospitals.  But this means taking away schools and hospitals from the rest of Spain which isn't terribly pleasant to do.

Political policy differences was the most important reason in Scotland but hardly seems to feature in Catalunya.  <a href="http://presidencia.gencat.cat/web/.content/ambits_actuacio/consells_assessors/catn/informes_publicats/llibre_blanc_angles.pdf">In their independence white paper</a> is hardly features at all with all the space being taken up by process.  Both Catalan and Spanish governments are right of centre political parties.  When I've asked the only differences people can come up with are a desire not to have a monarch and wanting to ban bull fighting.  But the head of state is a superficial position and bull fighting is already banned.

<b>What to vote?</b>

So I've no idea what to vote, I'm not sure national identity or taking away schools from poor little Andaluthians is a good reason to vote Si.  But on the other hand a Yes vote would help Scottish independence and it would make a government which was closer to the people who voted it but still united under a bigger European Union.

In this two question poll there's the middle option of Si No but nobody knows what it means to be a state but not an independent state.

In the end it's unlikely to matter, the vote will result in a Si vote but Madrid will point out it was mostly those who wanted Si who voted and the silent majority didn't bother for this hastily organised voluntary questionaire. (This silent majority is <a href="https://pbs.twimg.com/media/BzXPLqlIgAA5rpa.jpg">personified by a naked lady with her finger over her mouth</a> in a curious display of the state of feminism in Spain and what feels l like another reason to vote SiSi).

Answers on a postcard please.
