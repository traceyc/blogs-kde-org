---
title:   "Kubuntu Edgy Release Candidate: the Digikam release"
date:    2006-10-19
authors:
  - jriddell
slug:    kubuntu-edgy-release-candidate-digikam-release
---
The <a href="https://wiki.kubuntu.org/EdgyEft/RC/Kubuntu">Kubuntu Edgy Release Candidate</a> is out.  After Ubuntu added f-spot we couldn't be outdone and added Digikam to the default CD for camera goodness.  We also upped KDE to 3.5.5.

<img src="http://www.digikam.org/sites/www.digikam.org/themes/digikam/logo.png" width="388" height="76" />
<!--break-->
