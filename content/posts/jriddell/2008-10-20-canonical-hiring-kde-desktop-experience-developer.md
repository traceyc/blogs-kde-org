---
title:   "Canonical Hiring KDE Desktop Experience Developer"
date:    2008-10-20
authors:
  - jriddell
slug:    canonical-hiring-kde-desktop-experience-developer
---
Are you a KDE developer?  Do you want to help improve the KDE desktop experience?  Then Canonical's all new desktop experience team <a href="http://webapps.ubuntu.com/employment/canonical_KDEV/">is looking to hire you</a>.  Let me know if you have any questions.
