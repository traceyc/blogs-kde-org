---
title:   "KDE Meets in Scotland"
date:    2009-05-13
authors:
  - jriddell
slug:    kde-meets-scotland
---
We finally had a meeting of KDE people in Edinburgh, not quite Akademy but it's a start.  
<img src="http://people.ubuntu.com/~jriddell/DSCN3243.JPG" width="500" height="375" />
Nice tie Paul.

A couple weeks later Laura Dragan gave a talk to BCS Glasgow on Nepomuk, nice to see KDE interest going outside the free software crowd.
<img src="http://people.ubuntu.com/~jriddell/DSCN3253.JPG" width="500" height="312" />
<!--break-->
