---
title:   "Cool Kubuntu Users"
date:    2010-04-26
authors:
  - jriddell
slug:    cool-kubuntu-users
---
On the <a href="http://www.kubuntu.org/">kubuntu.org front page</a> we list a few interesting Kubuntu users of various shapes and sizes to give a feel for how diverse use is.

One rather cool user which is missing is <a href="http://blog.dustinkirkland.com/2010/01/39000-core-ubuntu-cluster-renders.html">Weta Digital</a>.  Whenever I've been out the flat this week I've seen adverts for the Avatar DVDs, those blue 3D faces are all made on Kubuntu desktops and a whopping 35,000 cluster of rendering machines.  That must be a large proportion of computers in New Zealand running Kubuntu.

<img src="http://1.bp.blogspot.com/_-mej0A6dVeU/S1jcjyqbmdI/AAAAAAAAAmo/dKRE5PP8Uvg/s400/avatar.png" width="275" height="400" alt="avatar poster" />
I am Kubuntu

Then I got an e-mail from a guy at <a href="http://www.lionstracs.com/">Lionstracs</a> makers of groovy musical keyboards.  The keyboards run Kubuntu for all your composing and music playing needs.

This <a href="http://www.youtube.com/watch?v=xoPuZvqtgW4">YouTube video of the Kubuntu keyboards</a> shows them in action.

<img src="http://www.lionstracs.com/store/images/groove/x6redfront800.jpg" width="400" height="178" alt="kubuntu keyboard" />
I am also Kubuntu

No doubt KDE is spreading out from the classic desktop use, from niche embedded use like keyboards to massive budget graphics we're taking over the world!

<hr />

<a href="http://www.kubuntu.org/countdown"><img src="http://people.canonical.com/~jriddell/10.10-countdown/kubuntu_3.png" width="180" height="150" alt="3 Days to Kubuntu 10.04 LTS" /></a>
Long Term is coming...
<!--break-->
