---
title:   "Akademy Call for Papers; Vodafone 3G Datacard and Kubuntu; Gnome Calendar"
date:    2007-01-15
authors:
  - jriddell
slug:    akademy-call-papers-vodafone-3g-datacard-and-kubuntu-gnome-calendar
---
The <a href="http://akademy2007.kde.org/conference/cfp.php">Akademy 2007 call for papers</a> is out.  If you are doing something cool that's KDE or cross desktop related, this is for you.  Don't think your project isn't important enough to contribute, it is.  Doing a talk is not hard, and it's very satisfying, let us know what you're up to.

<br>

As a christmas present to myself I got a Vodafone 3G data card.  It's quite a stuggle getting this thing working.  Infact it's quite a struggle to get it at all, first the card didn't arrive, then it didn't arrive again, then they didn't know anything about me, then my promotional code wasn't valid any more but on the fifth try it all went smoothly.  Next there are no useful instructions about how to put the SIM chip in.  It could go in any one of 4 ways but the instructions just say to match it to the contacts on the card for which I would need to take the card apart to find out where the contact are.  The answer is the chip faces the top of the card with the cut-out-corner towards the nearest corner of the card.  Unfortunately trying this with feisty Herd 2 linux just complains about IRQ's and gives up.  So I was daft enough to try on Windows where the 65MB of installed software just complains about "a problem with the SIM" and gets no further.  After trying various numbers I got through to someone who was happy to help and said I should have installed the software with the wifi disabled, but that didn't change anything.  So I fired up a dapper live CD and after some experiments found a wvdial setup which made the light go blue and it all worked. Files at http://kubuntu.org/~jriddell/vodafone-3g-datacard/ You need to set up DNS manually, but it seems nice.  At £7.50 a MB (Pay as you talk) I won't be browsing many web pages with this, but if I need to check e-mail from the train, it should be useful.

<br>

In return for dragging my girlfriend along to Croatia on the pretence of a holiday but actually to give a talk on Kubuntu she showed how much attention she was paying by buying me this wonderful Gnome Calendar as a present.  Very tasteful.

<img src="http://muse.19inch.net/~jr/tmp/070114_13411.jpeg" width="320" height="240" />
<!--break-->