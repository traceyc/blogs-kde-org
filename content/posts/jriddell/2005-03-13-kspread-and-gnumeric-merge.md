---
title:   "KSpread and Gnumeric to merge"
date:    2005-03-13
authors:
  - jriddell
slug:    kspread-and-gnumeric-merge
---
Here's a fun pipedream overheard on #kde-devel.  Very exciting I think you'll agree.  Yes you freedesktop.org skeptics, you'll agree too.  KOffice developers will agree too even though they don't know about it yet.

02:14 <b> JohnFlux</b> WOOHOO!  I'm hoping to merge kspread and gnumeric :)
02:14 <b> JohnFlux</b> It will be sooo cool if we can pull it off
02:14 <b> DirkGently</b> merge???
02:14 <b> JohnFlux</b> gnumeric is many man years ahead of kspread
02:14 <b> JohnFlux</b> DirkGently: merge the MV part (model,view) and keep our own C (controller)
02:15 <b> JohnFlux</b> share - create a libspreadsheet that we both use
02:15 <b> Riddell</b> JohnFlux: blog!
02:15 <b> JohnFlux</b> blog?
02:15 <b> Riddell</b> blog!
02:15 <b> JohnFlux</b> kay..
02:15 <b> DirkGently</b> JohnFlux: a C lib?
02:15 <b> Riddell</b> it's exciting news, create buzz, share the love
02:16 <b> JohnFlux</b> will be GLIB
02:16 <b> JohnFlux</b> which isn't a problem.  a libspreedsheet would be around 15MB source (my guess. gnumeric is 20MB)
02:16 <b> SadEagle</b> so no more spreadsheet on Linux with code that doesn't totally suck?
02:16 <b> JohnFlux</b> where as glib is like 1/2 mb
02:16 <b> SadEagle</b> JohnFlux: you really code to kill the last sane codebase?
02:17 <b> JohnFlux</b> SadEagle: I really hope so
02:17 <b> JohnFlux</b> always.  look at the destruction i caused in konversation :)
02:17 <b> JohnFlux</b> it's time to move to bigger things! :)
02:17 <b> Riddell</b> JohnFlux: do you have a blog?
02:17 <b> JohnFlux</b> Riddell: unfortunetly not.
02:17 <b> JohnFlux</b> Riddell: blog for me ;)
02:18 <b> JohnFlux</b> although i haven't discussed this with the koffice people yet...
02:18 <b> JohnFlux</b> :)
02:18 <b> Riddell</b> but well I tucked you into bed as FOSDEM, you owe me
02:18 <b> JohnFlux</b> was that you?!
02:18 <b> JohnFlux</b> Riddell: i wondered how the hell i got to bed
02:20 <b> JohnFlux</b> Riddell: please note in the blog that the gnumeric guys have worked hard to make the core of gnumeric not require GTK (think gobject signals etc) so that this could be possible, thanks to the work of "jody"
02:20 <b> JohnFlux</b> Riddell: blog if you want, but big disclaimer that koffice people don't know about it yet

<!--break-->
