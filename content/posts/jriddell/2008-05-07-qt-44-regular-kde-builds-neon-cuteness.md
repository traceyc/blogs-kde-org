---
title:   "Qt 4.4, Regular KDE builds from Neon, Cuteness!"
date:    2008-05-07
authors:
  - jriddell
slug:    qt-44-regular-kde-builds-neon-cuteness
---
Qt 4.4 brings lots of goodness.  Packages are now entering hardy-backports.

If you want the latest unstable trunk Qt, kdelibs, kdebase and amarok on a regular basis without the hassle of compiling it yourself, <a href="https://edge.launchpad.net/~project-neon">project Neon</a> has a Launchpad Personal Package Archive with regular builds.

Lastly, these were left on our doorstep today, free to a good home.  <a href="http://www.flickr.com/photos/jriddell/sets/72157604920245386/">Cuteness</a>.

<a href="http://www.flickr.com/photos/jriddell/sets/72157604920245386/"><img src="http://farm3.static.flickr.com/2129/2472475788_64a29e898e.jpg?v=0" width="500" height="375" /></a>
<!--break-->

