---
title:   "Kubuntu Plasma 5 ISOs Rolling"
date:    2014-07-28
authors:
  - jriddell
slug:    kubuntu-plasma-5-isos-rolling
---
<a href="http://download.kubuntu.co.uk/kubuntu-plasma5/"><img src="https://www.google.com/webpagethumbnail?c=41&r=4&s=300&d=http://qa.kubuntu.co.uk/kubuntu-plasma5/&a=AIYkKU_idlBdBa0MCofexJr5pTwD0acbEQ" width="300" height="300" /></a>

Your friendly Kubuntu team is hard at work packaging up <a href="http://www.kde.org/announcements/plasma5.0/">Plasma 5</a> and making sure it's ready to take over your desktop sometime in the future.  Scarlett has spent many hours packaging it and now Rohan has spent more hours putting it onto some ISO images which you can download to try as a live session or install.

This is the first build of a flavour we hope to call a technical preview at 14.10.  Plasma 4 will remain the default in 14.10 proper.  As I said earlier it will  eat your babies.  It has obvious bugs like kdelibs4 theme not working and mouse themes only sometimes working.  But also be excited and if you want to make it beautiful we're sitting in #kubuntu-devel having a party for you to join.

I recommend downloading by Torrent or failing that zsync, the server it's on has small pipes.

Default login is blank password, just press return to login.
