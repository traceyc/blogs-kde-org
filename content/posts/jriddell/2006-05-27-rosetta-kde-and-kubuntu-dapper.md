---
title:   "Rosetta for KDE and Kubuntu Dapper"
date:    2006-05-27
authors:
  - jriddell
slug:    rosetta-kde-and-kubuntu-dapper
---
A note to the KDE translation teams:

Ubuntu's web based translation tool Rosetta is now complete for Ubuntu
and Kubuntu Dapper.

 https://lists.ubuntu.com/archives/ubuntu-announce/2006-May/000082.html

This is the first release which has KDE imported into Rosetta so KDE
translation teams now have the option of using Rosetta for their KDE
translations.

As a web based application Rosetta offers an easier way to translate
Free Software without having to learn about Subversion and .po files.

The KDE translation domains in Rosetta are those in Ubuntu's main
archive, which is most of KDE 3.5.2.

 https://launchpad.net/distros/ubuntu/dapper/+translations

To use Rosetta you need to join the Launchpad translation team for
your language.  There are already translation teams for many languages
actively translating Ubuntua and now Kubuntu using Rosetta, even if
nobody in your translation team wants to use Rosetta it is probably a
good idea to peridically check if there are KDE translations that you
can take from it to put into KDE's SVN.  Anyone can export from
Rosetta.

A couple of notes.  If you already know all about Subversion and .po
files you probably won't be wanting to use a web based interface, but
keep in mind that new translators to your team may find it easer.

I believe that there is also a web based translation tool planned for
l10n.kde.org, you may wish to wait for that, but the advice about
checking Rosetta for translations by Ubuntu translation teams still
applies.

Let me know if you have any questions.
<!--break-->
