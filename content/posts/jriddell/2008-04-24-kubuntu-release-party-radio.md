---
title:   "Kubuntu Release Party Radio"
date:    2008-04-24
authors:
  - jriddell
slug:    kubuntu-release-party-radio
---
<a href="http://kubuntu.org/news/8.04-release">Kubuntu 8.04 is out</a>, <a href="https://help.ubuntu.com/community/HardyUpgrades/Kubuntu">upgrade now</a> for rock solid 3.5.9 goodness, or <a href="http://kubuntu.org/download.php">download the CD</a> with a choice of 3.5.9 or cutting edge KDE 4.

New stuff includes printer autoconfiguration (coming to the rest of the world in KDE 4.1), super easy compiz setup, auto-codec install for Kaffeine, Wubi the Windows installer making it easy to install for those first timers, bulletproof X, NTFS Support and User Mountable Hard Disks and the latest versions of everything you can think of.

Kubuntu Release Party Radio is streaming now on http://audio.radioamarok.com, join us on #kubuntu-devel for the party

<img src="http://kubuntu.org/~jriddell/hardy_kde_glasses.png" width="258" height="188" />
<!--break-->

