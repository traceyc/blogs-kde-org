---
title:   "Kubuntu to be Sponsored by Blue Systems"
date:    2012-04-11
authors:
  - jriddell
slug:    kubuntu-be-sponsored-blue-systems
---
A while ago I had to announce that Canonical weren't going to sponsor Kubuntu any more.  'Fair enough' I thought, Canonical needs to make money and after dropping support for Gnome support for KDE couldn't be far behind.  Then I got e-mails from companies, governments and individuals saying they depend on it, use it every day for themselves, their clients and their families. It turns out Kubuntu is very popular and important, people really use this KDE stuff I've been working on for the last decade.  Blue Systems took the plunge first and said they'd sponsor the project so Kubuntu will continue as a community led, KDE focused Ubuntu flavour and will branch out into new areas like the tablet and ARM.  Maybe even server? (KDE has nice apps in OwnCloud and Kolab, these should be better integrated into Ubuntu.)  So an exciting future, want to join us?
