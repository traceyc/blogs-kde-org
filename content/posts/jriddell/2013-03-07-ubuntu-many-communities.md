---
title:   "Ubuntu is Many Communities"
date:    2013-03-07
authors:
  - jriddell
slug:    ubuntu-many-communities
---
Some people mis-read (or I mis-wrote) my <a href="http://blogs.kde.org/2013/03/06/ubuntu-community-community">previous blog</a> as critisising Ubuntu for not caring about its community any more.  That's not what I wanted to say and it wouldn't make any sense, Ubuntu is a large community, it can hardly stop caring about itself.  Rather I was saying that Canonical has done some moves recently which show a lack of concern for the Ubuntu community.  Numerous other blogs on Planet Ubuntu say the same thing today.  <a href="http://www.markshuttleworth.com/archives/1228">Mark did too today</a> by saying that Canonical is taking more of a lead in development because GNOME and KDE et al were failing to take over the world, an entirely sensible decision to make.  So if you want to be able to take a strong part in contributing then Ubuntu Unity is not the best part of Ubuntu to go.  That's fine as there are many parts which are waiting for more people to help out, I recommend Kubuntu but there's dozens of other flavours and sub-projects waiting with open arms.