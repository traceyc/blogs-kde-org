---
title:   "Final CD Testing"
date:    2009-04-20
authors:
  - jriddell
slug:    final-cd-testing
---
Testers are needed for the <a href="http://cdimage.ubuntu.com/kubuntu/">proposed CD and DVD ISOs</a> for the final Kubuntu 9.04 Release.  Upgrades from <a href="https://help.ubuntu.com/community/JauntyUpgrades/Kubuntu">8.10</a> and <a href="https://help.ubuntu.com/community/JauntyUpgrades/Kubuntu/8.04">8.04</a> need testing too.  Report your results on <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">ISO testing</a> and #kubuntu-devel.
