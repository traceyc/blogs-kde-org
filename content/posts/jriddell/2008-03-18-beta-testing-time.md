---
title:   "Beta Testing Time"
date:    2008-03-18
authors:
  - jriddell
slug:    beta-testing-time
---
8.04 Beta is due this week in Ubuntu land.  We have two versions of Kubuntu needing testing this time, the plain old KDE 3 one and the cutting edge KDE 4 Remix, so plenty to help out with.  CDs will appear soon at <a href="http://iso.qa.ubuntu.com/">the ISO testing site</a>, then you can download (<a href="http://cdimage.ubuntu.com/">from cdimage</a>) and check if it runs & installs.  Report your results on the ISO testing site.  Join us in #kubuntu-devel and #ubuntu-testing to coordinate.

There's upgrade testing to be done too.  If you have gutsy installed run 

<pre>
kdesu "adept_manager --dist-upgrade-devel"
</pre>

and click on Version Upgrade. Let us know how it runs.

And we have the all new Windows Installer Wubi, if you have 5 gigs of free space on a windows machine please test that.

<!--break-->
