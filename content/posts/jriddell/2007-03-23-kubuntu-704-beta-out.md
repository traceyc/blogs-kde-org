---
title:   "Kubuntu 7.04 Beta Out"
date:    2007-03-23
authors:
  - jriddell
slug:    kubuntu-704-beta-out
---
Kubuntu Feisty now has a Beta.  We need testers for the <a href="http://wiki.kubuntu.org/KubuntuDistUpgrade">Edgy to Feisty update tool</a>, please give it a shot and comment on the bug mentioned.  

Take a look at the <a href="https://wiki.ubuntu.com/FeistyFawn/Beta/Kubuntu">beta release page</a> for what's new.

As usual we have a <a href="https://wiki.ubuntu.com/FeistyFawn/Beta/Kubuntu/Feedback">feedback page</a> to tell us what's good and what's bad.  I don't tend to respond to the comment on these pages but they do come directly into my inbox so please keep them short and to the point.

For releases I like to have a random Kubuntu themed picture.  This time I'm going for photos from Akademy 2006 of the Kubuntu banner.  So you may find yourself in the release announcement.

<img src="http://kubuntu.org/images/kubuntu-talk.jpg" width="282" height="215" class="showonplanet" />

In other news kubuntu-de.org <a href="http://www.kubuntu-de.org/nachrichten/software/kde/kde4-und-kubuntu">did a short interview with me about our KDE 4 plans</a>.  If you read German as well as I can <a href="http://www.kubuntu-de.org/interview-mit-riddell-zu-kde4-englisch">try the translation</a>.
<!--break-->
