---
title:   "KDE 4.2 Released"
date:    2009-01-28
authors:
  - jriddell
slug:    kde-42-released
---
<img src="http://www.kde.org/img/kde42.png" width="437" height="199" />

KDE 4.2 was released yesterday, causing a busy day of last minute package fixes, poking build daemons to go faster, adjusting release announcements and then that the final exciting release moment when I have to update Dot News, Planet (ooh new 4.2 artwork), kubuntu.org and IRC topics all at once.  Then sit back and wait for feedback.  Pleasingly it's been really good with coverage on a lot of news sites, so give it a shot and let us know what you think.

