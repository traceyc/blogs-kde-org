---
title:   "Ubuntu Open Week; D-Link Routers with Kubuntu"
date:    2007-04-23
authors:
  - jriddell
slug:    ubuntu-open-week-d-link-routers-kubuntu
---
<a href="https://wiki.kubuntu.org/UbuntuOpenWeek">Ubuntu Open Week</a> is happening again with lots of interesting talks and Q & A sessions with la creme de Ubuntu developers.  Tomorrow at 21:00 UTC (and again on Thursday) imbrandon tells us all about Kubuntu and how you can help out with the groovyest distro around.

D-Link, like make brands of ADSL routers run Linux.  Unlike certain other embedded Linux users they provide all the sources they use, but for their <a href="http://www.d-link.de/?go=gNTyP9CgrdFOIC4AStFCF834mptYKO9ZTdvhLPG3yV3oVox7haltbNlwaaFp6DQoHDruzidK/4gAAw==">Asterisk enabled VoIP router</a> not only do they have <a href="ftp://ftp.dlink.de/dva/dva-g3342sb/driver_software/">an ISO to download</a> with all their sources, but the ISO is also a live DVD Kubuntu disk which boots up into a lovely development enviroment to work with those sources.  Now that's groovy.  <a href="http://www.heise.de/english/newsticker/news/88428">Heise has the details</a>.

<a href="http://kubuntu.org/~jriddell/dlink.png"><img src="http://kubuntu.org/~jriddell/dlink-wee.png" width="400" height="300" alt="dlink Kubuntu DVD" /></a>
(Hint: they removed sudo support in favour of a root password of <em>dlink</em>.)
<!--break-->
