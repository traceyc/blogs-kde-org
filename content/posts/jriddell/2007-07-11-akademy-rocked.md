---
title:   "Akademy Rocked"
date:    2007-07-11
authors:
  - jriddell
slug:    akademy-rocked
---
Akademy happened in Glasgow and rocked lots.  Unfortunately a combination of no internet for the first day and a half and kdedevelopers.org being down meant much of the atmosphere didn't get onto the internet at the time.  Of course you can catch up with the <a href="http://akademy2007.kde.org/conference/programme.php">videos</a>, including talks from Mark Shuttleworth and myself.

I had to run away early but thanks to all those who organised Akademy, gave talks, ran BoFs and brought us sub-conferences like the text layout summit and the edu & schools day.  

The only problems we had were being kicked out of the university at the early hour of 10 at night, and people not drinking enough beer.  When we buy beer we expect everyone to drink their quantity but for unknown reasons a surprising amount was left at the end.

I dropped into Lugradio live briefly to sell a few t-shirts was pleased to meet some Kubuntu people.

This week I'm in the top secret Canonical Tower along with the Ubuntu distro team working on KDE 4 packages and looking down upon Big Ben.

<a href="http://jasmine.19inch.net/~jr/away/2007-07-04-akademy-glasgow/100_0945.jpg"><img src="http://jasmine.19inch.net/~jr/away/2007-07-04-akademy-glasgow/index.php?thumb=true&name=100_0945.jpg" width="133" height="100" /></a>
Downstream loves you, oh yes we do.  <a href="http://jasmine.19inch.net/~jr/away/2007-07-04-akademy-glasgow/">More love photos</a>.

<a href="http://en.wikipedia.org/wiki/Millbank_Tower"><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Millbank_Tower,_Thames_House,_Parliament.JPG/250px-Millbank_Tower,_Thames_House,_Parliament.JPG" width="250" height="188" alt="canonical tower photo" /></a>
Canonical Tower looks over lesser buildings, what's the weather like down there?
<!--break-->
