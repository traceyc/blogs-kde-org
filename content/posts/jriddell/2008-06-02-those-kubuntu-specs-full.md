---
title:   "Those Kubuntu Specs in Full"
date:    2008-06-02
authors:
  - jriddell
slug:    those-kubuntu-specs-full
---
The Ubuntu Develpers Summit finnished a week ago in Prague.  Unlike conferences, summits have written output in the form of specifications which form the basis for work on Kubuntu for the next six months.  You can find the Kubuntu ones on <a href="https://wiki.kubuntu.org/KubuntuUDSPragueSpecs">this wiki page</a>.  

We started with <a href="https://wiki.kubuntu.org/KubuntuReleaseSchedule">kubuntu-release-schedule</a> where we discussed the possibility of moving the Kubuntu release schedule to match that of KDE.  We decided against this in the end, but I think it was a worthwhile discussion to have.  There would be goodness in the form of the latest and greatest KDE with each release, but it would also be a more buggy .0 version, would mean releasing with a 4 month old platform and would be quite a bit of extra work.

<a href="https://blueprints.launchpad.net/ubuntu/+spec/kubuntu-intrepid-version">Kubuntu Intrepid Version</a> makes the decision to move to KDE 4 by default (anything else is history).  KDE 3 libs will still be available for applications without a KDE 4 version, but the desktop won't be.  It's a good time to move to KDE 4 since Intrepid is intended to be a more cutting edge release.

<a href="https://wiki.kubuntu.org/KubuntuIntrepidKDE4Porting">KDE 4 Porting</a> lists the various bits that need to be ported to KDE 4.  From the upgrade tool to printer configuration, there's quite a few here.

<a href="https://wiki.kubuntu.org/KubuntuIntrepidCouncil">Kubuntu Intrepid Council</a> says we'll be needing new council members for the group who vote on memberships and occasionally other decisions. We'll be implementing this at the Kubuntu council meeting on Wednesday 22:00UTC in #ubuntu-meeting, all welcome.

With <a href="https://wiki.kubuntu.org/KubuntuFilesharing">Kubuntu Filesharing</a> we specified how sharing files should be made easy for users.  This is something which has annoyed me for a while and it wouldn't be hard to fix, a daemon which launches a webdav server or pokes Samba in the right way plus integration into the file manager.

<a href="https://wiki.kubuntu.org/KubuntuIntrepidWebsite">Kubuntu Intrepid Website</a> looks at changes we'd like to our website, although currently the main blocker is getting the new Drupal version launched at all.

Finally <a href="https://wiki.kubuntu.org/KubuntuIntrepidDefaults">Kubuntu Intrepid Defaults</a> decided to go with the lovely KDE 4 defaults, stripy window bars and all.  But we came up with some changes to Kickoff we'd like.

So plenty of work there to do, we'd love you to join us in #kubuntu-devel to help out.

<img src="http://farm3.static.flickr.com/2111/2515965530_c097795f1a.jpg?v=0" width="500" height="334" />
Happy Summit Group Photo
<!--break-->
