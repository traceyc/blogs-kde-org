---
title:   "Ubuntu Open Week"
date:    2006-11-24
authors:
  - jriddell
slug:    ubuntu-open-week
---
<a href="https://wiki.ubuntu.com/UbuntuOpenWeek">Ubuntu Open Week</a> happens next week and has a plethora of intersting looking IRC sessions on how to develop for Ubuntu.  Among talks specific to one of the Ubuntu teams is the Kubuntu talks being done by Brandon, which should be really interesting.  

Part of the reason this was organised is an attempt to entice openSUSE developers annoyed at the Novell/Microsoft deal over to Ubuntu.  I'm not interested in doing that myself though, since I see the people and projects of SuSE as being quite different from Novell.  SuSE continues to be an excellent distribution giving vital core-development to KDE (and thus important to Kubuntu).

But regardless, I'll miss the whole thing since I'm on holiday in Croatia, if you're in the area I believe the #ubuntu-hr people will be organising a meeting.
<!--break-->
