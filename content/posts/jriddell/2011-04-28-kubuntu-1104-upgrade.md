---
title:   "Kubuntu 11.04 for the Upgrade"
date:    2011-04-28
authors:
  - jriddell
slug:    kubuntu-1104-upgrade
---
<img src="http://www.kubuntu.org/files/11.04-release/banner-short-3.png" width="700" height="230" />

A new release is out.  The <a href="http://www.kubuntu.org/news/11.04-release">11.04 release page</a> shows what's new.  Samba is a theme with working file and printer sharing, however did we let that stay broken for so long.  Improvements to the language selector are nice.  We switched to GStreamer for the Phonon backend, which gives us better HTML5 video support.  The GTK Oxygen theme makes Firefox et al fit in nicer.  And of course all the goodness of <a href="http://www.kde.org/announcements/4.6/">KDE's 4.6 releases</a>.  We even snuck in a game of Patience onto the CD after 5 years of persistent requests asking for a game by default.

See the <a href="https://help.ubuntu.com/community/NattyUpgrades/Kubuntu">upgrade instructions</a> or the <a href="http://www.kubuntu.org/getkubuntu/download">download page</a> to get it.

Users of other Ubuntu flavours can just install the <b>kubuntu-desktop</b> package to try it out.  Switching to Plasma is no less of a step than switching to any of the new Gnome desktops this release, it's got to be worth trying.

Many thanks as always to the wonderful Kubuntu community, including a bunch of new names doing plenty of useful packaging, bugfixes and all the rest of it.

Got ideas for the next release?  Let us know on the <a href="https://wiki.kubuntu.org/Kubuntu/Specs/UDS-O">Kubuntu UDS-O</a> planning page.
<!--break-->
