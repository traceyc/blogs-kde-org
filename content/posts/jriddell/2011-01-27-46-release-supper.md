---
title:   "4.6 Release Supper"
date:    2011-01-27
authors:
  - jriddell
slug:    46-release-supper
---
Fair fa' your honest, sonsie face,
Great chieftain o' the desktop-race!
Aboon them a' ye tak your place,
Coder, tester, or usability expert:

4.6 Release Supper in Edinburgh tonight had haggis, neeps, tatties and finest Burns poetry.

<img src="http://people.canonical.com/~jriddell/DSCF5252.JPG" width="400" height="300" />
Jonathan, Rabbie, Kenny, Thomas, Colin
<!--break-->
