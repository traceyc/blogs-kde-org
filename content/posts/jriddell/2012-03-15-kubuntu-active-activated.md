---
title:   "Kubuntu Active is Activated"
date:    2012-03-15
authors:
  - jriddell
slug:    kubuntu-active-activated
---
<img src="http://people.canonical.com/~jriddell/kubuntu-active-logo-idea.png" width="429" heigh="200" />

The first Ubuntu flavour for tablets is now <a href="http://cdimage.ubuntu.com/kubuntu-active/daily-live/">making daily builds</a>.  We even got our <a href="https://bugs.launchpad.net/ubuntu/+source/kubuntu-active-meta">first bug reports</a> from our localy Plasma Active upstreams.  Images are for i386 only for now, ARMv7 should be added when we know it's a bit more stable and have testers.

The logo above is only an idea, it's the extent of my SVG skills.  I also updated the <a href="http://blogs.kde.org/node/4545">blogs.kde.org poll</a> :)

<img src="http://people.canonical.com/~jriddell/kubuntu-active-wee.png" width="350" height="219" />
<!--break-->
