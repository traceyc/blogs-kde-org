---
title:   "KDE Thanks the Sponsors of the Akademy 2007 Conference"
date:    2007-10-10
authors:
  - jriddell
slug:    kde-thanks-sponsors-akademy-2007-conference
---
So says this advert me and the other organisers put in this month's Linux Magazine.  

<a href="http://kubuntu.org/~jriddell/akademy-2007-sponsor-thanks.jpg"><img src="http://kubuntu.org/~jriddell/akademy-2007-sponsor-thanks-wee.jpg" width="200" height="275" /></a>

Thanks to them all for making Akademy possible and to media partner LNM for the advert space.

It's quite pleasing having raised a few tens of thousands of euro in the name of our great project.  I'll be passing the responsibility onto Wendy and the crew for next year in Belgium.
<!--break-->
