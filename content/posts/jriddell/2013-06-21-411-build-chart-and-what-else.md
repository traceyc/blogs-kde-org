---
title:   "4.11 Build Chart and What Else?"
date:    2013-06-21
authors:
  - jriddell
slug:    411-build-chart-and-what-else
---
<a href="https://wiki.kubuntu.org/Kubuntu/Ninjas/DependencyGraph?action=AttachFile&do=get&target=kde-dep-graph-411.svg">
<img src="https://wiki.kubuntu.org/Kubuntu/Ninjas/DependencyGraph?action=AttachFile&do=get&target=kde-dep-graph-411.svg" />
SC 4.11 build chart.</a>  Lines and lines and lines!

<hr />

< Riddell> "Aurélien renewed their own membership in the Kubuntu Members"  yay agateau still loves us!
< aurélien> Riddell: of course I do!
< aurélie> You know, sometimes people are surprised I am still running Kubuntu now that I left Canonical. I like to answer them "What else?" :)