---
title:   "Kubuntu Breezy CDs"
date:    2006-01-09
authors:
  - jriddell
slug:    kubuntu-breezy-cds
---
My, what a lot of Kubuntu CDs.  Let me know if you're doing a KDE talk at a LUG, having an install day, running a KDE stall at an expo or otherwise have a good excuse and I'll send you a  box.

<a href="http://jriddell.org/photos/2006-01-09-kubuntu-breezy-cds.jpg"><img src="http://jriddell.org/photos/2006-01-09-kubuntu-breezy-cds.jpg" class="showonplanet" width="300" height="400" /></a>
<!--break-->

