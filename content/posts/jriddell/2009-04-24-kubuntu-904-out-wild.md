---
title:   "Kubuntu 9.04 Out in the Wild"
date:    2009-04-24
authors:
  - jriddell
slug:    kubuntu-904-out-wild
---
<img src="http://people.ubuntu.com/~jriddell/9.04-release/kubuntu-choc.jpg" width="350" height="217" />

The Kubuntu Team is proud to announce the <a href="http://www.kubuntu.org/news/9.04-release">release of Kubuntu 9.04, the Jaunty Jackalope</a>!
With this release, the development team brings you the best KDE distro out there. With its world-renowned Ubuntu core and the KDE 4.2 desktop, Kubuntu 9.04 gives users a well-rounded, feature-filled and elegant desktop.

On the top, KDE 4.2 brings beauty, innovation and updated software with new & improved features. Underneath, updated kernel and core systems bring stability and speed.
<!--break-->
