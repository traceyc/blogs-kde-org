---
title:   "Tutorials Day in a Few Hours"
date:    2009-06-29
authors:
  - jriddell
slug:    tutorials-day-few-hours
---
<a href="https://wiki.kubuntu.org/KubuntuTutorialsDay"><img src="http://people.ubuntu.com/~jriddell/kubuntu-tutorials-day-2009.png" width="472" height="148" /></a>

See you in the IRC channel in a few hours for interesting <a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">tutorials on a range of topics</a>.
