---
title:   "Canonical Software: No-Name-Yet Warthogs and Ubuntu"
date:    2004-08-28
authors:
  - jriddell
slug:    canonical-software-no-name-yet-warthogs-and-ubuntu
---
Canonical Software is the name of a company making a new Debian based distribution.  I suspect there isn't much knowledge of this among KDE developers so here is what is known.  Mark Shuttleworth is the man who used to do the company that become Verisign (and was the first African in space) has hired the best Debian, Gnome and Python developers to make a freely downloadable distribution which I suspect is supposed to be Debian made good (shorter release cycles being one of the main features).

They are working in private over the usual internet channels but feeding back to Debian and elsewhere through <a href="http://www.no-name-yet.com">no-name-yet.com</a>.  They have just had their first conference, which was open to all, in Oxford.  Daniel Stone came to akademy from there.

Apparantly the distribution will be called Ubuntu and may be paticularly suited to laptops.

The signs are that this could be something big, more so than the likes of Linspire, Xandros or Lycrosis.  Unlike those companies they understand Free Software and open development.

It is likely to be a Gnome based job, but maybe there is a KDE developer out there who is working for them without letting on, if not I'm always available...
<!--break-->