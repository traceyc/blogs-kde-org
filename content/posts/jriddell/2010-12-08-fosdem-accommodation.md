---
title:   "FOSDEM Accommodation"
date:    2010-12-08
authors:
  - jriddell
slug:    fosdem-accommodation
---
If you are coming to FOSDEM, one of the largest gathering of free software developers you probably want some accommodation.  Claudia will be booking beds for KDE people next week, if you want to stay with the KDE spods put your name on the <a href="http://community.kde.org/Promo/Events/FOSDEM/2011">KDE at FOSDEM wiki page</a> before Thursday.
<!--break-->
