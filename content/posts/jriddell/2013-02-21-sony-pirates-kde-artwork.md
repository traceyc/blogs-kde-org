---
title:   "Sony Pirates KDE Artwork"
date:    2013-02-21
authors:
  - jriddell
slug:    sony-pirates-kde-artwork
---
Sony, the company who created Audio CDs which installed a rootkit on Windows computer to try to stop people copying music has pirated KDE artwork.  The preferences-system.png icon from Oxygen is on their <a href="http://www.sony.co.uk/customise/vaio-t-series">Choose your Vaio</a> webpages (next to configure) but impressively is also on the UEFI firmware should you boot up into Assist mode.  Nowhere on their <a href="http://www.sony.co.uk/pages/terms/TandC_odw_en_GB.html?null">website terms of use</a> does it list the LGPL 3 licence it may be copied under (It does say "<i>Any unauthorised use or copying of site content, or use of site content which breaches these Terms (or their spirit) may violate trade mark, copyright and other proprietary rights, and have civil and criminal consequences</i>" although it also says "<i>You must seek and obtain the written consent the operator of this site before creating any link to this site</i>" so I don't give that page any legal credit.) Should KDE e.V. and Nuno's Oxygen friends start a new business model by sueing them for everything they're worth?

<img src="http://people.ubuntu.com/~jr/sony-pirate-artwork.png" width="300" height="90" />
Oxygen icon on left.  "Choose your Vaio" web page fragment on right.
<!--break-->