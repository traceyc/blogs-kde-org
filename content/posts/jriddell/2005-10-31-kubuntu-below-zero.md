---
title:   "Kubuntu Below Zero"
date:    2005-10-31
authors:
  - jriddell
slug:    kubuntu-below-zero
---
The Ubuntu developer summit got under way today in Montreal with a community love day.  Talks covered the wonders of Breezy, Edubuntu, LTSP, advocacy and Launchpad.  I <a href="https://wiki.ubuntu.com/UbuntuBelowZero/LoveDay">took some rough notes</a>.  My own talk on Kubuntu went well (<a href="http://kubuntu.org/~jr/kubuntu-below-zero/">slides</a>) and I was pleased to see a good number of Kubuntu users in the audience.  Also really exciting is that South African distribution Impilinux will be the first Kubuntu derivative.  

Back in KDE land KOffice removed the rainbow icon from their website.  KOffice has suffered from a lack of branding and in my mind if they didn't like that as a logo the correct action is to come up with another idea and poke some artists into drawing it up.  But much worse is that rainbows are also used by the gay community as a symbol and this was seen as a bad connotation for KOffice.  That enlightened free software developers can hold such prejudice views (and others don't critisise them) is really disappointing to me.

Meanwhile the Quaker takeover of Ubuntu continued with elite open source man Russ Nelson turning up to the show.

<img src="http://www.kubuntu.org/~jr/photos/2005-10-30-montreal/jonathan-russ.jpg" class="showonplanet" />
<!--break-->