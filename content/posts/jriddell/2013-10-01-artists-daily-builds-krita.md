---
title:   "Artists: daily builds of Krita"
date:    2013-10-01
authors:
  - jriddell
slug:    artists-daily-builds-krita
---
The <a href="https://launchpad.net/~dimula73/+archive/krita">Krita Lime</a> PPA provides daily builds of the best free software painting application out there.  For aspiring artists Krita is a fabulous app to create works of inspiration.

Krita is optimised for painting but I also use it for many image manipulation tasks, it's installed by default in Kubuntu.

See <a href="http://dimula73.blogspot.co.uk/2013/05/krita-lime-ppa-always-fresh-versions.html">dmitryK's blog</a> for instructions to install it.

<img src="http://krita.org/images/splash_big.jpg" width="720" height="474" />
