---
title:   "Croatia and Qt4 Porting"
date:    2006-12-24
authors:
  - jriddell
slug:    croatia-and-qt4-porting
---
Today's my birthday, this makes me happy because I get presents and cards and I get to tick off another successful year on the chart of life.

More exciting though was going to Croatia last month.  I gave a talk at the fantastically impressive <a href="http://www.fazan.org/">Monteparadiso Hacklab</a> where some borrowed bandwidth and Ubuntu machines give free internet access to the locals of Pula.  In the Mediteranian part of Croatia at least life seems to be very relaxed, and most people seem to only work a couple hours a day.  I didn't ask much about the Yugoslavian war, but the general attitude seems to be that it's not a major issue, they'll just stay out of the way of people in Serbia until the memory has passed some more.  

I've been porting a couple of apps to Qt 4.  The documentation for porting is a bit disappointing, depreciated method calls are usually listed without any pointers to the new alternative.  PyQt 4 works a charm though.

Language Installer now in PyQt 4, porting started by Josef, with usability improvements thanks to seele and nifty integration to the KControl module.

<a href="http://kubuntu.org/~jriddell/tmp/language-selector-kcontrol2.png"><img src="http://kubuntu.org/~jriddell/tmp/language-selector-kcontrol2.png" width="355" height="257" /></a>

and for comparison, the old Language Selector it replaces.

<a href="http://kubuntu.org/~jriddell/tmp/language-selector-old.png">
<img src="http://kubuntu.org/~jriddell/tmp/language-selector-old.png" width="225" height="225" /></a>
<!--break-->
