---
title:   "conf.kde.in Slides"
date:    2011-03-10
authors:
  - jriddell
slug:    confkdein-slides
---
Having a fabulous time at conf.kde.in in Bengaluru.  I've just given a tutorial on writing a programme in PyKDE, then releasing it to the wide world, then packaging it and releasing it for Kubuntu.  Here are the slides as requested by my lovely audience.

<a href="http://people.canonical.com/~jriddell/conf.kde.in/tutorial/">Programming PyKDE and Getting it into Kubuntu slides and files</a>.

Best "talk" so far, Knut doing his robotic break dance.

<img src="http://people.canonical.com/~jriddell/conf.kde.in/at-conf.kde.in.png" width="322" height="108" />
<!--break-->
