---
title:   "Ryanaired"
date:    2004-11-04
authors:
  - jriddell
slug:    ryanaired
---
My laptop was Ryanaired.  A new screen would be 500 quid so result was a new laptop (same trusty IBM Thinkpad r40e).  

Lessons: always carry your laptop as hand luggage on an aeroplane and always check your luggage before you leave the airport.

<img src="http://jriddell.org/photos/2004-11-04-broken-laptop.jpg" width="300" height="450" />
<!--break-->
