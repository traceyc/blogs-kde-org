---
title:   "Kubuntu Gear"
date:    2008-12-11
authors:
  - jriddell
slug:    kubuntu-gear
---
Stylish new <a href="http://shop.canonical.com/index.php?cPath=28">Kubuntu gear available at the Canonical Shop</a>.  Just the perfect give for that special someone for Christmas.

<img src="http://www.kubuntu.org/~jriddell/jriddell-wee.jpg" width="300" height="400" />
<!--break-->
