---
title:   "That Last Night Party in Pictures"
date:    2006-11-14
authors:
  - jriddell
slug:    last-night-party-pictures
---
On the final night of our Ubuntu Developer Summits we usually go into town for a posh dinner followed by revelling and merryness.  So who was the greatest dancer on the dance floor?  You decide...

"I won't dance with my girlfriend but I will dance with you..."

<img src="http://jasmine.19inch.net/~jr/away/2006-11-11-uds/index.php?thumb=true&name=100_0756.JPG" width="75" height="100" />

"Doing it for the masses..."

<img src="http://jasmine.19inch.net/~jr/away/2006-11-11-uds/index.php?thumb=true&name=100_0754.JPG" width="75" height="100" />

"Just here to watch the local anthropology..."

<img src="http://jasmine.19inch.net/~jr/away/2006-11-11-uds/index.php?thumb=true&name=100_0755.JPG" width="75" height="100" />

"Flowers in your hair..."

<img src="http://jasmine.19inch.net/~jr/away/2006-11-11-uds/index.php?thumb=true&name=100_0753.JPG" width="75" height="100" />


"Looking after the community..."

<img src="http://jasmine.19inch.net/~jr/away/2006-11-11-uds/index.php?thumb=true&name=100_0752.JPG" width="133" height="100" />

After the conference I've come to San Francisco for a week's team building with Canonical.  The motivational guru taught us that those who focus and follow instructions will fail and be humiliated, but otherwise I really like this city.  I canoed out to Alcatraz (which I was told on return is a criminal offence) and ice skated on the Embarcado rink.

<!--break-->
