---
title:   "Open Street Mapping"
date:    2008-03-20
authors:
  - jriddell
slug:    open-street-mapping
---
Went to Wolverhampton LUG the other week (don't ask).  Their LUG meetings are divided into two sorts, the loud sort where LUGRadio presenters turn up, and the quiet polite sort where they don't.  This meeting had the special task of mapping the entirety of Wolverhampton "city" centre (Wolverhampton is a suburb of Birmingham, but don't say that to anyone who lives there, you won't survive long).  Impressively enough we managed it, by dividing into four pairs and taking lots of GPS trails.  In years to come people will be riding through Wolverhampton using Satellite Navigation Systems and I'll be able to say, "that part of the world, I discovered it first".

<img src="http://kubuntu.org/~jriddell/wolverhampton-openstreetmap.png" width="300" height="188" alt="wolverhampton map thumbnail" />
<!--break-->
