---
title:   "Kubuntu: as Seen on TV"
date:    2006-02-14
authors:
  - jriddell
slug:    kubuntu-seen-tv
---
Linux.conf.au got onto New Zealand news.  The clip features Mark Shuttleworth signing one of the &gt;500 Kubuntu CDs that were given away.  <a href="http://kubuntu.org/~jriddell/lca2006_tvclip.xvid.avi">lca2006_tvclip.xvid.avi (13MB)</a>.
