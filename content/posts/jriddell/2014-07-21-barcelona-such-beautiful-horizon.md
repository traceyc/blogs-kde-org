---
title:   "Barcelona, such a beautiful horizon"
date:    2014-07-21
authors:
  - jriddell
slug:    barcelona-such-beautiful-horizon
---
When life gives you a sunny beach to live on, make a mojito and go for a swim.  Since KDE has an office that all KDE developer are welcome to use in Barcelona I decided to move to Barcelona until I get bored.  So far there's an interesting language or two, hot weather to help my fragile head and <a href="https://scontent-b-ams.xx.fbcdn.net/hphotos-xpf1/t1.0-9/10464349_10152267168983983_6602920010667013985_n.jpg">water polo in the sky</a>.  Do drop by next time you're in town.

<img src="https://farm6.staticflickr.com/5553/14686726111_456ebc7d3d_z.jpg" width="360" height="640" />
Plasma 5 Release Party Drinks
<hr />
Also <a href="https://blogs.kde.org/content/best-thing-about-plasma-5">new poll for Plasma 5</a>.  What's your favourite feature?
