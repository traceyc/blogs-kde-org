---
title:   "Re-blog: Martin Gräßlin Reply to “All the faces of Ubuntu”"
date:    2013-03-08
authors:
  - jriddell
slug:    re-blog-martin-gräßlin-reply-“all-faces-ubuntu”
---
<pre>A <a href="http://blog.martin-graesslin.com/blog/2013/03/reply-to-all-the-faces-of-ubuntu/">repost of elite KWin maintainer Martin Gräßlin</a> from Planet KDE</pre>

<p>Dear Mark Shuttleworth,</p>
<p>so you <a href="http://www.markshuttleworth.com/archives/1235">“have absolutely no doubt that Kwin will work just fine on top of Mir”</a>. This is great and I totally appreciate that you think Mir is a great system. But I’m wondering why you don’t use KWin then, after all it will work fine on top of Mir and is Qt based?</p>
<p>But I have doubt that KWin will work just fine on top of Mir and I have already <a href="http://blog.martin-graesslin.com/blog/2013/03/war-is-peace/">stated so</a>. You might have wanted to check the facts before stating such claims (somehow I get a feeling for a pattern here).</p>
<p>What makes me think that you cannot make such bold claims:</p>
<ul>
<li>You don’t even know how to write KWin</li>
<li>Currently the number of commits to KWin by an Canonical employee is 0 (git log — kwin | grep @canonical)</li>
<li>No Canonical employee has so far contacted the KWin team on how we could integrate Mir and whether we are interested at all</li>
<li>I have to question the abilities of Canonical to judge what other software can do and cannot after Canonical argued with non existing issues in Wayland for Mir</li>
<li>We are still waiting for the Wayland adjustments for KDE done by Canonical. May I <a href="http://www.markshuttleworth.com/archives/551">remind you</a>:<br>
<blockquote>We’ll help GNOME and KDE with the transition, there’s no reason for them not to be there on day one either.<p></p></blockquote>
</li>
</ul>
<p>I have to ask you to keep KWin out of the pro-Mir campaign. I didn’t ask for Mir, I don’t want Mir and reading blog posts like the one which triggered this reply does lower my motivation to ever have anything to do with Mir. Mir is an answer to a question nobody asked. It’s a solution to problem which does not exist.</p>
<p>Your community manager recently posted on Google+ he had a frustrating day. Guess what my week has been and guess who I can blame. Guess what I great day I will have after reading your blog post this morning.</p>