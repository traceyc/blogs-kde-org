---
title:   "Notes from Breakout sessions at Mataro Sessions II"
date:    2013-05-08
authors:
  - jriddell
slug:    notes-breakout-sessions-mataro-sessions-ii
---
<a href="http://www.flickr.com/photos/jriddell/8719060825/" title="13050016 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7317/8719060825_55c615c834.jpg" width="500" height="282" alt="13050016"></a>

Here's some notes from the breakout sessions we had yesterday for my own recolation.

<b>KWin and Wayland on Kubuntu</b>

<ul>
<li>Differences between Wayland and Mir are minimal, only client vs server side buffer allocation, so no benefit from it for a desktop distro</li>
<li>dri3000 is a project to make X do client side buffer allocating</li>
<li>KWin won't support Mir, (unless canonical wants to do it) it has no ABI stability</li>
<li>KWin was working on Wayland suppport, now shifted effort to Qt 5 support before going back to Wayland work.</li>
<li>KWin in 4.11 might work with Qt 5 but not for distros to use</li>
<li>X wayland is a rootless X server used in both Wayland and Mir, no use for KWin which needs a rooted X server.</li>
<li>wayland has libwaylandserver and libwaylandclient libraries, libwaylandclient for use in toolkits like Qt.</li>
<li>current KWin architecture not fully planned but probably Weston as system compositor and KWin as client compositor</li>
<li>worry that Canonical might havily patch mesa for Unity/Mir making Kwin/Weston break (more then it already is)</li>
<li>Only Weston needed for LightDM then Kwin started same as currently by startkde (maybe a bit earlier in the script)</li>
<li>Change to Wayland for Kubuntu about 14.10 or 15.04</li>
<li>Wayland & Weston release every 3 months, KWin every 6 months</li>
<li>Unity will have 1 binary equivalent to X, compiz and unity</li>
<li>Weston can be run on X for testing</li>
<li>current thinking is to ensure Debian's Weston packages are in Ubuntu archive and use those for Kubuntu, might make ubuntu-desktop and kubuntu-desktop not easily co-installable</li>
<li>Installer ubiquity-dm a tricky point</li>
</ul>

<a href="http://www.flickr.com/photos/jriddell/8719174921/" title="DSC06831 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7373/8719174921_364998f146.jpg" width="500" height="333" alt="DSC06831"></a>

<b>Kubuntu SRUs</b>

<ul>
<li>KDE people get frustrated when their point releases don't get into distros or get in very slowly</li>
<li>ubuntu SRU process is beurocratic for a reason - introducing new bugs is really nasty</li>
<li>lightdm did introduce a new bug recently in a point release which was caught in the SRU process</li>
<li>building SRUs in a PPA first to test unnecessary and time wasting, just test it locally then upload to -proposed</li>
<li>possible scope to have approved from ~ubuntu-sru during the 7 day testing period rather than before to remove one blocker?</li>
<li>delay in KWin updates causes bug reports to keep coming into KWin bugzilla, very time consuming for upstream</li>
<li>work items too brief - danger of not understanding later what a point is</li>
<li>use of trello to be encouraged for work item tracking with enough info to understand each point from someone coming fresh</li>
<li>UEHS unknown if it works reliably, needs to be investigated and improved to update packaging as much as possible itself, at lp:dehs</li>
<li>public kde-packager e-mail list (discussion has been done several times in past)</li>
<li>automated copyright file from upstream?</li>
</ul>
