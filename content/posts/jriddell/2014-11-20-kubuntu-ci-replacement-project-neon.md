---
title:   "Kubuntu CI: the replacement for Project Neon"
date:    2014-11-20
authors:
  - jriddell
slug:    kubuntu-ci-replacement-project-neon
---
<img src="http://people.ubuntu.com/~jr/kubuntu-ci.png" width="480" height="294" alt="Kubuntu CI" />
[thanks to jens for the lovely logo]

Many years ago Ubuntu had a plan for <a href="https://wiki.ubuntu.com/UbuntuDownUnder/BOFs/GrumpyGroundhog">Grumpy Groundhog</a>, a version of Ubuntu which was made from daily packages of free software development versions.  This never happened but Kubuntu has long provided Project Neon (and later Project Neon 5) which used launchpad to build all of KDE Software Compilation and make weekly installable images.  This is great for developers who want to check their software works in a final distribution or want to develop against the latest libraries without having to compile them, but it didn't help us packagers much because the packaging was monolithic and unrelated to the packages we use in Kubuntu real.

Recently Harald has been working on a replacement, <a href="https://community.kde.org/Kubuntu/PPAs#Kubuntu_Continuous_Integration_.28CI.29">Kubuntu Continuous Integration (Kubuntu CI)</a> which makes packages fresh each day from KDE Git for Frameworks and Plasma and crucially uses the Kubuntu packaging branches.  There are three PPAs, unstable (unchecked), unstable daily (with some automated checking to ensure it builds) and unstable weekly (with some manual checking)

At the same time he's been hard at work making weekly <a href="http://files.kde.org/snapshots/">Kubuntu CI images</a> which can be run as a live image or installable.  They include the latest KDE Frameworks, Plasma 5 and a few other apps.

We've moved our packaging into Debian Git because it'll make merges ever so much easier and mean we can share fixes faster.

The <a href="http://kci.pangea.pub/">Kubuntu CI Jenkins</a> setup has the reports on what has built and what needs fixed.

Ahora es la hora
