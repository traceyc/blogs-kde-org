---
title:   "Umbrello 1.4"
date:    2005-01-04
authors:
  - jriddell
slug:    umbrello-14
---
<a href="http://uml.sf.net">Umbrello UML Modeller</a> 1.4 has gained tabbed diagram layout and entity relationship diagrams for databases as well as a tonne of fixes and XMI compatibility from Oliver.  The ER diagrams stuff is quite basic at the moment and we could really do with some help, you don't have to be the world's best programmer just willing to spot the missing features (SQL generation please) and add it.

<a href="http://uml.sourceforge.net/screen.php"><img src="http://uml.sourceforge.net/screenshots/umbrello-entiry-relationship-medium.png" /></a>
<!--break-->
