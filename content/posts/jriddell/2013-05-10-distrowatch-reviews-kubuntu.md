---
title:   "Distrowatch Reviews Kubuntu"
date:    2013-05-10
authors:
  - jriddell
slug:    distrowatch-reviews-kubuntu
---
<a href="http://distrowatch.com/table.php?distribution=kubuntu">Distrowatch</a> is a handy site to check out the history of Kubuntu.  Today they put up a <a href="http://distrowatch.com/weekly.php?issue=20130506#feature">review of Kubuntu 13.04</a>. <b>"Kubuntu gave me a very responsive graphical environment, an application menu which was free from advertisements and the distribution did not insist I install third-party drivers just to use my desktop."</b>  Ubuntu Unity comes in for some critisism though, probably when they post (back) to Qt their graphics requirements will become sane again.

<a href="http://www.flickr.com/photos/jriddell/8724862175/" title="DSCF7601 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7294/8724862175_0ab91b1f47_n.jpg" width="320" height="240" alt="DSCF7601"></a>
P for Vendetta

<a href="http://www.flickr.com/photos/jriddell/8724863461/" title="DSCF7610 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7406/8724863461_39fea22a2a_n.jpg" width="320" height="240" alt="DSCF7610"></a>
Konqi suffers fourth nerve palsy.  He gets very upset at pirate comments.

<a href="http://www.flickr.com/photos/jriddell/8725986010/" title="DSCF7614 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7432/8725986010_351657aff0_n.jpg" width="320" height="240" alt="DSCF7614"></a>
Barcelona Office, KDE has a home in the sun!
