---
title:   "GMail as Spam Filter"
date:    2006-11-10
authors:
  - jriddell
slug:    gmail-spam-filter
---
A few weeks ago the spam filter at Canonical which was doing a good job of keeping the flood of viagra opportunities at bay was turned off, and I've had trouble keeping up with my e-mail since then.  Most of the servers I have access to are virtual servers and not powerful enough to run spamassassin without killing the other virtual servers on the machine.  So I've turned to another option which is to send all my e-mail to GMail and set GMail to forward it back to me, which has the handy side effect of filtering out the spam.  There's surprisingly little delay in the extra roundtrip and it has the handy side effect of keeping a copy of all my e-mail should I accidently run rm -rf / on the server that I read it from.  In the 24 hours since settings this up I've got 800 real e-mails (not that many considering I subscribe to kde-cvs, kde-bugs-dist, ubuntu-wiki and other high traffic lists) and 300 spams, with only 1 false positive.
<!--break-->
