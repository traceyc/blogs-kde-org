---
title:   "Akademy Sponsors Call"
date:    2007-02-13
authors:
  - jriddell
slug:    akademy-sponsors-call
---
The Akademy 2007 <a href="http://dot.kde.org/1171382220/">Call for Sponsors</a> is out there.  We've contacted our previous sponsors, but there must be companies and organisations out there who depend on KDE and would be willing to help the most important conference out there in return for their logo on a big banner infront of hundreds of computer professionals.  Send us an e-mail if you know of any companies who fit that description.

And remember to <a href="http://akademy2007.kde.org/conference/cfp.php">send in your talks</a>.
<!--break-->