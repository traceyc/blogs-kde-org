---
title:   "Hogmanay, Freedom, Music downloads, IRC and Kubuntu in Hawaii"
date:    2006-01-01
authors:
  - jriddell
slug:    hogmanay-freedom-music-downloads-irc-and-kubuntu-hawaii
---
Edinburgh was busy last night for the worlds largest Hogmanay party and we saw in the new year with enough fireworks to cover the whole of the castle and Castle Rock in thick smoke.

2006 will see the 10th anniversary of KDE, and since desktop software is what most people think of when they think about computers that marks 10 years of bringing free software to the people.  As computers become more prevelant in people's lives (both as desktop machines and embedded) it becomes more important that we can use them however we see fit without restriction.  Europe won something of a victory over software patents in 2005 but they still exist and still affect us. Digital restriction management is growing and makes basic functions like watching DVD videos with KDE too difficult for most users, the next generation DVD formats are likely to be worse.  And the next version of Windows won't support DVD players without region restrictions.  So be proud KDE developers to be making a difference in bringing freedom back to computing, it's important.

See <a href="http://www.simbioticstore.com/index.html?ssid=bfaeaddac50ac2304d22&s=downloads&m=downloads&c=downloads">Simbiotic</a> for a nice Glaswegian company who let you buy music to download in Ogg Vorbis or FLAC formats.

#kde-devel has descended into another argument about Israel, sadly a lot of #kde-devel is even more off topic and no doubt off-putting to newer developer and users of the channel.  It's a shame that some KDE projects have to resort to hidden channels so their developers can get some peace (and I see the main gnome developers channel has gone the same way).  Ubuntu has a code of conduct which allows developers to take the moral high ground when telling people to shut up.  Of course it's also nice to be able to chat randomly on IRC to an extent, especially for those of us without a real workplace to get the gossip from so I'm not sure how best to keep #kde-devel under control.

<a href="http://tposscon.com/">TPOSSCON</a> is the second pacific conference for Open Source.  The word on the street is that Kubuntu is going to be the official operating system of the conference.  It takes place next week in Haiwaii so if you're in the (rather large, blue and watery) neighbourhood drop by to see my favourite operating system in use.
<!--break-->
