---
title:   "Akademy 2013 Day 3 in Photos - Kubuntu Developer Summit"
date:    2013-07-16
authors:
  - jriddell
slug:    akademy-2013-day-3-photos-kubuntu-developer-summit
---
<a href="http://www.flickr.com/photos/jriddell/9294016351/" title="DSCF8048 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7298/9294016351_e9c8972c29.jpg" width="500" height="375" alt="DSCF8048"></a>
At the Kubuntu Developer Summit we discussed various topics.  The guys on the left are from a 15,000 seat Kubuntu rollout in Munich, we worked out a plan to supply LTS backport packages they need.

<a href="http://www.flickr.com/photos/jriddell/9296796148/" title="DSCF8049 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5516/9296796148_a3a6af7b25.jpg" width="500" height="375" alt="DSCF8049"></a>
Dani the Akademy organiser loved Kubuntu so much they gave us a bucket of free beer :)

<a href="http://www.flickr.com/photos/jriddell/9296797070/" title="DSCF8056 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5480/9296797070_df2410424e.jpg" width="500" height="375" alt="DSCF8056"></a>
In parallel to the Kubuntu Developer Summit was Qt Contributor Summit, here setting up the keynote room before a talk.

<a href="http://www.flickr.com/photos/jriddell/9296801814/" title="DSCF8053 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2854/9296801814_f703b0fd7e.jpg" width="500" height="375" alt="DSCF8053"></a>
Venue change so lunch not as classy but instead more filling tortilla baguettes.

<a href="http://www.flickr.com/photos/jriddell/9296815700/" title="13070037 by Jonathan Riddell, on Flickr"><img src="http://farm3.staticflickr.com/2813/9296815700_919391085e.jpg" width="500" height="282" alt="13070037"></a>
The blue infiltration

<a href="http://www.flickr.com/photos/jriddell/9296817818/" title="13070038 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5445/9296817818_4cd5d1ba31.jpg" width="500" height="282" alt="13070038"></a>
The most classy Akademy ever, now we fill a whole arena with our parties.

<a href="http://www.flickr.com/photos/jriddell/9294044481/" title="13070042 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7390/9296815046_ab3e1c97dd.jpg" width="500" height="282" alt="13070042"></a>
You've not heard the Beatles until you've heard them in the original Basque.  The <a href="http://www.flickr.com/photos/jriddell/9294315585/">police came to break up the party</a>, another first for Akademy.

