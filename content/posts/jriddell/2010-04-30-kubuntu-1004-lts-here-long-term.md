---
title:   "Kubuntu 10.04 LTS, Here for the Long Term"
date:    2010-04-30
authors:
  - jriddell
slug:    kubuntu-1004-lts-here-long-term
---
<a href="http://www.kubuntu.org/news/10.04-lts-release"><img src="http://people.canonical.com/~jriddell/10.04-lts-release/kubuntu-10.04-banner.png" width="582" height="101" /></a>

Today we released Kubuntu 10.04 LTS.  This is the first Long Term Support release to feature KDE 4 Platform and Applications.  It's very exciting that the long journey to KDE 4 has come to the level of stability where we can call it LTS.

A big thank you to the hard working Kubuntu team, too many to name but they have toiled day and night over the last 6 months to make sure you have the latest software with the minimum of bugs.

Read the <a href="http://www.kubuntu.org/news/10.04-lts-release">10.04 LTS release announcement</a> to find out what's new and how to download it or upgrade from 9.10.
<!--break-->
