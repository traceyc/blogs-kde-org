---
title:   "Kubuntu Developer Summit @ Akademy"
date:    2013-05-08
authors:
  - jriddell
slug:    kubuntu-developer-summit-akademy
---
Since there's no UDS we have a room booked at Akademy to spend the day discussing things in Kubuntu

if you're coming put yourself on <a href="https://notes.kde.org/p/kubuntu-akademy">https://notes.kde.org/p/kubuntu-akademy</a>

Would be good to have as many Kubuntu people there as possible.                                                                                              
The Kubuntu Council has funds if you need sponsorship for travel, don't be afraid to ask. 