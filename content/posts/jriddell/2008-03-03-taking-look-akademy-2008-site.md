---
title:   "Taking a Look at the Akademy 2008 Site"
date:    2008-03-03
authors:
  - jriddell
slug:    taking-look-akademy-2008-site
---
Akademy 2007 organiser Kenny went to Belgium a few days before FOSDEM to <a href="http://lj.geeksoc.org/users/sealne/19741.html">scout out the Akademy 2008 location</a>.
<hr />
I started a poll to see what current thinking is on <a href="https://blogs.kde.org/comment/reply/3304">best application programming language</a>.  Surprisingly high Java support.
<!--break-->
