---
title:   "Photo triage: KDE retrospective"
date:    2013-12-25
authors:
  - jriddell
slug:    photo-triage-kde-retrospective
---
I've also been triaging old photos to make some room on my server, so here's a Christmas present of a retrospective of old KDE photos.

<a href="http://www.flickr.com/photos/jriddell/11502746646/" title="ukuug-linux2003-umbrello-talk-2003-08 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3705/11502746646_6c54338fcd.jpg" width="500" height="375" alt="ukuug-linux2003-umbrello-talk-2003-08"></a>
It all started here, my first KDE talk about Umbrello, my dissertation project. (And if anyone else is needing a dissertation to work on, I recommend a KDE project, take over Umbrello, academics love that UML stuff, it's a guaranteed A).

<a href="http://www.flickr.com/photos/jriddell/11502740996/" title="linux-users-expo-jun-2003-kde-stall by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5491/11502740996_22e90960d7.jpg" width="500" height="375" alt="linux-users-expo-jun-2003-kde-stall"></a>
An early Linux expo with Jono Bacon, whatever happened to him?

<a href="http://www.flickr.com/photos/jriddell/11502675156/" title="2005-12-01-osdl-gnome-kde-love by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5498/11502675156_1c5d3afc9a.jpg" width="500" height="333" alt="2005-12-01-osdl-gnome-kde-love"></a>
The 2005 desktop architects meeting in Portland with Gnomers

<a href="http://www.flickr.com/photos/jriddell/11502636885/" title="2008-02-kde4-release-event-kubuntu by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3735/11502636885_8f592e63f0.jpg" width="500" height="375" alt="2008-02-kde4-release-event-kubuntu"></a>
The KDE 4 release party in 2008! Complete with Konqi and Katie.  

<a href="http://www.flickr.com/photos/jriddell/11502666924/" title="2008-02-28-fosdem-kde by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3695/11502666924_f8d23b0f54.jpg" width="500" height="333" alt="2008-02-28-fosdem-kde"></a>
KDE at FOSDEM in 2008, we're going again this year, make sure you're there!

<a href="http://www.flickr.com/photos/jriddell/11502661506/" title="2005-09-01-akademy-beach-party by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7351/11502661506_4d278f37c8.jpg" width="500" height="375" alt="2005-09-01-akademy-beach-party"></a>
KDE beach party in Malaga, my web server logs show this photo is a favourite of bloggers

<a href="http://www.flickr.com/photos/jriddell/11502546344/" title="2004-04-21-linux-user-expo-rich-jonathan-chris-jeff-george-charles by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7413/11502546344_438b325d60.jpg" width="500" height="333" alt="2004-04-21-linux-user-expo-rich-jonathan-chris-jeff-george-charles"></a>
Another expo, back when there was a market for Linux expos

<a href="http://www.flickr.com/photos/jriddell/11502576913/" title="2003-08-24-kde-konference-group-photo by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3798/11502576913_056b2e6f84.jpg" width="500" height="375" alt="2003-08-24-kde-konference-group-photo"></a>
The first real KDE conference a decade ago, Kastle in Nove Hrady

Happy Christmas.  Ubuntu retrospective photo blog still to come