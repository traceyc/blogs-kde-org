---
title:   "Kubuntu Tutorials Day next Sunday"
date:    2008-06-09
authors:
  - jriddell
slug:    kubuntu-tutorials-day-next-sunday
---
Kubuntu Tutorials Day is back.  Join us on IRC for some fascinating chats with Free Software's finest developers.  

We have five months of development ahead of us for Intrepid, so this is the perfect way to learn how to get involved.

<ul>
<li>19:00UTC 
<b>  Getting involved.</b>
   What's happening in Intrepid and how can you join? Artwork, documentation, packaging, programming and more.
   with Richard Johnson (nixternal)</li>
<li>20:00UTC 
<b>  Usability.</b> Is it just about removing options? 
with   Celeste Lyn Paul (seele) 
<li>21:00UTC
<b>Packaging and merging howto </b>
   Turn your apps into .debs, fix the packages already in the archive. 
with   Jonathan Riddell (Riddell) </li>
<li>   22:00UTC
<b>Plasma with Python </b>
   Plasmoids that are easy to make. 
with   Michael Anderson (nosrednaekim) </li>
<li>   23:00UTC
<b>  Bug triage </b>
   How to squish the beasties. 
with   Ralph Janke (txwikinger) </li>
<li>From end of talks onwards... 
<b>  Kubuntu Q&A </b>
   Got a question? We're here to answer. 
with   Your Friendly Kubuntu Team</li>
</ul>

See <a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">Kubuntu Tutorials Day page</a> for more information.

<img src="http://kubuntu.org/~jriddell/kubuntu-tutorials-day.png" width="557" height="150" />
<!--break-->
