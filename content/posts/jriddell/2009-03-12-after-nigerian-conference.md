---
title:   "After the Nigerian Conference"
date:    2009-03-12
authors:
  - jriddell
slug:    after-nigerian-conference
---
We finished the conference with a final marathon Q & A session.  The questions had changed from basic "Does Kubuntu run the same apps as Red Hat" to the sort of thing I can't reliably answer "Can you run a pre-installed OS on a virtual server" (probably).  

After the close of the conference everyone went across town to the museum for a social event.  I spent an hour giving out autographs and having photos taken with my groupies.  There were dancers in fancy costumes dancing to a drum band.  We were given North African clothes to wear (a shirt which goes down to your knees, matching light trousers and a hat) and presented with very nicely crafted python skin laptop bags (not my usual style of fashion being semi-vegetarian and all, also entirely illegal to bring back into Europe without a licence but very thoughtful of them anyway).  We were made to sit on a podium and felt a lot like visiting  dignitaries.  They also made us dance a little.

<img src="http://farm4.static.flickr.com/3583/3344327094_4c177eb261_m.jpg" width="240" height="180" /><img src="http://farm4.static.flickr.com/3604/3343491805_c932d08c0a_m.jpg" width="240" height="180" /><img src="http://farm4.static.flickr.com/3610/3343430769_0d9e50e854_m.jpg" width="240" height="180" />
Social Event

After the conference we were rushed off to a dinner meeting with the minister for IT.  His department has been hard at work having the tallest building in town constructed which should soon house lots of computing companies.  He is very keen for open source to be used by these companies.  That doesn't mean proprietary software companies will be excluded but he did say he had been bullied a bit by Microsoft who wouldn't use the office if they went around promoting free software.  He said that free software was so important if it meant Microsoft would leave then so be it.  Two items were agreed, that his department would subsidise an open source academy in Kano to train people up in free software and to fund a project to create a Hausa language distribution.  We had another meeting with the computing department head for the Nigerian Open University who was very excited about Open Street Map among other things.

<img src="http://farm4.static.flickr.com/3566/3343489007_b3bf3a7667_m.jpg" width="240" height="180" />
We're going to have a large KDE logo fitted to this building

On my final day we did tourist things.  We wandered around the textiles market and sports stadium.  We visited the ancient dye pits used to dye fabrics blue and visited a blacksmith.  We looked at the government buildings, had a personal tour around the old museum and raced on motorbike taxis.

<img src="http://farm4.static.flickr.com/3632/3344328244_bcd231acbc_m.jpg" width="240" height="180" /><img src="http://farm4.static.flickr.com/3335/3344329004_9ecceee02b_m.jpg" width="240" height="180" />
Kano History Museum

Nigeria is a fabulous country.  There's poverty of course, but not desperately so as far as we saw and there's plenty of middle class laptop owning people too.  There's litter all over the place, but there's plenty of that in Scotland too.  Bank fraud is probably done by people oversees as much as at home, and anyway it's nothing like as bad as what Scotland has achieved in that area recently.  The food is lovely so long as you like spicy, the sun it hot but not scorching (dust from the sahara keeps that down) and the place feels perfectly safe with everyone friendly and welcoming.

<img src="http://farm4.static.flickr.com/3572/3343466115_70236c9c1c_m.jpg" width="240" height="180" alt="DSCN3008" /><img src="http://farm4.static.flickr.com/3404/3344296222_b915e06cbc_m.jpg" width="240" height="180" alt="DSCN3008" /><img src="http://farm4.static.flickr.com/3572/3343462429_265b5cabe4_m.jpg" width="240" height="180" alt="DSCN3002" />
Blacksmiths, Dye pits, Market

<a href="http://www.flickr.com:80/photos/jriddell/sets/72157614991241443/">More photos on flickr</a>
<!--break-->
