---
title:   "Kubuntu Laptops in Poland"
date:    2008-06-05
authors:
  - jriddell
slug:    kubuntu-laptops-poland
---
Fresh from <a href="http://blogs.kde.org/node/3500">Planet KDE</a> is the news that Polish supermarket Bledronka has <a href="http://www.biedronka.pl/str/2/i/20.php">an offer on Kubuntu Laptops</a>.  Comes complete with Kadu, "killer app in Poland".
<!--break-->
