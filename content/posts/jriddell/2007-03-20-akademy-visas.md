---
title:   "Akademy Visas"
date:    2007-03-20
authors:
  - jriddell
slug:    akademy-visas
---
As happens the registration for Akademy has been delayed because the one we were looking at didn't work.  If you need a visa to enter the UK (<a href="http://www.ukvisas.gov.uk/servlet/Front?pagename=OpenMarket/Xcelerate/ShowPage&c=Page&cid=1006977149962">UKVisas tells you here</a>) and you are a known KDE dude coming to Akademy then contact me to get your invitation letters sorted.
