---
title:   "Strigi Applet"
date:    2007-09-05
authors:
  - jriddell
slug:    strigi-applet
---
One of the goals of Kubuntu Gutsy is to have Strigi installed by default.  Unfortunately the existing panel app was a line edit embedded in kicker, which took up loads of space.  So I made it into a systray applet.

<img src="http://kubuntu.org/~jriddell/strigi4.png" width="412" height="465" />

Giving us at last a second use for KAnimWidget, while it's searching.

<img src="http://kubuntu.org/~jriddell/strigi5.png" width="203" height="100" />
<!--break-->
