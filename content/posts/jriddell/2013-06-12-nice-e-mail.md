---
title:   "nice e-mail"
date:    2013-06-12
authors:
  - jriddell
slug:    nice-e-mail
---
I still get the best e-mails :)

<i>
From: Tibor
To: jriddell@ubuntu.com     
Subject: Thank you!     

<i>   Hello Kubuntu/Ubuntu-Team,    
   i only want to say thank you for the great operating system Kubuntu, you  
   are the best. I`m very happy with this.    

<i>   Best regards                                                                                                                                              

<i>   Tibor from germany                                                                                                                                        
</i>

<hr />
My flights are booked for the Basque Country..
<a href="http://akademy2013.kde.org/"><img src="http://community.kde.org/images.community/3/3b/Ak2013Badge2.png" width="400" height="178" /></a>  
for <a href="http://blogs.kde.org/2013/05/08/kubuntu-developer-summit-akademy">Kubuntu Developer Summit @ Akademy</a>

