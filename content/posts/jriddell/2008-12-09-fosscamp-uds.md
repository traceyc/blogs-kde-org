---
title:   "FOSSCamp to UDS"
date:    2008-12-09
authors:
  - jriddell
slug:    fosscamp-uds
---
The UDS warmup that is FOSSCamp ended on Saturday.  Alex managed to charter an entire bus for the KDE spods just so we could hunt round the Google campus to find the giant android and take a fuzzy photo.

<img src="http://farm4.static.flickr.com/3235/3091372810_27a5ffa702.jpg?v=0" width="500" height="375" />
Me, Blauzahl, Chani, Adrien of KPackageKit/Nepomuk, Gary

Ubuntu Developer Summit started.  Launchpad man Kiko gave a talk on the future of Launchpad which included the freeing up of the code which is set to be released as Affero GPL next July.



