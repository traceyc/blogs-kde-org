---
title:   "How to Type Kubuntu in Japanese"
date:    2013-02-18
authors:
  - jriddell
slug:    how-type-kubuntu-japanese
---
Oriental and other input methods have long been fiddly in Kubuntu.  I'm very pleased to see that ace Kubuntu ninja Michal  Zajac has fixed the language packs to set up the input methods correctly when installing a language that needs it.

<a href="http://www.flickr.com/photos/jriddell/8486624348/" title="スナップショット4 by Jonathan Riddell, on Flickr"><img src="https://farm9.staticflickr.com/8104/8486624348_d2b8257f80_n.jpg" width="320" height="240" alt="スナップショット4"></a>
