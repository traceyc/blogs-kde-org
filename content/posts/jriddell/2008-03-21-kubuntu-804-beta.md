---
title:   "Kubuntu 8.04 Beta"
date:    2008-03-21
authors:
  - jriddell
slug:    kubuntu-804-beta
---
<img src="http://kubuntu.org/images/kubuntu-8.04-beta.png" width="256" height="256" />

The beta of Kubuntu 8.04 is available for all testers.  The supported version comes with the rock solid KDE 3 while a remix features the cutting edge KDE 4.  <a
href="https://wiki.kubuntu.org/HardyHeron/Beta/Kubuntu#head-eacd960c95e6f1eb8fb73688e414e85b112f6073">Download</a> or <a href="https://help.ubuntu.com/community/HardyUpgrades/Kubuntu">upgrade</a>. </p><p><a href="http://wiki.kubuntu.org/HardyHeron/Beta/Kubuntu">See the release announcement</a>.

Thanks to all those who tested.  Pretty release image based on pinheiro's Oxygen artwork and uses Celeste's glasses.
<!--break-->
