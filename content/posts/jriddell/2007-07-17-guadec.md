---
title:   "GUADEC"
date:    2007-07-17
authors:
  - jriddell
slug:    guadec
---
Being in the area I wandered down to GUADEC, the Gnome conference.  I sidled past the registration desk where people were grudgingly paying their entrance fee (in return for free t-shirts) said hi to assorted Ubuntu people (and Hub in his KDE t-shirt) and went to find the talks.  "xcomposite, xdamage, xrender, gtk+" bravely tried to explain what all these fangled X extentions do.  Turns out xcomposite doesn't compose, xdamage has a cool name and then I got a bit lost but it seems these days everyone just uses openGL anyway.  

"Managed D-Bus" sounded interesting but turned out to be about an implementation of D-Bus for c# rather than much about D-Bus itself so I didn't learn much new.

The talk on <a href="http://live.gnome.org/ProjectMallard">Project Mallard</a> was probably the most interesting.  This is about the changes being made to the Gnome documentation system.  They're adopting a new markup language because Docbook is too complex, and they're adding a new metadata system which uses .desktop files and makes it possible to document plugins (so if you document a plugin for kmail the text will appear in the kmail documentation).  This should be on freedesktop soon.  They're also making a WYSIWYG editor and <a href="http://live.gnome.org/ProjectMallard/DocumentationStyle">reviewing the writing style to be more task based</a>.  As with KDE their documentation team lacks people although I think they do better for docs related coding people than we do currently.  It would be great to see their new meta data system (which is based on the proposed freedesktop spec done by cornelius schumacher) adopted cross desktop so we could read docs from any app regardless of desktop.

The hour timetabled is too long for most talks though, so those are the only talks I could fit into my day.  Guadec is notably bigger than Akademy or Debconf.  Like us they seem to have had their share of network and video streaming issues.  For now though I think I'm a bit over-conferenced.  It's nice to sit down at my desktop computer again.
<!--break-->
