---
title:   "KDE 4 packages for Edgy"
date:    2006-09-09
authors:
  - jriddell
slug:    kde-4-packages-edgy
---
<p align="center"><img src="http://kubuntu.org/images/kde4.png" width="242" height="81" class="showonplanet" /></p>

I put up some packages of the <a href="http://kubuntu.org/announcements/kde4-3.80.1.php">KDE 4 first tech preview</a> for Edgy.  i386 only at the moment but they're sitting in Edgy's NEW queue and will be compiled for everything when they pass through that.  They install to /usr/lib/kde4 so it's quite safe to install them alongside your existing KDE 3.  I compiled Umbrello and was very happy to see that it compiled and worked without major problems.  Thanks to Oliver and Kleag for getting that to happen.  Having text under buttons is going to be an interesting challenge for a lot of programmes.

<a href="http://kubuntu.org/~jriddell/umbrello-kde4.png"><img src="http://kubuntu.org/~jriddell/umbrello-kde4-wee.png" width="300" height="214" class="showonplanet" /></a>
<!--break-->
