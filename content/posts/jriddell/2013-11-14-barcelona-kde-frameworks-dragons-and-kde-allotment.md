---
title:   "Barcelona: KDE Frameworks Dragons and KDE Allotment"
date:    2013-11-14
authors:
  - jriddell
slug:    barcelona-kde-frameworks-dragons-and-kde-allotment
---
It's little advertised but KDE has an office in Barcelona.  With Scotland getting cold, dark and wet I set off in search of sun and colleagues to have a change of scene and improve my health after a surgeon poked needles into my eye.

I've spent the week looking at KDE Frameworks 5, the port of KDElibs 4 to Qt 5 where it is being modularised so apps can use it without bringing in large dependencies. I've been working out <a href="https://community.kde.org/Frameworks/Coinstallability">coinstallability with KDElibs 4</a> which is needed for us distros to make sure it can be packaged.  I also ported my first bit of code to KF5/Qt 5 which was the icons kcontrol module.  I had to track down and fix a KDE Frameworks bug which doing so.  I've always been quite nervous about touching kdelibs because one breakage can affect users later in horrible ways but KDE Frameworks is a good way to get into it, everything is pretty much broken so you can only make it better and there's friendly meetings and IRC chat to keep to right.

<a href="http://www.flickr.com/photos/jriddell/10853265014/" title="BYzR0LNCAAEPfg2 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7358/10853265014_153040efe6.jpg" width="500" height="375" alt="BYzR0LNCAAEPfg2"></a>
KDE Frameworkers, they're epic

<a href="http://www.flickr.com/photos/jriddell/10853386273/" title="photo by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7395/10853386273_b36a2e3dc4.jpg" width="500" height="375" alt="photo"></a>
The KDE Allotment was put together by elite urban agriculturalist Nim Kibbler in the sterile back yard of the office, vegetables will soon be growing to make a fresh KDE Salad.

<a href="http://www.flickr.com/photos/jriddell/10853200954/" title="photo1 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5500/10853200954_4deb32a5d5.jpg" width="500" height="375" alt="photo1"></a>
Barcelona has a dragon slide.  It really is that awesome.
