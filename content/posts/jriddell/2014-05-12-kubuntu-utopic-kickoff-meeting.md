---
title:   "Kubuntu Utopic Kickoff Meeting"
date:    2014-05-12
authors:
  - jriddell
slug:    kubuntu-utopic-kickoff-meeting
---
A new cycle and lots of interesting possibilities!  Will KF5 and Plasma 5 be supreme?  All welcome at the Kubuntu kickoff meeting this european evening and american afternoon at <a href="http://www.timeanddate.com/worldclock/fixedtime.html?iso=20140512T20&p1=304">19:00UTC</a>.

Install mumble, get a headset with headphones and microphone, adjust volumes to be sane and join us on mumble server kyofel.dyndns.org
Chat in #kde-devel

Add items to discuss at <a href="https://notes.kde.org/p/kubuntu-utopic">the meeting notes</a> and <a href="https://trello.com/b/fNxJO1Ac/14-10">review the TODO items on Trello</a>.

See you there!
