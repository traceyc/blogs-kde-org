---
title:   "NHS Hack Scotland day 2"
date:    2013-03-24
authors:
  - jriddell
slug:    nhs-hack-scotland-day-2
---
<a href="http://www.flickr.com/photos/jriddell/8584549515/" title="DSCF7406 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8251/8584549515_e2db0a7d68.jpg" width="500" height="375" alt="DSCF7406"></a>
Linux geeks

<a href="http://www.flickr.com/photos/jriddell/8584549161/" title="DSCF7405 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8100/8584549161_c596a0709e.jpg" width="500" height="375" alt="DSCF7405"></a>
The judge and the organiser

<a href="http://www.flickr.com/photos/jriddell/8584548799/" title="DSCF7404 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8513/8584548799_1114d18758.jpg" width="500" height="375" alt="DSCF7404"></a>
The Ambulance Massive
