---
title:   "Ubuntu App Developers Week"
date:    2011-04-11
authors:
  - jriddell
slug:    ubuntu-app-developers-week
---
<a href="https://wiki.kubuntu.org/UbuntuAppDeveloperWeek">Ubuntu App Developers Week</a> has IRC talks on developing /on/ Ubuntu (rather than developing bits of Ubuntu itself.  Of interest to Kubuntu and KDE folks is "<b>Widgetcraft: The Art of Creating Plasma Widgets</b>" by Harald Sitter today at 19:00UTC and "<b>Creating a KDE app with KAppTemplate</b>" Jonathan Thomas tomorrow Tuesday at 19:00UTC.  Friday at 17:00 sees "<b>Phonon: Multimedia in Qt</b>" with Harald Sitter.

Then on Thursday and Friday from 16:00UTC there's a special treat, Jürgen Bocklage-Ryannel from Nokia. talking on various <b>Qt Quick</b> topics.  

There's even a <a href="https://wiki.kubuntu.org/UbuntuAppDeveloperWeek/LightningTalks">lightning talk</a> with muesli on <b>Tomahawk Player</b> on Friday at 21:00UTC.

All on #ubuntu-classroom on freenode.
<!--break-->
