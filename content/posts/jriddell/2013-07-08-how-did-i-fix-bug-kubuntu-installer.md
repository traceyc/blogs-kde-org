---
title:   "How did I fix a bug in Kubuntu installer?"
date:    2013-07-08
authors:
  - jriddell
slug:    how-did-i-fix-bug-kubuntu-installer
---
New Kubuntu contributor Shrinivasan T's blogs about <a href="http://goinggnu.wordpress.com/2013/07/04/how-did-i-fix-a-bug-in-kubuntu-installer/">How did I fix a bug in kubuntu installer?</a>.  The post takes you through the process of forking, fixing and merging some code from Bazaar to get it into Kubuntu. Thanks Shrinivasan!



