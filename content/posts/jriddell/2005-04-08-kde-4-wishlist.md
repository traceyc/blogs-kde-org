---
title:   "KDE 4 Wishlist"
date:    2005-04-08
authors:
  - jriddell
slug:    kde-4-wishlist
---
Whenever something in KDE has frustrated me in the last month I've added it to a list:

<ul>
<li>Tidy Konqueror.  Konqueror has hardly changed since KDE 2 although the settings menu has gained a few extra unnecessary entries (which should be merged into the main Configure Konqueror dialogue).  The sidebar should maybe be part of each tab not global to the window and a context sensitive sidebar for file manager would be nice.</li>
<li>Kicker's Add to Panel and Remove from Panel menus should be dialogues.</li>
<li>HAL support in KDE 3.4 is nice but Konqueror should pop up into the forground when a device is entered not have to wait for the user.</li>
<li>The Help Centre suffers from much the same usability issues as Konqueror, it's got too much stuff in it's user interface.</li>
<li>I'm not convinced that long manuals are the best format for documentation, shorter articles on specific actions which can be easily searched might work better.  So in KWord if I search for mail merge it can give me a two or three paragraph article about how to do a mail merge.</li>
<li>Systempreferences (in kdenonbeta) makes a good KControl replacement, of course there are larger plans afoot for KControl.</li>
<li>Share folders sensibly.  kpf is nifty but the user interface is all wrong (in kicker when it should be in Konqueror).  And I've never got the right click on folder->share folders to work.</li>
<li>A single download dialogue to replace all those file copying dialogues.  Being able to pause and resume downloads would be nice to.  KGet can do that but there's no reason for it to be separate from KDE core.</li>
<li>Improved KNotify.  This is where dbus needs to replace dcop since then everything down to syslogd can pop up friendly messages on screen as required.  Juk pops up messages when playing a new song and amarok pops up slightly more pretty looking messages but there's no reason why that can't be done through KNotify.  See <a href="http://www.growl.info/">Growl</a> for this being done well on MacOS.</li>
<li>Search in minicli.  I should be able to type something into minicli and it will pop up with all the relevent applications, recent e-mails, files on my hard disk, contacts in address book, etc.  See <a href="http://docs.blacktree.com/doku.php?id=quicksilver:what_is_quicksilver">Quicksilver</a> for MacOS goodness.  Sounds like a perfect use of Tenor (klink).</li>
<li>An option to say that actually I really would like to quit when I close the window and please stop telling me you're going to be hiding in the system tray.</li>
<li><a href="http://grammarian.homelinux.net/abakus/">abakus</a>, because I've wanted something like it for ages.</li>
<li>Maybe a new icon theme, one that actually is based on SVG so that it can be maintined.  Maybe call it Appeal.  Standardised icon names too.</li>
<li>Nicer website, with photos and pretty things.  More usable wiki.</li>
</ul>
<!--break-->
