---
title:   "Call for Talks at FOSDEM 2011"
date:    2010-12-01
authors:
  - jriddell
slug:    call-talks-fosdem-2011
---
<a href="http://dot.kde.org/2010/12/01/call-talks-fosdem-2011">Talks wanted for Cross-Desktop room at FOSDEM</a>.  Doing something interesting in KDE?  Tell the world of developers about it.  Submissions by 22nd December 2010 to: crossdesktop@lists.fosdem.org
<img src="http://people.canonical.com/~jriddell/fosdem.png" width="599" height="104" />
