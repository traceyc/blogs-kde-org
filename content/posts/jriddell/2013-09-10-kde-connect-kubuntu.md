---
title:   "KDE Connect in Kubuntu"
date:    2013-09-10
authors:
  - jriddell
slug:    kde-connect-kubuntu
---
Thanks to the elite packaging skills of smartboyhw and the review skills of Quintasan we now have kdeconnect in Kubuntu saucy so you can use your phone to control Amarok.

<a href="http://www.omgubuntu.co.uk/2013/09/install-kde-connect-on-ubuntu-get-call-notifications">OMG Ubuntu</a> has a review.

<a href="http://people.ubuntu.com/~jr/plasma-desktopGM2040.png"><img src="http://people.ubuntu.com/~jr/plasma-desktopGM2040.png" width="300" height="285" /></a>
