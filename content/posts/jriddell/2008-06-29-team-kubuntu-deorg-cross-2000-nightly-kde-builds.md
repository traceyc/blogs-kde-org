---
title:   "Team kubuntu-de.org cross 2000.  Nightly KDE builds."
date:    2008-06-29
authors:
  - jriddell
slug:    team-kubuntu-deorg-cross-2000-nightly-kde-builds
---
<a href="https://wiki.kubuntu.org/5-A-Day">5-A-Day</a> is a simple way for everyone to get involved in helping Kubuntu or other parts of the Ubuntu family, just triage 5 bug reports a day.  Today <a href="http://daniel.holba.ch/5-a-day-stats/">the statistics show</a> Team Kubuntu-de.org are the first team to triage 2000 beasties.  Well done folks (especially top triager txwikinger who I had the pleasure of meeting earlier this week).

<hr />

Want to get your latest KDE fix?  Project Neon has launched <a href="http://apachelog.blogspot.com/2008/06/project-neon-kde-nightly-builds.html">nightly KDE builds</a> thanks to apachelogger and Launchpad PPAs.
<!--break-->