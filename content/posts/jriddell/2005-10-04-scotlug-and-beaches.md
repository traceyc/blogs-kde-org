---
title:   "ScotLUG and Beaches"
date:    2005-10-04
authors:
  - jriddell
slug:    scotlug-and-beaches
---
Last week I had the privilage of meeting Ariya at ScotLUG.  I introduced him to Irn-Bru and we discussed Speedcrunch, KSpread, Kubuntu and his top secret project (he didn't let on what it was).

<img src="http://jriddell.org/photos/2005-10-01-ariya-jonathan.jpg" width="400" height="300" class="showonplanet" />

Best part of Akademy was the beach party sponsored by the city council.  Free beer and Free fish.  The mayor (I believe it was) handed out sombreros to all and gave a rousing speech in broken English.  We went swimming of course and this should be the splash screen for KDE 3.5.

<img src="http://jriddell.org/photos/2005-09-01-akademy-beach-party.jpg" width="400" height="300" class="showonplanet" />

<!--break-->