---
title:   "conf.kde.in Slides II"
date:    2011-03-12
authors:
  - jriddell
slug:    confkdein-slides-ii
---
Here are the slides to my <a href="http://people.canonical.com/~jriddell/conf.kde.in/kubuntu-friendly-computing.odp">conf.kde.in talk Kubuntu: Friendly Computing</a>.  They're more images though so they don't make much sense without me talking.

India is a lovely country with lovely people.  Food is excellent of course.  Bengaluru is a busy city with crazy traffic (still preferable to Paris of course).  Only downside is the mosquitoes and lack of bandwidth.  

<img src="http://people.canonical.com/~jriddell/conf.kde.in/at-conf.kde.in.png" width="322" height="108" />

<div style="float: left; border: thin solid grey; margin: 1ex; padding: 1ex; width: 240px" />
<a href="http://www.flickr.com/photos/jriddell/5519171984"><img src="http://farm6.static.flickr.com/5091/5519171984_eb4c811b4d_m.jpg" width="240" height="180" /></a><br />
<a href="http://www.flickr.com/photos/jriddell/5519171984">conf.kde.in Group Photo</a>
</div>
<!--break-->
