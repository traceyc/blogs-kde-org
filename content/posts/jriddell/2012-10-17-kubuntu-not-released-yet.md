---
title:   "Kubuntu Not Released Yet"
date:    2012-10-17
authors:
  - jriddell
slug:    kubuntu-not-released-yet
---
My head trauma showing itself with a pre-ticked publish box on the Kubuntu release story that just escaped long enough to make it onto Planets KDE and Ubuntu.  We're still testing and will be needing more CD testers tonight, come and join us in #kubuntu-devel
