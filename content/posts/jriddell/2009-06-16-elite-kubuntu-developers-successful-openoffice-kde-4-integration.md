---
title:   "Elite Kubuntu Developers Successful OpenOffice KDE 4 Integration"
date:    2009-06-16
authors:
  - jriddell
slug:    elite-kubuntu-developers-successful-openoffice-kde-4-integration
---
At UDS in Barcelona we again bemoaned that nobody had done the work to integrate OpenOffice with KDE 4.  So elite Kubuntu developers Roman Shtylman and Yuriy Kozlov put their heads in their laptops.  They have only just resurfaced, successful and triumphant with Roman having done most of the widget and dialogue integration work and Yuiry porting the Oxygen icon theme to OpenOffice's obscure icon format.

<a href="http://people.ubuntu.com/~jriddell/oo-icons.png"><img  src="http://people.ubuntu.com/~jriddell/oo-icons-wee.png" width="500" height="293" /></a>

Meanwhile  elite Kubuntu developer Roderick has been hard at work <a href="http://roderick-greening.blogspot.com/2009/06/usb-creator-kde-update-and-screenie.html">porting USB Creator to KDE</a> in preparation for a potential Kubuntu Netbook edition.

Want to be an elite Kubuntu developer?  Take a look at the <a href="https://wiki.kubuntu.org/Kubuntu/Todo">Kubuntu Karmic Todo list</a> and see if there's anything you could help with.  We have <a href="https://wiki.kubuntu.org/Kubuntu/Meetings">a meeting tonight</a> at 23:00UTC  in #kubuntu-devel where we'll be discussing Karmic work and the makeup of the Kubuntu Council, do come along and say hi.
<!--break-->