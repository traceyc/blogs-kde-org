---
title:   "Burns Supper and KDE Release Party"
date:    2011-01-14
authors:
  - jriddell
slug:    burns-supper-and-kde-release-party
---
Me and Colin and Tomas are having a Burns supper at my place in Edinburgh to celebrate 4.6, let me know if you want to come

<img src="http://people.canonical.com/~jriddell/kde-release-4.6-party.png" width="500" height="707" />
<!--break-->
