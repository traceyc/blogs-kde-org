---
title:   "Linuxcon Talks"
date:    2013-10-23
authors:
  - jriddell
slug:    linuxcon-talks
---
<a href="http://www.flickr.com/photos/jriddell/10440667405/" title="DSCF8731 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3778/10440667405_24bfd104fe.jpg" width="500" height="375" alt="DSCF8731"></a>
I gave a lightning talk at Linuxcon about KDE and Kubuntu highlighting some of the mega rollouts with Kubuntu such as the <a href"http://dot.kde.org/2013/10/02/ezgo-free-and-open-source-software-taiwans-schools">one which is about to take over Taipai</a>.

<a href="http://www.flickr.com/photos/jriddell/10440661086/" title="DSCF8729 by Jonathan Riddell, on Flickr"><img src="http://farm4.staticflickr.com/3707/10440661086_dd3c6a9557.jpg" width="500" height="375" alt="DSCF8729"></a>
Dan Shearer wins my award for nicest guy at the conference by introducing me to Cloudsoft who are interested in providing some cloud power to Kubuntu and generally making sure I've enough energy (I just came out of eye surgery so feeling fragile).  Here giving a talk on <a href="http://en.wikipedia.org/wiki/Zero-knowledge_proof">zero knowledge proofs</a>.

<a href="http://www.flickr.com/photos/jriddell/10440801855/" title="DSCF8733 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5491/10440801855_e029be7d8b.jpg" width="500" height="375" alt="DSCF8733"></a>
Thiago giving a talk about Qt project and its many successes (and a few challenges) over the two years of its existance. 