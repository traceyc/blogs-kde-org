---
title:   "It's a Beta"
date:    2012-10-04
authors:
  - jriddell
slug:    its-beta
---
<img src="http://people.ubuntu.com/~jr/Kubuntu-quantal-quetzal.png" width="400" height="533" />
Two weeks until the final thing and we need lots of testing of the beta, now featuring the latest and greatest KDE Platform and Applications 4.9.2.

<a href="https://wiki.kubuntu.org/QuantalQuetzal/Beta2/Kubuntu">Beta release page</a> lists the new features and the download and upgrade instructions.
