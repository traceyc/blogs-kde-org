---
title:   "New Umbrello Website"
date:    2013-08-13
authors:
  - jriddell
slug:    new-umbrello-website
---
Umbrello is the premier UML diagram tool for Linux desktops.  UML is a standard for making diagrams of software, useful for documenting and planning your programs.

I took over Umbrello in about 2002 and maintained it as part of my University final year project.  One of the first changes that I made to Umbrello was to make it a KDE project, it was the natural home.  But I never moved the website or mailing list, Sourceforge was good enough.

Many years later and I have not been contributing to Umbrello for some time but it fits a gap in the Free Software world and there's been enough interest to port it away from Qt 3 compatibility classes so it should exist in the move to KDE Frameworks 5.

And now thanks to Ralf Habacker it has finally been moved completely to KDE infrastructure with a lovely new website <a href="http://umbrello.kde.org">umbrello.kde.org</a>.

I've been surprised that nobody else has taken a similar path as I did a decade ago.  Academics love UML and students should love working on software people actually use, so using Umbrello as a university project will be a guaranteed A.  I won a big award recognising the best university software projects in Scotland when I did it.  So if you're in your last year of uni after this summer and looking for a dissertation, do consider Umbrello, plenty of bugs to fix, features to add and the whole Qt 5 porting to do.

<a href="screenshots/umbrello-2013.png"><img src="http://umbrello.kde.org/screenshots/umbrello-2013-wee.png" width="400" height="279" /></a>
