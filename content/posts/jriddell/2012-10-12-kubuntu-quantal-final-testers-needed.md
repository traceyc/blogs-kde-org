---
title:   "Kubuntu Quantal Final Testers Needed"
date:    2012-10-12
authors:
  - jriddell
slug:    kubuntu-quantal-final-testers-needed
---
<img src="http://people.ubuntu.com/~jr/kubuntu-boot.png" width="443" height="69" />

Less than a week until Kubuntu 12.10 is due out and the candidate images are needing testing.

Join us in #kubuntu-devel and get the images from <a href="http://iso.qa.ubuntu.com/qatracker/milestones/240/builds">the ISO tester</a>, run them in a range of configurations and report the results on the tester.

