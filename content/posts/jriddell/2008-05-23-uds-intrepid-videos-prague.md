---
title:   "UDS Intrepid Videos from Prague"
date:    2008-05-23
authors:
  - jriddell
slug:    uds-intrepid-videos-prague
---
Ubuntu Developer Summit is winding down here in the tallest building in Prague.  <a href="https://wiki.kubuntu.org/KubuntuUDSPragueSpecs">You can preview the Kubuntu specs</a> and I'll write more about them when I get back.  Now we're off to dance at an Ubuntu music jam.

On the <a href="http://www.youtube.com/user/ubuntudevelopers">ubuntudevelopers group in YouTube</a> there are interviews of some of the summit participants including <a href="http://www.youtube.com/watch?v=VXBQRpg11bg">me talking about Kubuntu</a>.

<img src="http://kubuntu.org/~jriddell/uds-prague-martin-scott-tonio-jonathan-tseliot-seele-wee.jpg" width="400" height="300" />
Some of your hard working Kubuntu developers pouring over the details of 4.1.
<!--break-->
