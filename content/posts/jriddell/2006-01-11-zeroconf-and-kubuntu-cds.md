---
title:   "Zeroconf (and kubuntu CDs)"
date:    2006-01-11
authors:
  - jriddell
slug:    zeroconf-and-kubuntu-cds
---
Coming soon to a Kubuntu near you, zeroconf support!  Thanks to Jakub for making KDE zeroconf support, now with all Free Avahi.

<img src="http://muse.19inch.net/~jr/tmp/zeroconf.png" width="530" height="460" />

For those who left comments on my last blog asking for Kubuntu CDs, I can't do much without a postal address (and remember you need a good excuse like doing a talk or a KDE stand at an exhibition, this isn't shipit).
<!--break-->
