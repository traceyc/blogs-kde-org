---
title:   "UDS Jaunty and Desktop Experience"
date:    2008-12-24
authors:
  - jriddell
slug:    uds-jaunty-and-desktop-experience
---
The Ubuntu Development Summit happened at Google the other week.  Large numbers of people got together to spend a week discussing the next six months in Ubuntu.  Lots of specs got written and you can find <a href="https://wiki.kubuntu.org/KubuntuJauntySpecs">the Kubuntu Jaunty specs here</a>.  

One of the most notable changes is the plan to change to using Package Kit, it's always a shame to move on from the hard work that has gone in before, but it makes the most sense for us to use a framework and apps that can be shared amongst many other distros.  <a href="http://web.mornfall.net/blog/farewell__44___adept.html">Hugs to Peter</a> for work much appreciated.

I'd like to see KDE 3 libs off the CD for Jaunty, and if possible Qt 3 too.  With Akonadi bringing in mysqld we'll need all the space we can find.  One of the last hangers on to Qt 3 is OpenOffice, anyone feel like reviving <a href="http://kde.openoffice.org/">OpenOffice KDE project</a> for Oxygen and Qt 4 support?

Mysql is going to be a hassle, as I say it takes up a lot of space for Akonadi and Amarok seems to need a newer version than the server team want to support, hopefully we can find ways to make everything work.

<b>Desktop Experience Team</b>

People sometimes complain that Canonical doesn't do enough original development, there's also grumbles I hear that one of the two major free software desktops is <a href="http://wingolog.org/archives/2008/06/07/gnome-in-the-age-of-decadence">getting a bit laid back</a> in its development.  So there's a new team at Canonical to fill those gaps by working on exciting new desktopy projects.  In order to make maximum use of its unique position the team works a bit differently from normal open source development.  Rather than ideas growing organically with technical implementation happening alongside the design, the ideas are first designed by a group of top talent designers.  Then another group of coders takes those designs and works out how they should be coded to get onto the desktops of users and fit in with the rest of the desktop software.  Not to be left out of this a <a href="http://webapps.ubuntu.com/employment/canonical_KDEV/">Qt spod or two</a> are set to be hired (probably early next year) to make sure KDE gets the same experience design excitement.

This will be a bit of a change from how Kubuntu has been made previously.  We've always been deliberately close to pure upstream KDE because that's what we love.  Now with the desktop experience team we'll be getting input from another place and the challenge is to keep them on a path which makes the two work together.  Their first piece of work was announced at UDS as <a href="http://arstechnica.com/guides/other/canonical-linux-notification-system.ars">desktop notifications</a>.  With the design goals announced the team spent their week discussing how that would work in the real world and looking at the technical implementation.  I got them <a href="http://aseigo.blogspot.com/2008/12/free-dektop-notifications.html">talking to Aaron</a> and hopefully they'll be able to be the extra hands needed to unify the "freedesktop" (i.e. gnome) notifications spec and make it good enough for all desktops and apps to use, a win for everyone.

Google don't let you take photos in their building which makes it hard to show UDS in its full.  Imagine lots of good looking people in heated debate.

<a href="http://www.kubuntu.org/~jriddell/uds-mtv/dscn2368.jpg"><img src="http://www.kubuntu.org/~jriddell/uds-mtv/wee-dscn2368.jpg" width="300" height="225" /></a>
Good beer can be found in the US, if you know where to look.

<a href="http://www.kubuntu.org/~jriddell/uds-mtv/dscn2365.jpg"><img src="http://www.kubuntu.org/~jriddell/uds-mtv/wee-dscn2365.jpg" width="225" height="300" /></a>
My word, is that a blue t-shirt?

<a href="http://www.kubuntu.org/~jriddell/uds-mtv/dscn2367.jpg"><img src="http://www.kubuntu.org/~jriddell/uds-mtv/wee-dscn2367.jpg" width="300" height="225" /></a>
Your friendly Kubuntu UDS team: rgreening, nixternal, seele, Tonio, Dancing Santa, Greg, moi.

Thanks to everyone who remembered by birthday, you're all lovely.
<!--break-->
