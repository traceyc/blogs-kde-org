---
title:   "Go Faster Lights"
date:    2005-10-09
authors:
  - jriddell
slug:    go-faster-lights
---
This nice machine got dropped off yesterday for some AMD64 goodness and uberfast compiles.

<img src="http://jriddell.org/photos/2005-10-09-amd64.jpg" width="400" height="533" class="showonplanet" />

But if that wasn't fast enough, turn down the ambient light and it sports triple shades of go faster LEDs through its groovy transparent box.

<img src="http://jriddell.org/photos/2005-10-09-amd64-go-faster-lights.jpg" width="400" height="533" class="showonplanet" />

Ridiculously cheap too at 300quid first-hand from ebay.
<!--break-->
