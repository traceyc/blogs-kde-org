---
title:   "Plasma 5 is Here! All Ready to Eat Your Babies"
date:    2014-07-15
authors:
  - jriddell
slug:    plasma-5-here-all-ready-eat-your-babies
---
<img src="http://www.kde.org/announcements/plasma5.0/plasma-5-banner.png" width="800" height="289" />

A year and a half ago Qt 5 was released giving KDE the opportunity and excuse to do the sort of tidying up that software always needs every few years.  We decided that, like Qt, we weren't going for major rewrites of the world as we did for KDE 4.  Rather we'd modularise, update and simplify.  Last week I clicked the publish button on the story for <a href="https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers">KDE Frameworks 5</a>, the refresh of kdelibs.  Interesting for developers.  Today I clicked the publish button on the story of the first major piece of software to use KDE Frameworks, <a href="https://dot.kde.org/2014/07/15/plasma-5.0">Plasma 5</a>.

Plasma is KDE's desktop.  It's also the tablet interface and media centre but those ports are still a work in progress.  The basic layout of the desktop hasn't changed, we know you don't want to switch to a new workflow for no good reason.  But it is cleaner and more slick.  It's also got plenty of bugs in it, this release won't be the default in Kubuntu, but we will make a separate image for you to try it out.  We're not putting it in the Ubuntu archive yet for the same reason but you can try it out if you are brave.  

Three options to try it out:

1) On Kubuntu, Project Neon is available as PPAs which offers frequently updated development snapshots of KDE Frameworks. Packages will be installed to /opt/project-neon5 and will <b>co-install</b> with your normal environment and installs to <b>14.04</b>.
<pre>
sudo apt-add-repository ppa:neon/kf5
apt update
apt install project-neon5-session project-neon5-utils project-neon5-konsole
</pre>
Log out and in again
2) Releases of KDE Frameworks 5 and Plasma 5 are being packaged in the next PPA. These will <b>replace</b> your Plasma 4 install and installs to <b>Utopic</b> development version.
<pre>
sudo apt-add-repository ppa:kubuntu-ppa/next
sudo apt-add-repository ppa:ci-train-ppa-service/landing-005
apt update
apt install kubuntu-plasma5-desktop
apt full-upgrade
</pre>
Log out and in again
3) Finally the <a href="http://files.kde.org/snapshots/">Neon 5 Live image</a>, updated every Friday with latest source from Git to run a full system from a USB disk. 

Good luck! let us know how you get on using #PlasmaByKDE on Twitter or posting to Kubuntu's G+ or Facebook pages.
