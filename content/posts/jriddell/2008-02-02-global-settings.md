---
title:   "Global Settings"
date:    2008-02-02
authors:
  - jriddell
slug:    global-settings
---
I got a new laptop recently, a Thinkpad R61, works decently well (no suspend, sound settings are strange but otherwise all good).  However now I have three computers (new laptop, desktop and laptop-held-together-with-duct-tape) it leads to the issue of how to move over settings and keep them the same on all three.  Wouldn't it be nice to be able to tell KDE to keep its settings on a remote server and download them when you move to a new desktop.  Today I wrote a quick mockup application to investigate the idea.  It keeps Konqueror's bookmarks in bzr and uploads them to Launchpad, then from another computer you can grab them again.  Of course there's a hundred issues to solve this properly, it shouldn't be a separate app but integrated with the KDE config framework, it needs to know when to sync and what to sync (no passwords unless its to somewhere very secure), apps need to know when to refresh their settings and some settings, like Amarok podcasts, are stored in hard-to-sync binary databases.  So just an idea for now, <a href="https://code.edge.launchpad.net/~jr/global-settings/mockup">code is in Launchpad</a>, let me know if you want to pick it up.

<img src="http://kubuntu.org/~jriddell/global-settings.png" width="519" height="408" />
<!--break-->
