---
title:   "Thanks!"
date:    2013-08-02
authors:
  - jriddell
slug:    thanks
---
Gosh I only expected a donation occasionally but we've had 19 people want to <a href="http://www.kubuntu.org/donate">thank Kubuntu enough to donate us</a> some money.  Thanks Coorey, Orlando, Marcelo, Fabian, Giancarlo, Juan, Javier, Jussi, David, Kevin, Jeremy, Łukasz, Christoph, Nadeem, Philippe, Daniel, Lieven, Christoph, Noam!