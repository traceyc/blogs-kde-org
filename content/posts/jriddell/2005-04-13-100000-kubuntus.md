---
title:   "100,000 Kubuntus"
date:    2005-04-13
authors:
  - jriddell
slug:    100000-kubuntus
---
If I read the <a href="http://www.acc.umu.se/technical/statistics/ftp/index.html.en">Swedish mirror's statistics</a> correctly that's almost 100,000 downloads of Kubuntu.  2TB of Kubuntu is a lot of Kubuntu.  It even beats the Ubuntu downloads. that's because it's listed top on the kubuntu.org download page.  

I have a filter in my .procmailrc file to send anything containing Kubuntu direct to my mailing list.  In the last week I've been getting a lot of e-mails directly which would normally have gone to kde-bugs caused by bug reports referencing Kubuntu use.  This doesn't mean Kubuntu is any more beastie ridden than anything else, it means we are getting a lot of users who then feed back to KDE (and directly to Kubuntu as appropriate), and that is good for everyone.
<!--break-->
