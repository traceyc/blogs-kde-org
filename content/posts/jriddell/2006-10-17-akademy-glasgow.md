---
title:   "Akademy in Glasgow"
date:    2006-10-17
authors:
  - jriddell
slug:    akademy-glasgow
---
The secret is lifted and <a href="http://dot.kde.org/1161090032/">Akademy 2007 will be in Glasgow</a>, organised by me and Kenny.  We'll be holding it earlier than previous Akademies, hopefully that means we'll be able to get some students from the US who haven't been able to attend before.  We had our first local team meeting last week and it's very exciting to have a bunch of enthusiastic helpers.  With debconf in Edinburgh and Guadec in England this means we'll have the three largest free software projects having their conferences in Britain next year, an exciting trend (and even cooler, two of them are run by Quakers).   Now the fun starts of opening bank accounts and organising a conference, anyone who wants to help with sponsors especially welcome.
<!--break-->