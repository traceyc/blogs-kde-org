---
title:   "Launchpadding, bzr --verify-signatures, Desktop Summit"
date:    2011-07-01
authors:
  - jriddell
slug:    launchpadding-bzr-verify-signatures-desktop-summit
---
<img src="http://people.canonical.com/~jriddell/mathematical-die.jpg"
width="250" height="168" />
Bazaar developers like to play against the odds, whatever the odds

This week I'm in Dublin for Canonical's mid-cycle meetup where our Ubuntu
developers work on their normal projects but with the advantage of face to face
contact with colleagues.  It's a bit like working from an office but without
the commute and with more Guiness.

Except I'm on rotation to the Bazaar team who this week are working alongside
the lovely Launchpad team.  Launchpad is Canonical's free software project
hosting site, with a focus on inter-project collaboration and code hosting with
the world's nicest revision control system Bazaar.  It's also Free software
openly developed (unlike Github, Google Code, Sourceforge etc).

So to broaden my programming horizons I've been fixing bugs in Launchpad. 
Setting up a local instance of Launchpad involves lots of faff with databases
and web servers but fortunately it's all scripted and I could get it up in 3
hours (which impressed the Launchpad devs who are more used to it taking a day,
the power of cloud computing there).  Although it's all Python, getting into the
code is not trivial, the Zope templates are easy enough but working out the
layers from database through model classes into view classes via interfaces and
security proxies can be daunting.  But it turns out entirely doable, bugs can be
fixed and documentation improved and test cases written.  All code gets peer
reviewed (using Launchpad of course) before being merged then sent off to a
cloud server which runs the complete code test suite (which takes about 4 hours,
painful but a good sign that all the code really is tested).  Once merged it
appears on the qastaging server where it gets QA checked (again) and only then
is it ready for deployment, safe in the knowledge that bugs have been fixed
rather than added.

<img src="http://people.canonical.com/~jriddell/LPTeamDublin2011.jpg"
width="640" height="426" />
Your friendly Launchpad team

<hr />

Meanwhile in bzr land I added a new command <b>bzr --verify-signatures</b> to
check the GPG signatures on commits.  This is needed for Ubuntu Distributed
Development, the grand project to turn all Ubuntu packages into Bazaar
branches.  I also added a GUI for QBzr.

<img src="http://people.canonical.com/~jriddell/qbzr-verify-signatures.png"
width="644" height="454" />

<hr />

And I'm all booked up to visit Berlin next month for some top talks on the Free
software desktop.

<img src="http://vizzzion.org/blog/wp-content/uploads/2011/06/DS2011banner.png"
width="333" height="110" />
<!--break-->
