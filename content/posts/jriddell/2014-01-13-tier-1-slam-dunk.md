---
title:   "Tier 1 is a Slam Dunk"
date:    2014-01-13
authors:
  - jriddell
slug:    tier-1-slam-dunk
---
<a href="http://dot.kde.org/2014/01/07/frameworks-5-tech-preview">KDE Frameworks 5</a> tech preview shipped last week and we've been packaging furiously.  This is all new packaging from scratch, none of our automated scripts we have for KDE SC.  Tier 1 is in our <a href="https://launchpad.net/~kubuntu-ppa/+archive/experimental/+packages">experimental PPA</a> for Trusty.  Special thanks to Scarlett our newest Kubuntu Ninja who is doing a stormer.
