---
title:   "What I Did on my Rotation"
date:    2011-11-08
authors:
  - jriddell
slug:    what-i-did-my-rotation
---
Canonical has a company scheme where after working there for a few years you can
rotate to work at another part of the company for 6 months.  Having worked on
the desktop team for over five years I decided to do a rotation to Bazaar.  My
hopes for this were to build up my own programming skills by learning more
Python and by experiencing different programming practices from the ones I'm
used to in KDE.

I started off with some fixes to the developer documentation.  This got me used
to the process that you can not commit directly to bzr's trunk, instead all
committers are required to make merge proposals on Launchpad, have those
approved by a fellow developer, then send it to a programme called Patch Queue
Manager which will integrate the patch and run the test suite to check
everything still works.  

Next I started fixing a few easy command line UI bugs, improving error messages
 or stopping exception output and so forth.  This got me into the world of
writing test cases.  Everything in bzr needs a test case, merge proposals will
not be accepted without them.  Like much of bzr I find that the test cases lack
API documentation and comments but it turns out they are easy enough to read
and similarly easy to write.  There are both internal test cases, which run a
small part of the code within bzr, and blackbox test cases which run a bzr
command.

Bazaar is the version control system used by top open source project hosting
site Launchpad so I was surprised to come accross <a
href="https://bugs.launchpad.net/launchpad/+bug/274578">a bug which prevented
bzr from talking to Launchpad properly on errors</a>. "This is really important
to fix. We need error reporting." said Jonathan Lange over 2 years before. 
Pleasingly I could fix it, very satisfying.  I had to learn about the hooks
mechanism in bzr which shows up some of the downside of Python, you have to
guess the arguments to send the hook.  But who needs API documentation when you
can just read the code? :)

Bazaar's main GUI is qbzr (which provides GUIs for individual commands) and
Bazaar Explorer (which provides a complete GUI).  I worked with Martin Gz to
make these two talk to the normal Ubuntu crash system, Apport, rather than
showing a nasty crash backtrace to the user.

Then I noticed that Bazzar Explorer has a lot of "Refresh" toolbar buttons about
the place, any time you make a change to the file you have to click one before
the UI will update.  Not very user friendly.  So I added file watchers about
the place to make it magically update.  Nifty, except that after release it
turns out this breaks horribly when doing some commands outside of Bazaar
Explorer, oops.  Quick fix and message to packagers, hang head in shame.

The first large feature I worked on was GPG signing of commits.  The
documentation for Bazaar promised that this was implemented and all you need do
was set the various options in the config file.  Alas it lied.  I fixed up the
documentation and started looking into the GPG python bindings, which turn out
to be completely undocumented on the Python side and surprisingly badly
documented on the C side.  Security critical code which is badly documented
seems scary to me, mistakes could easily be made which go unnoticed until it
appears on full-disclosure.  But I manage to implement signing and adding a GUI
to Bazaar Explorer being cautious as I go.

Bazaar has a scheme called patch pilot where we review patches submitted by the
community and help them on their way to being integrated.  I started out with
this by following John Meinel who can write code faster than I can write English
prose.  We made small changes to some patches and integrated them, we gave
feedback to newer patches that needed some work and we chased up contributors
who had not responded.  The barrier to entry in Bazaar is pleasingly small, if
you don't have the skills to write a perfect patch it's encouraged to say so
and someone else will finish it off.

Why, I wondered, is bzr (the command line UI to Bazaar) not translated?  There
were parts of gettext scattered around the code, and some code to extract
strings but it didn't get used.  Turns out this code was a half completed
feature that had never been taken to completion.  I finished off translations
by adding gettext()s throughout the code, ensuring tests still pass, fix the
installation of .mos and enable the generation of .pot.  This missed the 2.4
release so I'm still waiting to see how it works for 2.5, I suspect some
strings will be missing context needed to do a good translation and of course
the occationally technical output of bzr might need some thought on how to
translate but it should make bzr easier to use for non-English speakers.

Ubuntu Distributed Development is the project to put all of Ubuntu's packages
and history into Bazaar branches and change our packages processes to use
Bazaar.  This makes a lot of sense, the Ubuntu archive is already a primitive
revision control system (you upload for each new version, often its useful to
look at older versions).  This project has been a long time coming and is one
of the original reasons why Canonical started Bazaar back in the day.  It
suffers from a number of problems, notably the failure of quite a lot of
packages to import into Bazaar including currently the whole of KDE due to a
patch into openSUSE's bz2 package.  Also the quilt patch system we use tends to
clash with being held within a revision control system so you end up with diffs
of diffs.  I tend to think that would have been an easier win to import only
the debian/ packaging into Bazaar branches.

I tidied up the new Ubuntu Packaging Guide which is a guide to packaging with
UDD branches (named in the hope that UDD will soon become the definitive way to
do packaging).  I also added a new command bzr get-orig-source to make it
easier to do packaging in the current directory rather than a separate
directory as used by bzr builddeb.  I also added a hook to set the bzr
changelog from the debian/changelog entry which is the current behaviour with
debcommit.  I got mixed feedback on this so I added a config option to disable
it too.  I also tidied up some of the bzr-builddeb code by removing weird terms
like "larstiq" and removing acronyms by default.

My Python programming has improved a lot and I'm a convert to the cause of unit
tests.  Python is a fun and productive language but the lack of culture for
documenting APIs is disappointing and being dynamic it's that much easier to
make mistakes without realising it.  My productivity is nothing like as high as
others on the Bazaar team but it seems I'm better at improving (graphical and
command) user interfaces than my colleagues who can memorise internal data
structures trivially.  My six months is now up, I've enjoyed them and now I'm
looking forward to getting back into Kubuntu and KDE.  
<!--break-->

