---
title:   "Kubuntu and the state of free consumer software"
date:    2012-03-21
authors:
  - jriddell
slug:    kubuntu-and-state-free-consumer-software
---
Since I had to announce that Canonical was dropping support for Kubuntu from 12.11 (and then had to announce two days later they were dropping support for 12.04) I've been getting lots of people asking "is this the end of KDE?"

Of course it isn't, KDE is a vibrant community of people making useful and fun software.  

Recently <a href="http://ploum.net/post/what-if-ubuntu-were-right">Gnome have been noticing they're not winning either</a>.  There is a growing realisation that Canonical dropped Gnome some years ago.  [This is melodramatic overstatement, there are still a bunch of Gnome programmes used in Ubuntu Desktop but the workspace and webbrowser and e-mail client aren't.  Canonical is also using more Qt and less GTK.]  Articles like <a href="http://www.tomshardware.com/reviews/fedora-16-gnome-3-review,3155-27.html">GNOME 3: Why It Failed</a> don't really help the impression.

All this just highlights that we've been making free software for users for over 15 years and still not got out of the geek market.  This <a href="http://ploum.net/post/what-if-ubuntu-were-right#c56452">comment from mpt, canonical designer</a> highlights some reasons why: third party software, marketing to users but importantly to OEMs and their supply chains, online services, an SDK etc.

= So how can KDE remain relevant? =

Better design?  This was one of the comments from Ettrich when I first met him at a trade show ages ago.  We can call it "usability" but design is a slightly broader term of stepping back and working out why certain tasks are hard to do.

New markets?  Aaron and his Make Play Live company is making hardware devices with the Vivaldi tablet, that's exciting.  That's a small company.  Canonical is a large company and may well do the same, will be interested to see if either work.

Fill in the gaps!  There is a common meme that "we've achieved what we wanted 15 years ago", well free software in general has but KDE is really nowhere near a usable desktop.  We miss a decent web browser, our office suite is looking promising but still isn't much used, the plasma media centre has never got past an alpha stage, Kontact is losing popularity due to a bumpy transition to Akonadi.  There's lots to be working on!

App shop?  It's what users expect now. Ubuntu has one from Canonical. Muon in Kubuntu is decent and Plasma Active are working on one but they need to link up to third party suppliers.

Server!  We should be welcoming in OwnCloud and Kolab to the KDE community.  So far we've failed to do that.

Modularisation.  KDE Frameworks 5 is a great project, it might mean developers like Canonical start picking up bits of KDE technology as well as Qt.

Take advantage of Qt project.  It's there for the using, has some bumpy areas in the infrastructure (you can't download a patch without a whole clone) and social structures but we can help them.

= And how can Kubuntu remain relevant? =

Kubuntu has the <a href="http://lwn.net/Articles/455972/">world's largest Linux desktop rollout</a>.  I'll say that again.  Kubuntu, often mistaken as a mere derivative of Ubuntu, has more spread than any other desktop Linux.  Since I had to announce Canonical moving to focus on Unity I've been contacted by plenty of people saying they rely on Kubuntu.  Fortunately Kubuntu isn't going anywhere, that's the advantage of Free Software, when you have a cool community (and we have about the most active community of any part of Ubuntu) then it carries on.  We may even find new sponsors.

We like to show KDE at its best.  We are the only regularly released distro to ship only KDE Software on the desktop(*), others fill it in with GTK tools or their own config tools, we want to be all KDE.  [For the benefit of those journalists who don't understand 'regularly' we ship every six months, just like KDE SC]

(*)There is one bug in the above which is shipping LibreOffice, I think the time is right to move to Calligra, they are doing great stuff and need our support to get it to users.  They also are reputed to have better MS Office format importers than LibreOffice thanks to the work of KO.

We must remain part of Ubuntu, they are a great community for distros and we couldn't survive without them.  Kubuntu is often incorrectly called a "derivative" of Ubuntu but we are part of the Ubuntu family and we are one of their flavours which is just where we should be.

New markets?  Kubuntu Active is taking shape.  I'd love to have TV friendly media centre support too for example.

But do we need a new name? Kubuntu has never been a great name, it was actually a joke name made up by the original Ubuntu developers for the KDE side.  I wonder if a new name would give us a new lease of life like Calligra has.  Suggestions welcome :)
<!--break-->
