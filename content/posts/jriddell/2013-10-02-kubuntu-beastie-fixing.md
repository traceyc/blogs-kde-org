---
title:   "Kubuntu Beastie Fixing"
date:    2013-10-02
authors:
  - jriddell
slug:    kubuntu-beastie-fixing
---
Bugs are a pain, especially when a fiddly one comes along involving Python bindings, the number of people who understand SIP is pretty low.  So it's nice we were able to fix it quickly.  In <a href="https://bugs.kde.org/show_bug.cgi?id=325460#c4">the reporter's words</a>:

<i>Can I just say you guys are awesome? I've used KDE with various distributions for years (and with Kubuntu for the last ~2) but never reported a bug until now. I am amazed at how quickly it was acknowledged and came to some form of resolution.</i>

Wish we could do them all that smoothly!
