---
title:   "Dolphin Bazaar Integration"
date:    2011-08-12
authors:
  - jriddell
slug:    dolphin-bazaar-integration
---
At the Desktop Summit it was pointed out that Bazaar lacked integration with Dolphin.  Of course Bazaar's main GUI is the cross platform Qt based <a href="http://doc.bazaar.canonical.com/explorer/en/">Bzr Explorer</a>.  But there is also value in integrating with the tools that are natively part of a desktop.  So I've coded up Dolphin integration with the world's finest revision control system..

<a href="http://people.canonical.com/~jriddell/dolphin-bzr.png"><img src="http://people.canonical.com/~jriddell/dolphin-bzr-wee.png" width="400" height="318" /></a>

<a href="http://people.canonical.com/~jriddell/dolphin-bzr1-wee.png"><img src="http://people.canonical.com/~jriddell/dolphin-bzr1-wee.png" width="400" height="246" /></a>
<!--break-->
