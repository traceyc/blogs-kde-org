---
title:   "Variable Width Planet"
date:    2008-10-19
authors:
  - jriddell
slug:    variable-width-planet
---
You asked, and I deliver.  I made an alternative stylesheet for Planet KDE with variable width.  <em>View -> Use Stylesheet -> Variable Width</em>.  My HTML foo is weak so it has mysterious gaps at below the header and above the footer for no reason I can see, plus the top and bottom shadow effects are semi-transparent and overlap in ways I wouldn't want.  But it should be good enough for those wanting something that fits on an N810 screen.

Here's some pictures to keep you amused.

<img src="http://farm4.static.flickr.com/3227/2892867382_7ca9d2168d.jpg?v=0" width="500" height="375" alt="Edinburgh Old Observatory, Calton Hill" />
Edinburgh Old Observatory, Calton Hill

<img src="http://farm4.static.flickr.com/3126/2892871532_fcaa0b9d3a.jpg?v=0" width="375" height="500" alt="West Register House, Edinburgh, National Archives of Scotland" />
West Register House, Edinburgh, National Archives of Scotland.  Every Scot for ever born in this picture. Rows from bottom: births, deaths, marrages, historic parish records (pre-1850s).

<img src="http://farm4.static.flickr.com/3147/2922127458_7c84969537.jpg?v=0" width="500" height="375" alt="Forth Bridge" />
The Forth Bridge

<img src="http://farm4.static.flickr.com/3059/2922127464_ec93b5491b.jpg?v=0" width="500" height="375" alt="Forth Bridge Absail" />
Me jumping off the Forth Bridge
<!--break-->


