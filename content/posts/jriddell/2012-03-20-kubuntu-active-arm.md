---
title:   "Kubuntu Active on ARM"
date:    2012-03-20
authors:
  - jriddell
slug:    kubuntu-active-arm
---
I've been playing with getting Kubuntu Active on ARM.  Getting a working ARM setup is a lot like getting a working Linux desktop setup when I started in 1999.  It's unclear what computer you need, it's unclear what install image you need, it's unclear how you install it and then it doesn't work and it's unclear how you debug it.  For some unknown reason Ubuntu Desktop images from precise don't work on my Pandaboard but from oneiric they do.  Ubuntu Server from precise seems to work so I've installed that and installed the Kubuntu Active packages on top of it.  Maybe soon we'll have working Kubuntu Active images on ARM.

The application in this photo is Muon Installer QML which is a shiny new app installer being written by Aleix Pol and now available from the new <a href="https://launchpad.net/~cyberspace/+archive/cyber-stuff">Cyberspace PPA</a> which is going to contain daily builds of various KDE projects.

<img src="http://starsky.19inch.net/~jr/tmp/DSCF6658.JPG" width="400" height="533" />
Weel are ye wordy o'a grace, As lang's my ARM.
<!--break-->
