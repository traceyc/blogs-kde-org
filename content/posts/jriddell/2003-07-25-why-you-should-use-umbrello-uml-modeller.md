---
title:   "Why you should use Umbrello UML Modeller"
date:    2003-07-25
authors:
  - jriddell
slug:    why-you-should-use-umbrello-uml-modeller
---
I'm giving <a href="http://www.ukuug.org/events/linux2003/prog/abstract-JRiddell-1.shtml">a talk next week at the <a href="http://www.ukuug.org/events/linux2003/">Linux 2003 conference</a> in Edinburgh about UML Modelling with <a href="http://uml.sf.net">Umbrello</a>.  Since I'm not too sure what to say I'm writing notes here in the hope that I'll be struck by inspiration and maybe some useful comments.

Free Software development has a reputation for being done without the software engineering process that I just spent four years studying at university and which is probably used by most software companies.  [insert waterfall model or spiral model diagram here].  This of course is a good thing, projects usually start as a hack to serve a need.  If the result is useful the project will continue to be developed by those who find it useful either the origional author or someone else.  If it isn't useful it will dissappear or if it's useful but the implementation is flawed then it might be re-written from scratch.  It has a Darwinian element to it.  You can't do that in commercial software development where results are more urgent.

But taking on software modelling (a suit and academic-friendly work for drawing diagrams) doesn't mean taking on the whole commercial software development process.

Maybe here I should ask what process the people present do use for software development

UML is the Unified Modelling Language (not to be confused with User Mode Linux), an industry standard for drawing diagrams of software developed in the late 1990s from various competing standards.  It gives you a level of abstraction away from the code that is very hard to get sitting at a text editor.  It is programming language independant but is heavily object orientated and is probably best suited to Java and C++ due to some similarities of syntax.

The diagram types in UML are:
 - at the highest level is use case diagrams which show how the program is used by people and other programs.
 - most common are class diagrams which show classes, their attributes (variables) and operations (methods) and the relationships between the classes
 - sequence diagrams show the flow of control for a given use case (i.e. the classes and the methods they call)
 - state and activity diagrams for finite state machines
 - component and deployment diagrams for high level view (i.e. above the class level view) of the system
 [have images for each of these]
 
 What are UML diagrams good for:
  - planning your program requirements (use case diagrams)
  - planning your program class structure
  - shareing your plans with other developers, being a standard language everyone can understand your scribbles
  - documenting your current program structure, very useful to refer to when programming, helps people new to the code base become familiar with it

  
Umbrello:
  - UML diagramming tool 
  - Stable version 1.1.1 is available now from <a href="http://uml.sf.net">http://uml.sf.net</a>, next version will be part of KDE 3.2  
  - Hopefully bringing UML modelling to Free software development
  - Now implements all the diagrams of UML and most of the features of them
  - Unlike general diagramming programmes such as Dia or Kivio it knows the rules for UML and only offers suitable widgets and association types
  - Can import C++ and export to a growing range of languages
  - File format (loosley) based on XMI, an XML language for UML
  - Currently in use not only to document and develop Umbrello but also by Quanta developers, NASA and Nextphere AS
  - Competition is ArgoUML (nasty Java swing interface), and proprietry programs such as Together (evil software patents) and Rational Rose (never used it)
  
  Nice screenshots of Umbrello
  
  Copyleft GNU FDL, Jonathan Riddell, July 2003
  