---
title:   "Gnome Founder Leaves"
date:    2013-03-05
authors:
  - jriddell
slug:    gnome-founder-leaves
---
Planet Gnome is headed by a curious blog today, <a href="http://tirania.org/blog/archive/2013/Mar-05.html">Miguel moving to Mac</a>.

"To me, the fragmentation of Linux as a platform, the multiple incompatible distros, and the incompatibilities across versions of the same distro were my Three Mile Island/Chernobyl."

No mention that it was him who started the fragmentation.

I occasionally get moaned at for having a policy of removing unconstructive blogs from Planet KDE.  This sort of blog confirm for me why it's not healthy to let anyone post anything on our Planet.

p.s. I'm single and eligible incase any cute slashdot ladies in the 30±5ish age bracket are interested :)