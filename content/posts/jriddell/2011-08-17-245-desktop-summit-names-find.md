---
title:   "245 Desktop Summit Names to Find"
date:    2011-08-17
authors:
  - jriddell
slug:    245-desktop-summit-names-find
---
The <a href="http://people.canonical.com/~jriddell/desktop-summit-akademy-guadec-group-photo-2011.html">Desktop Summit 2011 Group photo</a> needs your help!  Please find all the unnamed people (most of them Gnomers) and add their names, irc nicks and affiliations to <a href="http://notes.kde.org/desktop-summit-akademy-guadec-group-photo-2011">this etherpad document</a>.  Type "xxx" for the unnamed ones.

