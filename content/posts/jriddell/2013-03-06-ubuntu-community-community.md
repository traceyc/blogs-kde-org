---
title:   "An Ubuntu Community that is a Community"
date:    2013-03-06
authors:
  - jriddell
slug:    ubuntu-community-community
---
If like <a href="http://doctormo.org/2013/03/06/ubuntu-membership-2/">Martin Owens you're feeling the lack of Ubuntu community</a> and wanting an Ubuntu community that cares about everyone's contribution, doesn't make random announcements every couple of days that have obviously been made behind closed doors and cares about a community made upstream desktop (and err.. whole graphics stack), you'd be very welcome here at Kubuntu.  Join us in #kubuntu-devel

<img src="http://jriddell.org/me5.jpg" width="373" height="375" />
The Jonathan Riddell: as featured in Slashdot.
