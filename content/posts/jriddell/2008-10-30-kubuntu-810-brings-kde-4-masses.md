---
title:   "Kubuntu 8.10 Brings KDE 4 to the Masses"
date:    2008-10-30
authors:
  - jriddell
slug:    kubuntu-810-brings-kde-4-masses
---
Kubuntu 8.10 is out!  This release is the first fully supported one with KDE 4.

<img src="http://people.ubuntu.com/~jriddell/kubuntu-810-ibex-oxygen.png" width="300" height="500" />

If you're the cautious sort and you're not sure if KDE 4 is for you yet then try out the live CD to give it a test.  You are welcome to stay with 8.04 and the fully supported KDE 3 if you want to stay with what you know.

But where would be the fun in that?  <a href="https://help.ubuntu.com/community/IntrepidUpgrades/Kubuntu">Upgrade now!</a> :)

Or <a href="http://people.ubuntu.com/~jriddell/tmp/torrents/">help seed the torrents</a>.

Or <a href="http://nl.releases.ubuntu.com/kubuntu/intrepid/">try the</a> <a href="http://us.releases.ubuntu.com/kubuntu/intrepid/">first mirrors</a> <a href="http://se.releases.ubuntu.com/kubuntu/intrepid/">to have caught up</a>.

Hugs to everyone who helped out testing on the late shift last night as we had to do a night-before rebuild of the CDs and DVDs.

Infact hugs to everyone who has helped with this cycle, we've seen a great expansion in what the community can take on with much of the packaging work being a full community process now.  apachelogger took the lead in that, but vorian and stdin and awen and jontheechidna and devfil all made it possible.  Then there's rgreening who turned up one day and fixes anything anyone asks him to since.  There's Arby and yuiry who also fill my inbox with bug triage and ScottK who seems to control large parts of MOTU and backports.  Best of all mornfall wrote us a whole new Adept so we could actually have package management, he doesn't even run Kubuntu but he was lovely enough to work with our schedule, what a guy.  NCommander fixes every compile failure he can find. nixternal and jjesse got the docs updated and seele is always the best person to consult on cookery^Wusability.  Many more wonderful people too that I'd bore you to name, but they're the best community you are very welcome to <a href="https://wiki.kubuntu.org/HelpingKubuntu">come and join</a> as we start thinking about <a href="https://wiki.kubuntu.org/KubuntuUDSJauntySpecs">the next six months</a>.

<a href="https://edge.launchpad.net/~kubuntu-members/+mugshots"><img src="http://people.ubuntu.com/~jriddell/kubuntu-faces.png" width="200" height="438" /></a>
The Kubuntu community
<!--break-->
