---
title:   "New Kubuntu Dev Tools"
date:    2013-08-30
authors:
  - jriddell
slug:    new-kubuntu-dev-tools
---
There's been some tools added to the Kubuntu developer workflow recently which are handy for current developers and means anyone interested in helping out can find jobs to be done earily.

Top of them is <a href="https://trello.com/kubuntu">the Kubuntu Trello board</a>, a replacement todo list/kanban for the Work Items we used to keep on specifications.  Using the latest on HTML5 drag and drop whizzyness it lets you see what jobs need doing and who's doing them.  

I've also set up <a href="http://qa.kubuntu.co.uk">qa.kubuntu.co.uk</a> with a few handy items...

Today I've updated the <a href="http://qa.kubuntu.co.uk/kubuntu-buildstatus/kubuntu-buildstatus.html">Kubuntu build status</a> script that Felix Geyer wrote.  It shows which packages have not build or build with new symbols and which packages have a newer upstream version.

The <a href="http://qa.kubuntu.co.uk/cdimage-size/">cdimage size</a> page, also by Felix, shows what packages are on the images and taking up precious space.  Naughty Oxygen with all those beautiful icons takes up 28179 kB.

Then there's the <a href="http://qa.kubuntu.co.uk/ninjas-status/build_status_4.11.1_saucy.html">ninja status</a> page which shows the current build of KDE SC as we put it into our Kubuntu Ninjas PPA ready for each release.  Look out for <a href="http://qa.kubuntu.co.uk/ninjas-status/build_status_4.11.1_saucy.html">4.11.1 as it starts to appear</a>.  We also coordinate KDE SC builds on <a href="https://notes.kde.org/p/kubuntu-ninjas">a notes.kde.org page</a>.

A change made last cycle to the Ubuntu process is to put everything into -proposed first and only move to -release when it has been built at least as installable as the current version.  <a href="http://people.canonical.com/~ubuntu-archive/proposed-migration/update_excuses.html">update excuses</a> lists some reasons why it may not have copied.  Naughty naughty qtwebkit failing on ARM!

And then there's the <a href="http://goo.gl/vHRjj">milestoned Kubuntu bugs</a> which I use for bugs I care about being fixed before release.

So if you're interested in helping out with Kubuntu there's plenty in that lot to get you started, join us in #kubuntu-devel on freenode and say hi.


