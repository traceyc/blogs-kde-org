---
title:   "Kubuntu at LinuxTag"
date:    2005-06-22
authors:
  - jriddell
slug:    kubuntu-linuxtag
---
Kubuntu at LinuxTag in Germany today.  Eagle eyed attenders may have seen Amu on the KDE stall handing out limited edition Kubuntu CDs.  These have dragons on the cover so are assured to be cool.

We also released <a href="http://kubuntu.org/hoary-koffice-14.php">packages and a Live CD for KOffice 1.4</a>.  Whether KOffice should replace OpenOffice in breezy is the big question.

Corey Burger <a href="http://www.advogato.org/person/Burgundavia/diary.html?start=5">brought up the issue of large source packages</a>.  This has been discussed within KDE and may yet be discussed again before KDE 4.  KDE's repository is sorted into modules each with lots of different programmes, kdelibs has (most of the libraries), kdebase has konqueror, konsole, kcontrol and others, kdepim has all the Kontact programmes, kdesdk has a strange collection of scripts and development programmes, etc.  It all causes a bit of a problem for packagers especially when one small change has to be made to a package.  To KDE developers though it saves multiple SVN checkouts and more importantly multiple ./configure runs.  KDE i18n used to be one muckle big 250Meg source package, that has been split up into individual languages.  Generally the advantages to KDE of SVN modules with numberous related KDE programmes in them outweight the problems to packagers so I suspect they will stay.

<a href="http://www.kstuff.org/lt2k5/day1/"><img class="showonplanet" src="http://muse.19inch.net/~jr/tmp/800.IMG_1865.JPG" width="400" height="300" /></a>
<!--break-->
