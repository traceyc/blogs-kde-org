---
title:   "Plasma and KDE Applications 4.8 on Kubuntu"
date:    2012-01-27
authors:
  - jriddell
slug:    plasma-and-kde-applications-48-kubuntu
---
<a href="http://www.kubuntu.org/news/kde-sc-4.8.0">Kubuntu has packages for 4.8</a> bringing updates to Plasma workspaces and a load of KDE Applications.

To quote a nice user <a href="http://lists.kde.org/?l=kde-devel&m=132752020906824&w=2">posting on kde-devel</a>

"I upgraded to Ubuntu's Precise Alpha 1 a few days ago. After the upgrade completed, I tried out KDE 4.8 RC 2. It worked great until the final release of KDE 4.8 Final. KDE 4.8 Final is even better than the RC!"

or <a href="http://lists.kde.org/?l=kde-devel&m=132761891304607&w=2">later in the same thread</a>

"KDE 4.8 is rocking for me too.Using the Kubuntu PPA's on Sandy Bridge system and it's just lightning fast to do anything. "
<!--break-->
