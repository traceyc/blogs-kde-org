---
title:   "7.10, Awooga"
date:    2007-10-19
authors:
  - jriddell
slug:    710-awooga
---
<img src="http://kubuntu.org/images/gutsy-release.png" width="600" height="120"
     alt="Kubuntu 7.10 - Gutsy Gibbon On the Streets"
     style="margin: 1ex; padding: 1ex; border: 1px solid grey" />

<a href="http://kubuntu.org/announcements/7.10-release.php">Kubuntu 7.10</a> was released today.  Much jubilation and celebrations all around.  

I think we can safely say we're the first distro to include KDE 3.5.8 and KDE 4 Beta 3.  

We're also, as far as I know, first to include <a href="http://www.qyoto.org">Qyoto</a>, the C# bindings for Qt.  And at the last minute I managed to sneak in Jambi Qt Java bindings using the all free and GPLed Icedtea version of (almost) Java.

Slightly controversial perhaps, we're included Dolphin as the default file manager, there's Strigi as the default search engine and KDE PIM comes from the Enterprise branch (you can get plain 3.5.8 from backports soon if that works better for you).

This has been my best year yet as a Google Summer of Code mentor and mhb has made Kubuntu a lovely frontend to restricted manager (yay for easy install of broadcom wifi) and a .deb installer (should you wish to <a href="https://bugs.launchpad.net/ubuntu/+source/kdelibs/+bug/153500/comments/29">install that fix to kopete</a>).   

Bugs alas crop up, so if you're upgrading from 7.04 (Feisty) make sure you have Recommended Updates (feisty-updates) and Pre Released Updates (feisty-proposed) enabled and all packages up to date.

Thanks to all the wonderful people who helped out with this release.  I'll be upping my carbon footprint by going to <a href="http://www.fosscamp.org/">FOSSCamp</a> in Boston next weekend and the Ubuntu Developers Summit which follows.  If you're in the area do drop by.  If you have things which you think we should be discussing, let me know.
<!--break-->

