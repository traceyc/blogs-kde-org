---
title:   "Kubuntu Todo"
date:    2010-11-10
authors:
  - jriddell
slug:    kubuntu-todo
---
UDS in Florida is over, we flew home and left the sun behind.  The main outcome from the conference is a <a href="https://wiki.kubuntu.org/Kubuntu/Todo">long Kubuntu Todo list</a>.  

In the packaging section we naturally want the latest bits of KDE and Qt.  We'd like to try raster in Qt.  We're going to try gtk-oxygen theme.  Scott is going to look at automatic bulids on his ARM machines.  GStreamer will be investigated.  We'll be looking at whether it makes sense to packaging Owncloud and Plasma Media Centre.  Then there's Project Neon, a top secret relaunch of the project to make daily builds of Qt and KDE.

For coding we'd like to get Muon into main, get those Samba sharing patched into shape and upstreamed, make sure the installer wifi setup page gets implemented.

Out of the Kontact session Kolabsys kindly say they can do their test process on our Kontact packages.  We also want to investigate Lionmail.  On the Kolab server side there's some more work to get patches integrated.  

Kubuntu Mobile wants to use NoDM, slimmed down packages, and maybe packages for Kontact Mobile and Freoffice.  We might even get kdelibs built with the mobile profile.

There's a bunch of work needing done on our docs.  There's also bits of artwork needing done.  

Much of the above needs discussions with upstreams and testing to see if they're actually sensible to do.  It's going to be a busy six months for Kubuntu.  Do join us if you want to help.

<img src="http://farm5.static.flickr.com/4015/5142862846_3a3235dd47.jpg" width="500" height="375" />
Switcheroo

<img src="http://farm2.static.flickr.com/1063/5142261027_97e32d985f.jpg" width="500" height="375" />
Jussi bathed in heavenly light
<!--break-->
