---
title:   "Debconf Over"
date:    2007-06-24
authors:
  - jriddell
slug:    debconf-over
---
Debconf finished in Edinburgh.  Thursday night saw a ceilidh which went surprisingly well despite the poor ratio of lasses to laddies.  msp's talk on the Debian KDE Extras team was interesting and I hope I didn't ramble too much in my comments on why the Kubuntu relationship is the way it is.  There was also a KDE packagers BoF early on Saturday which gave some interesting history into the current team.  No volunteers to package Jambi yet though.

This was my first Debconf, it seemed quite loosly structured compared to Akademys or UDSs and there were lots and lots of people sitting at their computers at any one time.  A healthy number of KDE desktops present too.

<a href="https://gallery.debconf.org/debconf7/group"><img src="http://jriddell.org/photos/wee/2007-06-20-debconf-group.jpg" width="300" height="185" /></a>

Lots of Geeks

<a href="http://jriddell.org/photos/2007-06-22-debian-kilts.jpg"><img src="http://jriddell.org/photos/wee/2007-06-22-debian-kilts.jpg" width="300" height="185" /></a>

Lots of kilts.  Photos of oor Paul Sladen in plaid (budget kilt) surprisingly absent from the internet.
<!--break-->
