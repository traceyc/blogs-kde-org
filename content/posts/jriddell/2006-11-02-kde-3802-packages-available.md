---
title:   "KDE 3.80.2 Packages Available"
date:    2006-11-02
authors:
  - jriddell
slug:    kde-3802-packages-available
---
Packages of <a href="http://kubuntu.org/announcements/kde4-3.80.2.php">KDE 3.80.2 are available for Kubuntu Edgy</a>.  In this second developers snapshot of KDE 4 full sessions seem to be running nicely.  Lots of things crash but it's a good start.  As a distro developer one of my current worries is how KDE 3 apps will run under KDE 4, with these packages it works except for kioslaves which complain about klauncher breaking.  So if I can work that one out, it should be OK.

<a href="http://kubuntu.org/~jriddell/kde4-screenshot.png"><img src="http://kubuntu.org/~jriddell/kde4-screenshot.png" width="320" height="240" /></a>
<!--break-->