---
title:   "KDE and Kubuntu at Linuxcon in Edinburgh"
date:    2013-10-21
authors:
  - jriddell
slug:    kde-and-kubuntu-linuxcon-edinburgh
---
Linux has come to my home with Linuxcon in Edinburgh.  Me and Kenny C are manning the KDE stall showing off KDE software running Krita on a desktop (bling), Plasma Active on a Nexus (swish, very popular), KDE Software on Windows (ooh I didn't know you did that) and giving away lots of lanyards.  If you're here do come over and say hi.

<a href="http://www.flickr.com/photos/jriddell/10405507373/" title="DSCF8716 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7322/10405507373_641ea58ac1.jpg" width="500" height="375" alt="DSCF8716"></a>
Kenny C shows off Krita to some enthusiastic and non-biased stall visitors

<a href="http://www.flickr.com/photos/jriddell/10405356913/" title="DSCF8721 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7402/10405356913_2c5e375fb3.jpg" width="500" height="375" alt="DSCF8721"></a>
Matthew Garrett giving a talk in the keynote room
