---
title:   "Montreal and Prizes"
date:    2005-11-15
authors:
  - jriddell
slug:    montreal-and-prizes
---
Cycling around Montreal on the back of a Brompton me and Sladen discovered you can get <a href="http://jriddell.org/photos/2005-11-10-ubz-jonathan-pumpkins.jpg">very very large pumpkins</a> very cheaply after Halloween.  Montreal is on an island and has long bridges that take a good while to cycle over.  For that authentic North American experience we went to college (called a school) and hung out with the anime club who have nice sofas.  We discovered the original buckyball and found that their artworks make for very challenging climbs.

<img src="http://jriddell.org/photos/2005-11-10-ubz-paul-jonathan-tower.jpg" width="400" height="300" class="showonplanet" />

and <a href="http://jriddell.org/photos/2005-11-10-ubz-jonathan-tower.jpg">slightly more terrified at the top</a>.  Also available in <a href="http://jriddell.org/photos/2005-11-10-ubz-paul-tower.avi">full motion sladen-video</a>.

We also discovered this in the local Quaker meeting house which somewhat spoke to me, the only thing missing is "share your software".

<a href="http://jriddell.org/photos/2005-11-10-ubz-community.jpg"><img src="http://jriddell.org/photos/2005-11-10-ubz-community.jpg" width="400" height="300" class="showonplanet" /></a>

But back to matters Kubuntu.  A nice Kubuntu fan with unlimited bandwidth added a new German download mirror and reported an impressive >700GB of traffic in the two days since adding it.  The <a href="http://business.bostonherald.com/technologyNews/view.bg?articleid=112160&format=text">Boston Herald installed Kubuntu</a> even if they didn't get the pronounciation quite right.  And we won 3rd place in Best Debian Distribution at the <a href="http://www.linux-community.de/Neues/story?storyid=18337">Linux New Media awards</a>.  (That's a good two places below where I'd like of course, and isn't the lack of Debian on that list a bit of an insult?).  In his acceptance speech Mark Shuttleworth said how impressed he was at the community drive behind KDE.  KDE sister project Kolab won best groupware server.

<!--break-->
