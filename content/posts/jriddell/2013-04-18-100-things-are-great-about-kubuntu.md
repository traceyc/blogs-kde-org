---
title:   "100 things that are great about Kubuntu?"
date:    2013-04-18
authors:
  - jriddell
slug:    100-things-are-great-about-kubuntu
---
<a href="https://plus.google.com/105682810766335536482/posts/Qb4JxkieNHQ">Google+ topic to name 100 things that are great about Kubuntu</a> 

"Community involvement.﻿" 

"I like KDevelop, KDE and Debian packages combined with the fast update service of Kubuntu.﻿"

"I have to hand it to the Kubuntu package team, they really get the updates out fast"

"Everything is highly customizable."

Add your reason today :)

<img src="http://people.ubuntu.com/~jr/tmp/kubuntu.png" width="207" height="294" />
