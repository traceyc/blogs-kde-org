---
title:   "Kubuntu Lucid, LTS on its Way"
date:    2009-12-01
authors:
  - jriddell
slug:    kubuntu-lucid-lts-its-way
---
The Ubuntu Developer Summit happened in Dallas last week with 200 developers from every part of Ubuntu as well as upstreams and hardware vendors around.  Naturally the best looking of the lot was the Kubuntu contributors who turned up to discuss the next six months in the world's finest KDE distribution.  The Lucid Lynx will be a Long Term Support edition and it's exciting that KDE 4 is now at a stage of maturity where this will be possible to do for the first time.  LTS means fixing, completing and assuring over and above any new features.  The Doctor is in the house.

<img src="http://farm3.static.flickr.com/2609/4121163294_7ed4642965.jpg" alt="Kubuntu Contributors" width="333" height="500" /><br />
Kubuntu Contributors: Julian Edwards (Soyuz), Roman Shtylman (Installer Bling), Roderick Greening (The Creator, USB style), Michael Casadevall (fixes your broken Arm), Jonathan Riddell (moi), Ralph Janke (5 a day, every day), Aurelien Gateau (Desktop Experience), Jussi Schultink (IRC Council), Mackenzie Morgan (s/Abort/Stop/), Scott Kitterman (negotiator)

So what's in store?  Read the <a href="https://wiki.kubuntu.org/KubuntuLucidSpecs">specifications for full details</a>.

Under Packaging, of course we want KDE 4.4 and Qt 4.6.  We will review all the patches and make sure any we carry have a clear rationale (this is <a href="https://wiki.kubuntu.org/Kubuntu/LucidPatchReview">mostly done already</a>).  We'll have a policy of discussing any patches with upstream and assume to follow their wishes where we want to add any.  We won't go packaging beta applications unless upstream agree.  KOffice 1 will die but KOffice 2 may not all yet be ready for main.  We'll look at the Pulseaudio changes to Phonon (we don't use Pulseaudio in Kubuntu by default currently but many Gnome apps do so it's not something which is going away).  We'll get Virtuoso working with Nepomuk and we'll get policykit-1 in which means KPackagekit can be updated to the latest version.

Kubuntu Netbook will no longer be a Preview thanks to lovely work from the Plasma Netbook dudes.  We need to make sure kubuntu-netbook and kubuntu-desktop work sensibly when both are installed.  Apps and UIs will be reviewed for Netbook suitability.

On the development front I hope to make good progress on printer config, there's a rewrite of update-notifier-kde in the works, more than one person has promised to get SMB file sharing working and we want to get touchpad configuration easy.

In Ayatana Integration the Canonical Desktop Team wants to adopt KDE's new systray tray spec (or notifier item as I believe its now called) for Gnome.  They want to change it so the icon menu will be rendered natively with items sent over Dbus and we had a meeting with the Plasma folks to thrash out the details of that.  

We have a new website layout in the works, we need to poke sysadmins and security team to get that working.  We'd like to crystalise some marketing (besides being like Ubuntu Desktop but blue) and work out our target users and get some slogans.  We'd also like to make use of the Ubuntu loco teams by finding Kubuntu contacts for each one.

On bug triage we'll not DrKonqi for upstream KDE apps to stop our poor bug triagers being flooded with upstream bugs (it has the obvious downside that KDE bug triagers will get any bugs which are our fault but hopefully that'll be kept to a minimum).  

For translations we got the Launchpad devs to promise proper QA reporting so we can be sure Launchpad is helping and not hurting our translations.

If you want to help out, take a look at the <a href="https://wiki.kubuntu.org/Kubuntu/Todo">Todo list</a> then come and find us on #kubuntu-devel.

<img src="http://farm3.static.flickr.com/2628/4120389545_eeb1625f87.jpg" width="500" height="333" /><br />
The Ubuntu Family in Dallas

Dallas is a curious place.  They don't say "hello" they say "how are you", I didn't work out if it was a question or just a greeting.  You can't get anywhere without a car although mostly we took Limos because mere taxis wouldn't be luxurious enough.  Talking of luxury, rooftop swimming pool and nobody uses it except me!  After a week of hard discussions, early mornings and sharing a toothbrush I've escaped to the highlands and its failing internet, time for some cabogganing.
<!--break-->
