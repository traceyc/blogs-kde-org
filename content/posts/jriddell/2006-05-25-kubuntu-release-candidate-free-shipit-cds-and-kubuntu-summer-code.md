---
title:   "Kubuntu Release Candidate, Free Shipit CDs and the Kubuntu Summer of Code"
date:    2006-05-25
authors:
  - jriddell
slug:    kubuntu-release-candidate-free-shipit-cds-and-kubuntu-summer-code
---
The <a href="https://lists.ubuntu.com/archives/ubuntu-announce/2006-May/000081.html">release candidate for Kubuntu 6.06 LTS</a>, also known as Dapper, is out now for your downloading pleasure (please use bittorrent).

The LTS stands for Long Term Support which means the Ubuntu team and Canonical will support it for 3 years on the desktop and 5 on the server.  The focus for the release has been fixing all the bugs, and while we havn't got all of them it is feeling really polished.

Look out for the awesome new Ubiquity installer on the desktop CD (formerly called live CD), that means you only needs 1 CD for live and install which allows for <a href="https://shipit.kubuntu.org/">free Kubuntu CDs though Shipit</a> go and order yours now!

The best thing about this long Dapper release cycle has been the growth in the Kubuntu developer community, we have a really healthy number of people around the channel (and in real life too if you were at LinuxTag).  Lets hear it for mornfall for making Adept 2.0, Lure for fixing up knetworkconf, Sime for the new X configuration tool in Guidance and fixing System Settings, kwwii for the bling artwork, jjesse and robotgeek for the Kubuntu Desktop Guidance and other documentation, freeflying for CJK support, raphink and Tonio our top packagers and our team of bug triagers like sealne, Hobbsee, Odyx and bddebian.  If you want to help out or tell me how you got on installing the release candidate give me a ping on Freenode IRC (Riddell).

In other news we got four summer of code students for Kubuntu working on QtParted, a floppy/USB disk formatting tool, OEM installer and Guidance configuration.

<img src="http://kubuntu.org/images/kubuntu-mug.jpg" class="showonplanet" width="300" height="256" />
<!--break-->
