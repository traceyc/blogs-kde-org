---
title:   "Precise Ubuntu Developer Summit "
date:    2011-11-02
authors:
  - jriddell
slug:    precise-ubuntu-developer-summit
---
Ubuntu Developer Summit is happening in cloudy Florida.  The Kubuntu team have been busy talking about how to make a rock solid LTS release for Precise Pangolin that can be supported for 5 years.  KDE Software is in a good place for this LTS since we are coming towards the end of the long KDE/Qt 4 cycle and it is a stable and mature product.  

Other areas we have been looking at are a Kubuntu Active project to bring KDE's touch device software (Plasma Active, Calligra etc) to our users.  Some nice Chinese developers popped up on our IRC channel so we had a session on CJK support  which is currently broken in KDE and looks like we can get it fixed.  The docs will be polished for this release with plans for extra shiny changes in the Q-release.  There was a session on improving the filesharing plugin we wrote for Dolphin.

Still to come include sessions on accessibility, Muon Software Centre and a packaging tutorial for some Kubuntu people who want to step up their involvement (yay!)

You can join UDS remotely through the audio streams and IRC channels, see <a href="http://uds.ubuntu.com/participate/remote/">http://uds.ubuntu.com/participate/remote/</a>

There's a strong Qt presense here with a number of sessions to discuss how Qt can work with Ubuntu Desktop and fit into their software better.  Interesting development seem to be a Qt Software Centre and a Qt version of Ubuntu One, just now I'm in a session about Qt Quick for designers.

<img src="http://farm7.static.flickr.com/6092/6305867395_114887e151.jpg" width="500" height="375" />
A busy Qt session

<img src="http://farm7.static.flickr.com/6038/6306383512_003b0a8f3b.jpg" width="500" height="375" />
Qt Plenary talking about why Nokia (still) needs Qt

<img src="http://farm7.static.flickr.com/6117/6305865099_619baf95e5.jpg" width="500" height="375" />
Quintasan discovers American culture through Doctor Pepper

<img src="http://farm7.static.flickr.com/6101/6305867391_5688312cbc.jpg" width="500" height="375" />
Kubuntu's bi-annual UDS hot tub party
<!--break-->
