---
title:   "The Ubuntu Font, now with added Rupee"
date:    2010-10-05
authors:
  - jriddell
slug:    ubuntu-font-now-added-rupee
---
The <a href="http://font.ubuntu.com/">Ubuntu Font</a> has finally had its licence sorted out and been released to the masses. It has also been uploaded to the archive and set as default in Kubuntu.  

I'm a big fan of the font, I find it distinctive but undistracting, suitable at large and small sizes, readable on screens and printed.  Best of all this is the first time a font has been made with the intention of running it as an open project.  It supports Latin, Cyrillic and Greek but there's plenty of work to be done to get it supporting e.g. Arabic (which has suffered from a lack of fonts made for computer use).  I'm looking forward to monospace too since that's what I spend most of my day staring at.

Yesterday it gained the new <a href="http://en.wikipedia.org/wiki/Indian_rupee_sign">Indian Rupee Sign</a>, the which will make 10.10 the first operating system to include it by default.  

<img src="http://people.canonical.com/~jriddell/rupee-wee.png" width="183" height="161" />
Quick, time to take over India!

<img src="http://people.canonical.com/~jriddell/5days.png" width="185" height="149" />
Expect the internet to be running slow on Sunday
<!--break-->
