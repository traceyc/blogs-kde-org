---
title:   "French Parliament Chooses Kubuntu; oem-installer; docs hero"
date:    2007-03-20
authors:
  - jriddell
slug:    french-parliament-chooses-kubuntu-oem-installer-docs-hero
---
Some people have asked me which of the various Ubuntu distributions the French Assemblee Nationale has chosen to run for all its members. While there seems to be no official press release from the company involved yet, I can point you towards the one site which got it correct as <a href="http://www.lemondeinformatique.fr/actualites/lire-linux-sur-les-pc-des-deputes-ce-sera-ubuntu-22311.html">Le Monde</a> who put a K before the name.

Cutting in to Kubuntu before the beta release is the lovely new OEM installer from former Summer of Code student Abattoir.  OEM installer has always looked a bit drab, but for Kubuntu Feisty it will look as lovely as the new computers we hope it will run on.

From this:

<img src="https://wiki.kubuntu.org/KubuntuOEMInstaller?action=AttachFile&do=get&target=step1.png" width="400" height="300" class="showonplanet" />

to this:

<img src="http://kubuntu.org/~jriddell/oem-installer.png" width="640" height="480" class="showonplanet" />

(spot the bug where Qt 4 defaults to Motif style because there's no desktop environment running, it'll be fixed after beta)

Finally Kubuntu hero of the week award goes to nixternal who has done an excellent job of bringing Topic Based Help to the Kubuntu Documentation.  In the days of wikis and Google having docmentation distributed with the distro often seems redundant but with the new topic based approach users will have a good chance of being able to find what they need easily.  Still plenty of gaps to fill in for anyone wanting to help the docs team for feisty+1.
<!--break-->