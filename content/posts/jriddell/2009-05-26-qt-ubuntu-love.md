---
title:   "Qt Ubuntu Love"
date:    2009-05-26
authors:
  - jriddell
slug:    qt-ubuntu-love
---
Qt loves Ubuntu.  To show their love, they gave away t-shirts to everyone at the Ubuntu summit here in lovely Barcelona.  We love you too Qt.

<a href="http://www.flickr.com/photos/jriddell/3567635284/in/set-72157618745204849/"><img src="http://farm4.static.flickr.com/3373/3567635284_962500467d.jpg?v=0" width="500" height="375" /></a>

<a href="http://www.flickr.com/photos/jriddell/3567635284/in/set-72157618745204849/"><img src="http://people.ubuntu.com/~jriddell/qt-love.png" width="487" height="259" /></a>
<a href="http://www.flickr.com/photos/jriddell/3567635284/in/set-72157618745204849/">Qt Ubuntu Love photos on Flickr</a>
<!--break-->
