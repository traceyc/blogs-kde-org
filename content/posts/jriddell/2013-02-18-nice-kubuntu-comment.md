---
title:   "nice Kubuntu comment"
date:    2013-02-18
authors:
  - jriddell
slug:    nice-kubuntu-comment
---
Aaron's grumpy discussion of how Unity doesn't currently use Qt on the desktop made it to Slashdot got this nice comment in the /. discussion:

'I have been using Kubuntu -- the semi-official KDE Ubuntu -- for years. I like it, it's stable, and the interface with least surprise. It does what I want, when I want, and it doesn't try to "integrate" things that do not need to be, or should not be, integrated.'

Except we're an entirely official flavour, just like Ubuntu Desktop :)

