---
title:   "FOSSCamp, UDS and KOffice Release Schedule"
date:    2007-10-30
authors:
  - jriddell
slug:    fosscamp-uds-and-koffice-release-schedule
---
This cycle's Ubuntu Development Summit has been preceeded by FOSSCamp, an un-conference.  No schedule, no talks, just intelligent people and rooms to chat in.  <a href="http://kdemonkey.blogspot.com/2007/10/fosscamp.html">Robert Knight</a> does a good summary of some of the interesting sessions.  I also talked GPL 3 with people from the FSF, the good news is that core libraries like libc are unlikely to go GPL 3 only any time soon but we are likely to need an extra copy of libsmbclient and possibly other libraries until KDE can go GPL 3 happy.  We had a couple of sessions on usability in Ubiquity which hopefully will go somewhere, although partitions are never likely to be an easy thing to understand.  We looked at PackageKit and saw that it was good but apt support is lacking at the moment and it missed important apt features like debconf support.  I pondered the possibility of porting system-config-printer to KDE which I think is quite promising.  We also looked at our troublesome patch to add forkpty() support to Konsole which Robert promises to fix in return for lots of beer.  Finally we chatted about Java and the problems of getting it into Ubuntu server.

This week is the Ubuntu Developer Summit here in Boston, and so far we've discussed artwork, language selector improvements and improved dial up support which should be coming into the next network manager.

Great news from <a href="http://dot.kde.org/1193705373/">the KOffice developers</a>, they are syncing their releases with Kubuntu.  If you want to get your project into Hardy on time, follow <a href="https://wiki.kubuntu.org/HardyReleaseSchedule">the schedule</a> minus a couple of weeks.
<!--break-->
