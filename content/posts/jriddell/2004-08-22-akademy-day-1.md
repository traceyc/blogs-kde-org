---
title:   "Akademy day 1"
date:    2004-08-22
authors:
  - jriddell
slug:    akademy-day-1
---
It's been a great day here in Ludwigsburg.  Lots of interesting talks and people.  I've written up the talks I went to at <a href="http://wiki.kde.org/tiki-index.php?page=Talks+@+aKademy">Talks @ aKademy</a>.

Scott's metadata ideas are ambitious but necessary.  Making all files have metadata and be searchable is great, it shouldn't be easier to find things on the web than on your own computer as he said.  And if search sorts out KControl that will be just great.

Helio and Gustavo proposed basicly a CPAN for KDE/C++.  This is a great idea, there's loads of places where classes or methods are duplicated because it's not worth or possible to put them in a library.  Sidebar media player using Kaboodle's player engine is one example, the zoom button widget Umbrello nicked from KPlayer is another.  I hope this idea gets taken forward.

Matthais's talk on Qt style APIs made me realise how much work goes into creating Qt and how important a readable API is.

Quality Teams are an interesting idea, I can think of a few places where they may be able to help with Umbrello but for example I don't think they will help with kde-artists since what is needed there is experienced people with time to encourage and sort through submitions before new people come in.

And finally Freedesktop is great.  But is it just me or do they have a lack of PR?  I don't remember reading about the x.org release on slashdot etc.
