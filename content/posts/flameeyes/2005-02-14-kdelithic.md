---
title:   "KDElithic"
date:    2005-02-14
authors:
  - flameeyes
slug:    kdelithic
---
<i>I reposted this <a href="https://blog.flameeyes.eu/2005/02/kdelithic">on my personal blog</a> in 2014. Please add your comments there if you wish to comment.</i>

I was thinking about KDE development seriously in the last days, also because of many posts on many developers' blogs. For instance, when I was looking at <a href="http://aseigo.blogspot.com/2005/02/empower-users-and-new-developers-kde.html">Aaron post about new users and new developers</a> I found myself thinking about the "KDE: We Have A Lot Of Stuff" slogan. It's true: KDE has a very lot of stuff, but some stuff it's only dupe of dupe.

As I said in the comment, we have at least 4 image viewers: kuickview, kview, gwenview and pixieplus. Also if two of them are "navigable" viewers, and two only viewers, they still have a lot of functions which can be shared between them.

Also in my "field", network monitors, we have at least 4 applications/applets: KNetLoad, KnetmonApplet, KNemo and KNetStat. I'm wondering if it's worth for us developers to split so many times.

I'm usually for "biodiversity", but we are having too much similar applications. The reason, imho, could be found in the two-side blade that goes under the name of KDE releases.

Having syncronized releases makes surely the entire platform well integrated, but also makes the applications "bound" to limits in which they may not be so comfortable. An application may need to be updated within the time between releases. Newer application may need many releases to stabilize their interfaces. As read on the <a href="http://dot.kde.org/1107931942/">OpenUsability Interview</a> (also by Aaron :P), a good way to improve usability is the "release early and often" approach. This could not be done well when having synchronized releases. Not many people installs KDE from CVS, and the time needed to prepare a release of KDE is quite more than the one needed to prepare a single release of an application.

There are also cases in which "intermediate users", the ones which tries betas, haven't the time to submit desires, because the changes are made too closer to the i18n freeze. For example there was some changes in the latest 3.4b2 which made me think about the need for more options (just one could be an option to make kopete have the tabs also when only one tab is open in a window). Also there can be need for update something "during the time" between releases. Staying with Kopete example, looking at similar apps logs (AdiumX, Fire, just because I'm using the iBook now), we find that they was forced to update version day-by-day because the services was changing protocols.

In my opinion, the way we are going is frightening the users. We have too much apps in the "mainstream", also if we have a lot more "external" apps.

One of my dreams for the future KDEs is that we have a "base" environment (kdelibs, kdebase, the kernels of kdemultimedia, kdenetwork, kdegraphics and so on), which is released as now, and then the apps going their way, with only a need: provide a working version at every releaes of the "base environment". This way we can have KDE apps which are used outside KDE (for instance, many people uses K3B also if they don't use KDE at all) always compatible with at least one or two different versions of the "core", but always making use of newer APIs when they come into CVS.

Gentoo users, which before the split ebuilds, was compiling and installing the entire kde source modules, have seen the big problem: theorically one should be able to install the entire module without confusion, but if you try to compile and install all the base kde modules (without, obviously, kegs, kdenonbeta and playgrounds), you have many "dupe" apps, or apps which are used by very few people. I'm not a fan of KSirc myself, for instnace, or of JuK, and so on :)

The integration is one of the strong points of KDE, but it's also the weakest.

Another problem could be the missing of some applications which are not-so-used because we have similar applications for other environments which just works great. I'm thinking of EasyTAG; which is one of the applications which makes me relay on GTK2 for now. We could have a section of "Wanted Apps" where newcomers developers could look at to see if they can do something good for other users to start with.

Just to add my own comment about another comment on Aaron blog above, I'm one of the ones who thinks that having a Wiki at developer.kde.org could help. Also if many of the pages should be accessible via CVS, a Wiki is simpler to work with, and if we think, in these blogs we can find some useful and intersting views, which wasn't wrote as an HTML page, but simply as a blog post, which is way simpler :)

Ok just my 2 eurocent before going do something more useful (maybe eat something :) ).