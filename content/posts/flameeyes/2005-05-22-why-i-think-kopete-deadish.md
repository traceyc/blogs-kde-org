---
title:   "Why I think Kopete is deadish"
date:    2005-05-22
authors:
  - flameeyes
slug:    why-i-think-kopete-deadish
---
I feel sorry to restart blogging after a long time with a rant, but I'm still working on the translation and I also started working as <a href="http://www.gentoo.org/">Gentoo</a> dev, so I haven't had time to work on KNetLoad/general KDE apps.

Well time to start the rant, but before I want to say that I really respect Kopete developers, they did a good work, an extremely good one, but I think the way it's managed now it's moving it to the death.

In the past days there were many users who complained about problems with MSN, dued to changes on the MSN server. I don't know the exact details about this, but that's not so important.

I started in those days to use as IM client the other machine, with <a href="http://www.adiumx.com/">AdiumX</a> on it. AdiumX is a libgaim-based im client for MacOSX.. I'm still using it now that the MSN problem is fixed. Why that? Because I find AdiumX more useful as an IM client.

<!--break-->

Let analyze the problem: coping with proprietary IM servers sometime requires to have to change the way the protocol is implemented to have it working with new changes done to avoid users of unofficial clients to use the service; new features of the IM protocols are released very often; IM clients are something users uses daily, they must be simple and light to use.

The way Kopete is managed now makes this harder and harder to do: the release schedules makes really difficult fix problems like MSN's one, because Kopete can't be released on its own and is up to the downstream mainteiners to fix those problems on the spot. This is not good, this is not good at all as relying on downstream is not something which an be considered a good programming practice.
New features can't be added until a new minor version is released, and this haoppens just one time a year.
Kopete isn't exactly "light", and its UI is, instead, a lot difficult to manage. Just as an example, take the tabbed view of Kopete and the one of Adium.. Adium's can be switched using command-number, there's no way to set this up on Kopete; Adium allows you to select to which medium of a metacontact send the message to when you first write a message, Kopete doesn't. Adium interface takes the minimum space possible on the screen without the need of having a toolbar to connect/disconnect quickly. I think this can go for a looong time.

Unfortunately, the better way to achieve an usable and light interface is via a prototyping development. Adium used to release a new version every week when it started, changing features and UI design. Kopete can't do something like that because very few people will try the CVS versions and is constrained by KDE's release schedules.

So what's the solution? Eh, there's no clean solution. One of the ones which can be done is probably the development of a new IM client for KDE, maybe just using libgaim which seems to support most of the current IM features and many more protocols. This would require very quick releases to find a good design for the UI and a good set of features to implement.
I'd like to do something like that, but as I said I have no time at all... hope someone can just read that and start thinking of doing something to provide a better IM client for KDE users.
