---
title:   "Looking for Google Summer of Code students: OpenStreetMap vector rendering with tiling support "
date:    2012-04-02
authors:
  - torsten rahn
slug:    looking-google-summer-code-students-openstreetmap-vector-rendering-tiling-support
---
<p><a href="http://code.google.com/intl/de-DE/soc/">Google Summer of Code</a> application deadline is near and we are still looking for highly motivated students to work on a <a href="http://edu.kde.org/marble/">Marble Virtual Globe</a> project this summer.

<img src="http://devel-home.kde.org/~tackat/gsoc2012/gsoc-shirts.jpg">

<p>One of our favorite topics is "OpenStreetMap vector rendering with tiling support". And in this blog I'd like to describe our current ideas about this interesting subject a bit more in detail:

<p>Basically all the maps displayed by Marble are based on bitmap texture data. Some of these maps even consist of multiple texture layers. On top of these texture layers we display further content like placemarks and possibly even simple geometries (like GPX or KML data).
<p>For displaying this bitmap texture data efficiently we are making use of the popular concept of <a href="http://wiki.openstreetmap.org/wiki/QuadTiles">QuadTiles</a>: the data is arranged in different zoom levels. 

<a href="http://devel-home.kde.org/~tackat/marble_secrets1/tileid1.jpg"><img src="http://devel-home.kde.org/~tackat/marble_secrets1/tileid1_thumb.jpg"></a>

A single tile for a certain zoom level is split into four tiles in the next zoom level and so on:

<a href="http://devel-home.kde.org/~tackat/marble_secrets1/tileid2.jpg"><img src="http://devel-home.kde.org/~tackat/marble_secrets1/tileid2_thumb.jpg"></a>

This also works quite well across projections and zoom levels like in the case of the globe:

<a href="http://devel-home.kde.org/~tackat/marble_secrets1/tileid3.jpg"><img src="http://devel-home.kde.org/~tackat/marble_secrets1/tileid3_thumb.jpg"></a>

<p>However there is a single prominent case where it doesn't work as well as we would like it to do: For OpenStreetMap the size of the font in the pre-rendered tiles is very small. Scaling these tiles up and reprojecting them results in a somewhat fuzzy appearance of lines and labels. There is a <a href="http://techbase.kde.org/Projects/Marble/FAQ#The_labels_and_features_on_the_OpenStreetMap_map_theme_look_blurry.21_How_can_I_fix_this.3F">Marble FAQ</a> for this issue including a workaround for the case of the Mercator Projection.
<p>Still we'd love to provide crisp and sharp rendering for all projections and zoom levels. And the only way to do this properly is life vector-rendering. Konstantin Oblaukhov did an awesome job during last year in his Google Summer of Code project 2011 which provided vector-rendering for single OSM files. It got integrated with Marble 1.3 and we suggest you to give it a try. Click the link for the video below or click the thumbnail for a side-by-side comparison between the bitmap and vector map:

<p><a href="http://www.youtube.com/watch?v=QnZDkxj2SUU">Marble - OSM Vector Rendering Video</a>

<p><a href="http://devel-home.kde.org/~tackat/gsoc2012/marble-bitmapvector.jpg"><img src="http://devel-home.kde.org/~tackat/gsoc2012/marble-bitmapvector_thumb.jpg"></a>

<p>Currently OSM data must be downloaded and opened manually by the user. During this GSoC 2012 project a QuadTile scheme should be developed such that Marble can open and display the right part of the map without any further user interaction (other than panning/zooming).
This will require creation of special pre-filtered OSM files and extension of Marble's parsing and download capabilities. 
<p>We suggest the following rough mile-stones in order to come up with a working prototype:
<ul>
<li>Develop a tiling scheme that maps lon/lat/zoom tuples to filenames and vice versa
<li>Use a tool like osmosis to create tiles for a sample region (say, the city you live in)
<li>Extend Marble's .dgml format to be able to specify .osm files as input (and your tiling scheme)
<li>Improve the current vector rendering to handle different zoom levels better
<li>Add more OSM elements for vector rendering in Marble, improve existing ones (e.g. street names)
<li>(Optional) Work on a .pbf parser for Marble to read .pbf instead of .osm files (much faster)
<li>(Optional) Look into a new file format tailored for OSM vector rendering for Marble. Research existing formats like mapsforge
<li>(Optional) Create a tool that automates the process of creating the tiles needed by Marble 
</ul>
<p>If you are interested in this interesting project then you should act quickly: Deadline for applications is on Friday, April 6th, 2012. Apart from the <a href="http://www.google-melange.com/document/show/gsoc_program/google/gsoc2012/faqs#student_application_looks">usual GSoC student application guidelines</a> your application should:
<ul>
<li>describe the benefit of the feature from a user's perspective (including self-created mock-ups and screenshots)
<li>provide a rough technical explanation in your own words what the project will be about.
<li>state why you are the best person to master this project.
</ul>
<p>So if you are a student then we are looking forward to your application! Don't hesitate to <a href="edu.kde.org/marble/support.php">ask</a> us any questions.
<!--break-->
