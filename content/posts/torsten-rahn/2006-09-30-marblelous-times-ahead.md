---
title:   "MARBLElous times ahead"
date:    2006-09-30
authors:
  - torsten rahn
slug:    marblelous-times-ahead
---
<p>
Yesterday at aKademy I committed Marble to KDE SVN. Marble addresses an issue that exists since long: KDE lacks a generic widget that can be used to display geographical maps. Right now implementation as well as data for maps is duplicated all over SVN.
<p>
<b>Examples of Use</b>
<p>
<ul>
<li> <i>KDE Control Center</i> ( personalisation, timezones )
<li> <i>KDE-EDU</i> ( KGeography, KStars )
<li> <i>KDE-PIM</i> ( KAddressbook, Kopete)
<li> <i>KDE-GAMES</i> ( Risk ;-) )
<li> <i>Others:</i> KWorldWatch, KTraceRoute, KDesktop Wallpaper / Screensaver, ...
</ul>
<br>
<a href="http://developer.kde.org/~tackat/marble/marble0.jpg"><img src="http://developer.kde.org/~tackat/marble/marble0-thumb.jpg" class="showonplanet" />
</a>
<p>
<b>What it's not:</b> Marble neither tries to deliver a 200% academically accurate map that can be used to do science (so don't use it to control nuuukelear power plants) nor does it try to be a Google Earth clone - at least in terms of primary focus.
<p>
Based on the needs for such a generic widget I imposed the following requirements on the project which should always be kept in mind if you plan to contribute to the project (yes, you are very much welcome to do so):
<p>
<ul>
<li> <i>Marble uses a minimal free dataset that can be used offline.</i> Currently the total amount of data that is meant to be shipped is about 5 MB.
<li> <i>Marble runs decently without hardware acceleration.</i> It just uses Arthur as a painting backend and does NOT use OpenGL (However it largely benefits from EXA according to some initial testing). Extending it later on to support OpenGL as well shouldn't be hard however I don't consider that the primary focus. Depending on your hardware and the maps being displayed framerate is approximately 5-30 fps.
<li> <i>Marble uses vector as well as bitmap data:</i> Currently it uses the very old  MWDB II data combined with ETOPO 2, which I will update to current SRTM soon.
<li> <i>Marble displays the world map as 3D a sphere</i>, because it's more fun to use and less subject to distortion (So with regard to that it's just like NASA WorldWind, Earth3D and Google Earth)
<li> <i>Marble should start up almost instantly.</i> Currently it "cold" starts fully within 2-5 seconds. On each subsequent start it takes about one second.
</ul>
<p>
Beyond those requirements Marble already supports "themes" for different topics. In addition to the primary topograpical atlas map there are two other topics: "Earth at Night" and "Satellite View". It's easy to add further topics - I temporarily added a "Moon Theme" on request within 5 minutes of work (it just takes to create two bitmaps and adjust an XML file - try it).
<br>
<a href="http://developer.kde.org/~tackat/marble/marble2.jpg"><img src="http://developer.kde.org/~tackat/marble/marble2-thumb.jpg" class="showonplanet" />
</a>
<p>
Initial support for Google Earth KML files is there already, so it's possible to display placemarks easily. However the whole placemark rendering is still under development so don't complain if within the next two weeks it will fail or be slow. 

<!--break-->

<p>
Among the <i>TODO</i> items are:
<ul>
<li> Adding support for downloading data via the internet (that should be a matter of a few hours or days in terms of implementation). This might also offer the chance to display Google Maps data.
<li> Making placemarks and polygons accessible, so that they could be referenced to Wikipedia.
<li> An "editing mode" which might be used to add placemarks manually or via GPS devices - maybe even working together with efforts like Open Street Map.
<li> packaging for different plattforms. As Marble only depends on Qt >=4.1 it's easy to compile it for MS Windows. Daniel Molkentin even created an installable .exe files for it some time ago.
</ul>
<br>
<a href="http://developer.kde.org/~tackat/marble/marble1.jpg"><img src="http://developer.kde.org/~tackat/marble/marble1-thumb.jpg" class="showonplanet" />
</a>
<p>
If you want to try it these are the steps that lead to instant success:
<p>
<ul>
<li> Make sure you've got SVN and at least Qt 4.1 installed
<li> <code> svn co svn://anonsvn.kde.org/home/kde/trunk/playground/base/marble </code>
<li> <code> cd marble </code>
<li> <code> ./buildqmake qmake </code> (make sure that the parameter refers to Qt4's qmake - on Kubuntu you have to type in <code> buildqmake qmake-qt4 </code>").
<li> <code> make </code>
<li> <code> bin/marble </code>
</ul>
<p>
For optimal performance it's recommended to add " <code>-O2 -msse </code>" as compile options in marble/src/Makefile once it's generated and recompile. Any suggestions for how to add those options to src.pro directly are appreciated.
<p>
For each map marble needs some initialization to be done which is a one-time procedure and might take a few seconds on the very first startup (could be circumvented by packaging the resulting data if needed).
<p>
Have Fun :-)




