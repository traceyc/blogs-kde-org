---
title:   "Marble - Get involved!"
date:    2009-03-31
authors:
  - torsten rahn
slug:    marble-get-involved
---
We've recently extended our <a href="http://techbase.kde.org/Projects/Marble">Marble Wiki</a> to include more documentation about how you can participate in <a href="http://edu.kde.org/marble">Marble</a> development. Here are a few possible jobs for a start:
<ul>
<li> <i>If you are an <b>artist</b> or interested in <b>historical world maps</b> you can help us to improve our "Historic Maps" library: Magnus Valle ("wiscados" on #kde-edu) has done some great work in this area (resulting in the <a href="http://edu.kde.org/marble/screenshots/0.7/historical1.jpg">Historic Map</a> that comes with Marble 0.7). Find out <a href="http://techbase.kde.org/Projects/Marble/HistoricalMaps">here</a> how you are able to take part in this. 
<li> For <b>source code aficionados</b> we have lots and lots of <a href="http://techbase.kde.org/Projects/Marble/GoMarble/JJ">JuniorJobs for Marble</a>. You can have a look at our <a href="http://techbase.kde.org/Projects/Marble/GoMarble">How to become a Marble Developer</a> page to find out how to tackle those.
<li> If you're a student and you are still looking for a <b>Google Summer of Code 2009</b> topic, then you can either look at the <a href="http://techbase.kde.org/Projects/Summer_of_Code/2009/Ideas#Marble">KDE ideas</a> page or you look at Wikipedia's fine article about <a href="http://en.wikipedia.org/wiki/Virtual_globe">Virtual Globes</a>. They got a nice comparison matrix there which even includes Marble. There are still a few red "No" marks there which might help you to get an idea for your Marble topic for the Google Summer of Code application (like e.g. <i>Movie Maker, Guides, Planetarium,</i> etc.). Just make sure that you don't work on "Imagery of other planets" as Marble already has got this feature since Marble 0.7 (the author of the article just hasn't updated the page yet and I myself don't want to edit such wikipedia pages due to my obvious bias ;-)
</ul>
If you'd like to help us or have questions regarding Marble, just join us on IRC ( channel #kde-edu on irc.kde.org  ) or write an e-mail to marble-devel@kde.org.
<!--break-->
