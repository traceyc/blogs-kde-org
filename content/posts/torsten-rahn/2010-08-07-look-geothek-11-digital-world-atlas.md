---
title:   "A look at Geothek 1.1 Digital World Atlas"
date:    2010-08-07
authors:
  - torsten rahn
slug:    look-geothek-11-digital-world-atlas
---
Last weekend I received a postal package that contained a classroom atlas from Austria: the <a href="http://www.kozenn.at">Neuer Kozenn Atlas</a>. Inside there was a nice shiny CD with the title <b>GEOTHEK Schulatlas, Version 1.1 Digitaler Weltatlas</b>. The publisher of this atlas and its software is <a href="http://www.hoelzel.at">Ed. Hölzel</a>.  
<p>
<a href="http://developer.kde.org/~tackat/kozenn/kozenn.jpg"><img src="http://developer.kde.org/~tackat/kozenn/kozenn_thumb.jpg" class="showonplanet" />
</a>
<p>
The Kozenn Atlas (named after the slovenian born teacher <a href="http://de.wikipedia.org/wiki/Blasius_Kozenn">Blasius Kozenn</a>) has been produced in Vienna since 1861. It has been updated continously by the Geographical Institute <a href="http://www.hoelzel.at">Ed. Hölzel</a>. And it has been published in a lot of other countries as a world atlas (France, Netherlands, Belgium, etc.). Up to these days it's the most famous <a href="http://de.wikipedia.org/wiki/Atlas_%28Kartografie%29#.C3.96sterreich_und_.C3.96sterreich-Ungarn">austrian school atlas</a>.
<p>
<b>Geothek</b> is a software published by <a href="http://www.hoelzel.at">Ed. Hölzel</a> that had been developed by <i>Helmut Mülner</i> from the renowned <a href="http://www.joanneum.at">Joanneum Research</a>. I was curious since this was supposed to be the first version based on the <i>Free Software</i> <a href="http://edu.kde.org/marble">Marble</a>.
<p>
So I booted up the Windows 7 partition of my Thinkpad and put the CD into my external DVD drive. The setup application started automatically and I had to approve the License:
<p>
<a href="http://developer.kde.org/~tackat/kozenn/geothek0.png"><img src="http://developer.kde.org/~tackat/kozenn/kozenn_thumb-0.jpg" class="showonplanet" />
</a>
<p>
Afterwards the Nullsoft Installer quickly installed the files into the directory that I had chosen. A help text showed up and a new entry <b>Geothek Schulatlas</b> appeared in the Start Menu. A click on <b>Atlas</b> started the Geothek. It turned out that this version of <i>Geothek is a nicely enhanced version of the <b>Qt version</b> of Marble</i>: The application is fully translated into German. Also the location database is replaced by data from Ed. Hölzel.
<p>
<a href="http://developer.kde.org/~tackat/kozenn/geothek2.png"><img src="http://developer.kde.org/~tackat/kozenn/kozenn_thumb-2.jpg" class="showonplanet" />
</a>
<p>
Entering a location in the Search query field centers the globe onto the selected place as usual. However a second tab in addition to the globe view had also been added which contains a <i>2D-Viewer</i> for the physical maps of the Kozenn atlas (see screenshot above). This 2D viewer would automatically choose the correct physical map and center and zoom it according to the search query. <b>Symbols</b> are added on top of the the physical map which the user can click on and which <i>interactively</i> provide hundreds or thousands of <i>encyclopedic articles</i>, <i>climate diagrams</i> and beautiful <i>photo material</i> for lots of popular places. 
<p>
<a href="http://developer.kde.org/~tackat/kozenn/geothek1.png"><img src="http://developer.kde.org/~tackat/kozenn/kozenn_thumb-1.jpg" class="showonplanet" />
</a>
<p>
The <i>"Map Theme"</i> tab contains additional maps featuring topics like <i>"Population Density"</i>, <i>"Climate Zones"</i> and <i>"World Trade"</i>. And for each of these a specific legend had been created. Of course all those maps can be panned and zoomed as always in Marble. Very nice quality work!
<p>
<a href="http://developer.kde.org/~tackat/kozenn/geothek3.png"><img src="http://developer.kde.org/~tackat/kozenn/kozenn_thumb-3.jpg" class="showonplanet" />
</a>
<p>
At the bottom left there is a <i>"Statistics"</i> page. Clicking onto it makes a big table appear: The table lists all countries of the world. The columns cover all kinds of topics, like area, population, life expectancy and lots of other interesting facts. After selecting one of the columns the second tab displays a map that nicely color-codes this information. There are more configuration options for this map and it's pretty evident that the application developer had a lot of fun in developing this particular feature.
<p>
<a href="http://developer.kde.org/~tackat/kozenn/geothek4.png"><img src="http://developer.kde.org/~tackat/kozenn/kozenn_thumb-4.jpg" class="showonplanet" />
</a>
<p>
Another interesting addition is the <i>3D Satellite View</i>. Marble itself which serves as a base doesn't provide a true OpenGL view with flights over mountain landscape sceneries. Adding something like this is on our roadmap. But it will still take some time to add it properly.
<p>
So the <b>Geothek</b> developer added a special separate OpenGL based canvas which would allow to fly over a given area. The view features satellite imagery on a "flat" map and it's possible to tilt and rotate the view in all directions. Also the elevation of the landscape can be exaggerated:
<p>
As a bonus access to a set of "silent maps" is provided for teachers via the application menu.
<p>
All in all this application is a great showcase how <a href="http://edu.kde.org/marble">Marble</a> can be turned into a customized and polished quality product that is fun to use! The current version of <b>Geothek</b> is based on Marble 0.7/0.8, which is more than a year old. Since then Marble has developed a lot furter, adding stuff like <i>Routing</i>, <i>WMS support</i>, <i>multiple layers</i>, <i>better OpenStreetMap integration</i> and a lot of details that make life of application developers easier.
So I think that <i>Marble nowadays should be an even more attractive solution for publishers of schoolatlases and encyclopedias</i>: They could just rip out the original Marble content (if necessary) and replace it with their own high quality data. That would cost a lot less than building up a full custom solution on their own. In the Marble project we'd really like to support such kinds of projects. And since the <a href="http://edu.kde.org/marble">Marble</a> development pace seems to increase I wonder what the next two years will bring. 
<p>
I'd like to congratulate and thank <i><a href="mailto:helmut.muelner@joanneum.at">Helmut Mülner</a></i> and <i>Lukas Birsak</i> for this amazing product they have created. I was also very impressed by the way they credited the Marble team's work. Even better: The <b>Geothek</b> was published as an <a href="http://sourceforge.net/projects/geothek">LGPL project on sourceforge</a>. Of course the Hölzel maps are not included in the source code but that was naturally to be expected. But we also liked how the Geothek developer contributed his bug fixes back to the <a href="http://edu.kde.org/marble">Marble</a> Project in the best possible way.
<!--break-->
