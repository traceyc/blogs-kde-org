---
title:   "Let the Marble roll ..."
date:    2006-10-05
authors:
  - torsten rahn
slug:    let-marble-roll
---
<p>
My last <a href=http://blogs.kde.org/node/2412>last blog entry</a> about Marble covered how Marble is meant to be a generic geographical map widget. It shows the earth as a sphere but doesn't make use of any hardware acceleration (NO OpenGL). So although it might look similar to professional applications like Google Earth or Nasa World Wind it's rather meant to be a small light weight multi purpose widget for KDE.
<p> 
Still Marble comes already with basic Google Earth KML file support and therefore <a href=http://physos.org/>Rainer Endres</a> sat down and did some script magic to convert the data on <a href=http://worldwide.kde.org>KDE WorldWide</a> to KML file format.To improve the very basic placemark rendering I added some code to make sure that the labels don't cover each other. So here's the North American KDE Community in all its beauty (Say "hi" to Aaron, Jason, Chani, Annma, Jeff and all the others):
<br>
<a href="http://developer.kde.org/~tackat/marble/marble3.jpg"><img src="http://developer.kde.org/~tackat/marble/marble3-thumb.jpg" class="showonplanet" />
</a>
<p>
Of course the algorithm in place can't compete with more sophisticated automatic label placement methods (like <a href="http://en.wikipedia.org/wiki/Simulated_annealing"> Simulated Annealing</a>), but judging from the result it's a good start - especially given that it's not optimized yet at all. 
<p>
If you want to try it yourself these are the steps that lead to instant success:
<p>
<ul>
<li> Make sure you've got SVN and at least Qt 4.1 installed
<li> <code> svn co svn://anonsvn.kde.org/home/kde/trunk/playground/base/marble </code>
<li> <code> cd marble </code>
<li> <code> ./buildqmake qmake </code> (make sure that the parameter refers to Qt4's qmake - on Kubuntu you have to type in <code> buildqmake qmake-qt4 </code>").
<li> <code> make </code>
<li> Now <a href=http://developer.kde.org/~endres/worldwide/kde-devel-locations.kml>download</a> Rainer's fresh KDE Community KML file and save it in the "marble" directory that you have just created.
<li> As a last step execute: <code> bin/marble ./kde-devel-locations.kml</code>
</ul>
<p>
and Marble is ready to roll ...
<p>
Have a look at the fine work of my coworker <a href=http://en.wikipedia.org/wiki/Slartibartfast#Slartibartfast>Slartibartfast</a> and  see the <a href=http://www.trolltech.com>Trolls</a> while you're near. And as you're there already don't forget to <a href=http://events.kde.org/10years/> register</a> for the <b>10-Years-KDE anniversary</b> party: You might <b><a href=http://blogs.kde.org/node/2427>win a Qtopia Greenphone</a></b> if you do so ;-) .
<br>
<a href="http://developer.kde.org/~tackat/marble/marble4.jpg"><img src="http://developer.kde.org/~tackat/marble/marble4-thumb.jpg" class="showonplanet" />
</a>
Last but not least I'd like to thank <a href=http://www.jowenn.at//blog/>Joseph Wenninger</a> who did an initial port of Marble to cmake right after I had commited Marble to SVN.
<p>
<!--break-->

 