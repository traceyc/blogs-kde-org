---
title:   "Visual Changelog: Marble 0.10.0"
date:    2010-08-11
authors:
  - torsten rahn
slug:    visual-changelog-marble-0100
---
<p>
<b>Marble 0.10 was released on August 10th, 2010.</b> It is part of the KDE 4.5 Software Compilation. In the good tradition of recent years, we have collected those changes directly visible to the user. Unfortunately we were a bit late with our visual changelog for the release. So please enjoy looking over the new and noteworthy:
</p>
<br>
<p><b>Online Routing</b>

<p>Do you want to plan a bicycle tour in the nearby wood? Need driving instructions to get to an address in a foreign city? Besides searching for places, Marble can now display possible routes between two or more of them.</p>
<p>And the best thing is: The <b>routes are draggable</b>!
<p>
<dl> <dt> <a href="http://edu.kde.org/marble/screenshots/0.10/marble_routing0.png"><img border="0"
width="410" height="293" src="http://edu.kde.org/marble/screenshots/0.10/marble_routing0_thumb.png"
alt="Routing in Marble"></a> </dt> <dd><i>Online Routing in Marble</i></dd>
</dl>
<p>
Places to travel along can be entered using search terms (e.g. addresses) in the new Routing tab on the left. Of course Marble also allows you to input them directly on the map. Routes are retrieved using <a href="http://openrouteservice.org/">OpenRouteService</a> and displayed on the map. <b>Turn-by-turn instructions</b> are displayed on the left.
</p>

<p>
You can customize the route using preferences like transport type (<i>car, bicycle, foot</i>). An <i>arbitrary number of via points</i> can be added easily: Use either search terms or create stopovers quickly and conveniently by dragging them out of the existing route and dropping them at the desired position. While a real-time navigation mode is scheduled for Marble 0.11, you can already <i>export the route in the GPX format</i> now. This feature is handy for using routes in conjunction with your navigation device or other software.
</p>
<br>
<p><b>Bulk Download for Tile data in Marble for Offline Usage</b>

<p>For normal usage, Marble downloads the map data that is needed on the fly in the background. It also saves the data that has been downloaded on the hard disc. Now imagine that you make a trip to Norway, and you don't know for sure whether you'll have internet during the trip. So you want to download the whole Oslo area in advance. Up to now this hasn't been possible. But with Marble 0.10.0 you can click "File->Download Region ..." and you get a dialog where you can specify the region and the zoom levels that you want to download. This feature was brought to you by <i>Jens-Michael Hoffmann</i>.
</p>

<p>
<dl> <dt> <a href="http://edu.kde.org/marble/screenshots/0.10/marble_downloadregion0.png"><img border="0"
width="410" height="282" src="http://edu.kde.org/marble/screenshots/0.10/marble_downloadregion0_thumb.png"
alt="Downloading two levels of the currently visible map region" class="showonplanet"></a> </dt>
<dd><i>Download of the Visible Region</i></dd>
</dl>
</p>
<br>
<p><b>Support for Multiple Layers in Marble</b>

<p>So far, Marble has had support only for displaying a single map texture on top of the globe. (The only exception was the cloud feature which allowed having clouds displayed on top of the satellite map. This, however, was hard-coded and not extensible.)
<p>For this release, <i>Jens-Michal Hoffmann</i> has worked on <b>Multiple Layer support</b>.
This means that maps can now be created which display multiple texture layers.  For instance: a cloud layer on top of a street texture layer on top of a satellite texture layer.  This is all done in a generic way. So people who create maps for Marble can create an arbitrary amount of layers blended on top of each other. The best thing is: Due to the way the feature was implemented <i>the performance doesn't change</i>! And the clouds feature has been reworked to make use of the new mechanism.
<br>
<p><b>Support for Gimp-like Filters Between Layers in Marble</b>
<p>
<dl> <dt> <a href="http://edu.kde.org/marble/screenshots/0.10/marble-wms-level-16-multiply-blending-dresden.png"><img border="0"
width="410" height="448" src="http://edu.kde.org/marble/screenshots/0.10/marble-wms-level-16-multiply-blending-dresden_thumb.png"
alt="The City of Dresden 
shown in Marble with multiple layers: Satellite images provided via WMS displayed on top 
of OpenStreetMap data via Multiply Blending." class="showonplanet"></a> </dt> <dd><i>The City of Dresden shown in Marble with multiple layers: Satellite images provided via WMS displayed on top of OpenStreetMap data via Multiply Blending.</i></dd>
</dl>
</p>

<p>As described before, Marble has support for multiple layers now. Layers can get blended
on top of each other using Gimp-style "filters": You can choose among more than 30 blending algorithms, such as: <b>Overlay</b>, <b>ColorBurn</b>, <b>Darken</b>, <b>Divide</b>,<b>Multiply</b>, <b>HardLight</b>, <b>ColorDodge</b>, <b>Lighten</b>, <b>Screen</b>, <b>SoftLight</b> and <b>VividLight</b>. If you've ever use an application like Photoshop (TM), Krita or Gimp then you probably know what this means.
<br>
<p><b>Quick and Dirty WMS Support and More Url Download Schemes.</b>

<p>Lots of map data is provided on the internet on servers via the <a href="http://en.wikipedia.org/wiki/Web_Map_Service">Web Map Service ("WMS")</a> protocol. <i>Bernhard Beschow</i> has added initial quick and dirty WMS support to Marble. This means that there are now a huge number of maps that can be easily displayed using Marble.
<br>
<p><b>Marble Goes Mobile: Support for Nokia's N900 and UI profiles</b>

<p>With KDE 4.5, we have completed the first step toward mobile platform support: Marble will show a slightly different and simplified UI on the N900 Maemo platform compared to the desktop. For KDE 4.6 we aim for an even better user experience and improved performance.
<p>
<dl> <dt> <a href="http://edu.kde.org/marble/screenshots/0.10/marble_maemo0.jpg"><img border="0"
width="410" height="278" src="http://edu.kde.org/marble/screenshots/0.10/marble_maemo0_thumb.jpg"
alt="Marble running on a Nokia N900" class="showonplanet"></a> </dt> <dd><i>Marble on a Nokia N900</i></dd>
</dl>
</p>
<p>
<dl> <dt> <a href="http://edu.kde.org/marble/screenshots/0.10/marble_maemo1.png"><img border="0"
width="400" height="240" src="http://edu.kde.org/marble/screenshots/0.10/marble_maemo1_thumb.jpg"
alt="Marble Routing on Maemo 5" class="showonplanet"></a> </dt> <dd><i>Marble Routing on Maemo5</i></dd>
</dl>
</p>

<p>For more information please visit the <a href="http://marble.garage.maemo.org/">Marble Garage Project</a>. Next stop will be the <a href="http://gitorious.org/meegotouch-marble">MeeGo version for Marble</a>.
<br>
<p><b>Display APRS (Automatic Packet Reporting System) Senders with Marble</b>

<p>This is one of our first more specialized Online Service Plugins: The APRS plugin created by
<i>Wes Hardaker</i> shows worldwide Ham-Radio stations. <p>HAM Radio's APRS program allows radio transmitters to send their position and other information and is frequently used in <i>disaster relief efforts</i> for coordinating team distribution.
<p>
<dl> <dt> <a href="http://edu.kde.org/marble/screenshots/0.10/marble_aprs0.png"><img border="0"
width="410" height="290" src="http://edu.kde.org/marble/screenshots/0.10/marble_aprs0_thumb.png"
alt="APRS senders displayed in Marble" class="showonplanet"></a> </dt> <dd><i>APRS senders displayed in Marble</i></dd>
</dl>
</p>
<p>We are still looking for programmers who would like to create more Online-Plugins: e.g. <i>Twitter, News, Earthquakes or a social network plugin</i>. It's easy to do and there's an <a href="http://techbase.kde.org/Projects/Marble/OnlineServices">Online Service Plugin tutorial</a> available on our website that shows how to do it.
<br>
<p><b>Performance Improvements and More Changes Under the Hood ...</b>

<p>In addition to these major improvements, our Marble developers have worked on several other small features, bug fixes and performance improvements:
</p>
<ul>
<li>Two <i>additional search backends</i>: Hostip (try "planetkde.org") and <a
href="http://wiki.openstreetmap.org/wiki/Nominatim">OSM Nominatim</a> (try "ATM,
Karlsruhe") <i>(Dennis Nienhüser)</i></li>
<li>Improved <b>animation support</b> for zoom and panning <i>(Dennis Nienhüser)</i></li>
</ul>
<!--break-->
