---
title:   "New Mailing List for Marble"
date:    2007-08-09
authors:
  - torsten rahn
slug:    new-mailing-list-marble
---
<p>Hi everyone! Since yesterday our <a href="http://edu.kde.org/marble">Marble Project</a> has got a new mailing list for people who are interested in helping us to develop and promote Marble (and of course for people who use Marble to develop applications). 
If you're interested in signing up on marble-devel@kde.org you are invited to do so <a href="https://mail.kde.org/mailman/listinfo/marble-devel"> here </a>. If you want to join us on IRC, choose #kde-edu on freenode!
<br>
<p><a href="http://developer.kde.org/~tackat/marble/marble_20070809_09.png"><img src="http://developer.kde.org/~tackat/marble/marble_20070809_09_thumb.png" class="showonplanet" /></a>
<br>
<!--break-->