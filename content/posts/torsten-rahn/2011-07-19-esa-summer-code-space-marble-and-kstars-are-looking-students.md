---
title:   "ESA Summer of Code in Space - Marble and KStars are looking for students"
date:    2011-07-19
authors:
  - torsten rahn
slug:    esa-summer-code-space-marble-and-kstars-are-looking-students
---
<p>The <a href="http://www.esa.int">European Space Agency</a> is organizing <a href="http://sophia.estec.esa.int/socis2011">ESA Summer of Code in Space 2011</a>. And <a href="http://edu.kde.org/marble">Marble</a> and <a href="http://edu.kde.org/kstars">KStars</a> have just been <a href="">accepted</a> as mentor organizations! Thanks a lot ESA, this is terrific news!  

<p>The students application period starts today! And the schedule is tight: <b>The deadline for applications is on July 27th, 11:00 AM (UTC)</b> - that's about in a week!  
<p>So if you're a student and if you'd like to participate then please hurry up and
<ul>
<li>check the <a href="http://sophia.estec.esa.int/socis2011/?q=faq">FAQ</a> - especially the paragraph about <a href="http://sophia.estec.esa.int/socis2011/?q=faq#socis_elig_restrictions">eligibility</a>
<li>pick a project from the <a href="http://community.kde.org/SoCiS/2011/Ideas">ideas page</a> (or suggest your own project)
<li>fill out and submit the <a href="http://sophia.estec.esa.int/socis2011/?q=node/11">application form for students</a>.
</ul> 
 
<p>If you have any question, you can refer to the <a href="http://sophia.estec.esa.int/socis2011/?q=documentation_center">documentation center</a> or write to the public <a href="http://groups.google.com/group/esa-socis">SOCIS mailing list</a>. 

<p>If you have a question regarding Marble or KStars ideas just ask on our mailing lists (kstars-devel@kde.org and marble-devel@kde.org).

<p>And remember: <i>In space no one can hear you code.</i>
<br>
<br><img src="http://developer.kde.org/~tackat/marble_all_hands_thumb.jpg" class="showonplanet" />
<!--break-->
