---
title:   "Gran Canaria Desktop Summit - Slides and Video about the Virtual Globe \"Marble\""
date:    2009-08-12
authors:
  - torsten rahn
slug:    gran-canaria-desktop-summit-slides-and-video-about-virtual-globe-marble
---
Just recently my blog got syndicated by <a href="http://planet.osgeo.org">Planet OSGeo</a>. So I'd like to take the opportunity and say "Hello" to all readers  and I'd like to thank for the <a href="http://mateusz.loskot.net/2009/07/29/marble-on-the-planet/">warm welcome</a>!
<p>
As a <a href="http://edu.kde.org/marble">Marble</a> and KDE developer I had been at the <a href="http://www.grancanariadesktopsummit.org/">Gran Canaria Desktop Summit</a> last month (and that made me miss the <a href="http://www.stateofthemap.org/">State Of The Map</a> unfortunately). I enjoyed this conference a lot and I would like to thank especially the GCDS Team which did a terrific job and made this event a full success.
</p>

<p>
<a href="http://www.youtube.com/watch?v=iukEWGoDNho" border="0"><img border="0" src="http://edu.kde.org/marble/gcds2009_thumb.jpg" class="showonplanet" ></a>
</p>

<p>
For all people who have missed my 30-min presentation about Marble: The <a href="http://developer.kde.org/~tackat/marble-akademy2009.pdf">slides</a> of this presentation are now available. Additionally there is a
<a href="http://www.youtube.com/watch?v=iukEWGoDNho" border="0">Video on YouTube</a> which has the first ten minutes. The full OGV-video can be downloaded <a href="http://www.geeksoc.org/gcds/Torsten Rahn, Marble.ogv">here</a>. The video is licensed under the <a href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-Share Alike License 3.0</a>. Credits go to the GCDS team. Enjoy!</fullstory>
</p>
<p>As an introduction to Marble I can also recommend this <a href="http://www.dedoimedo.com/computers/marble.html">nice review</a> over at Dedoimedo.</p>

<!--break-->