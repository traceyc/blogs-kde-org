---
title:   "Marble's Secrets Part I: Behind the Scenes of Marble..."
date:    2008-02-09
authors:
  - torsten rahn
slug:    marbles-secrets-part-i-behind-scenes-marble
---
<p>
This is the first part of a new series about Marble. I'll try to address a few frequently asked questions as well as the current status of the development. So stay tuned.
<p>
If you've ever followed KDE 4 development then you've probably heard about Marble. Marble is a virtual globe which displays the earth. So Marble can be used as a nice digital replacement for your desktop globe at home where you can look up places. 
<p>
But wait! There's more to it: Actually these days Marble can also display flat maps (thanks to Carlos Licea), can show different "map themes" and can serve as a Qt4-widget as well as an application! This means that as a programmer you can use Marble in your very own project as a map widget (License: LGPL). <i>Marble was designed to run on any device and on any operating system supported by Qt4 without any further requirements.</i> You can download the latest version of Marble together with KDE 4.0.1 <a href="http://www.kde.org/info/4.0.1.php">here</a> (It's part of the KDE-EDU module).
<p>

<b>How Marble stores texture data</b>
<p>
If you start Marble you might realize that the startup time is pretty good: It usually takes maybe 2-5 secs to start Marble (and we are working on improving that dramatically). If you zoom into the earth you might notice that Marble doesn't get slower while zooming in. Looking at the amount of memory being used up you will also see that memory numbers don't change either. <i>No matter how much you zoom in it's as little as 65-100MB</i> which is pretty lean compared to other virtual globes.
Among other concepts this is being accomplished by loading the map piece by piece. Marble uses a concept that is very popular among virtual globes: <i>Quadtiles</i>. In fact we are using the most simple form of Quadtiles compared to other <a href="http://en.wikipedia.org/wiki/Grid_(spatial_index)">more sophisticated solutions</a>. We decided to do so for reasons of pragmatism and in order to keep things easy to understand for people who want to contribute to Marble. 
<br><div>
<a href="http://developer.kde.org/~tackat/marble_secrets1/tileid1.jpg"><img src="http://developer.kde.org/~tackat/marble_secrets1/tileid1_thumb.jpg" class="showonplanet" />
</a>
<a href="http://developer.kde.org/~tackat/marble_secrets1/tileid2.jpg"><img src="http://developer.kde.org/~tackat/marble_secrets1/tileid2_thumb.jpg" class="showonplanet" />
</a>
<a href="http://developer.kde.org/~tackat/marble_secrets1/tileid3.jpg"><img src="http://developer.kde.org/~tackat/marble_secrets1/tileid3_thumb.jpg" class="showonplanet" />
</a>
</div>
<p>
This means that for different zoomlevels the map is available at different pixel resolutions: 
level 0: 1350x675, level 1: 2700x1350, level 2: 5400x2700, ... . The highest zoom level currently provided from http://download.kde.org measures 86400x43200 pixel which equals about 500m per pixel at the equator.
<p>
Loading maps this large would exceed the physical memory or would at least take very long to load. So the maps are stored as a 2:1 ratio checkboard of several tiles. In Marble each of those tiles measures 675x675 pixel. The next higher zoom level gets created by dividing every tile into 4 pieces measuring 675x675 pixel each: 
<p>
So level 0 consists of a row of two 675x675 pixel tiles (1350x675). Level 1 consists of two rows of 675x675 pixel tiles with four columns each (2700x1300) and so on. You can see this in the screenshots which also show the filenames of the tiles: They contain the row number (counted from north to south) and the column number (counted from west to east). As you can see Marble will only load about 5-6 tiles for any view. For Spherical Projection ("Globe") these tiles get mapped onto the globe.
<br><div>
<a href="http://developer.kde.org/~tackat/marble_secrets1/osm1.jpg"><img src="http://developer.kde.org/~tackat/marble_secrets1/osm1_thumb.jpg" class="showonplanet" />
</a>
<a href="http://developer.kde.org/~tackat/marble_secrets1/osm2.jpg"><img src="http://developer.kde.org/~tackat/marble_secrets1/osm2_thumb.jpg" class="showonplanet" />
</a>
</div>
<p>
So in theory there is <i>technically no limit as to how far Marble can zoom into the earth</i>. As you can see in the screenshot we have tested Marble with data from the kind folks from the <b>OpenStreetMap Project</b> ( http://www.openstreetmap.org ) already and there we are down to approximately <i>level 13</i> where you can see objects that can reasonably get measured at the <i>order of centimeters</i> already (The test data tiles were kindly created by <i>Artem Pavlenko of Mapnik</i> fame).
<p>
During the very first application start of Marble you might have noticed that the first start takes a bit longer: According to the dialog Marble needs to "initialize the map". Actually it just creates the tiles from the huge base image while showing the dialog.
<p>
<b>How to create your own maps in Marble?</b>
<p>
Marble looks for maps on the local file system: these maps get specified in a .dgml-files. So that's what Marble will look for right from the start. Marble will search your home directory's ~/.marble/data/maps/earth as well as the respective system directory ( e.g. /usr/share/apps/marble/data/maps/earth ). The *.dgml file is a small XML-file that mostly contains 
<!--break-->
<p>
<ul>
<li> the <i>real "name"</i> of the map,
<li> the name of the directory (<i>"prefix"</i>) ( Usually a PNG file ), 
<li> the <i>icon</i> that gets used in the Marble application's "Map View"/"Theme" listview and 
<li> the actual file name of the basemap (<i>"installmap"</i>). This file should be available in JPG format. If you have map data available which got stored as tiles using the correct format then the installmap file isn't needed of course.
</ul>
<p>
You can simply create your own map by copying over an existing directory ( e.g. "citylights" ) from the system directory. Of course you need to change the name of the target directory to something different:
<code>tackat@tackat-laptop:~$ cp -r /usr/share/apps/marble/data/maps/earth/citylights/ /home/tackat/.marble/data/maps/earth/mymap
</code>
<br>
<p>
Then you just need to rename the citylights.dgml-file (e.g. to "mymap.dgml") and load it with a text editor. After adjusting the 4-5 values inside the xml file you are almost done and you just need to provide a random icon (preferably at a size of 128x128) and the basemap. The basemap needs to be provided in a special, but very commmon and simple format, called <a href="http://en.wikipedia.org/wiki/Plate_carrée_projection">Equirectangular Projection</a> or <a href="http://en.wikipedia.org/wiki/Plate_carrée_projection">Plate Carrée</a>. 
Entering this term into your favorite internet search engine will likely return suitable images for testing. You'll even discover that <a href="http://wiki.panotools.org/Equirectangular">panoramic photography</a> gets stored in this format - so Marble could even qualify as a <i>panoramic image viewer</i>! 
<p>
Once you are done you should (re)start Marble to detect the new map. If everything is ok there should be a new map available in the "Map View" listview labeled with the name that you have chosen. Once you select it Marble will start creating the needed tiles. 
<br><div>
<a href="http://developer.kde.org/~tackat/marble_secrets1/custom1.jpg"><img src="http://developer.kde.org/~tackat/marble_secrets1/custom1_thumb.jpg" class="showonplanet" />
</a>
<a href="http://developer.kde.org/~tackat/marble_secrets1/custom2.jpg"><img src="http://developer.kde.org/~tackat/marble_secrets1/custom2_thumb.jpg" class="showonplanet" />
</a>
</div>
<p>
It needs to be said that this way of creating maps is only suitable for "small" maps as there is a limitation: Automatically Marble can only create tiles from base images that measure up to approximately 10800x5400 pixel. If you want to create tiles from larger images then you are on your own: You need to find a suitable tool or you need to create a script that will create the tiles for you. In this case make sure that you adhere to the naming scheme of directories and tiles (note that tile rows and columns need to get specified in 6-digit format):
<code>
{marble-data-base-url}/maps/earth/{theme}/{level}/{row}/{row}_{column}.jpg
</code>
<br>
<p>While we are using 675x675 pixels as a tile format you should be able to use other tile sizes as well as long as you stick to the fixed size for the whole map theme.
<p>
If you have a Marble version >= 0.5.1 installed then you can even create a custom legend, too: 
<p>
<ul>
<li> Either you provide your very own HTML document with the file name "legend.html" in the map's directory. If you do so you can add checkboxes like in the original legend. Just have a look at the global "legend.html" file in the system directory's marble/data directory ( e.g. /usr/share/apps/marble/data/legend/legend.html ).
<li> Or you just extend the "template" by providing additional legend items. You can do so by adding a legend section to your own .dgml file. Look into the "Atlas" theme's srtm.dgml file to see how it's done. Usually the file is located inside /usr/share/apps/marble/data/maps/earth/srtm/srtm.dgml. 
</ul>
<p>
In the <a href="http://blogs.kde.org/node/3272">next part</a> I'll cover a topic that outlines a few pretty unique internal features of Marble.
<p>
<i>Until then I hope that you have fun creating your own maps!</i> Of course I'd be happy about any screenshots or links to actual new maps sent to tackat@kde.org by mail. For help or questions you can join us on IRC ( irc.kde.org, #kde-edu ) or send a mail to our <a href="https://mail.kde.org/mailman/listinfo/marble-devel">mailing list</a>.
<p>
<b>Marble Junior Jobs</b>
<p>
Given that Marble is a globe we have a pretty good chance for reaching world domination. However we need your help in terms of patches to accomplish this aim. If you plan to look into Marble's code here are a few easy junior jobs that you might want to start with: First you need to spend 10 minutes to get Marble from the <a href="http://edu.kde.org/marble/obtain.php">sources</a> and to <a href="http://edu.kde.org/marble/obtain.php">compile</a> it. You need to check out Marble "trunk". Once that is done you can take the plunge:
<small>
<p>
<ul>
<li> <i>Category EASY</i>: The view shown in the screen shot above shows a debug mode which displays Marble's texture filenames and the level on the globe. This is pretty handy for debugging. However right now this feature is only available by manually changing the source code in line 118 inside marble/src/lib/TextureTile.cpp to:
<br>
<code>
  bool tileIdVisible = true; if(tileIdVisible) m_painter.paintTileId(theme);
</code>
Your job would be to either enhance qtmain.cpp or kdemain.cpp to provide a command line option for this feature ( I'd suggest "--texture-id" ).

<li> <i>Category MEDIUM:</i> Currently Marble creates tiles from the install-map at runtime during its first use. However it would be great if we could easily provide prebuilt tiles in the Marble package. For this we need a command-line tool which would get executed at compile time if the data hasn't been tiled already. The tool should be based on the very same TileCreator class that gets used by Marble internally. There is some initial code in marble/src/tilecreator however it's far from done as it still needs some love.
</ul>
</small>
 