---
title:   "Marble integration with KOffice"
date:    2008-05-24
authors:
  - torsten rahn
slug:    marble-integration-koffice
---
<p> Shortly after the <a href="http://blogs.kde.org/node/3475">Marble OpenStreetMap integration</a> I'm happy to report that <b>Simon Schmeisser</b> has managed to integrate Marble as a <a href="http://wiki.koffice.org/index.php?title=Flake">Flake Shape</a> into KPresenter. As a flake shape you can change the map according to your liking even after it has been embedded into the KOffice application. So it's not just a simple image but rather a component that allows the user to adjust the content:
<p>
<a href="http://developer.kde.org/~tackat/marble-flake/marble_koffice_flake.jpg"><img src="http://developer.kde.org/~tackat/marble-flake/marble_koffice_flake_thumb.jpg" class="showonplanet" /></a>
<p>
Which other Free Software office application has a virtual globe that it can embedd into the documents across plattforms?
<p>
So that adds another successful application of KDE technology to Marble's capabilities: It's only a few weeks ago that our weather man Henry de Valence has started to work on a Marble WorldClock plasmoid:
<p>
<a href="http://developer.kde.org/~tackat/marble-flake/marble_plasmoid.jpg"><img src="http://developer.kde.org/~tackat/marble-flake/marble_plasmoid_thumb.jpg" class="showonplanet" /></a>
<p>
Of course apart from a <a href="http://developer.kde.org/~tackat/marble_secrets3/marble_worldclock.jpg">Marble Qt Designer Plugin</a> and the Marble KPart (which gets used by the Marble Desktop application itself) you can use the MarbleWidget in your very own application -- like Gilles Caullier has done for Digikam:
<p>
<a href="http://developer.kde.org/~tackat/marble-flake/marble_digikam.jpg"><img src="http://developer.kde.org/~tackat/marble-flake/marble_digikam_thumb.jpg" class="showonplanet" /></a>
<p>
The Marble Widget has no KDE ties, so even if your application uses only Qt you can still take advantage of Marble. 
So what's the next showcase that people will come up with? A Marble Netscape Plugin? I'm curious.
<p>
In other news Shashank Singh has just joined us for GSoC 2008 and will provide Panoramio support for Marble. Welcome Shashank! 
<p>
Right now the Marble Team is heavily working towards KDE 4.1 Beta2: Jens-Michael is working on further improving <a href="http://blogs.kde.org/node/3475">Marble OpenStreetMap support</a>, Inge is working on the Mercator Projection, Patrick is working on further improving KML for his GSoC 2008 project (which actually deals with vector rendering in Marble), Claudiu is working on his Satellite Plugin and Henry is working on texture colorization and Temperature / Precipitation maps. I'll continue to work on GeoPainter and Marble's new plugin architecture which will enable other developers to write Qt-Plugins for Marble to render their own layers and their own data. 
<p>
These are exciting times for Marble. Can you feel the Earth spinning?
<p>
<!--break-->
