---
title:   "Register for 10-Years-KDE Party early –– Win a Qtopia Greenphone!"
date:    2006-10-04
authors:
  - torsten rahn
slug:    register-10-years-kde-party-early-––-win-qtopia-greenphone
---
<a href="http://events.kde.org/10years"><img src="http://developer.kde.org/~tackat/10yearskde.jpg" class="showonplanet" /></a>
<p>
KDE is going to celebrate it's 10th anniversary. We'll have a birthday party on Friday, October 13th, 2006 in the Technische Akademie Esslingen in Ostfildern (near Stuttgart). More information is available at:
<p>
<a href="http://events.kde.org/10years">http://events.kde.org/10years</a>
<p>
If you <a href=http://events.kde.org/10years/>register</a> by Oct.11, 9:00 UT you'll be entered in the 10-Years-KDE raffle where <b>you could win a brand new <a href=http://www.qtopiagreenphone.com/>Qtopia Greenphone</a> </b> offered by <a href=http://www.trolltech.com>Trolltech</a>! So, hurry up and let us know whether you'll attend!  Participation will be possible for everybody who registers in time and we are happy to welcome the winner in our large community of KDE and Qt developers. Of course you also need to attend the event and be present at the raffle ceremony at 16:00 CET in order to qualify for participation.
<p>
Excluded from the raffle are the members of the party organisation team, the speakers of the event as well as Trolltech's employees.
<!--break-->