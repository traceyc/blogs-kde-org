---
title:   "Marble Virtual Globe and Google Summer Of Code 2010"
date:    2010-04-04
authors:
  - torsten rahn
slug:    marble-virtual-globe-and-google-summer-code-2010
---
<p>The student application deadline for Google Summer of Code 2010 is near: So if you plan to submit an application for a <a href="http://edu.kde.org/marble">Marble</a> project then it needs to arrive no later than April 9, 19:00 UTC.
<p>I have just added a few more ideas to the idea page: OpenDesktop.org support and Panorama support. So now in total there are five Marble ideas listed on our idea page:
<ul>
<li><a href="http://community.kde.org/GSoC/2010/Ideas#Project:_Time_Support_for_Marble">Time Support for Marble</a>
<li><a href="http://community.kde.org/GSoC/2010/Ideas#Project:_More_online_plugins_for_Marble">More online plugins for Marble</a>
<li><a href="http://community.kde.org/GSoC/2010/Ideas#Project:_Enhanced_KML_support_for_Marble">Enhanced KML support for Marble</a>
<li><a href="http://community.kde.org/GSoC/2010/Ideas#Project:_Marble_To_Go_.28Navigation_Mode.29">Marble To Go (Navigation Mode)</a>
<li><a href="http://community.kde.org/GSoC/2010/Ideas#Project:_Panoramic_Picture_Support_in_Marble_.28.22StreetView.22.29">Panoramic Picture Support in Marble ("StreetView")</a>
</ul>
Of course this isn't the limit, so if you come up with another great idea don't hesitate to apply for it.
<p>In other news we've just created a <a href="http://www.facebook.com/#!/group.php?gid=346064806033">Marble Facebook Group</a> for all users of Marble and Facebook.
<p>Join us! Join the Marble Community!
<!--break-->
