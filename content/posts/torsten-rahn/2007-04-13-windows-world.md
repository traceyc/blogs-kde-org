---
title:   "Windows to the world"
date:    2007-04-13
authors:
  - torsten rahn
slug:    windows-world
---
<p>
Great news for all people who like the <a href="http://www.kde-apps.org/content/show.php/Marble+-+Desktop+Globe?content=55105">Marble Desktop Globe</a>: Google Summer of Code '07 has started and our vivid KDE community managed to get 40 students accepted - three of them will work on Marble! I've got to admit that I was pretty overwhelmed by the interest people showed in Marble: lots of students have submitted applications that dealt with Marble. Most of the submissions were great so it was pretty hard for sure to choose among them. 
<br><p>I hope that in the future we'll be able to get also those people involved whose ideas weren't accepted this time (especially the geocaching idea and the Marble Almanac). Right now I'm happy that now that Inge Wallin of KOffice fame has joined forces we'll have at least five people working on this tiny little project. For GSoC '07 topics will cover KML and GPS support as well as flat 2D projection.
<br><p> In other news there have been lots of <a href="http://www.kde-apps.org/content/show.php/Marble+-+Desktop+Globe?content=55105">Marble packages for Linux</a>  -- thanks to Steffen "Whitey" Joeris, Beineri, Chitlesh Goorah and lots of others. However I'd like to point out that Christian Ehrlicher managed to create a <a href="http://edu.kde.org/marble/downloads/windows/marble-0.3.5-svn20070412-win32.exe">Marble install package for Windows</a> which is ready for download (keep in mind that Marble is still in early beta stage, so bugs certainly do exist). Now even if you are forced to use Windows there is no excuse anymore not to try Marble ;-) I hope people don't mind if I post a screenshot: 
<br>
<a href="http://edu.kde.org/marble/downloads/windows/marble_windows_xp.png"><img src="http://edu.kde.org/marble/downloads/windows/marble_windows_xp_thumb.png" class="showonplanet" />
</a>

<p>
<!--break-->

 