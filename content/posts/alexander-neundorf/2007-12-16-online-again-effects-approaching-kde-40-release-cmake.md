---
title:   "Online again, effects of the approaching KDE 4.0 release on CMake"
date:    2007-12-16
authors:
  - alexander neundorf
slug:    online-again-effects-approaching-kde-40-release-cmake
---
Two weeks ago I moved again and it took until yesterday that I got internet access again, now using cable. This actually rocks, it is really fast, 6 Mbit/s. I still can remember, I think it was in 2001 I still had an analog modem with 100something kbit/s. That's really an incredible increase in speed in just 6 years ! 
<!--break-->
I also got some more experience with WLAN under Linux, especially under Slackware 12. kwifimanager somehow didn't completely do what I wanted (and I didn't feel like reading a lot of docs), and knetworkmanager doesn't come with Slackware.

So I just had a look at /etc/rc.d/rc.inet1.config, and voila, everything there. This file contains prepared sections for several different network interfaces, including WLAN. So I entered my settings in that file, executed /etc/rc.d/rc.inet1 and was online again :-)
Really, Slackware makes such things, while having to edit text files, so easy :-)
Not so easy was that I actually had to compile the kernel modules myself, the RTL81xx chipset is not part of the 2.6.21 kernel, which is the default Slackware 12 kernel. <br>
Ok, enough about that. I wanted to note basically two things:
<ul>
<li> I was two weeks offline. During this time there were around 350 emails on kde-core-devel. Until not that long ago the cmake mailing list was quite low-traffic. But that has changed, I had around 450 (!) emails on this list. So there were more emails on the cmake list than on k-c-d, and I am quite sure that the fact that more people are starting to use KDE4 is the reason for that. 
<li> Do you know chrpath ? This is a small tool which can modify the RPATH in an existing ELF file. While that sounds kind of boring, it is actually really cool. If you build something which links to shared libraries, and (some of) these libraries are also built in your project, and you want to run the executable uninstalled, then you need different RPATHs in the uninstalled executables and in the installed executables. Without chrpath the only way to do this is linking the ELF file again with a different RPATH and then install this new file. This can take quite some time. With chrpath this is not necessary anymore, you can install the executable and then just change the RPATH inside the executable. Probably cmake will support that in the near future. In order to make this easier available for KDE developers, I "ported" chrpath to use CMake and added it to svn in kdesupport. I have one screenshot I'd like to share, it shows on one side the chrpath source with autotools and on the other side chrpath sources with cmake. Compare the number of files and the total size yourself.
Let me just note that the cmake version is only 13% the size of the autotools version :-)
[image:3155]
<p>
Alex

P.S. the CMake 2.6.0 release is also approaching, it will come with a lot really cool features, it's a pity that we can't use them for KDE 4.0.x
