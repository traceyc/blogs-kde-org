---
title:   "Rejoice - it has been done !"
date:    2008-01-05
authors:
  - alexander neundorf
slug:    rejoice-it-has-been-done
---
SCNR.

Rejoice - it has been done - KDE 4.0.0 has been tagged ! :-)

Although I didn't touch any significant number of lines of C++ code of KDE for the 4.0.0 release, the last (almost exactly) 2 years since I brought CMake to KDE4 (together with the help of many other developers, especially Laurent Montel !)  have been very busy, impressive, awesome, stressful, interesting, rewarding, exhausting, etc. for me.
<!--break-->

I feel kind of relieved that it is now finally released (maybe the feeling is totally wrong and the stress now actually starts, we'll see).

Anyway, time to celebrate :-)
Congratulations to all who made this happen !

Alex
