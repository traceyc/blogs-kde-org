---
title:   "How to get CMake find what you want it to"
date:    2008-12-12
authors:
  - alexander neundorf
slug:    how-get-cmake-find-what-you-want-it
---
Since some August or so we are now requiring <a href="http://www.cmake.org">CMake</a> 2.6 for KDE svn trunk.

One thing was has been added and which is very nice support for the new environment variable <b>CMAKE_PREFIX_PATH</b>. It's purpose is to help with getting CMake to find what you want it to find. 
<!--break -->
Usually a software package is installed to some "prefix" directory, in the lib/ subdir of that prefix go the libraries, in the include/ subdir of that prefix go the headers, and in the bin/ subdir of that prefix go the programs.

By default, if you use one of the find_(file|path|library|program)() commands, CMake searches in a set of such prefix directories, e.g. in /usr, /usr/local, /opt/csw and more (i.e. in the include/, library/ and bin/ directories depending on whether find_(file|path|library|program) was called. Have a look at Modules/Platform/UnixPath.cmake for the full set of default search directories.

Since CMake 2.6 it also searches in CMAKE_INSTALL_PREFIX of the current project and in the directory where CMake itself is installed.
This works successfully in a lot of case.

But let's say, you have some custom install directories on your system, e.g. if you are a kdepim developer you might have kdesupport and kdelibs in /opt/kde4, while you install kdepimlibs and kdepim to $HOME/kdepim/.

Or, as another example, I have kdesupport in /opt/kdesupport, kdelibs in /opt/kdelibs, kdepimlibs in /opt/kdepimlibs, qt-copy in /opt/qt-copy, the rest of KDE4 in /opt/kde4.

What's the best way to get CMake 2.6 find all that ?
By setting the environment variable <b>CMAKE_PREFIX_PATH</b>, which is a list of directories (similar to PATH). The prefixes (directories) listed in it will be searched <i>before</i> the default search directories, and lib/, include/ and bin/ will be appended appropriately.

So, this is what I do to get cmake find all my KDE4 stuff:
<pre>
$ export CMAKE_PREFIX_PATH=/opt/qt-copy:/opt/kdesupport:/opt/kdelibs:/opt/kdepimlibs:/opt/kde4
</pre>
Did you notice that I also put the directory of qt-copy there ? This also makes sure that CMake finds the qmake sitting there first, and I don't have to change the PATH additionally.

Now what would the aforementioned kdepim developer do ?
I guess something like:
<pre>
$ export CMAKE_PREFIX_PATH=$HOME/kdepim:/opt/kde4
</pre>
This environment variable has to be set (only) at CMake-time. And once CMake has found the stuff, it shouldn't be required anymore, authors of FindFoo.cmake modules should take care of that (i.e. that everything is properly stored in the cache).

So, what to keep from that ?

<i>If you have some custom install directories on your system and you want CMake to find the stuff installed there, set <b>CMAKE_PREFIX_PATH</b>.</i>

Alex



Now what's a good way to get CMake t
