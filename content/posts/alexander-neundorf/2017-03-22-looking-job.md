---
title:   "Looking for a job ?"
date:    2017-03-22
authors:
  - alexander neundorf
slug:    looking-job
---
Are you looking for a C++/Qt/Linux developer job in Germany ?
Then maybe this is something for you: <a href="http://jobs.cs.uni-kl.de/media/upload/jobboerse_pdf/Sharp_Reflections_-_Software_Developer.pdf">Sharp Reflections</a>

I'm looking forward to hear from you. :-)
Alex
