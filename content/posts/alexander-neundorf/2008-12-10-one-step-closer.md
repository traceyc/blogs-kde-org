---
title:   "One step closer..."
date:    2008-12-10
authors:
  - alexander neundorf
slug:    one-step-closer
---
I think we are getting one step closer to World Domination (TM) of Linux ! ;-)

How come that I think that ?
Have a look at that page (it's german):
<a href="http://www.dm-digifoto.de/gratis-download.html">http://www.dm-digifoto.de/gratis-download.html</a>
<!-- break-->
It's a page from a big german drug store chain, and I wanted to download their photobook software. I had already started installing wine on my box just for the purpose of creating a book of my own photos. And I remember one year ago that software was available only for Windows (or at least not for Linux). And I didn't expect that this would have changed.

Now, what did my eyes see ?
"Die Foto-Software im Komplett-Paket für Linux"

So this year the photobook software is also there in a version for Linux ! :-)
Now if that's not cool !
Great job, dm ! Way to go !

Here's a screenshot: <br><a href="http://blogs.kde.org/node/3797?size=_original"><img src="https://blogs.kde.org/files/images/dmfotobook.thumbnail.png" alt="dm_fotowelt" title="dm_fotowelt"   width="100" height="82"/> </a>

Alex

P.S. the application uses Qt4 :-)

P.P.S. as you can see, a KDE 3 desktop is still good enough to build KDE 4 ;-) It has even the advantage that the build breaks really bad if some include directory order goes wrong (and I get KDE3 includes in KDE4)

