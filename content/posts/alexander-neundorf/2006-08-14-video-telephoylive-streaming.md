---
title:   "Video telephoy/live streaming"
date:    2006-08-14
authors:
  - alexander neundorf
slug:    video-telephoylive-streaming
---
Hi,

now that I found that skype for Linux doesn't support video, is there a good solution ?

Ekiga ? 

Or FFserver on both ends ? Is ffplay able to ffserver streams ? (the docs mention only windows media player)

Or vlc ? Is it mature ? Reliable ? Buildable without major problems ?

Or nmm ? Is it actually supposed to be used for this ?

And, both clients are behind a firewall with NAT.

So, any good hints ?

Alex
