---
title:   "Regexp library benchmarks..."
date:    2011-12-02
authors:
  - alexander neundorf
slug:    regexp-library-benchmarks
---
Since I don't have an account to reply there <a href="http://blog.rburchell.com/2011/12/why-i-avoid-qregexp-in-qt-4-and-so.html">http://blog.rburchell.com/2011/12/why-i-avoid-qregexp-in-qt-4-and-so.html</a>, I'll do it here.

Recently somebody benchmarked regexp libraries as potential candidates for use in CMake.
Short version: PCRE was not exactly much faster than what is in CMake, re2 and TRE were magnitudes faster: <a href="http://public.kitware.com/pipermail/cmake-developers/2011-November/002547.html">[cmake-developers] Re: slow regex implementation in RegularExpression</a>.
This is the home of TRE: <a href="http://laurikari.net/tre/about/">http://laurikari.net/tre/about/</a>

Alex
