---
title:   "svn is slow..."
date:    2006-04-23
authors:
  - alexander neundorf
slug:    svn-slow
---
svn is slow.
Ever tried svn commit or cleanup over trunk KDE ?
I started a commit, pressed Ctrl-C to cancel it. It didn't cancel within maybe half a minute.
I killed it with -9.
Then I tried update, but it told me that it was locked and I should do a cleanup. So I did a cleanup. It took approx. 5 minutes :-/
Now I'm updating again...

cvs wasn't so bad after all. It felt much faster, and was dead-simple on the server. Shorter urls like "cvs co kdebase" and you had everything, no long "svn co https://whatever..." I never can remember.

Anyway, back to coding...

Alex

