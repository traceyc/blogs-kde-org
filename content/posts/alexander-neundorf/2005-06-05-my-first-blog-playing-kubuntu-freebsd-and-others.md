---
title:   "My first blog: playing with kUbuntu, FreeBSD and others"
date:    2005-06-05
authors:
  - alexander neundorf
slug:    my-first-blog-playing-kubuntu-freebsd-and-others
---
So finally I'll also try to start blogging since nowadays everybody seems to do so. So, most importantly, the sun is shining today here. Nice wheather make me feel good :-)
On the more KDE related side of things, now I also tried the famous kUbuntu distro. Since 1997 I'm Slackware user. I tried Suse, it's nice, polished, but feels quite slow and hides too much from the user (for my taste). Some years ago I tried Debian and was lost in dselect.  RedHat sucked in 1998, Mandrake didn't seem to be really stable in 2000. So, I always had Slackware running on all my boxes.
With all the hype around kUbuntu, I decided to give it a try and install it on a notebook. Or more exactly, I asked my girlfriend (non-geek) whether she would like to try to install Linux. So she inserted the CD, selected "German", "Deutschland" and "Europe/Berlin", entered the network config I told her and selected the (pre-formatted) partitions for Linux. Then she pressed enter and some minutes later she had successfully completed her very first Linux installation, actually her first operating system installation at all.
Even for her it was almost too boring :-)
Nothing had to be adjusted manually:soundcard, video, network, touchpad, USB, everything just worked.
Once there was the rumor that installing Linux would be hard...
Synaptic/kynaptic/apt is a nice thing, makes installing software really easy. That's kind of an advantage compared to Slackware...
On the negative side, booting takes with kUbuntu longer than with Slackware. It really does a lot of things when starting. I don't think all this is necessary (LVM, RAID and other things I don't have). Making this parallel could probably speed it up considerably.

While at it, I decided to give FreeBSD 5.4 a try too. After realizing what a slive and a disk label is, everything went smooth and it also recognized all the hardware correctly. The installer asked me kind questions like "Does CEST sound good ?" I don't think that "CEST" really sounds good so I said "no". But then it didn't come up with a better sounding alternative, but the same selection again. So I decided that I was probably wrong and "CEST" actually sounds good.The installer also asked me whether I'd like to insert CD2 now, but since I didn't really wanted this at this moment, I said "no" and so the installer didn't install the package from CD2 :-/
Once I realized this I decided that it is probably better to want to change the CDs. So I got everything installed.
After all, now I got a nice FreeBSD installation running KDE 3.4.0 :-)
It feels quite clean, the file system has less symbolic links (like /var/X11 -> /usr/X11/lib etc.) The ports system is really cool. Works like a charm. It even provides cutecom (http://cutecom.sf.net) :-)
Right now I'm trying to compile kdevelop trunk from cmake on FreeBSD. Works without problems :-)
More about cmake later...
All in all, also two nice operating systems :-)
But Slackware still rules !

Alex
