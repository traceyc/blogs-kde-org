---
title:   "CMake sadness ?"
date:    2006-04-14
authors:
  - alexander neundorf
slug:    cmake-sadness
---
Hi,

well, since it seems some discussions are nowadays done via blogs, let me say something as reply to [http://aseigo.blogspot.com/2006/04/cmake-sadness.html | CMake Sadness] by Aaron.

So, building KDE is a huge task. Building KDE 4 is a much harder task than build KDE 1, 2 or 3.
Why ? Because with KDE 4 we support more platforms and more compilers. With KDE < 4 we had only UNIX-like platforms with mainly gcc. With KDE 4 we have UNIX-like, OS X (listed separately because it is a bit different: frameworks, bundles, etc.) and Windows. Under Windows MSVC >6, mingw and msys is supported. 
This is *much* more than the autotools were every able to do.

Detecting Qt4 has become much harder compared to Qt3. With Qt  1,2 and 3 QTDIR was the preferred way to find Qt. Now QTDIR is deprecated, so that the Qt4 qmake has to be in the PATH. It doesn't help in this regard that some distributions decided to rename the Qt4 qmake to qmake-qt4. Especially if there is on some boxes Qt3 qmake, Qt4 qmake-qt *and* Qt4 qmake from qt-copy installed. Also the split in debug- and release libraries and how to deal with these introduces a lot of ways how to deal with them (... and how to break compiles).

And we can do even much more than simply building KDE on these platorms right now: on Windows projects for the Visual Studio IDEs can be generated, on OS X XCode can be used to build KDE, and project files for KDevelop can be generated automatically so that you can use KDevelop to work on KDE 4.
Neither autotools nor scons would be able to do something similar.

So, cmake is a new tool for most KDE developers.

Take the time to learn it.

Your primary source for information is http://www.cmake.org , go through the documentation (http://www.cmake.org/HTML/Documentation.html ), the intro, and the Wiki. I can't stress it enough, the cmake Wiki ( http://www.cmake.org/Wiki/CMake ) contains a lot of useful information. 

Use the cmake page in the KDE wiki just as a starting point: http://wiki.kde.org/tiki-index.php?page=KDECMakeIntro .


And, since cmake is a new tool for most KDE developers, of course there isn't very much knowledge about it yet among them. But, was there actually *that* much knowledge about the autotools among the KDE developers ?
When I gave a talk about cmake and asked how many of the audience really know autotools, maybe 5 out of 50 raised there hand. I think this is probably the same in the KDE eveloper crowd.

The state of the cmake files in KDE isn't finished yet, it is still maturing. As bad as it is, working on central parts of the build system can very easily break the compile for everybody except the own machine. This has happened, and it will happen again. And of course, it will be fixed quickly.

If you hit a problem, enable the verbose mode of the Makefiles:
$ make VERBOSE=1

this will show you the full command line, and you should be able to figure out what's wrong.
After all, everything is in the cmake files in kdelibs/cmake/modules/, which get installed to share/apps/cmake/modules/. There is a bunch of FindFoo.cmake files for finding "package" like Cups, Samba, libpng etc. These are simple, just have a look.

The most important files for building KDE are FindKDE4Internal.cmake, KDE4Macros.cmake and FindQt4.cmake. These three files are not that simple, but since the cmake syntax is quite specialized and not very large, you should be able to find your way through.


In the next let's say two weeks, more breakages might follow: the handling of the order of the include dirs will be changed. I am very certain that I won't get everything right on the first try. Especially considering the fact that when getting home from work I have more or less up to three hours per day left for KDE, and this time is consumed by working through the emails, fixing new issues and trying to rebuild kdelibs. Sometimes there is some time left to try to compile an additional module, e.g. kdebase, but all at once is just out-of-scope for me.

So, if you hit problems, report them either on kde-buildsystem@kde.org or kde-core-devel@kde.org, blogging about them isn't a real help.

Within a few weeks cmake 2.4.0 will be released and then the cmake support in KDE will mature and become more stable than it is now.

So, have fun learning cmake, it's worth it ! :-)

Bye
Alex


