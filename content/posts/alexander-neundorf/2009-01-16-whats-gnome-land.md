---
title:   "What's up in Gnome land ?"
date:    2009-01-16
authors:
  - alexander neundorf
slug:    whats-gnome-land
---
Hi,

just out of interest, where is Gnome heading these days ?
I'm a bit confused.
<!--break-->
There is Gnome2, which are applications written using libgtk2, mostly in C (also some Python and C#).

Then recently there was talk about Gnome3, and a stepwise process to get there by improving gtk. This sounds like it would be still mainly C.

Then in the last years I heard a lot about using Mono and C# in Gnome, since this should lead to faster development than using just C.

Then today I found something about Vala on the definitive source of information ;-)
<a href="http://tech.slashdot.org/comments.pl?sid=1091547&cid=26455413">http://tech.slashdot.org/comments.pl?sid=1091547&cid=26455413</a> and <a href="http://tech.slashdot.org/comments.pl?sid=1091547&cid=26450511"> http://tech.slashdot.org/comments.pl?sid=1091547&cid=26450511</a>.

Apparently Vala is an object-oriented C-like language, like C++, C# and Java.
Is this what they intend to use as their main development language for Gnome in the future ?

Now, I'm not sure this is a good idea. I mean, we in KDE have some experience with inventing our own stuff. We had arts, which was unsuccessful, we had DCOP, which lead to DBUS, for some time we had unsermake, which died. But even we didn't invent our own programming language. It seems the motivation for Vala is to make working with g_object easier ?

So, somebody who knows, what is the general trend in Gnome ?

Alex
