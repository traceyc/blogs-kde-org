---
title:   "git everywhere... (how to compare my stuff to the central repository)"
date:    2010-03-10
authors:
  - alexander neundorf
slug:    git-everywhere-how-compare-my-stuff-central-repository
---
KDE will be moving to git, Qt has moved to git, recently also CMake moved to git.

So, it's time to start using git.

...until now it really looks so complicated, compared to cvs/svn.
In cvs/svn it was easy: local working copy, remote central repository, just one step away. 
With git this is about three steps away: local working copy, stash, local repository, remote repository. You always have to be aware of where things are.

Ok, one has to get used to that "git add" is something different from "svn add", and "git commit" is different from "svn commit".
After a "git merge --rebase" (which seems to be the same as "svn up"), git told me that some files "need updates".
So I just tried "git update".
This would have been too easy:
git: 'update' is not a git command. See 'git --help'.

Did you mean this?
        update-ref
$

Ah, right, I think "git checkout" is the same as "svn update". 

At least "git diff" seemed to do the same as "svn diff" (or cvs diff).
Then I committed. To my local repository this means.
Now, I wanted to have a patch which I can send away.
So, with svn/cvs I did "svn diff", so I tried "git diff".
Nothing.
Of course, because there is no difference anymore between my working copy and my local repository. 
But how do I get the difference between my local repository and the repository from which I cloned ?

I mean, I'd like to know that before I push.

So, some browsing and googling follows:
http://marklodato.github.com/visual-git-guide/
http://www.kernel.org/pub/software/scm/git/docs/everyday.html
http://book.git-scm.com/3_comparing_commits_-_git_diff.html

I read things like "committing with a detached HEAD" or "Create a local reference to the remote repository as" and other things which sound quite complicated.
I try "git log '^origin/master' master" and then "git format-patch --stdout 62b095d". This looks quite good, but is only one commit, not two. So I try "git format-patch --stdout 62b095d 57c5d370". This gives me plenty weird stuff. Next try.

And finally I find a web page which seems to contain what I'm looking for, how do I compare my stuff to the central repository:
http://pjps.tumblr.com/post/96756489/git-diff-with-a-remote-repository

Phhh, is it really that complicated, doing like one of the most basic things ?

So, how do I do this ?

Alex
