---
title:   "My 2 cents on releases..."
date:    2008-05-10
authors:
  - alexander neundorf
slug:    my-2-cents-releases
---
Ok, we all know, blogs are the new mailing lists, so here just some quick comments from my side on all the release-cycle stuff.
<!--break-->
About being in sync with releases of other projects: I am caring for our buildsystem which uses CMake, so the releases of CMake and KDE matter to me. I want to provide a stable (as in "not chanigng too often") build environment. If project A (KDE) depends on project B (CMake), it doesn't help project A anything if it's releases are in sync with project B. Why ?
Because, well, in order to have a stable environment I don't want to force developers to have to update their cmake from non-distro packages (the binary cmake packages provided by Kitware work flawlessly on all systems I tested so far, btw). This is frustrating ("damn, which package do I have to update today to make KDE build again ?") and takes away a lot of time (not for a single developer, but accumulated for all).
So, before we (KDE) require a new version of CMake, I personally want this not to happen before that version of cmake is shipped by the most common distros (i.e. a few months after its release).
So I see also a value in not depending on always the latest and greatest releases of other projects.


Regarding DVCS: I don't have much experiences with them, but when I tried git, it wasn't really easy. Mercurial is said to be much more user friendly, which for me would be a big advantage, lowering the entry barrier to KDE development etc.

But, what I wanted to say: I big part of the value cvs/svn provide is IMO that all code in development is visible to all developers immediately, not only once a while. Especially for KDE with its many many developers this means that current code will be tested (at least built, maybe also run) immediately by many developers, so problems should be found quickly. We don't have that many developers on "exotic" platforms like Windows, OSX, Solaris, *BSD. For keeping KDE working on these platforms it is IMO important not to create more work for them, by requiring them to deal with more repositories.

About "always summer in trunk": sounds really good, but I also see the risk that less people will be working on the stable branch as soon as it is created.


About the 6 months vs. 9 months vs. 3 weeks: I agree with Aaron that 6 months is quite short. It leaves 4 months for feature development. Which also means you should start with the features really soon after the release, otherwise you may have only 2 or 3 months left for that. At least for me it has always been the case that there are weeks where I just don't have the time to do anything significant on KDE. E.g. when I was student, there were the weeks with the exams, sometimes you go on vacation, real-world stuff like moving, visiting friends and relatives etc. add up from time to time. For me this also means a time span like proposed by Aaron of 3 to 4 days for a stabilizing period absolutely wouldn't work for me, since it will happen often that just during these days I won't have anytime at all.

So, this was probably not as nicely written as the other blogs on this topic, but I just wanted to share my view on this too.

Alex


