---
title:   "fun with compiling kdelibs..."
date:    2005-08-16
authors:
  - alexander neundorf
slug:    fun-compiling-kdelibs
---
Hi,

as I mentioned previously, I'm trying to build kdelibs (still 3.5, not 4.0 yet) using cmake.

To make it short, yesterday it managed to get to build kwallet, where I discovered that I missed to insert a check for sys/types.h. So I inserted the check, and then could watch how the box started to recompile almost complete kdelibs (since config.h changed which is included in a lot of files).

Ok, so now I got to kio/misc/kpac/ (what is this ?), and, guess what, a check for netinet/in.h was missing. So, the same game again. Now the box is compiling again...

As an interesting fact I found out that cmake first build the libraries, and after these the binaries. IOW all libs have been successfully built right now. If a binary is created during the build which is needed in other parts of the build (e.g. dcopidl) cmake automatically builds dcopidl if the rules for creating the dcop files include a dependency to dcopidl. So the order of the directories you put into the CMakeLists.txt doesn't matter.

Alex
