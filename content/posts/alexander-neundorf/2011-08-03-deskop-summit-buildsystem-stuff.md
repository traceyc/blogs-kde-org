---
title:   "Deskop Summit ... buildsystem stuff"
date:    2011-08-03
authors:
  - alexander neundorf
slug:    deskop-summit-buildsystem-stuff
---
So, to make it short, I'll be also in Berlin at the Desktop Summit, from Saturday to Wednesday evening.

<img src="https://www.desktopsummit.org/sites/www.desktopsummit.org/files/DS2011banner.png" />

So if you have wishes, questions, want to contribute to the KDE buildsystem or CMake, just look around for me :-)
There'll be a KDE buildsystem BoF . If you want to attend, please enter here when it is suitable for you <a href="http://doodle.com/v53bgft9xkffdnft">in this doodle poll</a> .

What will we be talking about there:
<ul>
<li> modular kdelibs
<li> extra-cmake-modules, distributing our cmake files independent from KDE
<li> upstreaming automoc
<li> <a href="https://projects.kde.org/projects/kde/superbuild">KDE superbuilds</a>
<li> Maybe testing
<li> Everything else people come up with related to our buildsystem, packaging an testing
</ul>

From Wednesday on also Bill Hoffman from Kitware/CMake will be at the Desktop Summit. He's by far the better specialist fot CMake, and especially for CDash and testing.

And most probably I'll give a <a href="http://wiki.desktopsummit.org/Workshops_&_BoFs/2011/CMake_beyond_the_basics">"CMake - Beyond the basics"</a> workshop, intended for project maintainers, or, people who have or want to take care of their cmake files themselves.

Please note that this workshop will <b>not</b> take place Thursday (since I will have left already the day before), but most probably Wednesday at 11:00 AM (http://wiki.desktopsummit.org/Workshops_&_BoFs/Schedule).


Alex
