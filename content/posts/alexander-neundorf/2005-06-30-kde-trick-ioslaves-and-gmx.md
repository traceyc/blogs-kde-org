---
title:   "KDE trick: ioslaves and gmx"
date:    2005-06-30
authors:
  - alexander neundorf
slug:    kde-trick-ioslaves-and-gmx
---
Hi,

maybe you know it already, but I just learned it last week from Carsten at LinuxTag: if you have an email account at gmx, enter "webdav://mediacenter.gmx.net" in konqueror (or any other KDE application), and you get access to the mediacenter area of gmx. IOW one gigabyte storage available for free at your fingertips, thanks to the marvellous KDE ioslave architecture as comfortable to use as your local harddisk :-)<br>
In the authentication dialog just enter your complete gmx email address and password, and that's all. Wasn't that easy ?

Alex
