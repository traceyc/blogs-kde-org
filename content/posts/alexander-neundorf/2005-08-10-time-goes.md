---
title:   "As time goes by..."
date:    2005-08-10
authors:
  - alexander neundorf
slug:    time-goes
---
As time goes by KDE distributors come and go.
It seems in the meantime I'm one of the more long-time contributors, I got my cvs account late 1999 I think (with low but quite constant activity...). 
Thanks go to David Faure, without him I probably wouldn't be around here today ! :-)

Digging in my memories some names come to my mind which did a lot of very good work in past days but from which I didn't hear anything since a long time now already. Here they come in no special order and absolutely incomplete:
<ul>
<li> Kurt Granroth: did a lot of core stuff in the KDE 2.0 days, especially KXMLGUI if I remember right. Also kxmlrpcd. 
<li> Carsten Pfeiffer: He was also mainly active during the KDE 2.x days. Since he finished university it seems there isn't much time left for KDE. Worked on the file dialog, KFileItem, the combobox completion, kuickshow and others.
<li>Malte Starostik: He did a lot of cool stuff, also cool "hacks", like reaktivate, an active x (I think) compatible extension for khtml using Wine.
<li>Torben Weis: original kfm author. Do I have to say more ?
<li>...and a lot of others
</ul>

Are you still around ? KDE today is more exiting than ever, a lot of work is to be done. Would be nice to hear from you again :-)

Alex
