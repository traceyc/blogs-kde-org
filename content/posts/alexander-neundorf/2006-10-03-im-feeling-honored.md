---
title:   "I'm feeling honored"
date:    2006-10-03
authors:
  - alexander neundorf
slug:    im-feeling-honored
---
So I am back from <a href="http://conference2006.kde.org/">aKademy</a> in Dublin, and at first I want to say:
I'm feeling very honored that I received one of the three <a href="http://dot.kde.org/1159194107/">aKademy awards</a>: Best non-application contribution: <a href="http://lwn.net/Articles/188693/">CMake for KDE4</a>.

So, a big thank you to you !

It has been a lot of work, but it was worth it. Actually I hoped I would just demo that <a href="http://www.cmake.org">CMake</a> is able to build kdelibs, and then our (now previous) buildsystem guy Stephan Kulow would take over, but that didn't work out and now I'm in charge of the buildsystem of one of the biggest existing free software projects. Oh well.

I also want to congratulate Boudewijn Rempt on the Best Application Award for Krita and Laurent Montel on the Jury Special Award for being the KDE4 Commit Champion !
You guys rock !

In case you didn't know: next year we 3 will be the jury which decides about the awards.


This year was the first time that I attended aKademy. It was a very nice experience. I'm everytime deeply impressed when I see how <a href="http://static.kdenews.org/jr/akademy-2006-group-photo.html">so many people</a> from all over the world, which know each other only from mailinglists, IRC etc., can meet in one place, work together to get things done and immediately form a real community and become friends.

I was very happy to finally meet <a href="http://www.kitware.com/profile/team/hoffman.html">Bill Hoffman</a>, one of the CMake developers, who supported us so exceptionally, in person. Together with his wife and children we had a nice dinner. I'm looking forward to meet him again.

It was also great to be able to talk with the KDevelop team: Alex Dymo, Adam Treat, Nadeem Hasan and Jakob Petsovits. Talking directly with each other usually helps to sort things out much faster than via emails. Unfortunately Matt Rogers wasn't at aKademy, then the team would have been almost complete.
This was also the case for fixing some install issues with Volker Krause, adding a new profiling build type to our buildsystem by Holger Freyther, Duncan now made QCA build with CMake, Hubert Figuiere did the same with libgphoto (or Abiword ? not sure) AFAIK, a FindFlex module by Jakob and a gencmake script (in <a href="http://websvn.kde.org/trunk/KDE/kdesdk/cmake/scripts/">KDE svn </a>) by Tobias Hunger to create simple cmake files from just looking at the files in a directory.

After all, I didn't get much actual coding done, but therefor a lot of talking, discussing and helping other people with, guess what, CMake.

Beside KDE stuff we also had a lot of fun in the evenings in Dublins pubs, going there with Holger Schröder, Klaas Freitag, Adrian Schröter, Adam Treat, Nadem Hasan and others was great !

So, it was a really amazing event !
Special thanks go to the organization team, headed by Marcus Furlong !
Great work !

Alex
