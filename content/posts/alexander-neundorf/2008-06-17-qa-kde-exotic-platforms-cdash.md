---
title:   "QA for KDE on exotic platforms  - CDash ?"
date:    2008-06-17
authors:
  - alexander neundorf
slug:    qa-kde-exotic-platforms-cdash
---
As Friedrich already noticed, blogs are where discussions happen nowadays...

Well, also responding to one of Ade's posts, where he states 
<!--break-->
<ul><tt>
"I've been starting CMake "experimental" builds on various Solaris machines with SS12 to get numbers on how many warnings and errors we're producing. Since Dirk's dashboard works quite well for the vast majority of our developers -- Linux based -- I think a separate dashboard that counts and reports issues for non-Linux builds would be useful. "
</tt>
</ul>

Yes, it would be really nice to have nightly (or daily) builds of KDE (at least kdelibs) on more exotic platforms. Which means for KDE basically everything != Linux, i.e. Solaris, FreeBSD, Windows, OSX, more ?

<a href="http://www.kitware.com">Kitware</a>, the developers of our beloved <a href="http://www.cmake.org">CMake</a>, the buildsystem which is on its sure way to world domination ;-), also have a lot of other cool free software (<a href="http://www.paraview.org">ParaView</a>, <a href="http://www.vtk.org">VTK</a>, <a href="http://www.igstk.org">IGSTK</a>, <a href="http://www.gccxml.org">gccxml</a>), among them, useful for us for this purpose, <a href="http://www.cdash.org">CDash</a>. CDash is a replacement for the Dart 1 (hard to install) and the Dart 2 (buggy) dashboard server. CDash uses the common web stack: Apache, php, mysql, so it is much more lightweight than Dart2 was, is <a href="http://www.cdash.org/CDash/">already mature</a> and easy to set up.

So, how can we use that for KDE ? There are two options:
<ul>
<li>Install our own copy of CDash on some server and use this one then
<li>Ask Kitware if they can set up a project on their CDash server for KDE (they did that already in 2006, but nobody committed builds :-/, so it was removed again)
</ul>

So, comments ?

Alex
