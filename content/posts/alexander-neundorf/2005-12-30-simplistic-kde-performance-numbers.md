---
title:   "Simplistic KDE performance numbers"
date:    2005-12-30
authors:
  - alexander neundorf
slug:    simplistic-kde-performance-numbers
---
Hiya,

here's a *very* simple way to get a performance number for KDE:
<!--break-->
$ time konsole -e sh -c exit

This gave me the following numbers:

0.75 seconds on a notebook, Sempron 3000+, SUSE 10.0
0.80 seconds on a desktop, Athlon XP+ 2000, Slackware 9.1, KDE compiled from svn
1.0 seconds on a notebook, Intel P4 M, 1.8 GHz, SUSE 10.0
1.0 seconds on a notebook, Intel P4 M, 1.8 GHz, kUbuntu 5.10

(as a comparison xterm takes 0.1 seconds for this).
I guess we could save a lot if konsole would not link to libkio.  Does the delayed initialization of a lot of stuff in konsole actually still work ? (4 years ago on a K6/200 with KDE 2.x the same command took 1.9 seconds)

What numbers do you get ?

Happy new year
Alex

P.S. some days ago I had to compile the cdrtools written by Joerg Schilling from sources. I downloaded cdrtools sources and tried to build them. They shouted in my face that GNU make is broken and I should install smake written by Joerg Schilling. So I did this. As if we would need another make implementation...
It was installed in /opt/schily/. Hmm. And the readme complained that much to little software installs itself correctly under /opt/. But smake and cdrecord installed correctly under /opt/schily/. During make install it complained again that GNU make is broken and unmaintained and that the nice smake should be used instead.
It didn't make a good impression to me at all. Felt like shouting "I am the only one who does things right". Hmm...
