---
title:   "Looking for an exciting Qt-related job in HPC ?"
date:    2010-08-16
authors:
  - alexander neundorf
slug:    looking-exciting-qt-related-job-hpc
---
Hi,

I thought since others are announcing jobs here, so I can do that too :-)

So, here we go:
The <a href="http://www.itwm.fhg.de/index.php?abt=hpc&inc=hpc">Competence Center for High Performance Computing</a> at the <a href="http://www.itwm.fraunhofer.de">Fraunhofer ITWM</a> in Kaiserslautern has open positions: 
<a href="https://jobs.fraunhofer.de/Vacancies/57538/Description">https://jobs.fraunhofer.de/Vacancies/57538/Description</a>

We are working here on processing terrabytes of seismic data on Linux clusters. This is the big iron, high end stuff :-)
We need people working on the user interface (that's where Qt comes into play), the algorithms, optimizations, communication, etc. You'll be working in team of experts from different fields, from Mathematics over Geophysics to Computer Science.

Being fluent in C++ is a requirement, and if you consider yourself good also in 2 or more of the items below, we are looking forward to your application:
<ul>
<li> Qt
<li> STL
<li> multithreaded programming
<li> lowlevel stuff like SSE, NUMA, etc.
<li> signal processing
<li> Linux
<li> Windows development knowledge also doesn't hurt
<li> software testing
<li> computer graphics, OpenGL, etc.
</ul>

Location is Kaiserslautern, Germany, main work language is German.

Alex
