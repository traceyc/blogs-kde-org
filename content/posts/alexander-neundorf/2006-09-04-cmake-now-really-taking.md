---
title:   "CMake now really taking off ?"
date:    2006-09-04
authors:
  - alexander neundorf
slug:    cmake-now-really-taking
---
Recently it seem a lot of projects are switching to <a href="http://www.cmake.org">CMake</a>.
Just today I read the news about Debian <a href="http://lwn.net/Articles/198171/"> forking cdrecord</a> and almost missed the little note near the end of the article: 
"For our fork we used the last GPL-licensed version of the program code and killed the incompatibly licensed build system. It is now replaced by a cmake system,"

Do you know <a href="http://www.openwengo.org/">OpenWengo</a> ?
It's a VoIP client, also featuring video support, and you know what ? They are also <a href="http://dev.openwengo.com/trac/openwengo/trac.cgi/browser/wengophone-ng/trunk">testing CMake</a>. These guys also have a <a href="http://trac.edgewall.org/">Trac</a> running, which is  great software, check it out if you need an integrated Wiki/Bugtracker/Sourcecontrol web application.

Or take the premier Open Source Desktop Publishing application, <a href="http://www.scribus.net/">Scribus</a> . They switched already to CMake, and apparently without major problems: http://public.kitware.com/pipermail/cmake/2006-April/008847.html

All apps listed above are Qt4/KDE4 applications (except obviously cdrtools), but there are also other projects switching:
<a href="http://boson.eu.org/">Boson</a>, a real time 3D strategy game is now build with CMake.

Do you know <a href="http://www.call-with-current-continuation.org/">Chicken Scheme</a> ? CHICKEN is a compiler for the Scheme programming language, available under the BSD license. Chicken scheme can now also be built with CMake: http://galinha.ucpel.tche.br/chicken/INSTALL-CMake.txt .

CMake also seems to be popular among researchers: both <a href="http://www.robots.ox.ac.uk/~pnewman/TheMOOS/">MOOS</a>, the "Cross platform software for mobile robotics research", as also <a href="http://orca-robotics.sourceforge.net/index.html">Orca,</a> which is an  an open-source framework for developing component-based robotic systems, are using CMake for building their software.
There is also <a href="http://ncmi.bcm.tmc.edu/homes/stevel/EMAN/doc/">EMAN</a>, a software for single particle analysis and electron micrograph analysis developed at the Baylor College of Medicine, and they too went with CMake.

So, are we seeing the beginnings of a big change (at least for developers) ?

On the KDE side, there's <i>one important thing to mention</i>:
from September 12th, CMake 2.4.3 will be required for building KDE. So if you haven't updated yet, do so now !

I also plan to improve the automoc implementation. Currently approx. 20 to 25 percent of the time of a cmake run are spent in automoc. I plan to move this from cmake time to build time and do it only if required. This will cut off quite some time. 

Alex

P.S. and, well, if you really want to, you can even abuse CMake to convert wavs to mp3s:

<code>
file(GLOB wavs "*.wav")

foreach(file ${wavs})
   get_filename_component(basename ${file} NAME_WE)
   exec_program(lame ARGS -b 192 -h ${file} ${basename}.mp3)
endforeach(file)
</code>
