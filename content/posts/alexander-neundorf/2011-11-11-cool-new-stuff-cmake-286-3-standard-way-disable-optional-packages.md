---
title:   "Cool new stuff in CMake 2.8.6 (3): a standard way to disable optional packages"
date:    2011-11-11
authors:
  - alexander neundorf
slug:    cool-new-stuff-cmake-286-3-standard-way-disable-optional-packages
---
Welcome to the third part of this small series about new stuff in CMake 2.8.6, following the <a href="http://blogs.kde.org/node/4495">first</a> and the <a href="http://blogs.kde.org/node/4501">second</a> part.
Most non-trivial projects, whether built with CMake or something else, depend on multiple other packages, which can be either optional or required.
In CMake-based projects all these packages are searched using the find_package() command.

Now there are cases, e.g. for packagers, in which a project should be built without an optional dependency, although the dependency is actually present, e.g. to build an image viewer without mng support, although the mng-library is installed.
In autotools-based projects such optional packages can usually be disabled using ./configure --without-foo .

In CMake there didn't exist such a feature, so in KDE 4 we have the macro macro_optional_find_package(), which wraps the
find_package() command and adds a CMake option to the cache to disable the wrapped find_package() command.
But to get this feature, the developer had to use MacroOptionalFindPackage.cmake, which was installed as part of kdelibs, and he has to actually use this macro.

Now, since CMake 2.8.6, this is a builtin feature of the find_package() command.
To keep CMake from searching for a package named "Foo", set the variable CMAKE_DISABLE_FIND_PACKAGE_Foo to TRUE.
This works for any package, but the variable is, if not set, not in the cache.
So, it has to be set at the initial CMake run:

<pre>
  $ cmake -DCMAKE_DISABLE_FIND_PACKAGE_JPEG=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_LibXml2 &lt;srcdir&gt;
</pre>

For the developer this means, it is not necessary anymore to use macro_optional_find_package() from KDE, but simply the normal find_package() command.

For the packager or user this means, any optional dependency of any CMake-based project can now be disabled using a standard way, via -DCMAKE_DISABLE_FIND_PACKAGE_Foo=TRUE .
