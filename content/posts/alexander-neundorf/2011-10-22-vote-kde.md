---
title:   "Vote for KDE !"
date:    2011-10-22
authors:
  - alexander neundorf
slug:    vote-kde
---
Hi,

you like KDE and you want to support it ?

Here's an easy way to do it:

The german bank ING DiBa is giving away 1000 Euro each to 1000 associations.
Selecting the 1000 winning associations is done via voting.
So, if you vote for the KDE eV there, we have a good chance to be one of the winners :-)

The money will be used by the eV to fund sprints, cover travel costs, run servers, etc.

This is how to do it:
1) go to <a href="https://verein.ing-diba.de/sonstiges/10115/kde-ev"> https://verein.ing-diba.de/sonstiges/10115/kde-ev</a> and click "Stimme abgeben"
2) enter your email and the captcha it asks for and then click "absenden"
3) you'll get an email to confirm your vote - click the link in the email
4) you'll get to a website - click "Stimme abgeben"

You have 3 votes, i.e. you can vote 3 times for KDE (per email address).

Alex
