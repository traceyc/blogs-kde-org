---
title:   "Moving (temporarily) to Albany/New York... any KDE developers there ?"
date:    2007-04-12
authors:
  - alexander neundorf
slug:    moving-temporarily-albanynew-york-any-kde-developers-there
---
Some of you may already know it, but I thought I should announce it here: next Thursday I'll be going to Albany/New York for around 6 months. There I'll have a job for this time at <a href="http://www.kitware.com">Kitware</a>, you know, the guys who are (among others) writing the <a href="http://www.cmake.org"> best buildsystem in the world</a> ! ;-)

This will be the first time I'll be in America, so I'm already very excited and looking forward to a lot of new experiences. And finally almost all of the required paper work is done. This was really a lot of work, both from the American as well as from the German side. Puh.

This paper work is also one of the main reasons why I had less time for KDE in the last weeks. We'll see how my job Kitware will influence my time for KDE. I'll be paid by Kitware, for stuff which Kitware gets paid for. Beside that I'll take the chance and try to learn and see as much from America as possible, so it may very well be that I won't have much more time for KDE than "just" maintaining the buildsystem.

Unfortunately this also means that I won't be able to attend Akademy this year. That's a pity, last year I was for the first time at Akademy and it was a really great event !

Last but not least, are there any KDE developers living in or around Albany/NY ?
Would be nice to meet you :-)


Alex
