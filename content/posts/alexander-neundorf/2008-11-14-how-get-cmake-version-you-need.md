---
title:   "How to get the CMake version you need"
date:    2008-11-14
authors:
  - alexander neundorf
slug:    how-get-cmake-version-you-need
---
KDE svn trunk requires [http://www.cmake.org CMake] 2.6.2 since last week. Version 2.6.2 has been released just a few weeks ago, so maybe there are not yet packages for all distros.

So how do you get CMake 2.6.2 ?
<!--break-->
Of course you can get the sources (from cvs or as source package from http://www.cmake.org/files/ ) and build it yourself.

But you can also get away with less work and just use the binary CMake packages provided by [http://www.kitware.com Kitware]. You think they won't work on your Linux distribution ? I can tell you, they most probably will. At least I haven't found an x86 Linux box yet where they don't work.

So, here's how you can do it
<pre>
   $ cd
   $ wget http://www.cmake.org/files/v2.6/cmake-2.6.2-Linux-i386.tar.gz
   $ tar -zxvf cmake-2.6.2-Linux-i386.tar.gz 
</pre>
And now you are already ready to use it:
<pre>
   $ cd src/my_project
   $ mkdir build
   $ cd build
   $ ~/cmake-2.6.2-Linux-i386/bin/cmake ..
</pre>

Btw., this way it is also very easy to have multiple different versions of CMake on your system. In http://www.cmake.org/files/ you can still get CMake   2.4.x, 2.2.x and back until the ancient version 1.2.
Just download the packages (as shown above) and install them into separate directories e.g. in your HOME or at some other place.

Alex
