---
title:   "Requiring CMake >= 2.6.0 for svn trunk starting August 4th"
date:    2008-07-21
authors:
  - alexander neundorf
slug:    requiring-cmake-260-svn-trunk-starting-august-4th
---
starting August 4th, i.e. Monday in 2 weeks, CMake >= 2.6.0 will be required to build KDE from svn trunk, which will become KDE 4.2 in a few months.

So please update your <a href="http://www.cmake.org">CMake</a>.
In case there are no distribution packages yet, you can get the sources here:
http://www.cmake.org/files/v2.6/cmake-2.6.0.tar.gz

or the binary package, which should work on about every Linux distribution:
http://www.cmake.org/files/v2.6/cmake-2.6.0-Linux-i386.tar.gz
(just unpack it to any place you like, it will work).

Here is the full download page:
http://www.cmake.org/HTML/Download.html
<!--break-->
What will this bring us ?

So many things went into 2.6.0, actually more than I can remember. Just a 
short unsorted list:

-the env. variable CMAKE_PREFIX_PATH, which is used by the FIND_XXX() commands to find installed software (i.e. if you installed kdelibs under the prefix /opt/kdelibs and kdesupport under /opt/kdesupport, set CMAKE_PREFIX_PATH to /opt/kdesupport:/opt/kdelibs and it will be found)

-support for cross compiling, let's see if that helps with getting KDE on small devices

-defining the "link interface" of libraries, so we can limit the indirect dependencies of libs

-much shorter delay between entering "make" and when the first target is built (now a O(N) algorithm instead O(N^2) )

-libraries are linked to with the full path instead of relying on the correct order of the -L and -l flags

-a Qt GUI for configuring projects, editing the cache etc. ("cmake-gui")

-much faster installation, no relinking needed anymore, the RPATH is now patched directly in the binaries

-support for OSX library frameworks, let's see if we want to use that for our libs like kdecore

-improved packaging support, including RPM and DEB packages (still quite early, so needs tester)

-support for creating Eclipse and Code::Blocks projects

-improved documentation, now also for cmake variables and properties, split into separate man pages, and now also supports creating documentation for custom cmake modules (see http://api.kde.org/cmake/modules.html)

-bug fixes, minor features, and everything I forgot...

Alex

P.S. the last new cmake version we required was 2.4.5 on March 19th, 2007, i.e. 16 months ago, so I think we did quite good with that :-)
