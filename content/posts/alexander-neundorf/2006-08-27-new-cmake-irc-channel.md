---
title:   "New: CMake IRC channel"
date:    2006-08-27
authors:
  - alexander neundorf
slug:    new-cmake-irc-channel
---
Wanna chat directly with other cmake users ? Ask questions ? Exchange your experiences ? <!--break--> Share your cmake scripts with others ? Let the world know how much you like cmake ? How it made you understand your buildsystem again ? How it brought the fun back to hacking ? And how it finally brought world peace ? ;-)

Then just come in and join at #cmake , a new IRC channel created just today on Freenode.
You're welcome ! :)

Alex
