---
title:   "How to buy a Linux notebook in Germany, part II"
date:    2008-04-17
authors:
  - alexander neundorf
slug:    how-buy-linux-notebook-germany-part-ii
---
A few days ago I asked in the blog "where to buy a 14 inch non-glossy notebook with Linux in Germany".
I received quite a few responses and emails, thanks a lot for the support ! :-)
Now I (almost completely) succeeded and a notebook is ordered.
For those of you looking for something similar, here are the details.
<!--break-->
Finally I ordered a non-glossy 15.4 inch (that's why the "almost") Inspiron with Ubuntu from Dell.
So this will count as one machine sold with Linux, which is a very good thing :-)
I got some nice upgrades, so the machine will be well equipped for a good price. Intel Core 2 Duo, Intel Graphics, Intel WLAN, and according to Dell *everything* in this machine works under Linux, including e.g. Bluetooth and the integrated webcam. 

Another option is the 13.3 inch Dell XPS, which you can also order with Ubuntu. I didn't decide for this one because it has a glossy screen and is a bit more expensive.

The only bad thing: Dell wasn't able to sell me a 14 inch Vostro with Linux, this would have been my first choice :-/

But there are also other options beside Dell:

There is <a href="http://www.zareason.de">ZAReason Germany</a>, who sell only Linux systems. This is <i>very</i> nice, but they had only one type of notebook available.

There is also <a href="http://www.zareason.com">ZAReason USA</a>, who also sell only Linux systems. They <b>do</b> ship to Germany. They offer a 14 inch notebook, but the screen is glossy, and it would come with German keyboard. I could change the keyboard against a German one, but I didn't feel like fiddling around with this. The contact in the USA (and also in Germany) was very nice and fast.
Even including shipping to Germany the notebook would still have been not more expensive than the german alternatives. I didn't order that one because of the glossy screen, I don't know about the customs and the US keyboard (and the quality of my old Dell notebook was really good).

You can also get a notebook shipping with Linux e.g. from Acer, but there it's the low-end notebook, and that's not what I wanted (EeePc is also too small).

<a href="http://www.zepto.com">Zepto</a> sells notebooks without operating system in Germany. My new Dell notebook will have a Alpin-white glossy cover, compared to that the Zepto notebooks look a bit boring (and it wouldn't have increased the number of systems sold with Linux).

<a href="http://www.one.de">One</a> also sells notebooks without operating system and they are not even expensive. I didn't buy one there because they didn't seem to have a lot of configuration options, so I would have ended up with 1 15 inch glossy screen and only 512 MB RAM.

Conclusion ?
Well, instead of a 14 inch notebook I'll end up with a 15 inch notebook (which will have a lower resolution than the 6 year old notebook, which had 1400x1050), but beside that  I got what I wanted :-)
There are not many options if you don't want to buy a Windows license, but there are at least some, and this is good :-)


Hope this helps somebody :-)

Alex
