---
title:   "Re: Anatomy of a standalone KDE Mac package"
date:    2008-11-11
authors:
  - alexander neundorf
slug:    re-anatomy-standalone-kde-mac-package
---
There was this blog entry about the state of packaging for the Mac.
Containing "Then you think to yourself, if only those nice boys and girls in KDE-land had a native solution for installing their software,"

So, if you want a native solution, please join kde-buildsystem and let's see what needs to be done to get the packaging support you need for OSX. I don't have an OSX box here, so your help is needed.

Looking forward to hearing from you :-)
Alex
