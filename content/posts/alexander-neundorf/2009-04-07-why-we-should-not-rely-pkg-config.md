---
title:   "Why we should not rely on pkg-config..."
date:    2009-04-07
authors:
  - alexander neundorf
slug:    why-we-should-not-rely-pkg-config
---
I'm just trying to compile Battle of Wesnoth (yes, for me gaming means compiling games... ;-) ).
I just built it with CMake, which first complained that it didn't find Lua 5.1. I checked, it really wasn't there. So I downloaded the sources for lua, make, make install, and now lua is in /usr/local/.
Then I run cmake again on Wesnoth and it happily finds Lua, so CMake now succeeds and I can build Wesnoth.

To have more fun, I'm trying right now the autotools build for Wesnoth:
<pre>
checking for LUA... checking for LUA... configure: error: Package requirements (lua5.1 >= 5.1) were not met:

No package 'lua5.1' found

Consider adjusting the PKG_CONFIG_PATH environment variable if you installed software in a non-standard prefix.
</pre>

So it uses pkg-config to find Lua, and fails with that. Why ? Because Lua doesn't install a pkg-config file:
<pre>
lua-5.1.4$ sudo make install
cd src && mkdir -p /usr/local/bin /usr/local/include /usr/local/lib /usr/local/man/man1 /usr/local/share/lua/5.1 /usr/local/lib/lua/5.1
cd src && install -p -m 0755 lua luac /usr/local/bin
cd src && install -p -m 0644 lua.h luaconf.h lualib.h lauxlib.h ../etc/lua.hpp /usr/local/include
cd src && install -p -m 0644 liblua.a /usr/local/lib
cd doc && install -p -m 0644 lua.1 luac.1 /usr/local/man/man1
hammer:~/src/packages/lua-5.1.4$     
</pre>

So, now I'm sitting here fiddling around with this stuff, even although Lua is in the number 2 location where you would expect it (i.e. /usr/local, not /usr)...

So, let's just not rely on an optional additional tool for our builds.

Alex