---
title:   "CDash: kdelibs dashboard online"
date:    2008-06-25
authors:
  - alexander neundorf
slug:    cdash-kdelibs-dashboard-online
---
Since a few days <a href="http://www.kitware.com">Kitware</a> is hosting a <a href="http://www.cdash.org">cdash dashboard</a> for kdelibs. The ctest config file in kdelibs svn has been adapted accordingly.
So if you run now "make Experimental" in kdelibs the build and test results will be submitted to http://www.cdash.org/CDash/index.php?project=kdelibs . 
It would be very useful if we could get nightly builds for OSX, FreeBSD, Solaris and Windows, so we can make sure they stay compiling.

I tried it with an Experimental build. On the first run, the tests where not built, so they were all marked as "Not Run". To enable them the cmake option "KDE4_BUILD_TESTS" has to be enabled. I did that and did another "make Experimental". Now the tests where executed. I did this while working in a KDE3 desktop.
Apparently some things fail then, and I had to kill a few processes, otherwise some tests where hanging for a long time. I guess we could set the timeout lower, to 1 minute or so. But ideally the tests should succeed nevertheless.

So, we want you to set up a Nightly build of kdelibs fopr the dashboard, preferably on a platform != Linux. If you have questions how to do this, come over to kde-buildsystem@kde.org :-)

Alex

