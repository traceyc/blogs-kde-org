---
title:   "How to debug udev/HAL/dbus/solid problems ?"
date:    2009-02-02
authors:
  - alexander neundorf
slug:    how-debug-udevhaldbussolid-problems
---
Hi,

in the good old times(TM) using Linux was simple.

If you wanted to access some drive, e.g. CD-ROM, floppy disk (you know these 3.5" square plastic things which could hold a whopping 1.44 MB of data, if you formatted them with special tools you could push even a bit more on it), you just had to know the device file and the file system and if you knew that, it just worked (TM):

 $ mount -tiso 9660 /dev/hdb /media/cdrom 

Now times have changed and everything works automatically and without requiring that knowledge. This is very nice (as long as it works). Now, on my notebook which has OpenSUSE 11.1 (great release, KDE 4.1 looks beautiful and is stable), everything I connected to it was mounted automatically until a few days ago.
Now it doesn't do that anymore and I'd like to find out why.

The thing is, there was some problem with permissions and scanning, i.e. only when running as root the scan programs found the scanner. Then one week ago, I changed some things, e.g. the entry for the usbfs in /etc/fstab, but I'm quite sure I also changed it back to the way it was before.

Anyway, yesterday I plugged in a USB memory stick, solid showed me that something has been plugged in, and I clicked on it. Dolphin opened, but it showed $HOME/Documents/, instead of the mounted USB drive. Same for an SD-card in the internal card reader (probably also connected via USB, so the same problem).

Now I feel a bit lost. How can I debug this ? Which part doesn't work here ? Is it udev (I don't think so), HAL (maybe), dbus (shouldn't), or solid (maybe ?). Or is some *Kit involved and it is a permission problem ?

So, if you know how to debug such problems, please let me know :-)

Alex
