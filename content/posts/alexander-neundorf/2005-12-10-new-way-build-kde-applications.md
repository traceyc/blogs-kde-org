---
title:   "a new way to build KDE applications"
date:    2005-12-10
authors:
  - alexander neundorf
slug:    new-way-build-kde-applications
---
Hi,

in the screenshot you can see a different kpager than the one you know.

[image:1667]

So, what's different ?
This kpager is built from the same sources, but neither using autotools, nor unsermake, nor scons. It's built using cmake ( http://www.cmake.org ).
Here's how I did it:

<pre>
hammer:~/src/kdecmake/samples$ svn co https://neundorf@svn.kde.org:/home/kde/branches/KDE/3.5/kdebase/kpager
A  kpager/hi16-app-kpager.png
A  kpager/uninstall.desktop
A  kpager/kpager.desktop
A  kpager/hi48-app-kpager.png
A  kpager/config.cpp
A  kpager/windowdrag.cpp
A  kpager/kpagerIface.h
A  kpager/kpager.cpp
A  kpager/desktop.cpp
A  kpager/config.h
A  kpager/windowdrag.h
A  kpager/kpager.h
A  kpager/desktop.h
A  kpager/main.cpp
A  kpager/TODO
A  kpager/Makefile.am
A  kpager/hi22-app-kpager.png
A  kpager/hi32-app-kpager.png
 U kpager
Checked out revision 487266.
hammer:~/src/kdecmake/samples$ cd kpager
hammer:~/src/kdecmake/samples/kpager$ am2cmake
hammer:~/src/kdecmake/samples/kpager$ cmake .
-- Check for working C compiler: gcc
-- Check for working C compiler: gcc -- works
-- Check size of void*
-- Check size of void* - done
-- Check for working CXX compiler: c++
-- Check for working CXX compiler: c++ -- works
-- Looking for XOpenDisplay in /usr/X11R6/lib/libX11.so;/usr/X11R6/lib/libXext.so
-- Looking for XOpenDisplay in /usr/X11R6/lib/libX11.so;/usr/X11R6/lib/libXext.so - found
-- Looking for gethostbyname
-- Looking for gethostbyname - found
-- Looking for connect
-- Looking for connect - found
-- Looking for remove
-- Looking for remove - found
-- Looking for shmat
-- Looking for shmat - found
-- Looking for IceConnectionNumber in ICE
-- Looking for IceConnectionNumber in ICE - found
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Looking for pthread_create in pthreads
-- Looking for pthread_create in pthreads - not found
-- Looking for pthread_create in pthread
-- Looking for pthread_create in pthread - found
-- Configuring done
-- Generating done
-- Build files have been written to: /home/alex/src/kdecmake/samples/kpager
hammer:~/src/kdecmake/samples/kpager$ make
Generating desktop.moc
Generating kpager.moc
Generating config.moc
Generating kpagerIface_skel.kidl
Generating kpagerIface_skel_skel.cpp
Scanning dependencies of target kpager
Building CXX object CMakeFiles/kpager.dir/desktop.o
Building CXX object CMakeFiles/kpager.dir/kpager.o
Building CXX object CMakeFiles/kpager.dir/config.o
Building CXX object CMakeFiles/kpager.dir/windowdrag.o
Building CXX object CMakeFiles/kpager.dir/main.o
Building CXX object CMakeFiles/kpager.dir/kpagerIface_skel_skel.o
Linking CXX executable kpager
hammer:~/src/kdecmake/samples/kpager$ ./kpager
ERROR: kpager is already running!
hammer:~/src/kdecmake/samples/kpager$    
</pre>
So what did I do ? I checked out <b>only</b> kpager from kdebase and then ran the am2cmake script in it, which "translated" the Makefile.am to the CMakeLists.txt as required by cmake. Then cmake runs and creates the Makefiles. Finally make is invoked and builds kpager. When it's finished, kpager was started. But since there was already a kpager running it complains and exits.

Wanna know how the CMakeLists.txt created by am2cmake looks like ? Here it is (slightly edited to use less space and some comments added):
<pre>
# needed for finding Qt, KDE and the tools like moc, etc.
include(../../FindKDE3.cmake)
include(../../KDE3Macros.cmake)

# add the current directory to the include path
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR}  )

# list the source file
set(kpager_SRCS desktop.cpp kpager.cpp config.cpp windowdrag.cpp main.cpp )

# automoc, more to say ?
kde3_automoc(${kpager_SRCS})

# generate the dcop stuff
kde3_add_dcop_skels(kpager_SRCS  kpagerIface.h )

# create the binary named kpager from the sources listed in kpager_SRCS
kde3_add_executable(kpager ${kpager_SRCS})

# and link it to qt, kdecore and kdeui
target_link_libraries(kpager  ${QT_AND_KDECORE_LIBS} kdeui )

# install kpager into the bin/ dir relativ to the installation directory
install_targets(/bin kpager )

# install the desktop file
install_files( /share/applications/kde FILES kpager.desktop )

# install the icons
kde3_install_icons( hicolor )
</pre>

If you ask me, building KDE applications hasn't been that easy for a long time now.
After some more polishing I'll try to get the am2cmake script and the FindKDE3.cmake included in cmake, so that everybody will have them available when installing cmake. 
Maybe I'll also try my luck on KDE4.

Bye
Alex

