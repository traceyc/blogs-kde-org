---
title:   "From Akademy back to K-Town"
date:    2008-08-17
authors:
  - alexander neundorf
slug:    akademy-back-k-town
---
This year was just my second Akademy. And as my first one,
two years ago in Dublin, it was a really great experience again.
Actually I don't know where to start.
To put it in one sentence, it almost feels like a huge family. Hundreds of people from all over the world come together and meet, they know each other, they work towards the same goal, they appreciate each other.
Where else do you have that (for such a big number of people) ?
<!--break-->
But when I arrived back in K-Town (to some people also known as Kaiserslautern) I had the same strange feeling again as two years ago coming back from Dublin. It felt like being back from a parallel world or something. "Post Akademy blues" ?  Suddenly there were not everywhere you go, KDE people around. Do you know that feeling ?

One thing I noticed is that it seems in the meantime I'm one of the older guys in KDE. Oh well, that's ok. And it's nice to see the young guys coming into KDE. Well, usually I feel somewhat depressed (not sure that's the best word)
 when I see how good they already are with let's say 18 years. I think I started learning C/C++ with 20 (Pascal with 16 or so). But, that's great !
And, it's always so astonishing to see such a big group of highly motivated and talented people in one place.
I think this must be far above average. Well, I guess for somebody to start working on free software in his spare time without being payed, being highly motivated and talented is a prerequisite. That's probably the reason why we get so much done with so few resources and nevertheless top results. 
I guess you just can't get this in a company where people apply for a job because they need money for their daily lives (as everybody does). There's a difference between going to work to a job you like but which you do because you get paid, and working on a free project because you feel the urge to do it from within.
This doesn't apply only to our developers, but of course also to our artists, promo team, usability specialists, writers etc.
You all rock !

What did I do at Akademy ?

I met a lot of people, a lot I already knew and as always a lot of new faces (at least for me). This is always a lot of fun and I hope (well, I'm sure) we'll see us again soon ! :-)
Did I mention that Belgian fries are great (and also huge) ? And I recommend to add mayonaise, so you get your daily share of fat ;-)

I had planned a few things to work on, but didn't get around to finish all of them. Instead I spent a lot of time talking to people, helping them (you know, <a href="http://www.cmake.org">CMake</a> stuff) and discussing. I think this is even more valuable to do 
at Akademy than coding. I can do the coding also from home, but at Akademy I can talk to people and explain things. 
This should pay off more. :-)
Actually I can't remember any bad comments about CMake at Akademy, from many people I heard e.g. that they started to introduce CMake at their workplace, or that they really like that the buildsystem has become so much easier to grasp with CMake than before with autotools.

I also planned to get a full nightly build for KDE working: building qt-copy, building kdesupport, submitting a <a href="http://www.cdash.org">dashboard</a> for it, if that succeeded, building kdelibs and submit to a dashboard, and if that succeeds, finally building kdepimlibs, as the first module which uses kdelibs, and, of course, submit a dashboard for it. Unfortunately I didn't find the time to get this done. So we will get this later. Then we should run this on the more "exotic" platforms we support, as e.g. Windows, OSX, Solaris and FreeBSD nightly. It would be cool if we would have a big fat box with virtual machines for the different operating systems to do that. Maybe this would have to be an Apple server, or are there any virtual machines which can run OSX ?

Monday was the general assembly of the KDE eV. It was a long meeting, and not very exciting. But the decisions we made are important for the project as a whole (dealing with donations, legal stuff, etc.). So the eV has an important, if maybe not really exciting role for KDE.

We released 4.0 in January, which was important for us as a project to mark the point in time from which we will stay compatible and stable. That's what it was. It wasn't intended for actual everyday use by normal users, but that was kind of hard to communicate. Before releasing it we had long discussion whether it should be named 4.0 or 3.90 or KDE4 0.1 or KDE 4.0 Preview or something. Maybe the message that it's not for everyday use would have come across clearer with a differrent name, but we decided for "4.0", so it doesn't matter now anymore. 
In the meantime we released 4.1, and this is really taking off. Many people at Akademy were using it for their everyday work. I saw it working on Linux, Solaris, Windows, OSX, not sure I saw a FreeBSD box. So we are really getting there. But as others have said, this is just the beginning.
We, as KDE, are in unique position.
We have a complete software stack, ranging from hardware abstraction, multimedia, file managers, editors to IDEs, web browsers, multimedia applications and of course Marble - which works on all major operating systems. And which is IMO the most integrated set of software in existance. 
<i>Second to none.</i>

Give this let's say 2 years, then everything is rock stable, on all platforms, and the applications will use the full power of the new underlying frameworks (the "Pillars").
We may be up to something big !

Alex
