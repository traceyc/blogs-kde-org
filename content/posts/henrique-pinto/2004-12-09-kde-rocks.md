---
title:   "KDE Rocks!"
date:    2004-12-09
authors:
  - henrique pinto
slug:    kde-rocks
---
I got an ADSL connection last week, but as my operating system was not supported, I was left to configure it myself. After around 12 hours fighting with my network interface card, I gave up, plugged-in another hd and installed an old copy of windows on it so that I could at least get the connection working via USB and read my e-mails. And so for two days I was kind of forced to use Windows 2000. Using it was not very pleasant, but in the end it was a good thing: it reminded me how great KDE is.

Seriously, I can't imagine how can anyone survive without "gg:", "wp:" or using the "Run command..." dialog as a simple calculator... Firefox is a nice browser, but is way too slow when compared to Konqueror or IE, and the lack of an "up" button is very irritating (ok, you can use an extension for adding one, but when I tried that the whole program simply stopped working). Thunderbird is a pain to use if you ever used KMail... And I can't imagine how people can use IM not using Kopete... I missed also a lot of other programs, like Konsole and its tabs and JuK, but, interestingly enough, what I missed most were those small details that only KDE has.<!--break-->

Tabbed File Management simply rocks. And so do those search bars that appeared everywhere in KDE (can we have one for Konqueror's file views?). KIMProxy is also quite handy. I could go on for hours and hours citing interesting features. There's simply too many things in KDE I can't imagine using a computer without. It is addictive, and that is actually a nice thing. Thanks to everyone who helped KDE to get where it is today!

PS: I've been wondering for the whole day what that "Application FS", whose description is just "Details to come...", in the KDE 3.4 feature plan is about...

PPS:There was actually one thing I'm missing from my Windows experience. It isn't actually a Windows component, but rather MikTex's "texify" utility. Ok, Kile already has similar functionality, but unfortunately I can't use it, as KatePart performs very bad on my PC when dealing with large documents. I'm trying to find similar functionality for tetex, but I still haven't found any. Perhaps I should write something on my own -- it doesn't seem very difficult...

PPPS: The problem with my network interface card was that it was configured to use IRQ 10, and that was conflicting with the sound card. Just setting a jumper (it's an old card) solved the problem.

PPPPS: Do "PPPS" and "PPPPS" exist?
