---
title:   "Refactor the Lightning"
date:    2009-07-15
authors:
  - Killerfox
slug:    refactor-lightning
---
Another week into SoC, and status update for anyone mildly interested :)

So what has happened in the last week? Well I worked in the KDE hacking rooms at Akademy :).

<li>I merged my branch into trunk</li>
<li>Found a bug related to document text selection and its appropriate context</li>
<li>Added the functionality to CodeGenerator, so that it could be executed by a user, or another generator</li>
<li>Started writing Unit Tests</li>
<li>Reorganizing DocumentChangeSet code</li>

So what's the holdup? Where is all the cool stuff you promised?
<!--break-->
Well I have been stuck with two nasty issues when making the unit tests(which might actually be one and the same). For some reason only on my unit tests I have been unable to initialize the KDevplatform core. I get a few crashes that relate to KIcons not being able to load properly. We have looked at it for wuite some time, and have no idea why this is only happening on my tests.

Meanwhile I have moved some of the tests that were in other modules, and started drafting tests of my own, however it is very hard to do so without actually being able to test.

What is on the horizon?
<li>Finish writing unit tests</li>
<li>Add capability for layered changes (IE make one text change, then incorporate it, recreate duchain, and continue adding edits)</li>
<li>Create a Document change user review widget</li>
<li>Finish private implementation codegen in time for beta5</li>
<li>Code Generator concept manager</li>
<li>???</li>
<li>Profit!</li>

Well that is it for this status report, come back next week for more! :)