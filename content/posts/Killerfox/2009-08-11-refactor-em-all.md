---
title:   "Refactor 'em All"
date:    2009-08-11
authors:
  - Killerfox
slug:    refactor-em-all
---
The week before last into GSoC, and this is how things are:

Sadly I had very little time to work on the project last week because I had to be out of town for the most part. Despite of this setback though, there was a significant improvement, which is the user review widget. What this means is: Screenshot time!
<!--break-->
Here I will be using the half-complete Private Implementation(Which takes a class, and converts it's private members to the PIMPL idiom) generator.

First off Is the selection of a context in a class:
<img src="http://lh3.ggpht.com/_KJoj6krSo2A/SoD4SUltK5I/AAAAAAAABHE/OgpFRVlZwyA/s800/pimpl1.jpg" />

(<b>UPDATE:</b> This image is apparently not displaying in a lot of cases, here is the link: <a href="http://lh3.ggpht.com/_KJoj6krSo2A/SoD4SUltK5I/AAAAAAAABHE/OgpFRVlZwyA/s800/pimpl1.jpg">Image</a>)

Then either selecting the Private Implementation under Code, or using the Hotkey, the private implementation dialog pops up:
<img src="http://lh3.ggpht.com/_KJoj6krSo2A/SoD4S23n3bI/AAAAAAAABGY/Z3FVHuWf90g/pimpl2.jpg" />

After setting the options, the generator runs, and the changes widget will pop up. If KDevelop is compiled with Kompare support (Which is only available if you have trunk KDE ATM), then the changes widget will start displaying the differences between the original file, and the generated one:
<img src="http://lh4.ggpht.com/_KJoj6krSo2A/SoD4U6SEgyI/AAAAAAAABGc/9Pc2_kyfeH8/pimpl3.jpg" />

If you want to edit any part of the file(Or if you don't have Kompare support enabled), then by Clicking on the Edit button, the Kompare widget will switch over to a Kate widget, giving you freedom to edit the generated file before it is saved to the disk:
<img src="http://lh6.ggpht.com/_KJoj6krSo2A/SoD4YAH5mwI/AAAAAAAABGk/bg2kJLY-HJk/pimpl5.jpg" />

Clicking on the View button will now update the comparison between the original file, and the user edited file, all without a single write to the disk:
<img src="http://lh5.ggpht.com/_KJoj6krSo2A/SoD4a09xF7I/AAAAAAAABGo/hdusqM8WLH8/pimpl6.jpg" />
By clicking Ok, the changes will be commited to the disk.

Last week of GSoC will be used to finish up the generator, and documentation on the tools available for automatic code generation.