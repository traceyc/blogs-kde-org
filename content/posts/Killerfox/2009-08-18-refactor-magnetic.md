---
title:   "Refactor Magnetic"
date:    2009-08-18
authors:
  - Killerfox
slug:    refactor-magnetic
---
Well it is officially the end of GSoC. This update brings you the wrap-up of what happened in the project, what got done, what didn't get done, and what is next on the wonderful world of automated code Refactoring in C++.

<b>What Got Done</b>
<!--break-->
Most of my work on this project was focused towards having a stable, and easy to use framework for developing refactoring concepts for a language in KDevelop.The language independent part of the framework consists of:
<li>CodeGenerator - This is the base class for all generators. A simple concept, to take care of the simple management tasks (Like finding the context of the selected region, and querying the file AST), while letting the refactoring concept worry about generating the code </li>
<li>DocumentChangeSet - DocumentChangeSet was implemented for simple tasks, and one-time changes. Now DocumentChangeSet can handle multiple layered changes, merging of changesets, and management of temporary files where changes are applied</li>
<li>ApplyChangesWidget - The widget to display the changes to the user, and allow them to edit any inconsistencies before committing the changes to the disk</li>
<li>CodeRepresentation - A CodeRepresentation allows a generator to remain agnostinc of the best current representation for a code "file" (from editor, from file, or generated in memory). There were only small changes done here.</li>

The C++ part of the framework consists of:
<li>DuChain - AST mappings: The DUChain is a great abstract representation of code, but it only represents the code in very broad temrms of Declarations and Contexts. When there is a need of more precise information on the abstract representation of the code, then it is time to call the Abstract Syntax Tree. When the parser runs and the AST is built, the refactoring generators can ask to keep the AST, and map it meaningfully to the DUChain.</li>
<li>CodeGeneration Unit Testing - Up to now unit testing for the DUChain was done with parsing of simple strings, without doing any of the importing, or running all of the kdevelop core. However to test code generation there needs to be mappings between files, and artificial code needs to be managed (As to not trash code samples on the disk, everything is done in memory).</li>
<li>Conversion of ClassGeneration to a detached generator: code reuse is a good thing, and layared changes are possible, so why not make use of what was already there?</li>

<b>What <i>didn't</i> get done</b>

<li>More rigorous testing on the Framework: Getting the unit tests to work took considerably more time that what was expected, there were a lot of hoops to jump through, and this left for little time to implement rigorous testing</li>
<li>Complete implementation of MakeImplementationPrivate generator: This was for most intents and purpouses my testing grounds on user interaction and large scale testing. So work on it got repeatedly delayed because a bug on the framework was found</li>
<li>Implementation of Other generators: I had some ideas of generators I wanted to implement, however because of time constraints, I could not implement generators such as: Extract function, Clean #include dependancies & Order function Implementations</li>

<b>What next?</b>

I will continue to work on this project(at least as far as school load will permit me). Because I want to finish what was mentioned in the not done section. More immediately, I want to make sure that at least the MakeImplementationPrivate is in prime usable state before KDevelop 1.0 rolls around.


Thank you KDE community for this great experience. I want to stick around and keep contributing to this great project, and wonderful community. It has been very educational!