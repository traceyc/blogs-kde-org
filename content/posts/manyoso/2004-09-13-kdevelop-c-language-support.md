---
title:   "KDevelop C# Language Support"
date:    2004-09-13
authors:
  - manyoso
slug:    kdevelop-c-language-support
---
So, I've been working on some kdevelop language support C# for the past few days.  <a href="http://www.mit.edu/~manyoso/kdevelop/build.png">I</a> <a href="http://www.mit.edu/~manyoso/kdevelop/newcsharpproject.png">already</a> <a href="http://www.mit.edu/~manyoso/kdevelop/options.png">have</a> <a href="http://www.mit.edu/~manyoso/kdevelop/options1.png">some</a> <a href="http://www.mit.edu/~manyoso/kdevelop/references.png">pretty</a> <a href="http://www.mit.edu/~manyoso/kdevelop/run.png">interesting</a> <a href="http://www.mit.edu/~manyoso/kdevelop/execute.png">screenshots</a>, but I recently discovered that KDevelop is going through a major change in the way it handles different build systems.  Roberto Raggi is working on a <a href="http://lists.kde.org/?l=kdevelop-devel&m=109487786921457&w=2">New Project Manager (NPM)</a> that will form the basis of a common project editor widget for all the various languages.  Individual language/buildsystems will then customize the interfaces to this NPM in order to provide language/buildsystem specific features.  It is a great idea as it will cut down on the duplication of code in KDevelop and provide some standard and uniform UI widgets.  Since I found out about it, I've been porting my initial C# build part to this new architecture.  More...

I've also been hacking on a ANTLR parser to provide code completion and other various goodies.  It is all going well, but the crux of it is that I'll have to hold back the commit until the port of the three day old code is finished ;)  That might be a little longer than anticipated since the NPM interfaces aren't finished (in particular we don't have interfaces or specs for an exporter) and I'm now working on new nifty features like the code completion.

Questions:
<ol>
<li>What language are you using to implement?
<li>What toolchains are you using/have support for?
<li>Any build system preferences?
<li>Still working on Qt# and KDE#?
</ol>

Answers:
<ol>
<li>C++ silly ;)  I had thought of doing it in C# when KDE# was ready, but I want it now and besides this is easier since KDevelop is already in C++.
<li>Portable.NET, Mono and Rotor.  You'll have the choice of compilers, assemblies and runtimes.  You can mix and match to your hearts content.
<li>Yes. For now, I've decided to default with Portable.NET's csant.  In the future, I'll heavily consider using Marcus Urban's cool new msbuild clone project.  Marcus has been working on it and doing a great job.  This has a good chance of becoming the ant-like standard for CIL languages.
<li>Yes, absolutely, but working on this too.  We hope to have this all ready by KDE 3.4 so we can have a nice C# option for KDE development.  Wish us luck!
</ol>

Cheers.