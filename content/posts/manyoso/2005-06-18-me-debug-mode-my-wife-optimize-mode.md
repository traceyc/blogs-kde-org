---
title:   "Me? Debug mode. My Wife? Optimize mode."
date:    2005-06-18
authors:
  - manyoso
slug:    me-debug-mode-my-wife-optimize-mode
---
Like many software developers I am a lazy person by nature.  There is a theory which states that the primary motivation which drives software developers to work so hard is in fact, laziness.  This only seems like a paradox until you think about it for a moment.  Probably the single greatest moment in the history of laziness was the invention of the remote control.  Such a sublime discovery could not have occurred but for the basic primal human need to be a lazy slob of a couch potato every once in awhile.

Which brings me to the crux of the problem.  My wife is not an inherently lazy person.  Sure, she can laze out from time to time with the best of them, but generally she is intent on squeezing out every last ounce of productivity from her normal work day.    Even the weekends aren't safe from her efficiency algorithms.  I know, I know, the horror!

Me?  I'm almost _always_ in debug mode.  I would rather be slow and sure than efficient.  I hate to be late and I hate to wait...  while my wife insists upon cutting it as close to the bone as possible.  She will take every shortcut and try to optimize her every second even when this results in frequent crashes.  I'd rather work on optimizations after the design is completed.  Scratch that, in my day to day life, I don't really care to optimize anything.  Ever.  I just like my tried and true.

Any other KDE'ers experience this with their significant others?<!--break-->