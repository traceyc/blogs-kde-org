---
title:   "Apple Developing CLR Alternative Into GCC?"
date:    2005-11-19
authors:
  - manyoso
slug:    apple-developing-clr-alternative-gcc
---
People who know me understand that I'm <a href="http://llvm.cs.uiuc.edu/">a big fan of LLVM.</a>  Well, big news... Apple has hired the main developer and is now <a href="http://gcc.gnu.org/ml/gcc/2005-11/msg00888.html">urging LLVM to be integrated as an alternative backend of the GCC compiler suite.</a>

For those of you who haven't had the pleasure of introductions: LLVM is an alternative compiler framework and toolchain that provides, among other things, an optimizer, a code generator, a JIT, a bytecode IR, and a ported g++ frontend.  Or, as the LLVM website has it, LLVM is a compilation strategy, a virtual instruction set, a compiler infrastructure, and a virtual machine that does not require garbage collection or run-time code generation.  It is really a hell of a project with lots of moving parts complete with plenty of academic papers documenting the goings on.  Oh, and it is all written in c++.

However, the really crucial point for this post?  There are several indications that LLVM has big plans for bringing CLR like functionality to GCC language frontends.  Check out these points from <a href="http://gcc.gnu.org/ml/gcc/2005-11/msg00888.html">Chris Lattner's recent email:</a>

<code>
"Recently, at Apple, I have been working on a new version of the llvm- gcc translation layer, built on GCC 4. This implementation links the LLVM optimizers and code generator directly into the GCC process, replacing the tree-ssa optimizers and the RTL code generator with the corresponding LLVM components when enabled. The end result is a compiler that is command line compatible with GCC: 'gcc -S t.c -o t.s' does exactly what you'd expect, and most standard command line options are supported (those that aren't are very specific to the design of the RTL backend, which we just ignore). I plan to have this work committed to the Apple branch within a month."
</code>

I worked on LLVM last year to allow Qt4 to be compiled using LLVM's old g++ frontend, but it required some tweaks because LLVM didn't support inline asm.  Apple is now fixing this and with this merge, we're going to see Qt and perhaps all of KDE running under a JIT.  Should be interesting.

<code>
"The IR supports several features that are useful to various communities, including true tail calls, accurate garbage collection, etc. The IR is continuously evolving to add new features, and we plan several extensions in the future."

"In the future, I anticipate work to extend the LLVM to be capable of capturing higher-level information (e.g. that needed for dynamic dispatch optimizations and more alias information), as well as supporting new targets and the usual collection of miscellaneous improvements to the current system."
</code>

<code>
"LLVM does capture some important pieces of high-level information, and is continuing to add more. There is a fundamental design tension here between building a compiler that supports multiple languages (in a link-time context, that means it can link translation units from multiple languages together and optimize across the boundary) and building a compiler that supports only one."
 </code>

Look at that last quote in particular.  I think Chris is pointing out that he doesn't want LLVM to foreclose on the possibility of a CLR alternative.  I talked about this possibility <a href="http://blogs.kde.org/node/554">a little over a year ago.</a>  After all, a toolchain that can <b>"link translation units from multiple languages together and optimize across the boundary"</b> is pretty much the whole point of the the CLR.  Or, as I like to say, <a href="http://blogs.kde.org/node/1150">building the language binding into the compiler toolchain where it properly belongs :)</a>

Still a long way off, but I find this hopeful.  Imagine being able to link KDE/Qt C++ apps with KDE/Qt Java libs if you will.
<!--break-->