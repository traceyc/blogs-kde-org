---
title:   "A QtWebKit KPart is no answer for a KDE browser"
date:    2009-07-01
authors:
  - manyoso
slug:    qtwebkit-kpart-no-answer-kde-browser
---
<b>Disclaimer: I have no desire to re-ignite KHTML vs WebKit arguments.  Rather, the purpose of this blog post is to hopefully enlighten a technical question.</b>

Over the last few months I've heard many KDE developers in various forums bemoan the lack of a working and stable WebKit KPart.  The motivation behind this complaint seems to be that KDE folk want a WebKit browser option for KDE.  Thus the naive solution is to just get the WebKit KDE KPart in shape.  Given this motivation... the solution is wrong IMO.

I can speak from some experience here.  I've been working on QtWebKit for quite awhile and have also worked - in the past - on the WebKit KPart plus Konqueror integration that Simon started.  For a time, it was building and running just fine.  You could install it and change a configuration file and Konqueror would render using QtWebKit.  However, the integration was *far* from complete.  Plumbing the sources of Konqueror I learned a nasty secret: Konqueror is highly KHTML API specific.  Konqueror has deep integration with KHTML that goes far above and beyond the KPart API.  Creating a QtWebKit KPart is woefully insufficient for the purpose of providing anything more than a basic HTML viewer.  

A simple HTML viewer is no where close to a fully modern desktop browser.

This all makes sense if you stop to think about the history of Konqueror.  Konqueror is a generic desktop shell.  It is designed to allow basic viewing of various documents in various formats.  Of course, it has become much, much, more than that.  And the key to this growth of features is Konqueror's steady adoption of API's above and beyond the generic KPart API.

Which brings me to my point: if parts of the KDE community truly want a modern browser based on QtWebKit they'd best be looking at solutions beyond Konqueror.  Otherwise you are left with two hacky solutions:  make a QtWebKit KPart that is API compatible with KHTMLPart OR migrate Konqueror source to make it less dependent on KHTMLPart.  The former is not going to be fun as the KHTMLPart API is not refined or polished and highly KHTML specific.  The latter can only be accomplished with a lot of work set aside for refactoring or through nasty '#ifdef KHTML callThisWay() #else callThatWay();'

Both of these solutions are sub-optimal in my opinion.
<!--break-->
