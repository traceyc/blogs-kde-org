---
title:   "Beware QPainter::eraseRect() with a QBrush::texturedImage()!"
date:    2008-08-30
authors:
  - manyoso
slug:    beware-qpaintereraserect-qbrushtexturedimage
---
Funny little story... I was profiling with valgrind recently and was shocked to discover a huge hiccup in the painting performance of my QImage raster based app.  There was this repeated call to <a href="http://doc.trolltech.com/4.4/qpainter.html#drawTiledPixmap">QPainter::drawTiledPixmap()</a> that was soaking up CPU cycles.  It was a true mystery as I had thought the canvas of my app was completely QImage based.  <b>Not so.</b> 

You see, I had created a QBrush with a <a href="http://doc.trolltech.com/4.4/qbrush.html#textureImage">QImage texture</a> and called <a href="http://doc.trolltech.com/4.4/qpainter.html#eraseRect-2">QPainter::eraseRect()</a> with it in a fairly important path in my code.  What I didn't know was that QPainter was silently turning my QImage based textured brush into a QPixmap with <a href="http://doc.trolltech.com/4.4/qpixmap.html#fromImage">QPixmap::fromImage()</a> and using that to operate on my QImage based source device.

I have no idea why QPainter would decide to do this other than the mistaken assumption that QPixmap is always faster to draw.  And maybe that is true in some general case, but surely not when the device you are painting to is already itself a QImage.  And not in many other cases too.  In fact, I don't even know why the <a href="http://doc.trolltech.com/4.4/qbrush.html#QBrush-8">QBrush(const QImage &)</a> constructor exists if QPainter is going to insist on turning it into a QPixmap anyway.

Interestingly, a comment in the QPainter sources for the eraseRect shows another problem with this: it isn't thread-safe.  <a href="http://doc.trolltech.com/4.4/qimage.html">Qt 4.4 touts itself as thread-safe when painting to a QImage.</a>  But that isn't true here.  If you happen to try painting to a QImage in another thread and you have this use case you're in for some crashy, crashy.  I wonder that the comment doesn't recognize the performance implications of this...

So beware of QPainter::eraseRect() and a textured image.  <b>I really hope Qt 4.5 can solve some of these problems.</b>

<!--break-->