---
title:   "Qt painting is SLOW on N810"
date:    2008-08-22
authors:
  - manyoso
slug:    qt-painting-slow-n810
---
With all the recent interest in Nokia's N810 device among <a href="http://blog.forwardbias.in/2008/08/n810-is-awesome.html">KDE</a> <a href="http://blogs.kde.org/node/3628">and</a> <a href="http://blogs.kde.org/node/3624">Qt</a> <a href="http://www.notmart.org/index.php/Software/Misc_plasmoids_on_n810">developers</a> I think it is important to note that Qt/X11 painting performance is badly broken when running under Hildon.  I've been porting a QtWebKit based browser to the N810 for awhile now and have noticed a steep drop in performance when compared to the same browser running under Qtopia Phone Edition on the very same device.  

Specifically, Xrender performance on the N810 (at least the codepaths that Qt uses) is incredibly slow.  What's more, using CPU based raster image is also very slow because of the overhead of blitting the QImage to the screen under X11.  Inspired by <a href="http://zrusin.blogspot.com/2008/08/fast-graphics.html">Zack's recent post</a> I thought it would be a good idea to <b>quantify just how bad</b> the problem is.  With a little modification to disable the GL option, I ran Zack's <a href="http://ktown.kde.org/~zrusin/examples/qgears2.tar.bz2">qgears2</a> program on the N810.  

Here are the results in FPS:

<img src="https://blogs.kde.org/files/images/maemo-qgears.preview.png" alt="maemo-qgears" title="maemo-qgears"  class="image image-preview " width="640" height="384" />
GEARSFANCY
Xrender: 1.411 FPS
Raster:	2.750 FPS

GEARS
Xrender: 3.028 FPS
Raster: 5.004 FPS

TEXT:
Xrender: 6.016 FPS
Raster: 4.629 FPS

COMPO:
Xrender: 3.545 FPS
Raster: 4.060 FPS

Just horrible numbers.  However, this doesn't tell the whole story.  The raster based method incurs more overhead than the Xrender path by design.  For the raster based method all paint operations operate on a QImage and then are blitted at the end to the widget's backingstore.  This blit takes anywhere from 20-40% of the time depending upon the test.

These numbers taken together do not bode well for the idea of getting any kind of performance out of graphics intensive Qt programs on the N810. At least until Xrender is fixed.
<!--break-->