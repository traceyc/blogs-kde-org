---
title:   "dataKiosk, Kugar and Stuff"
date:    2005-06-16
authors:
  - manyoso
slug:    datakiosk-kugar-and-stuff
---
<b>dataKiosk:</b>

It has been a busy week and with the <a href="http://kde-apps.org/content/show.php?content=20631">release of dataKiosk 0.7</a> now completed I'm glad to see it is getting a <a href="http://dot.kde.org/1118884462/">little publicity on the dot.</a>  It is nice to know that others appreciate your work :)  The state of the application is pretty good at this point.  It still has bugs (if you come across any, please <a href="http://bugs.kde.org/">let me know</a>) and quirks, but it is mature enough to handle a former MS Access project with over a million records between a couple dozen tables.  Going forward, the focus is on making the data entry task as lighting fast as possible.  Then I hope to integrate KJSEmbed for scripting the Virtual Fields and business logic.

<b>Kugar:</b>

And as if I'm not busy enough, I've also agreed to take over maintainership of <a href="http://www.koffice.org/kugar/">Kugar.</a>  This app is about to get a whole lot of fresh new love and attention.  In fact it I already committed a bunch of it tonight.  More on that in a bit...

<b>Stuff:</b>
I still don't know if I'm going to be able to make it to aKademy :(  I've been so busy and this summer is just going to get worse.  I also don't think I have the wallet to fly from the US to Spain at this late of date.  Sucks, since I was really hoping to meet more KDE developers.  Drink beer.  Hack and stuff.

To make matters worse, Clee is coming over this weekend and we're going to hangout and maybe hack a bit ;)  We might even be social and go down to the Boston Common or something.  Originally, we were going to go scare the shit out of Geiseri by showing up at the door begging for food, but he's being a pissy bitch as usual.<!--break-->