---
title:   "dataKiosk release, la, la la"
date:    2005-03-16
authors:
  - manyoso
slug:    datakiosk-release-la-la-la
---
I just released v. 0.6 of dataKiosk.  The major changes include editing support via a new configurable form.  It also has some handy new editor widgets that are factory created depending upon the field's type.  The new widgets include a MS Access alike combo table dropdown foreign key relation editor.  For those of you who are interested you can check out the new <a href="http://extragear.kde.org/apps/datakiosk/">extragear homepage</a> for <a href="http://extragear.kde.org/apps/datakiosk/datakiosk-relation-combo.png">screenshots</a> and/or grab the 0.6 release bzip2 tar.