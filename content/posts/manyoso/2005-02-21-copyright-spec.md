---
title:   "Copyright a spec?"
date:    2005-02-21
authors:
  - manyoso
slug:    copyright-spec
---
<a href="http://blogs.kde.org/node/view/884">Brad Hards</a> brings up a twist to the whole kpdf, <i>"to DRM or not to DRM?"</i> question.  Would kpdf infringe Adobe's copyright on the pdf spec if they did not include "reasonable efforts" at implementing DRM?  

Are we sure it is even legally possible to <i>copyright</i> a specification?  That seems absolutely ridiculous to me.  How can <i>implementing</i> a given specification result in copyright infringement.  After all, you're not supposed to be able to copyright an idea.

More food for thought:  If Adobe can copyright their specification to the point that they can choose to license (or not license) according to their rules... what is to keep MS from copyrighting all of their interop interfaces.  Seems this is a door big enough to close all efforts at reverse engineering.  Which is another right we should possess.  Not that MS isn't trying to do the very same thing with patent law...

Anyways, I don't buy this one.
<!--break-->