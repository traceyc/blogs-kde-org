---
title:   "DataKiosk"
date:    2005-02-09
authors:
  - manyoso
slug:    datakiosk
---
DataKiosk is a JuK-like database interface tool for generic SQL databases.  What does that mean?  Essentially, DataKiosk provides a series of wizards (anyone familiar with Qt Designer's database wizards will find them familiar) that allow you to build a custom Juk-like interface for any SQL database with a QtSQL driver.  It now resides in kdecvs in the kdeextragear-1 module.
<br><br>
<img class="showonplanet" width="200" height="158" border="0" src="http://web.mit.edu/~treat/Public/datakiosk1-mini.png" alt="DataKiosk 0.5" />
<br>
Screenshots <a href="http://web.mit.edu/~treat/Public/datakiosk1.png">1</a>, <a href="http://web.mit.edu/~treat/Public/datakiosk2.png">2</a>, <a href="http://web.mit.edu/~treat/Public/datakiosk3.png">3</a>, <a href="http://web.mit.edu/~treat/Public/datakiosk4.png">4</a>, <a href="http://web.mit.edu/~treat/Public/datakiosk5.png">5</a>, <a href="http://web.mit.edu/~treat/Public/datakiosk6.png">6</a> and Flash demo <a href="http://web.mit.edu/~treat/Public/datakiosk.html">here</a>.  More below the fold...
<!--break-->
<br><br>
A little bit about where it came from... DataKiosk began as a solution for a customer with a thorny problem.  This customer had some nasty legacy databases with horrible user interfaces that all needed to be merged.  While brainstorming for a solution, I came up with the idea of a Juk-like interface.  After all, Juk and ITunes are wonderful interfaces for musical databases so, why not extend the concept to other generic SQL databases?
<br><br>
Basically, this customer had three disparate databases, all with different schema, all with data integrity issues, all with serious problems.  These databases were comprised of one Paradox 7 database, one Paradox 10 database, one MS Access database and one database yet to be created.

The three existing needed to be merged into a new database, the data thoroughly audited and the schema drastically transformed into an industry standard.  Long story short, I needed an interface that could be configured to work with all three and the resulting database that would be created from them.  Hence DataKiosk was born.
<br><br>
<b>What it can do:</b>
<ul>
<li> It can load tables with fully configurable fields.
<li> It can present Parent/Child tables through foreign key relations.
<li> Basic and Advanced Search where the terms are transformed into SQL SELECT statements.
<li> Integrated Kugar Reports where the report is created from the current search.
<li> Project based with Saved Searches, Saved Reports and so on.
</ul>

<b>What is planned:</b>
<ul>
<li> INSERT, DELETE, etc, etc.  It can not be used as an editor yet.
<li> Kiosk functionality to lockdown the project file for certain users.
<li> .desktop file creation for projects.
<li> Scripting support for integrated business logic.
<li> Perhaps a web export tool for automatic web interface generation.
<li> DataKiosk is heavily based on code from JuK and Qt Designer.  Many anachronisms need to be removed.
</ul>

<b>What it is not:</b>
<ul>
<li> Efficient -- to say the least -- on database resources.
<li> Suitable for very large databases.
<li> Suitable for 'enterprise' use.
</ul>

In the end, I hope it can be used as a generic data entry application for small businesses without the need for custom application development.  An kind of Excel-like app for SQL databases if you will.