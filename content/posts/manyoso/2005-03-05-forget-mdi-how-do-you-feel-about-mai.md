---
title:   "Forget MDI ... how do you feel about MAI?"
date:    2005-03-05
authors:
  - manyoso
slug:    forget-mdi-how-do-you-feel-about-mai
---
There are few certainties in life (death/taxes) and less in software, but if I were to nominate two they would be:

* MDI is here to stay.
* Some will always hate MDI.

Let's face it, it make to much sense in many cases to not have an MDI interface.  Look at our users love affair with Konq's tabs for instance.  KSirc's IRC tabs, Konsole's session tabs, aKregator's KHTML part tabs, on and on...  users would _kill_ if these were done away with.  Hell, I recently was checking out Kopete's IRC protocal handler and decided it was a non starter because it didn't offer to manage all of my IRC channels in one window.

The real problem facing developers shouldn't be <i>"to MDI or not to MDI"</i>?!  That is too simplistic.  Of course!  We _have_ to offer MDI in many cases.  The real problem is when the MDI metaphor gets blurred with the traditional single document interface.  Or even more complex situations that make this argument quaint.

It's not that your rant concerning Kate/KDevelop doesn't bring up some good issues, but the solution can't be, <i>"to hell with this MDI crap, let's make it all single document"</i> because that just won't work for a variety of reasons which I'll conveniently let others spell out ;)

For an even more pernicious case where the false dichotomy of MDI VS SDI is laid bare, check out, uhm, Kontact for instance.  Here we have multiple apps (some of which offer there own MDI) in a MAI (Multipe Application Interface) window!  KDE users begged for it.
<!--break-->