---
title:   "MS Office 12's Incredible New UI"
date:    2005-09-23
authors:
  - manyoso
slug:    ms-office-12s-incredible-new-ui
---
Ariya sent an email to the KOffice list today <a href="http://channel9.msdn.com/showpost.aspx?postid=114720">passing along this link to an interview with the program lead</a> for MS Office 12's new UI.  All I can say is WOW.  This is stunning stuff.  A colossal change and, IMO, a great one.  They have taken an incredibly complex app and eliminated all of the menu's _without_ eliminating the functionality.  They've also made it so that none of the functionality is hidden.

Here's a screenshot:

<img class="showonplanet" src="https://blogs.kde.org/system/files?file=images//office12ui_0.png" width="640" height="481"  alt="New Office 12 UI Screenshot" title="New Office 12 UI Screenshot" />

Now, I haven't actually played with it, so it might come off worse in practice (perhaps the hit to screen realestate is just too much for laptop users for instance), but I have to applaud their courage.

We have to show similar courage for KDE4.  No, not to eliminate our functionality, hide it or rock our users, but to develop something compelling :)

Can you imagine if we could take apps like Konqueror, KDevelop, KWord (insert-favorite-complex-KDE-app-here) and eliminate the menus while still delivering and presenting all of the functionality...  the mind boggles.
<!--break-->