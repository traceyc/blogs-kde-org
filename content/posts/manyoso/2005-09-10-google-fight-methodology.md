---
title:   "Google Fight Methodology"
date:    2005-09-10
authors:
  - manyoso
slug:    google-fight-methodology
---
First, it was just for shits and giggles folks.  Don't take it so seriously ;)

That said, quite a few folks wonder why I set 'restrict=linux' and not 'restrict=kde' or some such thing.  Well, the answer is quite simple...  If you'd click through to the <a href="http://www.google.com/apis/reference.html#2_4">parameters explanation link</a> that I posted in the <a href="http://blogs.kde.org/node/1428">methodology section</a> you'd see that Google has only four topic restrictions: US Government, Linux, Macintosh and FreeBSD ;)  There is no KDE restriction.  

Second, the algorithm Google uses to arrive at the estimated number is fuzzy.  Enter the same search on a couple of different days and it will change.  I don't know if this has to do with the cache or which servers are responding to the query...  Still, if you do the queries one after another then they should be reasonably relative.

Anyways, it was just for fun.  I'll probably do one with nicknames at some point...
<!--break-->