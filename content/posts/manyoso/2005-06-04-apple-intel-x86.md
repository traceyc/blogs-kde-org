---
title:   "Apple on Intel x86?"
date:    2005-06-04
authors:
  - manyoso
slug:    apple-intel-x86
---
I am  2 parts intrigued, 3 parts baffled, and 5 parts just plain <i>gobsmacked</i> by the breaking news that <a href="http://news.com.com/Apple+to+ditch+IBM,+switch+to+Intel+chips/2100-1006_3-5731398.html?tag=nefd.lede">Apple will be switching to Intel processors.</a>  The times, they are a chang'in!  Questions...

Does this mean that Apple will be releasing Mac OSX for non-Apple manufactured hardware?  If so, what will be Microsoft's reaction?  Hell, what will be KDE's reaction?  

And what in the hell is Apple thinking switching to x86 <a href="http://www.eetimes.com/news/latest/showArticle.jhtml?articleID=163106213">when this is coming down the pipe??!!</a>  Talk about a little late to the x86 bandwagon...<!--break-->