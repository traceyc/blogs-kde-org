---
title:   "Solaris, Java and the CDDL"
date:    2005-01-26
authors:
  - manyoso
slug:    solaris-java-and-cddl
---
By now everyone has heard of Sun's foray into <a href="http://www.opensolaris.org">Open Source Solaris</a>.  Groklaw has an <a href="http://www.groklaw.net/article.php?story=20050126023359386">interesting run down</a> of the incompatibilities between the <a href="http://www.opensolaris.org/license/cddl_license.html">CDDL</a> and the <a href="http://www.gnu.org/copyleft/gpl.html">GPL.</a>  I find the whole thing tremendously interesting, but when you really think about it and the possible implications for Linux, it is not all that exciting.  Sure, Solaris 10 is a hell of an operating system by most accounts, but at this point I think of it like the hoopla that surrounded Apple's release of Darwin.  That too, was one hell of an operating system, with all of the doomsayers heralding it as the end of Linux... Well, a few years later not so much.  Heh.

Now, while I find Sun releasing Solaris 10 under the CDDL, is in the end, much ado about nothing, <a href="http://www.eweek.com/article2/0,1759,1753535,00.asp">Sun releasing all of Java under the CDDL...</a> well, the implications boggle the mind.  Were this to happen, I can't help but think it would shake the FOSS community pretty darn hard.  Java would once again find itself in the limelight with renewed stocks of hype and tech buzz a plenty.  We'd see umpteen fresh calls announcing the ascendancy of Java.

I go way back with Java, but in recent years I have grown to hate it.  Utterly.  I hate the packaging model.  I hate the classpath headaches.  I hate the horribly slow GUI toolkits.  I hate the needless complexity of the various frameworks.  That said, it seems to be improving.  But every time I've gone back, out of curiosity or necessity, I'm always reminded by one thing or another of how Java disgusts me.

Who knows what the future holds.  Perhaps Sun will grow brash with all the publicity they are generating and decide to go the same way with Java, releasing it under the CDDL.  Perhaps Sun will tuck tail and decide that Solaris 10 was enough.  Who knows.  It'll be interesting to watch, though.  Any bets?