---
title:   "KGet popup notifications"
date:    2009-10-10
authors:
  - dario massarin
slug:    kget-popup-notifications
---
Now you can't say anymore you didn't noticed the download was already completed :) We show this only if the KGet window is not active.

<img src="https://blogs.kde.org/files/images/kget1.png" />

This is the only small "feature" I recently added. In fact most of my work has been related to making kget more usable and more stable. Ah, BTW, can't make it crash anymore... Please please make it crash otherwise we'll think it's already stable! :). Enjoy!