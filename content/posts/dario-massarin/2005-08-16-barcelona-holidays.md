---
title:   "Barcelona holidays !!"
date:    2005-08-16
authors:
  - dario massarin
slug:    barcelona-holidays
---
Tomorrow evening me and my friends will take the airplane to Barcelona I've rented a small flat in Barceloneta, near the beach, for 10 days. I really really hope to enjoy myself and now I'm starting to feel quite excited about this. 

So I'll stop my kde-related development and I'll not reply to any mail you'll send me. In the meantime I'm sure the work on kde4 will continue and I hope we really start doing the real hard stuff on kdelibs. There are lots of classes to clean up, since Qt4 seems to provide lots of functionalities that we implemented in the libraries.

I'm afraid that as far as kdelibs aren't in a good shape, the work on kde4 applications will be really slowed down. In the development of the make_kget_cool branch I've encountered lots of kdelibs bugs and, even if this is completely normal in this early stage of kde4 development, I hope we will not take too many months to setup something stable enough to focus on application development.

This is why recently I've started looking at kdelibs and started with fixing a bug. I've also spent some time looking at the KToolBarButton class, which was causing me some troubles, and I've been very pleased to realize that with Qt4 this one will loose really lots of code, since the QToolButton class has nearly all the things we need. This is a sign of the cleanliness and the power of new the Qt4 api. Good work, Trolltech! Maybe when I'll be back from Spain I'll try to help out with this stuff.

Happy hacking to everyone!
<!--break-->
