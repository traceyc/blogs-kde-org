---
title:   "KGet gets some love :)"
date:    2009-10-07
authors:
  - dario massarin
slug:    kget-gets-some-love
---
You will hardly remember of me, since I've not being so active recently (my job takes me lots of time resources). Anyway, stay calm.. It's been proved that knowing who I am will not make you feel any better :) That said it's not about me that I want to talk but about a great coding team doing a great job with a very promising application. That application is.. imagine.. you already know since it was in the title.. It's KGet.

I've been the man who, some years ago, started rewriting this application from scratch. Now, I'm lucky enough to have around me some great developers helping around and doing great things, like Lukas Appelhans, Matthias Fuchs, Urs Wolfer and some others. So now I decided to take the big step and assign the maintainership to Lukas :)

Well, accordingly to this <a href="http://www.tomshardware.com/reviews/linux-internet-application,2407-8.html">article</a> it looks like KGet is the only download manager (among the ones they reviewed) that supports bittorrents downloads. Well.. This made me smile a little bit, since in KGet is so easy to integrate a new protocol thanks to our plugin infrastructure :)

Ok, You'll ask.. But <b>what's the current state of KGet in trunk?</b> I would say it's in a good shape :) As Matthias already <a href="http://mat69.wordpress.com/2009/08/18/gsoc-kget-wrapup/">blogged about</a> we have already merged in trunk a lot of great feautures and we are now focusing a lot on bug fixing. We took some decisions to make the code safer and already rewrote some core parts of KGet. We also wrote an unit testing application which gives thousands of commands to kget via DBUS calls, just to verify the overall stability.

So.. <b>what's next?</b> We really need your help to test it more and to find as many bugs as we can. We definitely want the next version to rock. So, people, checkout the current trunk version, compile it and report as many bugs as you can :)

