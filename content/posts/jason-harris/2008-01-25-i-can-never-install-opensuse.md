---
title:   "I can never install openSUSE"
date:    2008-01-25
authors:
  - jason harris
slug:    i-can-never-install-opensuse
---
I've tried before, the installer always fails for me.  But, I'm getting fed up with Gentoo (finally), so I decided to give it another try.  Surely this time would be better...
<!--break-->
Boot from the DVD, the progress bar on the green screen gets to around 30% and just sits there.  Hard-boot.  Try again, press Esc to see log messages.  It's hanging on: "braille.4.1 alva read data".  

Search opensuse forums and mailing lists...nothing.
Search google...nothing.

What the heck?  It's not like I have bizarre hardware.  I'm just cursed I suppose :P

If anyone knows of a boot option to let the kernel know that I am sighted and therefore don't need braille, PLMK.

