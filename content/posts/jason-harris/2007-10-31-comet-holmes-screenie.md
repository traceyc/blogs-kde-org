---
title:   "Comet Holmes screenie"
date:    2007-10-31
authors:
  - jason harris
slug:    comet-holmes-screenie
---
As blogged by <a href="http://blogs.kde.org/node/3076">Bart Coppens</a>, there's currently a naked-eye comet gracing northern-hemisphere skies: <a href="http://en.wikipedia.org/wiki/Comet_Holmes">comet Holmes</a>.  It's in KStars, but you should update the ephemerides with Get New Stuff (Ctrl+D), or the position will be way off.  Here's a screenie showing its position as of tonight:

<a href="http://www.30doradus.org/kstars/comet_holmes.png"><img src="http://www.30doradus.org/kstars/comet_holmes_small.png"></a>

Holmes is a very strange comet.  Usually, a <a href="http://en.wikipedia.org/wiki/Comet">comet</a> gets bright only as it approaches the Sun, because the increase in radiant energy causes some of the ice to sublimate, resulting in a temporary "atmosphere" (called the coma) of gas and dust.  The gas portion gets ionized by solar radiation and the solar wind, and glows (just like the aurora borealis/australis in our atmosphere).  

Anyway, Holmes isn't near the Sun.  In fact, its orbit never carries it inside Mars's orbit, which is pretty far out; usually comets are quite dormant at this distance.  Indeed, until October 23rd of this year, Holmes was extremely faint, invisible to all but the largest telescopes.  Then, quite suddenly, its brightness increased by a factor of 1 million in just a few hours, making it the third brightest object in Perseus!  Calculations suggest that the size of its coma is some 1 million km, which is about 3 times the distance between the Earth and the moon.

I haven't heard much credible speculation regarding the cause for this outburst.  A lot of people think it must have hit something, or broken apart for some reason.  That seems plausible at first; however, any successful hypothesis will have to also explain the fact that this isn't the first such outburst that Holmes has had: there was another one in the 1800s.  Then, for over a century it floated quietly in the void between Mars and Jupiter, without so much as a burp.  Until now. 