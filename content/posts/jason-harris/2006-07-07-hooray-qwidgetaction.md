---
title:   "Hooray QWidgetAction!"
date:    2006-07-07
authors:
  - jason harris
slug:    hooray-qwidgetaction
---
With Qt-4.2, we finally get back the flexbility of adding arbitrary widgets to action containers like toolbars and menus, via the new QWidgetAction class.  For QToolbar, there was already a workaround for this in the addWidget() function, but there was no way to add a widget to a menu.

Why would one want to add arbitrary widgets to a menu?  It's not just for gee-whiz bells and whistles.  For example, in the K menu in 3.5 we had non-interactive "section headers" to organize the menu ("Most Used Applications"; "All Applications", and "Actions")...these are now possible again in KDE-4.0.  Also, instead of the "Run Command..." item that opens the mini-cli, we could have a mini-mini-cli embedded right in the K menu!

In my case, the context-sensitive right-click menu in KStars presents information about the clicked object right in the menu (name, type, constellation, and rise/set/transit time).  We had been forced to display this information as "disabled" actions, which made the text greyed out and generally ugly.  Now we can do it with QLabels that allows the text to be centered and boldfaced.  

Thanks, TT for this flexibility.  I should also mention that KAction is now derived from QWidgetAction (though the APIDOX don't yet reflect this change), so you can use this functionality with KActions!  8-)


