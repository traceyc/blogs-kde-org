---
title:   "KStars Community Forums"
date:    2006-03-03
authors:
  - jason harris
slug:    kstars-community-forums
---
One of our users suggested that it would be useful to have some mechanism by which members of the KStars user community could meet and interact, either a mailing list or a web forum.  I totally agree.  I know there are many people using KStars, and there are lots of reasons that they would want a way to talk with each other, share custom content that they've made, rant about their most hated bugs, whatever.  

We've had a user mailing list for years, but it's always languished in near-absolute silence and obscurity.  Probably because we never really made people aware of its existence.  This time, we're trying a web-based forum, and hopefully this time, more people will find out about it.  Here it is:

<b><a href="http://kstars.30doradus.org">http://kstars.30doradus.org</a></b>

We've had the boards up and running for a few days, and just announcing it on our development mailing list has already attracted some members.  We hereby invite all interested users to join the KStars Community forum!  Hope to see you there.
