---
title:   "My SUSE curse continues"
date:    2008-07-22
authors:
  - jason harris
slug:    my-suse-curse-continues
---
I've never been able to successfully run SUSE or openSUSE, on any of my machines.  I don't understand why; it just doesn't work.  My latest attempt was to download and burn the openSUSE <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> CD, last night.

Download ISO.  Verify checksum.  Burn ISO. Reboot machine.  Wait several minutes (more than five) for the boot process.....

Finally!  I was greeted by the functional, friendly and green openSUSE KDE4 desktop.  Woohoo, right?

I eagerly fired up my favorite app (Alt+F2, "kstars"), but before I could get through the Startup Wizard, my speakers emitted a short burst of rather distressed-sounding chirps, and then the machine was totally unresponsive.  I had to hard-reset.

On the second attempt to boot the live CD, it hung during the boot process.  Sigh.

I just tried a third time this morning, and it seemed to be working fine this time.  However, I'm planning to send this to someone with whom I'd like to make a good impression, so the flakiness is a bit troubling.

Can anyone recommend another LiveCD that includes KStars from KDE4?  Not KDE4Daily, I need an actual liveCD.

thanks!

