---
title:   "Diving into the OpenGL pool..."
date:    2003-10-02
authors:
  - jason harris
slug:    diving-opengl-pool
---
So, does anyone know any good resources for OpenGL programming under KDE?  I've started looking at the Qt OpenGL examples, and that seems straightforward enough.  

However, Someone at <a href="http://dot.kde.org">the dot</a> told me about the <a href="http://www.coin3d.org">Coin project</a>, which is an implementation of SGI's high-level <a href="http://oss.sgi.com/projects/inventor/">Open Inventor API</a> for OpenGL.  Even better, they have a library called <a href="http://doc.coin3d.org/SoQt/">SoQt</a> which is supposed to let you easily make openGL Qt apps.  I'd really like to try to use SoQt for KStars, but from what I have read so far, it isn't very flexible.  You have to place "SoQt::init(argc,argv)" inside your app's main function, which instantiates a main window for drawing OpenGL stuff.  This would seem to preclude making the window a KMainWindow.  Is there a way to use SoQt to just make a widget, instead of a toplevel window?  Also, it wasn't clearly stated, but I got the impression that it wasn't supported under Qt 3.x yet.  Can anyone confirm/refute that?

Is it possible to just use Qt's QGLWidget and then make calls to coin's Inventor API, or do I have to do coin all the way?

I'd love to hear people's thoughts and opinions on SoQt, QGLWidget, GL vs. GLUT, ar any related options that I have yet to discover :).  Thanks!