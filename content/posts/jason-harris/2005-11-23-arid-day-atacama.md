---
title:   "An arid day in the Atacama"
date:    2005-11-23
authors:
  - jason harris
slug:    arid-day-atacama
---
Yes, that's right, I am once again at my <a href="http://www.lco.cl/lco/magellan/">favorite telescope</a> in northern Chile.  Getting a metric assload of data, which should keep me busy for a while.  Normally, I would say at this point that it's nice to get away from winter and experience a summery Southern November, but I happen to live in Tucson, AZ, so I am actually missing the best weather of the year! :p

Also wanted to give a shout-out to aseigo: thanks for fixing that kicker bug!  I know it wasn't fun to track down.  BTW, why is the <a href="http://bugs.kde.org/show_bug.cgi?id=113235">BR</a> still open?

<!--break-->