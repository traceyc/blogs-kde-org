---
title:   "confused about Qt QPL/GPL license"
date:    2003-08-26
authors:
  - jason harris
slug:    confused-about-qt-qplgpl-license
---
(EDIT: changed the title.  Thanks for the discussion, I am less confused now :) )

How many of you, when confronted with the Qt licensing option (during the 'configure' step of the compile) select the GPL as your Qt/X11 license?  I had always done so, but I have come to understand that using KDE precludes this option.  
<br>
<br>
If you use KDE, the QPL is your only non-commercial licensing option, because a GPL'd library cannot be linked with non-GPL code.  KDE contains lots of this.  Here's a partial list, just of the core apps and libs:
<ul>
<li> kdelibs (LGPL)
<li> kwin (BSD)
<li> kicker (BSD)
<li> ksmserver (BSD)
<li> klipper (Artistic)
</ul>
There are many more besides these.
<br>
<br>
Anyway, just some food for thought.  Since there's no *explicit* choice made between QPL and GPL, I guess anyone who mistakenly chose the GPL like I did can just retroactively change their mind and continue using Qt under the QPL.  I have heard the opinion that the Qt licensing option can be interpeted as allowing the user to decide on a per-application basis which license they will use, but I can't really buy that.  Besides, it doesn't matter since kdelibs is LGPL'd, and <a href="http://www.fsf.org/licenses/gpl-faq.html#IfLibraryIsGPL">therefore can't be linked with GPL'd Qt/X11</a>.  (EDIT: actually, combining a GPL'd lib with a LGPL'd lib can be allowed, *if* the resulting app is GPL'd.  So maybe there's hope for this "per-application" Qt licensing meme, though it still seems like a huge stretch to me)
<br>
<br>
BTW, why not eliminate the GPL option in qt-copy?  It just adds confusion.
<br>
<br>
(EDIT: how am I supposed to insert paragraph breaks?  it doesn't recognize the p tag, so I used two br's in a row)