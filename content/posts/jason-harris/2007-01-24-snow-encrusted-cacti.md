---
title:   "Snow-Encrusted Cacti"
date:    2007-01-24
authors:
  - jason harris
slug:    snow-encrusted-cacti
---
Every five years or so, it snows in Tucson.  We got our latest taste last Sunday:

<a href="http://marvin.as.arizona.edu/~jharris/gallery/v/tucson/snowytucson/"><img src="http://marvin.as.arizona.edu/~jharris/gallery/d/43056-2/IMG_3305.JPG"></a>
(click for more photos)

I grew up in Illinois, where the winters were long and relatively harsh.  So it's pretty funny to watch native Tucsonans freak out after 1 cm of snowfall.  Schools were shut down.  More than 50 bridges in town were closed to all traffic.  Local auto stores sold out of ice scrapers.  You know, mass hysteria! &lt;/vinkman&gt;

The snow was all gone by about 10:30 am, so I'm glad I was able to get some pictures of it.  They will be nice to look at in 6 months when the temperature is 45C!

