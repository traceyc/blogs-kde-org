---
title:   "KStars: new details window"
date:    2005-03-10
authors:
  - jason harris
slug:    kstars-new-details-window
---
Finally, something worth blogging in KStars-land:
The Object Details window is getting a facelift.
<!--break-->
Before and after:

<a href="http://www.30doradus.org/kde/old_details.png"><img src="http://www.30doradus.org/kde/old_details_small.png"></a>
<a href="http://www.30doradus.org/kde/new_details.png"><img src="http://www.30doradus.org/kde/new_details_small.png"></a>

The layout is much less cluttered, and all of the data fields now use kde:KActiveLabel, so the text can be copy/pasted with the mouse.  Best of all, there's now a user-customizable thumbnail image of the object.  If you click on the image, the following tool opens:

<a href="http://www.30doradus.org/kde/thumb_chooser.png"><img src="http://www.30doradus.org/kde/thumb_chooser_small.png"></a>

Who can name that galaxy? ;)

The list is populated by images downloaded from "the internets", from two sources: the object's own list of image URLs (as shown in the popup menu), and also from a Google image search for the object's name.  Usually the google search works pretty well, but if you pick an obscure object, none of the images found will acually be of the object.  For example, try IC 440.

Anyway, just wanted to share :)
