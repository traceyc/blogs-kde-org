---
title:   "Highlighting Akarsh Simha's GSoC work"
date:    2008-08-04
authors:
  - jason harris
slug:    highlighting-akarsh-simhas-gsoc-work
---
Akarsh Simha is pursuing a very ambitious project for Google Summer of Code 2008: He's increasing the number of stars displayed by KStars by a factor of 10 (from 130,000 to over 1 million), without having a negative impact on the performance of the program. James Bowlin and I are his co-mentors in this project. James and Akarsh spent the pre-coding SoC period hashing out the code architecture required for this effort, which allowed Akarsh to hit the ground running when he started to code. His progress has been amazing: in the kstars/summer branch, we are already displaying over 2 million stars, and it feels just as responsive as trunk, because the new architecture is extremely efficient in memory usage. 

Sound too good to be true? Check out this <a href="http://www.30doradus.org/kde/soc2/">side-by-side comparison of 3.5 branch and trunk</a>, where I zoom in on the <a href="http://en.wikipedia.org/wiki/Carina_Nebula">Carina nebula</a> region with both versions of the program.

We already have plans to increase the number of stars to 10 million, 100 million...maybe even 1 billion stars.  However, the data for 1 billion stars is around 60 GB, so distribution is problematic.
