---
title:   "Install Hell"
date:    2005-10-16
authors:
  - jason harris
slug:    install-hell
---
I finally decided to replace my sick hard drive, which had been hobbling along for months.  I decided to take this as an opportunity to try out the new SUSE 10.0.  I've been a happy Gentoo user for a few years, but I've heard good things about SUSE and wanted to try a distro that would "just work".  *cue foreboding music*...

I first tried the "Network installation", which requires only one CD, and downloads packages from the net.  Early on in the installation process, it asked me for the address of the FTP server which contains the sources, with an unhelpful hint of "0.0.0.0".  Gee, thanks.  So, I scoured the net, looking for docu on using this network install.  I found almost nothing.  I tried filling in the address of one of the SUSE distribution mirrors (mirrors.kernel.org), which it resolved, and logged me in to the server.  Groovy.  Then it asks for the directory containing the distribution files.  Well, I have no idea, and apparently I am the only one on the planet to ever try this installlation option, because I am unable to find any indication anywhere that this is even an issue, much less find a solution.  So, I try some likely directories (suse/i386/10.0), but it never works, it always fails with "File not found".  Argh, at least tell me what file you are expecting to see, then maybe I can fumble around and find it for you!

Next, I decided to try the full 5-CD OpenSUSE-10.0 installation.  At first, this went much more smoothly.  However, after installing files from the first CD, rather than asking for the 2nd, it reboots!  No message about what the heck is going on (should I remove the CD?), it just reboots.  Okaaaay.  Then the new SUSE boot menu comes up.  Hmm, which should I do?  "Boot from Hard disk" or "Installation" (again).  I guess "Installation", hoping that it will know we are continuing that process, but no.  That was the wrong guess.  After getting to the partition step, I reboot again, and this time select "Boot from hard disk".  Okay, that was what it was expecting me to do (thanks for letting me know!).  The rest of the package installation proceeds normally; it asks me for disks 2, 3, 4, and 5.  Then some final configuration, and we're ready to boot the new system!  Woohoo!

Reboot.  Post.  Grub.  Select SUSE.  Blank screen.

That's it.  So, I give up on SUSE.  Hmm, I really don't want to spend hours on another Gentoo install.  Instead, I try to copy over my old Gentoo root partition with dd, but I couldn't get that to work.  

Well, let's give Kubuntu-5.10 a try.  People rave about its ease-of-use, and that's what I'm looking for.  Burn the install CD.  Boot, install, hang.  The package installer gets to "Configuring ttf-freefont", where it hangs.  According to google and the ubuntu forums, no one in the history of computing has ever had this problem, so I'm stuck.  I let it sit there for half an hour, hoping it's just a long step.  Then I reboot.

Kubuntu boots me to a text prompt.  Hmm, so how can I attempt to repair the failed install?  I'm not very familiar with dpkg/apt, but I stumble around a bit, and eventually get an error message which leads me to try "sudo dpkg --configure -a", which actually seems to be continuing where the install left off.  Groovy.  Well, for a while.  It eventually hangs again.  

Just then I realized that these hangs may be own fault.  My old HD (which is still in the case, so I can retrieve my personal files) sometimes hangs in this way, but usually only when the partition with bad sectors is being accessed.  The install isn't accessing that partition (it better not be!), but the partitions on the old disk are being mounted.  So I decide to start all over, this time telling the partitioner not to mount any partitions on the old disk.  Unfortunately for me, the install process is still hanging, this time on "Configuring hal".  >:(

So, perhaps my new disk is fubar?  Perhaps the old disk is still exerting is poisoned influence, even from beyond the veil of its un-mountedness?  It would be nice if I could see some kernel messages to deduce what is going on.  I'll investigate the latter possibility by...yet again...restarting the installation process, this time after disconnecting the old disk.

Are you surprised?  It hangs again.  This time I get an error message "Unable to handle kernel paging request".  Great.  Does that mean there's some issue with the swap space?  Looks like I am computer-less for a while...
