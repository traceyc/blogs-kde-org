---
title:   "new KDE3 dev book in the works!"
date:    2003-07-21
authors:
  - jason harris
slug:    new-kde3-dev-book-works
---
Did you hear that Ralf Nolden is starting work on a <a href="http://lists.kde.org/?l=kde-core-devel&m=105850839120182&w=2">new KDE3 development book</a>?  Wahoo!  It's going to be under the  FDL, natch.  Following <a href="http://lists.kde.org/?l=kde-devel&m=105870514021690&w=2">Holger's suggestion</a> that the book be a constantly updated guide to everything KDE, I propose it be titled "The KDEnomicon" :)
