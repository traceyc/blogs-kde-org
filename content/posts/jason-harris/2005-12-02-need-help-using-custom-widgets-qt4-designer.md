---
title:   "Need help using custom widgets in Qt4 designer"
date:    2005-12-02
authors:
  - jason harris
slug:    need-help-using-custom-widgets-qt4-designer
---
Sorry for the slight abuse of the blogosphere, but I did post this request (twice) on kde-devel, and got no replies.  I'm having trouble getting my custom widgets working in Qt4 designer; read on if you think you might be able to help, or if you want to learn more about how to use custom widgets in the new designer.
<!--break-->

In Qt4 Designer, it is apparently not possible to insert custom widgets unless you have written a designer plugin library which defines your widgets.  While this is a lot more overhead than what Qt3 designer required (just specify the name of the widget's header file), there is a "makekdewidgets" script available that makes it really easy to create the plugin library.  However, I can't convince it to install my plugin library.

I have relocated all code defining custom widgets for KStars into a new <a href="http://websvn.kde.org/trunk/KDE/kdeedu/kstars/kstars/widgets/">"widgets"</a> subdirectory, and constructed a <a href="http://websvn.kde.org/trunk/KDE/kdeedu/kstars/kstars/widgets/Makefile.am?rev=480581&view=markup">Makefile.am</a> that is supposed to create two libraries:  libkstarswidgets.la contains the object code for the widgets themselves, and gets linked into the main application binary.  kstarswidgets.la is the designer plugin library.  

To create the designer plugin library, I simply followed the existing implementation in kdepim, which was pointed out to me by Volker Krause.  It's actually quite simple: you just create an XML file that describes each widget, and add some fluff to the Makefile.am to run makekdewidgets on that file and compile the result.

It mostly works: I get both libraries created when I run "make".  However, nothing gets installed to my Qt4 designer plugins directory when I run "make install", and I can't figure out why that is.  If anyone can help me out, I'd appreciate it.

