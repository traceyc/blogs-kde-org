---
title:   "Porting to KDE4:  It's time!"
date:    2006-12-12
authors:
  - jason harris
slug:    porting-kde4-its-time
---
A recent <a href="http://dot.kde.org/1165793800/">story</a> on the Dot sparked a conversation about which branch KDE application developers should be developing against (3.5 or 4.0).  My usual stance on what developers "should" be doing is that they should do whatever they like.  Most of us are hobbyists, so go ahead and scratch that itch.  Trying to tell volunteers what they "should" be doing is at best futile and at worst, insulting.

Still, having said that, I do believe that (especially if you are developing an app that is included in one of the KDE modules) you do have some responsibility to the project as a whole.  Just as the community of your fellow developers can expect you to write API documentation and fix bugs, I think there is also a reasonable expectation of support for the 4.0 branch.  

One way to support the development of the 4.0 branch is to port your app sooner rather than later, and develop new features in trunk instead of in branches based on 3.5.  I'm not trying to impose it as a "rule" or to berate developers who chose delay porting; I'm merely suggesting that now is the right time to do it:
<!--break-->

<ul>
<li> In my experience, kdelibs is reasonably stable, and has been for months.
<li> There are remaining issues, but they will surely get resolved more quickly if more people are using the code ("many eyes make all bugs shallow")
<li> I believe that the release of 4.0 will be a very important step in KDE's history; the longer we are developing in trunk leading up to release, the better the release will be.
</ul>

I posted some comments in that thread on the Dot; these comments simply addressed what I perceive to be a misconception among some in the community: that kdelibs is not yet stable enough for development.  One poster claimed that kdelibs is so unstable, that application developers would have to constantly be fixing the libs *themselves*, if any of them were foolish enough to develop in trunk.  This is simply ridiculous.  In my own experience, almost everything is in a working state in trunk, and it's been that way for a while now.  

So, I encourage you to start porting your app soon, if you've been putting it off.  At least think about it.  Do you have a good reason to keep waiting?  
