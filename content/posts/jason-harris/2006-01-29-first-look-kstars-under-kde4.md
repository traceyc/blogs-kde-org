---
title:   "First look at KStars under KDE4"
date:    2006-01-29
authors:
  - jason harris
slug:    first-look-kstars-under-kde4
---
I've spent the last few months on the behind-the-scenes architecture of KStars.  Since the port to KDE4 was going to be a major overhaul for us anyway, we decided to simultaneously do a major refactoring of how the data catalogs are represented in KStars.  We're now using a hierarchical Composite/Component model, and it's working well.

Once that was nailed down, I began the dull drudgework of porting to KDE4.  It was mostly search-and-replace, followed by bug hunting.  Class by class, I replaced QPtrLists with QLists, for(Iterator)'s with foreach()'s, and QPoints with QPointFs (to name a few).

All of that effort was in service of a dream: of seeing, for the first time, my beloved KStars rendered in full antialiased glory.  Ladies and gentlemen, I give you: KStars under KDE4:

<!--break-->

First, KStars under KDE4.  Then, the same view, with KStars under KDE3.x
<p>
<img src="http://www.30doradus.org/kstars/kstars-4.x.png"><img src="http://www.30doradus.org/kstars/kstars-3.x.png">
<p>
Here also is a detail showing the antialiasing:
<p>
<img src="http://www.30doradus.org/kstars/kstars-4.x_detail.png">&nbsp;<img src="http://www.30doradus.org/kstars/kstars-3.x_detail.png">
<p>
KStars still needs a LOT of work before it's usable under KDE4, but it's really nice to at least see it in action again.  