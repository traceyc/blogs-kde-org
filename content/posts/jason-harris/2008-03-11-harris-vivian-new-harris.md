---
title:   "Harris *Vivian = new Harris();"
date:    2008-03-11
authors:
  - jason harris
slug:    harris-vivian-new-harris
---
Say hello to our new baby daughter, Vivian:

<img src="http://www.30doradus.org/baby/baby.jpg">

She was born on Feb 25th.  Her middle name is QiHui, which is Mandarin for "bright angel".  Haiyin and I are very happy, and not a little tired!

Some day, I will have time to work on KStars again...but it probably won't be soon!

