---
title:   "Clarification"
date:    2008-06-27
authors:
  - jason harris
slug:    clarification
---
I got some good comments on my last post.  Let me try to clarify what I was trying to say, because I don't want to be misunderstood as being denigrating toward users:

Make something beautiful, and make it available.  The users will come, and from the users, contributors will rise.  That's what sustains a project like KDE.  The users are not the end goal, IMO, but they are an important part of the feedback cycle that gives us a healthy and active community of contributors.
