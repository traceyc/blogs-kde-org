---
title:   "kstars status"
date:    2003-07-21
authors:
  - jason harris
slug:    kstars-status
---
For my first post, I thought I would give a short heads up on recent activity in KStars.

Jasem Mutlaq recently added telescope control to KStars, which is just fantastic.  Definitely has been one of our most-requested features.

I've been working on some data-plotting tools, such as the Altitude vs Time tool, a tool showing Jupiter's Moons as a function of time, and a solar system viewer.  These tools use a new widget (KPlotWidget) for the data plotting, which is available as a library in kdeed/libkdeedu/kdeeduplot.  The widget handles a lot of the plotting code automatically; you just have to feed it the data to be plotted (in the form of KPlotObjects, a companion class in the library).  Carsten Niehaus is also using KPlotWidget in Kalzium.  I may also let the developer of KST know about it.

I've also been trying to get a command-line "image dump" mode working.  To do this, I had to do quite a bit of backend/frontend separation so that the program can initialize itself without any GUI elements.  This is working, but I can't seem to get at the sky QPixmap, and I don't know why.  If you want to try it out, invoke dump-mode like so:  
% kstars --dump --width=800 --height=600 --filename foo.png
Thanks in advance for any hints!

I've been quite busy, so not much time for KStars.  I am moving to Arizona in the fall, so I am not planning for any more huge developments before 3.2, just thinking about getting the code and docs ready.

PS- what is the deal with the listbox of "Projects" on the edit post page?  Seems like an odd selection to me, don't really know where to put KStars-related entries.  Can we at least get a KDE-Edu entry?
