---
title:   "mailing list archive"
date:    2003-09-23
authors:
  - jason harris
slug:    mailing-list-archive
---
so after weeks of flakiness with our sourceforge mailing list, I finally requested a kstars-devel@kde.org mailing list, which we have been happily using for a few weeks now.  It's great!  

Unfortunately, we still aren't being archived at lists.kde.org, despite 2 emails to webguy@theaimsgroup.com and one to sysadmin@kde.org (all of which were unanswered).  Does anyone know from experience how long it takes to get listed at lists.kde.org, and if the complete archive since the list started will be available?

CORRECTION: I had originally said I sent email to "webmaster@kde.org", when in fact it was "sysadmin@kde.org".  The KStars Blog regrets the error.  Anyway, I didn't mean this to sound like a complaint; I am just seeking info on how long it usually takes, and whether or not the full archive will be available.

In other news, Ridley Scott's Sci-Fi classic "Alien" is returning to theaters this Halloween.  How excellent!  Director's cut, too.  Woo-Hoo! :)